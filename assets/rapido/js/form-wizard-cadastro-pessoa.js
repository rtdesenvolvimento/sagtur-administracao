var FormWizard = function () {
	"use strict";
    var wizardContent = $('#wizard');
    var wizardForm = $('#form');
    var numberOfSteps = $('.swMain > ul > li').length;

    var initWizard = function () {
        wizardContent.smartWizard({
            selected: 0,
            keyNavigation: false,
            onLeaveStep: leaveAStepCallback,
            onShowStep: onShowStep,
        });
        animateBar();
        initValidator();
    };
    var animateBar = function (val) {
        if ((typeof val === 'undefined') || val === "") {
            val = 1;
        };
        
        var valueNow = Math.floor(100 / numberOfSteps * val);
        $('.step-bar').css('width', valueNow + '%');
    };
    var validateCheckRadio = function (val) {
        $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
			$(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
		});
    };    
    var initValidator = function () {

        $.validator.setDefaults({
            errorElement: "span",
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.attr("type") === "radio" || element.attr("type") === "checkbox") {
                    var el = $(element).closest('.form-group').children('div').children().last();
                    error.insertAfter(el);
                }  else {
                    error.insertAfter(element);
                }
            },
            ignore: ':hidden',
            rules: {
                nome: {
                    minlength: 2,
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                cpf: {
                    required: true,
                    minlength: 2
                },
                rg: {
                    required: true
                },
                sexo: {
                    required: true
                },
                celular: {
                    required: true
                },
                dataNascimento: {
                    required : true
                },
                viagem: {
                    required: true
                },
                vendedor: {
                    required: true
                },
                local_embarque: {
                    required: true
                },
                formas_pagamento: {
                    required: true
                },
                tipos_hospedagem: {
                    required: true
                },
                'temos_de_aceite[]': {
                    required: true
                },
                'nomeDependente[]': {
                    required: true
                }
            }
            ,
            highlight: function (element) {
                $(element).closest('.help-block').removeClass('valid');
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            success: function (label, element) {
                label.addClass('help-block valid');
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
            }
        });
    };

    var onShowStep = function (obj, context) {
        $(".next-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goForward");
        });
        $(".back-step").unbind("click").click(function (e) {
            e.preventDefault();
            wizardContent.smartWizard("goBackward");
        });
        $(".finish-step").unbind("click").click(function (e) {
            e.preventDefault();
            onFinish(obj, context);
        });

    };
    var leaveAStepCallback = function (obj, context) {
        return validateSteps(context.fromStep, context.toStep);
    };
    var onFinish = function (obj, context) {
        if (validateAllSteps()) {
            $('.anchor').children("li").last().children("a").removeClass('wait').removeClass('selected').addClass('done').children('.stepNumber').addClass('animated tada');

            if (wizardForm.valid()) {
                wizardForm.validate().focusInvalid();
                wizardForm.submit();

                $.blockUI({message: '<h3> Enviando sua reserva para o servidor...</h3>'});
            } else {
                alert("CAMPO OBRIGATÓRIO! Você não preencheu algumas das informações solicitadas. O campo obrigatório será destacado em vermelho. Navegue acima na tela para verificar.");
            }

        }
    };
    var validateSteps = function (stepnumber, nextstep) {

        var isStepValid = false;
        if (numberOfSteps >= nextstep && nextstep > stepnumber) {

            if (wizardForm.valid()) {
                wizardForm.validate().focusInvalid();
                if(nextstep === 2){
                } else if (nextstep === 3) {
                } else if (nextstep === 4) {
                } else if (nextstep === 5) {}

                for (var i=stepnumber; i<=nextstep; i++){
        		    $('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
        		}

                animateBar(nextstep);
                isStepValid = true;
                return true;
            } else {
                alert("CAMPO OBRIGATÓRIO! Você não preencheu algumas das informações solicitadas. O campo obrigatório será destacado em vermelho. Navegue acima na tela para verificar.");
            }
        } else if (nextstep < stepnumber) {
        	for (i=nextstep; i<=stepnumber; i++){
        		$('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
        	}
            
            animateBar(nextstep);
            return true;
        } 
    };
    var validateAllSteps = function () {
        var isStepValid = true;
        return isStepValid;
    };
    return {
        init: function () {
            initWizard();
            validateCheckRadio();
        }
    };
}();