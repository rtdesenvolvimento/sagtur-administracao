<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    const  URL_SITE_KATATUR = 'https://www.katatur.com.br/';

    const  URL_SITE_SUA_RESERVA_ONLINE = 'https://www.suareservaonline.com.br';

    const  URL_SISTEMA_KATATUR = 'https://sistema.katatur.com.br';

    function __construct()
    {
        parent::__construct();

        define("DEMO", 0);

        $this->load->model('site');
        $this->Settings = $this->site->get_setting();

        $this->Settings->url_site_domain = $this->getUrlSiteDomain();

        if($sma_language = $this->input->cookie('sma_language', TRUE)) {
            $this->config->set_item('language', $sma_language);
            $this->lang->load('sma', $sma_language);
            $this->Settings->user_language = $sma_language;
        } elseif (!empty($this->Settings)) {
            //$this->config->set_item('language', $this->Settings->language);
            //$this->lang->load('sma', $this->Settings->language);
            $this->config->set_item('language', 'portugues');
            $this->lang->load('sma', 'portugues');
            $this->Settings->user_language = $this->Settings->language;
        } else {
            $this->config->set_item('language', 'portugues');
            $this->lang->load('sma', 'portugues');
        }

        if($rtl_support = $this->input->cookie('sma_rtl_support', TRUE)) {
            $this->Settings->user_rtl = $rtl_support;
        } else {
            $this->Settings->user_rtl = $this->Settings->rtl;
        }

        $this->theme = $this->Settings->theme.'/views/';
        if(is_dir(VIEWPATH.$this->Settings->theme.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR)) {
            $this->data['assets'] = base_url() . 'themes/' . $this->Settings->theme . '/assets/';
        } else {
            $this->data['assets'] = base_url() . 'themes/default/assets/';
        }

        $this->data['Settings'] = $this->Settings;
        $this->loggedIn = $this->sma->logged_in();

        if ($this->Settings->status != null) {
            if ($this->Settings->status == 0) {
                $this->loggedIn = false;
                $this->session->set_flashdata('error', lang('access_denied_paid'));//TODO AQUI VAMOS BLOQUEAR O SISTEMA POR FALTA DE PAGAMENTO
            }
        }

        if($this->loggedIn) {
            $this->default_currency = $this->site->getCurrencyByCode($this->Settings->default_currency);
            $this->data['default_currency'] = $this->default_currency;
            $this->Owner = $this->sma->in_group('owner') ? TRUE : NULL;
            $this->data['Owner'] = $this->Owner;
            $this->Customer = $this->sma->in_group('customer') ? TRUE : NULL;
            $this->data['Customer'] = $this->Customer;
            $this->Supplier = $this->sma->in_group('supplier') ? TRUE : NULL;
            $this->data['Supplier'] = $this->Supplier;
            $this->Admin = $this->sma->in_group('admin') ? TRUE : NULL;
            $this->data['Admin'] = $this->Admin;

            if($sd = $this->site->getDateFormat($this->Settings->dateformat)) {
                $dateFormats = array(
                    'js_sdate' => $sd->js,
                    'php_sdate' => $sd->php,
                    'mysq_sdate' => $sd->sql,
                    'js_ldate' => $sd->js . ' hh:ii',
                    'php_ldate' => $sd->php . ' H:i',
                    'mysql_ldate' => $sd->sql . ' %H:%i'
                    );
            } else {
                $dateFormats = array(
                    'js_sdate' => 'mm-dd-yyyy',
                    'php_sdate' => 'm-d-Y',
                    'mysq_sdate' => '%m-%d-%Y',
                    'js_ldate' => 'mm-dd-yyyy hh:ii:ss',
                    'php_ldate' => 'm-d-Y H:i:s',
                    'mysql_ldate' => '%m-%d-%Y %T'
                    );
            }

            if(file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'Pos.php')) {
                define("POS", 1);
            } else {
                define("POS", 0);
            }

            if(!$this->Owner && !$this->Admin) {
                $gp = $this->site->checkPermissions();

                if ($this->session->userdata('group_id') == 5 || $this->session->userdata('group_id') == 6 || $this->session->userdata('group_id') == 7) {//check-in
                    $gp[0]['checkins-index'] = '1';
                }

                $this->GP = $gp[0];
                $this->data['GP'] = $gp[0];

            } else {
                $this->data['GP'] = NULL;
            }

            $this->dateFormats = $dateFormats;
            $this->data['dateFormats'] = $dateFormats;
            $this->load->language('calendar');
            //$this->default_currency = $this->Settings->currency_code;
            //$this->data['default_currency'] = $this->default_currency;
            $this->m = strtolower($this->router->fetch_class());
            $this->v = strtolower($this->router->fetch_method());
            $this->data['m']= $this->m;
            $this->data['v'] = $this->v;
            $this->data['dt_lang'] = json_encode(lang('datatables_lang'));
            $this->data['dp_lang'] = json_encode(array('days' => array(lang('cal_sunday'), lang('cal_monday'), lang('cal_tuesday'), lang('cal_wednesday'), lang('cal_thursday'), lang('cal_friday'), lang('cal_saturday'), lang('cal_sunday')), 'daysShort' => array(lang('cal_sun'), lang('cal_mon'), lang('cal_tue'), lang('cal_wed'), lang('cal_thu'), lang('cal_fri'), lang('cal_sat'), lang('cal_sun')), 'daysMin' => array(lang('cal_su'), lang('cal_mo'), lang('cal_tu'), lang('cal_we'), lang('cal_th'), lang('cal_fr'), lang('cal_sa'), lang('cal_su')), 'months' => array(lang('cal_january'), lang('cal_february'), lang('cal_march'), lang('cal_april'), lang('cal_may'), lang('cal_june'), lang('cal_july'), lang('cal_august'), lang('cal_september'), lang('cal_october'), lang('cal_november'), lang('cal_december')), 'monthsShort' => array(lang('cal_jan'), lang('cal_feb'), lang('cal_mar'), lang('cal_apr'), lang('cal_may'), lang('cal_jun'), lang('cal_jul'), lang('cal_aug'), lang('cal_sep'), lang('cal_oct'), lang('cal_nov'), lang('cal_dec')), 'today' => lang('today'), 'suffix' => array(), 'meridiem' => array()));
        } else {

            //padrao pt-br
            $dateFormats = array(
                'js_sdate' => 'dd/mm/yyyy',
                'php_sdate' => 'd/m/Y',
                'mysq_sdate' => '%d/%m/%Y',
                'js_ldate' => 'dd/mm/yyyy hh:ii',
                'php_ldate' => 'd/m/Y H:i',
                'mysql_ldate' => '%d/%m/%Y %H:%i'
            );

            $this->dateFormats = $dateFormats;
            $this->data['dateFormats'] = $dateFormats;
        }

    }

    private function getUrlSiteDomain() {

        $url_site_domain = MY_Controller::URL_SITE_SUA_RESERVA_ONLINE.'/'.$this->session->userdata('cnpjempresa');

        if ($this->Settings->own_domain && $this->Settings->url_site) {
            $url_site_domain = $this->Settings->url_site;//TODO SITE PROPRIO
        } elseif ($this->Settings->own_domain) {
            $url_site_domain = 'https://'. $this->session->userdata('cnpjempresa') .'.suareservaonline.com.br';//TODO SUBDOMINIO
        } else if ($this->Settings->url_site == MY_Controller::URL_SITE_KATATUR) {
            $url_site_domain = MY_Controller::URL_SISTEMA_KATATUR;
        }

        return $url_site_domain;
    }

    function page_construct($page, $meta = array(), $data = array()) {

        $meta['message']    = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error']      = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        $meta['warning']    = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        $meta['info']       = $this->site->getNotifications();
				
        $meta['events']     = $this->site->getUpcomingEvents();
        $meta['ip_address'] = $this->input->ip_address();

        $meta['Owner'] = $data['Owner'];
        $meta['Admin'] = $data['Admin'];
        $meta['Supplier'] = $data['Supplier'];
        $meta['Customer'] = $data['Customer'];
        $meta['Settings'] = $data['Settings'];
        $meta['dateFormats'] = $data['dateFormats'];
        $meta['assets'] = $data['assets'];
        $meta['GP'] = $data['GP'];

        //$meta['qty_alert_num'] = $this->site->get_total_qty_alerts();
        //$meta['exp_alert_num'] = $this->site->get_expiring_qty_alerts();

        $meta['qty_alert_sale_cancel']      = $this->site->get_sales_canceleds_qty_alerts();
        $meta['qty_alert_overdue_bills']    = $this->site->get_overdue_bills_qty_alerts();

        $this->load->view($this->theme . 'header', $meta);
        $this->load->view($this->theme . $page, $data);
        $this->load->view($this->theme . 'footer');
    }

}
