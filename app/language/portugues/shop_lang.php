<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['menus'] = 'Itens do Menu';

$lang['new_page'] = 'Abrir em Nova Aba';
$lang['adicionar_menu'] = 'Adicionar Novo Item de Menu';
$lang['editar_menu'] = 'Editar Item de Menu';
$lang['url_page'] = 'URL';
$lang['type'] = 'Tipo';
$lang['type_menu'] = 'Tipo';
$lang['menu_adicionado_com_sucesso'] = 'Item de Menu Adicionado Com Sucesso';
$lang['menu_pai'] = 'Menu Pai / Superior';
$lang['name_menu_pai'] = 'Menu Superior';
$lang['menu_editado_com_sucesso'] = 'Menu Item editado com Sucesso';
$lang['menu_deletado_com_sucesso'] = 'Menu Item deletado com Sucesso';
$lang['menu_sendo_utilizada'] = 'Menu Item sendo utilizado com Menu Superior de outro Menu e não pode ser excluído.';
$lang['menu_superior'] = 'Menu Superior';
$lang['shop_settings_menus'] = 'Menus';
$lang['menu_superior_url'] = 'Menu Superior + URL';

$lang['pages'] = 'Páginas de Conteúdo';
$lang['adicionar_page'] = 'Adicionar Página de Conteúdo';
$lang['titulo'] = 'Título';
$lang['data_publicacao'] = 'Data da Publicação';
$lang['data_despublicacao'] = 'Data de Despublicação';
$lang['conteudo'] = 'Conteúdo da Página';
$lang['page_editado_com_sucesso'] = 'Página de Conteúdo Editada com Sucesso';
$lang['menu'] = 'Menu';
$lang['page_adicionada_com_sucesso'] = 'Página Adicionada Com Sucesso';
$lang['page_deletado_com_sucesso'] = 'Página Deletada Com Sucesso';

$lang['photo_blog'] = 'Foto Destaque';
$lang['is_rotate'] = 'Exibir pacotes em rotação no site';
$lang['max_packages_line'] = 'Número máximo de pacotes exibidos por (categoria ou Data)';
$lang['arredondar_bordas_img'] = 'Arredondar bordas das imagens';
$lang['nao_distorcer_img'] = 'Não distorcer imagens no site';
$lang['atividades_recomendadas']  = 'Exibir Atividades Recomendadas';