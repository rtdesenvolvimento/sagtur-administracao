<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['dataRetiradaPrevista']   = 'Data Retirada';
$lang['horaRetiradaPrevista']   = 'Horário';
$lang['horaDevolucaoPrevista']  = 'Horário';
$lang['dataDevolucaoPrevista']  = 'Data Devolução';
$lang['veiculo']    = 'Veículo';
$lang['cor']    = 'Cor';
$lang['placa']  = 'Placa';
$lang['ano']    = 'Ano';
$lang['add_location']   = "Adicionar Locação de Veículo";
$lang['data_location']  = "Dados da Locação de Veículo";

$lang['customer'] = 'Locatário';
$lang['desconto'] = 'Desconto';

$lang['local_retirada'] = "Local de Retirada";
$lang['local_devolucao'] = 'Local de Devolução';

$lang['info_local_retirada'] = "Local de Retirada do Veículo";
$lang['info_local_devolucao'] = "Local de Devolução do Veículo";

$lang['tipo_contrato']  = "Tipo de Locação";
$lang['tipo_franquia']  = "Franquia";
$lang['qtdDias']   = 'Qtd Dias';
$lang['qtdHoras']   = "Qtd Horas";
$lang['valor_aluguel'] = "Valor do Aluguel";

$lang['previsao_pagamento'] = 'Data do Primeiro Vencimento';
$lang['valor_vencimento']   = 'Valor';
$lang['tipo_cobranca'] = 'Tipo de Cobrança';
$lang['condicao_pagamento'] = 'Condição de Pagamento';

$lang['valor_franquia'] = "Valor da Franquia";
$lang['valor_excedente'] = "Excedente";
$lang['info_faturamento'] = "Informações do Faturamento da Locação";

$lang['local_retirada_digitada']  = "Informe o Local de Retirada do Veículo";
$lang['local_devolucao_digitada']  = "Informe o Local de Devolução do Veículo";
$lang['ver_map'] = "Ver Endreço no Mapa";


$lang['customer'] = 'Locatário';
$lang['grupo']  = 'Grupo';
$lang['categoria']  = 'Categoria';
$lang['locations']  = "Locações de Veículo";
$lang['data_retirada'] = "Data Retirada";
$lang['data_devolucao'] = 'Data Devolução';
$lang['location_reminders'] = "Agenda e Lembretes";

$lang['data_retidada_de'] = 'Data Retirada De';
$lang['data_retirada_ate'] = 'Até';
$lang['hora_retidada_de'] = 'Hora';
$lang['hora_retirada_ate'] = 'Até';


$lang['data_devolucao_de'] = 'Data Devolução De';
$lang['data_devolucao_ate'] = 'Até';
$lang['hora_devolucao_de'] = 'Hora';
$lang['hora_devolucao_ate'] = 'Até';
$lang['contrato']   = 'Contrato de Locação';
$lang['tipo']  = 'Tipo';
$lang['tipo_veiculo'] = 'Tipo Veículo';
$lang['status_disponibilidade'] = 'Status';
$lang['situacao'] = 'Situação';
$lang['edit_location'] = "Editar Locação";
$lang['download_voucher_location'] = "Baixar Voucher Locação";
$lang['historico_parcela'] = 'Ver Parcela/Adicionar Pagamento';
$lang['cancel_location'] = 'Cancelar Locação';
$lang['add_pickup_vehicle'] = 'Adicionar Retirada do Veículo';
$lang['add_return_vehicle'] = 'Adicionar Devolução do Veículo';
$lang['lease_canceled_successfully'] = 'Locação Cancelada Com Sucesso!';
$lang['sale_already_canceled'] = 'A Locação já encontra-se cancelada.';
$lang['sale_cannot_be_canceled_with_receipts'] = 'A Venda não pode ser cancelada pois já possui recebimentos. <br/>Para cancelar a venda é necessário estornar os recebimentos do cliente e tentar novamente!';
$lang['location_added'] = 'Nova Locação Adicionada Com Sucessso.';
$lang['dataRetiradaRealizada'] = "Data Retirada";
$lang['horaRetidadaRealizada'] = 'Hora';
$lang['dataPrevista'] = 'Data Prevista';
$lang['tanque'] = 'Tanque';
$lang['km_retirada'] = 'Km na Saída';
$lang['diarias'] = 'Diarias';
$lang['falta_pagar'] = 'Falta Pagar';
$lang['horas'] = 'Horas';
$lang['resumo_financeiro'] = 'Resumo Financeiro da Locação';
$lang['dados_retirada'] = 'Dados para Retirada do Veículo';
$lang['pickup_vehicle_added'] = 'Retirada do Veículo Adicinada com Sucesso.';
$lang['marca'] = 'Marca';
$lang['modelo'] = 'Modelo';
$lang['vehicles'] = 'Veiculos';
$lang['add_vehicle'] = "Adicionar Veículo";
$lang['edit_vehicle'] = 'Editar Veículo';
$lang['tipo_veiculo'] = 'Tipo Veículo';
$lang['categoria_veiculo'] = 'Categoria Veículo';
$lang['devolucao'] = 'Devolução';
$lang['data_availability_searh'] = 'Agenda de Disponíbilidade';
$lang['data_availability_searh_day'] = 'Conulta Diária';
$lang['availability_searh_by_vehicle'] = 'Consulta Disponibilidade Por Veículo';
$lang['list_by_day'] = 'Lista por Dia';
$lang['select_day'] = 'Selecione o Dia Para Consultar';
$lang['book_now'] = 'Reservar Agora';
$lang['booking_map'] = 'Mapa de Reservas';
$lang['vechicles'] = 'Veiculos';
$lang['municipio'] = 'Município';
$lang['uf'] = 'UF';
$lang['km_atual'] = 'Km Atual';
$lang['cambio_automatico'] = 'Câmbio Automático';
$lang['capacidade_min'] = 'Capacidade Min';
$lang['capacidade_max'] = 'Capacidade Max';
$lang['tipo_combustivel'] = 'Tipo de Combustivel';
$lang['chassi'] = 'Chassi';
$lang['vehicle_gallery'] = 'Galeria de imagens';
$lang['photo'] = 'Imagem Principal';
$lang['vehicle_gallery_images'] = 'Galeria de Fotos';
$lang['vehicle_updated'] = 'Veículo Atualizado Com Sucesso.';
$lang['vehicle_added'] = 'Veículo Adicionado Com Sucesso.';
$lang['availability'] = 'Disponibilidade do Veículo';
$lang['vehicle_details'] = 'Detalhes do Veículo';
$lang['status_veiculo'] = 'Status Veículo';
$lang['hora_saida'] = 'Horário De';
$lang['hora_retorno'] = 'Até';
$lang['data_inicio'] = 'Data De';
$lang['data_final'] = 'Até';

$lang['domingo'] = 'Domingo';
$lang['segunda'] = 'Segunda-feira';
$lang['terca'] = 'Terça-feira';
$lang['quarta'] = 'Quarta-feira';
$lang['quinta'] = 'Quinta-feira';
$lang['sexta'] = 'Sexta-feira';
$lang['sabado'] = 'Sábado';
$lang['datas_pontuais'] = 'Datas Pontuais';
$lang['periodos'] = 'Períodos';
$lang['data_inicio_comercializacao'] = 'Data Início Comercialização';
$lang['data_horario_bloqueado'] = 'Datas e horários bloqueados';


$lang['start_date'] = 'Data De';
$lang['end_date'] = 'Até';

$lang['start_time'] = 'Hora De';
$lang['end_time'] = 'Até';


$lang['add_new_location'] = 'Adicionar Locação';
$lang['location_list'] = 'Lista de Locações';
$lang['availability_panel'] = "Painel de Disponibilidade";
$lang['availability_searh'] = "Consultar Disponibilidade";
$lang['list_vehicle']       = 'Lista de Veículos';
$lang['vehicle_fleet_panel'] = 'Consultar Frota';
