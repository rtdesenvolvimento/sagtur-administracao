<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['P53037'] = 'O token do cartão de crédito é obrigatório.';
$lang['P53060'] = 'O endereço de cobrança é obrigatório.';
$lang['P53064'] = 'O estado do endereço de cobrança é obrigatório.';
$lang['P53062'] = 'A cidade do endereço de cobrança é obrigatória.';
$lang['P53055'] = 'A rua do endereço de preenchimento é obrigatória.';
$lang['P53053'] = 'O código postal do endereço de cobrança é obrigatório.';
$lang['P53057'] = 'o número do endereço de cobrança é obrigatório.';
$lang['P53066'] = 'O país do endereço de cobrança é obrigatório';
$lang['P53047'] = 'A data de nascimento do titular do cartão de crédito é obrigatória.';
$lang['P53048'] = 'Valor inválido da data de nascimento do titular do cartão de crédito: {0}';
$lang['P53150'] = 'Hash do remetente é necessário';

$lang['INITIATED'] = "INICIALIZADO";
$lang['WAITING_PAYMENT'] = "AGUARDANDO PAGAMENTO";
$lang['IN_ANALYSIS'] = "EM ANALISE";
$lang['PAID'] = "PAGA";
$lang['AVAILABLE'] = "DISPONÍVEL";
$lang['IN_DISPUTE'] = "EM DISPUTA";
$lang['REFUNDED'] = "DEVOLVIDA";
$lang['CANCELLED'] = "CANCELADA";