<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Customers
 * Language: Brazilian Portuguese Language File
 * 
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_customer'] 			= "Adicionar Cliente";
$lang['edit_customer'] 			= "Editar Cliente";
$lang['delete_customer'] 		= "Excluir Cliente";
$lang['delete_customers'] 		= "Excluir Clientes";
$lang['customer_added'] 		= "Cliente adicionado com sucesso";
$lang['customers_added']                = "Clientes adicionados com sucesso";
$lang['customer_updated'] 		= "Cliente atualizado com sucesso";
$lang['customer_deleted'] 		= "Cliente excluído com sucesso";
$lang['customers_deleted'] 		= "Os Clientes foram excluídos com sucesso";
$lang['import_by_csv'] 			= "Adicionar Clientes por CSV";
$lang['edit_profile'] 			= "Salvar Usuário";
$lang['delete_user'] 			= "Excluir Usuário";
$lang['no_customer_selected'] 	= "Nenhum Cliente selecionado. Por favor, selecione pelo menos um Cliente. ";
$lang['pw_not_same']                    = "A Confirmação de Senha não corresponde com a Senha";
$lang['user_added']                     = "Acessos do Cliente adicionado com sucesso";
$lang['user_deleted']                   = "Acessos do Cliente excluído com sucesso";
$lang['customer_x_deleted']             = "Falha na Ação! este Cliente não pode ser excluído";
$lang['customer_x_deleted_have_sales']  = "Falha na Ação! Cliente possue Vendas associadas";
$lang['customers_x_deleted_have_sales'] = "Alguns Clientes não podem ser excluídos por possuírem Vendas";
$lang['check_customer_email']           = "Por favor, verifique seu e-mail";
$lang['customer_already_exist']         = "Temos um Cliente já existente com este mesmo e-mail";
$lang['line_no']                        = "Nº da Linha";
$lang['first_6_required']               = "Os primeiros seis (6) campos são obrigatórios e os demais são opcionais.";
$lang['paid_by']                        = "Pago para";
$lang['deposits']                       = "Depositos";
$lang['list_deposits']                  = "Lista de Depositos";
$lang['add_deposit']                    = "Adicionar Deposito";
$lang['edit_deposit']                   = "Editar Viagem";
$lang['delete_deposit']                 = "Excluir Viagem";
$lang['deposit_added']                  = "Viagem adicionado com sucesso";
$lang['deposit_updated']                = "Viagem atualizado com sucesso";
$lang['deposit_deleted']                = "Viagem excluído com sucesso";
$lang['deposits_subheading']            = "Please use the table below to navigate or search the results.";
$lang['deposit_note']                   = "Obs. da Viagem";
$lang['expenses_report']                = "Relatório de Despesas";
$lang['tipo_faixa_etaria']                    = "Tipo";
$lang['observacao']                           = "Observação";
$lang['bloqueio_cliente']                     = "Bloquear Cliente";
$lang['motivo_bloqueio']                      = "Motivo do Bloqueio";
$lang['customers_bloqueados']                 = "Clientes Bloqueados";
$lang['social_name']                          = "Nome Social";
$lang['profession']                           = "Profissão do Cliente";
$lang['document_customer']                    = "Documento";
$lang['searh_customer']                       = "Pesquisar Cliente";
$lang['search_terms_customer']                = "Digite o termo da pesquisa: Nome, CPF, R.G ou Telefones para Consulta";
$lang['input_search']                         = "Termos da Pesquisa";
$lang['cf1'] = 'Documento';
$lang['cf5'] = 'WhatsApp';
$lang['cf3'] = 'Orgão Emissor';
$lang['cf4'] = 'Naturalidade';
$lang['export_to_excel_compras_all'] = "Exportar para Excel - Com Total de Compras";
$lang['nome_responsavel'] = 'Nome do Responsável pela empresa';
$lang['nome_fantasia'] = 'Nome Fantasia';
$lang['data_contratacao'] = 'Data de Contratação';
$lang['data_cancelamento'] = 'Data de Cancelamento';
$lang['responsavel'] = 'Responsável';
$lang['fantasia'] = 'Fantasia';