<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['departments'] = 'Departamentos';
$lang['department'] = 'Departamento';
$lang['add_department'] = 'Adicionar Departamento';
$lang['edit_department'] = 'Editar Departamento';
$lang['delete_department'] = 'Deletar Departamento';


$lang['open_whatsapp']  = 'Abrir no WhatsApp Após Registrar a Captação';
$lang['telefone']  = 'Telefone';

$lang['department_adicionado_com_sucesso'] = 'Departamento Adicionado com Sucesso!';
$lang['department_atualizado_com_sucesso'] = 'Departamento Atualizado com Sucesso!';