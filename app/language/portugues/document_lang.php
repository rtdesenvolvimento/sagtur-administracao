<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['documents']      = "Documentos";
$lang['document']       = "Documento";
$lang['add_document']   = "Novo Documento";

$lang['created_at']     =  "Data da criação";
$lang['doc_name']       = "Nome do documento";
$lang['signatures']     = "Assinaturas";
