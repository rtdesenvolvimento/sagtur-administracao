<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Transfers
 * Language: Brazilian Portuguese Language File
 * 
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_transfer'] 					= "Adicionar Transferência";
$lang['edit_transfer'] 					= "Salvar Transferência";
$lang['delete_transfer'] 				= "Excluir Transferência";
$lang['delete_transfers'] 				= "Excluir Transferências";
$lang['transfer_added'] 				= "Transferência adicionada com sucesso";
$lang['transfer_updated'] 				= "Transferência atualizada com sucesso";
$lang['transfer_deleted'] 				= "Transferência excluída com sucesso";
$lang['transfers_deleted'] 				= "Transferências excluídas com sucesso";
$lang['import_by_csv'] 					= "Adicionar Transferências por CSV";
$lang['ref_no'] 						= "No. de Ref.";
$lang['transfer_details'] 				= "Detalhes de Transferência";
$lang['email_transfer']				 	= "E-mail da Transferência";
$lang['transfer_quantity'] 				= "Quantidade da Transferência";
$lang['please_select_warehouse'] 		= "Por favor, selecione um Viagem";
$lang['please_select_different_warehouse'] = "Por favor, selecione um Viagem diferente";
$lang['to_warehouse'] 					= "Para a Viagem";
$lang['from_warehouse'] 				= "Da Viagem";
$lang['edit_transfer_quantity'] 		= "Salvar Quantidade da Transferência";
$lang['can_not_change_status_of_completed_transfer'] = "Você não pode mudar o status da transferência concluída";
$lang['transfer_by_csv'] 				= "Adicionar Transferência por CSV";
$lang['no_transfer_selected'] 			= "Nenhuma transferência selecionado. Por favor, selecione pelo menos uma transferência.";
$lang['received_by'] 					= "Recebido por";
$lang['users'] 							= "Usuários";
$lang['transferring']                                   = "Transferindo";
$lang['first_2_are_required_other_optional']            = "<strong>Os primeiros dois (2) campos são obrigatórios e os demais são opcionais.</strong>";
$lang['pr_not_found']                                   = "Viagem não encontrado ";
$lang['line_no']                                        = "Nº da Linha";
$lang['transfer_order']                                 = "Pedido de Transferência";
$lang['expenses_report']            = "Despesas Operacionais";