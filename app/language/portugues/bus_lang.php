<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['bus']                            = 'Plantas de Ônibus';
$lang['adicionar_bus'] 					= "Adicionar uma nova planta";
$lang['bus_adicionado_com_sucesso']     = 'Planta adicionada com sucesso';
$lang['habilitado']                     = 'Assento habilitado';
$lang['desabilitado']                   = 'Assento desabilitado';
$lang['edit_assento']                   = 'Editar Assento';

$lang['bus_plans']                      = 'Plantas de Ônibus';
$lang['andares']                        = 'Andares';
$lang['total_assentos']                 = 'Total Assentos';
$lang['automovel_deletado_com_sucesso'] = 'Planta de ônibus deletada com sucesso.';
$lang['editar_bus']                     = "Editar planta";
$lang['bus_editado_com_sucesso']        = "Planta de ônibus atualizada com sucesso";
$lang['plantas']                        = 'Plantas';

$lang['configurar_cobranca_extra'] = 'Cobrança Extra dos Assentos';
$lang['configurar'] = 'Configurar';
$lang['cobranca_extra_assento'] = 'Configuração dos Assentos com Cobrança Extra';
$lang['add_preco_assento_extra'] = 'Tabela de Preço para Cobrança Extra da Assentos';
$lang['edit_preco_assento_extra'] = 'Editar Cobrança Extra de Assentos';
$lang['add_assento_extra'] = 'Adicionar Cobrança Extra de Assentos';
$lang['automovel_id'] = 'Mapa Base';
$lang['cobranca_extra_assento_addicionada_com_sucesso'] = 'Cobrança Extra por Assento Adicionado Com Sucesso';
$lang['cobranca_extra_assento_editado_com_sucesso'] = 'Cobrança Extra por Assento Editada Com Sucesso';
$lang['edit_assento_extra'] = 'Editar';
$lang['cobranca_extra_deletado_com_sucesso'] = 'Cobrança Extra Deletado Com Sucesso';

$lang['cobranca_extra'] = 'Cobrança Extra de Assentos';
$lang['tipo_transporte'] = 'Tipo de Transporte';
$lang['bus_base'] ='Mapa Base';
