<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['install'] = 'Auto Instalação de Sistema';
$lang['add_install'] = 'Instalar';
$lang['nome_responsavel'] = 'Nome do Responsável';
$lang['cnpj'] = 'CNPJ';
$lang['theme_color'] = 'Cor Principal Site';
$lang['database'] = 'Nome do Banco de Dados';
$lang['site_logo'] = 'Logo do Sistema';
$lang['install_success'] = 'Instalação realizada com sucesso!';
$lang['client_exists'] = 'Cliente já possui um sistema instalado. Dados de acesso: ';
$lang['criar_subdominio'] = 'Criar Subdomínio';