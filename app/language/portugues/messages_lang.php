<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['message_groups'] = 'Grupos do WhatsApp';
$lang['add_new_group'] = 'Adicionar novo grupo';
$lang['participants'] = 'Membros';
$lang['poltrona'] = 'Assentos';
$lang['message_group_participants'] = 'Membros do Grupo';
$lang['invitationLink'] = 'Link de Convite do Grupo de WhatsApp';
$lang['participant_added'] = 'Membro adicionado com sucesso';
$lang['participant_removed'] = 'Membro removido com sucesso';
$lang['ddi_required'] = 'O DDI é obrigatório';
$lang['phone_required'] = 'O telefone é obrigatório';
$lang['participant_removed'] = 'Membro removido com sucesso';
$lang['add_new_group'] = 'Adicionar novo grupo';
$lang['add_group'] = 'Adicionar Grupo';
$lang['phone_admin'] = 'Telefone do Administrador do Grupo';
$lang['group_created_successfully'] = 'Grupo criado com sucesso';
$lang['group_already_created'] = 'Grupo já criado';
$lang['group_edited_successfully'] = 'Grupo editado com sucesso';
$lang['edit_group'] = 'Editar Grupo';
$lang['ativo'] = 'Ativo';
$lang['inativo'] = 'Inativo';
$lang['no_data'] = 'Nenhum dado disponível';
$lang['add_participant'] = 'Adicionar Membro';
$lang['delete_participant'] = 'Remover Membro';
$lang['add_admin_successfully'] = 'Administrador adicionado com sucesso';
$lang['add_admin'] = 'Adicionar Administrador';
$lang['add_member'] = 'Adicionar Membro';
$lang['exportar_para_agenda'] = 'Exportar para Agenda Padrão SAGTur';
$lang['participant_admin_added'] = 'Membro adicionado como administrador com sucesso';
$lang['peoples_exported'] = 'Pessoas exportadas com sucesso';
$lang['inserteds'] = 'Inseridos';
$lang['remover_todos'] = 'Remover todos do Grupo de WhatsApp';
$lang['participants_removed'] = 'Membros removido com sucesso';
$lang['peoplesadded'] = 'Pessoas adicionadas!';
$lang['add_all_participants'] = 'Incluir Todos no WhatsApp';
$lang['message_group_not_associated'] = 'Grupo de mensagens não encontrado.';
$lang['mostrar_vendas_canceladas'] = 'Incluir Vendas Canceladas';