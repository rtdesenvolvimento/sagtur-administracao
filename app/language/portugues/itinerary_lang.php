<?php defined('BASEPATH') OR exit('No direct script access allowed');


$lang['order_service']      = 'Ordem de Serviço';
$lang['orders_service']      = 'Ordens de Serviço';

$lang['set_up_itinerary']   = "Montar Ordem de Serviço";

$lang['tour']               = "Atividade";
$lang['tours']              = "Atividades";
$lang['pick_up']            = "Origem";
$lang['dataTour']           = "Data";
$lang['inserted_activities'] = "Serviços Inseridos na OS";
$lang['destiny']            = "Destino";
$lang['hour']               = "Hora";
$lang['add_tours']          = "Adicionar Serviço";
$lang['edit_itinerary']     = "Editar Ordem de Serviço";
$lang['add_itinerary']      = "Adiconar Ordem de Serviço";
$lang['date_itinerary']     = "Data";
$lang['hora_itinerary']     = "Hora";

$lang['operador']           = 'Operador do Serviço';
$lang['fornecedor']         = 'Operador';
$lang['guia']               = "Guia";
$lang['motorista']          = "Motorista";
$lang['veiculo']            = "Veículo";

$lang['itinerary_added']    = 'Ordem de Serviço Adicionada Com Sucesso';
$lang['itinerary_update']   = 'Ordem de Serviço Atualizada com Sucesso';
$lang['tipo_transporte']    = 'Veículo';
$lang['itinerary_details']  = 'Detalhes da OS';
$lang['tours_itinerary']    = 'Itens da OS';
$lang['data_embarque']      = 'Data';
$lang['hora_embarque']      = 'Saída';
$lang['mount_itinerary']    = 'Montar Ordem de Serviço';
$lang['itinerary_deleted']  = 'Ordem de Serviço Deletado com Sucesso';
$lang['delete_itinerary']   = 'Deletar Ordem de Serviço';

$lang['report_itinerary']   = 'Relatório Geral de Ordem de Serviço';
$lang['hourTour']           = 'Hora';
$lang['tipo_trajeto']       = 'Tipo Trajeto';
$lang['retorno_previsto']   = 'Retorno Previsto';
$lang['product']            = 'Serviço';
$lang['executivo']          = 'Executivo';
$lang['report_detail_itinerary'] = 'Relatório Detalhado de Ordem de Serviço';



$lang['portugues'] = 'Português';