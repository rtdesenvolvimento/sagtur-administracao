<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['total_commission'] = 'Total da Comissão';
$lang['header_add_commissions_close'] = 'SELECIONE AS COMISSÕES';
$lang['header_commissions_close']   = 'COMISSÕES INSERIDAS';
$lang['montar_fechamento_comissao'] = 'Fechamento de Comissão';
$lang['montar_fechamento'] = 'Montar o Fechamento de Comissão';
$lang['competencia'] = 'Date de Competência';
$lang['valor_vencimento'] = 'Valor';
$lang['data_vencimento'] = 'Vencimento';
$lang['tipo_cobranca_financeiro'] = 'Tipo de Cobrança';
$lang['condicao_pagamento'] = 'Condição de Pagamento';
$lang['confirmar_fechamento'] = 'Confirma Fechamento Comissão';
$lang['abrir_fechamento_comissao'] = 'Abrir Fechamento de Comissão';
$lang['add_fechamento_comissao'] = 'Adicionar Fechamento de Comissão';
$lang['fechamento_added'] = 'Fechamento Adicionado Com Sucesso';
$lang['dt_competencia'] = 'Data de Competência';
$lang['dt_competencia_de'] = 'Data de Competência De';
$lang['dt_competencia_ate'] = 'Até';
$lang['competencia'] = 'Competência';
$lang['base_value'] = 'Valor Base';
$lang['commission'] = 'Comissão';
$lang['commission_percentage'] = '%';
$lang['financeiro_conta_pagar_adicionada_com_sucesso'] = 'Comissões Adicionada(s) Com Sucesso!';
$lang['header_commissions'] = 'COMISSÕES';
$lang['pendente'] = 'Pendente';
$lang['confirmada'] = 'Confirmada';
$lang['cancelada'] = 'Cancelada';
$lang['info_header_billers'] = 'VENDEDORES';
$lang['edit_fechamento'] = 'Editar Fechamento';
$lang['fechamento_editdd'] = 'Fechamento Editado Com Sucesso';
$lang['edit_fechamento_comissao'] = 'Editar Fechamento de Comissão';
$lang['biller_required'] = 'Selecione pelo menos um Vendedor';
$lang['aberta'] = 'Aberta';
$lang['total_comissao'] = 'Comissão';
$lang['total_geral'] = 'Valor Total';
$lang['payment_reference'] 		= "Ref. de Pagamento";
$lang['paid_by'] 				= "Pago em";
$lang['report_commissions_landscape'] = 'Relatório de Comissão (Paisagem)';
$lang['report_commissions_portrait'] = 'Relatório de Comissão';
$lang['data_venda_de'] = 'Data da Venda De';
$lang['data_venda_ate'] = 'Até';
$lang['data_parcela_de'] = 'Data da Fatura De';
$lang['data_parcela_ate'] = 'Até';
$lang['status_parcela'] = 'Status Fatura';
$lang['liberacao_comissao'] = 'Status de Liberação';
$lang['primeira_parcela_paga']  = 'Filtrar Comisões Com a 1° Parcela da Venda PAGA';
$lang['ultima_parcela_paga']  = 'Filtrar Comissão Com Venda totalmente PAGA';
$lang['tipo_cobranca'] = 'Tipo Cobrança';
$lang['commissions_added'] = 'Comissões Adicionadas Com Sucesso';
$lang['no_commission_selected'] = 'Nenhuma Comissão Selecionada';
$lang['report_commissions'] = 'Relatório de Comissões';
$lang['commission_details'] = 'Ver Detalhes da Comissão';
$lang['insert_commissions'] = 'Inserir Comissões Selecionadas';
$lang['delete_commissions'] = 'Remover Comissões Selecionadas';
$lang['commissions_deleted'] = 'Comissões Removidas Com Sucesso';

$lang['vencida'] = 'Vencida';
$lang['parcial'] = 'Parcial';
$lang['quitada'] = 'Quitada';
$lang['aprovada'] = 'Aprovada Para Pagamento';
$lang['aprovada_parcial'] = 'Aprovada/Parcial Para Pagamento';
$lang['event'] = 'Evento';
$lang['em_revisao'] = 'Comissão Em Revisão';
$lang['pagamento_parcial'] = 'Pagamento Parcial';
$lang['comissao_paga'] = 'Comissão Paga';

$lang['confirmado'] = "Serviços Ativo";
$lang['produto_inativo'] = "Serviços Inativo";
$lang['arquivado']  = "Arquivados";
$lang['cancel_fechamento']  = "Cancelar Fechamento";
$lang['parcelas'] = 'Parcelas';
$lang['edit_commission_item'] = 'Editar Comissão Item';
$lang['motivo_alteracao_comissao'] = 'Motivo da Alteração da Comissão';
$lang['new_commission_percentage'] = 'Novo % Comissão';
$lang['new_commission'] = 'Nova Comissão (R$)';
$lang['comissison_item_edited'] = 'Comissão Item Editada Com Sucesso';
$lang['cancel_commission_item'] = 'Cancelar Comissão Item';
$lang['comissison_item_cancelled'] = 'Comissão Item Cancelada Com Sucesso';
$lang['motivo_cancelamento_comissao'] = 'Motivo do Cancelamento da Comissão';
$lang['status_comission'] = 'Status da Comissão';

$lang['historico_commission_item'] = 'Ver Histórico de Comissão';
$lang['payment_commission_biller'] = 'Pagamento de Comissão Vendedor';
$lang['payment_commissions_billers'] = 'Pagamento de Comissões Vendedores';
$lang['vencimento'] = "Vencimento";

$lang['data_vencimento_de'] = "Vencimento de";
$lang['data_vencimento_ate'] = "Até";
$lang['valor'] = "Valor Pagar";
$lang['pago'] = "Valor Pago";
$lang['pagar']  = "Pagar";

$lang['baixa_fatura_selecao'] = 'Baixas Faturas Por Seleção';

$lang['historico_faturas'] = "Ver Dados da Parcela";
$lang['ver_pagamentos'] = "Ver Pagamentos";
$lang['edit_fatura'] = "Editar Parcela";
$lang['add_pagamento'] = "Adicionar Pagamento";
$lang['cancelar_fatura'] = "Cancelar Fatura";
$lang['sent']  = 'Recebido';
$lang['list_commissions_approved'] = 'Lista de Comissões Aprovadas (Faturadas)';
$lang['excluir_commission_fechamento_item'] = 'Excluir Comissão do Fechamento';
$lang['item_comissao_fechamento_deleted'] = 'Item de Comissão do Fechamento Excluído Com Sucesso!';
$lang['item_comissao_fechamento_not_deleted'] = 'Item de Comissão do Fechamento Não Pode Ser Excluído! Status inválido para exclusão.';
$lang['data_pagamento_de'] = 'Data do Pagamento De';
$lang['data_pagamento_ate'] = 'Até';
$lang['billing_data'] = "Dados de Pagamento do Vendedor";
$lang['billers_select'] = 'Selecione o Vendedor Para Fechamento';
$lang['status'] = 'Situação';
$lang['export_commissions_to_excel'] = 'Exportar Comissões Para Excel';
$lang['export_commissions_approved_to_excel'] = 'Exportar Comissões Aprovadas Para Excel';
$lang['sale_id'] = 'ID Venda';