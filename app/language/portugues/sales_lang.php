<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Sales
 * Language: Brazilian Portuguese Language File
 * 
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_sale'] 				= "Adicionar Venda";
$lang['edit_sale'] 				= "Editar Venda";
$lang['delete_sale'] 			= "Excluir Venda";
$lang['delete_sales'] 			= "Excluir Vendas";
$lang['archived_sales']         = "Arquivar Vendas";
$lang['sales_archived']         = "Vendas Arquivadas";
$lang['sale_added'] 			= "Venda adicionada com sucesso";
$lang['sale_updated'] 			= "Venda atualizada com sucesso";
$lang['sale_deleted'] 			= "Venda excluída com sucesso";
$lang['sales_deleted'] 			= "Vendas excluídas com sucesso";
$lang['incorrect_gift_card'] 	= "O Nº da Nº da Carta de Crédito está incorreto. Por favor, verifique novamente. ";
$lang['gift_card_not_for_customer'] = "O Nº deste Vale-Despesas não é para este Passageiro. Por favor, verifique novamente. ";
$lang['view_payments'] 			= "Ver Pagamentos";
$lang['add_payment'] 			= "Adicionar Pagamento";
$lang['add_payment_mesclar'] 	= "Fechamento do Passageiro";
$lang['add_delivery'] 			= "Adicionar Entrega";
$lang['email_sale'] 			= "Enviar E-mail da Venda";
$lang['return_sale'] 			= "Devolução";
$lang['sale_status'] 			= "Status";
$lang['payment_term'] 			= "Prazo de Pagamento";
$lang['payment_term_tip'] 		= "Digite os dias (inteiro) apenas";
$lang['sale_note'] 				= "Nota da Venda";
$lang['staff_note'] 			= "Observações do Adm.";
$lang['serial_no'] 				= "Nº Serial";
$lang['product_option'] 		= "Opção da Viagem";
$lang['poltrona'] 		        = "Poltrona";
$lang['product_serial'] 		= "Nº Serial da Viagem";
$lang['list_sale'] 				= "Lista de Venda";
$lang['do_reference_no'] 		= "Nº da Ref. de Entrega";
$lang['sale_reference_no'] 		= "Nº da Ref. de Venda";
$lang['edit_delivery'] 			= "Salvar Entrega";
$lang['delete_delivery'] 		= "Excluir Entrega";
$lang['delete_deliveries'] 		= "Excluir Entregas";
$lang['delivery_added'] 		= "Entrega adicionada com sucesso";
$lang['delivery_updated'] 		= "Entrega atualizada com sucesso";
$lang['delivery_deleted'] 		= "Entrega excluída com sucesso";
$lang['deliveries_deleted'] 	= "Entregas excluídas com sucesso";
$lang['email_delivery'] 		= "E-mail de Entrega";
$lang['delivery_details'] 		= "Detalhes da Entrega";
$lang['fluxo_caixa'] 		    = "Fluxo de Caixa";
$lang['delivery_order'] 		= "Pedido de Entrega";
$lang['product_details'] 		= "Sobre o Serviço";
$lang['prepared_by'] 			= "Preparado por";
$lang['delivered_by'] 			= "Entregue por";
$lang['received_by'] 			= "Recebido por";
$lang['edit_payment'] 			= "Salvar Pagamento";
$lang['delete_payment'] 		= "Excluir Pagamento";
$lang['delete_payments'] 		= "Excluir Pagamentos";
$lang['payment_added'] 			= "Pagamento Adicionado com Sucesso";
$lang['payment_updated'] 		= "O Pagamento foi atualizado com sucesso";
$lang['payment_deleted'] 		= "Pagamento excluído com sucesso";
$lang['payments_deleted'] 		= "Pagamentos excluídos com sucesso";
$lang['paid_by'] 				= "Pago em";
$lang['send_email'] 			= "Enviar e-mail";
$lang['add_gift_card'] 			= "Adicionar Vale-Despesa";
$lang['edit_gift_card'] 		= "Salvar Vale-Despesa";
$lang['delete_gift_card'] 		= "Excluir Vale-Despesa";
$lang['delete_gift_cards'] 		= "Excluir Vale-Despesa";
$lang['gift_card_added'] 		= "Vale-Despesa adicionado com sucesso";
$lang['gift_card_updated'] 		= "Vale-Despesa atualizado com sucesso";
$lang['gift_card_deleted'] 		= "Vale-Despesa excluído com sucesso";
$lang['gift_cards_deleted'] 	= "Vale-Despesa excluído com sucesso";
$lang['card_no'] 				= "Nº da Carta de Crédito";
$lang['balance'] 				= "Pagar";
$lang['value'] 					= "Valor";
$lang['new_gift_card'] 			= "Adicionar Vale-Despesa";
$lang['sell_gift_card'] 		= "Usar um Vale-Despesa";
$lang['view_sales_details'] 	= "Detalhes da Venda";
$lang['sale_no'] 				= "Nº da Venda";
$lang['payment_reference'] 		= "Ref. de Pagamento";
$lang['quantity_out_of_stock_for_%s'] = "A quantidade está fora de estoque em %s";
$lang['no_sale_selected'] 		= "Nenhuma Venda selecionada. Por favor, selecione pelo menos uma Venda.";
$lang['you_will_loss_sale_data'] = "Você irá perder os dados da Venda atual. Você deseja continuar? ";
$lang['sale_status_x_competed'] = "O status da Venda está pendente, você pode adicionar à devoluções somente as Vendas concluídas.";
$lang['unselect_customer'] 		= "De-selecionar Passageiro";
$lang['payment_status_not_paid'] = "Informamos que o pagamento desta Venda não for concluído. Status: <strong>%s</strong>, Pago: <strong>%01.2f</ strong>";
$lang['paid_amount']             = "Já Pago";
$lang['surcharges'] 			= "Sobretaxas";
$lang['return_surcharge'] 		= "Devolver Sobretaxa";
$lang['return_amount'] 			= "Devolver Valor";
$lang['sale_reference'] 		= "Ref. da Venda";
$lang['return_sale_no'] 		= "Nº da Devolução";
$lang['view_return'] 			= "Ver Devolução";
$lang['return_sale_deleted'] 	= "Devolução excluída com sucesso";
$lang['total_before_return'] 	= "Total Antes da Devolução";
$lang['return_amount'] 			= "Valor à Devolver";
$lang['return_items'] 			= "Devolver os itens";
$lang['surcharge']              = "Sobretaxas";
$lang['returned_items'] 		= "Itens Devolvidos";
$lang['seller']                	= "Vendedor";
$lang['users']              	= "Usuários";
$lang['return_note'] 			= "Nota da Devolução";
$lang['return_sale_added'] 		= "Devolução adicionada com sucesso"; 
$lang['return_has_been_added'] 	= "Alguns itens foram devolvidos desta Venda";
$lang['return_surcharge']                   = "Reembolso de Valor Excedente";
$lang['payment_returned']                   = "Pagamento Devolvido";
$lang['payment_received']                   = "Pagamento Recebido";
$lang['payment_note']                       = "Obs. do Pagamento";
$lang['view_return_details']                = "Ver Detalhes da Devolução";
$lang['pay_by_paypal']                      = "Pagar com Paypal";
$lang['pay_by_skrill']                      = "Pagar com Skrill";
$lang['tax_summary']                        = "Resumo da Venda";
$lang['qty']                                = "Qtd./Peso";
$lang['tax_excl']                           = "Valor sem impostos";
$lang['tax_amt']                            = "Valor dos Impostos";
$lang['total_tax_amount']                   = "Total com Impostos";
$lang['tax_invoice']                        = "DADOS DA PASSAGEM";
$lang['sale_x_edited_older_than_3_months']  = "Esta Venda não pode ser editada, pois já tem mais de 12 meses. Você pode editar as Vendas dentro um período de 6 meses.";
$lang['use_award_points']                   = "Usar Pontos Ganhos em Despesas";
$lang['use_staff_award_points']             = "Usar Pontos Ganhos em Vendas";
$lang['use_points']                         = "Usar Pontos";
$lang['award_points_wrong']                 = "Por favor, verifique o campo de entrada de uso dos pontos, o valor está errado.";
$lang['tax_rate_name']                      = "Nome da Alíquota de Imposto";
$lang['auto_added_for_sale_by_csv']         = "Adicionado automaticamente na Venda por csv";
$lang['invoice']                            = "Fatura";
$lang['view_gift_card']                     = "Ver Cupom";
$lang['card_expired']                       = "Este Cumpom já está vencido";
$lang['card_is_used']                       = "Este Cupom já foi usado";
$lang['card_not_used']                      = "Carta de Crédito Válida";
$lang['no_delivery_selected']               = "Nenhum pedido de entrega selecionado. Por favor, selecione pelo menos um pedido.";
$lang['sale_not_found']                     = "Venda não encontrada.";
$lang['x_edit_payment']                     = "O Pagamento não pode ser Editado.";
$lang['transaction_id']                     = "Cód. da Transação";
$lang['return_quantity']                    = "Quantia Devolvida";
$lang['return_tip']                         = "Por favor edite a quantidade que retornou abaixo. Você pode remover o item ou ajustar a quantidade que retornou a zero se não está foi devolvido";
$lang['sale_has_returned']                  = "Vendas com Entrada de Devolução não pode ser editada";
$lang['sale_already_returned']              = "Venda já possui uma Entrada de Devolução";
$lang['expenses_report']            	    = "Despesas Operacionais";


#Modulo de Vendas

#Modulo de Vendas -> SubModulo -> Pacotes
$lang['header.label.venda.pacotes.servicos.turisticos'] = "Venda de pacotes e serviços turísticos";

$lang['buttom.label.adicionar.formulario.pacote.proprio'] = "Pacotes / Passeios Cadastrados";
$lang['buttom.label.adicionar.formulario.pacote.operadora'] = "Adicionar Manualmente";

$lang['button.label.adiconar.pacote.operadora'] = "Adicionar Serviço Turístico";
$lang['button.label.adiconar.pacote.proprio'] = "Adicionar";

$lang['nomePacote'] = "Nome do Serviço";
$lang['emissaoPacote'] = "Data de Emissão";
$lang['transportePacote'] = "Transporte";
$lang['tipoDestino'] = "Destino";
$lang['reservaPacote'] = "Reserva";
$lang['dateCheckinPacote'] = "Daída";
$lang['dateCheckoutPacote'] = "Volta";
$lang['supplierPacote'] = "Fornecedor";
$lang['pacote'] = "Pacote";
$lang['data_saida_pacote'] = "Data de saída";
$lang['nVagas'] = "Vagas disponível";
$lang['nReservas'] = "Reservado";

#Modulo de Vendas-> SubModulo ->Cadastrar Novo Cliente
$lang['header.label.adicionar.passageiro'] = "Verifique as informações do passageiro";
$lang['tipoContato'] = "Tipo de Contato";

#Modulo de Vendas-> SubModulo->Cadastro de Taxas, Tarifas do Passageiro
$lang['header.label.tarifa'] =  "Taxas e Tarifas";
$lang['currencieCustomer'] = "Moeda";
$lang['tarifaCurrencieCustomer'] = "Tarifa";
$lang['cambioCustomer'] = "Câmbio";
$lang['tarifaRealCustomer'] = "Tarifa";
$lang['taxaFornecedorCustomer'] = "Taxa Fornecedor";

$lang['header.label.taxas.bagagem.reserva.assento'] = "Taxas de Bagagem e Reserva Assento";
$lang['taxaBagagemCustomer'] = "(+) Taxa Bagagem";
$lang['reservaAssentoCustomer'] = "(+) Taxa Reserva Assento";

$lang['header.label.informacoes.bilhete'] = "Informações do Bilhete Aéreo";
$lang['numeroBilheteCustomer'] = "Número do bilhete";
$lang['tipoPassageiroCustomer'] = "Tipo Passageiro";
$lang['ravCustomer']= "(+) DU / RAV";
$lang['remarcacaoCustomer'] = "Remarcação";

$lang['header.label.taxas.adicionais'] = "Taxas Adicionais";
$lang['taxasAdicionaisDescontoCustomer'] = "(-) Desconto";
$lang['taxasAdicionaisCartaoCustomer'] = "(+) Taxas adic. (Fee)";

$lang['vlPagamentoFornecedorCustomer'] = "Valor";
$lang['cambioFornecedorCustomer'] = "Câmbio";
$lang['vlPagamentoFornecedorRealCustomer'] = "Valor";
$lang['condicaoPagamentoFornecedorCustomer'] = "Forma de Pagamento";
$lang['bandeiraCartaoFornecedorCustomer'] = "Bandeira Cartão";
$lang['nParcelasCustomer'] = "Nº Parcelas";

$lang['header.label.reembolso'] = "Reembolso";
$lang['reembolsoTarifaCustomer'] = "(-) Tarifa/Taxa Emb";
$lang['reembolsoMultaCustomer'] = "(+) Multa";
$lang['reembolsoTaxaReembolsoCustomer'] = "(+) Taxa Reembolso";

$lang['sale_descricao_servico'] = "Descrição dos serviços";
$lang['sale_obsrevacao_item'] = "Observações";


#Modulo de Vendas-> SubModulo->Cadastro de Taxas, Tarifas do Fornecedor

$lang['header.label.valores.venda'] = "Valores da Venda";
$lang['tarifaCurrencieSupplier'] = "Tarifa";
$lang['currencieSupplier'] = "Moeda Fornecedor";
$lang['slCambioSupplier'] = "Câmbio";
$lang['tarifaRealSupplier'] = "Tarifa";
$lang['taxaSupplier'] = "Taxa Fornecedor";

$lang['header.label.desconto'] = "Desconto";
$lang['descontoPercentualSupplier'] = "Porcentagem (%)";
$lang['descontoAbsolutoSupplier'] = "(-) Valor Desconto";

$lang['header.label.comissao.incentivo'] = "Comissão e Incentivo";
$lang['comissaoPercentualSupplier'] = "Comissão (%)";
$lang['comissaoAbsolutaSupplier'] = "(-) Valor Comissão (DU / RAV)";
$lang['incentivoPercentualSupplier'] = "Incentivo (%)";
$lang['incentivoAbsolutoSupplier'] = "(-) Valor Incentivo";

$lang['header.label.taxas.adicionais'] = "Taxas Adicionais";
$lang['outrasTaxasSupplier'] = "(+) Outras Taxas";
$lang['taxasCartaoSupplier'] = "(+) Taxas Cartão";

$lang['header.label.pagamento.cliente.direto.fornecedor'] = "Valor que foi pago Direto ao Fornecedor";
$lang['pagamentoFornecedorSupplier'] = "Total de pagamento ao fornecedor";

$lang['header.label.reembolso'] = "Reembolso";
$lang['reembolsoTarifaEmbarqueSupplier'] = "(-) Tarifa/Tx Embarque";
$lang['reembolsoMultaSupplier'] = "(+) Multa";
$lang['reembolsoIncentivoComissaoSupplier'] = "(+) Inc/Com/Out.Txas";
$lang['reembolsoTaxasAdicionaisSupplier'] = "(+) Taxa Adicional";

$lang['header.label.comissionamento.consultor'] = "Comissão & Receita Final da Agência";
$lang['comissaoConsultorPercentualSupplier'] = "(-) Comissão Vendedor(%)";
$lang['comissaoConsultorAbsolutoSupplier'] = "(-) Vlr.Comisssão do Vendedor";
$lang['header.label.resultado.venda'] = "Resultado da Venda";
$lang['service'] = "Serviço";
$lang['tipo_cobranca'] = "Tipo de cobrança";
$lang['valor_vencimento'] = "Valor do Vencimento";
$lang['previsao_pagamento'] = "Primeiro vencimento";
$lang['tipo_hospedagem']                      = 'Tipo de Hospedagem';
$lang['servicos_adicionais']                  = 'Serviços Adicionais';
$lang['local_embarque']                       = 'Local de Embarque';
$lang['onibus']                               = 'Tipo de Transporte';
$lang['companhiaPassagemAerea']               = "Cia.";
$lang['localizadorPassagemAerea']             = "Localizador";
$lang['emissaoPassagemAerea']                 = "Data de Emissão";
$lang['tipoDestinoPassagemAerea']             = "Destino";
$lang['supplierPassagemAerea']                = "Fornecedor";
$lang['header.label.itinerario']              = "Itinerário";
$lang['origemPassagemAerea']                  = "Origem";
$lang['destinoPassagemAerea']                 = "Destino";
$lang['dataSaidaPassagemAerea']               = "Fornecedor";
$lang['dataSaidaPassagemAerea']               = "Saída";
$lang['dataChegadaPassagemAerea']             = "Chegada";
$lang['horaChegadaPassagemAerea']             = "Hora";
$lang['horaSaidaPassagemAerea']               = "Hora";
$lang['vooPassagemAerea']                     = "Voo";
$lang['numeroBilete']                         = "Número do Bilhete";
$lang['assento']                              = "Assento";
$lang['bagagem']                              = "Bagagem";
$lang['origemPacote']                         = "Origem";
$lang['destinoPacote']                        = "Destino";
$lang['hotel']                                = "Hotel";
$lang['emissaoHospedagem']                    = "Data de Emissão";
$lang['dateCheckinHospedagem']                = "Check-IN";
$lang['dateCheckoutHospedagem']               = "Check-OUT";
$lang['reservaHospedagem']                    = "Nº Reserva";
$lang['acomodacaoHospedagem']                 = "Acomodação";
$lang['categoriaAcomodacaoHospedagem']        = "Categoria";
$lang['acomodacaoRegimeCategoria']            = "Regime";
$lang['supplierHospedagem']                   = "Fornecedor";
$lang['horaCheckinHospedagem']                = "Entrada à partir das";
$lang['horaCheckoutHospedagem']               = "Saída até";

$lang['header.label.transfer.agendar.in']     = "Agendar IN";
$lang['header.label.transfer.agendar.out']    = "Agendar OUT";

$lang['origemTransferIn']                     = "Local da Origem";
$lang['destinoTransferIn']                    = "Destino do Destino";
$lang['reservaTransferIn']                    = "Referência Origem";
$lang['dataEmissaoTransferIn']                = "Data da Emissão";
$lang['dataChegadaTransferIn']                = "Data da Chegada";
$lang['horaChegadaTransferIn']                = "Hora da Chegada";

$lang['destinoTransferOut']                   = "Destino";
$lang['origemTransferOut']                    = "Origem";
$lang['reservaTransferOut']                   = "Referência Destino";
$lang['dataEmissaoTransferOut']               = "Data da Emissão";
$lang['dataSaidaTransferOut']                 = "Data da Saída";
$lang['horaSaidaTransferOut']                 = "Hora da Saída";

$lang['dataPartidaTransferOut']               = "Data partida / voo / bus";
$lang['horaPartidaTransferOut']               = "Hora partida";
$lang['referenciaVooBusTransferOut']          = "Voo / bus";

$lang['hotelManual']                          = "Cadastro Manual do Hotel";
$lang['telefoneHotel']                        = "Telefone";

$lang['emissao']                              = "Emissão";
$lang['hotel_consulta']                       = "Buscar Hotel já Cadastrado";

$lang['adicionar_hospedagem']                 = "Adicionar Hospedagem";
$lang['enderecoHotel']                        = "Endereço";
$lang['operadora']                            = "Operadora";
$lang['fornecedorReceptivo']                  = "Fornecedor / Receptivo";
$lang['telefoneReceptivo']                    = "Telefone";
$lang['enderecoReceptivo']                    = "Endereço";
$lang['buscarReceptivo']                      = "Buscar Fornecedor / Receptivo já cadastrado";
$lang['consolidadora']                        = "Consolidadora";

$lang['apresentacaoPassagem']                 = "Cliente deverá se apresentação";
$lang['dataApresentacao']                     = "Data";
$lang['horaApresentacao']                     = "Hora";

$lang['button.label.adiconar.aereo']          = "Adicionar Passagem Aérea";
$lang['numeroBilhete']                        = "Número do Bilhete";
$lang['supplierTransfer']                     = "Fornecedor/Receptivo";
$lang['tipo']                                 = "Tipo";
$lang['button.label.adiconar.transfer']       = "Adicionar Transfer";
$lang['enviar_orcamento']                     = "Salvar Cotação";
$lang['pagamente']                            = "Nome do Pagador";
$lang['editar_faturar']                       = "Editar Financeiro";
$lang['apenas_editar']                        = "Editar Informações";
$lang['imprimir_reserva']                     = "Imprimir Reserva";
$lang['data_emissao']                         = "Data de Emissão";
$lang['venda_cancelada']                      = "Venda Cancelada Com Sucesso!";
$lang['cancelar_sale']                        = "Cancelar Venda";
$lang['archived_sale']                        = "Arquivar Venda";
$lang['status_montando_pacote']               = "Em Digitação (não lista na venda)";
$lang['status_confirmado_para_venda']         = "Disponível para venda";
$lang['status_viagem_executada']              = "Viagem já realizada";
$lang['status_viagem_cancelada']              = "Viagem cancelada";
$lang['ano'] = "Ano";
$lang['mes'] = "Mês";
$lang['sales_canceled'] = "Vendas Canceladas";
$lang['product_voucher_custom_text']            = "Informações Importantes! Fique Atento!";
$lang['this_sale_does_not_belong']              = "Você não tem permissão para visualizar esta venda. Ela pertence a outro vendedor.";
$lang['opcional']                               = "Serviços Adicionais";
$lang['situacao']                               = "Situação";
$lang['gerar_itineario']                        = "Gerar Itinerario";
$lang['itinerarios']                            = "Itinerarios";
$lang['produto_com_estoque_negativo']           = "Não foi possível confirmar a venda/reserva deste produto pois ele não possui vagas disponível. Entre em contato com agência para maiores informações.";
$lang['produto_com_estoque_tipo_hospedagem_negativo']  = "Não foi possível confirmar a venda/reserva deste produto pois ele não possui vagas disponível para o tipo de hospedagem. Entre em contato com agência para maiores informações.";
$lang['data_venda_de']                          = " Data da Venda De";
$lang['data_venda_ate']                         = " Até";
$lang['adicionar_novo_room_list']               = "Adicionar Novo RoomingList";
$lang['relatorio_seguradora_de_passageiros']    = "Relatório de Passageiro Seguradora";
$lang['captar_assinatura_passageiros']          = "Assinatura de Todos Passageiros";
$lang['captar_assinatura_passageiros_transporte'] = "Assinatura dos Passageiros";

$lang['captar_assinatura_passageiros_autorizacao_imagem'] = 'Assinatura de Autorização de Imagem';

$lang['confirmado'] = "Serviços Ativo";
$lang['produto_inativo'] = "Serviços Inativo";
$lang['arquivado']  = "Arquivados";

$lang['descontarVaga'] = "Descontar Vaga";
$lang['faixa_etaria'] = "Faixa de Valor";
$lang['valor_hospedagem'] = "Valor da Hospedagem";
$lang['forma_pagamento'] = "Forma de Pagamento";

$lang['lista_espera'] = "Lista de Espera";
$lang['relatorio_por_tipo_hospedagem'] = "Relatório por Tipo de Hospedagem";

$lang['send_message'] = "Abrir Conversa no WhatsApp";
$lang['whatsapp_send'] = "Abrir Conversa com Cliente no WhatsApp";

$lang['event'] = "Evento";
$lang['events_sale'] = "Eventos da Venda";

$lang['meio_divulgacao'] = 'Divulgação';

$lang['faixa_nao_cadastrada_na_venda'] = 'Não identificamos uma faixa de valores configurada para o passageiro: ';

$lang['assento_ja_bloqueado_ocultado'] = 'Este assento já encontra-se ocupado por outro passageiro.';

$lang['cliente_duplicidade_venda'] = 'Identificamos que você já realizou a compra do mesmo pacote/serviço para essa data.<br/> Entre em contato com o Administrador para mais informações! <br/>Mas não se preocupe, já estamos com o registro de sua reserva. Identificamos a duplicidade em nome de: <br/>';

$lang['customer_or_dependent'] = 'Passageiro ou Dependente';
$lang['filters'] = 'Filtros da Consulta Clique Aqui';
$lang['origem'] = 'Origem';
$lang['sale_items'] = 'VENDAS POR ITENS';
$lang['sale_by_items'] = "Vendas Por Itens";
$lang['search_terms_customer'] = ".Digite o termo da pesquisa: Nome, CPF, R.G ou Telefones para Consulta";
$lang['finish'] = 'Finalizar';

$lang['pagador_responsavel'] ='RESPONSÁVEL PAGADOR';
$lang['unarchive_sale'] = 'Desarquivar Venda';
$lang['sales_unarchive'] = "Vendas Desarquivadas";
$lang['unarchive_sales'] = 'Desarquivar Vendas';
$lang['combine_to_pdf'] = 'Mescar Vendas Em Um Único PDF';
$lang['motivo_cancelamento'] = 'Motivo pelo Cancelamento';
$lang['cancel_sale'] = 'Cancelar Venda';
$lang['motivo_cancelamento_venda'] = 'Motivo pelo Cancelamento da Venda';
$lang['gerar_credito'] = 'Gerar Crédito?';
$lang['gerar_reembolso'] = 'Gerar Reembolso?';
$lang['action_credited_sale'] = 'Foi lançado um crédito ou reembolso desta venda, neste caso não é possível reativar.';
$lang['hora_embarque'] = 'Hora';
$lang['referencia_item'] = 'Referência';
$lang['generate_reviews']   = 'Gerar Avaliações e Enviar p/ E-mail';
$lang['sales_ratings']      = 'Avaliações Enviadas com Sucesso!';
$lang['generate_sales_commissions'] = 'Gerar Comissões de Vendas';
$lang['modal_view_fechamento_commissions'] = 'Relatório de Fechamento de Comissões';
$lang['comissao_nao_encontrada_fechamento'] = 'Comissão Não Encontrada Em Nenhum Fechamento.';
$lang['commission'] = 'Comissão';
$lang['base_value'] = 'Valor Base';
$lang['total_commission'] = 'Total da Comissão';
$lang['report_commissions'] = 'Relatório de Comissões';
$lang['montar_fechamento'] = 'Abrir Fechamento Comissão';
$lang['report_commissions_biller'] = 'Relatório de Comissões Vendedor';
$lang['payment_print_note'] = 'Imprimir Comprovante de Pagamento';
$lang['payment_download_payment'] = 'Baixar Comprovante de Pagamento';
$lang['estonar_payment'] = 'Estornar Pagamento';
$lang['add_payment_fatura'] = 'Adicionar Pagamento';
$lang['edit_fatura'] = 'Editar Fatura';
$lang['generate_sale_contract'] = 'Gerar Contrato de Venda';
$lang['download_document'] = 'Baixar Contrato';
$lang['abrir_contrato'] = 'Abrir Contrato';
$lang['to_share_validate'] = 'Histórico do documento';
$lang['to_share'] = 'Compartilhar Documento';
$lang['contract_events'] = 'Eventos do Contrato';
$lang['draft'] = 'Rascunho';
$lang['unsigned'] = 'Aguardando Assinatura';
$lang['pending'] = 'Assinado Parcialmente';
$lang['signed'] = 'Documento Assinado';
$lang['issued'] = 'Emitido';
$lang['sent_signature'] = 'Enviado para Assinatura';
$lang['contract_events'] = 'Eventos do Contrato';
$lang['signatory_removed'] = 'Signatário Removido';
$lang['send_document_to'] = 'Documento Reenviado';
$lang['send_document_pdf'] = 'Documento Assinado Enviado para WhatsApp do Cliente';