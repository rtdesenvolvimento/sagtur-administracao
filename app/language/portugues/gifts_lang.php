<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['gifts']                  = 'Crédito de Clientes';
$lang['gift']                   = 'Crédito de Cliente';
$lang['gift_cards_report']      = 'Relatório de Crédito de Clientes';
$lang['gift_cards'] 			= "Carta de Crédito de Clientes";
$lang['add_gift_card'] 			= "Adicionar Carta de Crédito";
$lang['edit_gift_card'] 		= "Editar Carta de Crédito";
$lang['delete_gift_card'] 		= "Excluir Carta de Crédito";
$lang['delete_gift_cards'] 		= "Excluir Carta de Crédito";
$lang['gift_card_added'] 		= "Carta de Crédito adicionado com sucesso";
$lang['gift_card_updated'] 		= "Carta de Crédito atualizado com sucesso";
$lang['gift_card_deleted'] 		= "Carta de Crédito excluído com sucesso";
$lang['gift_cards_deleted'] 	= "Cartas de Crédito excluído com sucesso";
$lang['card_no'] 				= "Nº da Carta de Crédito";
$lang['balance'] 				= "Disponível";
$lang['usado']                  = 'Utilizado';
$lang['value'] 					= "Crédito De:";
$lang['new_gift_card'] 			= "Adicionar Carta de Crédito";
$lang['sell_gift_card'] 		= "Usar uma Carta de Crédito";
$lang['gift_card']              = 'Carta de Crédito';
$lang['view_gift_card']         = "Carta de Crédito Impressão";
$lang['card_expired']           = "Esta Carta de Crédito já está Vencida";
$lang['cancel_gift_card']       = 'Cancelar Carta de Crédito';
$lang['motivo_cancelamento']    = 'Motivo Cancelamento';
$lang['gift_cancelado_com_sucesso'] = 'Carta de Crédito de Cliente Cancelado com Sucesso';
$lang['card_is_used']           = "Este Crédito já foi usado";
$lang['card_cancel']            = 'Cartão Cancelado';
$lang['add_payment'] 		    = "Adicionar Pagamento";
$lang['valor_vencimento']       = 'Valor Vencimento';
$lang['pago_financeiro']        = "Pago";
$lang['tipo_cobranca_financeiro'] = "Tipo de Cobrança";
$lang['valor_pagamento']        = "Valor do Pagamento";
$lang['forma_pagamento']        = "Forma de Pagamento";
$lang['payment_added'] 			= "Pagamento Adicionado com Sucesso";
$lang['gift_card_balance']      = 'Saldo de Crédito';
$lang['carta_credito_impressao'] = 'Imprimir Carta de Crédito';
$lang['email_gift']             = 'Enviar E-mail da Carta de Crédito';
$lang['paid_by'] 				= "Pago em";
$lang['movimentador']           = "Conta";
$lang['view_payments']          = 'Ver Pagamentos';