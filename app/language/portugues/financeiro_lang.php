<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Sales
 * Language: Brazilian Portuguese Language File
 *
 *
 *
 * Translated by:
 * André Velho
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['pessoa'] = "Pessoa";
$lang['servico'] = "Serviço";
$lang['receita'] = "Receita";
$lang['despesa'] = "Despesa";
$lang['cobranca'] = "Cobrança";
$lang['total'] = "Total";
$lang['pago'] = "Pago";
$lang['pagar'] = "Pagar";
$lang['historico_faturas'] = "Ver Dados da Parcela";
$lang['ver_pagamentos'] = "Ver Pagamentos";
$lang['edit_fatura'] = "Editar Parcela";
$lang['add_pagamento'] = "Adicionar Pagamento";
$lang['cancelar_fatura'] = "Cancelar Fatura";
$lang['tipo_receita'] = "Receita";
$lang['tipo_cobranca'] = "Tipo de Cobrança";
$lang['contas_pagar'] = "Contas a Pagar";
$lang['extrato_financeiro'] = "Extrato Financeiro";

$lang['recebimentos_mensal'] = "RECEBIMENTO MENSAL";
$lang['pagamentos_mensal']  = "PAGAMENTO MENSAL";
$lang['total_mensal'] = "SALDO MENSAL";

$lang['recebimentos_por_filtro'] = "RECEBIMENTO POR FILTRO";
$lang['pagamentos_por_filtro'] = "PAGAMENTO POR FILTRO";
$lang['total_por_filtro'] = "TOTAL POR FILTRO";
$lang['vencimento_de'] = "De";

$lang['confirmado'] = "Serviços Ativo";
$lang['produto_inativo'] = "Serviços Inativo";
$lang['arquivado']  = "Arquivados";

$lang['contas']                                         = "Contas";
$lang['contas_link']                                    = "Contas";
$lang['adicionar_conta']                                = "Adicionar Conta";
$lang['editar_conta']                                   = "Editar Conta";
$lang['deletar_conta']                                  = "Deletar Conta";
$lang['conta_adicionada_com_successo']                  = "Conta adicionada com sucesso";
$lang['conta_atualizada_com_sucesso']                   = "Conta atualizada com sucesso";
$lang['conta_sendo_utilizada']                          = "Esta conta não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['conta_deletada_com_sucesso']                     = "Conta deletada com sucesso";
$lang['conta_cobranca']                                 = "Deletar Conta";
$lang['open']                                           = "Aberto";
$lang['close']                                          = 'Fechado';
$lang['conta_balanco']                                  = "Balanço";

$lang['financeiro'] = "Financeiro";

$lang['informacoes_conta_bancaria'] = "Contas que já tiveram lançamentos financeiro não podem ser excluidas apenas Inativadas";
$lang['ativo']                                = "Ativo";
$lang['inativo']                              = "Inativo";


$lang['tiposcobranca']                        = "Tipo de Cobrança";
$lang['adicionar_tipo_cobranca']              = "Adicionar Tipo de Cobrança";
$lang['editar_tipo_cobranca']                 = "Editar Tipo de Cobrança";
$lang['tipo_cobranca_adicionada_com_successo']= "Tipo de Cobrança adicionada com sucesso";
$lang['tipo_cobranca_atualizada_com_sucesso'] = "Tipo de Cobrança atualizada com sucesso";
$lang['tipo_cobranca_sendo_utilizada']        = "Este tipo de cobrança não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['tipo_cobranca_deletada_com_sucesso']   = "Tipo de Cobrança deletada com sucesso";
$lang['deletar_tipo_cobranca']                = "Deletar Tipo de Cobrança";
$lang['nome_tipo_cobranca']                   = "Nome";
$lang['tipo']                                 = "Tipo";
$lang['faturar_automatico']                   = "Faturar automaticamento";
$lang['conta_destino']                        = "Conta de Destino";
$lang['juno']                                 = "JUNO";

$lang['dinheiro']                             = "Dinheiro";
$lang['boleto']                               = "Boleto bancário";
$lang['cartao']                               = "Cartão de Crédito";
$lang['contravale']                           = "Contra-Vale";
$lang['cheque']                               = "Cheque";
$lang['outros']                               = "Outros Pagamentos";
$lang['carne']                                = "Carnê";
$lang['boleto_cartao']                        = "Boleto e cartão de crédito";
$lang['carne_boleto']                         = "Carnê em boletos";
$lang['carne_cartao']                         = "Carnê com cartão de crédito";
$lang['carne_boleto_cartao']                  = "Carnê Boleto e cartão de crédito";
$lang['credito_loja']                         = "Credito com a loja";

$lang['configurar_mercadopago'] = "MercadoPago";
$lang['configurar_pagseguro'] = "PagSeguro";
$lang['configurar_juno'] = "JUNO";
$lang['configar_taxas'] = "Cadastrar Condições e Taxas";

$lang['informacoes_tipo_cobranca'] = "Tipos de cobrança que já tiveram lançamentos financeiro não podem ser excluidos apenas Inativados";
$lang['info_configuracao_taxas_forma_pagamento'] = "Configurar as Taxas e Habilitar as Condições de Pagamento das Formas de Pagamento do Link de Reservas";

$lang['plano_despesas'] = "Plano de Contas - Despesas";
$lang['plano_receitas'] = "Plano de Contas - Receitas";

$lang['adicionar_plano_receitas'] = "Adicionar Plano de Receita";
$lang['por_grupo'] = "Grupo";
$lang['subgrupo'] = "SubGrupo";
$lang['dre'] = "DRE";
$lang['plano_conta_code'] = "Cód. Plano de Contas";
$lang['receita_pai'] = "Receita Superior";
$lang['editar_receita_plano_conta'] = "Editar Plano de Receita";

$lang['faturarVenda'] = "Faturar Venda ?";
$lang['integracao'] = "Qual operadora de integração de pagamento";

$lang['pagseguro']                                      = "PagSeguro";
$lang['mercadopago'] = "MercadoPago";

$lang['carne_cartao_transparent'] = "Cartão de Crédito - Checkout Transparente";
$lang['carne_cartao_transparent_mercado_pago'] = "Cartão de Crédito - Checkout Transparente";
$lang['carne_cartao_transparent_mercado_pago'] = "Cartão de Crédito - Checkout Transparente";
$lang['link_pagamento'] = "Link de Pagamento";
$lang['pix'] = "PIX";
$lang['loterioca'] = "Pagamento na Loterica";
$lang['configurar_mercadopago'] = "MercadoPago";

$lang['tipo_integracao_geracao'] = "Qual Forma de Pagamanto Gerar na Integração?";
$lang['nao_integrado_pagamento'] = "Não há integração";

$lang['tipo_cobranca_conta'] = "Conta de Destino";

$lang['plano_despesas']                       = "Plano de Contas - Despesas";
$lang['adicionar_despesa']              = "Adicionar Despesa";
$lang['editar_despesa']                 = "Editar Despesa";
$lang['despesa_adicionada_com_successo']= "Despesa adicionada com sucesso";
$lang['despesa_atualizada_com_sucesso'] = "Despesa atualizada com sucesso";
$lang['despesa_sendo_utilizada']        = "Esta despesa não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['despesa_deletada_com_sucesso']   = "Despesa deletada com sucesso";
$lang['deletar_despesa']                = "Deletar Despesa";
$lang['motivo_estorno'] = "Motivo pelo Estorno do Pagamento";

$lang['cobrefacil'] = "Cobre Fácil";
$lang['boleto_pix'] = "Boleto e PIX";
$lang['cartao_credito_link'] = "Link do Cartão de Crédito";
$lang['mensalidade'] = "Mensalidade";
$lang['asaas'] = "Asaas";
$lang['configurar_asaas'] = "Asaas";
$lang['configurar_valepay'] = "ValePay";

$lang['valepay'] = "ValePay";
$lang['cartao_credito_transparent_valepay'] = "Cartão de Crédito";

$lang['ano'] = "Ano";
$lang['mes'] = "Mês";

$lang['data_pagamento_de'] = "Data do Pagamento De";
$lang['data_pagamento_ate'] = "Até";

$lang['automatic_cancellation_sale'] = 'Habilitar o Cancelamento Automatico da Venda';
$lang['numero_dias_cancelamento'] = 'Número em <b>DIAS</b> após o Primeiro Vencimento para o <b>CANCELAMENTO AUTOMÁTICO</b> (Dias Corridos)';
$lang['conta_cancelada_com_sucesso'] = 'Conta Cancelada Com Sucesso';
$lang['card_not_used']                      = "Carta de Crédito Válida";
$lang['card_is_used']           = "Este Crédito já foi usado";
$lang['payment_added'] 			= "Pagamento Adicionado com Sucesso";
$lang['pagamento_estornado_com_sucesso'] = 'Pagamento Estornado com Sucesso';
$lang['note_credito'] = 'Nota';
$lang['credito_cliente'] = 'Lançar Crédito de Cliente';
$lang['reembolso'] = 'Lançar Reembolso';
$lang['expiry'] = 'Validade';
$lang['saldo'] = 'Saldo';
$lang['card_refund_not_used']     = "Reembolso Válido";
$lang['value_credit'] = 'Valor do Crédito';
$lang['value_refund'] = 'Valor do Reembolso';
$lang['card_no_refund'] = 'Número Cartão de Reembolso';

$lang['chave_pix'] = 'Chave PIX';
$lang['titular'] = 'Títular';
$lang['telefone'] = 'Telefone (Comprovante)';
$lang['email'] = 'E-mail';
$lang['configurar_pix'] = 'Configurar PIX?';

$lang['baixa_fatura_selecao'] = 'Baixa Fatura Por Seleção';
$lang['no_fatura_baixa_selecao_selected'] = 'Nenhuma Fatura Selecionada para Baixa';

$lang['add_payment_selection'] = 'Adicionar Pagamento por Seleção';

$lang['valor_pago'] = 'Valor Pago';
$lang['valor_pagamento'] = 'Valor do Pagamento';

$lang['valor_total_vencimento'] = 'Valor Total de Faturas';
$lang['valor_total_pago'] = 'Valor Total Pago';
$lang['valor_total_pagamento'] = 'Valor Total do Pagamento';

$lang['header_baixa_fatura_selecao'] = 'Baixas Faturas Por Seleção';

$lang['tipoCobranca']= 'Tipo de Cobrança';
$lang['valorpago'] = 'Valor Pago';
$lang['valorpagar'] = 'Valor a Pagar';
$lang['dtultimopagamento'] = 'Data do Último Pagamento';
$lang['quantidade_dias'] = 'Dias Atraso';
$lang['dtVencimento'] = 'Data de Vencimento';
$lang['valorfatura'] = 'Valor da Fatura';
$lang['tipooperacao'] = 'Operação';
$lang['categoria_receita'] = 'Categoria Receita';
$lang['categoria_despesa'] = 'Categoria Despesa';

$lang['tipoExibir'] = 'Tipo';
$lang['nf_agendamento'] = 'Agendar Nota Fiscal';