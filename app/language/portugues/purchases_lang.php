<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: Purchases
 * Language: Brazilian Portuguese Language File
 * 
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['add_purchase'] 					= "Adicionar Despesas";
$lang['edit_purchase'] 					= "Editar Despesa";
$lang['delete_purchase'] 				= "Excluir Despesa";
$lang['delete_purchases'] 				= "Excluir Despesas";
$lang['purchase_added'] 				= "Despesa adicionada com sucesso";
$lang['purchase_updated'] 				= "Despesa atualizada com sucesso";
$lang['purchase_deleted'] 				= "Despesa excluída com sucesso";
$lang['purchases_deleted'] 				= "Despesas excluídas com sucesso";
$lang['ref_no'] 						= "Nº de Ref.";
$lang['purchase_details'] 				= "Detalhes da Despesa";
$lang['email_purchase'] 				= "E-mail de Despesa";
$lang['purchase_quantity'] 				= "Quant. de Despesa";
$lang['please_select_warehouse'] 		= "Por favor, selecione a Viagem";
$lang['purchase_by_csv'] 				= "Adicionar Despesa por CSV";
$lang['received'] 						= "Recebido";
$lang['more_options'] 					= "Mais Opções";
$lang['add_standard_product'] 			= "Adicionar Pacote Padrão";
$lang['product_code_is_required'] 		= "O Código da Viagem é obrigatório";
$lang['product_name_is_required'] 		= "O Nome da Viagem é obrigatório";
$lang['product_category_is_required'] 	= "A Categoria da Viagem é obrigatória";
$lang['product_unit_is_required'] 		= "A Unid. é obrigatória";
$lang['product_cost_is_required'] 		= "O Custo da Viagem é obrigatório";
$lang['product_price_is_required'] 		= "O Preço da Viagem é obrigatório";
$lang['ordered'] 						= "Pedidos";
$lang['tax_rate_name'] 					= "Nome da Taxa";
$lang['first_3_are_required_other_optional'] = "<strong>As primeiras três colunas são necessárias e as outras são opcionais</strong>.";
$lang['no_purchase_selected'] 			= "Nenhuma Despesa selecionada. Por favor, selecione pelo menos uma Despesa.";
$lang['view_payments'] 					= "Ver Pagamentos";
$lang['add_payment'] 					= "Adicionar Pagamento";
$lang['add_payment_mesclar'] 	= "Fechamento do Passageiro";
$lang['payment_reference_no'] 			= "Nº de Ref. do Pagamento";
$lang['edit_payment'] 					= "Salvar Pagamento";
$lang['delete_payment'] 				= "Excluir Pagamento";
$lang['delete_payments'] 				= "Excluir Pagamentos";
$lang['payment_added'] 					= "Pagamento adicionado com sucesso";
$lang['payment_updated'] 				= "O Pagamento foi atualizado com sucesso";
$lang['payment_deleted'] 				= "Pagamento excluído com sucesso";
$lang['payments_deleted'] 				= "Pagamentos excluídos com sucesso";
$lang['paid_by'] 						= "Pago por";
$lang['payment_reference'] 				= "Ref. do Pagamento";
$lang['view_purchase_details'] 			= "Ver Detalhes da Despesa";
$lang['purchase_no'] 					= "Nº da Despesa";
$lang['balance'] 						= "Pagar";
$lang['product_option']                     = "Opções da Viagem";
$lang['payment_sent']                       = "Pagamento Enviado";
$lang['payment_note']                       = "Obs. do Pagamento";
$lang['payment_received']                   = "Pagamento Recebido";
$lang['purchase_status']                    = "Status da Despesa";
$lang['purchase_x_edited_older_than_3_months'] = "Esta Despesa não pode ser editada, pois já tem mais de 3 meses. Você pode editar as Despesas dentro de 3 meses.";
$lang['pr_not_found']                       = "Nenhum Viagem encontrado ";
$lang['line_no']                            = "Nº da Linha";
$lang['expense']                            = "Despesa";
$lang['edit_expense']                       = "Editar Despesa";
$lang['delete_expense']                     = "Excluir Despesa";
$lang['delete_expenses']                    = "Excluir Despesas";
$lang['expense_added']                      = "Despesa adicionada com sucesso";
$lang['expense_updated']                    = "Despesa atualizada com sucesso";
$lang['expense_deleted']                    = "Despesa excluída com sucesso";
$lang['reference']                          = "Referência";
$lang['expenses_deleted']                   = "Despesa excluída com sucesso";
$lang['expense_note']                       = "Obs. da Despesa";
$lang['no_expense_selected']                = "Nehuma Despesa selecionada. Por favor, selecionar pelo menos uma despesa.";
$lang['please_select_supplier']             = "Por favor, selecione um vendedor";
$lang['unit_cost']                          = "Custo";
$lang['product_expiry_date_issue']          = "A Data de validade da Viagem está errada";
$lang['received_more_than_ordered']         = "A quantidade recebida foi maior do que o pedido";
$lang['purchase_order']                     = "Pedido de Despesa";
$lang['payment_term']                       = "Termo de Pagamento";
$lang['payment_term_tip']                   = "Por favor, escreva o número de dias (inteiros) somente";
$lang['due_on']                             = "Vence em";
$lang['paid_amount']                        = "Valor Pago";
$lang['return_purchase']                    = "Devolução de Despesa";
$lang['return_surcharge']                   = "Reembolso de Valor Excedente";
$lang['return_amount']                      = "Reembolso de Valor";
$lang['purchase_reference']                 = "Referência da Despesa";
$lang['return_purchase_no']                 = "Nº da Devolução de Despesa";
$lang['view_return']                        = "Ver Devolução";
$lang['return_purchase_deleted']            = "Devolução de Despesa excluída com sucesso";
$lang['purchase_status_x_received']         = "o Status da Despesa está como Não Recebido";
$lang['total_before_return']                = "Total antes da Devolução";
$lang['return_amount']                      = "Valor Devolvido";
$lang['return_items']                       = "Ítens Devolvidos";
$lang['surcharge']                          = "Preço Excessivo";
$lang['returned_items']                     = "Ítens Retornados";
$lang['return_quantity']                    = "Quantidade Retornada";
$lang['seller']                             = "Vendedores";
$lang['users']                              = "Usuários";
$lang['return_note']                        = "Obs. da Devolução";
$lang['return_purchase_added']              = "Devolução de Despesa adicionada com sucesso";
$lang['return_has_been_added']              = "Alguns itens foram devolvidos nesta Despesa";
$lang['return_tip']                         = "Por favor edite a quantidade de ítens devolvidos abaixo. Você pode remover o item ou ajustar a quantidade de retorno a zero se não está sendo devolvido";
$lang['return_surcharge']                   = "Reembolso de Valor Excedente";
$lang['payment_returned']                   = "Pagamento Devolvido";
$lang['payment_received']                   = "Pagamento Recebido";
$lang['payment_note']                       = "Obs. do Pagamento";
$lang['payment_status']                     = "Status do Pagamento";
$lang['view_return_details']                = "Ver Detalhes da Devolução";
$lang['adjust_payments']                    = "Por favor ajustar os pagamentos para a Despesa manualmente";
$lang['purchase_has_returned']              = "Despesas com Entrada de Devolução não pode ser editada";
$lang['purchase_already_returned']          = "Despesa já possui uma Entrada de Devolução";
$lang['expenses_report']            		= "Despesas Operacionais";