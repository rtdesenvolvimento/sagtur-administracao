<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['documents']      = "Documentos";
$lang['document']       = "Documento";
$lang['add_document']   = "Criar Um Novo Documento";
$lang['created_at']     = "Data da criação";
$lang['doc_name']       = "Nome do Documento";
$lang['signatures']     = "Assinaturas";
$lang['add_signatories']  = "Adicionar Assinantes";
$lang['signatory']      = "Signatário";
$lang['signatorys']      = "Signatários";
$lang['signatories_details'] = 'Informe os signatários';
$lang['contracts_info'] = 'Signatários são as pessoas que irão assinar o documento, você pode adicionar quantos precisar.';
$lang['shipping_type'] = 'Envio Por';
$lang['signatory_name'] = 'Nome do Signatário';
$lang['signer_telefone'] = 'WhatsApp';
$lang['signer_email'] = 'E-mail';
$lang['signatories_note'] = 'Mensagem ao(s) Assinantes(s)';
$lang['signatory_note_placeholder'] = 'Ex: Segue o contrato para assinatura...';
$lang['signature_type'] = 'Como o(s) signatário(s) devem assinar?';
$lang['all_signature_type'] = 'De qualquer forma (Podem escolher).';
$lang['text_signature_type'] = 'Digitando texto (Recomendado).';
$lang['draw_signature_type'] = 'Desenhando na tela (Não recomendado para computadores).';
$lang['upload_signature_type'] = 'Envio de imagem da assinatura (Upload de imagem).';
$lang['solicitar_selfie'] = 'Solicitar Selfie (Beta)';
$lang['solicitar_documento'] = 'Solicitar Documento (Beta)';
$lang['auto_destruir_solicitacao'] = 'Autodestruir solicitação?';
$lang['data_vencimento_contrato'] = 'Quando a solicitação deve se cancelar automaticamente?<br/><small>Caso o signatário não assine o documento até a data selecionada, iremos cancelar a mesma automáticamente e te avisaremos por e-mail!   </small>';
$lang['add_signatory'] = 'Adicionar Signatário';
$lang['send_subscription_request'] = 'Enviar Solicitação de Assinatura';
$lang['subscription_request_sent_successfully'] = 'Solicitação de assinatura enviada com sucesso!';
$lang['document_successfully_added'] = 'Documento adicionado com sucesso!';
$lang['monitor_contract_signature'] = 'Monitor de Assinaturas';
$lang['download_original_document'] = 'Baixar Arquivo Original';
$lang['download_signed_document'] = 'Baixar Arquivo Assinado';
$lang['signatorys_history'] = 'Histórico';
$lang['resend_to_signatories'] = 'Reenviar aos Signatários';
$lang['successfully_forwarded_to_signatories'] = 'Documento reenviado com sucesso aos signatários!';
$lang['signatory_added_successfully'] = 'Signatário adicionado com sucesso ao contrato e enviado para assinatura!';
$lang['cancel_signatory'] = 'Remover Signatário';
$lang['resend_to_signatory'] = 'Reenviar Assinatura Ao Signatário';
$lang['successfully_removed_to_signatory'] = 'Signatário removido com sucesso!';
$lang['error_removed_to_signatory'] = 'Erro ao remover signatário!';
$lang['cancel_document'] = 'Cancelar Documento';
$lang['successfully_cancel_document'] = 'Documento cancelado com sucesso!';
$lang['telefone_to_signatario'] = 'Telefone do Signatário';
$lang['my_documents'] = 'Meus Documentos';
$lang['document_status'] = 'Status do Documento';
$lang['draft'] = 'Rascunho';
$lang['unsigned'] = 'Aguardando Assinatura';
$lang['pending'] = 'Assinado Parcialmente';
$lang['signed'] = 'Documento Assinado';
$lang['canceled'] = 'Documento Cancelado';
$lang['view_documents_list'] = 'Lista de Documentos';
$lang['no_documents_found'] = 'Nenhum documento encontrado!';
$lang['open_new_page'] = 'Abrir em Nova Página';
$lang['no_signatories_found'] = 'Nenhum signatário encontrado!';
$lang['load_more_documents'] = 'Carregar mais documentos';
$lang['edit_document'] = 'Editar Documento';
$lang['rename_document'] = 'Renomear Documento';
$lang['document_successfully_edited'] = 'Documento editado com sucesso!';
$lang['to_share'] = 'Compartilhar Documento';
$lang['folders'] = 'Pastas';
$lang['load_more_folders'] = 'Carregar mais pastas';
$lang['add_folder'] = 'Nova Pasta';
$lang['folder_name'] = 'Nome da Pasta';
$lang['folder_successfully_added'] = 'Pasta adicionada com sucesso!';
$lang['rename_folder'] = 'Renomear Pasta';
$lang['folder_deleted_successfully'] = 'Pasta deletada com sucesso!';
$lang['folder_successfully_edited'] = 'Pasta editada com sucesso!';
$lang['delete_folder'] = 'Deletar Pasta';
$lang['folder'] = 'Pasta';
$lang['edit_folder'] = 'Editar Pasta';
$lang['yes'] = 'Sim';
$lang['no'] = 'Não';
$lang['customers'] = 'Clientes';
$lang['customer'] = 'Cliente';
$lang['search_customers'] = 'Buscar Clientes';
$lang['add_customer'] = 'Adicionar Cliente';
$lang['sale_contract_generated_successfully'] = 'Contrato de Venda gerado com sucesso!';
$lang['confirmado'] = "Serviços Ativo";
$lang['produto_inativo'] = "Serviços Inativo";
$lang['arquivado']  = "Arquivados";
$lang['data_venda_de'] = 'Data da Venda de';
$lang['data_venda_ate'] = 'Até';
$lang['status_document'] = 'Status do Documento';
$lang['search_signatory'] = 'Buscar Signatário Por (Nome, telefone ou e-mail)';
$lang['search_by'] = 'Buscar Por';
$lang['search_folder'] = 'Pasta (Nome, telefone ou e-mail de signatário)';
$lang['assinar_documento'] = 'Assinar Documento';
$lang['successfully_signed_document'] = 'Documento assinado com sucesso!';
$lang['to_share_validate'] = 'Histórico do documento';
$lang['send_document'] = 'Enviar Documento';
$lang['signatories_note_send'] = 'Mensagem ao Destinatário';
$lang['add_contracts'] = 'Adicionar Contratos';
$lang['add_contract'] = 'Adicionar Contrato';
$lang['contract'] = 'Contrato';
$lang['contract_added_successfully'] = 'Contrato adicionado com sucesso!';
$lang['contract_edited_successfully'] = 'Contrato editado com sucesso!';
$lang['edit_contract'] = 'Editar Contrato';
$lang['delete_contract'] = 'Deletar Contrato';
$lang['contract_deleted_successfully'] = 'Contrato deletado com sucesso!';
$lang['completed'] = 'Assinado';
$lang['rejected'] = 'Rejeitado';
$lang['webhook_created_successfully'] = 'Webhook criado com sucesso!';
$lang['tags_contrato'] = 'Tags do Contrato';
$lang['text'] = 'Texto';
$lang['usar_clausulas_site'] = 'Usar Este Contrato para Exibir no Site (Usar quando a conversão do Word para HTML não estiver adequada para exibição)';
$lang['clauses'] = 'Contrato';
$lang['send_documento_to_whatsapp'] = 'Enviar Documento por WhatsApp';
$lang['view_sale'] = 'Visualizar Venda';
$lang['issued'] = 'Emitido';
$lang['sent_signature'] = 'Enviado para Assinatura';
$lang['events'] = 'Eventos do Contrato';
$lang['signatory_removed'] = 'Signatário Removido';
$lang['send_document_to'] = 'Documento Reenviado';
$lang['send_document_pdf'] = 'Documento Assinado Enviado para WhatsApp do Cliente';
$lang['contract_events'] = 'Eventos do Contrato';