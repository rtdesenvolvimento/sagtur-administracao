<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014 - 2015, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	http://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['captacoes'] = 'Captações';

$lang['product_interest'] = 'Interesse';
$lang['origem'] = "Origem";
$lang['forma_atendimento'] = 'Atendimento';
$lang['captacao_status'] = 'Situação';

$lang['read'] = 'Lido';
$lang['department'] = 'Setor';

$lang['whatsapp_send'] = "Abrir Conversa com Cliente no WhatsApp";
$lang['read_capture'] = 'Confirmar Leitura da Captação';

$lang['interesse_captacao'] = 'Motivo do Contato';

$lang['motivo'] = 'Motivo';
$lang['origem'] = 'Origem';