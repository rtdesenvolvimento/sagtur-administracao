<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Module: System Settings
 * Language: Brazilian Portuguese Language File
 * 
 * Last edited:
 * 10 de Maio de 2016
 *
 * Package:
 * Stock Manage Advance v3.0.2.8
 *
 * Translated by:
 * Robson Gonçalves (POP Computadores) robson@popcomputadores.com.br
 *
 * License:
 * GPL v3 or above
 *
 * You can translate this file to your language. 
 * For instruction on new language setup, please visit the documentations. 
 * You also can share your language files by emailing to saleem@tecdiary.com 
 * Thank you 
 */


$lang['site_name'] 						= "Nome da Empresa";
$lang['dateformat'] 					= "Formato de Data";
$lang['timezone'] 						= "Fuso Horário";
$lang['maintenance_mode'] 				= "Modo de Manutenção";
$lang['image_width'] 					= "Largura da imagem";
$lang['image_height'] 					= "Altura da imagem";
$lang['thumbnail_width'] 				= "Largura da Miniatura";
$lang['thumbnail_height'] 				= "Altura da Miniatura";
$lang['watermark'] 						= "Marca D'água (nas imagens)";
$lang['reg_ver'] 						= "Verificação de Registro";
$lang['allow_reg'] 						= "Permitir Registro";
$lang['reg_notification'] 				= "Notificação de Registro";
$lang['default_email'] 					= "E-mail Padrão";
$lang['default_warehouse'] 				= "Filial padrão";
$lang['product_tax'] 					= "Imposto sobre Viagens";
$lang['invoice_tax'] 					= "Imposto sobre Faturas";
$lang['sales_prefix'] 					= "Prefixo de Ref. de Vendas";
$lang['quote_prefix'] 					= "Prefixo de Ref. de Cotação";
$lang['purchase_prefix'] 				= "Prefixo de Ref. de Despesas";
$lang['transfer_prefix'] 				= "Prefixo de Ref. de Transferências";
$lang['delivery_prefix'] 				= "Prefixo de Ref. de Entregas";
$lang['payment_prefix'] 				= "Prefixo de Ref. de Pagamentos";
$lang['detect_barcode'] 				= "Detectar Cód. de Barras";
$lang['theme'] 							= "Tema";
$lang['rows_per_page'] 					= "Linhas por página";
$lang['accounting_method'] 				= "Método de Contabilidade";
$lang['product_serial'] 				= "Viagem com Nº de série";
$lang['product_discount'] 				= "Desconto de Viagens";
$lang['bc_fix'] 						= "Contador de Viagens para corrigir a entrada de Cód. de Barras";
$lang['decimals'] 						= "Decimais";
$lang['decimals_sep'] 					= "Separador Decimal";
$lang['thousands_sep'] 					= "Separador de Milhares";
$lang['overselling_will_only_work_with_AVCO_accounting_method_only'] = "Uma Passagem excessiva só irá funcionar com o método de contabilidade AVCO, temos que alterar o método de contabilidade para você AVCO.";
$lang['accounting_method_change_alert'] = "Você está alterando o método de contabilidade, isso irá atrapalhar a sua conta antiga. Sugerimos que você faça isso apenas no 1º dia de cada mês.";
$lang['setting_updated'] 				= "Configurações atualizadas com sucesso";
$lang['logo_uploaded'] 					= "Logo enviado com sucesso";
$lang['site_config'] 					= "Configuração do Sistema";
$lang['dot'] 							= "Ponto";
$lang['comma'] 							= "Vírgula";
$lang['space'] 							= "Espaço";
$lang['email_protocol'] 				= "Protocolo de e-mail";
$lang['smtp_host'] 						= "Servidor SMTP";
$lang['smtp_user'] 						= "Usuário do SMTP";
$lang['smtp_pass'] 						= "Senha do SMTP";
$lang['smtp_port'] 						= "Porta do SMTP";
$lang['update_settings'] 				= "Atualizar as Configurações";
$lang['prefix_year_no'] 				= "Prefixo/Ano/Nº Sequencial (SL/2014/001)";
$lang['prefix_month_year_no'] 			= "Prefixo/Ano/Mês/Nº Sequencial (SL/2014/08/001)";
$lang['login_captcha'] 					= "Entrada com Captcha";
$lang['default_currency'] 				= "Moeda Padrão";
$lang['calendar'] 						= "Calendário";
$lang['private'] 						= "Privado";
$lang['shared'] 						= "Compartilhado";
$lang['racks'] 							= "Assentos";
$lang['product_expiry'] 				= "Viagem com Vencimento";
$lang['image_size'] 					= "Tamanho da Imagem";
$lang['thumbnail_size'] 				= "Tamanho da Amostra";
$lang['over_selling'] 					= "Passagem Excessiva";
$lang['reference_format'] 				= "Formato da Ref.";
$lang['product_level_discount'] 		= "Nível de desconto da Viagem";
$lang['auto_detect_barcode'] 			= "Auto Detectar Cód. de Barras";
$lang['item_addition'] 					= "Método de Adicionar o Item";
$lang['add_new_item'] 					= "Adicionar novo item ao pedido";
$lang['increase_quantity_if_item_exist'] = "Aumentar a quantidade do item, se ele já existe no pedido";
$lang['prefix'] 						= "Prefixo";
$lang['money_number_format'] 			= "Formato do Números e Moeda";
$lang['reg_notification'] 				= "Notificação de Registro";
$lang['site_logo'] 						= "Logo do Sistema";
$lang['biller_logo'] 					= "Logo do Vendedor";
$lang['upload_logo'] 					= "Atualizar Logo";
$lang['add_currency'] 					= "Adicionar Moedas";
$lang['edit_currency'] 					= "Salvar Moeda";
$lang['delete_currency'] 				= "Excluir Moeda";
$lang['delete_currencies'] 				= "Excluir Moedas";
$lang['currency_code'] 					= "Código da Moeda";
$lang['currency_name'] 					= "Nome da Moeda";
$lang['exchange_rate'] 					= "Taxa de Câmbio";
$lang['new_currency'] 					= "Adicionar Moedas";
$lang['currency_added'] 				= "Moeda adicionada com sucesso";
$lang['currency_updated'] 				= "Moeda atualizada com sucesso";
$lang['currency_deleted'] 				= "Moeda excluída com sucesso";
$lang['currencies_deleted'] 			= "Moedas excluídas com sucesso";
$lang['add_customer_group'] 			= "Adicionar Grupo de Passageiros";
$lang['edit_customer_group'] 			= "Salvar Grupo de Passageiros";
$lang['delete_customer_group'] 			= "Excluir Grupo de Passageiros";
$lang['delete_customer_groups'] 		= "Excluir Grupos de Passageiros";
$lang['percentage'] 					= "Porcentagem";
$lang['group_name'] 					= "Nome do Grupo";
$lang['group_percentage'] 				= "Grupo de Percentagem (sem o sinal %)";
$lang['customer_group_added'] 			= "Grupo de Passageiros adicionado com sucesso";
$lang['customer_group_updated'] 		= "Grupo de Passageiros atualizado com sucesso";
$lang['customer_group_deleted'] 		= "Grupo de Passageiros excluído com sucesso";
$lang['customer_groups_deleted'] 		= "Os Grupos de Passageiros foram excluídos com sucesso";
$lang['tax_rate'] 						= "Taxas";
$lang['add_tax_rate'] 					= "Adicionar Taxa de Imposto";
$lang['edit_tax_rate'] 					= "Salvar Taxa de Imposto";
$lang['delete_tax_rate'] 				= "Excluir Taxa de Imposto";
$lang['delete_tax_rates'] 				= "Excluir Alíquotas";
$lang['fixed'] 							= "Fixo";
$lang['type'] 							= "Digite";
$lang['rate'] 							= "Taxa";
$lang['tax_rate_added'] 				= "Taxa de imposto adicionada com sucesso";
$lang['tax_rate_updated'] 				= "Taxa de imposto atualizada com sucesso";
$lang['tax_rate_deleted'] 				= "Taxa de imposto excluída com sucesso";
$lang['tax_rates_deleted'] 				= "As taxas de impostos foram excluídas com sucesso";
$lang['warehouse'] 						= "Filial";
$lang['add_warehouse'] 					= "Adicionar Filial";
$lang['edit_warehouse'] 				= "Editar Filial";
$lang['delete_warehouse'] 				= "Excluir Filial";
$lang['delete_warehouses'] 				= "Excluir Filial";
$lang['code'] 							= "Código";
$lang['name'] 							= "Nome";
$lang['map'] 							= "mapa";
$lang['map_image'] 						= "Imagem do Mapa";
$lang['warehouse_map'] 					= "Logo da Filial";
$lang['warehouse_added'] 				= "Filial adicionada com sucesso";
$lang['warehouse_updated'] 				= "Filial atualizado com sucesso";
$lang['warehouse_deleted'] 				= "Filial excluído com sucesso";
$lang['warehouses_deleted'] 			= "Filial excluídos com sucesso";
$lang['mail_message'] 					= "Mensagem do E-mail";
$lang['activate_email'] 				= "Ativação do e-mail";
$lang['forgot_password'] 				= "Esqueci minha senha";
$lang['short_tags'] 					= "Tags curtas";
$lang['message_successfully_saved'] 	= "Mensagem salva com sucesso";
$lang['group'] 							= "Grupo de Usuários";
$lang['groups'] 						= "Grupos de Usuários";
$lang['add_group'] 						= "Adicionar Grupo de Usuários";
$lang['create_group'] 					= "Adicionar Grupo";
$lang['edit_group'] 					= "Salvar Grupo de Usuários";
$lang['delete_group'] 					= "Excluir Grupo de Usuários";
$lang['delete_groups'] 					= "Excluir Grupos de Usuários";
$lang['group_id'] 						= "ID do Grupo de Usuários";
$lang['description'] 					= "Descrição";
$lang['group_description'] 				= "Descrição do grupo";
$lang['change_permissions'] 			= "Alterar permissões";
$lang['group_added'] 					= "Grupo de usuários adicionado com sucesso";
$lang['group_updated'] 					= "Grupo do usuários atualizado com sucesso";
$lang['group_deleted'] 					= "Grupo do usuários excluído com sucesso";
$lang['groups_deleted'] 				= "Grupos do usuários excluídos com sucesso";
$lang['permissions'] 					= "Permissões";
$lang['set_permissions'] 				= "Definir Permissões";
$lang['module_name'] 					= "Nome do Módulo";
$lang['misc'] 							= "Diversos";
$lang['default_customer_group'] 		= "Grupo";
$lang['paypal'] 						= "Paypal";
$lang['paypal_settings'] 				= "Config. do Paypal";
$lang['activate'] 						= "Ativar";
$lang['paypal_account_email'] 			= "E-mail da Conta no Paypal";
$lang['fixed_charges'] 					= "Taxas Fixas";
$lang['account_email_tip'] 				= "Digite seu e-mail da conta do paypal";
$lang['fixed_charges_tip'] 				= "quaisquer custos adicionais fixos para todos os pagamentos através deste gateway de pagamentos";
$lang['extra_charges_my'] 				= "Despesas adicionais percentual para o seu país.";
$lang['extra_charges_my_tip'] 			= "Despesas adicionais percentual para todos os pagamentos do seu país.";
$lang['extra_charges_others'] 			= "Despesas adicionais percentual para outros países.";
$lang['extra_charges_others_tip'] 		= "Despesas adicionais percentual para todos os pagamentos de outros países.";
$lang['ipn_link'] 						= "IPN Link";
$lang['ipn_link_tip'] 					= "Adicionar este link na sua conta paypal, a fim de ativar o IPN.";
$lang['paypal_setting_updated'] 		= "Configurações do Paypal atualizadas com sucesso";
$lang['skrill'] 						= "Skrill";
$lang['skrill_account_email'] 			= "E-mail da Conta Skrill";
$lang['skrill_email_tip'] 				= "Digite seu e-mail da conta Skrill";
$lang['secret_word'] 					= "Palavra Secreta";
$lang['paypal_setting_updated'] 		= "Configurações do Paypal atualizadas com sucesso";
$lang['skrill_settings'] 				= "Config. do Skrill";
$lang['secret_word_tip'] 				= "Digite a sua palavra secreta do Skrill";
$lang['default_currency'] 				= "Moeda padrão";
$lang['auto_update_rate'] 				= "Atualização Automática de Taxas";
$lang['return_prefix'] 					= "Prefixo de Devolução";
$lang['product_variants_feature_x'] 	= "O recurso de Variantes de Viagens não funcionará com esta opção";
$lang['group_permissions_updated'] 		= "Permissões de grupo atualizadas com sucesso";
$lang['tax_invoice'] 					= "Nota Fiscal";
$lang['standard'] 						= "Padrão";
$lang['invoice_view'] 					= "Ver Fatura";
$lang['restrict_user'] 					= "Restringir Usuário";
$lang['logo_image_tip']                 = "Tamanho max. do arquivo é de 1024KB e (Largura=300px) x (Altura=80px).";
$lang['biller_logo_tip']                = "Por favor edite o faturamento após carregar o novo logotipo e selecione um logotipo atualizado.";
$lang['default_biller']                 = "Vendedor Padrão";
$lang['group_x_b_deleted']              = "Falha na ação, existem alguns usuários associados à esse grupo";
$lang['profit_loss']                    = "Ganhos e/ou Percas";
$lang['staff']                          = "Equipe";
$lang['stock_chartr']                   = "Gráfico de Estoque";
$lang['rtl_support']                    = "Suporte RTL";
$lang['backup_on']                      = "Backup realizado em ";
$lang['restore']                        = "Restaurar";
$lang['download']                       = "Download";
$lang['file_backups']                   = "Backups de Arquivos";
$lang['backup_files']                   = "Fazer Backup dos Arquivos";
$lang['database_backups']               = "Backups de Bancos de Dados";
$lang['db_saved']                       = "Bancos de Dados gravado com sucesso.";
$lang['db_deleted']                     = "Bancos de Dados excluído com sucesso.";
$lang['backup_deleted']                 = "Backup excluído com sucesso.";
$lang['backup_saved']                   = "Backup gravado com sucesso.";
$lang['backup_modal_heading']           = "Gravando seus Arquivos...";
$lang['backup_modal_msg']               = "Por favor aguarde, isso pode levar alguns minutos.";
$lang['restore_modal_heading']          = "Restaurando os Arquivos do Backup";
$lang['restore_confirm']                = "Essa ação não pode ser desfeita. Você tem certeza sobre essa restauração?";
$lang['delete_confirm']                 = "Essa ação não pode ser desfeita. Você tem certeza sobre essa exclusão?";
$lang['restore_heading']                = "Faça um backup antes de restaurar para uma versão mais antiga.";
$lang['full_backup']                    = 'Backup Completo';
$lang['database']                       = 'Banco de Dados';
$lang['files_restored']                 = 'Arquivos Restaurados com Sucesso';
$lang['delete_variant']                 = "Excluir Variação";
$lang['delete_variants']                = "Excluir custos";
$lang['variant_added']                  = "Variação adicionada com sucesso";
$lang['variant_updated']                = "Variação atualizada com sucesso";
$lang['variant_deleted']                = "Variação excluída com sucesso";
$lang['variants_deleted']               = "Custo excluídas com sucesso";
$lang['customer_award_points']          = 'Pontos do Passageiro';
$lang['staff_award_points']             = 'Pontos da Passagem';
$lang['each_spent']                     = 'Cada <i class="fa fa-arrow-down"></i> gasto é equivalente à';
$lang['each_in_sale']                   = 'Cada <i class="fa fa-arrow-down"></i> em Vendas é equivalente à';
$lang['mailpath']                       = "Mail Path";
$lang['smtp_crypto']                    = "SMTP Crypto";
$lang['random_number']                  = "Número Aleatório";
$lang['sequence_number']                = "Número Sequencial";
$lang['update_heading']                 = "Esta página irá ajudá-lo a verificar e instalar as atualizações facilmente com único clique. <strong>Se houver mais de 1 atualização disponível, atualizá-las uma por uma a partir da primeira (menor versão)</strong>.";
$lang['update_successful']              = "Ítem Atualizado com Sucesso";
$lang['using_latest_update']            = "Você está usando a Última Versão.";
$lang['version']                        = "Versão";
$lang['install']                        = "Instalar";
$lang['changelog']                      = "Change Log";
$lang['expense_prefix']                 = "Prefixo de Despesas";
$lang['purchase_code']                  = "Código da Licença";
$lang['envato_username']                = "Usuário da Agência";
$lang['sac']                            = "Formato de Moéda de Países do Sul da Asia";
$lang['qty_decimals']                   = "Qtd. de Decimais";
$lang['display_all_products']           = "Mostrar Viagens de Todos os Serviços";
$lang['hide_with_0_qty']                = "Ocultar com quantidade 0";
$lang['show_with_0_qty']                = "Mostrar todos mesmo os com quantidade 0";
$lang['display_currency_symbol']        = "Mostrar o Símbolo da Moeda";
$lang['currency_symbol']                = "Símbolo da Moeda";
$lang['after']                          = "Após";
$lang['before']                         = "Antes";
$lang['remove_expired']                 = "Remover Viagens vencidos do Estoque";
$lang['remove_automatically']           = "Remover do estoque automaticamente";
$lang['i_ll_remove']                    = "Eu irei remover manualmente";
$lang['db_restored']                    = "Banco de Dados Restaurado com Sucesso";
$lang['bulk_actions']                   = "Bulk actions";
$lang['barcode_separator']              = "Separador do Código de Barras";
$lang['-']                              = "Traço ( - )";
$lang['.']                              = "Ponto ( . )";
$lang['~']                              = "Til ( ~ )";
$lang['_']                              = "Underline ( _ )";
$lang['deposits']                       = "Viagens (Ver, Adicionar e Salvar)";
$lang['delete_deposit']                 = "Excluir Viagem";
$lang['expense_categories']             = "Plano de Contas";
$lang['add_expense_category']           = "Adicionar Categoria de Despesa";
$lang['edit_expense_category']          = "Salvar Categoria de Despesa";
$lang['delete_expense_category']        = "Excluir Categoria de Despesa";
$lang['delete_expense_categories']      = "Excluir Plano de contas";
$lang['expense_category_added']         = "Categoria de Despesa adicionada com sucesso";
$lang['expense_category_updated']       = "Categoria de Despesa atualizada com sucesso";
$lang['expense_category_deleted']       = "Categoria de Despesa excluída com sucesso";
$lang['category_has_expenses']          = "Categorias com Despesas não podem ser excluídas";
$lang['returnp_prefix']                 = "Prefixo de Devoluções de Despesas";
$lang['set_focus']                      = "Foco Padrão da Página de Pedido";
$lang['add_item_input']                 = "Adicionar Entrada de Ítem";
$lang['last_order_item']                = "Quantia de Entrada dos últimos ítens pedidos";
$lang['import_categories']              = "Importar Categorias";
$lang['categories_added']               = "Categorias Importadas com Sucesso";
$lang['parent_category_code']           = "Código da Categoria Relacionada";
$lang['import_subcategories']           = "Importar Sub-Categorias";
$lang['subcategories_added']            = "Sub-Categorias Importadas com Sucesso";
$lang['import_expense_categories']      = "Importar Plano de contas";
$lang['expenses_report']            	= "Despesas Operacionais";
$lang['data_aniversario']               = "Data de Aniversario";

$lang['variants']                       = "Cadastro de custos";
$lang['add_variant']                    = "Adicionar custo";
$lang['edit_variant']                   = "Editar custo";
$lang['planta']                         = "Planta";

$lang['receitas']                       = "Plano de Contas - Receitas";
$lang['adicionar_receita']              = "Adicionar Receita";
$lang['editar_receita']                 = "Editar Receita";
$lang['receita_adicionada_com_successo']= "Receita adicionada com sucesso";
$lang['receita_atualizada_com_sucesso'] = "Receita atualizada com sucesso";
$lang['receita_sendo_utilizada']        = "Esta receita não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['receita_deletada_com_sucesso']   = "Receita deletada com sucesso";
$lang['deletar_receita']                = "Deletar Receita";


$lang['ativo']                                = "Ativo";
$lang['inativo']                              = "Inativo";


$lang['formaspagamento']                        = "Formas de pagamento";
$lang['adicionar_forma_pagamento']              = "Adicionar Forma de pagamento";
$lang['editar_forma_pagamento']                 = "Editar Forma de pagamento";
$lang['forma_pagamento_adicionada_com_successo']= "Forma de pagamento adicionada com sucesso";
$lang['forma_pagamento_atualizada_com_sucesso'] = "Forma de pagamento atualizada com sucesso";
$lang['forma_pagamento_sendo_utilizada']        = "Esta forma de pagamento não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['forma_pagamento_deletada_com_sucesso']   = "Forma de pagamento deletada com sucesso";
$lang['deletar_forma_pagamento']                = "Deletar Forma de pagamento";

$lang['condicoespagamento']                             = "Condições de pagamento";
$lang['adicionar_condicoes_pagamento']                  = "Adicionar Condição de pagamento";
$lang['editar_condicao_pagamento']                      = "Editar Condição de pagamento";
$lang['condicao_pagamento_adicionada_com_successo']     = "Condição de pagamento adicionada com sucesso";
$lang['condicao_pagamento_atualizada_com_sucesso']      = "Condição de pagamento atualizada com sucesso";
$lang['condicao_pagamento_sendo_utilizada']             = "Esta condição de pagamento não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['condicao_pagamento_deletada_com_sucesso']        = "Condição de pagamento deletada com sucesso";
$lang['deletar_condicao_pagamento']                     = "Deletar Condição de pagamento";
$lang['condicao_pagamento_name']                        = "Nome";
$lang['intervalo']                                      = "Intervalo";
$lang['parcelas']                                       = "Gerar quantas vezes";
$lang['intervalo_em_dias']                              = "Dias do vencimento separado por ',' (deixe em branco para não utilizar)";

$lang['dre_receitas']                                   = "(+) Receitas";
$lang['dre_despesas']                                   = "(-) Despesas";
$lang['ambas']                                          = "(+/-) Ambas";
$lang['totalizador']                                    = "(=) Totalizador";

$lang['dre']                                            = "Classes de DRE - Demostrativo de Resultado";
$lang['adicionar_dre']                                  = "Adicionar DRE";
$lang['editar_dre']                                     = "Editar DRE";
$lang['dre_adicionada_com_successo']                    = "DRE adicionada com sucesso";
$lang['dre_atualizada_com_sucesso']                     = "DRE atualizada com sucesso";
$lang['dre_sendo_utilizada']                            = "Este dre não pode ser deletada pois está sendo utilizada por outro registro do sistema.";
$lang['dre_deletada_com_sucesso']                       = "DRE deletada com sucesso";
$lang['deletar_dre']                                    = "Deletar DRE";
$lang['dre_name']                                       = "Nome";
$lang['dre_tipo']                                       = "Tipo";
$lang['ordem_exibir_dre']                               = "Ordem exibiação em relatórios";
$lang['forma_pagamento']                                = "Forma de Pagamento";
$lang['tipo_tipo_cobranca']                             = "Cobrança será feita em";
$lang['segunda']                                        = "segunda-feira";
$lang['terca']                                          = "terça-feira";
$lang['quarta']                                         = "quarta-feira";
$lang['quinta']                                         = "quinta-feira";
$lang['sexta']                                          = "sexta-feira";
$lang['sabado']                                         = "sábado";
$lang['domingo']                                        = "domingo";
$lang['configurar_horario_comercial']                   = "Configurar horário comercial";
$lang['atualizar_horario_comercial']                    = "Atualizar";
$lang['horarios_atualizados_com_sucesso']               = "Horários comerciais atualizados com sucesso.";
$lang['horario_comercial']                              = "Horário Comercial";
$lang['pagseguro']                                      = "PagSeguro";
$lang['pagSeguro_settings']                             = "Configuração do PagSeguro";
$lang['pagseguro_account_email']                        = "E-mail da sua conta no PagSeguro";
$lang['pagseguro_account_token']                        = "Token em PRODUÇÃO da sua conta PagSeguro";
$lang['pagseguro_account_token_sandbox']                = "Token em SANDBOX da sua conta PagSeguro";
$lang['pagseguro_setting_updated']                      = "Configuração do PagSeguro atualizada com sucesso.";
$lang['em_percentual']   = "%";
$lang['absoluto']        = "R$";
$lang['desconto']        = "Desconto";
$lang['diasAvancaPrimeiroVencimento'] = "Máximo de dias p/ Pagar Primeira Parcela";
$lang['diasMaximoPagamentoAntesViagem']        = "Máximo de dias p/ pagar antes da Viagem";
$lang['taxaIntermediacao']  = "(%) Taxa de Intermediação";
$lang['taxaFixaIntermediacao']  = "(R$) Tarifa fixa por Transação";
$lang['sinal_primeiro_vencimento'] = "Sinal de Pagamento";
$lang['configuracoes_taxas_tipo_cobranca'] = "Configurar Taxas";
$lang['taxas_setting_updated']  = "Taxas atualizadas com sucesso";
$lang['exibirNaSemanaDaViagem'] = "Permitir usar essa condição na semana da viagem? <small>(se habilitado permite usar essa condição depois que passar o máximo de dias antes da viagem que você definiu) </small>";
$lang['usarTaxasPagSeguro'] = "Usar taxas do PAGSeguro <small>(Ao habilitar essa opção taxas de intermediação, tarifas fixas ou por parcela digitadas aqui serão desconsideradas)</small>";


$lang['cadastro_contas'] = "Cadastrar Contas Bancárias";
$lang['tipo_cobranca_conta'] = "Conta de Destino";
$lang['tipos_cobranca'] = "Cadastrar Tipo de Cobrança";
$lang['info_configurar_taxas_cartao_boleto'] = "Configurar Taxas do Cartão de Crédito ou Boleto";
$lang['info_habilitar_condicoes_pagamento'] = "Habilitar Condições de Pagamento Para as Formas de Cobrança";

$lang['faturar_venda'] = "Sim - Faturar Automático";
$lang['orcamento_venda'] = "Não - Lançar Venda Apenas em Orçamento";
$lang['configurar_pagseguro'] = "Integração PagSeguro";
$lang['configurar_juno'] = "Integração JUNO";

$lang['juno_settings'] = "Configurar Integração JUNO";
$lang['juno_account_email'] = "Seu E-mail de Cadastro na JUNO";
$lang['juno_account_token'] = "Seu TOKEN de Integração na JUNO (TOKEN PRIVADO)";
$lang['juno_account_token_sandbox'] = "TOKEN apenas para Teste (não preencha)";
$lang['sandbox'] = "Habilitar Testes (Deixe como Não - usado apenas para testes internos)";
$lang['nao'] = "Não";
$lang['sim'] = "Sim";
$lang['info_cadastre_se_com_nossa_conta_juno'] = "Ainda não possui uma conta JUNO? Use nosso link abaixo para abrir sua conta e ter benefícios com nosso link de afiliado.";

$lang['pagseguro_account_email'] = "Seu E-mail de Cadastro no PagSeguro";
$lang['juno_settings'] = "Configuração Integração JUNO";
$lang['configar_taxas'] = "Configurar Taxas de Pagamento";


$lang['mercadopago'] = "MercadoPago";
$lang['mercadopago_settings'] = "Configuração Integração MercadoPago";
$lang['account_token_private'] = "Token Privado";
$lang['account_token_public'] = "Token Publico";
$lang['mercadopago_setting_updated'] = "Dados do MercadoPago atualizado com sucesso";

$lang['product_contrato'] = "Contrato (Será exibido no Voucher)";
$lang['product_informacoes_importantes_voucher'] = "Informações importantes para exibir no Voucher (Exibido em amarelo no Voucher)";
$lang['exibirCancelamentosListaVenda'] = "Exibir Vendas Canceladas Junto com a Lista de Vendas?";
$lang['facebook'] = "Facebook";
$lang['instagram'] = "Intagram";
$lang['youtube'] = "YouTube";
$lang['frase_site'] = "Frase do Site (Slogan)";
$lang['pixelFacebook'] = "Código do Pixel do Facebook";
$lang['googleAnalytics'] = "Código do Google Analytics";
$lang['termos_aceite'] = "Termos de Aceite (Exibidos no Link de Reserva)";
$lang['exibirDadosFaturaVoucher'] = "Exibir Dados da Fatura no Voucher?";
$lang['plano_sagtur'] = "Seu Plano";
$lang['numero_ususarios'] = "Número de Usuários Ativos";
$lang['dia_vencimento_plano'] = "Dia Vencimento do Plano";
$lang['usarOrcamentoLoja'] = "Apenas Orçamento no Link de Reservas";
$lang['ocultarProdutosSemEstoqueLoja'] = "Ocultar Viagem Sem Vagas da Loja?";

$lang['filtrar_itens_loja']     = "Filtrar Itens por mês? (link da loja)";
$lang['config_loja']            = "Configurações da Loja";
$lang['logo_shop']              = "Imagem em destaque no cabeçalho da loja";

$lang['habilitar_cupom_desconto'] = "Habilitar Cupom de Desconto";
$lang['envio_email_venda_manual'] = "Enviar voucher ao lançar uma venda manual";
$lang['send_email_canceling_sale'] = "Enviar e-mail ao cancelar venda";

$lang['assinatura'] = "Assinatura para o Recibo de Pagamento";

$lang['theme_color'] = "Cor Principal Site";
$lang['theme_color_secondary'] = "Cor Secundaria Site";
$lang['theme_site'] = "Tema do Site";
$lang['view_logomarca_site'] = "Exibir Logomarca";
$lang['about'] = "Sobre Nós";
$lang['about_photo'] = "Fotos Sobre Nós";
$lang['slider1'] = "Slider 1";
$lang['slider2'] = "Slider 2";
$lang['slider3'] = "Slider 3";
$lang['slider4'] = "Slider 4";

$lang['slider1_slogan'] = "Slogan";
$lang['slider2_slogan'] = "Slogan";
$lang['slider3_slogan'] = "Slogan";
$lang['slider4_slogan'] = "Slogan";

$lang['site_width'] = "Largura Imagem Site";
$lang['site_height'] = "Altura Imagem Site";
$lang['horario_atendimento'] = "Horário de Atendimento";
$lang['quantity_thumbs'] = "Qtd Imagem Pequena no Site";

$lang['add_testimonial'] = "Adicionar um Depoimento";
$lang['editar_testimonial'] = "Editar Depoimento";
$lang['testimonial'] = "Depoimento";
$lang['testimonials'] = "Depoimentos";
$lang['profession'] = "Profissão";
$lang['testimonial_atualizado_com_sucesso'] = "Depoimento Atualizado com Sucesso";
$lang['testimonial_adicionado_com_sucesso'] = "Depoimento Adicionado com Sucesso";
$lang['photo'] = "Foto";

$lang['add_team'] = "Adicionar Novo Funcionário Equipe";
$lang['editar_team'] = "Editar Depoimento";
$lang['team'] = "Equipe Site";
$lang['office'] = "Cargo";
$lang['team_atualizado_com_sucesso'] = "Depoimento Atualizado com Sucesso";
$lang['team_adicionado_com_sucesso'] = "Depoimento Adicionado com Sucesso";
$lang['biography'] = "Biografia";


$lang['add_gallery'] = "Adicionar Novo Galeria";
$lang['editar_gallery'] = "Editar Galeria";
$lang['gallery'] = "Galeria Site";
$lang['gallery_atualizado_com_sucesso'] = "Galeria Atualizado com Sucesso";
$lang['gallery_adicionado_com_sucesso'] = "Galeria Adicionado com Sucesso";

$lang['asaas'] = "Asaas";
$lang['asaas_settings'] = "Configuração Integração Asaas";
$lang['account_token_asaas'] = "Token";
$lang['asaas_setting_updated'] = "Configuração Atualizada com Sucesso";

$lang['valepay_settings'] = "Configurar ValePay";
$lang['public_key'] = "Chave publica";
$lang['private_key'] = "Chave privada";
$lang['valepay_setting_updated'] = "Configurações ValePay atualizadas com sucesso";

$lang['numero_max_parcelas'] = "Número máximo de parcelas";
$lang['numero_max_parcelas_sem_juros'] = "Número máximo de parcelas sem juros";
$lang['cobrar_juros_todas_parcelas'] = "Cobrar Juros para todas as parcelas";

$lang['use_product_landing_page'] = 'Usar Página por Produto';

$lang['inf_head_code'] = 'Código no cabeçalho do site';
$lang['inf_body_code'] = 'Código no corpo do site';
$lang['inf_sections_code'] = 'Código inserido no final do site dentro da tag body';

$lang['head_code'] = 'Código inserido na parte inferior da seção de cabeçalho, logo acima da tag head de encerramento.';
$lang['body_code'] = 'Código inserido na parte inicial da seção de corpo, logo depois da tag body de abertura.';
$lang['sections_code'] = 'Código inserido na parte final da seção de corpo html, será inserido no final do site dentro da tag body.';

$lang['shop_settings'] = 'Config. Loja Virtual';

$lang['show_payment_report_shipment'] = 'Exibir Coluna Com Saldo "A Pagar" no Relatório de Embarque';
$lang['dependent_shipping_report'] = 'Ao Exibir a Coluna Com Saldo, Cobrar dos Dependentes.';

$lang['instrucoes_planta_onibus'] = 'Informações para Apresentação juntamente com o Layout do ônibus no Link de Reservas.';

$lang['usar_email_area_cliente'] = 'Usar E-mail Como Login da Área do Cliente';
$lang['usar_dtnascimento_area_cliente'] = 'Usar Data de Nascimento Como Login da Área do Cliente';

$lang['instrucoes_tipo_cobranca'] = 'Informações Apresentadas Junto aos Tipos de Cobrança no Link de Reservas.';
$lang['instrucoes_faixa_valores'] = 'Informações Apresentadas Junto as Faixas de Valores no Link de Reservas.';
$lang['instrucoes_local_embarque'] = 'Informações Apresentadas Junto aos Locais de Embarques no Link de Reservas.';
$lang['instrucoes_sinal_pagamento'] = 'Informações Apresentadas Junto as Formas de Pagamento Sinal no Link de Reservas.';

$lang['is_receptive'] = 'Usar Modulo do Receptivo';
$lang['ocultar_horario_saida'] = 'Ocultar Horário de Saída do Site';
$lang['filtar_local_embarque'] = 'Exibir o Filtro de de <b>Embarques</b> na Busca do Site';
$lang['captar_meio_divulgacao'] = 'Exibir no Link de Reserva os Meios de Divulgação ao Registrar uma Reserva';
$lang['ocultar_embarques_produto'] = 'Ocultar Locais de Embarque do Site (Página do Pacote)';
$lang['web_page_caching'] = 'Usar WEB PAGE CACHING (Cache do Site). Essa função acelera o carregamento do site (Para usar fale com o Administrador)';
$lang['cache_minutes'] = 'Tempo de Cache Em Minutos (Só é válido se o cache page estier ativo)';

$lang['tipo_sinal']  = 'Tipo de Sinal';
$lang['acrescimo_desconto_sinal'] = 'Tipo';
$lang['valor_acres_desc_sinal'] = 'Taxa do Sinal';
$lang['tipo_acres_desc_sinal'] = 'Em (%)/(R$)';
$lang['is_sinal'] = 'Habilitar Sinal';

$lang['mostrar_taxas'] = 'Mostar Taxa Administrativo no Link de Reservas';
$lang['is_avaliar'] = 'Habilitar Avaliação dos Serviços';
$lang['is_usar_captacao'] = 'Habilitar Captações no Link de Reservas';

$lang['despesas'] = 'Despesas';
$lang['tipo_cobranca'] = 'Tipo de Cobrança';
$lang['commission_settings'] = 'Config. de Comissão';

$lang['pagination_type'] = 'Tipo de Paginação';
$lang['number_packages_per_line'] = 'Número de Pacotes por Linha';