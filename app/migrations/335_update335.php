<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update335 extends CI_Migration {

    public function up() {

        $this->db->update('settings',  array('version' => '2021.12.0'), array('setting_id' => 1));
    }

    public function down() {}
}
