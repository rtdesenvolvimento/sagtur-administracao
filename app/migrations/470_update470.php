<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update470 extends CI_Migration {

    public function up() {

        $this->alter_table_message_groups();

        $this->db->update('settings',  array('version' => '4.7.0'), array('setting_id' => 1));
    }

    public function alter_table_message_groups()
    {
        $fields = array(
            'total_customers'   => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
            'imported_google'   => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
            'imported_whatsapp'   => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
            'total_customers_whatsapp'   => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
        );

        $this->dbforge->add_column('message_groups', $fields);
    }

    public function down() {}
}
