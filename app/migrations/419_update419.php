<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update419 extends CI_Migration {

    public function up() {

        $this->alter_table_sma_migrations();

        $this->db->update('settings',  array('version' => '4.1.9'), array('setting_id' => 1));
    }

    public function alter_table_sma_migrations() {
        $this->db->query('ALTER TABLE sma_migrations ADD id int unsigned auto_increment primary key first;');
    }

    public function down() {}
}
