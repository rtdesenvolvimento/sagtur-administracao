<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update316 extends CI_Migration {

    public function up() {

        //pacotes
        $this->create_table_local_embarque();
        $this->create_table_tipo_quarto();
        $this->create_table_destino();
        $this->create_table_local_embarque_viagem();
        $this->create_table_tipo_cobranca_produto();
        $this->create_table_valor_faixa_etaria();

        //financeiro
        $this->create_table_forma_pagamento();
        $this->create_table_condicao_pagamento();
        $this->create_table_despesa();
        $this->create_table_dre();
        $this->create_table_receita();
        $this->create_table_tipo_cobranca();

        $this->create_table_conta_receber();
        $this->create_table_conta_pagar();
        $this->create_table_parcela();
        $this->create_table_parcela_fatura();
        $this->create_table_fatura();
        $this->create_table_movimentador_saldo_periodo();
        $this->create_table_fatura_cobranca();

        //alteracao no cadastro de produtos
        $this->alter_table_product();
        $this->alter_table_movimentador();
        $this->alter_table_parcela_pagamento();
        $this->alter_table_configuracao();
        $this->alter_table_sale_items();

        //atualizar versao
        $this->db->update('settings',  array('version' => '2020.1'), array('setting_id' => 1));
    }

    function create_table_destino() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'endereco' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'latitude' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'longitude' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('destino', TRUE, $attributes);
    }

    function create_table_conta_pagar() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'product' => array('type' => 'INT', 'constraint' => 11 ),
            'pessoa' => array('type' => 'INT', 'constraint' => 11 ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'tipoCobranca' => array('type' => 'INT', 'constraint' => 11 ),
            'simbolo_moeda' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'despesa' => array('type' => 'INT', 'constraint' => 11 ),
            'condicaoPagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'dtvencimento' => array('type' => 'DATE', 'null' => TRUE ),
            'dtcompetencia' => array('type' => 'DATE', 'null' => TRUE ),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'desconto' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'conta_origem' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'warehouse' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'VARCHAR', 'constraint' => '10000', 'null' => TRUE ),
            'reference' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'palavra_chave' => array('type' => 'VARCHAR', 'constraint' => '1000', 'null' => TRUE ),
            'attachment' => array('type' => 'VARCHAR', 'constraint' => '1000', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('conta_pagar', TRUE, $attributes);
    }

    function create_table_conta_receber() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'product' => array('type' => 'INT', 'constraint' => 11 ),
            'sale' => array('type' => 'INT', 'constraint' => 11 ),
            'pessoa' => array('type' => 'INT', 'constraint' => 11 ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'tipoCobranca' => array('type' => 'INT', 'constraint' => 11 ),
            'simbolo_moeda' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'receita' => array('type' => 'INT', 'constraint' => 11 ),
            'conta_origem' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'condicaoPagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'dtvencimento' => array('type' => 'DATE', 'null' => TRUE ),
            'dtcompetencia' => array('type' => 'DATE', 'null' => TRUE ),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'desconto' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'numero_documento' => array('type' => 'VARCHAR', 'constraint' => '55' , 'null' => TRUE),
            'warehouse' => array('type' => 'INT', 'constraint' => 1 ),
            'note' => array('type' => 'VARCHAR', 'constraint' => '10000', 'null' => TRUE ),
            'reference' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'palavra_chave' => array('type' => 'VARCHAR', 'constraint' => '1000', 'null' => TRUE ),
            'attachment' => array('type' => 'VARCHAR', 'constraint' => '1000', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('conta_receber', TRUE, $attributes);
    }

    function create_table_parcela() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'pessoa' => array('type' => 'INT', 'constraint' => 11 ),
            'dtvencimento' => array('type' => 'DATE', 'null' => TRUE),
            'dtcompetencia' => array('type' => 'DATE', 'null' => TRUE),
            'dtultimopagamento' => array('type' => 'DATE', 'null' => TRUE),
            'indicecorrecao' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'totalAcrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'totalDesconto' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'percentualmulta' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'percentualmora' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'multa' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'mora' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'acrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'desconto' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'diasatraso' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),

            'valorpago' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valorpagar' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'numeroparcela' => array('type' => 'INT', 'constraint' => 11 ),
            'totalParcelas' => array('type' => 'INT', 'constraint' => 11 ),
            'contareceberId' => array('type' => 'INT', 'constraint' => 11 ),
            'contapagarId' => array('type' => 'INT', 'constraint' => 11 ),
            'tipocobranca' => array('type' => 'INT', 'constraint' => 11 ),
            'taxas' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'numero_documento' => array('type' => 'VARCHAR', 'constraint' => '55' , 'null' => TRUE),

            'despesa' => array('type' => 'INT', 'constraint' => 11 ),
            'receita' => array('type' => 'INT', 'constraint' => 11 ),
            'condicaoPagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'movimentador' => array('type' => 'INT', 'constraint' => 11 ),

            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'warehouse' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('parcela', TRUE, $attributes);
    }

    function create_table_parcela_fatura() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'parcela' => array('type' => 'INT', 'constraint' => 11 ),
            'fatura' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'warehouse' => array('type' => 'INT', 'constraint' => 11 )
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('parcela_fatura', TRUE, $attributes);
    }

    function create_table_fatura_cobranca() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'fatura' => array('type' => 'INT', 'constraint' => 11),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'code' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'integracao' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'dueDate' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'checkoutUrl' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'link' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'installmentLink' => array('type' => 'VARCHAR', 'constraint' => '900' ,  'null' => TRUE),
            'payNumber' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'bankAccount' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE),
            'ourNumber' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'barcodeNumber' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'portfolio' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55',  'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('fatura_cobranca', TRUE, $attributes);
    }

    function create_table_fatura() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'pessoa' => array('type' => 'INT', 'constraint' => 11 ),
            'code' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'dtvencimento' => array('type' => 'DATE', 'null' => TRUE ),
            'dtfaturamento' => array('type' => 'DATE', 'null' => TRUE),
            'dtultimopagamento' => array('type' => 'DATE', 'null' => TRUE),
            'totalAcrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'totalDesconto' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'numero_documento' => array('type' => 'VARCHAR', 'constraint' => '55' , 'null' => TRUE),
            'numero_transacao' => array('type' => 'VARCHAR', 'constraint' => '55' , 'null' => TRUE),

            'acrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'desconto' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'multa' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'mora' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'percentualmulta' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'percentualmora' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'strReceitas' => array('type' => 'VARCHAR', 'constraint' => '999',  'null' => TRUE ),
            'strDespesas' => array('type' => 'VARCHAR', 'constraint' => '999',  'null' => TRUE ),
            'tipoCobranca' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'motivo_cancelamento' => array('type' => 'VARCHAR', 'constraint' => '900',  'null' => TRUE ),
            'motivo_reembolso' => array('type' => 'VARCHAR', 'constraint' => '10000', 'null' => TRUE ),

            'totalcredito' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'totaldebito' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'tipooperacao' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'totalacrescimento' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'valorfatura' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valorpago' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valorpagar' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'taxas' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'warehouse' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'VARCHAR', 'constraint' => '999', 'null' => TRUE ),
            'reference' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),

            'diasatraso' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),

            'contas_receber' => array('type' => 'VARCHAR', 'constraint' => '900', 'null' => TRUE ),
            'contas_pagar' => array('type' => 'VARCHAR', 'constraint' => '900', 'null' => TRUE ),

            'despesa' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'receita' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'condicaoPagamento' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'movimentador' => array('type' => 'INT', 'constraint' => 11 ),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('fatura', TRUE, $attributes);
    }

    function alter_table_parcela_pagamento() {

        $fields = array(
            'fatura' => array('type' => 'INT', 'constraint' => 11 ),
            'pessoa' => array('type' => 'INT', 'constraint' => 11 ),
            'movimentador' => array('type' => 'INT', 'constraint' => 11 ),
            'formapagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'acrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'taxa' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'tipoCobranca' => array('type' => 'INT', 'constraint' => 1 ),
            'motivo_estorno' => array('type' => 'VARCHAR', 'constraint' => '10000', 'null' => TRUE )
        );
        $this->dbforge->add_column('payments', $fields);
    }

    function create_table_receita() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'code' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'receitasuperior' => array('type' => 'INT', 'constraint' => 11 ),
            'dre' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('receita', TRUE, $attributes);
    }

    function create_table_movimentador_saldo_periodo() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'entradas' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'saidas' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'saldo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'bloqueado' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'diferenca' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'formapagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'movimentador' => array('type' => 'INT', 'constraint' => 11 ),
            'dtmovimento' => array('type' => 'DATE', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('movimentador_saldo_periodo', TRUE, $attributes);
    }

    function create_table_dre() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'ordem' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('dre', TRUE, $attributes);
    }

    function create_table_despesa() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'code' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'despesasuperior' => array('type' => 'INT', 'constraint' => 11 ),
            'dre' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('despesa', TRUE, $attributes);
    }

    function create_table_tipo_cobranca() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'integracao' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'conta' => array('type' => 'INT', 'constraint' => 11 ),
            'formapagamento' => array('type' => 'INT', 'constraint' => 11, 'null' => TRUE),
            'faturar_automatico' => array('type' => 'VARCHAR', 'constraint' => '5' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_cobranca', TRUE, $attributes);
    }

    function create_table_forma_pagamento() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('forma_pagamento', TRUE, $attributes);
    }

    function create_table_condicao_pagamento() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'dias' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'parcelas' => array('type' => 'VARCHAR', 'constraint' => 11 ),
            'intervalo' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('condicao_pagamento', TRUE, $attributes);
    }

    function create_table_local_embarque_viagem() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'localEmbarque' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),
            'endereco' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'latitude' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'longitude' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'tolerancia' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
            'horaEmbarque' => array('type' => 'TIME'),
            'horaEmbarqueGeral' => array('type' => 'TIME'),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('local_embarque_viagem', TRUE, $attributes);
    }

    function create_table_local_embarque() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'endereco' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'latitude' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'longitude' => array('type' => 'VARCHAR', 'constraint' => '30' ),
            'tolerancia' => array('type' => 'INT', 'constraint' => 11 ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('local_embarque', TRUE, $attributes);
    }

    function create_table_tipo_cobranca_produto() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'tipoCobranca' => array('type' => 'INT', 'constraint' => 11 ),
            'formaPagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),
            'tipo' => array('type' => 'INT', 'constraint' => 11 ),
            'tipoValor' => array('type' => 'INT', 'constraint' => 11 ),
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_cobranca_produto', TRUE, $attributes);
    }

    function create_table_valor_faixa_etaria() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'tipo' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => TRUE ),
            'comissao' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'tipoComissao' => array('type' => 'INT', 'constraint' => 1 ),
            'idadeInicio' => array('type' => 'INT', 'constraint' => 11 ),
            'idadeFinal' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),
            'valor' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'note' => array('type' => 'VARCHAR', 'constraint' => '10000', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('valor_faixa_etaria', TRUE, $attributes);
    }

    function create_table_tipo_quarto() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '55' ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_quarto', TRUE, $attributes);
    }

    function alter_table_product() {
        $fields = array(
            'isTransporteTuristico' => array('type' => 'INT', 'constraint' => 1 ),
            'isHospedagem' => array('type' => 'INT', 'constraint' => 1 ),
            'isServicosAdicionais' => array('type' => 'INT', 'constraint' => 1 ),
            'isTaxasComissao' => array('type' => 'INT', 'constraint' => 1 ),
            'tipoComissao' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'tipoCalculoComissao' => array('type' => 'INT', 'constraint' => 1 ),
            'comissao' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'destinoId' => array('type' => 'INT', 'constraint' => 1 ),
        );
        $this->dbforge->add_column('products', $fields);
    }

    function alter_table_movimentador() {
        $fields = array(
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'caixa' => array('type' => 'INT', 'constraint' => 1 ),
            'pessoa' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
        );
        $this->dbforge->add_column('pos_register', $fields);
    }

    function alter_table_sale_items() {
        $fields = array(
            'contaReceberId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'contaPagarId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'fornecedorId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            //'colo' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'tipoCobrancaFornecedorId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'condicaoPagamentoFornecedorId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'valorRecebimentoValor' => array( 'type' => 'DECIMAL', 'constraint' => '25,4' ),
            'product_comissao' => array( 'type' => 'DECIMAL', 'constraint' => '25,4' ),
        );
        $this->dbforge->add_column('sale_items', $fields);
    }

    function alter_table_categorie() {
        $fields = array(
            'despesaId' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('categorie', $fields);
    }

    function alter_table_configuracao() {
        $fields = array(
            'receitaNecociacao' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'percentualMulta' => array( 'type' => 'DECIMAL', 'constraint' => '25,4' ),
            'percentualJurosMoraDia' => array( 'type' => 'DECIMAL', 'constraint' => '25,4' ),
            'tipoCobrancaAVista' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'tipoCobrancaBoleto' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'condicaoPagamentoAVista' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'dinheiro' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'receitaPadrao' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'receitaTransferencia' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
            'despesaTransferencia' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
