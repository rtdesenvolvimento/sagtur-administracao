<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update494 extends CI_Migration {

    public function up() {

        $atualizar_banco_dados = true;//todo apenas para o admin

        $this->ajustar_gallery();

        $this->alter_table_companies();
        $this->alter_table_valor_faixa();

        if ($atualizar_banco_dados) {
            $this->atualizar_banco_dados_administracao();
        }

        $this->db->update('settings',  array('version' => '4.9.4'), array('setting_id' => 1));
    }

    public function atualizar_banco_dados_administracao()
    {
        $this->db->query('UPDATE sma_companies SET nome_responsavel = company;');
        $this->db->query('UPDATE sma_companies SET company = name;');
        $this->db->query('UPDATE sma_companies SET data_contratacao = data_aniversario;');
    }

    public function ajustar_gallery()
    {
        $sql = "delete from sma_gallery_item where gallery_id not in (select id from sma_gallery)";
        $this->db->query($sql);
    }

    public function alter_table_companies() {
        $fields = array(
            'data_contratacao' => array('type' => 'DATE', 'null' => TRUE ),
            'data_cancelamento' => array('type' => 'DATE', 'null' => TRUE ),
            'nome_responsavel' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE , 'default' => ''),
        );
        $this->dbforge->add_column('companies', $fields);
    }

    public function alter_table_valor_faixa() {
        $fields = array(
            'formato_cpf' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
        );
        $this->dbforge->add_column('valor_faixa', $fields);
    }

    public function down() {}
}
