<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update368 extends CI_Migration {

    public function up() {

        $this->create_table_assas();

        $this->db->update('settings',  array('version' => '3.6.8'), array('setting_id' => 1));
    }

    public function create_table_assas() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'token' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('assas', TRUE, $attributes);

        $data_assas = array(
            'token' => '#',
            'note' => 'Integração SAGTur com Assas - Boleto, Carnê (Parcelamento) e Pix',
        );
        $this->db->insert('assas', $data_assas);
    }

    public function down() {}
}
