<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update471 extends CI_Migration {

    public function up() {

        $this->alter_table_contract_settings();

        $this->db->update('settings',  array('version' => '4.7.1'), array('setting_id' => 1));
    }

    public function alter_table_contract_settings()
    {
        $fields = array(
            'cancel_contract_by_sale' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('contract_settings', $fields);
    }

    public function down() {}
}
