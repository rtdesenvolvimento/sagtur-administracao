<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update379 extends CI_Migration {

    public function up() {

        $this->insert_tempo();

        $this->db->update('settings',  array('version' => '3.7.9'), array('setting_id' => 1));
    }

    public function insert_tempo() {
        $this->db->query('delete from sma_tempo');

        // Definição das datas inicial e final
        $data_inicial = '2022-01-01';
        $data_final = '2024-12-31';

        for ($data = $data_inicial; $data <= $data_final; $data = date('Y-m-d', strtotime($data . ' +1 day'))) {
            $sql = "INSERT INTO sma_tempo (data) VALUES ('$data')";
            $this->db->query($sql);
        }

        $this->db->query('CREATE INDEX indxs_tempo_data ON sma_tempo (data)');

    }

    public function down() {}
}
