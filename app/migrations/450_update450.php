<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update450 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.5.0'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'permite_pagamento_total' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
