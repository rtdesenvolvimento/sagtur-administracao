<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update317 extends CI_Migration {

    public function up() {

        $this->alter_table_product();
        $this->alter_table_settings();
        $this->create_table_product_photos_site();
        $this->alter_table_agendamento_viagem();

        $this->atualizar_table_product();

        //atualizar versao
        $this->db->update('settings',  array('version' => '2021.1'), array('setting_id' => 1));
    }

    function atualizar_table_product() {
        $this->db->set('valor_pacote', 'A partir de');
        $this->db->update('products');

        $this->db->set('simboloMoeda', 'R$');
        $this->db->update('products');

        $this->db->set('enviar_site', '1');
        $this->db->update('products');

        $this->db->set('isServicoOnline', 1);
        $this->db->update('products');

        $this->db->set('apenas_cotacao', 'nao');
        $this->db->update('products');

        $this->db->set('alertar_polcas_vagas', 10);
        $this->db->update('products');

        $this->db->set('alertar_ultimas_vagas', 5);
        $this->db->update('products');

        $this->db->set('precoExibicaoSite', 'price', FALSE);
        $this->db->update('products');

        $this->db->set('isIntegracaoSite', 0);
        $this->db->update('settings');

        $this->db->set('frase_site', '"Seu Sonho. Sua Viagem. Nós Realizamos."');
        $this->db->update('settings');
    }

    function create_table_product_photos_site() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'product_id' => array('type' => 'INT', 'constraint' => 1 ),
            'post_id' => array('type' => 'INT', 'constraint' => 1 ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_photos_site', TRUE, $attributes);
    }

    function alter_table_settings() {

        $fields = array(
            'frase_site' => array(
                'name' => 'frase_site',
                'type' => 'LONGTEXT',
            ),
        );
        $this->dbforge->modify_column('settings', $fields);

        $fields = array(
            'isIntegracaoSite' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0 ),
            'url_site' => array('type' => 'LONGTEXT'  ),
            'upload_path_site' => array('type' => 'LONGTEXT'  ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    function alter_table_product() {

        $fields = array(
            'origem' => array(
                'name' => 'origem',
                'type' => 'LONGTEXT',
            ),
            'details' => array(
                'name' => 'details',
                'type' => 'LONGTEXT',
            ),
            'itinerario' => array(
                'name' => 'itinerario',
                'type' => 'LONGTEXT',
            ),
            'oqueInclui' => array(
                'name' => 'oqueInclui',
                'type' => 'LONGTEXT',
            ),
            'valores_condicoes' => array(
                'name' => 'valores_condicoes',
                'type' => 'LONGTEXT',
            )
        );
        $this->dbforge->modify_column('products', $fields);

        $fields = array(
            'isServicoOnline' => array('type' => 'INT', 'constraint' => 1 ),
            'precoExibicaoSite' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'simboloMoeda' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => 'R$' ),
            'products_image_site_id' => array('type' => 'INT', 'constraint' => 1 ),
        );
        $this->dbforge->add_column('products', $fields);
    }

    function alter_table_agendamento_viagem() {
        $fields = array(
            'agenda_site_id' => array('type' => 'INT', 'constraint' => 1 ),
        );
        $this->dbforge->add_column('agenda_viagem', $fields);
    }

    public function down() {}
}
