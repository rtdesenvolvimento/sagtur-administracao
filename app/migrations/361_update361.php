<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update361 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.6.1'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'qty_client_records' => array('type' => 'INT', 'constraint' => 11 , 'default' => 10),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
