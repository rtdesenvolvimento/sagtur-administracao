<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update473 extends CI_Migration {

    public function up() {

        $this->create_email_queue();

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.7.3'), array('setting_id' => 1));
    }

    public function create_email_queue() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'to' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => FALSE),
            'subject' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => FALSE),
            'body' => array('type' => 'LONGTEXT', 'default' => '', 'null' => FALSE),
            'from' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => TRUE),
            'from_name' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => TRUE),
            'attachment' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '', 'null' => TRUE),
            'cc' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => TRUE),
            'bcc' => array('type' => 'VARCHAR', 'constraint' => '255', 'default' => '', 'null' => TRUE),
            'status'    => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => 'pending'),
            'sent_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'email_origin' => array('type' => 'VARCHAR', 'constraint' => '55', 'default' => '', 'null' => TRUE),
            'function_origin' => array('type' => 'VARCHAR', 'constraint' => '55', 'default' => '', 'null' => TRUE),
            'object_id' => array('type' => 'INT', 'constraint' => 11 ),
            'attempts'   => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
            'error' => array('type' => 'LONGTEXT', 'default' => '', 'null' => TRUE),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('email_queue', TRUE, $attributes);
    }

    public function alter_table_settings() {
        $fields = array(
            'is_email_queue' => array('type' => 'INT', 'constraint' => 1, 'default' => 1),
        );

        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('is_email_queue' => 0), array('setting_id' => 1));
    }

    public function down() {}
}
