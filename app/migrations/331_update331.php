<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update331 extends CI_Migration {

    public function up() {

        $this->update_table_settings();

        $this->db->update('settings',  array('version' => '2021.4.3'), array('setting_id' => 1));
    }

    function update_table_settings() {
        $this->db->update('settings',
            array(
                'pixelFacebook'   => '818930862132772',//pixel do nosso Facebook do SAGTur
            ), array('setting_id' => 1));
    }

    public function down() {}
}
