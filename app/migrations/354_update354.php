<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update354 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.5.4'), array('setting_id' => 1));
    }

    function alter_table_settings() {
        $this->db->update('settings',
            array(
                'pixelFacebook'   => '290030321740819',//pixel do nosso Facebook
            ), array('setting_id' => 1));
    }

    public function down() {}
}
