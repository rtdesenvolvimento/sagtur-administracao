<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update467 extends CI_Migration {

    public function up() {

        $this->create_table_messages();
        $this->create_table_message_groups();
        $this->create_table_message_group_participants();

        $this->db->update('settings',  array('version' => '4.6.7'), array('setting_id' => 1));
    }

    function create_table_messages() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'type' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE, 'default' => 'text' ),
            'contact' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'message' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('messages', TRUE, $attributes);
    }

    function create_table_message_groups() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'name_phone' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),

            'invitationLink' => array('type' => 'VARCHAR', 'constraint' => '999' , 'null' => TRUE ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('message_groups', TRUE, $attributes);
    }

    function create_table_message_group_participants() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'contact' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => TRUE ),
            'phone' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => TRUE ),
            'message_group_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE ),
            'administrador' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),

            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
            'update_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'update_by' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('message_group_participants', TRUE, $attributes);
    }

    public function down() {}
}
