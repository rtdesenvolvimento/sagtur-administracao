<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update353 extends CI_Migration {

    public function up() {
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.5.3'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'own_domain' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
