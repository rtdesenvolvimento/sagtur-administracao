<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update410 extends CI_Migration {

    public function up() {

        $this->db->update('products',  array('isTaxasComissao' => false));
        $this->db->update('settings',  array('version' => '4.1.0'), array('setting_id' => 1));
    }

    public function down() {}
}
