<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update391 extends CI_Migration {

    public function up() {
        $this->db->update('settings',  array('version' => '3.9.1', 'show_payment_report_shipment' => 0), array('setting_id' => 1));
    }

    public function down() {}
}
