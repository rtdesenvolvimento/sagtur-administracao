<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update358 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->create_table_testimonial();
        $this->create_table_team_site();
        $this->create_table_gallery();

        $this->db->update('settings',  array('version' => '3.5.8'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'tiktok' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

            'horario_atendimento' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => 'Seg - Sáb' ),
            'about_photo' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => 'about.jpg' ),
            'about_photo1' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),
            'about_photo2' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

            'slider_button' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

            'slider1' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),
            'slider1_slogan' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '' ),
            'slider1_button' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

            'slider2' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),
            'slider2_slogan' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '' ),
            'slider2_button' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

            'slider3' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),
            'slider3_slogan' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '' ),
            'slider3_button' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),

        );
        $this->dbforge->add_column('settings', $fields);
    }

    function create_table_gallery() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('gallery', TRUE, $attributes);
    }

    function create_table_testimonial() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'profession' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'testimonial' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('testimonial', TRUE, $attributes);
    }

    function create_table_team_site() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'office' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'facebook' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'instagram' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'testimonial' => array('type' => 'LONGTEXT', 'default' => ''),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100' ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('team_site', TRUE, $attributes);
    }

    public function down() {}
}
