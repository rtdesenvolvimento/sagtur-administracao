<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update422 extends CI_Migration {

    public function up() {

        $this->atualizar_vendas_com_pagamentos_duplicados();

        $this->db->update('settings',  array('version' => '4.2.2'), array('setting_id' => 1));
    }

    public function atualizar_vendas_com_pagamentos_duplicados()
    {
        $sql = 'SELECT sma_payments.id as payment_id, sma_payments.sale_id, sma_payments.fatura, sma_payments.approval_code, sma_payments.taxa, COUNT(*) as quantidade_duplicados
                FROM sma_payments, sma_sales 
                WHERE sma_payments.sale_id = sma_sales.id AND approval_code IN (
                    SELECT approval_code
                    FROM sma_payments
                    GROUP BY approval_code
                    HAVING COUNT(*) > 1
                ) AND sma_sales.sale_status = "faturada"
                GROUP BY approval_code;';

        $payments_duplicados = $this->db->query($sql);

        if ($payments_duplicados->num_rows() > 0) {
            foreach (($payments_duplicados->result()) as $row) {

                $this->deltar_pagamento_duplicado($row->payment_id);

                $this->atualizar_fatura($row->sale_id, $row->fatura, $row->taxa);

                $this->atualizar_venda($row->sale_id);
            }
        }

        return true;
    }

    public function deltar_pagamento_duplicado($payment_id)
    {
        $this->db->query("DELETE FROM sma_payments WHERE id =".$payment_id);
    }

    public  function atualizar_fatura($sale_id, $fatura_id, $taxas)
    {
        $sql = "SELECT id FROM `sma_fatura` WHERE sale_id =".$sale_id." AND id =".$fatura_id;

        $atualizar_fatura_dupilcada = $this->db->query($sql);

        if ($atualizar_fatura_dupilcada->num_rows() > 0) {
            $row = $atualizar_fatura_dupilcada->row();

            $sql_uppdate_fatura = "UPDATE sma_fatura SET valorpagar = 0, valorpago =  valorfatura, taxas = ".$taxas." WHERE id = ".$row->id;
            $this->db->query($sql_uppdate_fatura);
        }
    }

    public function atualizar_venda($sale_id)
    {
        $payments = $this->getInvoicePayments($sale_id);
        $total_pago = 0;
        $taxas = 0;

        foreach ($payments as $payment) {
            if ($payment->status == 'ESTORNO') continue;

            $total_pago = $total_pago + $payment->amount;
            $taxas = $taxas + $payment->taxa;
        }

        $total_pago = $total_pago + $taxas;

        $sql_update_sale = "UPDATE sma_sales SET paid = ".$total_pago.", payment_status = IF('$total_pago' >= grand_total, 'paid', 'partial') WHERE id = ".$sale_id;

        $this->db->query($sql_update_sale);
    }

    public function getInvoicePayments($sale_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function down() {}
}
