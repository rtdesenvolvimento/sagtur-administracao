<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update402 extends CI_Migration {

    public function up() {

        $this->db->update('settings',  array('version' => '4.0.2', 'site_width' => 320), array('setting_id' => 1));
    }

    public function down() {}
}
