<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update435 extends CI_Migration {

    public function up() {

        $this->insert_despesa_multa();

        $this->db->update('settings',  array('version' => '4.3.4'), array('setting_id' => 1));
    }

    public function insert_despesa_multa() {
        $data_despesa = array(
            'name' => 'MULTA DE CANCELAMENTO',
            'code' => 'MULTA DE CANCELAMENTO',
            'tipo' => 'subgrupo',
            'despesasuperior' => 18,
        );

        $this->db->insert('despesa', $data_despesa);
        $despesa_id = $this->db->insert_id();

        $fields = array(
            'receita_padrao_multa_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('receita_padrao_multa_id' => $despesa_id), array('setting_id' => 1));
    }

    public function down() {}
}
