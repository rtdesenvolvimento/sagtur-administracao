<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update330 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '2021.4.2', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));
    }

    function alter_table_settings() {
        $fields = array(
            'pixelFacebook' => array('type' => 'VARCHAR', 'constraint' => '999', 'null' => TRUE ),
            'googleAnalytics' => array('type' => 'VARCHAR', 'constraint' => '999', 'null' => TRUE ),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',
            array(
                'pixelFacebook'   => '290030321740819',//pixel do nosso Facebook
                'googleAnalytics' => 'UA-165016020-1',//nosso google analytics
            ), array('setting_id' => 1));
    }

    public function down() {}
}
