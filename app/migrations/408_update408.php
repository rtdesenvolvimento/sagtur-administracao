<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update408 extends CI_Migration {

    public function up() {

        $this->drop_table();

        $this->create_table_configuracao_cobranca_extra_assento();
        $this->create_table_configuracao_cobranca_extra_assento_marcacao();
        $this->alter_table_tipo_transporte_rodoviario_viagem();

        $this->db->update('settings',  array('version' => '4.0.8'), array('setting_id' => 1));
    }

    public function drop_table() {
        $this->db->query('DROP TABLE sma_assento_cobranca_extra');
        $this->db->query('DROP TABLE sma_assento_cobranca_extra_base');

    }

    public function create_table_configuracao_cobranca_extra_assento() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'tipo_transporte_id' => array('type' => 'INT', 'constraint' => 11 ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('configuracao_cobranca_extra_assento', TRUE, $attributes);
    }

    public function create_table_configuracao_cobranca_extra_assento_marcacao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'configuracao_extra_id' => array('type' => 'INT', 'constraint' => 11 ),
            'assento' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'valor' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'andar' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('configuracao_cobranca_extra_assento_marcacao', TRUE, $attributes);
    }

    public function alter_table_tipo_transporte_rodoviario_viagem() {
        $fields = array(
            'configuracao_assento_extra_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE ),
        );

        $this->dbforge->add_column('tipo_transporte_rodoviario_viagem', $fields);
    }


    public function down() {}
}
