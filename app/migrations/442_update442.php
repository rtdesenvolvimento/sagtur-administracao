<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update442 extends CI_Migration {

    public function up() {

        $this->alter_table_taxas_venda_configuracao();

        $this->db->update('settings',  array('version' => '4.4.2'), array('setting_id' => 1));
    }

    public function alter_table_taxas_venda_configuracao()
    {

        $fields = array(
            'acrescimo_desconto_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'acrescimo' ),
            'valor_acres_desc_sinal' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'tipo_acres_desc_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'percentual' ),
        );
        $this->dbforge->add_column('taxas_venda_configuracao', $fields);
    }

    public function down() {}
}
