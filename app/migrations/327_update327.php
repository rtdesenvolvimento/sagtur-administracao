<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update327 extends CI_Migration {

    public function up() {

        $this->atualizar_juno();
        $this->atualizar_pagseguro();
        $this->inserir_formas_pagamento();
        $this->alter_table_tipo_cobranca();

        $this->db->update('settings',  array('version' => '2021.3.27', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));
    }

    function alter_table_tipo_cobranca() {
        $fields = array(
            'tipoExibir' => array('type' => 'VARCHAR', 'constraint' => '50' , 'default' => 'receita' ),
        );
        $this->dbforge->add_column('tipo_cobranca', $fields);
    }

    public function atualizar_juno () {
        $this->db->update('juno',  array('sandbox' => '0'), array('id' => 1));
    }

    public function atualizar_pagseguro() {
        $this->db->update('pagseguro',  array('sandbox' => '0'), array('id' => 1));
    }

    public function inserir_formas_pagamento() {
        $data_cheque = array(
            'name' => 'CHEQUE',
            'status' => 'Ativo'
        );

        $data_credito_loja = array(
            'name' => 'CREDITO LOJA',
            'status' => 'Ativo'
        );

        $data_boleto = array(
            'name' => 'BOLETO',
            'status' => 'Ativo'
        );

        $this->db->insert('forma_pagamento', $data_cheque);
        $this->db->insert('forma_pagamento', $data_credito_loja);
        $this->db->insert('forma_pagamento', $data_boleto);
    }

    public function down() {}
}
