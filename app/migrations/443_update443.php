<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update443 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();
        $this->alter_table_taxas_venda_configuracao();
        $this->alter_table_sales();

        $this->db->update('settings',  array('version' => '4.4.3'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'mostrar_taxas' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function alter_table_sales()
    {
        $fields = array(
            'valor_sinal' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'tipo_cobranca_sinal' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('sales', $fields);
    }

    public function alter_table_taxas_venda_configuracao()
    {
        $fields = array(
            'is_sinal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('taxas_venda_configuracao', $fields);
    }

    public function down() {}
}
