<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update399 extends CI_Migration {

    public function up() {

        $this->base_onibus_1_andares_primeiro_andar();
        $this->base_onibus_2_andares_primeiro_andar();

        $this->db->update('settings',  array('version' => '3.9.9'), array('setting_id' => 1));
    }

    function base_onibus_2_andares_primeiro_andar() {

        $this->db->insert('automovel', array('name' => 'MAPA BASE ÔNIBUS 2 ANDARES', 'andares' => 2, 'total_assentos' => 112));

        $automovel_id = $this->db->insert_id();

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 7, 'name' => '07', 'assento' => 7, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 8, 'name' => '08', 'assento' => 8, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 9, 'name' => '09', 'assento' => 9, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 10, 'name' => '10', 'assento' => 10, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 11, 'name' => '11', 'assento' => 11, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 12, 'name' => '12', 'assento' => 12, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 13, 'name' => '13', 'assento' => 13, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 14, 'name' => '14', 'assento' => 14, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 15, 'name' => '15', 'assento' => 15, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 16, 'name' => '16', 'assento' => 16, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 17, 'name' => '17', 'assento' => 17, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 18, 'name' => '18', 'assento' => 18, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 19, 'name' => '19', 'assento' => 19, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 20, 'name' => '20', 'assento' => 20, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 21, 'name' => '21', 'assento' => 21, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 22, 'name' => '22', 'assento' => 22, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 23, 'name' => '23', 'assento' => 23, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 24, 'name' => '24', 'assento' => 24, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 25, 'name' => '25', 'assento' => 25, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 26, 'name' => '26', 'assento' => 26, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 27, 'name' => '27', 'assento' => 27, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 28, 'name' => '28', 'assento' => 28, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 29, 'name' => '29', 'assento' => 29, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 30, 'name' => '30', 'assento' => 30, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 31, 'name' => '31', 'assento' => 31, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 32, 'name' => '32', 'assento' => 32, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 33, 'name' => '33', 'assento' => 33, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 34, 'name' => '34', 'assento' => 34, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 35, 'name' => '35', 'assento' => 35, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 36, 'name' => '36', 'assento' => 36, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 37, 'name' => '37', 'assento' => 37, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 38, 'name' => '38', 'assento' => 38, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 39, 'name' => '39', 'assento' => 39, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 40, 'name' => '40', 'assento' => 40, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 41, 'name' => '41', 'assento' => 41, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 42, 'name' => '42', 'assento' => 42, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 43, 'name' => '43', 'assento' => 43, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 44, 'name' => '44', 'assento' => 44, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 45, 'name' => '45', 'assento' => 45, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 46, 'name' => '46', 'assento' => 46, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 47, 'name' => '47', 'assento' => 47, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 48, 'name' => '48', 'assento' => 48, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 49, 'name' => '49', 'assento' => 49, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 50, 'name' => '50', 'assento' => 50, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 51, 'name' => '51', 'assento' => 51, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 52, 'name' => '52', 'assento' => 52, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 53, 'name' => '53', 'assento' => 53, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 54, 'name' => '54', 'assento' => 54, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 55, 'name' => '55', 'assento' => 55, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 56, 'name' => '56', 'assento' => 56, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => true));


        $this->base_onibus_2_andares_segundo_andar($automovel_id);
    }

    function base_onibus_2_andares_segundo_andar($automovel_id) {

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 7, 'name' => '07', 'assento' => 7, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 8, 'name' => '08', 'assento' => 8, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 9, 'name' => '09', 'assento' => 9, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 10, 'name' => '10', 'assento' => 10, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 11, 'name' => '11', 'assento' => 11, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 12, 'name' => '12', 'assento' => 12, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 13, 'name' => '13', 'assento' => 13, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 14, 'name' => '14', 'assento' => 14, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 15, 'name' => '15', 'assento' => 15, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 16, 'name' => '16', 'assento' => 16, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 17, 'name' => '17', 'assento' => 17, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 18, 'name' => '18', 'assento' => 18, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 19, 'name' => '19', 'assento' => 19, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 20, 'name' => '20', 'assento' => 20, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 21, 'name' => '21', 'assento' => 21, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 22, 'name' => '22', 'assento' => 22, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 23, 'name' => '23', 'assento' => 23, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 24, 'name' => '24', 'assento' => 24, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 25, 'name' => '25', 'assento' => 25, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 26, 'name' => '26', 'assento' => 26, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 27, 'name' => '27', 'assento' => 27, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 28, 'name' => '28', 'assento' => 28, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 29, 'name' => '29', 'assento' => 29, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 30, 'name' => '30', 'assento' => 30, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 31, 'name' => '31', 'assento' => 31, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 32, 'name' => '32', 'assento' => 32, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 33, 'name' => '33', 'assento' => 33, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 34, 'name' => '34', 'assento' => 34, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 35, 'name' => '35', 'assento' => 35, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 36, 'name' => '36', 'assento' => 36, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 37, 'name' => '37', 'assento' => 37, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 38, 'name' => '38', 'assento' => 38, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 39, 'name' => '39', 'assento' => 39, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 40, 'name' => '40', 'assento' => 40, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 41, 'name' => '41', 'assento' => 41, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 42, 'name' => '42', 'assento' => 42, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 43, 'name' => '43', 'assento' => 43, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 44, 'name' => '44', 'assento' => 44, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 45, 'name' => '45', 'assento' => 45, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 46, 'name' => '46', 'assento' => 46, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 47, 'name' => '47', 'assento' => 47, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 48, 'name' => '48', 'assento' => 48, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 49, 'name' => '49', 'assento' => 49, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 50, 'name' => '50', 'assento' => 50, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 51, 'name' => '51', 'assento' => 51, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 52, 'name' => '52', 'assento' => 52, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 53, 'name' => '53', 'assento' => 53, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 54, 'name' => '54', 'assento' => 54, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 55, 'name' => '55', 'assento' => 55, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 2, 'ordem' => 56, 'name' => '56', 'assento' => 56, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));
    }

    function base_onibus_1_andares_primeiro_andar() {

        $this->db->insert('automovel', array('name' => 'MAPA BASE ÔNIBUS 1 ANDAR', 'andares' => 1, 'total_assentos' => 54));

        $automovel_id = $this->db->insert_id();

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 7, 'name' => '07', 'assento' => 7, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 8, 'name' => '08', 'assento' => 8, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 9, 'name' => '09', 'assento' => 9, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 10, 'name' => '10', 'assento' => 10, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 11, 'name' => '11', 'assento' => 11, 'x' => 2, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 12, 'name' => '12', 'assento' => 12, 'x' => 2, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 13, 'name' => '13', 'assento' => 13, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 14, 'name' => '14', 'assento' => 14, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 15, 'name' => '15', 'assento' => 15, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 16, 'name' => '16', 'assento' => 16, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 17, 'name' => '17', 'assento' => 17, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 18, 'name' => '18', 'assento' => 18, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 19, 'name' => '19', 'assento' => 19, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 20, 'name' => '20', 'assento' => 20, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 21, 'name' => '21', 'assento' => 21, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 22, 'name' => '22', 'assento' => 22, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 23, 'name' => '23', 'assento' => 23, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 24, 'name' => '24', 'assento' => 24, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 25, 'name' => '25', 'assento' => 25, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 26, 'name' => '26', 'assento' => 26, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 27, 'name' => '27', 'assento' => 27, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 28, 'name' => '28', 'assento' => 28, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 29, 'name' => '29', 'assento' => 29, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 30, 'name' => '30', 'assento' => 30, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 31, 'name' => '31', 'assento' => 31, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 32, 'name' => '32', 'assento' => 32, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 33, 'name' => '33', 'assento' => 33, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 34, 'name' => '34', 'assento' => 34, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 35, 'name' => '35', 'assento' => 35, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 36, 'name' => '36', 'assento' => 36, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 37, 'name' => '37', 'assento' => 37, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 38, 'name' => '38', 'assento' => 38, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 39, 'name' => '39', 'assento' => 39, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 40, 'name' => '40', 'assento' => 40, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 41, 'name' => '41', 'assento' => 41, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 42, 'name' => '42', 'assento' => 42, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 43, 'name' => '43', 'assento' => 43, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 44, 'name' => '44', 'assento' => 44, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 45, 'name' => '45', 'assento' => 45, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 46, 'name' => '46', 'assento' => 46, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 47, 'name' => '47', 'assento' => 47, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 48, 'name' => '48', 'assento' => 48, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 49, 'name' => '49', 'assento' => 49, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 50, 'name' => '50', 'assento' => 50, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 51, 'name' => '51', 'assento' => 51, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 52, 'name' => '52', 'assento' => 52, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 53, 'name' => '53', 'assento' => 53, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 54, 'name' => '54', 'assento' => 54, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 55, 'name' => '55', 'assento' => 55, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => $automovel_id, 'andar' => 1, 'ordem' => 56, 'name' => '56', 'assento' => 56, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => true));

    }


    public function down() {

    }
}
