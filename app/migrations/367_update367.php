<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update367 extends CI_Migration {

    public function up() {

        $this->create_table_cobre_facil();
        $this->alter_table_fatura_cobranca();
        $this->alter_table_customer_groups();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.6.6'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'modoCobrancaMulta' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'percentage'),
            'modoCobrancaJurosMora' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'daily_percentage' ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_customer_groups() {
        $fields = array(
            'products_services_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'plans_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'payable_with' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
        );
        $this->dbforge->add_column('customer_groups', $fields);
    }

    public function alter_table_fatura_cobranca() {
        $fields = array(
            'booklets_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'subscriptions_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
        );
        $this->dbforge->add_column('fatura_cobranca', $fields);
    }

    public function create_table_cobre_facil() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'app_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'app_secret' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'notification_rule_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sandbox' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cobre_facil', TRUE, $attributes);

        $data_cobre_facil = array(
            'app_id' => '#',
            'app_secret' => '',
            'note' => 'Integração SAGTur com CobreFacil',
        );
        $this->db->insert('cobre_facil', $data_cobre_facil);
    }

    public function down() {}
}
