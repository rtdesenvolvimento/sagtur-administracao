<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update433 extends CI_Migration {

    public function up() {

        $this->update_condicao_pagamento();

        $this->db->update('settings',  array('version' => '4.3.3'), array('setting_id' => 1));
    }


    public function update_condicao_pagamento()
    {
        $this->db->update('settings',  array('condicaoPagamentoAVista' =>  1), array('setting_id' => 1));

    }

    public function down() {}
}
