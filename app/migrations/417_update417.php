<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update417 extends CI_Migration {

    public function up() {

        $this->recreate_index();

        $this->db->update('settings',  array('version' => '4.1.7'), array('setting_id' => 1));
    }

    public function recreate_index() {
        $this->db->query('CREATE INDEX idxs_sma_menu_search ON sma_menu (active, type, name);');
        $this->db->query('CREATE INDEX idxs_sma_categories_name ON sma_categories (name);');
        $this->db->query('CREATE INDEX idxs_sma_local_embarque_name ON sma_local_embarque (name);');
        $this->db->query('CREATE INDEX idxs_sma_meio_divulgacao_active ON sma_meio_divulgacao (active);');
        $this->db->query('CREATE INDEX idxs_sma_currencies_code ON sma_currencies (code);');

        $this->db->query('CREATE INDEX idxs_sma_taxas_venda_configuracao_tipo_cobranca_id ON sma_taxas_venda_configuracao (tipoCobrancaId);');
        $this->db->query('CREATE INDEX idxs_sma_taxas_venda_configuracao_ativo ON sma_taxas_venda_configuracao (ativo);');
        $this->db->query('CREATE INDEX idxs_sma_tipo_cobranca_status_exibirLinkCompras ON sma_tipo_cobranca (status, exibirLinkCompras);');
        $this->db->query('CREATE INDEX idxs_sma_tipo_cobranca_name ON sma_tipo_cobranca (name);');

    }

    public function down() {}
}
