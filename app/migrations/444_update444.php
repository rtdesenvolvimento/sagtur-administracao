<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update444 extends CI_Migration {

    public function up() {

        $this->alter_table_tipo_cobranca_produto();

        $this->db->update('settings',  array('version' => '4.4.4'), array('setting_id' => 1));
    }

    public function alter_table_tipo_cobranca_produto()
    {
        $fields = array(
            'sinal' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'tipo_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'percentual' ),
            'acrescimo_desconto_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'acrescimo' ),
            'valor_acres_desc_sinal' => array( 'type' => 'DECIMAL', 'constraint' => '25,2', 'default' => 0 ),
            'tipo_acres_desc_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'percentual' ),
            'is_sinal' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('tipo_cobranca_produto', $fields);
    }

    public function down() {}
}
