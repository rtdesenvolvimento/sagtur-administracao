<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update446 extends CI_Migration {

    public function up() {


        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.4.6'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'hospedagem_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
        );
        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
