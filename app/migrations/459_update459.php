<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update459 extends CI_Migration {

    public function up() {

        $this->atualizar_faixa_guia();

        $this->alter_table_commission_settings();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.5.9'), array('setting_id' => 1));
    }

    public function alter_table_commission_settings() {
        $fields = array(
            'cancelar_venda_comissao_aprovada' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//permite cancelar venda com comissao aprovada (paga)
        );

        $this->dbforge->add_column('commission_settings', $fields);
    }

    public function alter_table_settings() {
        $fields = array(
            'open_excel_disponibilidade' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function atualizar_faixa_guia()
    {
        $guia = $this->getFaixaGuia();

        if ($guia) {
            $this->db->update('valor_faixa', array('descontarVaga' => true), array('id' => $guia->id));
        }
    }
    public function getFaixaGuia()
    {
        $q = $this->db->get_where("valor_faixa", array('name' => 'Guia'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function down() {}
}
