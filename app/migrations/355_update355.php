<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update355 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '3.5.5'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'theme_color' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '#e58800' ),
            'theme_color_secondary' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '#e58800' ),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
