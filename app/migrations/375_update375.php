<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update375 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '3.7.5'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'alert_stripe' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
        );
        $this->dbforge->add_column('products', $fields);
    }


    public function down() {}
}
