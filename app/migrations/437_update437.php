<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update437 extends CI_Migration {

    public function up() {

        $this->insert_user_groups();

        $this->db->update('settings',  array('version' => '4.3.7'), array('setting_id' => 1));
    }

    public function insert_user_groups()
    {
        $data = array(
            'id'            => 7,
            'name'          => 'checkin',
            'description'   => 'CHECK-IN/OUT',
        );

        if ($this->db->insert("groups", $data)) {
            $this->db->insert('permissions', array('group_id' => 7));
        }
    }

    public function down() {}
}
