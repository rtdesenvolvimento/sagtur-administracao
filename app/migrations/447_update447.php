<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update447 extends CI_Migration {

    public function up() {

        $this->create_table_ratings();
        $this->create_table_rating_question();
        $this->create_table_rating_response();

        $this->insert_rating_question();

        $this->alter_table_products();
        $this->alter_table_agenda_viagem();

        $this->db->update('settings',  array('version' => '4.4.7'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'avaliar' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'avaliar_dias' => array('type' => 'INT', 'constraint' => 1 , 'default' => 2),//dois dias por padrao
        );

        $this->dbforge->add_column('products', $fields);
    }

    public function alter_table_agenda_viagem() {
        $fields = array(
            'avaliar' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('agenda_viagem', $fields);
    }

    function create_table_ratings() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'uuid' => array('type' => 'VARCHAR', 'constraint' => '300'  ),
            'sale_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'item_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'answered' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'rating_private' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'publish' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'customer' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
            'customer_id' => array('type' => 'INT', 'constraint' => 1 ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 1 ),
            'product_id' => array('type' => 'INT', 'constraint' => 1 ),
            'average' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('ratings', TRUE, $attributes);
    }

    function create_table_rating_response() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'rating_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'question_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'type_question' => array('type' => 'VARCHAR', 'constraint' => '55', 'default' => 'rating' ),
            'question' => array('type' => 'LONGTEXT', 'default' => ''),
            'rating' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'response' => array('type' => 'LONGTEXT', 'default' => ''),
            'created_at' => array('type' => 'TIMESTAMP', 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('rating_responses', TRUE, $attributes);
    }

    function create_table_rating_question() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'question' => array('type' => 'LONGTEXT', 'default' => ''),
            'type_question' => array('type' => 'VARCHAR', 'constraint' => '55', 'default' => 'rating' ),
            'ordem' => array('type' => 'INT', 'constraint' => 11 , 'null' => TRUE),
            'force_note' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('rating_questions', TRUE, $attributes);
    }

    public function insert_rating_question()
    {
        $this->db->insert('rating_questions', array('ordem'=> 1, 'question' => 'E aí, como foi a sua experiência?', 'type_question' => 'rating'));
        $this->db->insert('rating_questions', array('ordem'=> 2, 'question' => 'Como você avaliaria a comunicação antes e durante a excursão?', 'type_question' => 'rating'));
        $this->db->insert('rating_questions', array('ordem'=> 3, 'question' => 'Os guias turísticos foram informativos e prestativos?', 'type_question' => 'rating'));
        $this->db->insert('rating_questions', array('ordem'=> 4, 'question' => 'Como você avaliaria a qualidade do nosso serviço durante a excursão?', 'type_question' => 'rating'));
        $this->db->insert('rating_questions', array('ordem'=> 5, 'question' => 'Existe algo mais que você gostaria de compartilhar sobre sua experiência conosco?', 'type_question' => 'note', 'force_note' => true));
    }

    public function down() {}
}
