<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update341 extends CI_Migration {

    public function up() {

        $this->db->update('settings',  array('version' => '3.22.08.30', 'default_email' => 'voucher_no-replay@sagtur.com.br'), array('setting_id' => 1));
    }



    public function down() {}
}
