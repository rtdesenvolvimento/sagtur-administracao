<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update394 extends CI_Migration {

    public function up() {

        $this->create_table_assento_bloqueio();

        $this->create_table_automovel();
        $this->create_table_mapa_automovel();
        $this->alter_table_tipo_transporte_rodoviario();

        $this->insert_table_automovel_semi_leito_ld_46_lugares();
        $this->insert_table_automovel_semi_leito_ld_44_lugares();

        $this->db->update('settings',  array('version' => '3.9.4'), array('setting_id' => 1));
    }

    public function create_table_assento_bloqueio() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'product_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'programacao_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'tipo_transporte_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'note' => array('type' => 'VARCHAR', 'constraint' => '300' , 'null' => FALSE ),
            'assento' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'str_assento' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('assento_bloqueio', TRUE, $attributes);
    }

    public function create_table_automovel() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'andares' => array('type' => 'INT', 'constraint' => 11 ),
            'total_assentos' => array('type' => 'INT', 'constraint' => 11 ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => 'about_bus.jpg' ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('automovel', TRUE, $attributes);
    }

    public function create_table_mapa_automovel() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'automovel_id' => array('type' => 'INT', 'constraint' => 11 ),
            'andar' => array('type' => 'INT', 'constraint' => 11 ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' , 'null' => FALSE ),
            'assento' => array('type' => 'INT', 'constraint' => 11 ),
            'habilitado' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'ordem' => array('type' => 'INT', 'constraint' => 11 ),
            'x' => array('type' => 'INT', 'constraint' => 11 ),
            'y' => array('type' => 'INT', 'constraint' => 11 ),
            'z' => array('type' => 'INT', 'constraint' => 11 ),

            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'img' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cor_habilitado' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '#008745' ),
            'cor_desabilitado' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '#ff0000' ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('mapa_automovel', TRUE, $attributes);
    }

    public function insert_table_automovel_semi_leito_ld_44_lugares() {

        $this->db->insert('automovel', array('name' => 'Semi Leito LD 44 Lugares', 'andares' => 1, 'total_assentos' => 44));
        $this->db->insert('tipo_transporte_rodoviario', array('name' => 'Semi Leito LD 44 Lugares', 'automovel_id' => 1, 'totalPoltronas' => 44, 'numeroAndares' => 1, 'active' => 0));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));


        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        //desabilitado
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 7, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 8, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 9, 'name' => '07', 'assento' => 7, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 10, 'name' => '08', 'assento' => 8, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 11, 'name' => '09', 'assento' => 9, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 12, 'name' => '10', 'assento' => 10, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 13, 'name' => '11', 'assento' => 11, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 14, 'name' => '12', 'assento' => 12, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 15, 'name' => '13', 'assento' => 13, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 16, 'name' => '14', 'assento' => 14, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 17, 'name' => '15', 'assento' => 15, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 18, 'name' => '16', 'assento' => 16, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 19, 'name' => '17', 'assento' => 17, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 20, 'name' => '18', 'assento' => 18, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 21, 'name' => '19', 'assento' => 19, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 22, 'name' => '20', 'assento' => 20, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 23, 'name' => '21', 'assento' => 21, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 24, 'name' => '22', 'assento' => 22, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 25, 'name' => '23', 'assento' => 23, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 26, 'name' => '24', 'assento' => 24, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 27, 'name' => '25', 'assento' => 25, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 28, 'name' => '26', 'assento' => 26, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 29, 'name' => '27', 'assento' => 27, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 30, 'name' => '28', 'assento' => 28, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 31, 'name' => '29', 'assento' => 29, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 32, 'name' => '30', 'assento' => 30, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 33, 'name' => '31', 'assento' => 31, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 34, 'name' => '32', 'assento' => 32, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 35, 'name' => '33', 'assento' => 33, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 36, 'name' => '34', 'assento' => 34, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 37, 'name' => '35', 'assento' => 35, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 38, 'name' => '36', 'assento' => 36, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 39, 'name' => '37', 'assento' => 37, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 40, 'name' => '38', 'assento' => 38, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 41, 'name' => '39', 'assento' => 39, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 42, 'name' => '40', 'assento' => 40, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 43, 'name' => '41', 'assento' => 41, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 44, 'name' => '42', 'assento' => 42, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 45, 'name' => '43', 'assento' => 43, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 46, 'name' => '44', 'assento' => 44, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 47, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 48, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 49, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 50, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 51, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 52, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 53, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 1, 'andar' => 1, 'ordem' => 54, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));

    }

    public function insert_table_automovel_semi_leito_ld_46_lugares() {

        $this->db->insert('automovel', array('name' => 'Semi Leito LD 46 Lugares', 'andares' => 1, 'total_assentos' => 46));
        $this->db->insert('tipo_transporte_rodoviario', array('name' => 'Semi Leito LD 46 Lugares', 'automovel_id' => 2, 'totalPoltronas' => 46, 'numeroAndares' => 1, 'active' => 0));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 1, 'name' => '01', 'assento' => 1, 'x' => 0, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 2, 'name' => '02', 'assento' => 2, 'x' => 0, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 3, 'name' => '03', 'assento' => 3, 'x' => 0, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 4, 'name' => '04', 'assento' => 4, 'x' => 0, 'y' => 2, 'z' => 0, 'habilitado' => true));


        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 5, 'name' => '05', 'assento' => 5, 'x' => 1, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 6, 'name' => '06', 'assento' => 6, 'x' => 1, 'y' => 1, 'z' => 0, 'habilitado' => true));

        //desabilitado
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 7, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 8, 'name' => '', 'assento' => 0, 'x' => 1, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 9, 'name' => '07', 'assento' => 7, 'x' => 2, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 10, 'name' => '08', 'assento' => 8, 'x' => 2, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 11, 'name' => '09', 'assento' => 9, 'x' => 3, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 12, 'name' => '10', 'assento' => 10, 'x' => 3, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 13, 'name' => '11', 'assento' => 11, 'x' => 3, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 14, 'name' => '12', 'assento' => 12, 'x' => 3, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 15, 'name' => '13', 'assento' => 13, 'x' => 4, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 16, 'name' => '14', 'assento' => 14, 'x' => 4, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 17, 'name' => '15', 'assento' => 15, 'x' => 4, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 18, 'name' => '16', 'assento' => 16, 'x' => 4, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 19, 'name' => '17', 'assento' => 17, 'x' => 5, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 20, 'name' => '18', 'assento' => 18, 'x' => 5, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 21, 'name' => '19', 'assento' => 19, 'x' => 5, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 22, 'name' => '20', 'assento' => 20, 'x' => 5, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 23, 'name' => '21', 'assento' => 21, 'x' => 6, 'y' => 1, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 24, 'name' => '22', 'assento' => 22, 'x' => 6, 'y' => 0, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 25, 'name' => '23', 'assento' => 23, 'x' => 6, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 26, 'name' => '24', 'assento' => 24, 'x' => 6, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 27, 'name' => '25', 'assento' => 25, 'x' => 7, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 28, 'name' => '26', 'assento' => 26, 'x' => 7, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 29, 'name' => '27', 'assento' => 27, 'x' => 7, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 30, 'name' => '28', 'assento' => 28, 'x' => 7, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 31, 'name' => '29', 'assento' => 29, 'x' => 8, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 32, 'name' => '30', 'assento' => 30, 'x' => 8, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 33, 'name' => '31', 'assento' => 31, 'x' => 8, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 34, 'name' => '32', 'assento' => 32, 'x' => 8, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 35, 'name' => '33', 'assento' => 33, 'x' => 9, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 36, 'name' => '34', 'assento' => 34, 'x' => 9, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 37, 'name' => '35', 'assento' => 35, 'x' => 9, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 38, 'name' => '36', 'assento' => 36, 'x' => 9, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 39, 'name' => '37', 'assento' => 37, 'x' => 10, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 40, 'name' => '38', 'assento' => 38, 'x' => 10, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 41, 'name' => '39', 'assento' => 39, 'x' => 10, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 42, 'name' => '40', 'assento' => 40, 'x' => 10, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 43, 'name' => '41', 'assento' => 41, 'x' => 11, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 44, 'name' => '42', 'assento' => 42, 'x' => 11, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 45, 'name' => '43', 'assento' => 43, 'x' => 11, 'y' => 3, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 46, 'name' => '44', 'assento' => 44, 'x' => 11, 'y' => 2, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 47, 'name' => '45', 'assento' => 45, 'x' => 12, 'y' => 0, 'z' => 0, 'habilitado' => true));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 48, 'name' => '46', 'assento' => 46, 'x' => 12, 'y' => 1, 'z' => 0, 'habilitado' => true));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 49, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 50, 'name' => '', 'assento' => 0, 'x' => 12, 'y' => 2, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 51, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 0, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 52, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 1, 'z' => 0, 'habilitado' => false));

        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 53, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 3, 'z' => 0, 'habilitado' => false));
        $this->db->insert('mapa_automovel', array('automovel_id' => 2, 'andar' => 1, 'ordem' => 54, 'name' => '', 'assento' => 0, 'x' => 13, 'y' => 2, 'z' => 0, 'habilitado' => false));
    }

    public function alter_table_tipo_transporte_rodoviario() {
        $fields = array(
            'automovel_id' => array('type' => 'INT', 'constraint' => 11, 'default' => NULL),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('tipo_transporte_rodoviario', $fields);
    }

    public function down() {}
}
