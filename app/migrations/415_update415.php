<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update415 extends CI_Migration {

    public function up() {

        $this->db->update('settings',  array('version' => '4.1.5'), array('setting_id' => 1));
    }

    public function down() {}
}
