<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update414 extends CI_Migration {

    public function up() {


        $this->create_indexs();

        $this->db->update('settings',  array('version' => '4.1.4'), array('setting_id' => 1));
    }

    public function create_indexs() {

        //create
        $this->db->query('CREATE INDEX idx_product_details_product_id ON sma_product_details(product_id);');
        $this->db->query('CREATE INDEX idx_product_midia_product_id   ON sma_product_midia(product_id);');
        $this->db->query('CREATE INDEX idx_product_seo_product_id     ON sma_product_seo(product_id);');
        $this->db->query('CREATE INDEX idx_product_addresses_product_id  ON sma_product_addresses(product_id);');

        $this->db->query('CREATE INDEX idx_settings_location_setting_id  ON sma_settings(location_setting_id);');
        $this->db->query('CREATE INDEX idx_settings_shop_settings_id     ON sma_settings(shop_settings_id);');
    }

    public function down() {}
}
