<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update423 extends CI_Migration {

    public function up() {

        $this->alter_table_agenda_viagem();

        $this->db->update('settings',  array('version' => '4.2.1'), array('setting_id' => 1));
    }

    public function alter_table_agenda_viagem() {
        $fields = array(
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('agenda_viagem', $fields);
    }
    public function down() {}
}
