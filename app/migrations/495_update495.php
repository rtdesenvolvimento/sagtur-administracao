<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update495 extends CI_Migration {

    public function up() {

        $this->create_table_nf_agendamentos();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.9.5'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'emitir_nota_fiscal' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    function create_table_nf_agendamentos()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'status' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'generated'),//Status da nota fiscal. Valores possíveis: generated, scheduled, issued, cancel
            'code' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'customer_id' => array('type' => 'INT', 'constraint' => 1 ),
            'fatura_id' => array('type' => 'INT', 'constraint' => 1 ),
            'nf_code' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'serviceDescription' => array('type' => 'LONGTEXT', 'default' => ''),//Descrição dos serviços da nota fiscal
            'observations' => array('type' => 'LONGTEXT', 'default' => ''),
            'externalReference' => array('type' => 'VARCHAR', 'constraint' => '300' ),//Identificador da nota fiscal no seu sistema
            'valor_nf' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),
            'deductions' => array( 'type' => 'decimal', 'constraint' => '25,2', 'default' => 0 ),//Deduções. As deduções não alteram o valor total da nota fiscal, mas alteram a base de cálculo do ISS
            'effectiveDate' => array('type' => 'DATE', 'null' => TRUE ),//Data de emissão da nota fiscal
            'municipalServiceId' => array('type' => 'VARCHAR', 'constraint' => '300' ),//Identificador único do serviço municipal
            'municipalServiceCode' => array('type' => 'VARCHAR', 'constraint' => '300' ),//Código de serviço municipal
            'municipalServiceName' => array('type' => 'VARCHAR', 'constraint' => '300' ),//Nome do serviço municipal. Se não for informado, será utilizado o atributo municipalServiceCode como nome para identificação.
            'updatePayment' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),//Atualizar o valor da cobrança com os impostos da nota já descontados.
            'type_nf' => array('type' => 'VARCHAR', 'constraint' => '10' , 'default' => 'nf'),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('nf_agendamentos', TRUE, $attributes);
    }

    public function down() {}
}
