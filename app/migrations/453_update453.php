<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update453 extends CI_Migration {

    public function up() {

        $this->create_table_interesse_captacao();

        $this->alter_table_captacao();
        $this->alter_table_captacao_settings();

        $this->db->update('settings',  array('version' => '4.5.3'), array('setting_id' => 1));
    }

    function create_table_interesse_captacao() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('interesse_captacao', TRUE, $attributes);

        $this->db->insert('interesse_captacao', array('name' => 'Contato'));
        $this->db->insert('interesse_captacao', array('name' => 'Orçamento'));
        $this->db->insert('interesse_captacao', array('name' => 'Lista de Espera'));
        $this->db->insert('interesse_captacao', array('name' => 'Tenho Interesse Rodoviário'));
        $this->db->insert('interesse_captacao', array('name' => 'Tenho Interesse Aéreo'));
        $this->db->insert('interesse_captacao', array('name' => 'Contato Site'));
    }

    public function alter_table_captacao()
    {
        $fields = array(
            'interesse_id' => array('type' => 'INT', 'constraint' => 1 ),
        );

        $this->dbforge->add_column('captacao', $fields);

        $this->db->update('captacao',  array('interesse_id' => 1));
    }

    function alter_table_captacao_settings() {

        $fields = array(
            'department_default_id' => array('type' => 'INT', 'constraint' => 1 ),
            'interesse_default_id' => array('type' => 'INT', 'constraint' => 1 ),
            'interesse_lista_espera_default_id' => array('type' => 'INT', 'constraint' => 1 ),
            'interesse_tenho_interesse_default_id' => array('type' => 'INT', 'constraint' => 1 ),
        );

        $this->dbforge->add_column('captacao_settings', $fields);

        $this->db->update('captacao_settings', array('department_default_id' => 1, 'interesse_default_id' => 1, 'interesse_lista_espera_default_id' => 3, 'interesse_tenho_interesse_default_id' => 4), array('id' => 1));

    }

    public function down() {}
}
