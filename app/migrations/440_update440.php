<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update440 extends CI_Migration {

    public function up() {


        $this->alter_table_taxas_venda_configuracao();
        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.4.0'), array('setting_id' => 1));
    }

    public function alter_table_taxas_venda_configuracao()
    {

        $fields = array(
            'tipo_sinal' => array('type' => 'VARCHAR', 'constraint' => '55',  'default' => 'percentual' ),
        );
        $this->dbforge->add_column('taxas_venda_configuracao', $fields);
    }

    public function alter_table_settings() {
        $fields = array(
            'instrucoes_sinal_pagamento' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }


    public function down() {}
}
