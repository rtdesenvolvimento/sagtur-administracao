<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update491 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.9.1'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'atividades_recomendadas' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
