<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update350 extends CI_Migration {

    public function up() {

        $this->alter_to_emoji();

        $this->db->update('settings',  array('version' => '3.5.0'), array('setting_id' => 1));
    }

    public function alter_to_emoji() {
        try {
            $this->db->query('UPDATE `sma_products` set start_date = null, end_date = null');
            $this->db->query('ALTER TABLE sma_products MODIFY COLUMN product_details longtext CHARACTER SET utf8mb4');
            $this->db->query('ALTER TABLE sma_products MODIFY COLUMN oqueInclui longtext CHARACTER SET utf8mb4');
            $this->db->query('ALTER TABLE sma_products MODIFY COLUMN itinerario longtext CHARACTER SET utf8mb4');
            $this->db->query('ALTER TABLE sma_products MODIFY COLUMN valores_condicoes longtext CHARACTER SET utf8mb4');
            $this->db->query('ALTER TABLE sma_products MODIFY COLUMN details longtext CHARACTER SET utf8mb4;');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
