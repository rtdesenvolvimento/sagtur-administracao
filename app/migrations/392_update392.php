<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update392 extends CI_Migration {

    public function up() {

        $this->alter_table_valor_faixa();

        $this->db->update('settings',  array('version' => '3.9.2', 'show_payment_report_shipment' => 0), array('setting_id' => 1));
    }

    public function alter_table_valor_faixa() {
        $fields = array(
            'ocultar_faixa_relatorio' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
        );

        $this->dbforge->add_column('valor_faixa', $fields);
    }

    public function down() {}
}
