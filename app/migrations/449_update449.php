<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update449 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->update_rating_questions();
        $this->db->update('settings',  array('version' => '4.4.7'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'avaliar' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function update_rating_questions()
    {
        $this->db->update('rating_questions',  array('question' => 'Os guias de turismo foram informativos e prestativos?'), array('id' => 3));

    }

    public function down() {}
}
