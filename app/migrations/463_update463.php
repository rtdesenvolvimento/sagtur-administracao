<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update463 extends CI_Migration {

    public function up() {
        $this->db->update('settings',  array('version' => '4.6.3', 'pixelFacebook' => ''), array('setting_id' => 1));
    }

    public function down() {}
}
