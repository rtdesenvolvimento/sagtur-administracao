<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update400 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '4.0.0'), array('setting_id' => 1));
    }

    public function create_index() {

        $this->db->query('CREATE INDEX indxs_sma_assento_bloqueio_active_cart_lock ON sma_assento_bloqueio (active, cart_lock, cart_time_blocked)');
        $this->db->query('CREATE INDEX indxs_sma_analytical_access_record_date ON sma_analytical_access_record (date)');
    }
    public function down() {

    }
}
