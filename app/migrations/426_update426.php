<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update426 extends CI_Migration {

    public function up() {

        $this->alter_table_sma_sales();

        $this->db->update('settings',  array('version' => '4.2.6'), array('setting_id' => 1));
    }


    public function alter_table_sma_sales()
    {

        $fields = array(
            'archived' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_column('sales', $fields);
    }

    public function down() {}
}
