<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update318 extends CI_Migration {

    public function up() {

        $this->alter_table_parcela();
        $this->alter_table_fatura();
        $this->update_datas_table_parcela_fatura();

        //atualizar versao
        $this->db->update('settings',  array('version' => '2021.2'), array('setting_id' => 1));
    }

    function update_datas_table_parcela_fatura() {

        $this->db->select("fatura.id as faturaId,
            parcela.id as parcelaId,
            parcela.tipo, 
            fatura.strreceitas,
            fatura.strdespesas,
            conta_receber.sale as saleId,
            concat({$this->db->dbprefix('parcela')}.numeroparcela,'/',{$this->db->dbprefix('parcela')}.totalParcelas) as numero_parcela, 
            sale_items.programacaoId,
            sale_items.product_name,
            agenda_viagem.dataSaida");

        $this->db->from('fatura');
        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('sales', 'sales.id = conta_receber.sale', 'left');
        $this->db->join('sale_items', 'sales.id = sale_items.sale_id', 'left');
        $this->db->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId', 'left');

        $query = $this->db->get();
        $datas = $query->result();

        foreach ($datas as $data) {

            $product_name = '';

            if ($data->programacaoId != null) {
                $product_name = $data->product_name.' '.date('d/m/Y', strtotime($data->dataSaida));
            } else if ($data->tipo == 'receita') {
                $product_name = $data->strreceitas;
            } else if ($data->tipo == 'despesa'){
                $product_name = $data->strdespesas;
            }

            $sale_id = $data->saleId != null  && $data->saleId > 0 ? $data->saleId : NULL;

            $update_parcela = array(
                'sale_id' => $sale_id,
                'programacaoId' => $data->programacaoId,
            );

            $this->edit('parcela', $update_parcela, 'id', $data->parcelaId);

            $update_fatura = array(
                'sale_id' => $sale_id,
                'programacaoId' => $data->programacaoId,
                'numero_parcela' => $data->numero_parcela,
                'product_name' => $product_name ,
            );

            $this->edit('fatura', $update_fatura, 'id', $data->faturaId);
        }
    }

    function edit($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
        return FALSE;
    }

    function alter_table_parcela() {
        $fields = array(
            'sale_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('parcela', $fields);
    }

    function alter_table_fatura() {
        $fields = array(
            'sale_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
            'numero_parcela' => array('type' => 'VARCHAR', 'constraint' => '100' , 'default' => '' ),
            'product_name' => array('type' => 'VARCHAR', 'constraint' => '300' , 'default' => '' ),
        );
        $this->dbforge->add_column('fatura', $fields);
    }
    public function down() {}
}
