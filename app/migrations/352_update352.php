<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update352 extends CI_Migration {

    public function up() {
        $this->alter_table_companies_column();
        $this->db->update('settings',  array('version' => '3.5.2'), array('setting_id' => 1));
    }

    public function alter_table_companies_column() {
        try {
            //sma_companies
            $this->db->query('ALTER TABLE `sma_companies` CHANGE `postal_code` `postal_code` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL;');
        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
