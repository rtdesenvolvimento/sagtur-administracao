<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update346 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->db->update('settings',  array('version' => '3.22.09.22.1'), array('setting_id' => 1));
    }

    public function create_index() {
        try {
            $this->db->query('CREATE INDEX indxs_product_photos	ON sma_product_photos (product_id)');

        } catch (Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function down() {}
}
