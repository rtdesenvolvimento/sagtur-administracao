<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update458 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->alter_table_order_ref();

        $this->db->update('settings',  array('version' => '4.5.8'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'commission_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'COM'),
        );

        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_order_ref() {
        $fields = array(
            'comm' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('order_ref', $fields);
    }

    public function down() {}
}
