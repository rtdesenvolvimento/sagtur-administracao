<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update359 extends CI_Migration {

    public function up() {

        $this->create_table_gallery_item();
        $this->alter_table_gallery();

        $this->db->update('settings',  array('version' => '3.5.9'), array('setting_id' => 1));
    }

    public function alter_table_gallery() {
        $fields = array(
            'name' => array('type' => 'VARCHAR', 'constraint' => '100' ),
        );
        $this->dbforge->add_column('gallery', $fields);
    }

    function create_table_gallery_item() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'gallery_id' => array('type' => 'INT', 'constraint' => 11 , 'default' => 12),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('gallery_item', TRUE, $attributes);
    }

    public function down() {}
}
