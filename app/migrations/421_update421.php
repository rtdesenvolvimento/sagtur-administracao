<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update421 extends CI_Migration {

    public function up() {

        $this->create_index();

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.2.1'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'servicos_adicionais_venda' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public  function create_index()
    {
        $this->db->query('CREATE INDEX idxs_faixa_id ON sma_valor_faixa_etaria (faixaId);');
        $this->db->query('CREATE INDEX idxs_sma_local_embarque_viagem_produto ON sma_local_embarque_viagem (produto);');
        $this->db->query('CREATE INDEX idxs_sma_local_embarque_viagem_status ON sma_local_embarque_viagem (status);');
        $this->db->query('CREATE INDEX idxs_sma_local_embarque_viagem_dataEmbarque ON sma_local_embarque_viagem (dataEmbarque);');
        $this->db->query('CREATE INDEX idxs_sma_local_embarque_viagem_horaEmbarque ON sma_local_embarque_viagem (horaEmbarque);');
    }

    public function down() {}
}
