<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update424 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->alter_table_valor_faixa();
        $this->add_new_pagador();
        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.2.4'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'faixa_pagador' => array('type' => 'INT', 'constraint' => 1, 'default' => null),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_valor_faixa() {
        $fields = array(
            'uso_interno' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
        );
        $this->dbforge->add_column('valor_faixa', $fields);
    }

    public function add_new_pagador()
    {
        $data = array(
            'active'=> 0,
            'name' => 'Dados do comprador',
            'note' => 'Uso interno',
            'descontarVaga' => FALSE,
            'obrigaCPF' => TRUE,
            'captarCPF' => TRUE,
            'faixaDependente' =>  FALSE,
            'captarDataNascimento' => TRUE,
            'captarDocumento' => FALSE,
            'captarOrgaoEmissor' => FALSE,
            'captarEmail' => TRUE,
            'captarProfissao' =>  FALSE,
            'captarInformacoesMedicas' => FALSE,
            'captarNomeSocial' => FALSE,
            'obrigaOrgaoEmissor' =>  FALSE,
            'staff' =>  FALSE,
            'uso_interno' => TRUE,
        );

        $this->db->insert("valor_faixa", $data);
        $valor_faixa_id = $this->db->insert_id();

        $this->db->update('settings',  array('faixa_pagador' => $valor_faixa_id), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'cat_precificacao' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => 'preco_faixa_valores'),
        );
        $this->dbforge->add_column('products', $fields);
    }
    public function down() {}
}
