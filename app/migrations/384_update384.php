<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update384 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '3.8.4'), array('setting_id' => 1));
    }

    public function alter_table_products() {
        $fields = array(
            'type_calendar' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),//{1 - exibir calendario, 2 - exibir lista de datas , 3 - nao exibir calendario/lista de data}
        );

        $this->dbforge->add_column('products', $fields);
    }

    public function down() {}
}
