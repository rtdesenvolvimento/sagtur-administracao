<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update461 extends CI_Migration {

    public function up() {

        $this->alter_table_fatura_cobranca();

        $this->db->update('settings',  array('version' => '4.6.1'), array('setting_id' => 1));
    }

    public function alter_table_fatura_cobranca() {
        $fields = array(
            'cod_installment' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
        );
        $this->dbforge->add_column('fatura_cobranca', $fields);
    }

    public function down() {}
}
