<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update445 extends CI_Migration {

    public function up() {

        $this->alter_table_tipo_cobranca();

        $this->db->update('settings',  array('version' => '4.4.5'), array('setting_id' => 1));
    }

    public function alter_table_tipo_cobranca() {
        $fields = array(
            'chave_pix' => array('type' => 'VARCHAR', 'constraint' => '500', 'null' => TRUE ),
            'titular' => array('type' => 'VARCHAR', 'constraint' => '500', 'null' => TRUE ),
            'telefone' => array('type' => 'VARCHAR', 'constraint' => '500', 'null' => TRUE ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '500', 'null' => TRUE ),
            'configurar_pix' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('tipo_cobranca', $fields);
    }

    public function down() {}
}
