<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update485 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.8.5'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'max_packages_line'   => array('type' => 'INT', 'constraint' => 11, 'default' => 12),
            'footer_color' => array('type' => 'VARCHAR', 'constraint' => '999' , 'default' => '#212121' ),
        );

        $this->dbforge->add_column('shop_settings', $fields);
    }

    public function down() {}
}
