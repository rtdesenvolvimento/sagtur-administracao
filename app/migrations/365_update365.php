<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update365 extends CI_Migration {

    public function up() {

        $this->alter_table_categories();

        $this->db->update('settings',  array('version' => '3.6.5'), array('setting_id' => 1));
    }

    public function alter_table_categories() {
        $fields = array(
            'use_day' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('categories', $fields);
    }

    public function down() {}
}
