<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update416 extends CI_Migration {

    public function up() {

        $this->drop_index_sales();
        $this->recreate_index_sales();

        $this->db->update('settings',  array('version' => '4.1.6'), array('setting_id' => 1));
    }

    public function drop_index_sales() {

        if ($this->isIndexExists('sma_sales', 'indxs_sales_created_by1')) {
            $this->db->query('DROP INDEX indxs_sales_created_by1 ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_payment_status1')) {
            $this->db->query('DROP INDEX indxs_sales_payment_status1 ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_sale_status1')) {
            $this->db->query('DROP INDEX indxs_sales_sale_status1 ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_date1')) {
            $this->db->query('DROP INDEX indxs_sales_date1 ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_customer1')) {
            $this->db->query('DROP INDEX indxs_sales_customer1 ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_created_by')) {
            $this->db->query('DROP INDEX indxs_sales_created_by ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_payment_status')) {
            $this->db->query('DROP INDEX indxs_sales_payment_status ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_sale_status')) {
            $this->db->query('DROP INDEX indxs_sales_sale_status ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_date')) {
            $this->db->query('DROP INDEX indxs_sales_date ON sma_sales;');
        }

        if ($this->isIndexExists('sma_sales', 'indxs_sales_customer')) {
            $this->db->query('DROP INDEX indxs_sales_customer ON sma_sales;');
        }
    }

    public function recreate_index_sales() {

        $this->db->query('CREATE INDEX indxs_sales_created_by ON sma_sales (created_by)');
        $this->db->query('CREATE INDEX indxs_sales_payment_status ON sma_sales (payment_status)');
        $this->db->query('CREATE INDEX indxs_sales_sale_status ON sma_sales (sale_status)');
        $this->db->query('CREATE INDEX indxs_sales_date ON sma_sales (date)');
        $this->db->query('CREATE INDEX indxs_sales_customer ON sma_sales (customer)');
    }

    private function isIndexExists($table, $index)
    {
        $query = $this->db->query("SHOW INDEX FROM $table WHERE Key_name = '$index'");
        return $query->num_rows() > 0;
    }

    public function down() {}
}
