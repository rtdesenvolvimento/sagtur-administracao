<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update434 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();
        $this->alter_table_gift_cards();
        $this->alter_table_motivo_cancelamento();

        $this->insert_dados_para_reembolso();
        $this->insert_despesa();

        $this->db->update('settings',  array('version' => '4.3.4'), array('setting_id' => 1));
    }

    public function alter_table_settings()
    {
        $fields = array(
            'tipo_cobranca_reembolso_cliente_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_table_gift_cards()
    {
        $fields = array(
            'type' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => 'credit'),
            'gerar_reembolso' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('gift_cards', $fields);
    }

    public function alter_table_motivo_cancelamento()
    {
        $fields = array(
            'gerar_reembolso' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('motivo_cancelamento', $fields);

        $this->db->update('motivo_cancelamento',  array('gerar_reembolso' => 1));
    }

    public function insert_dados_para_reembolso()
    {
        $conta_credito = array(
            'user_id'       => 1,
            'name'          => 'CAIXA - REEMBOLSO',
            'status'        => 'open',
            'date'          => date('Y-m-d H:i:s'),
            'cash_in_hand'  => 0,
            'caixa'         => 0,
        );
        $this->db->insert('pos_register', $conta_credito);
        $conta_reembolso_id = $this->db->insert_id();


        $forma_pagamento_credito = array(
            'name'          => 'REEMBOLSO',
            'status'        => 'Ativo',
        );
        $this->db->insert('forma_pagamento', $forma_pagamento_credito);
        $forma_pagmento_id = $this->db->insert_id();

        $tipo_cobranca = array(
            'name'      => 'REEMBOLSO DE CLIENTE',
            'status'    => 'Ativo',
            'note'      => '',
            'tipo'      => 'nenhuma',
            'tipoExibir'=> 'despesa',
            'integracao'=> 'nenhuma',
            'faturar_automatico' => 'sim',
            'formapagamento' => $forma_pagmento_id,
            'conta'     => $conta_reembolso_id,
            'faturarVenda' => 1,
            'automatic_cancellation_sale' => 0,
            'numero_dias_cancelamento' => 0,
            'exibirLinkCompras' => TRUE,
            'reembolso' => 1,
        );

        $this->db->insert('tipo_cobranca', $tipo_cobranca);
        $tipo_cobranca_reembolso_id = $this->db->insert_id();

        $this->db->update('settings',  array('tipo_cobranca_reembolso_cliente_id' => $tipo_cobranca_reembolso_id), array('setting_id' => 1));
    }

    public function insert_despesa() {
        $data_despesa = array(
            'name' => 'REEMBOLSO DE CLIENTE',
            'code' => 'REEMBOLSO DE CLIENTE',
            'tipo' => 'subgrupo',
            'despesasuperior' => 18,
        );

        $this->db->insert('despesa', $data_despesa);
        $despesa_id = $this->db->insert_id();

        $fields = array(
            'despesa_padrao_reembolso_id' => array('type' => 'INT', 'constraint' => 1 , 'null' => TRUE),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('despesa_padrao_reembolso_id' => $despesa_id), array('setting_id' => 1));
    }

    public function down() {}
}
