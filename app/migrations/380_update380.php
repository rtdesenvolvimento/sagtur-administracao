<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update380 extends CI_Migration {

    public function up() {

        $this->create_table_tipo_local_embarque();
        $this->create_table_tipo_veiculo();
        $this->create_table_veiculo_photos();
        $this->create_cagetoria_acomodacao();
        $this->create_acomodacao_regime();
        $this->create_table_cor_agenda();
        $this->create_table_servicos_inclusos();
        $this->create_table_tipo_trajeto_veiculo();
        $this->create_table_status_veiculo();
        $this->create_table_tipo_combustivel_veiculo();
        $this->create_table_categoria_veiculo();
        $this->create_table_acessorio();
        $this->create_table_acessorio_veiculo();
        $this->create_table_caracteristicas_product();
        $this->create_table_product_caracteristica();
        $this->create_table_tipo_contrato();
        $this->create_table_ageda_programacao();
        $this->create_table_agenda_viagem_bloqueio();
        $this->create_table_locacao_veiculo();
        $this->create_table_status_disponibilidade();
        $this->create_table_agenda_disponibilidade();

        $this->alter_table_local_embarque();
        $this->alter_table_veiculo();
        $this->alter_table_tipo_hospedagem();
        $this->alter_table_tipo_hospedagem_viagem();
        $this->alter_table_sale_items();
        $this->alter_valor_faixa();
        $this->alter_table_settings();
        $this->alter_table_destino();

        $this->db->update('settings',  array('version' => '3.8.0'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'active_location' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active_transfer' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active_tour' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function alter_valor_faixa() {
        $fields = array(
            'sigla' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => 'N/A' ),
        );
        $this->dbforge->add_column('valor_faixa', $fields);
    }

    public function alter_table_veiculo() {
        $fields = array(
            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),

            'tipo_veiculo' => array('type' => 'INT', 'constraint' => 11 ),
            'categoria' => array('type' => 'INT', 'constraint' => 11 ),

            'marca' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'modelo' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'ano' => array('type' => 'VARCHAR', 'constraint' => '5', 'default' => '' ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            'placa' => array('type' => 'VARCHAR', 'constraint' => '15', 'default' => '' ),
            'chassi' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'municipio' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'uf' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'tipo_combustivel' => array('type' => 'INT', 'constraint' => 11 ),

            'capacidade_min'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 1),
            'capacidade_max'   => array('type' => 'INT', 'constraint' => 11),
            'km_atual'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'cambio_automatico' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'photo' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),

            'status_veiculo' => array('type' => 'INT', 'constraint' => 11 ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'locadoura' => array('type' => 'INT', 'constraint' => 11 ),
            'qtd_malas'   => array('type' => 'INT', 'constraint' => 11),
            'qtd_malas_mao'   => array('type' => 'INT', 'constraint' => 11),
        );

        $this->dbforge->add_column('veiculo', $fields);
    }

    public function create_table_acessorio_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'acessorio' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('acessorio_veiculo', TRUE, $attributes);
    }

    public function create_table_product_caracteristica() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'caracteristica' => array('type' => 'INT', 'constraint' => 11 ),
            'product' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('product_caracteristica', TRUE, $attributes);
    }

    public function create_table_tipo_combustivel_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_combustivel_veiculo', TRUE, $attributes);

        $this->db->insert('tipo_combustivel_veiculo', array('name' => 'Gasolina'));
        $this->db->insert('tipo_combustivel_veiculo', array('name' => 'Diesel'));
        $this->db->insert('tipo_combustivel_veiculo', array('name' => 'Etanol'));
        $this->db->insert('tipo_combustivel_veiculo', array('name' => 'Flex (Gasolina + Etanol)'));
        $this->db->insert('tipo_combustivel_veiculo', array('name' => 'GNV'));
    }
    public function create_table_categoria_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('categoria_veiculo', TRUE, $attributes);

        $this->db->insert('categoria_veiculo', array('name' => 'Econômico'));
        $this->db->insert('categoria_veiculo', array('name' => 'Compactos'));
        $this->db->insert('categoria_veiculo', array('name' => 'Intermediário'));
        $this->db->insert('categoria_veiculo', array('name' => 'Médio'));
        $this->db->insert('categoria_veiculo', array('name' => 'SUV'));
        $this->db->insert('categoria_veiculo', array('name' => 'Utilitário'));
        $this->db->insert('categoria_veiculo', array('name' => 'Full-Size'));
        $this->db->insert('categoria_veiculo', array('name' => 'SUV Grande'));
        $this->db->insert('categoria_veiculo', array('name' => 'Van e Minivan'));
        $this->db->insert('categoria_veiculo', array('name' => 'Pickup Grandes'));
        $this->db->insert('categoria_veiculo', array('name' => 'Econômico sem A/C'));
        $this->db->insert('categoria_veiculo', array('name' => 'Luxo'));
        $this->db->insert('categoria_veiculo', array('name' => 'Especial'));
        $this->db->insert('categoria_veiculo', array('name' => 'Reboque'));
        $this->db->insert('categoria_veiculo', array('name' => 'Outros'));
    }

    public function create_table_status_disponibilidade() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'inativo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '30', 'null' => FALSE  ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('status_disponibilidade', TRUE, $attributes);

        $this->db->insert('status_disponibilidade', array('name' => 'Disponível', 'cor' => '#5cb85c'));
        $this->db->insert('status_disponibilidade', array('name' => 'Reservado', 'cor' => '#e68900'));
        $this->db->insert('status_disponibilidade', array('name' => 'Locado' , 'cor' => '#d9534f'));
        $this->db->insert('status_disponibilidade', array('name' => 'Cancelado', 'cor' => '#777'));
        $this->db->insert('status_disponibilidade', array('name' => 'Devolvido', 'cor' => '#5cb85c'));
        $this->db->insert('status_disponibilidade', array('name' => 'Em Manutenção', 'cor' => '#5bc0de'));
        $this->db->insert('status_disponibilidade', array('name' => 'Em Vistoria', 'cor' => '#5bc0de'));
        $this->db->insert('status_disponibilidade', array('name' => 'À Venda'));
        $this->db->insert('status_disponibilidade', array('name' => 'Em Contrato'));
        $this->db->insert('status_disponibilidade', array('name' => 'Emplacamento'));
        $this->db->insert('status_disponibilidade', array('name' => 'Em Transferência'));
        $this->db->insert('status_disponibilidade', array('name' => 'Inativo', 'inativo' => true));
        $this->db->insert('status_disponibilidade', array('name' => 'Outros'));
    }

    public function create_table_status_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'inativo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '30', 'null' => FALSE  ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('status_veiculo', TRUE, $attributes);

        $this->db->insert('status_veiculo', array('name' => 'Disponível'));
        $this->db->insert('status_veiculo', array('name' => 'Locado'));
        $this->db->insert('status_veiculo', array('name' => 'Em Manutenção'));
        $this->db->insert('status_veiculo', array('name' => 'Reservado'));
        $this->db->insert('status_veiculo', array('name' => 'À Venda'));
        $this->db->insert('status_veiculo', array('name' => 'Em Contrato'));
        $this->db->insert('status_veiculo', array('name' => 'Inativo', 'inativo' => true));
        $this->db->insert('status_veiculo', array('name' => 'Emplacamento'));
        $this->db->insert('status_veiculo', array('name' => 'Em Transferência'));
        $this->db->insert('status_veiculo', array('name' => 'Outros'));
    }

    public function create_table_caracteristicas_product() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('caracteristicas_product', TRUE, $attributes);

        $this->db->insert('caracteristicas_product', array('name' => 'Cobertura por dano e roubo com franquia', 'note' => 'Este seguro te dará tranquilidade em casos de roubo ou dano parcial/total do veículo. Neste caso você só deverá pagar o valor indicado no contrato de aluguel como franquia, não como o total do valor do carro. O que é uma franquia? É um valor determinado pela agência de carros que cabe ao locatário pagar em caso de dano ou roubo. Se o dano ou roubo for superior ao valor da franquia, o dinheiro que faltar será pago pela seguradora. '));
        $this->db->insert('caracteristicas_product', array('name' => 'Cancelamento até 24 horas antes da data de retirada', 'note' => 'Se você cancelar até 24 horas antes da retirada, somente cobraremos as taxas administrativas. Caso você não compareça, perderá o valor já cobrado no seu cartão de crédito.'));
        $this->db->insert('caracteristicas_product', array('name' => 'Seguro de responsabilidade civil. Beneficio de cobertura estendida'));
        $this->db->insert('caracteristicas_product', array('name' => 'Condutor adicional'));
        $this->db->insert('caracteristicas_product', array('name' => 'Confirmação imediata' , 'note' => 'Sua reserva é confirmada e garantida imediatamente assim que realizar o pagamento'));
        $this->db->insert('caracteristicas_product', array('name' => 'Sem intermediador' , 'note' => ''));
        $this->db->insert('caracteristicas_product', array('name' => 'Segurança SSL' , 'note' => 'Usamos criptografia para proteger seus dados.'));
        $this->db->insert('caracteristicas_product', array('name' => 'Cancelamento sem custo' , 'note' => 'Você pode desistir em até 7 dias depois da compra e ter seu dinheiro ressarcido em 100%, desde que seja respeitado o prazo de 48 horas antes do horário/data agendado.'));
    }

    public function create_table_servicos_inclusos() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('servicos_inclusos', TRUE, $attributes);

        $this->db->insert('servicos_inclusos', array('name' => 'City Tour'));
        $this->db->insert('servicos_inclusos', array('name' => 'Cavalgada'));
        $this->db->insert('servicos_inclusos', array('name' => 'Cassino'));
        $this->db->insert('servicos_inclusos', array('name' => 'Capela'));
        $this->db->insert('servicos_inclusos', array('name' => 'Transfer IN'));
        $this->db->insert('servicos_inclusos', array('name' => 'Transfer Out'));
        $this->db->insert('servicos_inclusos', array('name' => 'Transporte'));
        $this->db->insert('servicos_inclusos', array('name' => 'Cadeira Infantil'));
        $this->db->insert('servicos_inclusos', array('name' => 'Cadeira de Bebê'));
        $this->db->insert('servicos_inclusos', array('name' => 'Água e Refrigentante a Vontade'));
        $this->db->insert('servicos_inclusos', array('name' => 'Lanche a bordo'));
        $this->db->insert('servicos_inclusos', array('name' => 'Brindes'));
        $this->db->insert('servicos_inclusos', array('name' => 'Fotografo'));
        $this->db->insert('servicos_inclusos', array('name' => 'Academia'));
        $this->db->insert('servicos_inclusos', array('name' => 'Seguro Viagem'));
    }

    public function create_table_cor_agenda() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'cor' => array('type' => 'VARCHAR', 'constraint' => '50', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cor_agenda', TRUE, $attributes);

        $this->db->insert('cor_agenda', array('name' => 'SAGTUR-color', 'cor' => '#e58800'));
    }

    public function create_cagetoria_acomodacao() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('cagetoria_acomodacao', TRUE, $attributes);

        $this->db->insert('cagetoria_acomodacao', array('name' => 'Albergue'));
        $this->db->insert('cagetoria_acomodacao', array('name' => 'Bangalô'));
        $this->db->insert('cagetoria_acomodacao', array('name' => 'Bed & Breakfest'));
        $this->db->insert('cagetoria_acomodacao', array('name' => 'Chalé'));
        $this->db->insert('cagetoria_acomodacao', array('name' => 'Luxo'));
        $this->db->insert('cagetoria_acomodacao', array('name' => 'Standard'));
    }

    public function create_acomodacao_regime() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('acomodacao_regime', TRUE, $attributes);

        $this->db->insert('acomodacao_regime', array('name' => 'All Inclusive'));
        $this->db->insert('acomodacao_regime', array('name' => 'Café da Manhã'));
        $this->db->insert('acomodacao_regime', array('name' => 'Meia Pensão'));
        $this->db->insert('acomodacao_regime', array('name' => 'Pensão Completa'));
    }

    public function create_table_tipo_trajeto_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_trajeto_veiculo', TRUE, $attributes);

        $this->db->insert('tipo_trajeto_veiculo', array('name' => 'Privativo'));
        $this->db->insert('tipo_trajeto_veiculo', array('name' => 'Semi privativo'));
        $this->db->insert('tipo_trajeto_veiculo', array('name' => 'Privativo'));
        $this->db->insert('tipo_trajeto_veiculo', array('name' => 'Não definido'));
    }

    public function create_table_tipo_veiculo() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_veiculo', TRUE, $attributes);

        $this->db->insert('tipo_veiculo', array('name' => 'Moto'));
        $this->db->insert('tipo_veiculo', array('name' => 'Carro convencional'));
        $this->db->insert('tipo_veiculo', array('name' => 'Carrro executivo'));
        $this->db->insert('tipo_veiculo', array('name' => 'Carro crossover'));
        $this->db->insert('tipo_veiculo', array('name' => 'Carro sport'));
        $this->db->insert('tipo_veiculo', array('name' => 'Jipe'));
        $this->db->insert('tipo_veiculo', array('name' => 'Picape'));
        $this->db->insert('tipo_veiculo', array('name' => 'SUV'));
        $this->db->insert('tipo_veiculo', array('name' => 'Buggy'));
        $this->db->insert('tipo_veiculo', array('name' => 'Van'));
        $this->db->insert('tipo_veiculo', array('name' => 'Carretinha Reboque'));
        $this->db->insert('tipo_veiculo', array('name' => 'Ônibus'));
    }

    public function create_table_veiculo_photos() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'photo' => array('type' => 'VARCHAR', 'constraint' => '100'),
            'veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('veiculo_photos', TRUE, $attributes);
    }

    public function alter_table_destino() {
        $fields = array(
            'cep' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            'numero' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'complemento' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cidade' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'estado' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'link_endereco' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '' ),
        );

        $this->dbforge->add_column('destino', $fields);
    }

    public function alter_table_local_embarque() {
        $fields = array(
            'cep' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            'numero' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'complemento' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cidade' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'estado' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'link_endereco' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '' ),
            'solicitar_cliente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'captar_endereco_completo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'tipo_local_embarque_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('local_embarque', $fields);
    }

    public function create_table_tipo_local_embarque() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_local_embarque', TRUE, $attributes);

        $this->db->insert('tipo_local_embarque', array('name' => 'Outros'));
        $this->db->insert('tipo_local_embarque', array('name' => 'Hotel'));
        $this->db->insert('tipo_local_embarque', array('name' => 'Aeroporto'));
        $this->db->insert('tipo_local_embarque', array('name' => 'Cidade'));
        $this->db->insert('tipo_local_embarque', array('name' => 'Endereço'));
    }

    public function alter_table_tipo_hospedagem() {
        $fields = array(
            'categoria_id' => array('type' => 'INT', 'constraint' => 11 ),
            'regime_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('tipo_hospedagem', $fields);
    }

    public function alter_table_tipo_hospedagem_viagem() {
        $fields = array(
            'acomodacao_categoria_id' => array('type' => 'INT', 'constraint' => 11 ),
            'acomodacao_regime_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('tipo_hospedagem_viagem', $fields);
    }

    public function alter_table_sale_items() {
        $fields = array(
            'acomodacao_categoria_id' => array('type' => 'INT', 'constraint' => 11 ),
            'acomodacao_regime_id' => array('type' => 'INT', 'constraint' => 11 ),
        );

        $this->dbforge->add_column('sale_items', $fields);
    }

    public function create_table_acessorio() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('acessorio', TRUE, $attributes);

        $this->db->insert('acessorio', array('name' => 'Manual'));
        $this->db->insert('acessorio', array('name' => 'KM Ilimitada'));
        $this->db->insert('acessorio', array('name' => '4 portas'));
        $this->db->insert('acessorio', array('name' => 'Estepe'));
        $this->db->insert('acessorio', array('name' => 'Chave de roda'));
        $this->db->insert('acessorio', array('name' => 'Triângulo'));
        $this->db->insert('acessorio', array('name' => 'Macaco'));
        $this->db->insert('acessorio', array('name' => 'UT do ano'));
        $this->db->insert('acessorio', array('name' => 'Tapete'));
        $this->db->insert('acessorio', array('name' => 'Rádio AM/FM'));
        $this->db->insert('acessorio', array('name' => 'Kit multimídia'));
        $this->db->insert('acessorio', array('name' => 'Antena'));
        $this->db->insert('acessorio', array('name' => 'Calota'));
        $this->db->insert('acessorio', array('name' => 'Ar condicionado'));
        $this->db->insert('acessorio', array('name' => 'Direção hidrâulica'));
        $this->db->insert('acessorio', array('name' => 'Vidros elétricos'));
        $this->db->insert('acessorio', array('name' => 'Travas elétricas'));
        $this->db->insert('acessorio', array('name' => 'Auto-falantes Portas'));
        $this->db->insert('acessorio', array('name' => 'Faróis de Neblina'));
        $this->db->insert('acessorio', array('name' => 'Alarme'));
        $this->db->insert('acessorio', array('name' => 'GNV'));
        $this->db->insert('acessorio', array('name' => 'Rastreador'));
        $this->db->insert('acessorio', array('name' => 'Roda Liga Leve'));
        $this->db->insert('acessorio', array('name' => 'Estofamento em couro'));
        $this->db->insert('acessorio', array('name' => 'Pelíc. de Prot. nos vidro'));
        $this->db->insert('acessorio', array('name' => 'Bagageiro'));
        $this->db->insert('acessorio', array('name' => 'Engate'));
        $this->db->insert('acessorio', array('name' => 'Documentos'));
        $this->db->insert('acessorio', array('name' => 'Extintor'));
        $this->db->insert('acessorio', array('name' => 'Acendedor'));
        $this->db->insert('acessorio', array('name' => 'Blindado'));
        $this->db->insert('acessorio', array('name' => 'Outros'));
    }

    public function create_table_tipo_contrato() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'sigla' => array('type' => 'VARCHAR', 'constraint' => '30', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),
            'regra_cobranca_id' => array('type' => 'INT', 'constraint' => 11 ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('tipo_contrato', TRUE, $attributes);

        $this->db->insert('tipo_contrato', array('name' => 'Contrato Diário', 'sigla' => 'Diário'));
    }

    public function create_table_locacao_veiculo() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'reference_no' => array('type' => 'VARCHAR', 'constraint' => '300'  ),

            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),

            'locacao_status' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'billed'  ),
            'payment_status' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'due' ),

            'biller_id' => array('type' => 'INT', 'constraint' => 11 ),
            'warehouse_id' => array('type' => 'INT', 'constraint' => 11 ),

            'customer_id' => array('type' => 'INT', 'constraint' => 11 ),
            'product_id' => array('type' => 'INT', 'constraint' => 11 ),
            'veiculo_id' => array('type' => 'INT', 'constraint' => 11 ),
            'status_disponibilidade_id' => array('type' => 'INT', 'constraint' => 11 ),

            'tipo_contrato' => array('type' => 'INT', 'constraint' => 11 ),
            'tipo_cobranca' => array('type' => 'INT', 'constraint' => 11 ),
            'condicao_pagamento' => array('type' => 'INT', 'constraint' => 11 ),
            'qtd_dias_locacao'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),
            'qtd_horas_locacao'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'dataRetiradaPrevista' => array('type' => 'DATE', 'null' => TRUE ),
            'horaRetiradaPrevista' => array('type' => 'TIME', 'null' => TRUE ),

            'dataDevolucaoPrevista' => array('type' => 'DATE', 'null' => TRUE ),
            'horaDevolucaoPrevista' => array('type' => 'TIME', 'null' => TRUE ),

            'dataRetiradaRealizada' => array('type' => 'DATE', 'null' => TRUE ),
            'horaRetidadaRealizada' => array('type' => 'TIME', 'null' => TRUE ),

            'dataDevolucaoRealizada' => array('type' => 'DATE', 'null' => TRUE ),
            'horaDevolucaoRealizada' => array('type' => 'TIME', 'null' => TRUE ),

            'local_retirada_id' => array('type' => 'INT', 'constraint' => 11 ),
            'local_devolucao_id' => array('type' => 'INT', 'constraint' => 11 ),

            'local_retirada' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'local_devolucao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),

            'desconto' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'acrescimo' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valor_aluguel' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valor_excedente' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'valor_franquia' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'grand_total' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),
            'paid' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'cep_retirada' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            'endereco_retirada' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'numero_retirada' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'bairro_retirada' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'complemento_retirada' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cidade_retirada' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'estado_retirada' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'country_retirada' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),

            'cep_devolucao' => array('type' => 'VARCHAR', 'constraint' => '10', 'default' => '' ),
            'endereco_devolucao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'bairro_devolucao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'numero_devolucao' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'complemento_devolucao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'cidade_devolucao' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'estado_devolucao' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),
            'country_devolucao' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => '' ),

            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'created_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_by' => array('type' => 'INT', 'constraint' => 11 ),
            'updated_at' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('locacao_veiculo', TRUE, $attributes);
    }

    public function create_table_agenda_viagem_bloqueio() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'agenda_viagem_id' => array('type' => 'INT', 'constraint' => 11 ),

            'dataInicioBloqueio' => array('type' => 'DATE', 'null' => TRUE ),
            'dataFinalBloqueio' => array('type' => 'DATE', 'null' => TRUE ),
            'horaInicioBloqueio' => array('type' => 'TIME', 'null' => TRUE ),
            'horaFinalBloqueio' => array('type' => 'TIME', 'null' => TRUE ),

            'titulo' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('agenda_viagem_bloqueio', TRUE, $attributes);
    }

    public function create_table_agenda_disponibilidade() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'agenda_viagem_id' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),

            'data' => array('type' => 'DATE', 'null' => TRUE ),
            'hora' => array('type' => 'TIME', 'null' => TRUE ),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('agenda_disponibilidade', TRUE, $attributes);
    }

    public function create_table_ageda_programacao() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),

            'agenda_viagem_id' => array('type' => 'INT', 'constraint' => 11 ),
            'produto' => array('type' => 'INT', 'constraint' => 11 ),

            'dataSaida' => array('type' => 'DATE', 'null' => TRUE ),
            'dataRetorno' => array('type' => 'DATE', 'null' => TRUE ),
            'horaSaida' => array('type' => 'TIME', 'null' => TRUE ),
            'horaRetorno' => array('type' => 'TIME', 'null' => TRUE ),

            'titulo' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE  ),
            'note' => array('type' => 'LONGTEXT', 'default' => ''),

            'isPromocao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'isUsarPreco' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

            'preco' => array( 'type' => 'DECIMAL', 'constraint' => '25,2' ),

            'tipoDisponibilidade' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => FALSE, 'default' => 'DATA_PONTUAL'  ),
            'vagas'   => array('type' => 'INT', 'constraint' => 11 , 'default' => 0),

            'segunda' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'terca' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'quarta' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'quinta' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sexta' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'sabado' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'domingo' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),

        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('agenda_programacao', TRUE, $attributes);

    }
    public function down() {}
}
