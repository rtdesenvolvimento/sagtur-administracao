<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update407 extends CI_Migration {

    public function up() {

        $this->alter_table_settings();

        $this->db->update('settings',  array('version' => '4.0.7'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'instrucoes_tipo_cobranca' => array('type' => 'LONGTEXT', 'default' => ''),
            'instrucoes_faixa_valores' => array('type' => 'LONGTEXT', 'default' => ''),
            'instrucoes_local_embarque' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
