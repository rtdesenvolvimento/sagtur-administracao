<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update329 extends CI_Migration {

    public function up() {

        $this->create_table_room_list();
        $this->create_table_room_list_hospedagem();
        $this->create_table_room_list_hospedagem_hospede();

        $this->db->update('settings',  array('version' => '2021.4.1', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));
    }

    function create_table_room_list() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 1 ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300', 'null' => TRUE ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);

        $this->dbforge->create_table('room_list', TRUE, $attributes);
    }

    function create_table_room_list_hospedagem() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'room_list_id' => array('type' => 'INT', 'constraint' => 1 ),
            'tipo_hospedagem_id' => array('type' => 'INT', 'constraint' => 1 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);

        $this->dbforge->create_table('room_list_hospedagem', TRUE, $attributes);
    }

    function create_table_room_list_hospedagem_hospede() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'room_list_hospedagem_id' => array('type' => 'INT', 'constraint' => 1 ),
            'tipo_hospedagem_id' => array('type' => 'INT', 'constraint' => 1 ),
            'customer_id' => array('type' => 'INT', 'constraint' => 1 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);

        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);

        $this->dbforge->create_table('room_list_hospedagem_hospede', TRUE, $attributes);
    }

    public function down() {}
}
