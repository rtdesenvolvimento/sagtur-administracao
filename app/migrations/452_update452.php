<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update452 extends CI_Migration {

    public function up() {

        $this->create_table_forma_atendimento();
        $this->create_table_contact_website();
        $this->create_table_captacao_settings();
        $this->create_table_departments();

        $this->drop_tables();

        $this->alter_table_settings();
        $this->alter_table_captacao();
        $this->alter_table_order_ref();

        $this->db->update('settings',  array('version' => '4.5.2'), array('setting_id' => 1));
    }

    public function alter_table_settings() {
        $fields = array(
            'captacao_prefix' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => 'CAP'),
            'usar_captacao' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'captacao_setting_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_column('settings', $fields);

        $this->db->update('settings',  array('captacao_setting_id' => 1), array('setting_id' => 1));

    }

    public function alter_table_order_ref() {
        $fields = array(
            'cp' => array('type' => 'INT', 'constraint' => 11, 'default' => 1),
        );

        $this->dbforge->add_column('order_ref', $fields);
    }

    function drop_tables()
    {
        $this->db->query('ALTER TABLE sma_captacao DROP interesse_id;');
        $this->db->query('ALTER TABLE sma_captacao DROP date;');

        $this->db->query('DROP TABLE sma_interesses;');
        $this->db->query('DROP TABLE sma_assunto;');
    }

    function create_table_captacao_settings() {

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'captacao_name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'plantao_default_id' => array('type' => 'INT', 'constraint' => 11 ),
            'meio_default_divulgacao_id' => array('type' => 'INT', 'constraint' => 11 ),
            'forma_default_atendimento_id' => array('type' => 'INT', 'constraint' => 11 ),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('captacao_settings', TRUE, $attributes);

        $this->db->insert('meio_divulgacao', array('name' => 'CHAT', 'active' => 0));
        $meio_divulgacao_id = $this->db->insert_id();

        $data = array(
            'captacao_name' => 'CONFIGURAÇÕES PADRÃO DE CAPTAÇÃO',
            'plantao_default_id' => 2,//site
            'meio_default_divulgacao_id' => $meio_divulgacao_id,//chat
            'forma_default_atendimento_id' => 1,//assunto,
        );

        $this->db->insert('captacao_settings', $data);
    }

    function create_table_contact_website()
    {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'telefone' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'subject' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'message' => array('type' => 'LONGTEXT', 'default' => ''),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 , 'null' => FALSE ),//id do vendedor
            'read' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
        );

        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('contact_website', TRUE, $attributes);
    }

    function create_table_departments() {

        $this->db->query('DROP TABLE sma_department;');

        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
            'telefone' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'email' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'biller_id' => array('type' => 'INT', 'constraint' => 11 ),
            'message' => array('type' => 'LONGTEXT', 'default' => ''),
            'avatar' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'open_whatsapp' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('departments', TRUE, $attributes);

        $this->db->insert('departments', array('name' => 'Setor de Vendas'));
        $this->db->insert('departments', array('name' => 'Setor Financeiro'));
    }


    public function alter_table_captacao()
    {
        $fields = array(
            'biller' => array('type' => 'VARCHAR', 'constraint' => '300' ),
            'read' => array('type' => 'INT', 'constraint' => 1 , 'default' => 0),
            'date' => array('type' => 'TIMESTAMP', 'null' => TRUE ),
            'programacao_id' => array('type' => 'INT', 'constraint' => 1 ),
            'product_id' => array('type' => 'INT', 'constraint' => 1 ),
            'department_id' => array('type' => 'INT', 'constraint' => 1 ),
            'url' => array('type' => 'VARCHAR', 'constraint' => '300' ),
        );

        $this->dbforge->add_column('captacao', $fields);
    }

    function create_table_forma_atendimento() {
        $fields = array(
            'id' => array( 'type' => 'INT', 'constraint' => 11, 'auto_increment' => TRUE ),
            'name' => array('type' => 'VARCHAR', 'constraint' => '999' ),
            'active' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_field($fields);
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB', 'AUTO_INCREMENT' => 1);
        $this->dbforge->create_table('forma_atendimento', TRUE, $attributes);

        $this->db->insert('forma_atendimento', array('name' => 'WhatsApp'));
        $this->db->insert('forma_atendimento', array('name' => 'Instagram'));
        $this->db->insert('forma_atendimento', array('name' => 'Facebook'));
        $this->db->insert('forma_atendimento', array('name' => 'E-mail'));
        $this->db->insert('forma_atendimento', array('name' => 'Telefonema'));
        $this->db->insert('forma_atendimento', array('name' => 'Visita Agência'));

    }

    public function down() {}
}
