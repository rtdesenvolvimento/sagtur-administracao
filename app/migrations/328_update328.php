<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update328 extends CI_Migration {

    public function up() {

        $this->db->update('settings',
            array(
                'version' => '2021.3.25',
                'protocol' => 'smtp',
                'smtp_host' => 'mail.sagtur.com.br',
                'smtp_user' => 'voucher_no-replay@sagtur.com.br',
                'smtp_pass' => '0ef1108e379055c7e603c3293c16964ef7870669dafd6b3da21153b6464055acef590fea563065e287cac0dd79cfa23e4dd8fdd7660c7fce9b89ac63048157554nX2uQHmtuGSMxFIpyErp9ob6Oq1/A+JC8KDF8tjkMs=',
                'smtp_port' =>  '465',
                'smtp_crypto' => 'ssl',
                'default_email' => 'voucher_no-replay@sagtur.com.br'
            ), array('setting_id' => 1)
        );

        $this->db->update('settings',  array('version' => '2021.3.28', 'tax1' => '0', 'tax2' => '0'), array('setting_id' => 1));

        $fields = array(
            'voucherCustomText' => array('type' => 'LONGTEXT', 'default' => ''),
        );
        $this->dbforge->add_column('settings', $fields);
    }

    public function down() {}
}
