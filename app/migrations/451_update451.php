<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update451 extends CI_Migration {

    public function up() {

        $this->alter_table_shop_settings();

        $this->db->update('settings',  array('version' => '4.5.1'), array('setting_id' => 1));
    }

    public function alter_table_shop_settings() {
        $fields = array(
            'usar_dtnascimento_area_cliente' => array('type' => 'INT', 'constraint' => 1 , 'default' => 1),
        );
        $this->dbforge->add_column('settings', $fields);
    }


    public function down() {}
}
