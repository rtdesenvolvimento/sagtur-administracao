<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update428 extends CI_Migration {

    public function up() {

        $this->alter_table_products();

        $this->db->update('settings',  array('version' => '4.2.8'), array('setting_id' => 1));
    }

    public function alter_table_products() {//product_captacao
        $fields = array(
            'product_action' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),
            'product_action_text' => array('type' => 'INT', 'constraint' => 1, 'default' => 0),//0 Reservar| 1 Comprar| 2 Agendar
            'desc_duracao' => array('type' => 'VARCHAR', 'constraint' => '50', 'default' => ''),
        );
        $this->dbforge->add_column('products', $fields);
    }


    public function down() {}
}
