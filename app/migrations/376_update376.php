<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update376 extends CI_Migration {

    public function up() {

        $this->alter_table_sale_events();

        $this->db->update('settings',  array('version' => '3.7.6'), array('setting_id' => 1));
    }

    public function alter_table_sale_events() {
        $fields = array(
            'ip_address' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'sessions_id' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'page_web' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'server_name' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'server_port' => array('type' => 'VARCHAR', 'constraint' => '30', 'default' => '' ),
            'server_signature' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'server_software' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'http_referer' => array('type' => 'VARCHAR', 'constraint' => '300', 'default' => '' ),
            'http_method' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'http_response_code' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'browser_information_name' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'browser_information_version' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'operating_system_information_name' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'information_cookies' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '' ),
            'user_agent_info' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'information_session' => array('type' => 'VARCHAR', 'constraint' => '999', 'default' => '' ),
            'request_time' => array('type' => 'VARCHAR', 'constraint' => '100', 'default' => '' ),
            'termos_aceite' => array('type' => 'LONGTEXT', 'default' => ''),
        );

        $this->dbforge->add_column('sale_events', $fields);
    }


    public function down() {}
}
