<?php defined('BASEPATH') or exit('No direct script access allowed');

/*
 *  ==============================================================================
 *  Author    : Mian Saleem
 *  Email    : saleem@tecdiary.com
 *  For        : Stock Manager Advance
 *  Web        : http://tecdiary.com
 *  ==============================================================================
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
use League\OAuth2\Client\Provider\Google;
use Mpdf\Mpdf;

//Load Composer's autoloader
require 'vendor/autoload.php';

class Sma
{
    public function __construct(){}

    public function __get($var)
    {
        return get_instance()->$var;
    }

    private function _rglobRead($source, &$array = array())
    {
        if (!$source || trim($source) == "") {
            $source = ".";
        }
        foreach ((array) glob($source . "/*/") as $key => $value) {
            $this->_rglobRead(str_replace("//", "/", $value), $array);
        }
        $hidden_files = glob($source . ".*") and $htaccess = preg_grep('/\.htaccess$/', $hidden_files);
        $files = array_merge(glob($source . "*.*"), $htaccess);
        foreach ($files as $key => $value) {
            $array[] = str_replace("//", "/", $value);
        }
    }

    private function _zip($array, $part, $destination, $output_name = 'sma')
    {
        $zip = new ZipArchive;
        @mkdir($destination, 0777, true);

        if ($zip->open(str_replace("//", "/", "{$destination}/{$output_name}" . ($part ? '_p' . $part : '') . ".zip"), ZipArchive::CREATE)) {
            foreach ((array) $array as $key => $value) {
                $zip->addFile($value, str_replace(array("../", "./"), null, $value));
            }
            $zip->close();
        }
    }

    public function formatMoney($number,$symbol=NULL)
    {
        if ($this->Settings->sac) {
            return ($this->Settings->display_symbol == 1 ? $this->Settings->symbol : '') .
            $this->formatSAC($this->formatDecimal($number)) .
            ($this->Settings->display_symbol == 2 ? $this->Settings->symbol : '');
        }
        $decimals = $this->Settings->decimals;
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;

        if ($symbol != null) return  $symbol . number_format($number, $decimals, $ds, $ts) . ($this->Settings->display_symbol == 2 ? $this->Settings->symbol : '');

        return ($this->Settings->display_symbol == 1 ? $this->Settings->symbol : '') . number_format($number, $decimals, $ds, $ts) . ($this->Settings->display_symbol == 2 ? $this->Settings->symbol : '');
    }

    public function formatQuantity($number, $decimals = null)
    {
        if (!$decimals) {
            $decimals = $this->Settings->qty_decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatNumber($number, $decimals = null)
    {
        if (!$decimals) {
            $decimals = $this->Settings->decimals;
        }
        if ($this->Settings->sac) {
            return $this->formatSAC($this->formatDecimal($number, $decimals));
        }
        $ts = $this->Settings->thousands_sep == '0' ? ' ' : $this->Settings->thousands_sep;
        $ds = $this->Settings->decimals_sep;
        return number_format($number, $decimals, $ds, $ts);
    }

    public function formatDecimal($number, $decimals = null)
    {
        if (!is_numeric($number)) {
            return null;
        }
        if (!$decimals) {
            $decimals = $this->Settings->decimals;
        }
        return number_format($number, $decimals, '.', '');
    }

    public function dataDeHojePorExtensoRetorno($data=null, $format=null) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        if ($format != null) return strftime($format, $data);

        return strftime('%d de %B de %Y', $data);
    }

    public function dataToSiteComplet($data=null) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        $dia = strftime('%d', $data);
        $mes = strftime('%B', $data);

        return $dia.' de '.$mes;
    }

    public function dataToSiteCompletVoucher($data = null) {
        setlocale(LC_ALL, 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        // Captura o nome do dia da semana em maiúsculas
        $dia_semana = strftime('%A', $data);
        $dia_semana = strtoupper($dia_semana);

        // Captura o dia do mês
        $dia = strftime('%d', $data);

        // Captura o nome do mês abreviado
        $mes = strftime('%b', $data);
        $mes = strtoupper($mes);

        return $dia_semana . ', ' . $dia . ' ' . $mes;
    }

    public function dataToSiteSemana($data=null) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        $semana = strftime('%A', $data);

        if ($semana == 'sá') $semana = 'sábado';

        return  $semana;
    }

    public function dataToSite($data=null) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        $semana = strftime('%A', $data);
        $semana = substr($semana, 0, 3);

        if ($semana == 'sá'){
            $semana = 'sáb';
        }

        $dia = strftime('%d', $data);
        $mes = strftime('%B', $data);
        $mes = substr($mes, 0, 3);

        return $dia.' de '.$mes.' > '.$semana;
    }

    public function dataToAreaCliente($data=null) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) {
            $data = strtotime('today');
        } else {
            $data = strtotime($data);
        }

        $semana = strftime('%A', $data);
        $semana = substr($semana, 0, 3);

        if ($semana == 'sá'){
            $semana = 'sáb';
        }

        $dia = strftime('%d', $data);
        $mes = strftime('%B', $data);
        $ano = strftime('%Y', $data); // Obtém o ano

        $mes = substr($mes, 0, 3);

        return $dia.' de '.$mes. ' de '. $ano .' | '.$semana;
    }

    public function diferencaEntreduasHoras($horario1, $horario2) {
        $entrada = $horario1;
        $saida = $horario2;
        $hora1 = explode(":",$entrada);
        $hora2 = explode(":",$saida);
        $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60) + $hora1[2];
        $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60) + $hora2[2];
        $resultado = $acumulador2 - $acumulador1;
        $hora_ponto = floor($resultado / 3600);
        $resultado = $resultado - ($hora_ponto * 3600);
        $min_ponto = floor($resultado / 60);
        $resultado = $resultado - ($min_ponto * 60);
        $secs_ponto = $resultado;

        if ($hora_ponto < 9) $hora_ponto = '0'.$hora_ponto;
        if ($min_ponto < 9) $min_ponto = '0'.$min_ponto;
        if ($secs_ponto < 9) $secs_ponto = '0'.$secs_ponto;

        $tempo = $hora_ponto."h".$min_ponto."m";
        return $tempo;
    }

    public function dataDeHojePorExtensoRetornoComSemana($data=null) {

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) $data = strtotime('today');
        else $data = strtotime($data);

        return strftime('%A, %d de %B de %Y', $data);
    }

    public function dataDeHojePorExtensoRetornoMensAno($data=null) {

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) $data = strtotime('today');
        else $data = strtotime($data);

        return strftime('%B de %Y', $data);
    }

    public function dataDeHojePorExtensoRetornoDiaMensAno($data=null) {

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        if ($data == null) $data = strtotime('today');
        else $data = strtotime($data);

        return strftime('%d %B %Y', $data);
    }

    public function clear_tags($str)
    {
        return htmlentities(
            strip_tags($str,
                '<span><div><a><br><p><b><i><u><img><blockquote><small><ul><ol><li><hr><big><pre><code><strong><em><table><tr><td><th><tbody><thead><tfoot><h3><h4><h5><h6>'
            ),
            ENT_QUOTES | ENT_XHTML | ENT_HTML5,
            'UTF-8'
        );
    }

    public function decode_html($str)
    {
        return html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
    }

    public function roundMoney($num, $nearest = 0.05)
    {
        return round($num * (1 / $nearest)) * $nearest;
    }

    public function roundNumber($number, $toref = null)
    {
        switch ($toref) {
            case 1:
                $rn = round($number * 20) / 20;
                break;
            case 2:
                $rn = round($number * 2) / 2;
                break;
            case 3:
                $rn = round($number);
                break;
            case 4:
                $rn = ceil($number);
                break;
            default:
                $rn = $number;
        }
        return $rn;
    }

    public function unset_data($ud)
    {
        if ($this->session->userdata($ud)) {
            $this->session->unset_userdata($ud);
            return true;
        }
        return false;
    }

    public function hrsd($sdate)
    {
        if ($sdate) {
            return date($this->dateFormats['php_sdate'], strtotime($sdate));
        } else {
            return '0000-00-00';
        }
    }

    public function hrld($ldate)
    {
        if ($ldate) {
            return date($this->dateFormats['php_ldate'], strtotime($ldate));
        } else {
            return '0000-00-00 00:00:00';
        }
    }

    public function fsd($inv_date)
    {
        if ($inv_date) {
            $jsd = $this->dateFormats['js_sdate'];
            if ($jsd == 'dd-mm-yyyy' || $jsd == 'dd/mm/yyyy' || $jsd == 'dd.mm.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 3, 2) . "-" . substr($inv_date, 0, 2);
            } elseif ($jsd == 'mm-dd-yyyy' || $jsd == 'mm/dd/yyyy' || $jsd == 'mm.dd.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 0, 2) . "-" . substr($inv_date, 3, 2);
            } else {
                $date = $inv_date;
            }
            return $date;
        } else {
            return '0000-00-00';
        }
    }

    public function fld($ldate)
    {
        if ($ldate) {
            $date = explode(' ', $ldate);
            $jsd = $this->dateFormats['js_sdate'];
            $inv_date = $date[0];
            $time = $date[1];
            if ($jsd == 'dd-mm-yyyy' || $jsd == 'dd/mm/yyyy' || $jsd == 'dd.mm.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 3, 2) . "-" . substr($inv_date, 0, 2) . " " . $time;
            } elseif ($jsd == 'mm-dd-yyyy' || $jsd == 'mm/dd/yyyy' || $jsd == 'mm.dd.yyyy') {
                $date = substr($inv_date, -4) . "-" . substr($inv_date, 0, 2) . "-" . substr($inv_date, 3, 2) . " " . $time;
            } else {
                $date = $inv_date;
            }
            return $date;
        } else {
            return '0000-00-00 00:00:00';
        }
    }

    public function hf($h) {
        if ($h) {
            return str_replace(':', 'h', date('H:i', strtotime($h)));
        } else {
            return '';
        }
    }

    public function send_email_api($to, $subject, $body, $cc = null)
    {
        // requires module php5-curl
        $x_auth_token   = '4172169648847151e6793fe79398f0f7';
        $api_url        = 'https://api.smtplw.com.br/v1';

        $from   = "voucher@envios.suareservaonline.com.br";
        $to     = array($to);
        $cc     = array($cc);

        $headers = array(
            "x-auth-token: $x_auth_token",
            "Content-type: application/json"
        );

        $data_string = array(
            'from'    => $from,
            'to'      => $to,
            'cc'      => $cc,
            'subject' => $subject,
            'body'    => $body,
            'headers' => array('Content-type' => 'text/html')
        );

        $ch = curl_init("$api_url/messages");

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data_string));
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);

        $response = curl_exec($ch);
        $curlInfo = curl_getinfo($ch);
        curl_close($ch);

        switch($curlInfo['http_code']) {
            case '201':
                break;
            default:
                $status = "Error: $curlInfo[http_code]";
                log_message('error', 'Mail Error: ' . $status);
                throw new \Exception($status);
        }
    }

    public function send_email_queue($db, $origin, $function_origin, $object_id, $to, $subject, $body, $from = null, $from_name = null, $attachment = null, $cc = null, $bcc = null)
    {

        $data = array(
            'to' => $to,
            'subject' => $subject,
            'body' => $body,
            'from' => $from,
            'from_name' => $from_name,
            'attachment' => $attachment,
            'cc' => $cc,
            'bcc' => $bcc,
            'status' => 'pending',
            'created_at' => date('Y-m-d H:i:s'),
            'email_origin' => $origin,
            'function_origin' => $function_origin,
            'object_id' => $object_id,
        );

        return $db->insert('email_queue', $data);
    }

    public function send_email($to, $subject, $body, $from = null, $from_name = null, $attachment = null, $cc = null, $bcc = null)
    {
        // $mail = new PHPMailer;
        $mail          = new PHPMailer(true);
        $mail->CharSet = 'UTF-8';

        try {
            if (DEMO) {
                $mail->isSMTP();
                $mail->Host     = '127.0.0.1';
                $mail->SMTPAuth = true;
                $mail->Port     = 2525;
                $mail->Username = 'SMA';
                $mail->Password = '';
                // $mail->SMTPDebug = 2;
            } elseif ($this->Settings->protocol == 'mail') {
                $mail->isMail();
            } elseif ($this->Settings->protocol == 'sendmail') {
                $mail->isSendmail();
            } elseif ($this->Settings->protocol == 'smtp') {
                $mail->isSMTP();
                $mail->Host       = 'smtplw.com.br'; //$this->Settings->smtp_host;
                $mail->SMTPAuth   = true;
                $mail->SMTPSecure = true;// !empty($this->Settings->smtp_crypto) ? $this->Settings->smtp_crypto : false;
                $mail->Port       = '587'; //$this->Settings->smtp_port;

                if ($this->Settings->smtp_oauth2) {
                    $email        = $this->Settings->smtp_user;
                    $clientId     = $this->config->item('client_id');
                    $clientSecret = $this->config->item('client_secret');
                    $refreshToken = $this->config->item('refresh_token');

                    $this->mail->AuthType = 'XOAUTH2';

                    $provider = new Google(['clientId' => $clientId, 'clientSecret' => $clientSecret]);

                    $this->mail->setOAuth(new OAuth([
                        'provider'     => $provider,
                        'clientId'     => $clientId,
                        'clientSecret' => $clientSecret,
                        'refreshToken' => $refreshToken,
                        'userName'     => $email,
                    ]));
                } else {
                    $mail->Username = 'sagtur';//$this->Settings->smtp_user;
                    $mail->Password = 'QfepnoVG8761';//$this->Settings->smtp_pass;
                }
            } elseif ($this->Settings->protocol == 'smtpmima') {
                $mail->isSMTP();
                $mail->Host         = 'mail.mimatour.com.br';
                $mail->SMTPAuth     = true;
                $mail->SMTPSecure   = PHPMailer::ENCRYPTION_SMTPS;
                $mail->Port         = '465';
                $mail->Username     = 'contato@mimatour.com.br';
                $mail->Password     = 'Mima@2023';
            } else {
                $mail->isMail();
            }

            if ($from && $from_name) {
                $mail->setFrom($from, $from_name);
                $mail->addReplyTo($from, $from_name);
            } elseif ($from) {
                $mail->setFrom($from, $this->Settings->site_name);
                $mail->addReplyTo($from, $this->Settings->site_name);
            } else {
                $mail->setFrom($this->Settings->default_email, $this->Settings->site_name);
                $mail->addReplyTo($this->Settings->default_email, $this->Settings->site_name);
            }

            $mail->addAddress($to);
            if ($cc) {
                try {
                    if (is_array($cc)) {
                        foreach ($cc as $cc_email) {
                            $mail->addCC($cc_email);
                        }
                    } else {
                        $mail->addCC($cc);
                    }
                } catch (\Exception $e) {
                    log_message('info', 'PHPMailer Error: ' . $e->getMessage());
                }
            }
            if ($bcc) {
                try {
                    if (is_array($bcc)) {
                        foreach ($bcc as $bcc_email) {
                            $mail->addBCC($bcc_email);
                        }
                    } else {
                        $mail->addBCC($bcc);
                    }
                } catch (\Exception $e) {
                    log_message('info', 'PHPMailer Error: ' . $e->getMessage());
                }
            }
            $mail->Subject = $subject;
            $mail->isHTML(true);
            $mail->Body    = $body;
            $mail->AltBody = strip_tags($mail->Body);
            if ($attachment) {
                if (is_array($attachment)) {
                    foreach ($attachment as $attach) {
                        $mail->addAttachment($attach);
                    }
                } else {
                    $mail->addAttachment($attachment);
                }
            }


            if (!$mail->send()) {
                log_message('error', 'Mail Error: ' . $mail->ErrorInfo);
                //throw new Exception($mail->ErrorInfo);
            }

            return true;
        } catch (Exception $e) {
            log_message('error', 'Mail Error: ' . $e->getMessage());
            throw new \Exception($e->errorMessage());
        } catch (\Exception $e) {
            log_message('error', 'Mail Error: ' . $e->getMessage());
            throw new \Exception($e->getMessage());
        }
    }


    public function send_email_out($to, $subject, $message, $from = null, $from_name = null, $attachment = null, $cc = null, $bcc = null)
    {
        $this->load->library('email');
        $config['useragent'] = "SAGTur Sistema para Agência de Turismo";
        $config['protocol'] = 'smtp';
        $config['mailtype'] = "html";
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        if ($this->Settings->protocol == 'sendmail') {
            $config['mailpath'] = $this->Settings->mailpath;
        } elseif ($this->Settings->protocol == 'smtp') {

            $this->load->library('encryption');

            $config['smtp_host'] = 'smtplw.com.br';
            $config['smtp_user'] = 'sagtur';
            //$config['smtp_pass'] = trim($this->encryption->decrypt($this->Settings->smtp_pass));
            $config['smtp_pass'] = 'QfepnoVG8761';
            $config['smtp_port'] = '587';

            if (!empty($this->Settings->smtp_crypto)) {
                $config['smtp_crypto'] = 'tls';
            }

        }

       // print_r($config);

        $this->email->initialize($config);

        if ($from && $from_name) {
            $this->email->from($from, $from_name);
        } elseif ($from) {
            $this->email->from($from, $this->Settings->site_name);
        } else {
            $this->email->from($this->Settings->default_email, $this->Settings->site_name);
        }

        $this->email->to($to);
        if ($cc) {
            $this->email->cc($cc);
        }
        if ($bcc) {
            $this->email->bcc($bcc);
        }
        $this->email->subject($subject);
        $this->email->message($message);
        if ($attachment) {
            if (is_array($attachment)) {
                foreach ($attachment as $file) {
                    $this->email->attach($file);
                }
            } else {
                $this->email->attach($attachment);
            }
        }

        if ($this->email->send()) {
           // echo $this->email->print_debugger(); die();
            return true;
        } else {

            //echo $this->email->print_debugger(); die();
            $logEmailName = '/var/www/webroot/sistema/files';

            if (!file_exists($logEmailName))  mkdir($logEmailName, 0700);

            $logEmail = fopen($logEmailName.'/email.log','a');

            $date_message = "{" . @date("Y/m/d H:i:s", time()) . "} - ".$this->session->userdata('cnpjempresa')."\r\n";
            $strError = $this->email->print_debugger(). "\r\n";

            $str = "$date_message $strError";

            fwrite($logEmail, "$str \r\n");
            fclose($logEmail);


            return false;
        }
    }

    public function checkPermissions($action = null, $js = null, $module = null)
    {
        if (!$this->actionPermissions($action, $module)) {
            $this->session->set_flashdata('error', lang("access_denied"));
            if ($js) {
                die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
            } else {
                redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }
        }
    }

    public function actionPermissions($action = null, $module = null)
    {
        if ($this->Owner || $this->Admin) {
            if ($this->Admin && stripos($action, 'delete') !== false) {
                return false;
            }
            return true;
        } elseif ($this->Customer || $this->Supplier) {
            return false;
        } else {
            if (!$module) {
                $module = $this->m;
            }
            if (!$action) {
                $action = $this->v;
            }
            //$gp = $this->site->checkPermissions();
            if ($this->GP[$module . '-' . $action] == 1) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function save_barcode($text = null, $bcs = 'code128', $height = 56, $stext = 1, $sq = null)
    {
        $file_name = 'assets/uploads/barcode' . $this->session->userdata('user_id') . ($sq ? $sq : '') . '.png';
        $drawText = ($stext != 1) ? false : true;
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $barcodeOptions = array('text' => $text, 'barHeight' => $height, 'drawText' => $drawText, 'factor' => 1);
        $rendererOptions = array('imageType' => 'png', 'horizontalPosition' => 'center', 'verticalPosition' => 'middle');
        $image = Zend_Barcode::draw($bcs, 'image', $barcodeOptions, $rendererOptions);
        if (imagepng($image, $file_name)) {
            imagedestroy($image);
            $bc = file_get_contents($file_name);
            $bcimage = base64_encode($bc);
            return $bcimage;
        }
        return false;
    }

    public function qrcode($type = 'text', $text = 'PHP QR Code', $size = 2, $level = 'H', $sq = null)
    {
        $file_name = 'assets/uploads/qrcode' . $this->session->userdata('user_id') . ($sq ? $sq : '') . '.png';
        if ($type == 'link') {
            $text = urldecode($text);
        }
        $this->load->library('phpqrcode');
        $config = array('data' => $text, 'size' => $size, 'level' => $level, 'savename' => $file_name);
        $this->phpqrcode->generate($config);
        $qr = file_get_contents($file_name);
        $qrimage = base64_encode($qr);
        return $qrimage;
    }

    public function qrcode_pdf_sales($type = 'text', $text = 'PHP QR Code', $size = 2, $level = 'H', $sq = null)
    {
        $file_name = 'assets/uploads/qrcode/' . $this->session->userdata('user_id') . ($sq ? $sq : '') . '.png';
        if ($type == 'link') {
            $text = urldecode($text);
        }
        $this->load->library('phpqrcode');
        $config = array('data' => $text, 'size' => $size, 'level' => $level, 'savename' => $file_name);
        $this->phpqrcode->generate($config);
        $qr = file_get_contents($file_name);
        $qrimage = base64_encode($qr);
        return $qrimage;
    }

    public function generate_pdf_showWatermarkImage($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P', $showWatermarkImage=null)
    {
        if (!$output_type) {
            $output_type = 'D';
        }
        if (!$margin_bottom) {
            $margin_bottom = 5;
        }
        if (!$margin_top) {
            $margin_top = 5;
        }

        $constructor = [
            'mode' => '',
            'format' => 'A4',
            'default_font_size' => 0,
            'default_font' => '',
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => $margin_top,
            'margin_bottom' => $margin_bottom,
            'margin_header' => 9,
            'margin_footer' => 9,
            'orientation' => 'P',
        ];

        $mpdf                   = new Mpdf($constructor);
        $mpdf->debug            = (ENVIRONMENT == 'development');
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont   = true;

        $mpdf->SetTitle($this->Settings->site_name);
        $mpdf->SetAuthor($this->Settings->site_name);
        $mpdf->SetCreator($this->Settings->site_name);
        //$mpdf->SetProtection(array('copy', 'print', 'modify', 'annot-forms', 'fill-forms', 'extract', 'assemble'), '', '123456');
        $mpdf->SetDisplayMode('fullpage');

        if ($showWatermarkImage) {
            $mpdf->SetWatermarkImage($showWatermarkImage);
            $mpdf->showWatermarkImage = true;
        }

        $stylesheet = file_get_contents('assets/bs/bootstrap.min.css');
        $mpdf->WriteHTML($stylesheet, 1);

        if (is_array($content)) {
            $mpdf->SetHeader($this->Settings->site_name . '||{PAGENO}/{nbpg}', '', true); // For simple text header
            $as = sizeof($content);
            $r  = 1;
            foreach ($content as $page) {
                $mpdf->WriteHTML($page['content']);
                if (!empty($page['footer'])) {
                    $mpdf->SetHTMLFooter('<p class="text-center">' . $page['footer'] . '</p>', '', true);
                }
                if ($as != $r) {
                    $mpdf->AddPage();
                }
                $r++;
            }
        } else {
            $mpdf->WriteHTML($content);
            if ($header != '') {
                $mpdf->SetHTMLHeader('<p class="text-center">' . $header . '</p>', '', true);
            }
            if ($footer != '') {
                $mpdf->SetHTMLFooter('<p class="text-center">' . $footer . '</p>', '', true);
            }
        }

        if ($output_type == 'S') {
            $file_content = $mpdf->Output('', 'S');
            write_file('assets/uploads/' . $name, $file_content);
            return 'assets/uploads/' . $name;
        }
        $mpdf->Output($name, $output_type);
    }

    public function generate_pdf($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'A4', $showWatermarkImage=null)
    {
        if (!$output_type) {
            $output_type = 'D';
        }
        if (!$margin_bottom) {
            $margin_bottom = 5;
        }
        if (!$margin_top) {
            $margin_top = 5;
        }

        $constructor = [
            'mode' => '',
            'format' => $orientation,
            'default_font_size' => 0,
            'default_font' => '',
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => $margin_top,
            'margin_bottom' => $margin_bottom,
            'margin_header' => 9,
            'margin_footer' => 9,
            'orientation' => 'P',
        ];

        $mpdf                   = new Mpdf($constructor);
        $mpdf->debug            = (ENVIRONMENT == 'development');
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont   = true;

        $mpdf->SetTitle($this->Settings->site_name);
        $mpdf->SetAuthor($this->Settings->site_name);
        $mpdf->SetCreator($this->Settings->site_name);
        //$mpdf->SetProtection(array('copy', 'print', 'modify', 'annot-forms', 'fill-forms', 'extract', 'assemble'), '', '123456');
        $mpdf->SetDisplayMode('fullpage');

        $stylesheet = file_get_contents('assets/bs/bootstrap.min.css');
        $mpdf->WriteHTML($stylesheet, 1);

        if (is_array($content)) {
            $mpdf->SetHeader($this->Settings->site_name . '||{PAGENO}/{nbpg}', '', true); // For simple text header
            $as = sizeof($content);
            $r  = 1;
            foreach ($content as $page) {
                $mpdf->WriteHTML($page['content']);
                if (!empty($page['footer'])) {
                    $mpdf->SetHTMLFooter('<p class="text-center">' . $page['footer'] . '</p>', '', true);
                }
                if ($as != $r) {
                    $mpdf->AddPage();
                }
                $r++;
            }
        } else {
            $mpdf->WriteHTML($content);
            if ($header != '') {
                $mpdf->SetHTMLHeader('<p class="text-center">' . $header . '</p>', '', true);
            }
            if ($footer != '') {
                $mpdf->SetHTMLFooter('<p class="text-center">' . $footer . '</p>', '', true);
            }
        }

        if ($output_type == 'S') {
            $file_content = $mpdf->Output('', 'S');
            write_file('assets/uploads/' . $name, $file_content);
            return 'assets/uploads/' . $name;
        }
        $mpdf->Output($name, $output_type);
    }

    public function gerar_contrato_pdf($content, $name = 'download.pdf', $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P', $showWatermarkImage=null)
    {
        if (!$output_type) {
            $output_type = 'D';
        }
        if (!$margin_bottom) {
            $margin_bottom = 5;
        }
        if (!$margin_top) {
            $margin_top = 5;
        }

        $constructor = [
            'mode' => '',
            'format' => 'A4',
            'default_font_size' => 0,
            'default_font' => '',
            'margin_left' => 15,
            'margin_right' => 15,
            'margin_top' => $margin_top,
            'margin_bottom' => $margin_bottom,
            'margin_header' => 10,
            'margin_footer' => 10,
            'orientation' => 'P',
        ];

        $mpdf                   = new Mpdf($constructor);
        $mpdf->debug            = (ENVIRONMENT == 'development');
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont   = true;

        $mpdf->SetTitle($this->Settings->site_name);
        $mpdf->SetAuthor($this->Settings->site_name);
        $mpdf->SetCreator($this->Settings->site_name);
        //$mpdf->SetProtection(array('copy', 'print', 'modify', 'annot-forms', 'fill-forms', 'extract', 'assemble'));

        $mpdf->SetDisplayMode('fullpage');

        if ($showWatermarkImage) {
            $mpdf->SetWatermarkImage($showWatermarkImage);
            $mpdf->showWatermarkImage = true;
        }

        $stylesheet = file_get_contents('assets/bs/bootstrap.min.css');
        $mpdf->WriteHTML($stylesheet, 1);

        if (is_array($content)) {
            $mpdf->SetHeader($this->Settings->site_name . '||{PAGENO}/{nbpg}', '', true); // For simple text header
            $as = sizeof($content);
            $r  = 1;
            foreach ($content as $page) {
                $mpdf->WriteHTML($page['content']);
                if (!empty($page['footer'])) {
                    $mpdf->SetHTMLFooter('<p class="text-center">' . $page['footer'] . '</p>', '', true);
                }
                if ($as != $r) {
                    $mpdf->AddPage();
                }
                $r++;
            }
        } else {
            $mpdf->WriteHTML($content);
            if ($header != '') {
                $mpdf->SetHTMLHeader('<p class="text-center">' . $header . '</p>', '', true);
            }
            if ($footer != '') {
                $mpdf->SetHTMLFooter('<p class="text-center">' . $footer . '</p>', '', true);
            }
        }

        if ($output_type == 'S') {

            $file_content = $mpdf->Output('', 'S');

            $upload_path = 'assets/uploads/documents/'.$this->session->userdata('cnpjempresa');

            if (!file_exists($upload_path . '/')) {
                mkdir($upload_path , 0777, true);
            }

            write_file($upload_path . '/' . $name, $file_content);

            return $name;
        }
        $mpdf->Output($name, $output_type);
    }


    public function print_arrays()
    {
        $args = func_get_args();
        echo "<pre>";
        foreach ($args as $arg) {
            print_r($arg);
        }
        echo "</pre>";
        die();
    }

    public function logged_in()
    {
        return (bool) $this->session->userdata('identity');
    }

    public function in_group($check_group, $id = false)
    {
        if ( ! $this->logged_in()) {
            return false;
        }
        $id || $id = $this->session->userdata('user_id');
        $group = $this->site->getUserGroup($id);
        if ($group->name === $check_group) {
            return true;
        }
        return false;
    }

    public function log_payment($msg, $val = null)
    {
        $this->load->library('logs');
        return (bool) $this->logs->write('payments', $msg, $val);
    }

    public function update_award_points($total, $customer, $user, $scope = null)
    {
        if (!empty($this->Settings->each_spent) && $total >= $this->Settings->each_spent) {
            $company = $this->site->getCompanyByID($customer);
            $points = floor(($total / $this->Settings->each_spent) * $this->Settings->ca_point);
            $total_points = $scope ? $company->award_points - $points : $company->award_points + $points;
            $this->db->update('companies', array('award_points' => $total_points), array('id' => $customer));
        }
        if (!empty($this->Settings->each_sale) && !$this->Customer && $total >= $this->Settings->each_sale) {
            $staff = $this->site->getUser($user);
            $points = floor(($total / $this->Settings->each_sale) * $this->Settings->sa_point);
            $total_points = $scope ? $staff->award_points - $points : $staff->award_points + $points;
            $this->db->update('users', array('award_points' => $total_points), array('id' => $user));
        }
        return true;
    }

    public function zip($source = null, $destination = "./", $output_name = 'sma', $limit = 5000)
    {
        if (!$destination || trim($destination) == "") {
            $destination = "./";
        }

        $this->_rglobRead($source, $input);
        $maxinput = count($input);
        $splitinto = (($maxinput / $limit) > round($maxinput / $limit, 0)) ? round($maxinput / $limit, 0) + 1 : round($maxinput / $limit, 0);

        for ($i = 0; $i < $splitinto; $i++) {
            $this->_zip(array_slice($input, ($i * $limit), $limit, true), $i, $destination, $output_name);
        }

        unset($input);
        return;
    }

    public function unzip($source, $destination = './')
    {
        // @chmod($destination, 0777);
        $zip = new ZipArchive;
        if ($zip->open(str_replace("//", "/", $source)) === true) {
            $zip->extractTo($destination);
            $zip->close();
        }
        // @chmod($destination,0755);
        return true;
    }

    public function view_rights($check_id, $js = null)
    {
        if (!$this->Owner && !$this->Admin) {
            if ($check_id != $this->session->userdata('user_id')) {
                $this->session->set_flashdata('warning', $this->data['access_denied']);
                if ($js) {
                    die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome') . "'; }, 10);</script>");
                } else {
                    redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
                }
            }
        }
        return true;
    }

    public function makecomma($input)
    {
        if (strlen($input) <= 2) {return $input;}
        $length = substr($input, 0, strlen($input) - 2);
        $formatted_input = $this->makecomma($length) . "," . substr($input, -2);
        return $formatted_input;
    }

    public function formatSAC($num)
    {
        $pos = strpos((string) $num, ".");
        if ($pos === false) {$decimalpart = "00";} else {
            $decimalpart = substr($num, $pos + 1, 2);
            $num = substr($num, 0, $pos);}

        if (strlen($num) > 3 & strlen($num) <= 12) {
            $last3digits = substr($num, -3);
            $numexceptlastdigits = substr($num, 0, -3);
            $formatted = $this->makecomma($numexceptlastdigits);
            $stringtoreturn = $formatted . "," . $last3digits . "." . $decimalpart;
        } elseif (strlen($num) <= 3) {
            $stringtoreturn = $num . "." . $decimalpart;
        } elseif (strlen($num) > 12) {
            $stringtoreturn = number_format($num, 2);
        }

        if (substr($stringtoreturn, 0, 2) == "-,") {$stringtoreturn = "-" . substr($stringtoreturn, 2);}

        return $stringtoreturn;
    }

    public function md()
    {
        die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome') . "'; }, 10);</script>");
    }

    public function analyze_term($term)
    {
        $spos = strpos($term, $this->Settings->barcode_separator);
        if ($spos !== false) {
            $st = explode($this->Settings->barcode_separator, $term);
            $sr = trim($st[0]);
            $option_id = trim($st[1]);
        } else {
            $sr = $term;
            $option_id = false;
        }
        return array('term' => $sr, 'option_id' => $option_id);
    }

    public function paid_opts($paid_by = null, $purchase = false)
    {
        $opts = '
        <option value="cash"'.($paid_by && $paid_by == 'cash' ? ' selected="selected"' : '').'>'.lang("cash").'</option>
       <!-- <option value="gift_card"'.($paid_by && $paid_by == 'gift_card' ? ' selected="selected"' : '').'>'.lang("gift_card").'</option>!-->
        <option value="CC"'.($paid_by && $paid_by == 'CC' ? ' selected="selected"' : '').'>'.lang("CC").'</option>
        <!--<option value="Cheque"'.($paid_by && $paid_by == 'Cheque' ? ' selected="selected"' : '').'>'.lang("cheque").'</option>!-->
        <option value="other"'.($paid_by && $paid_by == 'other' ? ' selected="selected"' : '').'>'.lang("other").'</option>';
        if (!$purchase) {
            $opts .= '<option value="deposit"'.($paid_by && $paid_by == 'deposit' ? ' selected="selected"' : '').'>'.lang("deposit").'</option>';
            $opts .= '<option value="deposit2"'.($paid_by && $paid_by == 'deposit2' ? ' selected="selected"' : '').'>'.lang("deposit2").'</option>';

        }
        return $opts;
    }

    public function paid_opts_compact_cash($paid_by = null, $purchase = false)
    {
        $opts = '
        <option value="cash_deposit"'.($paid_by && $paid_by == 'cash_deposit' ? ' selected="selected"' : '').'>'.lang("cash_deposit").'</option>
        <option value="cash"'.($paid_by && $paid_by == 'cash' ? ' selected="selected"' : '').'>'.lang("somente_cash").'</option>
        <option value="deposit"'.($paid_by && $paid_by == 'deposit' ? ' selected="selected"' : '').'>'.lang("somente_deposit").'</option>
       <!-- <option value="gift_card"'.($paid_by && $paid_by == 'gift_card' ? ' selected="selected"' : '').'>'.lang("gift_card").'</option>!-->
        <option value="CC_other"'.($paid_by && $paid_by == 'CC_other' ? ' selected="selected"' : '').'>'.lang("CC_other").'</option>
        <option value="CC"'.($paid_by && $paid_by == 'CC' ? ' selected="selected"' : '').'>'.lang("somente_CC").'</option>
        <!--<option value="Cheque"'.($paid_by && $paid_by == 'Cheque' ? ' selected="selected"' : '').'>'.lang("cheque").'</option>!-->
        <option value="other"'.($paid_by && $paid_by == 'other' ? ' selected="selected"' : '').'>'.lang("somente_other").'</option>';

        return $opts;
    }

    public function calcularIdade($datadonascimento) {

        if ($datadonascimento == null || $datadonascimento == '') {
            return 0;
        }

        // Separa em dia, mês e ano
        list($ano, $mes, $dia) = explode('-', $datadonascimento);

        // Descobre que dia é hoje e retorna a unix timestamp
        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        // Descobre a unix timestamp da data de nascimento do fulano
        $diadonascimento = mktime( 0, 0, 0, $mes, $dia, $ano);

        // Depois apenas fazemos o cálculo já citado :)
        $idade = floor((((($hoje - $diadonascimento) / 60) / 60) / 24) / 365.25);

        return (int) $idade;
    }

    public function send_json($data)
    {
        header('Content-Type: application/json');
        die(json_encode($data));
        exit;
    }

    public function diferencaEntreDuasDatasEmDias($dataInicio, $dataFinal) {

        if ($dataInicio == null) return 0;
        if ($dataFinal == null) return 0;

        $dataInicio = strtotime($dataInicio);
        $dataFinal = strtotime($dataFinal);

        $diferenca = $dataFinal - $dataInicio; // 19522800 segundos
        $dias = (int)floor( $diferenca / (60 * 60 * 24)); // 225 dias

        return $dias;
    }

    public function mostrarDiasVencimento($status, $dataVencimento) {

        if ($status === Financeiro_model::STATUS_CANCELADA) return '';
        if ($status === Financeiro_model::STATUS_REPARCELADA) return '';
        if ($status === Financeiro_model::STATUS_PAGO) return '';

        if(!$this->isContaVencida($dataVencimento)) return '<br/><small>&nbsp;</small>';

        $dias = $this->diasDeAtrasoConta($dataVencimento);

        if ($dias > 1) return '<br/><small>-'.$dias.' '.lang('dias').'</small>';
        else return '<br/><small>-'.$dias.' '.lang('dia').'</small>';

    }

    public function row_status_agendamento($status) {
        if($status == null) {
            return '';
        } else if($status === 'ORCAMENTO') {
            return '<div class="text-center"><span class="label label-warning">'.lang($status).'</span></div>';
        } else if($status === 'BLOQUEADO') {
            return '<div class="text-center"><span class="label label-warning">'.lang($status).'</span></div>';
        } else if($status === 'PAGO') {
            return '<div class="text-center"><span class="label label-success">'.lang($status).'</span></div>';
        } else if($status === 'REALIZADO') {
            return '<div class="text-center"><span class="label label-info">'.lang($status).'</span></div>';
        } else if($status === 'AGUARDANDO_PAGAMENTO') {
            return '<div class="text-center"><span class="label label-danger">'.lang($status).'</span></div>';
        } else if ($status === 'CANCELADA') {
            return '<div class="text-center"><span class="label label-default">'.lang($status).'</span></div>';
        } else {
            return '<div class="text-center"><span class="label label-default">'.lang($status).'</span></div>';
        }
    }

    public function forma_pagamento_design($tipo, $title , $status, $dataVencimento) {

        $title = lang('pagamento_com').' '.$title;

        if ($status == 'CANCELADA') return '<i class="fa fa-warning" title="'.$title.'" style="font-size: 14px;color: black;"></i>';
        if ($status == 'ARQUIVADA') return '<i class="fa fa-warning" title="'.$title.'" style="font-size: 14px;color: red;"></i>';

        if ($tipo == 'dinheiro') return '<i class="fa fa-money" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'boleto') return '<i class="fa fa-barcode" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'cartao') return '<i class="fa fa-credit-card"  title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'contravale') return '<i class="fa fa-dollar" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'cheque') return '<i class="fa fa-pencil-square-o" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'boleto_cartao') return '<i class="fa fa-credit-card" title="'.$title.'" style="font-size: 14px;"></i> <i class="fa fa-barcode" style="font-size: 14px;"></i>';
        if ($tipo == 'carne') return '<i class="fa fa-newspaper-o" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'carne_boleto') return '<i class="fa fa-fa-barcode" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'carne_cartao') return '<i class="fa fa-credit-card" title="'.$title.'" style="font-size: 14px"></i>';
        if ($tipo == 'carne_boleto_cartao') return '<i class="fa fa-credit-card" title="'.$title.'" style="font-size: 14px;"></i> <i class="fa fa-barcode" style="font-size: 14px;"></i>';
        if ($tipo == 'credito_loja') return '<i class="fa fa-money" title="'.$title.'" style="font-size: 14px;"></i>';
        if ($tipo == 'outros') return '<i class="fa fa-question" title="'.$title.'" style="font-size: 14px;"></i>';
    }

    public function billing_status_design($status, $dataVencimento) {

        $atrasada = FALSE;

        if($this->isContaVencida($dataVencimento)) $atrasada = TRUE;

        if ($status == 'ABERTA' || $status == 'FATURADA' || $status == 'PARCIAL' || $status == 'VENCIDA') {
            if ($atrasada) {
                return '<span class="label label-danger">'.lang('vencida').'</span>';
            } else if ($status == 'ABERTA' || $status == 'FATURADA' ) {
                return '<span class="label label-warning" style="background-color: #0044cc;" >'.lang($status).'</span>';
            } else {
                return '<span class="label label-info" >'.lang($status).'</span>';
            }
        } else if ($status == 'QUITADA') {
            return '<span class="label label-success"  style="background-color: #4AB858;">'.lang($status).'</span>';
        } else if ($status == 'CANCELADA'){
            return '<span class="label label-danger" style="background-color: black;">'.lang($status).'</span>';
        } else if ($status == 'ARQUIVADA') {
            return '<span class="label label-danger">'.lang($status).'</span>';
        } else if ($status == 'REPARCELADA') {
            return '<span class="label label-warning" style="background-color: #51eef0;color: black;text-decoration: line-through;">'.lang($status).'</span>';
        } else if ($status == 'REEMBOLSO') {
            return '<span class="label label-danger">'.lang('REEMBOLSO').'</span>';
        } else {
            return $status;
        }
    }

    public function diasDeAtrasoConta($dataVencimento) {
        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));
        return $this->diferencaEntreDuasDatasEmDias($dataVencimento, $dataHoje);
    }

    public function getJurosMora($valorPagar, $dataVencimento) {

        $moraDia = $this->getPercentualJuros();

        if($this->isContaVencida($dataVencimento) && $moraDia > 0) {
            $dias = $this->diasDeAtrasoConta($dataVencimento);

            return $valorPagar * $moraDia * $dias;
        }

        return 0;
    }

    public function getPercentualMulta() {
        return $this->Settings->percentualMulta;
    }

    public function getPercentualJuros() {
        return $this->Settings->percentualJurosMoraDia;
    }

    public function getValorMulta($valorPagar, $dataVencimento) {

        $percentualMulta = $this->getPercentualMulta();

        if( $this->isContaVencida($dataVencimento) && $percentualMulta > 0)  {
            return $valorPagar * $percentualMulta/100;
        }

        return 0;
    }

    public function getValorVencimentoComMultaJuros($valorPagar, $dataVencimento) {
        return $valorPagar + $this->getValorMulta($valorPagar, $dataVencimento) + $this->getJurosMora($valorPagar, $dataVencimento);
    }

    public function isContaVencida($dataVencimento) {
        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));
        return strtotime($dataVencimento) < strtotime($dataHoje);
    }

    function curlExec($url, $post = NULL, array $header = array()){

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if(count($header) > 0) curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        if($post !== null) curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post, '', '&'));
        //Ignore SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    function getTokenPagSeguro() {
        $pagSeguro = $this->settings_model->getPagSeguroSettings();

        if (!$pagSeguro->active) return '';

        if ($pagSeguro->sandbox) {
            $PAGSEGURO_API_URL = 'https://ws.sandbox.pagseguro.uol.com.br/v2';
            $token = $pagSeguro->account_token_sandbox;
        } else {
            $PAGSEGURO_API_URL = 'https://ws.pagseguro.uol.com.br/v2';
            $token = $pagSeguro->account_token;
        }

        $params = array(
            'email' => $pagSeguro->account_email,
            'token' => $token
        );

        $response = $this->curlExec($PAGSEGURO_API_URL . "/sessions", $params, array());

        $json = json_decode(json_encode(simplexml_load_string($response)));
        $sessionCode = $json->id;

        return $sessionCode;
    }

    function extenso($valor = 0, $maiusculas = false) {
        if(!$maiusculas){
            $singular = ["centavo", "real", "mil", "milhões", "bilhições", "trilhões", "quatrilhões"];
            $plural = ["centavos", "reais", "mil", "milhões", "bilhições", "trilhões", "quatrilhões"];
            $u = ["", "um", "dois", "tres", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
        }else{
            $singular = ["CENTAVO", "REAL", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
            $plural = ["CENTAVOS", "REAIS", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
            $u = ["", "UM", "DOIS", "TRES", "QUANTRO", "CINCO", "SEIS",  "SETE", "OITO", "NOVE"];
        }

        $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
        $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
        $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];

        $z = 0;
        $rt = "";

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for($i=0;$i<count($inteiro);$i++)
            for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
                $inteiro[$i] = "0".$inteiro[$i];

        $fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
        for ($i=0;$i<count($inteiro);$i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd &&
                    $ru) ? " e " : "").$ru;
            $t = count($inteiro)-1-$i;
            $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")$z++; elseif ($z > 0) $z--;
            if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
            if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if(!$maiusculas){
            $return = $rt ? $rt : "zero";
        } else {
            if ($rt) $rt = str_replace(" E "," e ",ucwords($rt));
            $return = ($rt) ? ($rt) : "Zero" ;
        }

        if(!$maiusculas){
            return utf8_encode(str_replace(" E "," e ",ucwords(trim($return))));
        }else{
            return utf8_encode(strtoupper(trim($return)));
        }
    }

    public function tirarAcentos($string)
    {

        $string = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);

        $string = str_replace(',', '_', $string);
        $string = str_replace('!', '', $string);
        $string = str_replace('Ç', 'C', $string);
        $string = str_replace('ç', 'c', $string);
        $string = str_replace('/', '_', $string);
        $string = str_replace('°', '_', $string);
        $string = str_replace(':', '_', $string);

        return  $string;
    }

    function getIdade($dataNascimento) {

        if ($dataNascimento == null) {
            return '';
        }

        list($ano, $mes, $dia) = explode('-',$dataNascimento);

        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

        return $idade;
    }

    public function slug($title)
    {
        $this->load->helper('text');
        $slug       = url_title(convert_accented_characters($title), '-', true);
        return $slug;
    }

    public function removeSpecialCharacters($string) {
        // Remove emojis e outros caracteres não ASCII
        $string = preg_replace('/[^\x20-\x7E]/', '', $string);
        return $string;
    }

    public function isCPFObrigaNovoCadastroEhValidaCPFCNJ($numero = null): bool
    {
        if (empty($numero)) {
            return false;
        }

        $numero = preg_replace("/[^0-9]/", "", $numero);

        if (strlen($numero) == 11) {
            return $this->validarCPFNumerosSequencial($numero);
        } elseif (strlen($numero) == 14) {
            return $this->validarCNPJNumeroSequencial($numero);
        } else {
            return false;
        }
    }

    private function validarCPFNumerosSequencial($cpf): bool
    {
        // Verifica sequências repetidas
        if (preg_match('/^(\d)\1{10}$/', $cpf)) {
            return false;
        }
        return true;
    }

    private function validarCNPJNumeroSequencial($cnpj): bool
    {
        // Verifica sequências repetidas
        if (preg_match('/^(\d)\1{13}$/', $cnpj)) {
            return false;
        }
        return true;
    }

}
