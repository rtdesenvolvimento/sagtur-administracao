<?php defined('BASEPATH') OR exit('No direct script access allowed');


class JunoV2 {

    public $version = 2;
    public $sandbox = true;

    public $token   = 'F6C94B6AA911575AB0635BAA91FF85E154050A6BF96F7B920F88A99CE29C48A2';
    public $public_token = '3EEBF048CD688C560FD8A7CC93BCB662E7CA35A238C71125950F630861D1922B';
    public $client_id = 'F0zwfI5xwAqhVylR';
    public $client_secret = 'aN98h,+BZnkIH0si<5&FMyK#bxpnYh.+';

    public function __construct() {}

    public function fetchBalance () {
        $response = $this->do_request( 'balance', 'GET' );

        return $response;
    }

    private function do_request( $endpoint, $method = 'POST') {

        $url    = $this->get_api_endpoint($endpoint);

        $params = array(
            'method'  => $method,
            'timeout' => 60,
            'headers' => array(
                'content-type'     => 'application/json;charset=UTF-8',
                'accept-charset'   => 'utf-8',
                'x-platform'       => 'pluginresultatecjuno',
                'X-Api-Version'    => $this->version,
                'X-Resource-Token' => $this->token,
                'Authorization'    => 'Bearer ' . $this->get_authorization_token()
            )
        );

        return $this->get( $url, $params );
    }

    public function get_authorization_token() {

        $endpoint = $this->get_api_endpoint( 'oauth/token', true );

        $param =  array(
            'method' => 'POST',
            'timeout' => 30,
            'headers' => array(
                'Authorization' => 'Basic ' . base64_encode( $this->client_id . ':' . $this->client_secret ),
                'content-type' => 'application/x-www-form-urlencoded'
            ),
            'body' => array(
                'grant_type' => 'client_credentials'
            ));

        $response = $this->post($endpoint, $param);

        return $response;
    }

    protected function get_api_endpoint( $endpoint, $authorization = false ) {
        $base = $this->sandbox ? 'sandbox.boletobancario.com/api-integration' : 'api.juno.com.br';

        if ( $authorization ) {
            $base = $this->sandbox ? 'sandbox.boletobancario.com/authorization-server' : 'api.juno.com.br/authorization-server';
        }

        return "https://$base/$endpoint";
    }

    private function post($url, $param) {
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, 'grant_type=client_credentials');

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'content-type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic ' . base64_encode( $this->client_id . ':' . $this->client_secret ),
            )
        );
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function get($url, $param) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url );
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($param));


        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}