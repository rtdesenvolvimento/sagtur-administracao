<?php

use GuzzleHttp\Client;

class Whatsapp
{
    private $client;
    private $url = 'https://api.plugzapi.com.br';
    private $headers;

    public $instance;
    public $token;
    public $client_token;

    public function __construct()
    {
        $this->client = new \GuzzleHttp\Client();
    }

    /**
     * @throws Exception
     */
    public function send_text($message, $phone)
    {
        $data = array(
            'message' => urldecode($message),
            'phone' => $phone,
          //  'delayMessage' => 5,
        );
        return $this->sendRequest('/send-text', 'POST', $data, PlugMessage::class);
    }

    /**
     * @throws Exception
     */
    public function send_link($message, $linkUrl, $title, $linkDescription, $image,  $phone)
    {
        $data = array(
            'message' => urldecode($message),
            'phone' => $phone,
            'linkUrl' => $linkUrl,
            'title' => $title,
            'linkDescription' => $linkDescription,
            'image' => $image,
            //  'delayMessage' => 5,
        );
        return $this->sendRequest('/send-link', 'POST', $data, PlugMessage::class);
    }


    /**
     * @throws Exception
     */
    public function send_document_pdf($document_link, $fileName,  $phone)
    {
        $data = array(
            'document' => $this->convertPdfToBase64($document_link),
            'fileName' => $fileName,
            'phone' => $phone,
            //  'delayMessage' => 5,
        );
        return $this->sendRequest('/send-document/pdf', 'POST', $data, PlugMessage::class);
    }

    /**
     * @throws Exception
     */
    public function create_group($groupName, $phones = [])
    {
        $data = array(
            'groupName' => $groupName,
            'phones' => $phones,
        );
        return $this->sendRequest('/create-group', 'POST', $data, GroupMessage::class);
    }

    /**
     * @throws Exception
     */
    public function add_participant_group($groupId, $phones = []): bool
    {
        $data = array(
            'groupId' => $groupId,
            'phones' => $phones,
        );
        $retorno =  $this->sendRequest('/add-participant', 'POST', $data);

        return (bool) $retorno;
    }

    /**
     * @throws Exception
     */
    public function remove_participant_group($groupId, $phones = []): bool
    {
        $data = array(
            'groupId' => $groupId,
            'phones' => $phones,
        );
        $retorno =  $this->sendRequest('/remove-participant', 'POST', $data);

        return (bool) $retorno;
    }

    /**
     * @throws Exception
     */
    public function add_admin_group($groupId, $phones = []): bool
    {
        $data = array(
            'groupId' => $groupId,
            'phones' => $phones,
        );

        $retorno =  $this->sendRequest('/add-admin', 'POST', $data);

        return (bool) $retorno;
    }

    /**
     * @throws Exception
     */
    public function queue()
    {
        $retorno = $this->sendRequest('/queue', 'GET', []);

        return $retorno;
    }

    /**
     * @throws Exception
     */
    public function get_contact($phone)
    {
        return $this->sendRequest('/contacts/'.$phone, 'GET', [], Phone::class);
    }

    /**
     * @throws Exception
     */
    public function get_contacts()
    {
        return $this->sendRequest('/contacts?pageSize=15000', 'GET', [], Phones::class);
    }

    /**
     * @throws Exception
     */
    public function group_metadata($groupId)
    {
        return $this->sendRequest('/group-metadata/'.$groupId, 'GET', [], GroupMetadata::class);
    }

    function convertPdfToBase64($pdfUrl): string
    {
        $pdfContent = file_get_contents($pdfUrl);

        if ($pdfContent === false) {
            throw new Exception("Falha ao baixar o PDF." . $pdfUrl);
        }

        return 'data:application/pdf;base64,' . base64_encode($pdfContent);
    }

    private function sendRequest($endpoint, $method = 'POST', $params = [], $responseClass = null)
    {
        try {

            $this->headers = [
                'Client-Token' => $this->client_token,
                'Content-Type' => 'application/json',
            ];

            if (!empty($params)) {
                if ($this->hasFile($params)) {
                    $options['multipart'] = $this->prepareMultipartData($params);
                } else {
                    $options['headers']['Content-Type'] = 'application/json';
                    $options['headers']['Client-Token'] = $this->client_token;
                    $options['json'] = $params;
                }
            } else {
                $options['headers']['Content-Type'] = 'application/json';
                $options['headers']['Client-Token'] = $this->client_token;
            }

            $response = $this->client->request($method, $this->url. '/instances/' . $this->instance . '/token/' . $this->token . $endpoint, $options);
            $body = $response->getBody();

            // Verifica se é um tipo de arquivo binário (PDF, Word, Imagem, etc.)
            $contentType = $response->getHeaderLine('Content-Type');
            if (preg_match('/application\/(pdf|msword|vnd.openxmlformats-officedocument.wordprocessingml.document)/', $contentType) ||
                preg_match('/image\/(jpeg|png|gif)/', $contentType)) {

                // Define o nome do arquivo para download ou exibição
                $fileName = "file" . $this->getFileExtensionFromContentType($contentType);

                // Define os cabeçalhos apropriados para exibir ou baixar o arquivo
                header('Content-Type: ' . $contentType);
                header('Content-Disposition: inline; filename="' . basename($fileName) . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');
                echo $body; // Exibe o conteúdo do arquivo
                exit;
            }

            $data = json_decode((string)$body, true);

            if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
                if ($responseClass && class_exists($responseClass)) {
                    return new $responseClass($data);
                }
                return $data;
            } else {
                throw new \Exception("Erro: " . $response->getReasonPhrase());
            }
        } catch (\GuzzleHttp\Exception\ClientException $e) {
            $responseBody = json_decode($e->getResponse()->getBody(), true);
            $errorMessage = $this->mapErrorMessage($responseBody['message']) ?? 'Erro desconhecido';
            $errors = $responseBody['errors'] ?? [];

            $detailedErrors = '';
            foreach ($errors as $field => $messages) {
                $translatedField = $this->mapFieldName($field);
                foreach ($messages as $message) {
                    $translatedMessage = $this->mapErrorMessage($message);
                    $detailedErrors .= $translatedField . ': ' . $translatedMessage . "<br/>";
                }
            }

            if ($errorMessage) {
                throw new \Exception("Mensagem de Erro: " . $errorMessage . "<br/>Detalhes:<br/>" . $detailedErrors);
            } else {
                throw new \Exception("Erro: ". $this->mapErrorMessage($responseBody['error']) . ' Code ' . $e->getResponse()->getReasonPhrase());
            }

        } catch (\GuzzleHttp\Exception\ServerException $e) {
            throw new \Exception("Server error: " . $e->getResponse()->getBody());
        } catch (\Exception $e) {
            throw new \Exception("Error : " . $e->getMessage());
        }
    }

    private function getFileExtensionFromContentType($contentType): string
    {
        $map = [
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'image/gif' => '.gif',
        ];

        return $map[$contentType] ?? '';
    }

    private function hasFile($params): bool
    {
        foreach ($params as $key => $value) {
            if (is_resource($value) || (is_array($value) && isset($value['file'])) && isset($value['company_brand'])) {
                return true;
            }
        }
        return false;
    }

    private function prepareMultipartData($params): array
    {
        $multipartData = [];
        foreach ($params as $key => $value) {
            if (is_resource($value) || (is_array($value) && isset($value['file']))) {
                $multipartData[] = [
                    'name' => $key,
                    'contents' => $value,
                ];
            } else {
                $multipartData[] = [
                    'name' => $key,
                    'contents' => (string)$value,
                ];
            }
        }
        return $multipartData;
    }

    private function mapFieldName($field): string
    {
        $fieldMap = [
            'name' => 'Nome',
            // Adicione outros campos conforme necessário
        ];

        return $fieldMap[$field] ?? ucfirst($field);
    }

    private function mapErrorMessage($message): string
    {
        if ($message === null) {
            return '';
        }

        $messageMap = [
            'The name has already been taken.' => 'O nome já foi escolhido.',
            'The given data was invalid.' => 'Os dados fornecidos eram inválidos.',
            'The name field is required.' => 'O campo nome é obrigatório.',
            'The file field is required.' => 'O campo arquivo é obrigatório.',
            'Token not found!' => 'Token Inválido',
            'Status not pending' => 'Status não pendente',
            'file not found!' => 'Arquivo não encontrado',
            'Name exists' => 'Nome já existe',
            'doc not found' => 'Documento não encontrado',
            'The page field is required.' => 'O campo página é obrigatório.',
            'The company brand must be an image.' => 'A marca da empresa deve ser uma imagem.',
            'The company brand must be a file of type: png, jpg, jpeg.' => 'A marca da empresa deve ser um arquivo do tipo: png, jpg, jpeg.',
            'Too Many Attempts.' => 'Muitas tentativas.',
            'Folder not found' => 'Pasta não encontrada',
            'The email has already been taken.' => 'O email já foi escolhido.',
            'The cert password must be a string.' => 'A senha do certificado deve ser uma string.',
        ];

        return $messageMap[$message] ?? $message;
    }

}

class PlugMessage {

    public $id;
    public $zaapId;
    public $messageId;

    public function __construct($data) {
        $this->zaapId = $data['zaapId'];
        $this->messageId = $data['messageId'];
        $this->id = $data['id'];
    }
}

class GroupMessage {

    public $phone;
    public $invitationLink;

    public function __construct($data) {
        $this->phone = $data['phone'];
        $this->invitationLink = $data['invitationLink'];
    }
}


class GroupMetadata {
    public string $phone;
    public string $owner;
    public string $subject;
    public int $creation;
    public array $participants;
    public int $subjectTime;
    public string $subjectOwner;

    public function __construct(array $data) {

        if ($data['phone']) {
            $this->phone = $data['phone'];
            $this->owner = $data['owner'];
            $this->subject = $data['subject'];
            $this->creation = $data['creation'];
            $this->participants = array_map(function ($participant) {
                return new Participant($participant);
            }, $data['participants']);
            $this->subjectTime = $data['subjectTime'];
            $this->subjectOwner = $data['subjectOwner'];
        }
    }
}

class Participant {
    public string $phone;
    public bool $isAdmin;
    public bool $isSuperAdmin;
    public ?string $short; // Pode ser nulo
    public ?string $name;  // Pode ser nulo

    public function __construct(array $data) {
        $this->phone = $data['phone'];
        $this->isAdmin = $data['isAdmin'];
        $this->isSuperAdmin = $data['isSuperAdmin'];
        $this->short = $data['short'] ?? null;
        $this->name = $data['name'] ?? null;
    }
}


class Phone {
    public $name;
    public $phone;
    public $notify;
    public $short;
    public $imgUrl;

    public function __construct(array $data) {
        $this->name = $data['name'];
        $this->phone = $data['phone'];
        $this->notify = $data['notify'];
        $this->short = $data['short'];
        $this->imgUrl = $data['imgUrl'];
    }
}

class Phones
{
    public array $contacts = [];

    public function __construct(array $data) {

        foreach ($data as $contact) {
            $this->contacts[] = [
                "name" => $contact['name'],
                "short" => $contact['short'],
                "notify" => $contact['notify'],
                "vcard" => $contact['vcard'],
                "phone" => $contact['phone']
            ];
        }
    }
}