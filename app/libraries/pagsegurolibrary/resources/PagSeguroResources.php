<?php

// Production environment
$PagSeguroResources['environment'] = array();

$PagSeguroResources['environment']['production']['webserviceUrl'] = "https://ws.pagseguro.uol.com.br";


//$PagSeguroResources['environment']['development']['webserviceUrl'] = "https://ws.sandbox.pagseguro.uol.com.br";
$PagSeguroResources['environment']['development']['webserviceUrl'] = "https://ws.pagseguro.uol.com.br";


// Payment link
$PagSeguroResources['paymentServiceCheckout'] = array();
$PagSeguroResources['paymentServiceCheckout']['servicePath'] = "/v2/checkout";


//$PagSeguroResources['paymentServiceCheckout']['checkoutUrl'] = "https://sandbox.pagseguro.uol.com.br/v2/checkout/payment.html";
$PagSeguroResources['paymentServiceCheckout']['checkoutUrl'] = "https://pagseguro.uol.com.br/v2/checkout/payment.html";


$PagSeguroResources['paymentServiceCheckout']['serviceTimeout'] = 20;












// Payment service
$PagSeguroResources['paymentService'] = array();
$PagSeguroResources['paymentService']['servicePath'] = "/v2/transactions";
$PagSeguroResources['paymentService']['checkoutUrl'] = "https://pagseguro.uol.com.br/v2/checkout/payment.html";
$PagSeguroResources['paymentService']['serviceTimeout'] = 20;

// Notification service
$PagSeguroResources['notificationService'] = array();
$PagSeguroResources['notificationService']['servicePath'] = "/v2/transactions/notifications";
$PagSeguroResources['notificationService']['serviceTimeout'] = 20;

// Transaction search service
$PagSeguroResources['transactionSearchService'] = array();
$PagSeguroResources['transactionSearchService']['servicePath'] = "/v2/transactions";
$PagSeguroResources['transactionSearchService']['serviceTimeout'] = 20;

// Notification service cancel
$PagSeguroResources['notificationServiceCancel'] = array();
$PagSeguroResources['notificationServiceCancel']['servicePath'] = "/v2/transactions/cancels";
$PagSeguroResources['notificationServiceCancel']['serviceTimeout'] = 20;


// Notification service refunds
$PagSeguroResources['notificationServiceEstorno'] = array();
$PagSeguroResources['notificationServiceEstorno']['servicePath'] = "/v2/transactions/refunds";
$PagSeguroResources['notificationServiceEstorno']['serviceTimeout'] = 20;