<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Itinerary_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id) {

        $this->db->select('itinerario.*, 
                            tipo_trajeto_veiculo.name as tipo_trajeto, 
                            products.name as servico, 
                            m.name as motorista, g.name as guia, tipo_transporte_rodoviario.name as tipo_transporte');

        $this->db->join('companies g', 'g.id = itinerario.guia', 'left');
        $this->db->join('companies m', 'm.id = itinerario.motorista', 'left');
        $this->db->join('tipo_trajeto_veiculo', 'tipo_trajeto_veiculo.id=itinerario.tipo_trajeto_id', 'left');
        $this->db->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id = itinerario.tipo_transporte_id', 'left');
        $this->db->join('products', 'products.id = itinerario.product_id', 'left');

        $q = $this->db->get_where('itinerario', array('itinerario.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItemById($id) {
        $q = $this->db->get_where('itinerario_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItens($id, $group_by = false) {

        $this->db->select('sale_items.*, 
                            sales.*,  sales.id as sale_id, 
                            l.id as localEmbarqueId,  
                            l.name as localEmbarque, 
                            l.endereco, l.complemento, 
                            l.numero, 
                            l.bairro, 
                            l.cidade, 
                            l.estado, 
                            l.cep');
        $this->db->join('sale_items', 'sale_items.id=itinerario_items.sale_item');
        $this->db->join('sales', 'sales.id=sale_items.sale_id');
        $this->db->join('local_embarque l', 'l.id=sale_items.localEmbarque', 'left');

        if ($group_by) {
            $this->db->group_by('sales.id, sale_items.product_id, l.id');
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->db->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->db->where("payment_status in ('due','partial','paid') ");
        }

        $this->db->order_by('sale_items.hora_embarque');

        $q = $this->db->get_where('itinerario_items', array('itinerario_items.itineario' => $id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addItinerary($data) {

        if ($this->site->getReference('os') == $data['reference_no']) {
            $this->site->updateReference('os');
        }

        if ($this->db->insert('itinerario', $data)) {
            $itinerario_id = $this->db->insert_id();
            return $itinerario_id;
        }
        return false;
    }

    public function updateItinerary($id, $data) {

        $this->db->update('itinerario', $data, array('id' => $id));

        return $id;
    }

    public function addItem($itineary_id, $data) {

        $data['itineario'] = $itineary_id;

        if ($this->db->insert('itinerario_items', $data)) {

            $itineary = $this->getById($itineary_id);

            $data_item = array(
                'guia' => $itineary->guia,
                'motorista' => $itineary->motorista,
                'tipoTransporte' => $itineary->tipo_transporte_id,
            );
            $this->db->update('sale_items', $data_item, array('id' => $data['sale_item']));

            return true;
        }
        return false;
    }

    public function removeItem($id) {

        $item = $this->getItemById($id);

        if ($this->db->delete('itinerario_items', array('id' => $id))) {
            $data_item = array(
                'guia' => NULL,
                'motorista' => NULL,
                'tipoTransporte' => NULL,
            );
            $this->db->update('sale_items', $data_item, array('id' => $item->sale_item));
        }

        return  true;
    }

    public function delete($id)
    {
        if ($this->db->delete('itinerario', array('id' => $id)) && $this->db->delete('itinerario_items', array('itineario' => $id)) ) {
            return true;
        }
        return FALSE;

    }

    public function getAllInvoiceItems($sale_id, $product_id = NULL, $embarque_id = NULL,  $exibirApenasServicos = false)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            tipo_trajeto_veiculo.name as tipo_trajeto,
            products.code,
            products.isTaxasComissao,  
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tipo_trajeto_veiculo', 'products.tipo_trajeto_id=tipo_trajeto_veiculo.id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')

            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->join('sales', 'sales.id = sale_items.sale_id')

            ->order_by('sale_items.customerClient, sale_items.adicional, sale_items.id, sale_items.tipoDestino');

        if ($exibirApenasServicos) {
            $this->db->where('adicional', false);
        }

        if ($product_id) {
            $this->db->where('sale_items.product_id', $product_id);
        }

        if ($embarque_id) {
            $this->db->where('sale_items.localEmbarque', $embarque_id);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->db->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->db->where("payment_status in ('due','partial','paid') ");
        }

        $q = $this->db->get_where('sale_items', array('sale_items.sale_id' => $sale_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}