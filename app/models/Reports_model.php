<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Reports_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('id, code, name')
            ->like('name', $term, 'both')->or_like('code', $term, 'both');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaff()
    {
        if ($this->Admin) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSalesTotals($customer_id)
    {

        $this->db->select('SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('customer_id', $customer_id);
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCustomerSales($customer_id)
    {
        $this->db->from('sales')->where('customer_id', $customer_id);
        return $this->db->count_all_results();
    }

    public function getCustomerQuotes($customer_id)
    {
        $this->db->from('quotes')->where('customer_id', $customer_id);
        return $this->db->count_all_results();
    }

    public function getCustomerReturns($customer_id)
    {
        $this->db->from('return_sales')->where('customer_id', $customer_id);
        return $this->db->count_all_results();
    }

    public function getStockValue()
    {
        $q = $this->db->query("SELECT SUM(by_price) as stock_by_price, SUM(by_cost) as stock_by_cost FROM ( Select COALESCE(sum(" . $this->db->dbprefix('warehouses_products') . ".quantity), 0)*price as by_price, COALESCE(sum(" . $this->db->dbprefix('warehouses_products') . ".quantity), 0)*cost as by_cost FROM " . $this->db->dbprefix('products') . " JOIN " . $this->db->dbprefix('warehouses_products') . " ON " . $this->db->dbprefix('warehouses_products') . ".product_id=" . $this->db->dbprefix('products') . ".id GROUP BY " . $this->db->dbprefix('products') . ".id )a");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseStockValue($id)
    {
        $q = $this->db->query("SELECT SUM(by_price) as stock_by_price, SUM(by_cost) as stock_by_cost FROM ( Select sum(COALESCE(" . $this->db->dbprefix('warehouses_products') . ".quantity, 0))*price as by_price, sum(COALESCE(" . $this->db->dbprefix('warehouses_products') . ".quantity, 0))*cost as by_cost FROM " . $this->db->dbprefix('products') . " JOIN " . $this->db->dbprefix('warehouses_products') . " ON " . $this->db->dbprefix('warehouses_products') . ".product_id=" . $this->db->dbprefix('products') . ".id WHERE " . $this->db->dbprefix('warehouses_products') . ".warehouse_id = ? GROUP BY " . $this->db->dbprefix('products') . ".id )a", array($id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    // public function getmonthlyPurchases()
    // {
    //     $myQuery = "SELECT (CASE WHEN date_format( date, '%b' ) Is Null THEN 0 ELSE date_format( date, '%b' ) END) as month, SUM( COALESCE( total, 0 ) ) AS purchases FROM purchases WHERE date >= date_sub( now( ) , INTERVAL 12 MONTH ) GROUP BY date_format( date, '%b' ) ORDER BY date_format( date, '%m' ) ASC";
    //     $q = $this->db->query($myQuery);
    //     if ($q->num_rows() > 0) {
    //         foreach (($q->result()) as $row) {
    //             $data[] = $row;
    //         }
    //         return $data;
    //     }
    //     return FALSE;
    // }

    public function getChartData()
    {
        $myQuery = "SELECT S.month,
        COALESCE(S.sales, 0) as sales,
        COALESCE( P.purchases, 0 ) as purchases,
        COALESCE(S.tax1, 0) as tax1,
        COALESCE(S.tax2, 0) as tax2,
        COALESCE( P.ptax, 0 ) as ptax
        FROM (  SELECT  date_format(date, '%Y-%m') Month,
                SUM(total) Sales,
                SUM(product_tax) tax1,
                SUM(order_tax) tax2
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL 12 MONTH )
                GROUP BY date_format(date, '%Y-%m')) S
            LEFT JOIN ( SELECT  date_format(date, '%Y-%m') Month,
                        SUM(product_tax) ptax,
                        SUM(order_tax) otax,
                        SUM(total) purchases
                        FROM " . $this->db->dbprefix('purchases') . "
                        GROUP BY date_format(date, '%Y-%m')) P
            ON S.Month = P.Month
            GROUP BY S.Month
            ORDER BY S.Month";
        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDailySales($year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
			FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
			GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMonthlySales($year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
			FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y' ) =  '{$year}'
			GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffDailySales($user_id, $year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('sales')." WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffMonthlySales($user_id, $year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('sales') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasesTotals($supplier_id)
    {
        $this->db->select('SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('supplier_id', $supplier_id);
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSupplierPurchases($supplier_id)
    {
        $this->db->from('purchases')->where('supplier_id', $supplier_id);
        return $this->db->count_all_results();
    }

    public function getStaffPurchases($user_id)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('created_by', $user_id);
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStaffSales($user_id)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid', FALSE)
            ->where('created_by', $user_id);
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalSales($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
            ->where('sale_status !=', 'pending');
           // ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalPurchases($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE);
            //->where('status', 'received')
          ///  ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalExpenses($start, $end, $warehouse_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(amount, 0)) as total_amount', FALSE);
           // ->where('date BETWEEN ' . $start . ' and ' . $end);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalPaidAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'sent')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedCashAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'cash')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedCCAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'CC')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedChequeAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'Cheque')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedPPPAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'ppp')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReceivedStripeAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'received')->where('paid_by', 'stripe')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalReturnedAmount($start, $end)
    {
        $this->db->select('count(id) as total, SUM(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where('type', 'returned')
            ->where('date BETWEEN ' . $start . ' and ' . $end);
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseTotals($warehouse_id = NULL)
    {
        $this->db->select('sum(quantity) as total_quantity, count(id) as total_items', FALSE);
        $this->db->where('quantity !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('warehouses_products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCosting($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', FALSE);
        if ($date) {
            $this->db->where('costing.date', $date);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('costing.date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('costing.date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->join('sales', 'sales.id=costing.sale_id')
            ->where('sales.warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpenses($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }
        

        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturns($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
        ->where('sale_status', 'returned');
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getOrderDiscount($date, $warehouse_id = NULL, $year = NULL, $month = NULL)
    {
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $this->db->select('SUM( COALESCE( order_discount, 0 ) ) AS order_discount', FALSE);
        if ($date) {
            $this->db->where('date >=', $sdate)->where('date <=', $edate);
        } elseif ($month) {
            $this->load->helper('date');
            $last_day = days_in_month($month, $year);
            $this->db->where('date >=', $year.'-'.$month.'-01 00:00:00');
            $this->db->where('date <=', $year.'-'.$month.'-'.$last_day.' 23:59:59');
        }

        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getExpenseCategories()
    {
        $q = $this->db->get('expense_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDailyPurchases($year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getMonthlyPurchases($year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffDailyPurchases($user_id, $year, $month, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%e' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases')." WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y-%m' ) =  '{$year}-{$month}'
            GROUP BY DATE_FORMAT( date,  '%e' )";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaffMonthlyPurchases($user_id, $year, $warehouse_id = NULL)
    {
        $myQuery = "SELECT DATE_FORMAT( date,  '%c' ) AS date, SUM( COALESCE( product_tax, 0 ) ) AS tax1, SUM( COALESCE( order_tax, 0 ) ) AS tax2, SUM( COALESCE( grand_total, 0 ) ) AS total, SUM( COALESCE( total_discount, 0 ) ) AS discount, SUM( COALESCE( shipping, 0 ) ) AS shipping
            FROM " . $this->db->dbprefix('purchases') . " WHERE ";
        if ($warehouse_id) {
            $myQuery .= " warehouse_id = {$warehouse_id} AND ";
        }
        $myQuery .= " created_by = {$user_id} AND DATE_FORMAT( date,  '%Y' ) =  '{$year}'
            GROUP BY date_format( date, '%c' ) ORDER BY date_format( date, '%c' ) ASC";
        $q = $this->db->query($myQuery, false);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsDespesas($unit=null, $data_saida=null, $data_retorno=null)
    {
        $this->db->select("products.*,
         categories.name categoria, sum(grand_total) grand_total , 
         sum(paid) paid , sum(grand_total-paid) as balance", FALSE);
        $this->db->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code','left');
        $this->db->join('categories', 'categories.id=products.category_id','left');

        if ($unit) {
            $this->db->where('products.unit', $unit);
        }

        if ($data_saida) {
            $this->db->where('products.data_saida >=', $data_saida);
        }

        if ($data_retorno) {
            $this->db->where('products.data_retorno <=', $data_retorno);
        }

        $this->db->group_by('products.id');

        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllProducts($unit=null, $data_saida=null, $data_retorno=null)
    {
        $this->db->select("products.*,
         categories.name categoria, sum(grand_total) grand_total , 
         sum(paid) paid , sum(grand_total-paid) as balance", FALSE);
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code','left');
        $this->db->join('categories', 'categories.id=products.category_id','left');

        if ($unit) {
            $this->db->where('products.unit', $unit);
        }

        if ($data_saida) {
            $this->db->where('products.data_saida >=', $data_saida);
        }

        if ($data_retorno) {
            $this->db->where('products.data_retorno <=', $data_retorno);
        }

        $this->db->group_by('products.id');

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function  getPurchasesComissao($warehouse=null , $biller=null,  $status_pagamento=null ) {

        $this->db->select("
            purchases.date, 
            purchases.warehouse_id,
            product_variants.name despesa,
            reference_no,
            products.name as nmProducts,
            supplier,
            grand_total grand_total,
            sum(paid) paid,
            warehouses.name as nmWarehouses,
            sum(grand_total-paid) as balance, 
            payment_status, 
            purchases.supplier_id,
            purchases.supplier,
            sum(sma_purchase_items.subtotal) subtotal,
            purchases.vencimento,
            purchases.id as id", FALSE);

        $this->db->join('purchase_items', 'purchase_items.purchase_id=purchases.id');
        $this->db->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');
        $this->db->join('product_variants', 'product_variants.id=purchases.option_id', 'left');
        $this->db->join('companies', 'companies.id=purchases.supplier_id', 'left');

        $this->db->where('companies.group_name', 'biller');
        $this->db->where('grand_total > 0');

        $this->db->group_by('purchases.id');

        if ($status_pagamento) {
            if ($status_pagamento == 'atrasada') {
                $this->db->where('purchases.vencimento < ', date('Y-m-d'));
            } else {
                $this->db->where('purchases.payment_status', $status_pagamento);
            }
        }

        /*
        if ($vencimento_de) {
            $this->db->where('purchases.vencimento >= ', $vencimento_de);
        }

        if ($vencimento_ate) {
            $this->db->where('purchases.vencimento <= ', $vencimento_de);
        }
        */

        if ($biller && $biller != 'todos') $this->db->where('purchases.supplier_id', $biller);

        if ($warehouse) {
            if ($warehouse == 'outras_receitas') {
                $this->db->where('products.name is null');
            } else {
                $this->db->where('purchases.warehouse_id', $warehouse);
            }
        }

        $this->db->order_by('purchases.id desc');

        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function  getPurchases($status_pagamento=null, $vencimento_de=null, $vencimento_ate=null, $supplier=null, $warehouse=null, $customer_group=NULL) {

        $this->db->select("
            purchases.date, 
            product_variants.name despesa,
            reference_no,
            products.name as nmProducts,
            supplier,
            sum(grand_total) grand_total,
            sum(paid) paid,
            warehouses.name as nmWarehouses,
            sum(grand_total-paid) as balance, 
            payment_status, 
            purchases.supplier_id,
            purchases.supplier,
            sum(`sma_purchase_items`.subtotal) subtotal,
            purchases.vencimento,
            purchases.id as id", FALSE);
        $this->db->join('purchase_items', 'purchase_items.purchase_id=purchases.id','left');
        $this->db->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');
        $this->db->join('product_variants', 'product_variants.id=purchases.option_id', 'left');
        $this->db->join('companies', 'companies.id=purchases.supplier_id', 'left');

        $this->db->group_by('purchases.id');

        if ($status_pagamento) {
            if ($status_pagamento == 'atrasada') {
                $this->db->where('purchases.vencimento < ', date('Y-m-d'));
            } else {
                $this->db->where('purchases.payment_status', $status_pagamento);
            }
        }

        if ($vencimento_de) {
            $this->db->where('purchases.vencimento >= ', $vencimento_de);
        }

        if ($vencimento_ate) {
            $this->db->where('purchases.vencimento <= ', $vencimento_de);
        }

        if ($supplier) {
            $this->db->where('purchases.supplier_id', $supplier);
        }

        if ($warehouse) {
            if ($warehouse == 'outras_receitas') {
                $this->db->where('products.name is null');
            } else {
                $this->db->where('purchases.warehouse_id', $warehouse);
            }
        }

        if ($customer_group) {
            $this->db->where('companies.supplier_group_id', $customer_group);
        }

        $this->db->order_by('purchases.id desc');

        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSalesProgramacao($vendaFilter) {

        //$vendaFilter = new VendaFilterDTO_model();

        $this->db->select("
            date, 
            reference_no,
            biller,
            products.name as nmProducts,
            customer,
            grand_total,
            paid,
            warehouses.name as nmWarehouses,
            (grand_total-paid) as balance, 
            payment_status, 
            sale_status,
            sale_items.customerClient,
            sale_items.quantity,
            sale_items.subtotal,
            sale_items.poltronaClient,
            sale_items.faixaNome,
            sale_items.descontarVaga,
            sales.vencimento,
            sales.id as id", FALSE);
        $this->db->join('sale_items', 'sale_items.sale_id=sales.id','left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');
        $this->db->join('companies', 'sale_items.customerClient=companies.id', 'left');

        if ($vendaFilter->getProgramacaoId() != null) {
            $this->db->where('sale_items.programacaoId', $vendaFilter->getProgramacaoId());
        }

        if ($vendaFilter->getStatusPagamento() != null) {
            $this->db->where('payment_status', $vendaFilter->getStatusPagamento());
        }

        if ($vendaFilter->getClienteId() != null) {
            $this->db->where('sale_items.customerClient', $vendaFilter->getClienteId());
        }

        if ($vendaFilter->sale_status != null) {
            $this->db->where($vendaFilter->sale_status);
        } else {
            $this->db->where('(sale_status = "orcamento" or sale_status = "faturada" or  sale_status = "lista_espera" )');
        }

        $this->db->where('sale_items.adicional', '0');

        if ($vendaFilter->order_by != null) {
            $this->db->order_by('IF(sma_companies.cf5 IS NULL OR sma_companies.cf5 = "", 1, 0)', FALSE);
            $this->db->order_by('companies.cf5 ASC, companies.phone DESC, sales.id DESC');
        } else {
            $this->db->order_by('sales.id desc');
        }

        $q = $this->db->get('sales');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function  getSales($status_pagamento=null, $vencimento_de=null, $vencimento_ate=null, $customer=null, $warehouse=null) {

        $this->db->select("
            date, 
            reference_no,
            biller,
            products.name as nmProducts,
            customer,
            grand_total,
            paid,
            warehouses.name as nmWarehouses,
            (grand_total-paid) as balance, 
            payment_status, 
            sale_items.customerClient,
            sale_items.quantity,
            sale_items.subtotal,
            sale_items.poltronaClient,
            sales.vencimento,
            sales.id as id", FALSE);
        $this->db->join('sale_items', 'sale_items.sale_id=sales.id','left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');

        if ($status_pagamento) {
            if ($status_pagamento == 'atrasada') {
                $this->db->where('sales.vencimento < ', date('Y-m-d'));
            } else {
                $this->db->where('sales.payment_status', $status_pagamento);
            }
        }

        if ($vencimento_de) {
            $this->db->where('sales.vencimento >= ', $vencimento_de);
        }

        if ($vencimento_ate) {
            $this->db->where('sales.vencimento <= ', $vencimento_de);
        }

        if ($customer) {
            $this->db->where('sale_items.customerClient', $customer);
        }

        if ($warehouse) {
            if ($warehouse == 'outras_receitas') {
                $this->db->where('products.name is null');
            } else {
                $this->db->where('sales.warehouse_id', $warehouse);
            }
        }

        $this->db->order_by('sales.id desc');

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsGroupByType() {

        $this->db->select("payments.date, 
            payments.reference_no as payment_ref, 
            payments.reference_no as sale_ref, 
            payments.reference_no as purchase_ref,
             paid_by,sum(amount) amount, 
             type, payments.id as id");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->where('type', 'received');
        $this->db->group_by('payments.paid_by');


        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getPayments($paid_by=null, $data_pagamento_de=null,$data_pagamento_ate=null,$valor_de, $valor_ate, $cliente, $warehouse) {

        $this->db->select("
            payments.date date, 
            payments.note,
            payments.reference_no as payment_ref, 
            payments.reference_no as sale_ref, 
            payments.reference_no as purchase_ref,
            paid_by, 
            amount, 
            payments.type, 
            payments.id as id, 
            sales.customer,
            sales.id sale_id");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('sale_items', 'sale_items.sale_id=sales.id','left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');
        $this->db->where('payments.type', 'received');

        if ($paid_by && $paid_by != 'Todos') {

            if ($paid_by == 'cash_deposit') {
                $this->db->where("(paid_by = 'cash' or paid_by = 'deposit')");
            } else  if ($paid_by == 'CC_other'){
                $this->db->where("(paid_by = 'CC' or paid_by = 'other')");
            } else {
                $this->db->where('paid_by', $paid_by);
            }
        }

        if ($data_pagamento_de) {
            $this->db->where(" date_format(`sma_payments`.`date` , '%Y-%m-%d') >= ", $data_pagamento_de);
        }

        if ($data_pagamento_ate) {
            $this->db->where(" date_format(`sma_payments`.`date`, '%Y-%m-%d') <= ", $data_pagamento_ate);
        }

        if ($valor_de) {
            $this->db->where('amount >= ', $valor_de);
        }

        if ($valor_ate) {
            $this->db->where('amount <= ', $valor_ate);
        }

        if ($cliente) {
            $this->db->where('sale_items.customerClient', $cliente);
        }

        if ($warehouse) {
            if ($warehouse == 'outras_receitas') {
                $this->db->where('products.name is null');
            } else {
                $this->db->where('warehouses.id', $warehouse);
            }
        }

        $this->db->group_by('payments.id');
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getPaymentsDespesas($paid_by=null, $data_pagamento_de=null,$data_pagamento_ate=null,$valor_de, $valor_ate, $cliente, $warehouse, $customer_group=NULL) {

        $this->db->select("
            payments.date date, 
            payments.note,
            payments.reference_no as payment_ref, 
            payments.reference_no as sale_ref, 
            payments.reference_no as purchase_ref,
            paid_by, 
            amount, 
            payments.type, 
            payments.id as id, 
            purchases.supplier_id,
            purchases.supplier,
            purchases.id purchases_id");
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('purchase_items', 'purchase_items.purchase_id=purchases.id','left');
        $this->db->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
        $this->db->join('products', 'products.code=warehouses.code', 'left');
        $this->db->join('companies', 'companies.id=purchases.supplier_id', 'left');
        $this->db->where('payments.type', 'sent');

        if ($paid_by && $paid_by != 'Todos') {

            if ($paid_by == 'cash_deposit') {
                $this->db->where("(paid_by = 'cash' or paid_by = 'deposit')");
            } else  if ($paid_by == 'CC_other'){
                $this->db->where("(paid_by = 'CC' or paid_by = 'other')");
            } else {
                $this->db->where('paid_by', $paid_by);
            }
        }

        if ($data_pagamento_de) {
            $this->db->where(" date_format(`sma_payments`.`date` , '%Y-%m-%d') >= ", $data_pagamento_de);
        }

        if ($data_pagamento_ate) {
            $this->db->where(" date_format(`sma_payments`.`date`, '%Y-%m-%d') <= ", $data_pagamento_ate);
        }

        if ($valor_de) {
            $this->db->where('amount >= ', $valor_de);
        }

        if ($valor_ate) {
            $this->db->where('amount <= ', $valor_ate);
        }

        if ($cliente) {
            $this->db->where('purchases.supplier_id', $cliente);
        }

        if ($warehouse) {
            if ($warehouse == 'outras_receitas') {
                $this->db->where('products.name is null');
            } else {
                $this->db->where('warehouses.id', $warehouse);
            }
        }

        if ($customer_group) {
            $this->db->where('companies.supplier_group_id', $customer_group);
        }

        $this->db->group_by('payments.id');
        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsDinheiro($sales_id) {

        $this->db->select("sum(amount) as amount");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->where('type', 'received');
        $this->db->where("(paid_by = 'cash' or paid_by = 'deposit'or paid_by = 'deposit2')");
        $this->db->where('payments.sale_id', $sales_id);
        $this->db->limit(1);

        return $this->db->get('payments')->row();
    }

    public function getPaymentsDinheiroDespesa($purchase_id) {

        $this->db->select("sum(amount) as amount");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->where('type', 'sent');
        $this->db->where("(paid_by = 'cash' or paid_by = 'deposit')");
        $this->db->where('payments.purchase_id', $purchase_id);
        $this->db->limit(1);

        return $this->db->get('payments')->row();

    }

    public function getPaymentsCartao($sales_id) {

        $this->db->select("sum(amount) as amount");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->where('type', 'received');
        $this->db->where("(paid_by = 'CC' or paid_by = 'other')");
        $this->db->where('payments.sale_id', $sales_id);
        $this->db->limit(1);

        return $this->db->get('payments')->row();

    }

    public function getPaymentsCartaoDespesa($purchase_id) {

        $this->db->select("sum(amount) as amount");
        $this->db->join('sales', 'payments.sale_id=sales.id', 'left');
        $this->db->join('purchases', 'payments.purchase_id=purchases.id', 'left');
        $this->db->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        $this->db->where('type', 'sent');
        $this->db->where("(paid_by = 'CC' or paid_by = 'other')");
        $this->db->where('payments.purchase_id', $purchase_id);
        $this->db->limit(1);

        return $this->db->get('payments')->row();
    }

    public function getSumPaymentsVendaAndFormaPagamento($sale_id, $formaPagamento)
    {
        $this->db->select("sum(amount) as amount");
        $this->db->where('sale_id', $sale_id);
        $this->db->where('formapagamento', $formaPagamento);
        $this->db->limit(1);

        return $this->db->get('payments')->row();
    }

}
