<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProgramacaoFilter_DTO_model extends CI_Model
{

    public $produto;
    public $category;
    public $embarque;
    public $destaque = false;

    public $ocultarViagensPassadas = true;
    public $verificarConfiguracaoOcultarProdutosSemEstoque = true;

    public $status;
    public $mes;
    public $ano;
    public $dia;

    public $promotion = false;
    public $date_shedule;
    public $group_by_date = false;

    public $group_data_website = false;

    public $enviar_site = true;

    public $apenas_agendas_ativa = true;

    public $order_by_custom = '';

    public $limit = false;
    public $offset = false;


    public $limit_by_category = false;
    public $limit_by_data = false;

    public function __construct() {
        parent::__construct();
    }

}