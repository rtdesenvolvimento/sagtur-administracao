<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BusDTO_model extends CI_Model
{

    public $item_id;
    public $product_id;
    public $programacao_id;
    public $tipo_transporte_id;

    public $assento;

    public $assentoStr;

    public $note;
    public function __construct() {
        parent::__construct();
    }
}
