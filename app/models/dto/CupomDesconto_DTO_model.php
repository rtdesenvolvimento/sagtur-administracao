<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CupomDesconto_DTO_model extends CI_Model
{

    public $status;
    public $cupom;
    public $produto;
    public $quantidade_disponibilizada;
    public $dataInicio;
    public $dataFinal;
    public $programacao_id;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCupom()
    {
        return $this->cupom;
    }

    /**
     * @param mixed $cupom
     */
    public function setCupom($cupom): void
    {
        $this->cupom = $cupom;
    }

    /**
     * @return mixed
     */
    public function getProduto()
    {
        return $this->produto;
    }

    /**
     * @param mixed $produto
     */
    public function setProduto($produto): void
    {
        $this->produto = $produto;
    }

    /**
     * @return mixed
     */
    public function getQuantidadeDisponibilizada()
    {
        return $this->quantidade_disponibilizada;
    }

    /**
     * @param mixed $quantidade_disponibilizada
     */
    public function setQuantidadeDisponibilizada($quantidade_disponibilizada): void
    {
        $this->quantidade_disponibilizada = $quantidade_disponibilizada;
    }

    /**
     * @return mixed
     */
    public function getDataInicio()
    {
        return $this->dataInicio;
    }

    /**
     * @param mixed $dataInicio
     */
    public function setDataInicio($dataInicio): void
    {
        $this->dataInicio = $dataInicio;
    }

    /**
     * @return mixed
     */
    public function getDataFinal()
    {
        return $this->dataFinal;
    }

    /**
     * @param mixed $dataFinal
     */
    public function setDataFinal($dataFinal): void
    {
        $this->dataFinal = $dataFinal;
    }

    /**
     * @return mixed
     */
    public function getProgramacaoId()
    {
        return $this->programacao_id;
    }

    /**
     * @param mixed $programacao_id
     */
    public function setProgramacaoId($programacao_id): void
    {
        $this->programacao_id = $programacao_id;
    }


}
