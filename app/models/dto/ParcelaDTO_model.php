<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParcelaDTO_model extends CI_Model
{

    private $dataVencimentoDe;
    private $dataVencimentoAte;
    private $dataPagamentoDe;
    private $dataPagamentoAte;
    private $tipoCobranca;
    private $filial;
    private $condicaoPagamento;
    private $receita;
    private $despesa;
    private $cliente;
    private $status;
    private $movimentador;
    private $numeroDocumento;
    private $programacao;
    private $categoria;
    private $vendedor;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getDataVencimentoDe()
    {
        return $this->dataVencimentoDe;
    }

    /**
     * @param mixed $dataVencimentoDe
     */
    public function setDataVencimentoDe($dataVencimentoDe)
    {
        $this->dataVencimentoDe = $dataVencimentoDe;
    }

    /**
     * @return mixed
     */
    public function getDataVencimentoAte()
    {
        return $this->dataVencimentoAte;
    }

    /**
     * @param mixed $dataVencimentoAte
     */
    public function setDataVencimentoAte($dataVencimentoAte)
    {
        $this->dataVencimentoAte = $dataVencimentoAte;
    }

    /**
     * @return mixed
     */
    public function getTipoCobranca()
    {
        return $this->tipoCobranca;
    }

    /**
     * @param mixed $tipoCobranca
     */
    public function setTipoCobranca($tipoCobranca)
    {
        $this->tipoCobranca = $tipoCobranca;
    }

    /**
     * @return mixed
     */
    public function getReceita()
    {
        return $this->receita;
    }

    /**
     * @param mixed $receita
     */
    public function setReceita($receita)
    {
        $this->receita = $receita;
    }

    /**
     * @return mixed
     */
    public function getMovimentador()
    {
        return $this->movimentador;
    }

    /**
     * @param mixed $movimentador
     */
    public function setMovimentador($movimentador)
    {
        $this->movimentador = $movimentador;
    }

    /**
     * @return mixed
     */
    public function getCondicaoPagamento()
    {
        return $this->condicaoPagamento;
    }

    /**
     * @param mixed $condicaoPagamento
     */
    public function setCondicaoPagamento($condicaoPagamento)
    {
        $this->condicaoPagamento = $condicaoPagamento;
    }

    /**
     * @return mixed
     */
    public function getFilial()
    {
        return $this->filial;
    }

    /**
     * @param mixed $filial
     */
    public function setFilial($filial)
    {
        $this->filial = $filial;
    }

    /**
     * @return mixed
     */
    public function getCliente()
    {
        return $this->cliente;
    }

    /**
     * @param mixed $cliente
     */
    public function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDataPagamentoDe()
    {
        return $this->dataPagamentoDe;
    }

    /**
     * @param mixed $dataPagamentoDe
     */
    public function setDataPagamentoDe($dataPagamentoDe)
    {
        $this->dataPagamentoDe = $dataPagamentoDe;
    }

    /**
     * @return mixed
     */
    public function getDataPagamentoAte()
    {
        return $this->dataPagamentoAte;
    }

    /**
     * @param mixed $dataPagamentoAte
     */
    public function setDataPagamentoAte($dataPagamentoAte)
    {
        $this->dataPagamentoAte = $dataPagamentoAte;
    }

    /**
     * @return mixed
     */
    public function getNumeroDocumento()
    {
        return $this->numeroDocumento;
    }

    /**
     * @param mixed $numeroDocumento
     */
    public function setNumeroDocumento($numeroDocumento)
    {
        $this->numeroDocumento = $numeroDocumento;
    }

    /**
     * @return mixed
     */
    public function getDespesa()
    {
        return $this->despesa;
    }

    /**
     * @param mixed $despesa
     */
    public function setDespesa($despesa)
    {
        $this->despesa = $despesa;
    }

    /**
     * @return mixed
     */
    public function getProgramacao()
    {
        return $this->programacao;
    }

    /**
     * @param mixed $programacao
     */
    public function setProgramacao($programacao)
    {
        $this->programacao = $programacao;
    }

    /**
     * @return mixed
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param mixed $categoria
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return mixed
     */
    public function getVendedor()
    {
        return $this->vendedor;
    }

    /**
     * @param mixed $vendedor
     */
    public function setVendedor($vendedor)
    {
        $this->vendedor = $vendedor;
    }
}
