<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InstallDTO_model extends CI_Model
{
    public $db_name;
    public $customer_id;
    public $nome_empresa;
    public $theme_color;
    public $logo = null;
    PUBLIC $criar_subdominio = true;

    public function __construct() {
        parent::__construct();
    }
}
