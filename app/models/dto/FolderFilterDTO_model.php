<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FolderFilterDTO_model extends CI_Model
{

    public $statusDocument;
    public $biller_id;
    public $strFolder;

    public function __construct()
    {
        parent::__construct();
    }

}