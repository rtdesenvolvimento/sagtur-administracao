<?php defined('BASEPATH') OR exit('No direct script access allowed');

class BaixaFaturaCobrancaDTO_model extends CI_Model
{
    public $code;
    public $reference;
    public $payNumber;
    public $valorVencimento;
    public $dtVencimento;
    public $pagamentos = [];
    public $log;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarPagamento($pagamento) {
        $this->pagamentos[count($this->pagamentos)] = $pagamento;
    }
}
