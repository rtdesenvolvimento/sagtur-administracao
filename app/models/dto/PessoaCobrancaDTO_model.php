<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PessoaCobrancaDTO_model extends CI_Model
{

    public $id;
    public $nome;
    public $cpfCnpj;
    public $email;
    public $endereco;
    public $numero;
    public $complemento;
    public $bairro;
    public $cidade;
    public $estado;
    public $cep;
    public $telefone;
    public $celular;
    public $tipoPessoa;
    public $dataNascimento;

    public $telefonePagSeguro;
    public $dddTelefonePagSeguro;

    public $sexo;
    public $rg;

    public function __construct() {
        parent::__construct();
    }
}
