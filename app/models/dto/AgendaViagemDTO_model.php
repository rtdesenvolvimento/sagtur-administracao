<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaViagemDTO_model extends CI_Model
{

    public $produto;
    public $vagas;
    public $preco;
    public $active = true;

    public $percentualMaximoDesconto;
    public $sinalMinimoPagamento;
    public $doDia;
    public $aoDia;
    public $horaSaida;
    public $horaRetorno;

    public $habilitar_marcacao_area_cliente;
    public $permitir_marcacao_dependente;
    public $data_inicio_marcacao;
    public $data_final_marcacao;

    public $cupons;

    public $agenda_programacao_id;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarCupom($cupom) {
        $this->cupons[count($this->cupons)] = $cupom;
    }

}