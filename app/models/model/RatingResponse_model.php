<?php defined('BASEPATH') OR exit('No direct script access allowed');

class RatingResponse_model extends CI_Model
{

    public $rating_id;
    public $question_id;
    public $type_question;
    public $question;
    public $rating;
    public $response;
    public $created_at;

    public function __construct() {
        parent::__construct();
    }

}
