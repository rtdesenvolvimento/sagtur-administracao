<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Automovel_model extends CI_Model
{
    public $name;
    public $andares;
    public $total_assentos;
    public $note;

    private $assentos;

    public function __construct() {
        parent::__construct();
    }

    public function addAssento($assento) {
        $this->assentos[count($this->assentos)] = $assento;
    }

    /**
     * @return mixed
     */
    public function getAssentos()
    {
        return $this->assentos;
    }

    /**
     * @param mixed $assentos
     */
    public function setAssentos($assentos): void
    {
        $this->assentos = $assentos;
    }


}
