<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Venda_model extends CI_Model
{

    const STATUS_ORCAMENTO = 'orcamento';
    const STATUS_FATURADA = 'faturada';
    const STATUS_LISTA_EMPERA = 'lista_espera';

    public $data;
    public $conta_receber = null;
    public $valorVencimentos = null;
    public $vencimentos = null;
    public $tiposCobranca = null;
    public $descontos = null;
    public $movimentadores = null;
    public $products  = array();
    public $apenasEditar;

    public function __construct() {
        parent::__construct();
    }

}
