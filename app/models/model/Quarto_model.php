<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Quarto_model extends CI_Model
{
    public $tipo_hospedagem = null;

    public $name = '';

    public $programcao_id  = null;

    public $total_faturadas = 0;

    public $total_canceladas = 0;

    public $total_orcamentos = 0;

    public $total_lista_espera = 0;

    public $acomoda       = 0;//quantas pessoas acomodam

    public $estoque_acomodacao = 0;
    public $estoque_total = 0;
    public $estoque_atual = 0;

    public $qtd_quartos_disponivel = 0;


    public function __construct() {
        parent::__construct();
    }
}