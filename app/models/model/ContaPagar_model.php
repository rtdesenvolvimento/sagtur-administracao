<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContaPagar_model extends CI_Model
{

    const STATUS_ABARTA = 'ABERTA';

    public $id = '';
    public $data = array();
    public $valores = [];
    public $vencimentos = [];
    public $cobrancas = [];
    public $movimentadores = [];
    public $descontos = [];
    public $pagamentos = [];

    public function __construct() {
        parent::__construct();
    }

    public static function create($data, FechamentoComissaoDTO_model $fechamentoDTO, $valorComissao): ContaPagar_model
    {
        $instance = new self();
        $instance->data           = $data;
        $instance->vencimentos    = $fechamentoDTO->vencimentos;
        $instance->cobrancas      = $fechamentoDTO->tiposCobranca;
        $instance->movimentadores = $fechamentoDTO->movimentadores;
        $instance->valores = [];
        $instance->descontos = [];
        $totalCobrancas = count($fechamentoDTO->tiposCobranca);

        for ($i = 0; $i < $totalCobrancas; $i++) {
            $instance->valores[]    = $valorComissao / $totalCobrancas;
            $instance->descontos[]  = 0;
        }

        return $instance;
    }

}
