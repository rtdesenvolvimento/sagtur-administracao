<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Signatures_model extends CI_Model
{

    const STATUS_UNSIGNED = 'unsigned';//nao assinado

    const STATUS_SIGNED = 'signed';//assinado


    const STATUS_CANCELED = 'canceled';//cancelado

    const STATUS_REMOVED = 'removed';//pendente

    const STATUS_REJECTED = 'rejected';//rejeitado


    public $id;
    public $document_id;

    public $status = '';

    public $customer;
    public $customer_id;

    public $is_biller = 0;
    public $biller;
    public $biller_id;

    public $name;
    public $phone;
    public $email;

    public $public_id;
    public $url_link;

    public $note = '';

    public $removed  = 0;
    public $canceled = 0;

    public $date_removed;
    public $date_canceled;

    //log
    public $created_at;
    public $created_by;

    public $signed = 0;
    public $date_signature;

    public function __construct($id = null) {
        parent::__construct();

        if ($id !== null) {
            $this->loadById($id);
        }
    }

    public function loadById($id): ?Signatures_model
    {
        $query = $this->db->get_where('document_signatures', array('id' => $id));

        if ($query->num_rows() > 0) {

            $row = $query->row();

            $this->id = $row->id;
            $this->status = $row->status;

            $this->document_id = $row->document_id;

            $this->customer = $row->customer;
            $this->customer_id = $row->customer_id;

            $this->is_biller = $row->is_biller;
            $this->biller = $row->biller;
            $this->biller_id = $row->biller_id;

            $this->name = $row->name;
            $this->email = $row->email;
            $this->phone = $row->phone;

            $this->public_id = $row->public_id;
            $this->url_link = $row->url_link;

            $this->signed = $row->signed;
            $this->date_signature = $row->date_signature;

            $this->created_at = $row->created_at;
            $this->created_by = $row->created_by;

            return $this;
        }

        return null;
    }


}
