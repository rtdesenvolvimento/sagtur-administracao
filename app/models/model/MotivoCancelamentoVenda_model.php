<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MotivoCancelamentoVenda_model extends CI_Model
{
    public $sale_id;
    public $note;

    public $motivo_cancelamento_id;
    public $customer_id;
    public $customer;


    public $gerar_credito = false;
    public $value;
    public $expiry;
    public $note_credito;
    public $card_no;


    public $gerar_reembolso = false;
    public $value_refund;
    public $expiry_refund;
    public $note_refund;
    public $card_no_refund;


    public function __construct()
    {
        parent::__construct();
    }

}
