<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Checkin_model extends CI_Model
{

    const IN = 'in';
    const OUT = 'out';

    public $checkin_location_id;
    public $sale_id;
    public $customer_id;
    public $programacao_id;
    public $note;
    public $type = 'in';

    public function __construct() {
        parent::__construct();
    }

}
