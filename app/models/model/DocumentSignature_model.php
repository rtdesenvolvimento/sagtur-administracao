<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Documentsignature_model extends CI_Model
{
    public $id;
    public $email;
    public $telefone;
    public $name;
    public $link;
    public $action;

    public function __construct()
    {
        parent::__construct();
    }

}
