<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentData_model extends CI_Model
{

    public $id;
    public $document_key;
    public $download;
    public $extension;
    public $size;


    public $signatures;

    public function __construct(){
        parent::__construct();
    }

    public function addSignature($signature) {
        $this->signatures[] = $signature;
    }

    public function getSignature(int $index): ?DocumentSignature_model
    {
        return $this->signatures[$index] ?? null;
    }
}
