<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Parcela_model extends CI_Model
{
    const STATUS_CANCELADA = 'CANCELADA';
    const STATUS_REEMBOLSO = 'REEMBOLSO';

    const STATUS_QUITADA = 'QUITADA';

    public function __construct() {
        parent::__construct();
    }

}
