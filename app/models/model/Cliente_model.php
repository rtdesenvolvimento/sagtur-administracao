<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente_model extends CI_Model
{

    public $id;

    public $tipoPessoa;
    public $group_id;
    public $name;
    public $email;
    public $group_name;
    public $company;
    public $nome_responsavel;
    public $data_aniversario;
    public $tipo_documento;
    public $validade_rg_passaporte;
    public $address;
    public $vat_no;//cpf
    public $cf1;//rg
    public $cf4;//naturalidade

    public $sexo;
    public $phone;//telefone
    public $cf5;//celular

    public $cf2;
    public $cf3;//orgao emissor
    public $cf6;

    public $observacao;
    public $plano_saude;
    public $alergia_medicamento;
    public $doenca_informar;
    public $telefone_emergencia;

    public $created_by;
    public $data_cadastro;

    public $customer_group_id;
    public $customer_group_name;


    public $faixaId;
    public $faixaName;

    public $bloqueado;

    public $faixaEtariaValor;

    public $descontarVaga;

    public $social_name;
    public $profession;


    //endereco
    public $postal_code;//cep
    public $bairro;
    public $numero;
    public $complemento;
    public $city;//cidade
    public $state;//estado
    public $country;//pais


    public $ultimo_assento;

    public $ultimo_valor_extra_assento;

    public $ultima_nota;

    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    /**
     * @param mixed $tipoPessoa
     */
    public function setTipoPessoa($tipoPessoa)
    {
        $this->tipoPessoa = $tipoPessoa;
    }

    /**
     * @return mixed
     */
    public function getGroupId()
    {
        return $this->group_id;
    }

    /**
     * @param mixed $group_id
     */
    public function setGroupId($group_id)
    {
        $this->group_id = $group_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }


    /**
     * @return mixed
     */
    public function getGroupName()
    {
        return $this->group_name;
    }

    /**
     * @param mixed $group_name
     */
    public function setGroupName($group_name)
    {
        $this->group_name = $group_name;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getDataAniversario()
    {
        $data =  $this->data_aniversario;

        if ($data == '--') return null;

        return $this->data_aniversario;
    }

    /**
     * @param mixed $data_aniversario
     */
    public function setDataAniversario($data_aniversario)
    {
        $this->data_aniversario = $data_aniversario;
    }

    /**
     * @return mixed
     */
    public function getTipoDocumento()
    {
        return $this->tipo_documento;
    }

    /**
     * @param mixed $tipo_documento
     */
    public function setTipoDocumento($tipo_documento)
    {
        $this->tipo_documento = $tipo_documento;
    }

    /**
     * @return mixed
     */
    public function getValidadeRgPassaporte()
    {
        return $this->validade_rg_passaporte;
    }

    /**
     * @param mixed $validade_rg_passaporte
     */
    public function setValidadeRgPassaporte($validade_rg_passaporte)
    {
        $this->validade_rg_passaporte = $validade_rg_passaporte;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getVatNo()
    {
        return $this->vat_no;
    }

    /**
     * @param mixed $vat_no
     */
    public function setVatNo($vat_no)
    {
        $this->vat_no = $vat_no;
    }

    /**
     * @return mixed
     */
    public function getCf1()
    {
        return $this->cf1;
    }

    /**
     * @param mixed $cf1
     */
    public function setCf1($cf1)
    {
        $this->cf1 = $cf1;
    }

    /**
     * @return mixed
     */
    public function getCf4()
    {
        return $this->cf4;
    }

    /**
     * @param mixed $cf4
     */
    public function setCf4($cf4)
    {
        $this->cf4 = $cf4;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * @param mixed $postal_code
     */
    public function setPostalCode($postal_code)
    {
        $this->postal_code = $postal_code;
    }


    /**
     * @return mixed
     */
    public function getComplemento()
    {
        return $this->complemento;
    }

    /**
     * @param mixed $complemento
     */
    public function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    /**
     * @return mixed
     */
    public function getBairro()
    {
        return $this->bairro;
    }

    /**
     * @param mixed $bairro
     */
    public function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    /**
     * @return mixed
     */
    public function getSexo()
    {
        return $this->sexo;
    }

    /**
     * @param mixed $sexo
     */
    public function setSexo($sexo)
    {
        $this->sexo = $sexo;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getCf5()
    {
        return $this->cf5;
    }

    /**
     * @param mixed $cf5
     */
    public function setCf5($cf5)
    {
        $this->cf5 = $cf5;
    }

    /**
     * @return mixed
     */
    public function getCf2()
    {
        return $this->cf2;
    }

    /**
     * @param mixed $cf2
     */
    public function setCf2($cf2)
    {
        $this->cf2 = $cf2;
    }

    /**
     * @return mixed
     */
    public function getCf3()
    {
        return $this->cf3;
    }

    /**
     * @param mixed $cf3
     */
    public function setCf3($cf3)
    {
        $this->cf3 = $cf3;
    }

    /**
     * @return mixed
     */
    public function getCf6()
    {
        return $this->cf6;
    }

    /**
     * @param mixed $cf6
     */
    public function setCf6($cf6)
    {
        $this->cf6 = $cf6;
    }

    /**
     * @return mixed
     */
    public function getObservacao()
    {
        return $this->observacao;
    }

    /**
     * @param mixed $observacao
     */
    public function setObservacao($observacao)
    {
        $this->observacao = $observacao;
    }

    /**
     * @return mixed
     */
    public function getPlanoSaude()
    {
        return $this->plano_saude;
    }

    /**
     * @param mixed $plano_saude
     */
    public function setPlanoSaude($plano_saude)
    {
        $this->plano_saude = $plano_saude;
    }

    /**
     * @return mixed
     */
    public function getAlergiaMedicamento()
    {
        return $this->alergia_medicamento;
    }

    /**
     * @param mixed $alergia_medicamento
     */
    public function setAlergiaMedicamento($alergia_medicamento)
    {
        $this->alergia_medicamento = $alergia_medicamento;
    }

    /**
     * @return mixed
     */
    public function getDoencaInformar()
    {
        return $this->doenca_informar;
    }

    /**
     * @param mixed $doenca_informar
     */
    public function setDoencaInformar($doenca_informar)
    {
        $this->doenca_informar = $doenca_informar;
    }

    /**
     * @return mixed
     */
    public function getTelefoneEmergencia()
    {
        return $this->telefone_emergencia;
    }

    /**
     * @param mixed $telefone_emergencia
     */
    public function setTelefoneEmergencia($telefone_emergencia)
    {
        $this->telefone_emergencia = $telefone_emergencia;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by)
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getDataCadastro()
    {
        return $this->data_cadastro;
    }

    /**
     * @param mixed $data_cadastro
     */
    public function setDataCadastro($data_cadastro)
    {
        $this->data_cadastro = $data_cadastro;
    }

    /**
     * @return mixed
     */
    public function getCustomerGroupId()
    {
        return $this->customer_group_id;
    }

    /**
     * @param mixed $customer_group_id
     */
    public function setCustomerGroupId($customer_group_id)
    {
        $this->customer_group_id = $customer_group_id;
    }

    /**
     * @return mixed
     */
    public function getCustomerGroupName()
    {
        return $this->customer_group_name;
    }

    /**
     * @param mixed $customer_group_name
     */
    public function setCustomerGroupName($customer_group_name)
    {
        $this->customer_group_name = $customer_group_name;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getBloqueado()
    {
        return $this->bloqueado;
    }

    /**
     * @param mixed $bloqueado
     */
    public function setBloqueado($bloqueado): void
    {
        $this->bloqueado = $bloqueado;
    }

    /**
     * @return mixed
     */
    public function getFaixaId()
    {
        return $this->faixaId;
    }

    /**
     * @param mixed $faixaId
     */
    public function setFaixaId($faixaId): void
    {
        $this->faixaId = $faixaId;
    }

    /**
     * @return mixed
     */
    public function getFaixaName()
    {
        return $this->faixaName;
    }

    /**
     * @param mixed $faixaName
     */
    public function setFaixaName($faixaName): void
    {
        $this->faixaName = $faixaName;
    }

    /**
     * @return mixed
     */
    public function getFaixaEtariaValor()
    {
        return $this->faixaEtariaValor;
    }

    /**
     * @param mixed $faixaEtariaValor
     */
    public function setFaixaEtariaValor($faixaEtariaValor): void
    {
        $this->faixaEtariaValor = $faixaEtariaValor;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return mixed
     */
    public function getSocialName()
    {
        return $this->social_name;
    }

    /**
     * @param mixed $social_name
     */
    public function setSocialName($social_name): void
    {
        $this->social_name = $social_name;
    }

    /**
     * @return mixed
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param mixed $profession
     */
    public function setProfession($profession): void
    {
        $this->profession = $profession;
    }

    /**
     * @return mixed
     */
    public function getDescontarVaga()
    {
        return $this->descontarVaga;
    }

    /**
     * @param mixed $descontarVaga
     */
    public function setDescontarVaga($descontarVaga): void
    {
        $this->descontarVaga = $descontarVaga;
    }

    /**
     * @return mixed
     */
    public function getUltimoAssento()
    {
        return $this->ultimo_assento;
    }

    /**
     * @param mixed $ultimo_assento
     */
    public function setUltimoAssento($ultimo_assento): void
    {
        $this->ultimo_assento = $ultimo_assento;
    }

    /**
     * @return mixed
     */
    public function getUltimoValorExtraAssento()
    {
        return $this->ultimo_valor_extra_assento;
    }

    /**
     * @param mixed $ultimo_valor_extra_assento
     */
    public function setUltimoValorExtraAssento($ultimo_valor_extra_assento): void
    {
        $this->ultimo_valor_extra_assento = $ultimo_valor_extra_assento;
    }

    /**
     * @return mixed
     */
    public function getUltimaNota()
    {
        return $this->ultima_nota;
    }

    /**
     * @param mixed $ultima_nota
     */
    public function setUltimaNota($ultima_nota): void
    {
        $this->ultima_nota = $ultima_nota;
    }

    /**
     * @return mixed
     */
    public function getNomeResponsavel()
    {
        return $this->nome_responsavel;
    }

    /**
     * @param mixed $nome_responsavel
     */
    public function setNomeResponsavel($nome_responsavel): void
    {
        $this->nome_responsavel = $nome_responsavel;
    }

}
