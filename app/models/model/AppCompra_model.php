<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppCompra_model extends CI_Model
{

    public $vendedorId;
    public $cliente;
    public $dependentes;
    public $ingressos;
    public $adicionais;
    public $programacao;
    public $produto;

    public $vencimentosParcela;
    public $valorVencimentosParcelas;
    public $descontos;
    public $tiposCobranca;
    public $movimentadores;

    public $localEmbarque;
    public $tipoHospedagem;
    public $tipoCobranca;
    public $condicaoPagamento;
    public $tipoTransporte;

    public $observacaoAdicional;

    public $qtdBebesColo;
    public $qtdCriancas;
    public $qtdAdultos;

    public $totalPassageiros;

    public $total;
    public $totalComTaxas;

    public $valorSinal = 0.000;
    public $tipoCobrancaSinal;

    public $valorHospedagemAdulto;
    public $valorHospedagemCrianca;
    public $valorHospedagemBebe;

    public $valorPacoteAdulto;
    public $valorPacoteCrianca;
    public $valorPacoteBebe;

    public $meioDivulgacao;

    public $cupom_id;
    public $desconto_cupom;

    //field cc
    public $cardName;
    public $cardNumber;
    public $cardExpiryMonth;
    public $cardExpiryYear;
    public $cvc;

    public $senderHash;
    public $creditCardToken;
    public $deviceId;

    public $parcelas;

    public $cpfTitularCartao;
    public $celularTitularCartao;
    public $cepTitularCartao;
    public $enderecoTitularCartao;
    public $numeroEnderecoTitularCartao;
    public $complementoEnderecoTitularCartao;
    public $bairroTitularCartao;
    public $cidadeTitularCartao;
    public $estadoTitularCartao;

    public $listaEspera;

    //pagseguro cc
    public $totalParcelaPagSeguro;

    public $file_pdf;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarDependente($dependednte) {
        $this->dependentes[count($this->dependentes) + 1] = $dependednte;
    }

    public function addAdicionais($adicional) {
        $this->adicionais[count($this->adicionais) + 1] = $adicional;
    }

    public function addIngressos($ingresso) {
        $this->ingressos[count($this->ingressos) + 1] = $ingresso;
    }

    public function getAcrescimo() {
        $total = $this->total != null ? $this->total : 0;
        $totalComTaxas = $this->totalComTaxas != null ? $this->totalComTaxas :0;

        return $totalComTaxas - $total;
    }


    public function getDescontoCupom() {
        if ($this->desconto_cupom == null) return 0;

        return $this->desconto_cupom;
    }

}
