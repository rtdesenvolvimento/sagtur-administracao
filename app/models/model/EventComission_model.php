<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EventComission_model extends CI_Model
{

    public $date;
    public $commission_id;
    public $commission_item_id;
    public $event;
    public $status;
    public $log;

    public $created_by;
    public $created_at;


    public function __construct() {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date): void
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getCommissionId()
    {
        return $this->commission_id;
    }

    /**
     * @param mixed $commission_id
     */
    public function setCommissionId($commission_id): void
    {
        $this->commission_id = $commission_id;
    }

    /**
     * @return mixed
     */
    public function getCommissionItemId()
    {
        return $this->commission_item_id;
    }

    /**
     * @param mixed $commission_item_id
     */
    public function setCommissionItemId($commission_item_id): void
    {
        $this->commission_item_id = $commission_item_id;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event): void
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * @param mixed $log
     */
    public function setLog($log): void
    {
        $this->log = $log;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by): void
    {
        $this->created_by = $created_by;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @param mixed $created_at
     */
    public function setCreatedAt($created_at): void
    {
        $this->created_at = $created_at;
    }



}
