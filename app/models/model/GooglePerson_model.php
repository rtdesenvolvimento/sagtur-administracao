<?php defined('BASEPATH') OR exit('No direct script access allowed');

class GooglePerson_model extends CI_Model
{

    public $id;
    public $imported = 0;
    public $givenName = '';
    public $familyName = '';
    public $phoneNumbers = '';
    public $emailAddresses = '';
    public $google_person_id = null;

    public $error = '';

    public function __construct() {
        parent::__construct();
    }


}