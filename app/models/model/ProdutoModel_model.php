<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoModel_model extends CI_Model
{
    public $data;
    public $items;
    public $product_attributes;
    public $photos;

    public $programacaoDatas;

    public $transportes;
    public $locaisEmbarque;
    public $tiposHospedagem;
    public $tiposHospedagemFaixaEtaria;

    public $faixaEtariaValores;
    public $faixaEtariaValoresServicoAdicional;

    public $disponibilidades = [];

    public $extras = [];

    public $midia_data;

    public $seo;

    public  $addresses;

    public $tiposCobranca;
    public $condicoesPagamento;

    public $servicos_inclui;

    public function __construct() {
        parent::__construct();
    }

    public function adicionarDisponibilidadeRecorrente($disponibilidade) {
        $this->disponibilidades[] = $disponibilidade;
    }

}