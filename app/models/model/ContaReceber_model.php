<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContaReceber_model extends CI_Model
{

    const STATUS_ABARTA = 'ABERTA';
    const STATUS_QUITADA = 'QUITADA';

    public $id = '';
    public $data = array();
    public $valores = [];
    public $vencimentos = [];
    public $cobrancas = [];
    public $movimentadores = [];
    public $descontos = [];
    public $pagamentos = [];

    //field cc
    public $cardName;
    public $cardNumber;
    public $cardExpiryMonth;
    public $cardExpiryYear;
    public $cvc;

    public $senderHash = '';
    public $deviceId;
    public $creditCardToken;
    public $numeroParcelas;

    public $cpfTitularCartao;
    public $celularTitularCartao;
    public $cepTitularCartao;
    public $enderecoTitularCartao;
    public $numeroEnderecoTitularCartao;
    public $complementoEnderecoTitularCartao;
    public $bairroTitularCartao;
    public $cidadeTitularCartao;
    public $estadoTitularCartao;

    //pagseguro cc
    public $totalParcelaPagSeguro;

    public $venda_manual;


    public function __construct() {
        parent::__construct();
    }

}
