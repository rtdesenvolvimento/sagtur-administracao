<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Folder_model extends CI_Model
{

    public $id;
    public $folder_group_id;
    public $programacao_id;
    public $public_id;

    public $hidden = 0;
    public $name;
    public $note = '';

    public $created_at;
    public $created_by;

    public $update_at;
    public $update_by;

    public function __construct($id = null) {
        parent::__construct();

        if ($id !== null) {
            $this->loadById($id);
        }
    }

    public function loadById($id): ?Folder_model
    {
        $query = $this->db->get_where('folders', array('id' => $id));

        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->id = $row->id;
            $this->folder_group_id = $row->folder_group_id;
            $this->programacao_id = $row->programacao_id;
            $this->public_id = $row->public_id;
            $this->hidden = $row->hidden;
            $this->name = $row->name;
            $this->note = $row->note;

            $this->created_at = $row->created_at;
            $this->created_by = $row->created_by;
            $this->update_at = $row->update_at;
            $this->update_by = $row->update_by;

            return $this;
        }

        return null;
    }

}
