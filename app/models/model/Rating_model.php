<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Rating_model extends CI_Model
{

    public $sale_id;
    public $item_id;

    public $uuid;
    public $rating_private;
    public $publish = false;
    public $customer;
    public $customer_id;
    public $programacao_id;
    public $product_id;
    public $answered = false;

    public $average;//media
    public $created_at;

    public function __construct() {
        parent::__construct();
    }

}
