<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CommissionItem_model extends CI_Model
{
    public $commission_id;
    public $status;

    //valores
    public $commission_percentage = 0.00;
    public $commission = 0.00;

    public $base_value = 0.00;

    //vendedor
    public $biller;
    public $biller_id;

    //cliente
    public $customer;
    public $customer_id;

    public $sale_id;
    public $programacao_id;

    //produto
    public $commission_type_product;
    public $product_id;
    public $product_name;

    public $date_payment = NULL;
    public $date_approval = NULL;
    public $date_closure = NULL;

    //log de usuarios
    public $created_by;
    public $updated_by;

    //log de datas
    public $created_at;
    public $updated_at;

    public function __construct() {
        parent::__construct();
    }

}
