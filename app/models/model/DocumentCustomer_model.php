<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentCustomer_model extends CI_Model
{
    const DEFAULT_PASSWORD = '1(W9Lc6_[SAr';

    public $id;
    public $public_id;
    public $customer_id;

    public $name;
    public $last_name;
    public $email;
    public $phone;
    public $cpf;
    public $address;
    public $recive_email = true;//Se o usuário vai receber e-mails do sistema, 0 = Não recebe nada, 1 = Recebe tudo, 2 = Recebe somente solicitações.
    public $recive_whatsapp = true;

    public $password = DocumentCustomer_model::DEFAULT_PASSWORD;

    //public $cert_file;
    //public $cert_password = '';

    public $can_login = 0;//permite login do cliente no sistema tecnospeed

    public $created_at;
    public $created_by;

    public function __construct($id = null) {
        parent::__construct();

        if ($id !== null) {
            $this->loadById($id);
        }
    }

    public function loadById($id): ?DocumentCustomer_model
    {
        $query = $this->db->get_where('document_customers', array('id' => $id));

        if ($query->num_rows() > 0) {

            $row = $query->row();

            $this->id = $row->id;
            $this->public_id = $row->public_id;
            $this->customer_id = $row->customer_id;

            $this->name = $row->name;
            $this->last_name = $row->last_name;
            $this->email = $row->email;
            $this->phone = $row->phone;
            $this->cpf = $row->cpf;
            $this->address = $row->address;
            $this->recive_email = $row->recive_email;
            $this->recive_whatsapp = $row->recive_whatsapp;
            $this->password = $row->password;

            return $this;
        }

        return null;
    }



}