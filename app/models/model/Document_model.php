<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model
{

    const DOCUMENT_STATUS_DRAFT = 'draft';//rascunho
    const DOCUMENT_STATUS_UNSIGNED = 'unsigned';//nao assinado
    const DOCUMENT_STATUS_SIGNED = 'signed';//quando alguem assinou
    const DOCUMENT_STATUS_CANCELED = 'canceled';

    const DOCUMENT_STATUS_PENDING = 'pending';//aguardando assinatura
    const DOCUMENT_STATUS_COMPLETED = 'completed';//todos assinaram

    public $id;

    public $reference_no;

    public $document_status;
    public $folder_group_id = 1;
    public $folder_id;

    public $sale_id;
    public $biller_id;
    public $biller;

    public $hidden = 0;
    public $name;
    public $file_name;
    public $url_link;
    public $url_validate;
    public $document_key;
    public $extension;
    public $size = 0;

    public $note;
    public $public_id;
    public $created_at;
    public $update_at;
    public $created_by;
    public $update_by;


    public $monitorar_validade;
    public $validade_meses;

    public $auto_destruir_solicitacao;
    public $validade_auto_destruir_dias;

    public $solicitar_selfie;
    public $solicitar_documento;

    public $signature_type;

    public $is_observadores;
    public $observadores;

    public $data_vencimento_contrato;

    public $signatories_note;

    public $enviar_sequencial = 0;

    private array $signatories = [];

    public function __construct($id = null) {
        parent::__construct();

        if ($id !== null) {
            $this->loadById($id);
        }
    }

    public function addSignature($signatories) {
        $this->signatories[count($this->signatories) + 1] = $signatories;
    }

    // Método para carregar o objeto a partir de um ID
    public function loadById($id): ?Document_model
    {
        $query = $this->db->get_where('documents', array('id' => $id));

        if ($query->num_rows() > 0) {
            $row = $query->row();

            $this->id = $row->id;
            $this->reference_no = $row->reference_no;

            $this->document_status = $row->document_status;

            $this->folder_group_id = $row->folder_group_id;
            $this->folder_id = $row->folder_id;

            $this->sale_id = $row->sale_id;
            $this->biller_id = $row->biller_id;
            $this->biller = $row->biller;

            $this->hidden = $row->hidden;
            $this->name = $row->name;
            $this->file_name = $row->file_name;
            $this->note = $row->note;
            $this->url_link = $row->url_link;
            $this->url_validate = $row->url_validate;
            $this->public_id = $row->public_id;
            $this->document_key = $row->document_key;
            $this->extension = $row->extension;
            $this->size = $row->size;
            $this->monitorar_validade = $row->monitorar_validade;
            $this->validade_meses = $row->validade_meses;
            $this->auto_destruir_solicitacao = $row->auto_destruir_solicitacao;
            $this->validade_auto_destruir_dias = $row->validade_auto_destruir_dias;
            $this->solicitar_selfie = $row->solicitar_selfie;
            $this->solicitar_documento = $row->solicitar_documento;
            $this->signature_type = $row->signature_type;
            $this->is_observadores = $row->is_observadores;
            $this->observadores = $row->observadores;
            $this->data_vencimento_contrato = $row->data_vencimento_contrato;
            $this->signatories_note = $row->signatories_note;

            $this->created_at = $row->created_at;
            $this->update_at = $row->update_at;
            $this->created_by = $row->created_by;
            $this->update_by = $row->update_by;

            $query_signatures = $this->db->get_where('document_signatures', array('document_id' => $id));

            if ($query_signatures->num_rows() > 0) {
                foreach ($query_signatures->result() as $row) {
                    $this->addSignature(new Signatures_model($row->id));
                }
            }

            return $this;
        }

        return null;
    }

    public function getSignatories(): array
    {
        return $this->signatories;
    }

    public function setSignatories($signatories): void
    {
        $this->signatories = $signatories;
    }

}
