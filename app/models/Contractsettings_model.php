<?php defined('BASEPATH') OR exit('No direct script access allowed');


class Contractsettings_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function updateContractSetting($data): bool
    {
        $this->db->where('id', 1);
        if ($this->db->update('contract_settings', $data)) {
            return true;
        }
        return false;
    }

    public function updatePlugsign($data)
    {
        $this->db->where('id', 1);
        if ($this->db->update('plugsign', $data)) {
            return true;
        }
        return false;
    }

    public function getPlugsignSettings()
    {
        $q = $this->db->get('plugsign');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}