<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function get_overdue_bills_qty_alerts()
    {

        $this->db->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('agenda_viagem', 'agenda_viagem.id = fatura.programacaoId', 'left')
            ->join('products', 'products.id = agenda_viagem.produto', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'CREDITO')
            ->order_by('fatura.dtVencimento');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $data_incio);
        $end_date = date('Y-m-d', $data_fim);

        $this->db->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$start_date}' ");
        $this->db->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$end_date}' ");
        $this->db->where("DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) < 0");
        $this->db->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL')");

        return $this->db->count_all_results('fatura');
    }
    public function get_sales_canceleds_qty_alerts()
    {
        $this->db->join('sale_events', 'sale_events.sale_id = sales.id');
        $this->db->where("sale_events.status", "Cancelada");
        $this->db->where('pos !=', 1);
        $this->db->where("payment_status", "cancel");
        $this->db->where('DATE(sma_sale_events.date)', date('Y-m-d'));//TADA HOJE

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->db->where('customer_id', $this->session->userdata('user_id'));
        }

        return $this->db->count_all_results('sales');
    }
    public function get_total_qty_alerts() {
        $this->db->where('quantity < alert_quantity', NULL, FALSE)->where('track_quantity', 1);
        return $this->db->count_all_results('products');
    }
    public function get_expiring_qty_alerts() {
        $date = date('Y-m-d', strtotime('+7 days'));//7 dias antes do vencimento ele avisa
        $this->db->select('SUM(quantity_balance) as alert_num')
            ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
            ->where('expiry <', $date);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $res = $q->row();
            return (INT) $res->alert_num;
        }
        return FALSE;
    }
    public function get_setting() {
        if (method_exists($this->db, 'get')) {

            $this->db->select('settings.*,
            
            shop_settings.head_code,
            shop_settings.body_code,
            shop_settings.sections_code,
            shop_settings.filtar_local_embarque,
            shop_settings.captar_meio_divulgacao,
            shop_settings.ocultar_embarques_produto,
            shop_settings.cache_minutes,
            shop_settings.web_page_caching,
            shop_settings.ocultar_horario_saida,
            shop_settings.mostrar_taxas,
            shop_settings.pagination_type,
            shop_settings.number_packages_per_line,
            shop_settings.is_paginate,
            shop_settings.is_rotate,
            shop_settings.max_packages_line,
            shop_settings.arredondar_bordas_img,
            shop_settings.nao_distorcer_img,
            shop_settings.atividades_recomendadas,

            commission_settings.despesa_fechamento_id,
            commission_settings.tipo_cobranca_fechamento_id,

            captacao_settings.captacao_name as captacao_setting, 
            captacao_settings.plantao_default_id, 
            captacao_settings.meio_default_divulgacao_id, 
            captacao_settings.forma_default_atendimento_id, 
            captacao_settings.interesse_default_id, 
            captacao_settings.interesse_tenho_interesse_default_id, 
            captacao_settings.interesse_lista_espera_default_id, 
            captacao_settings.department_default_id, 
           
            contract_settings.contract_name as contract_setting,
            contract_settings.monitorar_validade,
            contract_settings.validade_meses,
            contract_settings.auto_destruir_solicitacao,
            contract_settings.validade_auto_destruir_dias,
            contract_settings.solicitar_selfie,
            contract_settings.solicitar_documento,
            contract_settings.signature_type,
            contract_settings.is_observadores,
            contract_settings.observadores,
            contract_settings.enviar_sequencial,
            contract_settings.enviar_contrato_signatarios_venda,
            contract_settings.gerar_contrato_faturamento_venda,
            contract_settings.gerar_contrato_faturamento_venda_site,
            contract_settings.auto_assinar_biller_contrato,
            contract_settings.auto_assinar_customer_contrato,
            contract_settings.note as note_contract,
            contract_settings.contract_id as contract_id,
            contract_settings.default_biller_contract,
            contract_settings.cancel_contract_by_sale,

            location_setting.name as location_setting, 
            location_setting.status_disponibilidade_disponivel_id, 
            location_setting.status_disponibilidade_reservado_id, 
            location_setting.status_disponibilidade_locado_id, 
            location_setting.status_disponibilidade_devolvido_id, 
            location_setting.status_disponibilidade_cancelado_id');

            $this->db->join('location_setting', 'location_setting.id = settings.location_setting_id', 'left');
            $this->db->join('captacao_settings', 'captacao_settings.id = settings.captacao_setting_id', 'left');
            $this->db->join('shop_settings', 'shop_settings.id = settings.shop_settings_id', 'left');
            $this->db->join('commission_settings', 'commission_settings.id = settings.commission_settings_id', 'left');
            $this->db->join('contract_settings', 'contract_settings.id = settings.contract_settings_id', 'left');

            $q = $this->db->get('settings');

            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return FALSE;
    }
    public function getAllVisaoAutomovel() {
        $q = $this->db->get('visao_automovel');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getVisaoAutomovel($id) {
        $this->db->where('id', $id);
        $this->db->order_by('name');
        $q = $this->db->get('visao_automovel');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllVisao() {
        $this->db->order_by('name');
        $q = $this->db->get('visao_automovel');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPosicaoAutomovel($visao_automovel_id) {
        $this->db->where('visao_automovel_id', $visao_automovel_id);
        $this->db->order_by('name');
        $q = $this->db->get('disposicao_acentos_automovel');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDateFormat($id) {
        $q = $this->db->get_where('date_format', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies($group_name) {
        $q = $this->db->get_where('companies', array('group_name' => $group_name));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id) {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItineraryByID($id) {
        $q = $this->db->get_where('itinerario', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCustomerGroupByID($id) {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUser($id = NULL) {
        if (!$id) {
            $id = $this->session->userdata('user_id');
        }
        $q = $this->db->get_where('users', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserByBiller($billers = NULL) {

        $q = $this->db->get_where('users', array('biller_id' => $billers), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserByBillerActive($billers = NULL) {

        $q = $this->db->get_where('users', array('biller_id' => $billers, 'active' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getCondicaoPagamentoById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('condicao_pagamento')->row();
    }

    function getProximaCondicaoPagamento($parcela){
        $this->db->where('parcelas',$parcela + 1);
        $this->db->limit(1);
        return $this->db->get('condicao_pagamento')->row();
    }

    function getTipoCobrancaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('tipo_cobranca')->row();
    }

    function getCobrancaIntegracaoByFatura($id){
        $this->db->where('fatura',$id);
        $this->db->where('(status = "ABERTA" OR status = "QUITADA")');
        $this->db->limit(1);
        return $this->db->get('fatura_cobranca')->row();
    }


    public function getAllCondicoesPagamento()
    {
        $this->db->where('status', 'Ativo');
        $this->db->order_by('parcelas');

        $q = $this->db->get('condicao_pagamento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllContas()
    {
        $this->db->where('status', 'open');
        $q = $this->db->get('pos_register');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllServicosIncluso()
    {
        $this->db->select('servicos_inclusos.*, images_icon.icon as icon');
        $this->db->join('images_icon', 'images_icon.id=servicos_inclusos.image_icon_id', 'left');

        $this->db->order_by('servicos_inclusos.name');

        $q = $this->db->get('servicos_inclusos');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllServicosInclusoByProduct($product_id, $limite = false)
    {
        $this->db->where("product_id", $product_id);
        $this->db->where("product_services_include.active", true);

        if ($limite) {
            $this->db->limit($limite);
            $this->db->order_by('RAND()');
        } else {
            $this->db->order_by('servicos_inclusos.name');
        }

        $this->db->select('product_services_include.*, servicos_inclusos.name, servicos_inclusos.note as note_incluso, images_icon.icon as icon');
        $this->db->join('servicos_inclusos', 'servicos_inclusos.id=product_services_include.service_id', 'left');
        $this->db->join('images_icon', 'images_icon.id=servicos_inclusos.image_icon_id', 'left');
        $q = $this->db->get('product_services_include');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id) {

        $this->db->select('products.*, 
        product_midia.url_video,
        product_seo.tag_title,
        product_seo.meta_tag_description,
        product_seo.url_product,
        product_seo.key_words,
        product_addresses.ponto_encontro,
        product_addresses.cep, 
        product_addresses.endereco, 
        product_addresses.numero, 
        product_addresses.complemento, 
        product_addresses.bairro, 
        product_addresses.cidade, 
        product_addresses.estado,
        product_addresses.pais');

        $this->db->join('product_midia', 'products.id=product_midia.product_id', 'left');
        $this->db->join('product_addresses', 'products.id=product_addresses.product_id', 'left');
        $this->db->join('product_seo', 'products.id=product_seo.product_id', 'left');

        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByProductSite($products_site_id) {
        $q = $this->db->get_where('products', array('products_site_id' => $products_site_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getRatingID($id)
    {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('ratings');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllGallery() {
        $q = $this->db->get('gallery');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllGalleryItem($gallery_id) {
        $q = $this->db->get_where('gallery_item',  array('gallery_id' => $gallery_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPhotosGallery($limit  = 15) {

        $this->db->order_by('RAND()');
        $q = $this->db->get('gallery_item', $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTestimonial() {
        $q = $this->db->get('testimonial');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTeans() {
        $q = $this->db->get('team_site');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCurrencies() {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByCode($code) {
        $q = $this->db->get_where('currencies', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTaxRates() {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id) {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllWarehouses($unit = NULL) {
        $q = $this->db->get('warehouses');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseByID($id) {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPlugsignSettings()
    {
        $q = $this->db->get('plugsign');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseByCode($code) {
        $q = $this->db->get_where('warehouses', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseByMatriz() {
        $q = $this->db->get_where('warehouses', array('name' => 'Matriz'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseByProducts($products_name) {
        $q = $this->db->get_where('warehouses', array('name' => $products_name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCategories() {
        $this->db->order_by('name');
        $q = $this->db->get('categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getLocaisEmbarque($active = true) {
        $this->db->select('local_embarque.*, local_embarque.name as text');
        $this->db->order_by('name');

        if ($active){
            $this->db->where('local_embarque.active', 1);
        }

        $q = $this->db->get('local_embarque');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getTiposTransporteRodoviario() {
        $q = $this->db->get('tipo_transporte_rodoviario');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getLocalEmbarqueByID($id) {
        $q = $this->db->get_where('local_embarque', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTipoHospedagemID($id) {
        $q = $this->db->get_where('tipo_hospedagem', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTipoTransporteID($id) {
        $q = $this->db->get_where('tipo_transporte_rodoviario', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDestinos() {
        $this->db->order_by('name');
        $q = $this->db->get('destino');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id) {

        $this->db->select('categories.*, images_icon.icon as icon');
        $this->db->join('images_icon', 'images_icon.id=categories.image_icon_id', 'left');
        $q = $this->db->get_where('categories', array('categories.id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductPhotosLimit($id, $limite = 50)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id), $limite);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;
    }

    public function getProdutosAdicionais($pid, $status = 'ATIVO')
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, 
        ' . $this->db->dbprefix('products') . '.code as code, 
        ' . $this->db->dbprefix('combo_items') . '.quantity as qty, 
        ' . $this->db->dbprefix('products') . '.product_details, 
        ' . $this->db->dbprefix('products') . '.details, 
        ' . $this->db->dbprefix('products') . '.image, 
        ' . $this->db->dbprefix('products') . '.name as name, combo_items.comissao, combo_items.fornecedorId,
         ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');

        if ($status) {
            $this->db->where('products.unit', $status);
        }

        $this->db->order_by("products.name");

        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getGiftCardByID($id) {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getRefundCardByID($id) {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByNO($no) {
        $q = $this->db->get_where('gift_cards', array('card_no' => $no), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateInvoiceStatus() {
        $date = date('Y-m-d');
        $q = $this->db->get_where('invoices', array('status' => 'unpaid'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->due_date < $date) {
                    $this->db->update('invoices', array('status' => 'due'), array('id' => $row->id));
                }
            }
            $this->db->update('settings', array('update' => $date), array('setting_id' => '1'));
            return true;
        }
    }

    public function modal_js() {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/modal.js') . '</script>';
    }

    public function getReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            switch ($field) {
                case 'so':
                    $prefix = $this->Settings->sales_prefix;
                    break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                case 'pos':
                    $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'rep':
                    $prefix = $this->Settings->returnp_prefix;
                    break;
                case 'loc':
                    $prefix = $this->Settings->location_prefix;
                    break;
                case 've':
                    $prefix = $this->Settings->vehicle_prefix;
                    break;
                case 'os':
                    $prefix = $this->Settings->itinerary_prefix;
                    break;
                case 'cp':
                    $prefix = $this->Settings->captacao_prefix;
                    break;
                case 'comm':
                    $prefix = $this->Settings->commission_prefix;
                    break;
                case 'doc':
                    $prefix = $this->Settings->doc_prefix;
                    break;
                default:
                    $prefix = '';
            }

            $ref_no = (!empty($prefix)) ? $prefix . '/' : '';

            if ($this->Settings->reference_format == 1) {
                $ref_no .= date('Y') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 2) {
                $ref_no .= date('Y') . "/" . date('m') . "/" . sprintf("%04s", $ref->{$field});
            } elseif ($this->Settings->reference_format == 3) {
                $ref_no .= sprintf("%04s", $ref->{$field});
            } else {
                $ref_no .= $this->getRandomReference();
            }

            return $ref_no;
        }
        return FALSE;
    }

    public function getRandomReference($len = 12) {
        $result = '';
        for ($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }

        if ($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }

        return $result;
    }

    public function getSaleByReference($ref) {
        $this->db->like('reference_no', $ref, 'before');
        $q = $this->db->get('sales', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCountSalesByProducts($idProduto) {
        $this->db->select('SUM(COALESCE(quantity, 0)) as stock, sales.id');
        $this->db->where('product_id', $idProduto);
        $this->db->join('sales', 'sales.id=sale_items.sale_id');
        $q = $this->db->get("sale_items",1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }

    public function getAllProductsDisponivelSite() {
        $this->db->where("enviar_site", '1');
        $q = $this->db->get("products");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updateReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('ref_id' => '1'));
            return TRUE;
        }
        return FALSE;
    }

    public function checkPermissions() {
        $q = $this->db->get_where('permissions', array('group_id' => $this->session->userdata('group_id')), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getNotifications() {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date);
        $this->db->where("till_date >=", $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get("notifications");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUpcomingEvents() {
        $dt = date('Y-m-d');

        $this->db->where('customers is null ');
        $this->db->where('start >=', $dt)->order_by('start')->limit(10);
        if ($this->Settings->restrict_calendar) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('calendar');
        $data = [];

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
        }

        $start = date('Y-m-d');

        //buscar aniversarios
        $ano_start 	= date('Y', strtotime($start));
        $start 	= date('m-d', strtotime($start));

        $this->db->where(" DATE_FORMAT(start,'%m-%d')  >=", $start)->order_by('start')->limit(10);
        $this->db->where('customers is not null ');

        if ($this->Settings->restrict_calendar) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('calendar');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
        }
        if ($data) {
            return $data;
        } else {
            return FALSE;
        }
    }

    public function getUserGroup($user_id = false) {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $group_id = $this->getUserGroupID($user_id);
        $q = $this->db->get_where('groups', array('id' => $group_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserGroupID($user_id = false) {
        $user = $this->getUser($user_id);
        return $user->group_id;
    }

    public function getWarehouseProductsVariants($option_id, $warehouse_id = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedItem($where_clause) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get_where('purchase_items', $where_clause);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getFaturaById($faturaId){
        $this->db->select('fatura.*, companies.name as nomeCliente');
        $this->db->join('companies', 'companies.id = fatura.pessoa');

        $this->db->where('fatura.id',$faturaId);
        $this->db->limit(1);
        return $this->db->get('fatura')->row();
    }

    function getFormaPagamentoById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('forma_pagamento')->row();
    }

    public function getAllSalesPaymentPending() {
        $this->db->where("order_site_id <> '' ");
        $this->db->where("payment_status", 'pending');
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncVariantQty($variant_id, $warehouse_id, $product_id = NULL) {
        $balance_qty = $this->getBalanceVariantQuantity($variant_id);
        $wh_balance_qty = $this->getBalanceVariantQuantity($variant_id, $warehouse_id);
        if ($this->db->update('product_variants', array('quantity' => $balance_qty), array('id' => $variant_id))) {
            if ($this->getWarehouseProductsVariants($variant_id, $warehouse_id)) {
                $this->db->update('warehouses_products_variants', array('quantity' => $wh_balance_qty), array('option_id' => $variant_id, 'warehouse_id' => $warehouse_id));
            } else {
                if($wh_balance_qty) {
                    $this->db->insert('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'option_id' => $variant_id, 'warehouse_id' => $warehouse_id, 'product_id' => $product_id));
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getWarehouseProducts($product_id, $warehouse_id = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncProductQty($product_id, $warehouse_id) {
        $balance_qty = $this->getBalanceQuantity($product_id);
        $wh_balance_qty = $this->getBalanceQuantity($product_id, $warehouse_id);
        if ($this->db->update('products', array('quantity' => $balance_qty), array('id' => $product_id))) {
            if ($this->getWarehouseProducts($product_id, $warehouse_id)) {
                $this->db->update('warehouses_products', array('quantity' => $wh_balance_qty), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            } else {
                if( ! $wh_balance_qty) { $wh_balance_qty = 0; }
                $this->db->insert('warehouses_products', array('quantity' => $wh_balance_qty, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getSaleByID($id) {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSalePayments($sale_id) {
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSalePaymentsNaoEstornado($sale_id) {
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function atualizarSaleStatusCompleted($id) {
        if ($this->db->update('sales', array('sale_status' => 'completed'), array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function atualizarSaleStatusReembolso($id) {
        if ($this->db->update('sales', array('sale_status' => 'reembolso', 'payment_status' => 'reembolso'), array('id' => $id))) {
            return $this->deleteAllPaymantsBySale($id);
        }
        return FALSE;
    }

    public function atualizarSaleStatusCancel($id) {
        if ($this->db->update('sales', array('sale_status' => 'cancel', 'payment_status' => 'cancel'), array('id' => $id))) {
            return $this->deleteAllPaymantsBySale($id);
        }
        return FALSE;
    }

    public function deleteAllPaymantsBySale($sale_id) {
        if ($this->db->delete('payments', array('sale_id' => $sale_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function isVendaImportada($post_id) {
        $q = $this->db->get_where('sales', array('order_site_id' => $post_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncSalePayments($id, $taxas=0) {

        $sale = $this->getSaleByID($id);
        $payments = $this->getSalePaymentsNaoEstornado($id);
        $paid = 0;

        foreach ($payments as $payment) {

            if ($payment->type == 'returned') {
                $paid -= $payment->amount;
            } else if ($payment->status == 'ESTORNO') {
                $paid += 0;
            } else {
                $paid += $payment->amount;
            }
        }

        if ($this->isIntegracaoComBoletoPagSeguro($payment, $taxas)) {
            $paid += $taxas -1;//retira a taxa de 1 real de boleto
        } else {
            $paid += $taxas;
        }

        $payment_status = $paid == 0 ? 'pending' : $sale->payment_status;

        if ($this->sma->formatDecimal($sale->grand_total) == $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        } elseif ($paid != 0) {
            $payment_status = 'partial';
        } elseif ($sale->due_date <= date('Y-m-d') && !$sale->sale_id) {
            $payment_status = 'due';
        }

        if ($this->db->update('sales', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }


    public function isIntegracaoComBoletoPagSeguro($payment, $taxas) {
        $fatura = $this->__getById('fatura', $payment->fatura);
        $tipoCobranca = $this->__getById('tipo_cobranca', $fatura->tipoCobranca);


        return $tipoCobranca->tipo == 'boleto' && $tipoCobranca->integracao == 'pagseguro' && $taxas > 0;
    }
    public function getPurchaseByID($id) {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id) {
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchasePayments($id) {
        $purchase = $this->getPurchaseByID($id);
        $payments = $this->getPurchasePayments($id);
        $paid = 0;
        foreach ($payments as $payment) {
            $paid += $payment->amount;
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    private function getBalanceQuantity($product_id, $warehouse_id = NULL) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('product_id', $product_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    private function getBalanceVariantQuantity($variant_id, $warehouse_id = NULL) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('option_id', $variant_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    public function calculateAVCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity) {
        $real_item_qty = $quantity;
        $wp_details = $this->getWarehouseProduct($warehouse_id, $product_id);
        if ($pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $cost_row = array();
            $quantity = $item_quantity;
            $balance_qty = $quantity;
            $avg_net_unit_cost = $wp_details->avg_cost;
            $avg_unit_cost = $wp_details->avg_cost;
            foreach ($pis as $pi) {
                if (!empty($pi) && $pi->quantity > 0 && $balance_qty <= $quantity && $quantity > 0) {
                    if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                        $quantity = 0;
                    } elseif ($quantity > 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                    }
                }
                if (empty($cost_row)) {
                    break;
                }
                $cost[] = $cost_row;
                if ($quantity == 0) {
                    break;
                }
            }
        }
        if ($quantity > 0 && !$this->Settings->overselling) {
            //  $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            // redirect($_SERVER["HTTP_REFERER"]);
        } elseif ($quantity > 0) {
            $cost[] = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $wp_details->avg_cost, 'purchase_unit_cost' => $wp_details->avg_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => NULL, 'overselling' => 1, 'inventory' => 1);
            $cost[] = array('pi_overselling' => 1, 'product_id' => $product_id, 'quantity_balance' => (0 - $quantity), 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
        }
        return $cost;
    }

    public function calculateCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity) {
        $pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id);
        $real_item_qty = $quantity;
        $quantity = $item_quantity;
        $balance_qty = $quantity;
        foreach ($pis as $pi) {
            if (!empty($pi) && $balance_qty <= $quantity && $quantity > 0) {
                $purchase_unit_cost = $pi->unit_cost ? $pi->unit_cost : ($pi->net_unit_cost + ($pi->item_tax / $pi->quantity));
                if ($pi->quantity_balance >= $quantity && $quantity > 0) {
                    $balance_qty = $pi->quantity_balance - $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $real_item_qty, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                    $quantity = 0;
                } elseif ($quantity > 0) {
                    $quantity = $quantity - $pi->quantity_balance;
                    $balance_qty = $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                }
            }
            $cost[] = $cost_row;
            if ($quantity == 0) {
                break;
            }
        }
        if ($quantity > 0) {
            //$this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), ($pi->product_name ? $pi->product_name : $product_name)));
            // redirect($_SERVER["HTTP_REFERER"]);
        }
        return $cost;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        if ($option_id) {
            $this->db->where('option_id', $option_id);
        }
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name, products.type as type, combo_items.unit_price as unit_price, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getItensVendasFaturadas($produtoId, $programacaoId , $tipoTrasporteId=null)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        $this->db->order_by('companies.name');

        if ($tipoTrasporteId !== null)  {
            $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        }

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($productId, $tipoTrasporteId, $programacaoId,  $localEmbarqueId=null)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left');

        if ($localEmbarqueId)  $this->db->where('sale_items.localEmbarque', $localEmbarqueId);

        $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.
        $this->db->where('sale_items.descontarVaga', 1);

        $this->db->order_by('companies.name');

        $q = $this->db->get_where('sale_items', array('product_id' => $productId));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasTodosPassageiros($produtoId, $programacaoId , $tipoTrasporteId = NULL, $order_by = NULL)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        if ($order_by) {
            $this->db->order_by($order_by);
        } else {
            $this->db->order_by('companies.name');
        }

        if ($tipoTrasporteId !== null) {
            $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        }

        $this->db->where('(sales.sale_status = "faturada" OR sales.sale_status = "orcamento")');

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function item_costing($item, $pi = NULL) {
        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        if (!isset($item['option_id']) || empty($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = NULL;
        }

        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->calculateCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard') {
                            $cost = $this->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, NULL, $item_quantity);
                        } else {
                            $cost = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => NULL, 'inventory' => NULL));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        } else {

            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard') {
                    $cost = $this->calculateAVCost($item['product_id'], $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $cost = $this->calculateAVCost($combo_item->id, $item['warehouse_id'], $item['net_unit_price'], $item['unit_price'], ($combo_item->qty * $item['quantity']), $item['product_name'], $item['option_id'], $item_quantity);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $item['net_unit_price'], 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        }
        return $cost;
    }

    public function costing($items) {
        $citems = array();
        foreach ($items as $item) {
            $pr = $this->getProductByID($item['product_id']);
            if ($pr->type == 'standard') {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
            } elseif ($pr->type == 'combo') {
                $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                foreach ($combo_items as $combo_item) {
                    if ($combo_item->type == 'standard') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty*$item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty*$item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate.'%' : $cpr_tax->rate), 'option_id' => NULL);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty*$item['quantity']);
                        }
                    }
                }
            }
        }
        // $this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing($item, TRUE);
        }
        return $cost;
    }

    public function syncQuantity($sale_id = NULL, $purchase_id = NULL, $oitems = NULL, $product_id = NULL) {
        if ($sale_id) {

            $sale_items = $this->getAllSaleItems($sale_id);
            foreach ($sale_items as $item) {
                if ($item->product_type == 'standard') {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                } elseif ($item->product_type == 'combo') {
                    $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        if($combo_item->type == 'standard') {
                            $this->syncProductQty($combo_item->id, $item->warehouse_id);
                        }
                    }
                }
            }

        } elseif ($purchase_id) {

            $purchase_items = $this->getAllPurchaseItems($purchase_id);
            foreach ($purchase_items as $item) {
                $this->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                }
            }

        } elseif ($oitems) {

            foreach ($oitems as $item) {
                if (isset($item->product_type)) {
                    if ($item->product_type == 'standard') {
                        $this->syncProductQty($item->product_id, $item->warehouse_id);
                        if (isset($item->option_id) && !empty($item->option_id)) {
                            $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                        }
                    } elseif ($item->product_type == 'combo') {
                        $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            if($combo_item->type == 'standard') {
                                $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            }
                        }
                    }
                } else {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                }
            }

        } elseif ($product_id) {
            $warehouses = $this->getAllWarehouses();
            foreach ($warehouses as $warehouse) {
                $this->syncProductQty($product_id, $warehouse->id);
                if ($product_variants = $this->getProductVariants($product_id)) {
                    foreach ($product_variants as $pv) {
                        $this->syncVariantQty($pv->id, $warehouse->id, $product_id);
                    }
                }
            }
        }
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantsPassagem($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name'=> 'Passagem'));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getVariantsByName($nome)
    {
        $this->db->where("name like '%".$nome."%'");
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getVariantsByNamePlanta($name) {
        $this->db->where("variants.name",$name);
        $this->db->join('visao_automovel', 'variants.planta=visao_automovel.id', 'left');
        $this->db->limit(1);
        $q = $this->db->get('variants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllSaleItems($sale_id) {
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseItems($purchase_id) {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchaseItems($data = array()) {
        if (!empty($data)) {
            foreach ($data as $items) {
                foreach ($items as $item) {
                    if (isset($item['pi_overselling'])) {
                        unset($item['pi_overselling']);
                        $option_id = (isset($item['option_id']) && !empty($item['option_id'])) ? $item['option_id'] : NULL;
                        $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'option_id' => $option_id);
                        if ($pi = $this->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance + $item['quantity_balance'];
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), $clause);
                        } else {
                            $clause['quantity'] = 0;
                            $clause['item_tax'] = 0;
                            $clause['quantity_balance'] = $item['quantity_balance'];
                            $this->db->insert('purchase_items', $clause);
                        }
                    } else {
                        if ($item['inventory']) {
                            $this->db->update('purchase_items', array('quantity_balance' => $item['quantity_balance']), array('id' => $item['purchase_item_id']));
                        }
                    }
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function check_customer_deposit($customer_id, $amount)
    {
        $customer = $this->getCompanyByID($customer_id);
        return $customer->deposit_amount >= $amount;
    }

    public function getWarehouseProduct($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateAVCO($data)
    {
        $wp_details = $this->getWarehouseProduct($data['warehouse_id'], $data['product_id']);
        $total_cost = (($wp_details->quantity * $wp_details->avg_cost) + ($data['quantity'] * $data['cost']));
        $total_quantity = $wp_details->quantity + $data['quantity'];
        $avg_cost = ($total_cost / $total_quantity);
        if ($this->db->update('warehouses_products', array('avg_cost' => $avg_cost), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getVerificaCPF($cpf)
    {
        $this->db->select('id, 
        name, 
        company, 
        nome_responsavel,
        social_name, 
        sexo, 
        email, 
        data_aniversario, 
        cf1, 
        cf3, 
        cf5, 
        tipo_documento, 
        telefone_emergencia, 
        profession, 
        doenca_informar,  
        postal_code,  
        address,  
        numero,  
        complemento,  
        bairro,  
        city,  
        state,  
        bloqueado');
        $this->db->where("vat_no",$cpf);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return null;
    }

    public function getParcelasOrcamentoVenda($saleId)
    {
        $this->db->select('sales_parcelas_orcamento.*, tipo_cobranca.name as tipoCobranca, pos_register.name as movimentador');
        $this->db->where('saleId', $saleId);
        $this->db->order_by('dtVencimento', 'asc');

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = sales_parcelas_orcamento.tipoCobrancaId');
        $this->db->join('pos_register', 'pos_register.id = sales_parcelas_orcamento.movimetnadorId');

        $q = $this->db->get_where('sales_parcelas_orcamento');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelasFaturaByContaReceberCanceled($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.id as tipoCobrancaId,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_receber.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');

        $this->db->where("fatura.status", "CANCELADA");

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getParcelasFaturaByContaReceber($vendaId) {

        $this->db->select('fatura.*, parcela.numeroparcela, tipo_cobranca.id as tipoCobrancaId, tipo_cobranca.note as note_tipo_cobranca,  tipo_cobranca.name as tipoCobranca');
        $this->db->where('conta_receber.sale',$vendaId);

        $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left');

        $this->db->where('(fatura.status = "QUITADA" OR fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsForSale($sale_id)
    {
        $this->db->select('payments.id as payment_id, payments.date, payments.status, payments.paid_by, payments.amount, payments.taxa, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type, attachment, fatura, note, status, motivo_estorno')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItems($sale_id, $exibirApenasServicos=false)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            tipo_trajeto_veiculo.name as tipo_trajeto,
            products.code,
            products.isTaxasComissao,  
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tipo_trajeto_veiculo', 'products.tipo_trajeto_id=tipo_trajeto_veiculo.id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')

            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')

            //->group_by('sale_items.id')
            ->order_by('sale_items.customerClient, sale_items.adicional, sale_items.id, sale_items.tipoDestino');
        //->order_by('sale_items.product_category desc, sale_items.dateCheckin, sale_items.horaCheckin, sale_items.dateCheckOut,sale_items.horaCheckOut');

        if ($exibirApenasServicos) {
            $this->db->where('adicional', false);
        }

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByID($id)
    {
        $this->db->select('sales.*, meio_divulgacao.name as meio_divulgacao_name, condicao_pagamento.name as condicaoPagamento, tipo_cobranca.name as tipoCobranca, sma_companies.name as biller_name ')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('meio_divulgacao', 'meio_divulgacao.id=sales.meioDivulgacao', 'left')
            ->join('sma_companies', 'sma_companies.id=sales.biller_id', 'left');

        $q = $this->db->get_where('sales', array('sales.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentByCC($ccNo)
    {
        $this->db->select('payments.*, sales.date as dsale, sales.reference_no');

        $this->db->join('sales', 'sales.id = payments.sale_id');
        $q = $this->db->get_where('payments', array('cc_no' => $ccNo), 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllEvents($sale_id)
    {
        $this->db->order_by('id');
        $q = $this->db->get_where('sale_events', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobrancaDespesa()
    {
        $this->db->where('status', 'Ativo');

        $this->db->where("tipoExibir", 'despesa');

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobrancaReceita()
    {
        $this->db->where('status', 'Ativo');

        $this->db->where("tipoExibir", 'receita');

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobranca($ocultar_interna = false, $somente_despesa = false)
    {
        $this->db->where('status', 'Ativo');

        if ($ocultar_interna) {
            $this->db->where("credito_cliente", 0);
            $this->db->where("reembolso", 0);
        }

        if ($somente_despesa) {
            $this->db->where("tipoExibir", 'despesa');
        }

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllReceitasSuperiores()
    {
        $this->db->where('receitasuperior', '');
        $q = $this->db->get('receita');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReceitaByReceitaSuperiorId($id)
    {
        $this->db->where('receitasuperior', $id);
        $q = $this->db->get('receita');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function verificaClienteVenda($cpf, $programacaoId) {
        $this->db->select('sales.id as sale_id, sale_status');
        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');
        $this->db->join('companies', 'companies.id = sale_items.customerClient');
        $this->db->where("sales.sale_status in ('faturada', 'orcamento') ");

        $q = $this->db->get_where('sale_items', array('companies.vat_no' => $cpf, 'programacaoId' => $programacaoId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function getParcelaOneByFatura($faturaId) {
        $this->db->where('fatura',$faturaId);
        $this->db->limit(1);
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');

        return $this->db->get('parcela_fatura')->row();
    }

    function getParcelaById($id){
        $this->db->where('id',$id);
        $this->db->limit(1);
        return $this->db->get('parcela')->row();
    }

    public function getContaReceberById($id)
    {
        $this->db->select('conta_receber.*, companies.name as nomeCliente');
        $this->db->join('companies', 'companies.id = conta_receber.pessoa', 'left');

        $q = $this->db->get_where("conta_receber", array('conta_receber.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getContaPagarById($id)
    {
        $this->db->select('conta_pagar.*, companies.name as nomeCliente');
        $this->db->join('companies', 'companies.id = conta_pagar.pessoa', 'left');

        $q = $this->db->get_where("conta_pagar", array('conta_pagar.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllDespesasSuperiores()
    {
        $this->db->where('despesasuperior', '');
        $q = $this->db->get('despesa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReceitaByDespesaSuperiorId($id)
    {
        $this->db->where('despesasuperior', $id);
        $q = $this->db->get('despesa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSelectHorarios() {

        $optsIntervalo = array(
            'DESLIGAR'  => 'DESLIGAR',
            '08:00:00'  => '08:00:00 am',
            '11:59:00'  => '11:59:00 pm',
            '12:00:00'  => '12:00:00 pm',
            '14:00:00'  => '14:00:00 pm',
            '18:00:00'  => '18:00:00 pm',
            '20:00:00'  => '20:00:00 pm',
            '20:30:00'  => '20:30:00 pm',
            '21:00:00'  => '21:00:00 pm',
            '12' => '12 min',
            '15' => '15 min',
            '20' => '20 min',
            '30' => '30 min',
            '45' => '45 min',
            '60' => '1 h',
            '90' => '1 h 30 min',
            '120'=> '2 h',
            '180'=> '3 h',
            '240'=> '4 h',
            '360'=> '6 h',
        );

        return $optsIntervalo;
        /*
        return '<select>
                     <option value="00:00:00">12:00 am</option>
                    <option value="00:15:00">12:15 am</option>
                    <option value="00:30:00">12:30 am</option>
                    <option value="00:45:00">12:45 am</option>
                    <option value="01:00:00">1:00 am</option>
                    <option value="01:15:00">1:15 am</option>
                    <option value="01:30:00">1:30 am</option>
                    <option value="01:45:00">1:45 am</option>
                    <option value="02:00:00">2:00 am</option>
                    <option value="02:15:00">2:15 am</option>
                    <option value="02:30:00">2:30 am</option>
                    <option value="02:45:00">2:45 am</option>
                    <option value="03:00:00">3:00 am</option>
                    <option value="03:15:00">3:15 am</option>
                    <option value="03:30:00">3:30 am</option>
                    <option value="03:45:00">3:45 am</option>
                    <option value="04:00:00">4:00 am</option>
                    <option value="04:15:00">4:15 am</option>
                    <option value="04:30:00">4:30 am</option>
                    <option value="04:45:00">4:45 am</option>
                    <option value="05:00:00">5:00 am</option>
                    <option value="05:15:00">5:15 am</option>
                    <option value="05:30:00">5:30 am</option>
                    <option value="05:45:00">5:45 am</option>
                    <option value="06:00:00">6:00 am</option>
                    <option value="06:15:00">6:15 am</option>
                    <option value="06:30:00">6:30 am</option>
                    <option value="06:45:00">6:45 am</option>
                    <option value="07:00:00">7:00 am</option>
                    <option value="07:15:00">7:15 am</option>
                    <option value="07:30:00">7:30 am</option>
                    <option value="07:45:00">7:45 am</option>
                    <option value="08:00:00">8:00 am</option>
                    <option value="08:15:00">8:15 am</option>
                    <option value="08:30:00">8:30 am</option>
                    <option value="08:45:00">8:45 am</option>
                    <option value="09:00:00">9:00 am</option>
                    <option value="09:15:00">9:15 am</option>
                    <option value="09:30:00">9:30 am</option>
                    <option value="09:45:00">9:45 am</option>
                    <option value="10:00:00">10:00 am</option>
                    <option value="10:15:00">10:15 am</option>
                    <option value="10:30:00">10:30 am</option>
                    <option value="10:45:00">10:45 am</option>
                    <option value="11:00:00">11:00 am</option>
                    <option value="11:15:00">11:15 am</option>
                    <option value="11:30:00">11:30 am</option>
                    <option value="11:45:00">11:45 am</option>
                    <option value="12:00:00">12:00 pm</option>
                    <option value="12:15:00">12:15 pm</option>
                    <option value="12:30:00">12:30 pm</option>
                    <option value="12:45:00">12:45 pm</option>
                    <option value="13:00:00">1:00 pm</option>
                    <option value="13:15:00">1:15 pm</option>
                    <option value="13:30:00">1:30 pm</option>
                    <option value="13:45:00">1:45 pm</option>
                    <option value="14:00:00">2:00 pm</option>
                    <option value="14:15:00">2:15 pm</option>
                    <option value="14:30:00">2:30 pm</option>
                    <option value="14:45:00">2:45 pm</option>
                    <option value="15:00:00">3:00 pm</option>
                    <option value="15:15:00">3:15 pm</option>
                    <option value="15:30:00">3:30 pm</option>
                    <option value="15:45:00">3:45 pm</option>
                    <option value="16:00:00">4:00 pm</option>
                    <option value="16:15:00">4:15 pm</option>
                    <option value="16:30:00">4:30 pm</option>
                    <option value="16:45:00">4:45 pm</option>
                    <option value="17:00:00">5:00 pm</option>
                    <option value="17:15:00">5:15 pm</option>
                    <option value="17:30:00">5:30 pm</option>
                    <option value="17:45:00">5:45 pm</option>
                    <option value="18:00:00">6:00 pm</option>
                    <option value="18:15:00">6:15 pm</option>
                    <option value="18:30:00">6:30 pm</option>
                    <option value="18:45:00">6:45 pm</option>
                    <option value="19:00:00">7:00 pm</option>
                    <option value="19:15:00">7:15 pm</option>
                    <option value="19:30:00">7:30 pm</option>
                    <option value="19:45:00">7:45 pm</option>
                    <option value="20:00:00">8:00 pm</option>
                    <option value="20:15:00">8:15 pm</option>
                    <option value="20:30:00">8:30 pm</option>
                    <option value="20:45:00">8:45 pm</option>
                    <option value="21:00:00">9:00 pm</option>
                    <option value="21:15:00">9:15 pm</option>
                    <option value="21:30:00">9:30 pm</option>
                    <option value="21:45:00">9:45 pm</option>
                    <option value="22:00:00">10:00 pm</option>
                    <option value="22:15:00">10:15 pm</option>
                    <option value="22:30:00">10:30 pm</option>
                    <option value="22:45:00">10:45 pm</option>
                    <option value="23:00:00">11:00 pm</option>
                    <option value="23:15:00">11:15 pm</option>
                    <option value="23:30:00">11:30 pm</option>
                    <option value="23:45:00">11:45 pm</option>
                    </select>';
        */
    }

    public function getProgramacaoAll($status=NULL, $ano=NULL, $mes=NULL)
    {

        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        if ($status) $this->db->where('products.unit', $status);
        if ($mes) $this->db->where('Month(sma_agenda_viagem.dataSaida)', $mes);
        if ($ano) $this->db->where('Year(sma_agenda_viagem.dataSaida)', $ano);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getReturnBySID($sale_id)
    {
        $q = $this->db->get_where('return_sales', array('sale_id' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllInvoiceItemsAdicionais($sale_id)
    {
        $this->db->select('
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details, 
            products.product_details as product_details')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->group_by('products.id');

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id, 'adicional' => true));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllReturnItems($return_id)
    {
        $this->db->select('return_items.*, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=return_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_items.option_id', 'left')
            ->group_by('return_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('return_items', array('return_id' => $return_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllItinerarioByItem($saleItemId)
    {
        $q = $this->db->get_where('aereo_itinerario', array('sale_item' => $saleItemId));
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }


    public function getTransferBySaleItem($saleItemId)
    {
        $q = $this->db->get_where('transfer_itinerario', array('sale_item' => $saleItemId));
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getAllProductDetails($product_id) {

        $this->db->where('product_id', $product_id);

        $this->db->order_by('titulo');

        $q = $this->db->get('product_details');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getLocalEmbarqueRodoviarioById($produto, $localEmbarque) {

        $this->db->select('
        local_embarque.id, 
        
        sma_local_embarque.name, 
        sma_local_embarque.endereco, 
        sma_local_embarque.numero, 
        sma_local_embarque.complemento, 
        sma_local_embarque.bairro, 
        sma_local_embarque.cidade, 
        sma_local_embarque.estado, 
        sma_local_embarque.cep, 

        local_embarque_viagem.horaEmbarque, 
        local_embarque_viagem.dataEmbarque, 
        local_embarque_viagem.note, 
        local_embarque_viagem.status ', FALSE);
        $this->db->join('local_embarque', 'local_embarque.id=local_embarque_viagem.localEmbarque', 'left');
        $this->db->where('local_embarque.id',$localEmbarque);
        $this->db->where('produto',$produto);
        $this->db->limit(1);

        return $this->db->get('local_embarque_viagem')->row();
    }

    public function getCupomById($cupom_id) {
        $q = $this->db->get_where('cupom_desconto', array('id' => $cupom_id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAgendaViagemByID($programacaoId) {
        $q = $this->db->get_where('agenda_viagem', array('id' => $programacaoId), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInvoicePayments($sale_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getTipoHospedagemRodoviario($produtoId, $tipoHospedagem = NULL)
    {
        $this->db->select('tipo_hospedagem.id, tipo_hospedagem_viagem.preco as preco, sma_tipo_hospedagem.name, tipo_hospedagem_viagem.status, tipo_hospedagem.note, tipo_hospedagem.ocupacao as acomoda, tipo_hospedagem_viagem.estoque', false);
        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_tipo_hospedagem_viagem.tipoHospedagem', 'left');

        if ($tipoHospedagem != null)  $this->db->where('tipo_hospedagem.id',$tipoHospedagem);

        $q = $this->db->get_where('tipo_hospedagem_viagem', array('produto' => $produtoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransportes($pid)
    {
        $this->db->select('tipo_transporte_rodoviario.id, tipo_transporte_rodoviario.name as text, tipo_transporte_rodoviario.automovel_id, tipo_transporte_rodoviario_viagem.habilitar_selecao_link');
        $this->db->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=tipo_transporte_rodoviario_viagem.tipoTransporte', 'left');
        $this->db->order_by('tipo_transporte_rodoviario.name');

        $q = $this->db->get_where('tipo_transporte_rodoviario_viagem', array('produto' => $pid, 'tipo_transporte_rodoviario_viagem.status' => 'ATIVO'));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllFormasPagamentoProdutoExibirLinkCompra($produtoID)
    {
        $this->db->select('tipo_cobranca.id as id, 
        tipo_cobranca.name, 
        tipo_cobranca.tipo, 
        tipo_cobranca.note, 
        tipo_cobranca.conta, 
        tipo_cobranca_produto.diasMaximoPagamentoAntesViagem, 
        tipo_cobranca_produto.exibirNaSemanaDaViagem');

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = tipo_cobranca_produto.tipoCobrancaId');

        $this->db->where('tipo_cobranca.status', 'Ativo');
        $this->db->where('tipo_cobranca.exibirLinkCompras', 1);
        $this->db->where('tipo_cobranca_produto.status', 1);
        $this->db->where('tipo_cobranca_produto.produtoId', $produtoID);

        $this->db->order_by('tipo_cobranca.name');
        $this->db->group_by('tipo_cobranca.name');

        $q = $this->db->get('tipo_cobranca_produto');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobrancaExibirLinkCompra()
    {
        $this->db->where('tipo_cobranca.status', 'Ativo');
        $this->db->where('tipo_cobranca.exibirLinkCompras', 1);
        $this->db->where('taxas_venda_configuracao.ativo', 1);

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = taxas_venda_configuracao.tipoCobrancaId');

        $this->db->order_by('tipo_cobranca.name');
        $this->db->group_by('tipo_cobranca.name');

        $q = $this->db->get('taxas_venda_configuracao');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobrancaDebito($ocultar_interna = false)
    {
        $this->db->where('status', 'Ativo');
        $this->db->where("tipoExibir", 'receita');

        if ($ocultar_interna) {
            $this->db->where("credito_cliente", 0);
            $this->db->where("reembolso", 0);
        }

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasPorLocalEmbarque($productId, $tipoTrasporteId, $programacaoId,  $localEmbarqueId=null, $order_by = FALSE)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        if ($localEmbarqueId) {
            $this->db->where('sale_items.localEmbarque', $localEmbarqueId);
        }

        $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        if ($order_by) {
            $this->db->order_by($order_by);
        } else {
            $this->db->order_by('companies.name');
        }

        $q = $this->db->get_where('sale_items', array('product_id' => $productId));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensByFaixaValores($produtoId, $programacaoId, $faixaid, $status, $tipoHospedagem= NULL)
    {
        $this->db->select('COALESCE(count(sma_sale_items.id), 0) as qtd, valor_faixa.name as faixa, valor_faixa.descontarVaga')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaId', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where('sale_items.faixaId', $faixaid);
        $this->db->where('sales.sale_status', $status);

        if ($tipoHospedagem) {
            $this->db->where('sale_items.tipoHospedagem', $tipoHospedagem);
        }

        $this->db->group_by('sale_items.faixaId');
        $this->db->order_by('valor_faixa.name');

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalVendasRealizadasParaAhViagem($product_id)
    {
        $this->db->select('sum(sma_sale_items.quantity) as quantity')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left')->group_by('sale_items.product_id');
        $q = $this->db->get_where('sale_items', array('product_id' => $product_id, 'colo' => 0));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscarTotalDeItensDeUmaVendaPorVenda($vendaId)
    {
        $this->db->select('sum(sma_sale_items.quantity) as quantity');
        $q = $this->db->get_where('sale_items', array('sale_id' => $vendaId));

        if ($q->num_rows() > 0) {
            return $q->row()->quantity;
        }
        return 0;
    }

    public function getSaleByItensByPoltrona($product_id, $poltrona, $variacao) {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $q = $this->db->get_where('sale_items', array(
            'sale_items.product_id' => $product_id,
            'sale_items.poltronaClient' => $poltrona,
            'sales.reference_no_variacao' => $variacao), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPagamentosByFatura($faturaId)
    {
        $this->db->order_by('id', 'asc');

        $this->db->select('payments.*, pos_register.name as conta, tipo_cobranca.name tipoCobranca');
        $this->db->join('pos_register', 'pos_register.id = payments.movimentador', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = payments.tipoCobranca', 'left');

        $q = $this->db->get_where('payments', array('fatura' => $faturaId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    public function buscarHistoricoParcelasByFatura($faturaId) {

        $fatura =  $this->getFaturaById($faturaId);

        if ($fatura->contas_receber != null)  return $this->buscarHistoricoParcelasByFaturaCredito($fatura);
        if ($fatura->contas_pagar != null) return $this->buscarHistoricoParcelasByFaturaDebito($fatura);
    }

    private function buscarHistoricoParcelasByFaturaCredito($fatura) {

        $this->db->select('parcela.*,
            conta_receber.id as contareceberId, 
            conta_receber.conta_origem as contaOriginal, 
            conta_receber.note,
            conta_receber.reference,
            companies.id as clienteId,  
            companies.name as nomeCliente,  
            receita.name as receita,
            tipo_cobranca.id as tipoCobrancaId,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
        $this->db->join('companies', 'companies.id = parcela.pessoa', 'left');
        $this->db->join('receita', 'receita.id = conta_receber.receita', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca', 'left');
        $this->db->where('conta_receber.id in ('.$fatura->contas_receber.')');

        if ($fatura->contas_pagar != null) $this->db->where('conta_receber.id in ('.$fatura->contas_pagar.')');

        $q = $this->db->get("parcela");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) $data[] = $row;
            return $data;
        }

        return FALSE;
    }

    private function buscarHistoricoParcelasByFaturaDebito($fatura) {

        $this->db->select('parcela.*,
            conta_pagar.id as contareceberId,
            conta_pagar.note,
            conta_pagar.reference,
            companies.id as clienteId,
            companies.name as nomeCliente,
            despesa.name as receita,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId', 'left');
        $this->db->join('companies', 'companies.id = parcela.pessoa', 'left');
        $this->db->join('despesa', 'despesa.id = conta_pagar.despesa', 'left');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = parcela.tipocobranca', 'left');
        $this->db->where('conta_pagar.id in ('.$fatura->contas_pagar.')');

        $q = $this->db->get("parcela");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) $data[] = $row;

            return $data;
        }

        return FALSE;
    }

}