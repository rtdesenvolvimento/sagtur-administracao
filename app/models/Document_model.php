<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Document_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('service/DocumentService_model');
    }

    public function getById($id)
    {
        return parent::__getById('document', $id);
    }

    public function save($data) {

        try {
            $this->__iniciaTransacao();

            $doc_id = $this->addDocument($data);

            if ($doc_id) {
                $this->DocumentService_model->createDocument($doc_id);
            }

            $this->__confirmaTransacao();

            return $doc_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function editDocument($data, $id) {
        $this->__edit('document', $data, 'id', $id);
    }

    public function addDocument($data)
    {
        if ($this->db->insert("document", $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function addSignature($doc_id, $data) {
        $data['document_id'] = $doc_id;
        if ($this->db->insert("signatures", $data)) {
            return true;
        }
        return false;
    }

}
