<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Fatura_Repository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getFaturasByFilter($faturas_id)
    {
        $this->db->select("{$this->db->dbprefix('fatura')}.id as id,
                {$this->db->dbprefix('fatura')}.reference,
                date_format({$this->db->dbprefix('fatura')}.dtVencimento, '%d/%m/%Y') as dtVencimento, 
                {$this->db->dbprefix('fatura')}.tipooperacao,
                {$this->db->dbprefix('companies')}.name, 
                CONCAT({$this->db->dbprefix('products')}.name, ' ' , DATE_FORMAT({$this->db->dbprefix('agenda_viagem')}.dataSaida, '%d/%m/%Y' )) as product,
                {$this->db->dbprefix('fatura')}.strReceitas as categoria_receita,
                {$this->db->dbprefix('fatura')}.strDespesas as categoria_despesa,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.taxas,
                {$this->db->dbprefix('fatura')}.valorpago,
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                CASE 
                    WHEN DATEDIFF({$this->db->dbprefix('fatura')}.dtVencimento, NOW()) < 0 
                    THEN DATEDIFF({$this->db->dbprefix('fatura')}.dtVencimento, NOW())*-1 
                    ELSE 0 
                END AS quantidade_dias,
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code,
                {$this->db->dbprefix('fatura')}.note")
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('agenda_viagem', 'agenda_viagem.id = fatura.programacaoId', 'left')
            ->join('products', 'products.id = agenda_viagem.produto', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');

        $this->db->where_in('fatura.id', $faturas_id);

        return $this->db->get('fatura');
    }
}