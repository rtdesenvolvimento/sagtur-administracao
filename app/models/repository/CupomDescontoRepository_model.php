<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CupomDescontoRepository_model extends CI_Model
{
    public function __construct()
    {
        $this->load->model('dto/CupmDescontoViewDTO_model', 'CupmDescontoViewDTO_model');

        parent::__construct();
    }

    public function getById($cupom_id) {
        $q = $this->db->get_where('cupom_desconto', array('id' => $cupom_id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCupomDescontoByProgramacaoId($cupom_id, $programacao_id)
    {
        $q = $this->db->get_where('cupom_desconto_item', array('cupom' => $cupom_id,'programacao_id' => $programacao_id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function  validar($programacao_id, $codigo) {
        $encontrouCupomCodigoProgramacao = $this->buscarCupomByCodigo($programacao_id, $codigo);//verifica se encontrou o cupom

        if ($encontrouCupomCodigoProgramacao) {

            $validade = $this->valida_validade_cupom_programacao($programacao_id, $codigo, $encontrouCupomCodigoProgramacao->dataInicio, $encontrouCupomCodigoProgramacao->dataFinal);
            $cupomDTO = new CupmDescontoViewDTO_model();

            if ($validade) {
                $cupomDTO->valor = $encontrouCupomCodigoProgramacao->valor;
                $cupomDTO->status = 'Valid';
                $cupomDTO->cupom = $encontrouCupomCodigoProgramacao->cupom;
                $cupomDTO->tipo_desconto = $encontrouCupomCodigoProgramacao->tipo_desconto;
            } else {
                $cupomDTO->valor = $encontrouCupomCodigoProgramacao->valor;
                $cupomDTO->status = 'Expired';
                $cupomDTO->cupom = $encontrouCupomCodigoProgramacao->cupom;
                $cupomDTO->tipo_desconto = $encontrouCupomCodigoProgramacao->tipo_desconto;
            }

            $totalCupomUsados = $this->total_cupom_usado($programacao_id, $encontrouCupomCodigoProgramacao->cupom);
            $totalCupomDisponibilizados = $encontrouCupomCodigoProgramacao->quantidade_disponibilizada;

            $disponivel = $totalCupomDisponibilizados - $totalCupomUsados;

            if ($disponivel <= 0) {
                $cupomDTO->status = 'Expired';
            }

            return $cupomDTO;
        } else {

            $encontrouCupomGeral = $this->buscarCupomGeralByCodigo($codigo);//verifica se encontrou o cupom

            if ($encontrouCupomGeral) {

                $encontrouCupomCodigoProgramacao = $this->buscarCupomGeralByCodigoProgramacao($codigo, $programacao_id);//verifica se encontrou o cupom

                if ($encontrouCupomCodigoProgramacao) {

                    $totalCupomDisponibilizados = $encontrouCupomCodigoProgramacao->quantidade_disponibilizada;

                    if ($totalCupomDisponibilizados <= 0) {
                        return false;
                    }

                }

                $validade = $this->valida_validade_cupom_geral($codigo, $encontrouCupomGeral->dataInicio, $encontrouCupomGeral->dataFinal);
                $cupomDTO = new CupmDescontoViewDTO_model();

                if ($validade) {
                    $cupomDTO->valor = $encontrouCupomGeral->valor;
                    $cupomDTO->status = 'Valid';
                    $cupomDTO->cupom = $encontrouCupomGeral->id;
                    $cupomDTO->tipo_desconto = $encontrouCupomGeral->tipo_desconto;
                } else {
                    $cupomDTO->valor = $encontrouCupomGeral->valor;
                    $cupomDTO->status = 'Expired';
                    $cupomDTO->cupom = $encontrouCupomGeral->id;
                    $cupomDTO->tipo_desconto = $encontrouCupomGeral->tipo_desconto;
                }

                $totalCupomUsados = $this->total_cupom_usado(NULL, $encontrouCupomGeral->id);
                $totalCupomDisponibilizados = $encontrouCupomGeral->quantidade_disponibilizada;

                $disponivel = $totalCupomDisponibilizados - $totalCupomUsados;

                if ($disponivel <= 0) {
                    $cupomDTO->status = 'Expired';
                }

                return $cupomDTO;

            }
        }

        return false;
    }

    private function valida_validade_cupom_programacao($programacao_id, $codigo, $dataInicio, $dataFinal) {

        $dataHoje = date('Y-m-d');

        if ($dataInicio && $dataInicio != '0000-00-00') {
            $this->db->where("cupom_desconto_item.dataInicio <='".$dataHoje."'");
        }

        if ($dataFinal && $dataFinal != '0000-00-00') {
            $this->db->where("cupom_desconto_item.dataFinal  >='".$dataHoje."'");
        }

        $this->db->join('cupom_desconto', 'cupom_desconto.id=cupom_desconto_item.cupom', 'left');
        $q = $this->db->get_where('cupom_desconto_item', array('programacao_id' => $programacao_id, 'codigo' => $codigo, 'all_products' => 0, 'active' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function valida_validade_cupom_geral($codigo, $dataInicio, $dataFinal) {

        $dataHoje = date('Y-m-d');

        if ($dataInicio && $dataInicio != '0000-00-00') {
            $this->db->where("dataInicio <='".$dataHoje."'");
        }

        if ($dataFinal && $dataFinal != '0000-00-00') {
            $this->db->where("dataFinal >='".$dataHoje."'");
        }

        $q = $this->db->get_where('cupom_desconto', array('codigo' => $codigo, 'all_products' => 1, 'active' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function total_cupom_usado($programacao_id, $cupom_id) {

        $this->db->select('sma_sale_items.id as totalLinhas');

        $this->db->where("sma_sales.sale_status in ('faturada', 'orcamento')");
        $this->db->join('sma_sales', 'sma_sales.id=sale_items.sale_id', 'left');

        if ($programacao_id) {
            $this->db->where('sale_items.programacaoId', $programacao_id);
        }

        $this->db->group_by('sale_items.sale_id');

        $q = $this->db->get_where('sale_items', array('sale_items.cupom_id' => $cupom_id));

        return $q->num_rows();
    }

    public function findAllPassageirosByCupom($programacao_id, $cupom_id) {

        $this->db->where("sma_sales.sale_status in ('faturada', 'orcamento')");
        $this->db->join('sma_sales', 'sma_sales.id=sale_items.sale_id', 'left');

        $this->db->group_by('sale_items.sale_id');

        $q = $this->db->get_where('sale_items', array('programacaoId' => $programacao_id, 'sale_items.cupom_id' => $cupom_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    private function buscarCupomByCodigo($programacao_id, $codigo) {
        $this->db->select('cupom_desconto.*, cupom_desconto_item.*, cupom_desconto_item.quantidade_disponibilizada');
        $this->db->join('cupom_desconto', 'cupom_desconto.id=cupom_desconto_item.cupom', 'left');
        $q = $this->db->get_where('cupom_desconto_item', array('programacao_id' => $programacao_id, 'codigo' => $codigo, 'all_products' => 0, 'active' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function buscarCupomGeralByCodigo($codigo) {
        $q = $this->db->get_where('cupom_desconto', array('codigo' => $codigo, 'all_products' => 1, 'active' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function buscarCupomGeralByCodigoProgramacao($codigo, $programacao_id) {
        $this->db->select('cupom_desconto.*, cupom_desconto_item.*, cupom_desconto_item.quantidade_disponibilizada');
        $this->db->join('cupom_desconto', 'cupom_desconto.id=cupom_desconto_item.cupom', 'left');
        $q = $this->db->get_where('cupom_desconto_item', array('programacao_id' => $programacao_id, 'codigo' => $codigo, 'all_products' => 1, 'active' => 1), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}