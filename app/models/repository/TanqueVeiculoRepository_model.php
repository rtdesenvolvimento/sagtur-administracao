<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TanqueVeiculoRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll() {
        $this->db->order_by('id');
        $q = $this->db->get('tanque_veiculo');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllByTipoVeiculo($tipo_combustivel_id) {
        $this->db->where('tipo_combustivel_id', $tipo_combustivel_id);
        $this->db->order_by('id');
        $q = $this->db->get('tanque_veiculo');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}