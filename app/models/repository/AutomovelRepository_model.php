<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AutomovelRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll() {
        $q = $this->db->get('automovel');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getById($id) {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('automovel');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}