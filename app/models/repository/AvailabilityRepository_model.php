<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AvailabilityRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getVehicle_availability($data = false) {

        $this->db->select("
                agenda_disponibilidade.id as id,
                products.image as image,
                veiculo.reference_no, 
                tipo_veiculo.name as tipo_veiculo, 
                categoria_veiculo.name as categoria_veiculo, 
                veiculo.name as veiculo, 
                veiculo.placa, 
                veiculo.ano, 
                veiculo.cor, 
                case when sma_status_disponibilidade.name is null then 'Disponível' else sma_status_disponibilidade.name end as disponibilidade,
                agenda_disponibilidade.data as data,
                agenda_disponibilidade.hora as hora,
                concat(sma_companies.name, '<br/>' , sma_companies.cf5) as cliente,
                concat(sma_locacao_veiculo.dataDevolucaoPrevista,' ', sma_locacao_veiculo.horaDevolucaoPrevista) as devolucao
                ")
            ->join('locacao_veiculo', '
                (sma_locacao_veiculo.dataDevolucaoPrevista >= sma_agenda_disponibilidade.data AND (sma_locacao_veiculo.horaRetiradaPrevista < sma_agenda_disponibilidade.hora AND sma_locacao_veiculo.dataRetiradaPrevista = sma_agenda_disponibilidade.data AND sma_locacao_veiculo.dataRetiradaPrevista != sma_locacao_veiculo.dataDevolucaoPrevista)) 
                OR (sma_locacao_veiculo.dataDevolucaoPrevista > sma_locacao_veiculo.dataRetiradaPrevista AND sma_agenda_disponibilidade.data > sma_locacao_veiculo.dataRetiradaPrevista  AND sma_agenda_disponibilidade.data < sma_locacao_veiculo.dataDevolucaoPrevista)
                OR (sma_locacao_veiculo.dataDevolucaoPrevista >= sma_agenda_disponibilidade.data AND (sma_locacao_veiculo.horaDevolucaoPrevista > sma_agenda_disponibilidade.hora AND sma_locacao_veiculo.dataRetiradaPrevista != sma_agenda_disponibilidade.data AND sma_locacao_veiculo.dataDevolucaoPrevista = sma_agenda_disponibilidade.data))
                OR ((sma_locacao_veiculo.dataRetiradaPrevista = sma_agenda_disponibilidade.data  AND sma_locacao_veiculo.horaRetiradaPrevista <= sma_agenda_disponibilidade.hora  AND sma_locacao_veiculo.horaDevolucaoPrevista >= sma_agenda_disponibilidade.hora)
                OR (sma_locacao_veiculo.dataDevolucaoPrevista = sma_agenda_disponibilidade.data  AND sma_locacao_veiculo.horaRetiradaPrevista <= sma_agenda_disponibilidade.hora  AND sma_locacao_veiculo.horaDevolucaoPrevista >= sma_agenda_disponibilidade.hora))
                ', 'left')
            ->join('status_disponibilidade', 'status_disponibilidade.id = locacao_veiculo.status_disponibilidade_id', 'left')
            ->join('companies', 'companies.id = locacao_veiculo.customer_id', 'left')
            ->join('products', 'products.id = agenda_disponibilidade.produto')
            ->join('veiculo', 'veiculo.id = products.veiculo')
            ->join('tipo_veiculo', 'tipo_veiculo.id = veiculo.tipo_veiculo')
            ->join('categoria_veiculo', 'categoria_veiculo.id = veiculo.categoria')
            ->order_by('agenda_disponibilidade.data, agenda_disponibilidade.hora');

        if ($data) {
            //$this->db->where('agenda_disponibilidade.data', $data);
        } else {
            $this->db->group_by('agenda_disponibilidade.data');
        }

        $q = $this->db->get('agenda_disponibilidade');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }
        return FALSE;
    }

}