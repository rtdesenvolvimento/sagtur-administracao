<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DepartmentRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($id) {
        $q = $this->db->get_where('departments', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function add($data)
    {
        if ($this->db->insert("departments", $data)) {
            return true;
        }
        return false;
    }

    public function updateDepartment($id, $data = array())
    {
        if ($this->db->update("departments", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }
}