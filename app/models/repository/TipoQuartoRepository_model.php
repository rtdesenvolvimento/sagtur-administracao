<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TipoQuartoRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getTiposQuarto($hospedagem_id = null) {

        if ($hospedagem_id != null && $hospedagem_id != '0') {
            $this->db->where('fornecedor', $hospedagem_id);
        }

        $q = $this->db->get('tipo_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getTipoHospedagemById($id) {
        $this->db->limit(1);
        return $this->db->get_where('tipo_hospedagem', array('id' => $id) )->row();
    }
}