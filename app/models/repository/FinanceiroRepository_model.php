<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FinanceiroRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function buscarFaturas($tipo, $parcelaDTO_model) {

        $programacao = null;

        if ($parcelaDTO_model->getProgramacao() != null) $programacao = $this->__getById('agenda_viagem', $parcelaDTO_model->getProgramacao() );

        $this->db->select('fatura.*,
            companies.name as note,
            pos_register.name as movimentador,
            fatura.reference as reference,
            fatura.totalAcrescimo,
            fatura.totalDesconto,
            fatura.diasatraso,
            companies.id as clienteId,  
            companies.name as nomeCliente,
            companies.cf5 as whatsapp,
            companies.phone as telefone,
            fatura.strReceitas as receita,
            fatura.strDespesas as despesa,
            fatura.dtultimopagamento,
            tipo_cobranca.tipo tipocobrancaTipo,
            tipo_cobranca.name tipocobranca');

        $this->db->join('companies', 'companies.id = fatura.pessoa');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');
        $this->db->join('pos_register', 'pos_register.id = fatura.movimentador');

        //fatura.status = "CANCELADA" OR //TODO POR ENQUANTO ESTA FORA ATE CRIAR UM STATUS EXCLUIDO

        $this->db->where('(fatura.status = "REEMBOLSO" OR 
        fatura.status = "ABERTA" OR 
        fatura.status = "PARCIAL" OR 
        fatura.status = "QUITADA" OR 
        fatura.status = "VENCIDA")');

        $this->db->where('fatura.tipooperacao', $tipo);

        //$parcelaDTO_model = new ParcelaDTO_model();

        if (($parcelaDTO_model->getProgramacao() != null ||
            $parcelaDTO_model->getCategoria()) &&
            $tipo == Financeiro_model::TIPO_OPERACAO_CREDITO) {

            $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
            $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
            $this->db->join('conta_receber', 'conta_receber.id = parcela.contareceberId', 'left');
            $this->db->join('sales', 'sales.id = conta_receber.sale', 'left');
            $this->db->join('sale_items', 'sales.id = sale_items.sale_id', 'left');

            if ($parcelaDTO_model->getProgramacao() != null) {//todo e lancado por item pois e lancado apenas 1 conta para todos os itens
                $this->db->where('sale_items.programacaoId', $programacao->id);
                $this->db->where('sale_items.product_id', $programacao->produto);
            }

            if ($parcelaDTO_model->getCategoria() != null) $this->db->where('sale_items.product_category', $parcelaDTO_model->getCategoria() );

            $this->db->group_by('fatura.id');

        } else if (($parcelaDTO_model->getProgramacao() != null ||
            $parcelaDTO_model->getCategoria() != null) &&
            $tipo == Financeiro_model::TIPO_OPERACAO_DEBITO){

            $this->db->join('parcela_fatura', 'parcela_fatura.fatura = fatura.id', 'left');
            $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela', 'left');
            $this->db->join('conta_pagar', 'conta_pagar.id = parcela.contapagarId', 'left');
            $this->db->join('sale_items', 'sale_items.id = conta_pagar.sale_item', 'left');

            $this->db->where('conta_pagar.programacaoId', $programacao->id);//todo filtro pelo contas a pagar pois é lancado 1 conta por item

            if ($parcelaDTO_model->getCategoria() != null) $this->db->where('sale_items.product_category', $parcelaDTO_model->getCategoria() );

            $this->db->group_by('fatura.id');
        }

        if ($parcelaDTO_model->getVendedor() != null)  $this->db->where('fatura.pessoa',$parcelaDTO_model->getVendedor());
        if ($parcelaDTO_model->getDataVencimentoDe() != null)  $this->db->where('fatura.dtVencimento>=', $parcelaDTO_model->getDataVencimentoDe());
        if ($parcelaDTO_model->getDataVencimentoAte() != null) $this->db->where('fatura.dtVencimento<=', $parcelaDTO_model->getDataVencimentoAte());
        if ($parcelaDTO_model->getTipoCobranca() != null) $this->db->where('fatura.tipoCobranca',$parcelaDTO_model->getTipoCobranca());
        if ($parcelaDTO_model->getFilial() != null)  $this->db->where('fatura.warehouse',$parcelaDTO_model->getFilial());
        if ($parcelaDTO_model->getReceita() != null) $this->db->where('fatura.receita',$parcelaDTO_model->getReceita());
        if ($parcelaDTO_model->getDespesa() != null) $this->db->where('fatura.despesa',$parcelaDTO_model->getDespesa());
        if ($parcelaDTO_model->getCondicaoPagamento() != null) $this->db->where('fatura.condicaoPagamento',$parcelaDTO_model->getCondicaoPagamento());
        if ($parcelaDTO_model->getCliente() != null) $this->db->where('fatura.pessoa',$parcelaDTO_model->getCliente());
        if ($parcelaDTO_model->getStatus() != null) $this->db->where('fatura.status',$parcelaDTO_model->getStatus());
        if ($parcelaDTO_model->getDataPagamentoDe() != null ) $this->db->where('fatura.dtultimopagamento>=',$parcelaDTO_model->getDataPagamentoDe());
        if ($parcelaDTO_model->getDataPagamentoAte() != null ) $this->db->where('fatura.dtultimopagamento<=',$parcelaDTO_model->getDataPagamentoAte());
        if ($parcelaDTO_model->getNumeroDocumento() != null ) $this->db->where('(fatura.numero_documento LIKE "%'.$parcelaDTO_model->getNumeroDocumento().'%" OR fatura.numero_transacao LIKE "%'.$parcelaDTO_model->getNumeroDocumento().'%") ');
        if ($parcelaDTO_model->getMovimentador() != null) $this->db->where('fatura.movimentador',$parcelaDTO_model->getMovimentador());

        $this->db->from('fatura');

        $this->db->order_by('fatura.dtVencimento, fatura.id', 'asc');
        $query = $this->db->get();

        return $query->result();
    }

    public function buscarExtrato($mes=NULL, $ano=NULL, $movimentador) {

        $this->db->select('payments.*,
            fatura.tipooperacao as tipoOperacao,
            fatura.reference as referenceFatura,
            fatura.strReceitas, 
            fatura.strDespesas, 
            pos_register.name as movimentador, 
            companies.id as clienteId, 
            companies.name as nomeCliente,
            companies.cf5 as whatsapp');

        $this->db->join('companies', 'companies.id = payments.pessoa');
        $this->db->join('fatura', 'fatura.id = payments.fatura');
        $this->db->join('pos_register', 'pos_register.id = payments.movimentador');

        $this->db->order_by('payments.date');

        if ($movimentador) $this->db->where('fatura.movimentador', $movimentador);
        if ($mes) $this->db->where('MONTH(sma_payments.date) = '.$mes);
        if ($ano) $this->db->where('YEAR(sma_payments.date) = '.$ano);

        $this->db->from('payments');
        $query = $this->db->get();

        return $query->result();
    }

    function getParcelasByFatura($faturaId) {
        $this->db->where('fatura',$faturaId);
        $q = $this->db->get('parcela_fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getContaReceberById($id) {
        return $this->__getById('conta_receber', $id);
    }

    function getContaPagarById($id) {
        return $this->__getById('conta_pagar', $id);
    }

    function buscaCobrancaByCodeIntegracao($code){

        $this->db->where('(status = "ABERTA" OR status = "PENDENTE" OR status = "QUITADA" OR status = "CANCELADA")');

        $this->db->where('code',$code);
        $this->db->limit(1);
        return $this->db->get('fatura_cobranca')->row();
    }

    function buscaCobrancaByCodeIntegracaoAberta($code){

        $this->db->where('(status = "ABERTA" OR status = "PENDENTE")');

        $this->db->where('code',$code);
        $this->db->limit(1);
        return $this->db->get('fatura_cobranca')->row();
    }

    function getParcelaOneByFatura($faturaId) {
        $this->db->where('fatura',$faturaId);
        $this->db->limit(1);
        $this->db->join('parcela', 'parcela.id = parcela_fatura.parcela');

        return $this->db->get('parcela_fatura')->row();
    }

    function buscarUltimoPagamentoParaEstorno($pagamentoId, $faturaId=NULL) {
        $this->db->select_max('date');
        if($faturaId != null) $this->db->where('fatura', $faturaId);
        $this->db->where_not_in('id',$pagamentoId);
        $this->db->limit(1);
        return $this->db->get('payments')->row();
    }

    function buscarPagamentoNaoEstornadosFatura($faturaId) {
        $this->db->where('status', null);
        $this->db->where('fatura', $faturaId);
        return $this->db->get('payments')->result();
    }
}
