<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyappRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getAllInvoiceItems($customer_id, $sale_id = NULL, $proximas_viagem = TRUE)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            
            sales.reference_no,
            sales.sale_status,
            sales.payment_status,
            
            tipo_cobranca.name as tipo_cobranca,
            condicao_pagamento.name as condicao_pagamento,

            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            products.code,
            products.image, 
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')
            ->join('sales', 'sales.id=sale_items.sale_id')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->order_by('agenda_viagem.dataSaida');

        if ($proximas_viagem) {
            $this->db->where('agenda_viagem.dataSaida >= "' . date('Y-m-d') . '" ');//SO VIAGEM QUE NAO PASSARAM
        } else {
            $this->db->where('agenda_viagem.dataSaida < "' . date('Y-m-d') . '" ');//SO VIAGEM QUE NAO PASSARAM
        }

        $this->db->where('(sales.sale_status = "faturada")');
        $this->db->where('adicional', false);

        if ($sale_id)  $this->db->where('sales.id', $sale_id);

        $q = $this->db->get_where('sale_items', array('customerClient' => $customer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItemsBySaleID($sale_id)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            
            sales.reference_no,
            sales.sale_status,
            sales.payment_status,
            
            tipo_cobranca.name as tipo_cobranca,
            condicao_pagamento.name as condicao_pagamento,

            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            products.code,
            products.image, 
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')
            ->join('sales', 'sales.id=sale_items.sale_id')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->order_by('agenda_viagem.dataSaida');

        $this->db->where('(sales.sale_status = "faturada")');
        $this->db->where('adicional', false);

        $this->db->where('sales.id', $sale_id);

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProximaViagem($customer_id, $sale_id = NULL)
    {
        $this->db->select('sale_items.*, sales.customer_id as pagador')
            ->join('sales', 'sales.id=sale_items.sale_id')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->order_by('agenda_viagem.dataSaida');

        $this->db->where('agenda_viagem.dataSaida >= "' . date('Y-m-d') . '" ');//SO VIAGEM QUE NAO PASSARAM

        if ($sale_id)  $this->db->where('sales.id', $sale_id);

        $this->db->where('(sales.sale_status = "faturada")');
        $this->db->where('adicional', false);
        $this->db->limit(1);

        $q = $this->db->get_where('sale_items', array('customerClient' => $customer_id));

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProximaViagemBySaleID($sale_id)
    {
        $this->db->select('sale_items.*')
            ->join('sales', 'sales.id=sale_items.sale_id')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')
            ->order_by('agenda_viagem.dataSaida');

        $this->db->where('agenda_viagem.dataSaida >= "' . date('Y-m-d') . '" ');//SO VIAGEM QUE NAO PASSARAM

        $this->db->where('(sales.sale_status = "faturada")');
        $this->db->where('adicional', false);
        $this->db->where('sales.id', $sale_id);

        $q = $this->db->get_where('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}
