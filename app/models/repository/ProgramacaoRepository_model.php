<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProgramacaoRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getAllDisponibilidade($filter=NULL) {

        //$filter = new ProgramacaoFilter_DTO_model();

        $this->db->select('agenda_viagem.id as programacaoId, vagas, dataSaida, dataRetorno, horaSaida, horaRetorno, products.id as produtoId, products.name, products.precoExibicaoSite, products.valor_pacote, simboloMoeda');
        $this->db->join('products', 'products.id=agenda_viagem.produto');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.enviar_site', '1');//APENAS DISPONIVEL ONLINE
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        $this->db->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida');

        if ($filter->produto) $this->db->where("products.id", $filter->produto);
        if ($filter->mes) $this->db->where("MONTH(dataSaida)", $filter->mes);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return FALSE;
    }

    public function getTotalVendaAll($programacoes) {
        $this->db->select('sma_sale_items.quantity as quantity, sale_status as status, sale_items.programacaoId, sale_items.product_id as produto')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where('sale_items.descontarVaga', 1);//TODO DESCONTAR VAGA

        $this->db->where('programacaoId in ('.$programacoes.')');

        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return 0;
    }

}