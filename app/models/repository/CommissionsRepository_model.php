<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CommissionsRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function updateStatusItemComissao($itemID, $status)
    {
        $this->db->set('status', $status);
        $this->db->where('id', $itemID);
        $this->db->update('commission_items');
    }

    public function updateStatusComissao($commissionID, $status)
    {
        $this->db->set('status', $status);
        $this->db->where('id', $commissionID);
        $this->db->update('commissions');
    }

    public function getAllCommissionItensByComissionID($comissionID)
    {
        $q = $this->db->get_where('commission_items', array('commission_id' => $comissionID));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCommissionItensByComissionSaleID($sale_id)
    {
        $this->db->where("commission_items.status in ('Pendente', 'Aprovada', 'QUITADA') ");

        $q = $this->db->get_where('commission_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getComissionsItemByID($id)
    {
        $q = $this->db->get_where('commission_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInvoiceByID($id)
    {
        $q = $this->db->get_where('sales', array('sales.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllInvoiceItems($sale_id)
    {

        $this->db->where('adicional', false);

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllHistoricoComissao($comission_item_id)
    {
        $this->db->order_by('id');
        $q = $this->db->get_where('commission_item_events', array('commission_item_id' => $comission_item_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getComissionBySaleID($sale_id, $status = 'Pendente')
    {
        $q = $this->db->get_where('commissions', array('sale_id' => $sale_id, 'status' => $status), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCommissionsToExportExcel($commissionsItemID)
    {
        $this->db
            ->select("commission_items.id as id,
                    sales.reference_no,
                    sales.date, 
                    commission_items.biller,
                    commission_items.customer,
                    commission_items.base_value,
                    commission_items.commission_percentage,
                    commission_items.commission, 
                    commission_items.sale_id")
            ->join('sales', 'sales.id = commission_items.sale_id');

        if ($commissionsItemID) {
            $this->db->where_in('commission_items.id', $commissionsItemID);
        }

        if ($this->input->post('flStatusCommission')) {
            $this->db->where('commission_items.status', $this->input->post('flStatusCommission'));
        } else {
            $this->db->where('commission_items.status', 'Pendente');
        }

        if ($this->input->post('programacaoFilter')) {
            $this->db->where('commission_items.programacao_id =', $this->input->post('programacaoFilter'));
        }

        if ($this->input->post('flDataVendaDe')) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") >=', $this->input->post('flDataVendaDe'));
        }

        if ($this->input->post('flDataVendaDeAte')) {
            $this->db->where('DATE_FORMAT(sma_sales.date,"%Y-%m-%d") <=', $this->input->post('flDataVendaDeAte'));
        }

        if ($this->input->post('billerFilter')) {
            $this->db->where('commission_items.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('flDataParcelaDe') ||
            $this->input->post('flStatusParcela') ||
            $this->input->post('flTipoCobranca') ||
            $this->input->post('flLiberacaoComissao')) {

            $subqueryf = '(SELECT sma_fatura .sale_id
               FROM sma_fatura WHERE ';

            if ($this->input->post('flStatusParcela')) {

                if ($this->input->post('flStatusParcela') == 'VENCIDA') {
                    $subqueryf .= ' DATEDIFF (sma_fatura.dtVencimento, NOW() ) < 0';
                    $subqueryf .= ' AND sma_fatura.status in ("ABERTA", "PARCIAL")';
                } else {
                    $subqueryf .= ' sma_fatura.status  = "' . $this->input->post('flStatusParcela') . '"';
                }

            } else {
                $subqueryf .= ' sma_fatura.status in ("ABERTA", "PARCIAL", "QUITADA")';
            }

            if ($this->input->post('flDataParcelaDe')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") >= "' . $this->input->post('flDataParcelaDe') . '"';
            }

            if ($this->input->post('flDataParcelaAte')) {
                $subqueryf .= ' AND DATE_FORMAT(sma_fatura.dtvencimento,"%Y-%m-%d") <= "' . $this->input->post('flDataParcelaAte') . '"';
            }

            if ($this->input->post('flTipoCobranca')) {
                $subqueryf .= ' AND sma_fatura.tipoCobranca = "' . $this->input->post('flTipoCobranca') . '"';
            }

            if ($this->input->post('flLiberacaoComissao') == 'primeira_parcela_paga') {
                $subqueryf .= ' AND (SELECT COUNT(*) FROM sma_fatura WHERE sma_fatura.sale_id = sma_sales.id AND sma_fatura.status = "QUITADA") > 0';
            }

            $subqueryf .= ')';

            $this->db->where("sma_commission_items.sale_id IN $subqueryf");

            if ($this->input->post('flLiberacaoComissao') == 'ultima_parcela_paga') {
                $this->db->where(' (sma_sales.grand_total - sma_sales.paid) <= ',0);
            }
        }

        $this->db->where("sales.payment_status in ('due','partial','paid') ");

        return $this->db->get('commission_items');

    }

}