<?php defined('BASEPATH') OR exit('No direct script access allowed');

class EventSaleRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getById($id) {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('sale_events');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function isEmailEnviado($sale_id) {
        $this->db->limit(1);
        $this->db->where("sale_id", $sale_id);
        $this->db->where("status", SaleEventService_model::EMAIL_ENVIADO);
        $q = $this->db->get('sale_events');

        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }
}