<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MotivoCancelamentoRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll() {

        $this->db->where('internal', 0);
        $q = $this->db->get('motivo_cancelamento');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}