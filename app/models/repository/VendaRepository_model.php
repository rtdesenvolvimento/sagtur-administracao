<?php defined('BASEPATH') OR exit('No direct script access allowed');

class VendaRepository_model extends CI_Model
{
    const  FATURADA = 'faturada';
    const  ORCAMENTO= 'orcamento';
    const  LISTA_ESPERA = 'lista_espera';

    public function __construct() {
        parent::__construct();
    }

    public function getServicosAdicionaisItem($vendId, $saleItemId){
        $q = $this->db->get_where('sale_item_adicional', array('saleId' => $vendId, 'sale_item' => $saleItemId) );

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItinerarios($vendId, $saleItemId){
        $q = $this->db->get_where('aereo_itinerario', array('saleId' => $vendId, 'sale_item' => $saleItemId) );

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransfers($vendId, $saleItemId){
        $q = $this->db->get_where('transfer_itinerario', array('saleId' => $vendId, 'sale_item' => $saleItemId) );

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}