<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ContractRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id)
    {
        return parent::__getById('contracts', $id);
    }

    public function getAll() {

        $q = $this->db->get('contracts');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addContract($contracts): bool
    {
        $user   = $this->site->getUser($this->session->userdata('user_id'));

        $contracts['created_by']  = $user->id;
        $contracts['created_at']  = date('Y-m-d H:i:s');
        $contracts['update_by']   = $user->id;
        $contracts['update_at']  = date('Y-m-d H:i:s');

        if ($this->db->insert("contracts", $contracts)) {
            return true;
        }
        return false;
    }

    public function editContract($id, $contracts = array()): bool
    {
        $user   = $this->site->getUser($this->session->userdata('user_id'));

        $contracts['update_by']   = $user->id;
        $contracts['update_at']  = date('Y-m-d H:i:s');

        if ($this->db->update("contracts", $contracts, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deleteContract($id): bool
    {
        if ($this->db->delete("contracts", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

}