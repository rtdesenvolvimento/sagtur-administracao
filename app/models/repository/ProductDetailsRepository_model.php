<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProductDetailsRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getAll($product_id) {

        $this->db->where('product_id', $product_id);

        $this->db->order_by('titulo');

        $q = $this->db->get('product_details');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

}