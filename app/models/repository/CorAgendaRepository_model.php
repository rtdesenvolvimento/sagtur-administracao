<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CorAgendaRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($id) {
        $q = $this->db->get_where('cor_agenda', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAll() {

        $this->db->where("active", 1);
        $q = $this->db->get('cor_agenda');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}