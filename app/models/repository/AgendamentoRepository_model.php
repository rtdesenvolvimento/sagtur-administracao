<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendamentoRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function agendamentosEncontrado($data, $dataAte, $horaInicio, $horaFinal) {

        $this->db->select(
            "agendamento.id as id, 
                agendamento.nomeProduto as servico, 
                CONCAT(".$this->db->dbprefix('agendamento').".nomeCompleto, ' <br/> ', ".$this->db->dbprefix('agendamento').".celular, '<br/>', email ) as nomeCompleto, 
                agendamento.nomeTipoCobranca as cobranca, 
                agendamento.celular,
                agendamento.valor as valor, 
                agendamento.data as data,
                agendamento.nomeProduto as nomeProduto,
                agendamento.horaInicio as horaInicio,
                agendamento.horaTermino as horaTermino,                
                agendamento.dataReserva as dataReserva,
                agendamento.status as status", FALSE);

        $this->db->where("data >= '".$data."'");
        $this->db->where("data <= '".$dataAte."'");

        $horaInicio = date('H:i:s', strtotime($horaInicio.' + 1 minutes'));
        $horaFinal = date('H:i:s', strtotime($horaFinal.' - 1 minutes'));

        $this->db->where('(("'.$horaInicio.'" BETWEEN horaInicio AND horaTermino) OR ("'.$horaFinal.'"  BETWEEN horaInicio AND horaTermino))');

        $this->db->where('(agendamento.status = "PAGO" OR 
        agendamento.status = "AGUARDANDO_PAGAMENTO" OR 
        agendamento.status = "REALIZADO" OR 
        agendamento.status = "BLOQUEADO" OR 
        agendamento.status = "ORCAMENTO")');

        $q = $this->db->get('agendamento');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $dados[] = $row;
            }
            return $dados;
        }

        return FALSE;
    }

    public function getAgendamentoById($id)
    {
        $q = $this->db->get_where('agendamento', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAgendamentosDatatables($action, $agendamentoFilter) {

        $this->load->library('datatables');

        $this->datatables
            ->select(
                "agendamento.id as id, 
                agendamento.nomeCategoria as categoria,
                agendamento.nomeProduto as servico, 
                CONCAT(".$this->db->dbprefix('agendamento').".nomeCompleto, '  ', ".$this->db->dbprefix('agendamento').".celular, ' ' , email ) as nomeCompleto, 
                agendamento.nomeTipoCobranca as cobranca, 
                agendamento.valor as valor,
                agendamento.data as data,
                CONCAT(".$this->db->dbprefix('agendamento').".horaInicio, ' - ', ".$this->db->dbprefix('agendamento').".horaTermino) as hora,
                agendamento.status as status", FALSE)

            ->from('agendamento')
            ->order_by('agendamento.data')->order_by('agendamento.horaInicio')->order_by('agendamento.horaTermino');

        if ($agendamentoFilter->getStartDate())  $this->db->where($this->db->dbprefix('agendamento').'.data >= "'.$agendamentoFilter->getStartDate().'"');
        if ($agendamentoFilter->getEndDate()) $this->db->where($this->db->dbprefix('agendamento').'.data <= "'.$agendamentoFilter->getEndDate().'"');
        if ($agendamentoFilter->getStartHour())  $this->db->where($this->db->dbprefix('agendamento').'.horaInicio >= "'.$agendamentoFilter->getStartHour().'"');
        if ($agendamentoFilter->getEndHour()) $this->db->where($this->db->dbprefix('agendamento').'.horaTermino <= "'.$agendamentoFilter->getEndHour().'"');
        if ($agendamentoFilter->getProduct()) $this->db->where($this->db->dbprefix('agendamento').'.produto = "'.$agendamentoFilter->getProduct().'"');
        if ($agendamentoFilter->getStatus()) $this->db->where($this->db->dbprefix('agendamento').'.status = "'.$agendamentoFilter->getStatus().'"');
        if ($agendamentoFilter->getName()) $this->db->where('(nomeCompleto LIKE "%'.$agendamentoFilter->getName().'%" OR celular LIKE "%'.$agendamentoFilter->getName().'%" OR email LIKE "%'.$agendamentoFilter->getName().'%" )');

        $this->datatables->add_column("Actions", $action, "id, categoria, servico, nomeCompleto, cobranca, valor, data, status");

        return $this->datatables->generate();
    }

    function getAgendamentoByContaReceber($contaReceber){
        $this->db->where('contaReceber',$contaReceber);
        $this->db->limit(1);
        return $this->db->get('agendamento')->row();
    }
}