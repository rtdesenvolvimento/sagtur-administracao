<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PlugsignRepository_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function searchClientDocument($customerID)
    {
        $this->db->where('customer_id', $customerID);
        $this->db->where('public_id IS NOT NULL');

        $q = $this->db->get('document_customers');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateDocumentCustomer($customerDocument, $encontrouCustomerDocumentToPlugsing)
    {
        $name       = $encontrouCustomerDocumentToPlugsing->name;
        $last_name  = $encontrouCustomerDocumentToPlugsing->last_name;

        if ($name) {
            $customerDocument->name = $name;
            $customerDocument->last_name = $last_name;
        }

        $customerDocument->public_id    = $encontrouCustomerDocumentToPlugsing->id;
        $customerDocument->phone        = $encontrouCustomerDocumentToPlugsing->phone;
        $customerDocument->cpf          = $encontrouCustomerDocumentToPlugsing->cpf;
        $customerDocument->address      = $encontrouCustomerDocumentToPlugsing->address;

        return $this->__save('document_customers', $customerDocument->__toArray());
    }

    public function saveDocumentCustomer($customerDocument, $customerPlugsingCreated)
    {
        $customerDocument->public_id = $customerPlugsingCreated->id;

        return $this->__save('document_customers', $customerDocument->__toArray());
    }
}