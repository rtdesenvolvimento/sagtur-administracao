<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    function getValoresPorFaixaEtariaRodoviario($produto) {
        $this->db->select('
        valor_faixa_etaria.id,
        valor_faixa_etaria.tipo, 
        valor_faixa_etaria.status,
        concat(sma_valor_faixa_etaria.tipo ," R$", sma_valor_faixa_etaria.valor) as text, 
        valor_faixa_etaria.valor as valor', false);
        $this->db->where('produto',$produto);
        $q = $this->db->get('valor_faixa_etaria');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getValoresPorFaixaEtariaByFaixaId($produto, $faixaId=NULL) {
        $this->db->where('faixaId',$faixaId);
        $this->db->where('produto',$produto);

        $this->db->join('valor_faixa', 'valor_faixa.id=valor_faixa_etaria.faixaId', 'left');

        $this->db->limit(1);
        $q = $this->db->get('valor_faixa_etaria');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    function getServicosInclusosByProductID($produtoId, $servicoId) {

        $q = $this->db->get_where('product_services_include', array('product_id' => $produtoId,'service_id' => $servicoId), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    function getValoresPorFaixaEtaria($produto, $tipo = NULL) {
        $this->db->select('
        valor_faixa_etaria.id,
        valor_faixa_etaria.faixaId,
        valor_faixa_etaria.tipo, 
        valor_faixa_etaria.status,
        
        valor_faixa.name,
        valor_faixa.note,
        valor_faixa.descontarVaga,
        valor_faixa.faixaDependente,
        valor_faixa.obrigaCPF,
        valor_faixa.captarCPF,
        valor_faixa.formato_cpf,
        valor_faixa.captarDataNascimento,
        valor_faixa.captarDocumento,
        valor_faixa.captarOrgaoEmissor,
        valor_faixa.captarEmail,
        valor_faixa.captarProfissao,
        valor_faixa.captarInformacoesMedicas,
        valor_faixa.captarNomeSocial,
        valor_faixa.obrigaOrgaoEmissor,
        valor_faixa.captar_observacao,
        valor_faixa.placeholder_observacao,

        concat(sma_valor_faixa.name ," R$", sma_valor_faixa_etaria.valor) as text, 
        valor_faixa_etaria.valor as valor', false);

        $this->db->join('valor_faixa', 'valor_faixa.id=valor_faixa_etaria.faixaId', 'left');

        if ($tipo) {
            $this->db->where('tipo',$tipo);
        }

        $this->db->where('produto',$produto);
        $this->db->where('valor_faixa_etaria.status','ATIVO');

        $q = $this->db->get('valor_faixa_etaria');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            $faixaDependente = array();
            $name = array();

            foreach ($data as $key => $row) {
                $faixaDependente[$key] = $row->faixaDependente;
                $name[$key] = $row->name;
            }

            array_multisort($faixaDependente, SORT_ASC, $name, SORT_ASC, $data);

            return $data;
        }
        return FALSE;
    }

    public function getValorFaixaByID($id) {

        $q = $this->db->get_where('valor_faixa', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;

    }

    function getLocalEmbarqueRodoviarioById($produto, $localEmbarque) {

        $this->db->select('
        local_embarque.id, 
        
        sma_local_embarque.name, 
        sma_local_embarque.endereco, 
        sma_local_embarque.numero, 
        sma_local_embarque.complemento, 
        sma_local_embarque.bairro, 
        sma_local_embarque.cidade, 
        sma_local_embarque.estado, 
        sma_local_embarque.cep, 

        local_embarque_viagem.horaEmbarque, 
        local_embarque_viagem.dataEmbarque, 
        local_embarque_viagem.note, 
        local_embarque_viagem.status ', FALSE);
        $this->db->join('local_embarque', 'local_embarque.id=local_embarque_viagem.localEmbarque', 'left');
        $this->db->where('local_embarque.id',$localEmbarque);
        $this->db->where('produto',$produto);
        $this->db->limit(1);

        return $this->db->get('local_embarque_viagem')->row();
    }

    function getLocaisEmbarqueRodoviario($produto, $localEmbarque=null) {
        $this->db->select('local_embarque.id, sma_local_embarque.name, local_embarque_viagem.horaEmbarque, local_embarque_viagem.dataEmbarque, local_embarque_viagem.note, local_embarque_viagem.status ', FALSE);
        $this->db->where('produto',$produto);
        $this->db->join('local_embarque', 'local_embarque.id=local_embarque_viagem.localEmbarque', 'left');

        if ($localEmbarque != null) $this->db->where('local_embarque.id',$localEmbarque);

        $q = $this->db->get('local_embarque_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getLocaisEmbarque($produto, $active = true) {

        $this->db->select('
        local_embarque.id, local_embarque.name as name, 
        local_embarque_viagem.dataEmbarque, 
        local_embarque_viagem.horaEmbarque, 
        local_embarque_viagem.note, 
        CONCAT(sma_local_embarque.name, " - ",  COALESCE(DATE_FORMAT(sma_local_embarque_viagem.dataEmbarque  ,"%d/%m/%Y"), "") ,  " ", DATE_FORMAT(sma_local_embarque_viagem.horaEmbarque, "%Hh%i") ) as text', FALSE);

        $this->db->join('local_embarque', 'local_embarque.id=local_embarque_viagem.localEmbarque', 'left');

        $this->db->where('produto',$produto);
        $this->db->where('local_embarque_viagem.status', 'ATIVO');

        if ($active){
            $this->db->where('local_embarque.active', 1);
        }

        $q = $this->db->get('local_embarque_viagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $text = $row->name;

                if ($row->dataEmbarque != null && $row->dataEmbarque != '0000-00-00') {
                    $text .= ' - ' . $this->sma->hrsd($row->dataEmbarque);
                }

                if ($row->horaEmbarque != null &&
                    $row->horaEmbarque != '00:00:00.0'&&
                    $row->horaEmbarque != '00:00:00') {
                    $text .=  ' às '.$this->sma->hf($row->horaEmbarque);
                }

                $row->text  = $text;
                $data[]     = $row;
            }

            $dataEmbarque = array();
            $horaEmbarque = array();
            $name = array();

            foreach ($data as $key => $row) {
                $dataEmbarque[$key] = $row->dataEmbarque;
                $horaEmbarque[$key] = $row->horaEmbarque;
                $name[$key] = $row->name;
            }

            array_multisort($dataEmbarque, SORT_ASC, $horaEmbarque, SORT_ASC, $name, SORT_ASC, $data);

            return $data;
        }
        return FALSE;
    }

    public function getTransportesRodoviario($pid, $tipoTransporte=null) {

        $this->db->select('tipo_transporte_rodoviario.id, tipo_transporte_rodoviario.automovel_id as automovel_id, tipo_transporte_rodoviario.name as text, tipo_transporte_rodoviario_viagem.status, tipo_transporte_rodoviario_viagem.habilitar_selecao_link, tipo_transporte_rodoviario_viagem.configuracao_assento_extra_id ');
        $this->db->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=tipo_transporte_rodoviario_viagem.tipoTransporte', 'left');

        if ($tipoTransporte != null) $this->db->where('tipo_transporte_rodoviario.id',$tipoTransporte);

        $q = $this->db->get_where('tipo_transporte_rodoviario_viagem', array('produto' => $pid));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransportes($pid)
    {
        $this->db->select('tipo_transporte_rodoviario.id, tipo_transporte_rodoviario.name as text, tipo_transporte_rodoviario.automovel_id, tipo_transporte_rodoviario_viagem.habilitar_selecao_link');
        $this->db->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=tipo_transporte_rodoviario_viagem.tipoTransporte', 'left');
        $this->db->order_by('tipo_transporte_rodoviario.name');

        $q = $this->db->get_where('tipo_transporte_rodoviario_viagem', array('produto' => $pid, 'tipo_transporte_rodoviario_viagem.status' => 'ATIVO'));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTipoHospedagemRodoviario($produtoId, $tipoHospedagem = NULL)
    {
        $this->db->select('tipo_hospedagem.id, tipo_hospedagem_viagem.preco as preco, sma_tipo_hospedagem.name, tipo_hospedagem_viagem.status, tipo_hospedagem.note, tipo_hospedagem.ocupacao as acomoda, tipo_hospedagem_viagem.estoque', false);
        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_tipo_hospedagem_viagem.tipoHospedagem', 'left');

        if ($tipoHospedagem != null)  $this->db->where('tipo_hospedagem.id',$tipoHospedagem);

        $q = $this->db->get_where('tipo_hospedagem_viagem', array('produto' => $produtoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTipoHospedagemRodoviarioByTipoHospedage($pid, $tipoHospedagem)
    {
        $this->db->select('tipo_hospedagem.id, tipo_hospedagem_viagem.estoque as estoque, tipo_hospedagem_viagem.preco as preco, sma_tipo_hospedagem.name, tipo_hospedagem_viagem.status', false);
        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_tipo_hospedagem_viagem.tipoHospedagem', 'left');

        $this->db->where('tipo_hospedagem.id',$tipoHospedagem);

        $q = $this->db->get_where('tipo_hospedagem_viagem', array('produto' => $pid));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTipoHospedagemRodoviarioSomenteAtivo($pid, $tipoHospedagem=null)
    {
        $this->db->select('tipo_hospedagem.id, tipo_hospedagem_viagem.preco as preco, sma_tipo_hospedagem.name, tipo_hospedagem_viagem.status', false);
        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_tipo_hospedagem_viagem.tipoHospedagem', 'left');

        if ($tipoHospedagem != null) {
            $this->db->where('tipo_hospedagem.id',$tipoHospedagem);
        }

        $this->db->where('tipo_hospedagem_viagem.status', 'ATIVO');

        $q = $this->db->get_where('tipo_hospedagem_viagem', array('produto' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTipoHospedagem($pid)
    {
        $this->db->select('tipo_hospedagem.id, 
        tipo_hospedagem_viagem.preco as preco,
        tipo_hospedagem_viagem.status,
        tipo_hospedagem_viagem.estoque,
        tipo_hospedagem.name,
        tipo_hospedagem.note,
        tipo_hospedagem.name as text', false);
        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_tipo_hospedagem_viagem.tipoHospedagem', 'left');
        $this->db->order_by('tipo_hospedagem.name');

        $q = $this->db->get_where('tipo_hospedagem_viagem', array('produto' => $pid, 'tipo_hospedagem_viagem.status'=> 'ATIVO'));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getValoresPorFaixaEtariaTipoHospedagemRodoviarioByTipoHospedagemTipoFaixa($produto, $tipoHospedagem, $faixaId) {

        $this->db->where('produto',$produto);
        $this->db->where('tipoHospedagem',$tipoHospedagem);
        $this->db->where('faixaId',$faixaId);

        $this->db->join('valor_faixa', 'valor_faixa.id=faixa_etaria_tipo_hospedagem.faixaId', 'left');

        $q = $this->db->get('faixa_etaria_tipo_hospedagem');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    function getValoresPorFaixaEtariaTipoHospedagemRodoviario($produto, $tipoHospedagem = null) {

        $this->db->select('
        tipo_hospedagem.id, 
        faixa_etaria_tipo_hospedagem.tipo,
        faixa_etaria_tipo_hospedagem.valor as preco, 
        faixa_etaria_tipo_hospedagem.status,
        sma_tipo_hospedagem.name', false);

        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=sma_faixa_etaria_tipo_hospedagem.tipoHospedagem', 'left');
        $this->db->where('produto',$produto);

        if ($tipoHospedagem != null) $this->db->where('tipo_hospedagem.id',$tipoHospedagem);

        $q = $this->db->get('faixa_etaria_tipo_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getValoresPorFaixaEtariaTipoHospedagemByTipoHopedagem($produto, $tipoHospedagemId) {

        $this->db->select('
        faixa_etaria_tipo_hospedagem.id, 
        faixa_etaria_tipo_hospedagem.tipo,
        faixa_etaria_tipo_hospedagem.valor, 
        faixa_etaria_tipo_hospedagem.status,
        
        valor_faixa.id as faixaId,
        valor_faixa.name,
        valor_faixa.note,
        valor_faixa.descontarVaga,
        valor_faixa.faixaDependente,
        valor_faixa.obrigaCPF,
        valor_faixa.captarCPF,
        valor_faixa.formato_cpf,
        valor_faixa.captarDataNascimento,
        valor_faixa.captarDocumento,
        valor_faixa.captarOrgaoEmissor,
        valor_faixa.captarEmail,
        valor_faixa.captarProfissao,
        valor_faixa.captarInformacoesMedicas,
        valor_faixa.captarNomeSocial,
        valor_faixa.obrigaOrgaoEmissor,
        valor_faixa.captar_observacao,
        valor_faixa.placeholder_observacao,

        faixa_etaria_tipo_hospedagem.tipo,
        concat(sma_tipo_hospedagem.name," R$", 
        sma_faixa_etaria_tipo_hospedagem.valor) as text', false);

        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=faixa_etaria_tipo_hospedagem.tipoHospedagem');
        $this->db->join('valor_faixa', 'valor_faixa.id=faixa_etaria_tipo_hospedagem.faixaId');

        $this->db->where('produto',$produto);
        $this->db->where('tipo_hospedagem.id',$tipoHospedagemId);

        $this->db->where('faixa_etaria_tipo_hospedagem.status','ATIVO');

        $this->db->order_by('valor_faixa.faixaDependente asc, valor_faixa.name');

        $q = $this->db->get('faixa_etaria_tipo_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getFaixasTipoHospedagemProduto($produto = NULL) {

        $this->db->select('
        faixa_etaria_tipo_hospedagem.id, 
        faixa_etaria_tipo_hospedagem.tipo,
        faixa_etaria_tipo_hospedagem.valor, 
        faixa_etaria_tipo_hospedagem.status,
        
        valor_faixa.id as faixaId,
        valor_faixa.name,
        valor_faixa.descontarVaga,
        
        faixa_etaria_tipo_hospedagem.tipo', false);

        $this->db->join('valor_faixa', 'valor_faixa.id=faixa_etaria_tipo_hospedagem.faixaId');

        $this->db->where('produto',$produto);

        $this->db->where('faixa_etaria_tipo_hospedagem.status','ATIVO');

        $this->db->group_by('valor_faixa.id');

        $q = $this->db->get('faixa_etaria_tipo_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getValoresPorFaixaEtariaTipoHospedagem($produto = NULL, $faixaId = NULL) {

        $this->db->select('
        tipo_hospedagem.id, 
        faixa_etaria_tipo_hospedagem.tipo,
        faixa_etaria_tipo_hospedagem.valor as preco, 
        faixa_etaria_tipo_hospedagem.status,
        tipo_hospedagem.name,
        faixa_etaria_tipo_hospedagem.tipo,
        concat(sma_tipo_hospedagem.name," R$", sma_faixa_etaria_tipo_hospedagem.valor) as text', false);

        $this->db->join('tipo_hospedagem', 'tipo_hospedagem.id=faixa_etaria_tipo_hospedagem.tipoHospedagem');
        $this->db->where('produto',$produto);

        if ($faixaId != null) $this->db->where('faixa_etaria_tipo_hospedagem.faixaId',$faixaId);

        $this->db->where('faixa_etaria_tipo_hospedagem.status','ATIVO');

        $q = $this->db->get('faixa_etaria_tipo_hospedagem');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getServicosAdicionais($pid)
    {
        $this->db->select('products.id, 
        products.name as text,
        products.code as code,
        combo_items.unit_price as price,
        categories.id as category_id, 
        categories.name as category');

        $this->db->join('products', 'products.id=combo_items.item_id', 'left');
        $this->db->join('categories', 'categories.id=products.category_id', 'left');

        $q = $this->db->get_where('combo_items', array('product_id' => $pid));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getServicosAdicionaisFaixaEtariaRodoviarioParaLinkDeReservas($produto, $servicoAdicional) {

        $this->db->select('faixa_etaria_adicional.id as id, faixa_etaria_adicional.valor, valor_faixa.note, valor_faixa.name, valor_faixa.id as faixaId');
        $this->db->join('valor_faixa', 'valor_faixa.id=faixa_etaria_adicional.faixaId', 'left');

        $this->db->where('produto',$produto);
        $this->db->where('servico', $servicoAdicional);
        $this->db->where('status', 'ATIVO');

        $this->db->order_by('valor_faixa.name');

        $q = $this->db->get('faixa_etaria_adicional');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    function getServicosAdicionaisFaixaEtariaRodoviario($produto, $servicoAdicional=null, $tipo = null) {

        $this->db->select('faixa_etaria_adicional.*, faixa_etaria_adicional.tipo as tipoFaixaEtaria, 
         faixa_etaria_adicional.valor as faixaEtariaValor');

        $this->db->where('produto',$produto);

        if ($servicoAdicional != null) {
            $this->db->where('servico', $servicoAdicional);
        }

        if ($tipo != null) {
            $this->db->where('tipo', $tipo);
        }

        $q = $this->db->get('faixa_etaria_adicional');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getServicosAdicionaisPorFaixaEtaria($productId, $servico, $faixaId)
    {
        $this->db->select('products.id as id, 
        products.name as text,
        products.code as code,
        faixa_etaria_adicional.valor as price,
        categories.id as category_id, 
        categories.name as category');

        $this->db->join('combo_items', 'combo_items.id=faixa_etaria_adicional.servico', 'left');
        $this->db->join('products', 'products.id=faixa_etaria_adicional.servico', 'left');
        $this->db->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('faixa_etaria_adicional',
            array('produto' => $productId,
                'servico' => $servico,
                'faixaId' => $faixaId,
                'faixa_etaria_adicional.status' => 'ATIVO'));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getHospedagens($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


}