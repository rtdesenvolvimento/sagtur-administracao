<?php defined('BASEPATH') OR exit('No direct script access allowed');

class WelcomeRepository_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getTotalReceberMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpagar) as totalReceberMes");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("{$this->db->dbprefix('fatura')}.status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $this->db->join('companies', 'companies.id = fatura.pessoa');
        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalRecebimentoMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpago) as totalRecebimentoMes");
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalRecebimentoAno() {


        $ano = date('Y');

        $this->db->select("SUM(valorfatura) as totalRecebimentoAno");
        $this->db->where("status IN ('QUITADA')");//TODO Todas as faturas quitadas
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("YEAR(dtVencimento) = '{$ano}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalReceberAno() {

        $ano = date('Y');

        $this->db->select("SUM(valorfatura) as totalReceberAno");
        $this->db->where("tipooperacao", 'CREDITO');
        $this->db->where("{$this->db->dbprefix('fatura')}.status IN ('ABERTA')");
        $this->db->where("YEAR(dtVencimento) = '{$ano}' ");
        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalPagamentoMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpago) as totalPagamentoMes");
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("tipooperacao", 'DEBITO');
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getTotalPagarMes() {

        $start_date = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $end_date = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);

        $this->db->select("SUM(valorpagar) as totalPagarMes");
        $this->db->where("tipooperacao", 'DEBITO');
        $this->db->where("status IN ('ABERTA', 'PARCIAL', 'QUITADA', 'VENCIDA')");
        $this->db->where("dtVencimento >= '{$start_date}' ");
        $this->db->where("dtVencimento <= '{$end_date}' ");

        $q = $this->db->get_where('fatura', array(), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getRelatorioVendasPorDespesas()
    {
        $myQuery = "SELECT S.month,
            COALESCE(S.Sales, 0) as sales,
            COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  date_format(date, '%Y-%m') Month,
                SUM(total) Sales
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL 48 MONTH ) AND
                sale_status in ('faturada', 'orcamento', 'lista_espera')
                GROUP BY date_format(date, '%Y-%m')) S
        LEFT JOIN ( SELECT  date_format(dtvencimento, '%Y-%m') Month,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                         ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                        GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FD
        
        ON S.Month = FD.Month
        GROUP BY S.Month
        ORDER BY S.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBestSeller($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date)  $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $sp = "( SELECT 
            si.product_id, 
            SUM( si.quantity ) soldQty, 
            s.date as sdate
         from " . $this->db->dbprefix('sales') . " s JOIN " . $this->db->dbprefix('sale_items') . " si on s.id = si.sale_id 
         where s.date >= '{$start_date}' and s.date < '{$end_date}' and s.sale_status in ('faturada', 'orcamento', 'lista_espera') group by si.product_id ) PSales";

        $this->db
            ->select("CONCAT(" . $this->db->dbprefix('products') . ".name, ' (', " . $this->db->dbprefix('products') . ".code, ')') as name, COALESCE( PSales.soldQty, 0 ) as SoldQty", FALSE)
            ->from('products', FALSE)
            ->join($sp, 'products.id = PSales.product_id', 'left')
            ->order_by('PSales.soldQty desc')
            ->limit(10);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVendasPorVendedor($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("companies.name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('companies', 'companies.id = sales.biller_id', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('companies.name')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVendasPorCategoria($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("nmCategoria as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('nmCategoria')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getVendasPorMeioDivulgacao($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("meio_divulgacao.name as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('meio_divulgacao', 'meio_divulgacao.id = sales.meioDivulgacao', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('meio_divulgacao.id')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getVendasPorTipoCobranca($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("tipo_cobranca.name as name, SUM(subtotal) as subtotal", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = sales.tipoCobrancaId', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->where("sale_status in ('faturada', 'orcamento', 'lista_espera')")
            ->group_by('tipo_cobranca.id')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
}