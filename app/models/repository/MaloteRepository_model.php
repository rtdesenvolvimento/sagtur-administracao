<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MaloteRepository_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getByID($id)
    {
        $q = $this->db->get_where('malotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getFaturasByMalote($maloteID)
    {
        $q = $this->db->get_where('malote_faturas', array('malote_id' => $maloteID));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function openMaloteModel($id) {
        return $this->__toModel($this->__getById('malotes', $id), new Malote_model());
    }
}