<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roomlist_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function getAll($programacaoId)
    {
        $this->db->where('programacao_id', $programacaoId);

        $q = $this->db->get('room_list');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function add($data = array()) {
        if ($this->db->insert('room_list', $data)) {
            return true;
        }
        return false;
    }

    public function update($id, $data = array()) {
        if ($this->db->update('room_list', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function delete($id) {
        if ($this->db->delete('room_list', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getById($id)
    {
        $q = $this->db->get_where('room_list', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}
