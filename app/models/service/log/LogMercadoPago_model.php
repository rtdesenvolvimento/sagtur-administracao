<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 ************************************************************************
 Copyright [2011] [PagSeguro Internet Ltda.]

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 ************************************************************************
 */

/**
 * Logging class
 */
class LogMercadoPago_model extends CI_Model
{
    private static $fileLocation;

    public function __construct()
    {
        parent::__construct();
        self::reLoad();
    }

    public function reLoad()
    {
        self::createFile();
    }

    /**
     * Creates the log file
     * @throws Exception
     * @return boolean
     */
    public function createFile()
    {
        $defaultPath = 'files/'.$_SESSION['cnpjempresa'].'/log/';

        if (!file_exists($defaultPath)) mkdir($defaultPath, 0777, true);

        $defaultName = 'LogMercadoPago_'.$_SESSION['cnpjempresa'].'.log';
        self::$fileLocation = $defaultPath . DIRECTORY_SEPARATOR . $defaultName;

        try {
            $f = fopen(self::$fileLocation, "a");
            fclose($f);
            return true;
        } catch (Exception $e) {
            echo $e->getMessage() . " - Can't create log file. Permission denied. File location: " .
                self::$fileLocation;
            return false;
        }
    }

    /**
     * Prints a info message in the log file
     * @param String $message
     */
    public static function info($message)
    {
        self::logMessage($message, 'info');
    }

    /**
     * Prints a warning message in the log file
     * @param String $message
     */
    public static function warning($message)
    {
        self::logMessage($message, 'warning');
    }

    /**
     * Prints an error message in the log file
     * @param String $message
     */
    public static function error($message)
    {
        self::logMessage($message, 'error');
    }

    /**
     * Prints a debug message in the log file
     * @param String $message
     */
    public static function debug($message)
    {
        self::logMessage($message, 'debug');
    }

    /**
     * Logs a message
     * @param String $message
     * @param String $type
     * @throws Exception
     * @return void|boolean
     */
    private static function logMessage($message, $type = null)
    {
        try {

            $file = fopen(self::$fileLocation, "a");
            $date_message = "{" . @date("d/m/Y H:i:s", time()) . "}";

            switch ($type) {
                case 'info':
                    $type_message = "[Info]";
                    break;
                case 'warning':
                    $type_message = "[Warning]";
                    break;
                case 'error':
                    $type_message = "[Error]";
                    break;
                case 'debug':
                default:
                    $type_message = "[Debug]";
                    break;
            }
            $str = "$date_message $type_message $message";
            fwrite($file, "$str \r\n");
            fclose($file);

        } catch (Exception $e) {
            echo $e->getMessage() . " - Can't create log file. Permission denied. File location: " .
                self::$fileLocation;
        }

    }

    /**
     * Retrieves the log messages
     * @param integer $negativeOffset
     * @param boolean|string $reverse
     *
     * @return boolean|string
     */
    public static function getHtml($negativeOffset = null, $reverse = null)
    {

        if (file_exists(self::$fileLocation) && $file = file(self::$fileLocation)) {
            if ($negativeOffset !== null) {
                $file = array_slice($file, (-$negativeOffset), null, true);
            }
            if ($reverse) {
                $file = array_reverse($file, true);
            }
            $content = "";
            foreach ($file as $key => $value) {
                $html = ("<p>" . str_replace("\n", "<br>", $value) . "</p>");
                $html = str_replace("[", "<strong>", $html);
                $html = str_replace("]", "</strong>", $html);
                $html = str_replace("{", "<span>", $html);
                $html = str_replace("}", "</span>", $html);
                $content .= $html;
            }
        }
        return isset($content) ? $content : false;
    }
}
