<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FolderService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        //service
        $this->load->model('service/signature/PlugsignService_model');

        //model
        $this->load->model('model/Folder_model', 'Folder_model');
    }

    public function addFolder($folder, $user = null): int
    {
        try {

            $this->__iniciaTransacao();

            if (!$user) {
                $user = $this->site->getUser($this->session->userdata('user_id'));
            }

            $folder->folder_group_id   = 1;
            $folder->created_by         = $user->id;
            $folder->created_at         = date('Y-m-d H:i:s');
            $folder->update_by          = $user->id;

            $folder_id = $this->__save('folders', $folder->__toArray());

            $this->createFolder($folder_id);

            $this->__confirmaTransacao();

            return $folder_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function createFolder($folderID): bool
    {

        $folder = new Folder_model($folderID);

        $folderCreated = $this->PlugsignService_model->createFolder(array('name' => $folder->name));

        $this->__edit('folders', array('public_id' => $folderCreated->id), 'id', $folderID);

        return true;
    }

    public function editFolder($folder): bool
    {
        try {

            $this->__iniciaTransacao();

            $data = array(
                'name' => $folder->name,
            );

            if ($folder->public_id) {
                $this->PlugsignService_model->editFolder($data, $folder->public_id);
            }

            $this->__edit('folders', $data, 'id', $folder->id);

            $this->__confirmaTransacao();

            return true;

        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    public function deleteFolder($folderID): bool
    {
        try {

            $this->__iniciaTransacao();

            $folder = new Folder_model($folderID);

            if ($folder->public_id) {
                $this->PlugsignService_model->deleteFolder($folder->public_id);
            }

            $this->__delete('folders', 'id', $folder->id);

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }

    }

}