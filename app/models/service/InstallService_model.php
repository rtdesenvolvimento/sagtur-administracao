<?php defined('BASEPATH') OR exit('No direct script access allowed');

class InstallService_model extends CI_Model
{

    private $servers = [
        'server1' => ['ip' => 'node113848-sagturserver.sp1.br.saveincloud.net.br', 'user' => 'resultatec@gmail.com', 'password' => 'W#23M5(k:)G3'],
    ];

    public function __construct()
    {
        parent::__construct();

        //service
        $this->load->model('service/SendMessageService_model', 'SendMessageService_model');

        //repository
        $this->load->model('repository/MessageGroupRepository_model', 'MessageGroupRepository_model');

        $this->load->model('companies_model');
        $this->load->model('settings_model');

        $this->load->library('whatsapp');
        $this->load->library('ion_auth');

        //tokens do telefone da satgtur
        $this->whatsapp->instance       = '3D7ACA0042A4A0E77763F69CB7A0D002';
        $this->whatsapp->token          = 'D2A7B1790022B7F512BC5062';
        $this->whatsapp->client_token   = 'Fe908c5f7c5d243ba843f275b8d99af45S';

    }

    public function install($installDTO)
    {
        try {

            //$installDTO = new InstallDTO_model();

            // 1. Verifica se o banco de dados (nome informado) já existe
            if ($this->databaseExists($installDTO->db_name)) {
                throw new Exception('O banco de dados já existe: ' . $installDTO->db_name);
            }

            // 2. Criar o banco de dados (nome informado)
            if (!$this->createDatabase($installDTO->db_name)) {//TODO ARRUMAR A BASE DE DADOS
                throw new Exception('Falha ao criar o banco de dados. '. $installDTO->db_name);
            }

            // 3. Importar o banco de dados padrão (arquivo .sql)
            if (!$this->importDefaultDatabase($installDTO->db_name)) {
                throw new Exception('Falha ao importar o SQL padrão.');
            }

            // 4. Atualizar dados da empresa
            if (!$this->insert_company_data($installDTO)) {
                throw new Exception('Falha ao inserir os dados da empresa.');
            }

            // 5. Criar subdomínio inicial (usando Cloudflare)
            if ($installDTO->criar_subdominio) {
                if (!$this->createSubdomain($installDTO->db_name)) {
                    throw new Exception('Falha ao criar o subdomínio inicial.');
                }
            }

            // 6. Criação do grupo de trabalho (automação para criar o grupo de trabalho)
            //if (!$this->createWorkGroup($installDTO->nome_empresa)) {
               //throw new Exception('Falha ao criar o grupo de trabalho no whatsapp.');
            //}


        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function databaseExists($dbName): bool
    {
        $query = $this->db->query("SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$dbName'");
        return $query->num_rows() > 0;
    }

    protected function createDatabase($dbName): bool
    {
        $sql = "CREATE DATABASE IF NOT EXISTS `$dbName` CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci;";
        $result = $this->db->query($sql);

        if ($result) {
            return true;
        }
        return false;
    }

    protected function importDefaultDatabase($dbName): bool
    {

        try {

            $this->__iniciaTransacao();

            $dbfile = './files/sql/0afffab2b53187005a68be175f419675.sql';

            if (!file_exists($dbfile)) {
                throw new Exception('Arquivo SQL Padrão não encontrado.');
            }

            $sqlContent = file_get_contents('./files/sql/0afffab2b53187005a68be175f419675.sql');

            if (empty($sqlContent)) {
                throw new Exception('O arquivo SQL está vazio.');
            }

            $dbConfig = $this->getConfiguracaoDB($dbName);
            $dbSelected = $this->load->database($dbConfig, TRUE);

            // Quebra o conteúdo em linhas
            $lines = explode("\n", $sqlContent);
            $sqlWithoutComments = '';
            $inCommentBlock = false;

            foreach ($lines as $line) {
                $trimLine = trim($line);

                if ($inCommentBlock) {
                    if (strpos($trimLine, '*/') !== false) {
                        $inCommentBlock = false;
                    }
                    continue;
                }

                if (strpos($trimLine, '/*') === 0) {
                    $inCommentBlock = true;
                    continue;
                }

                if ($trimLine === '' || strpos($trimLine, '--') === 0 || strpos($trimLine, '//') === 0) {
                    continue;
                }

                $sqlWithoutComments .= $line . "\n";
            }

            $queries = explode(';', $sqlWithoutComments);

            foreach ($queries as $query) {
                $query = trim($query);
                if (!empty($query)) {
                    if (!$dbSelected->query($query)) {
                        log_message('error', 'Erro ao executar query: ' . $query);
                        return false;
                    }
                }
            }

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $e) {
            $this->__cancelaTransacao();
            throw new Exception($e->getMessage());
        }
    }

    protected function createSubdomain($subdomain): bool
    {
        $apiToken = 'BqROWbIuGN7Hf1jF05qpRYyU0yGIVto75beulRv_';
        $zoneId   = 'ccea2520b207b1f7927f9354fb6f4a60';

        $data = [
            'type'    => 'CNAME',
            'name'    => $subdomain, // isso resultará em subdomain.seusite.com
            'content' => 'suareservaonline.com.br',
            'ttl'     => 120,
            'proxied' => true,
        ];

        $ch = curl_init("https://api.cloudflare.com/client/v4/zones/{$zoneId}/dns_records");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer ' . $apiToken,
        ]);

        $result = curl_exec($ch);
        if(curl_errno($ch)){
            log_message('error', 'cURL error: ' . curl_error($ch));
            curl_close($ch);
            return false;
        }
        curl_close($ch);

        $response = json_decode($result, true);
        if (isset($response['success']) && $response['success'] === true) {
            return true;
        } else {
            throw new Exception('Erro na API do Cloudflare: ' . print_r($response, true));
        }
    }

    protected function createWorkGroup($name): bool
    {
        try {

            $response = $this->whatsapp->create_group($name,  ['5548998235746', '5548998579104']);
            $this->whatsapp->add_admin_group($response->phone, ['5548998235746', '5548998579104']);

            return true;

        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function insert_company_data($installDTO): bool
    {
        $customer = $this->site->getCompanyByID($installDTO->customer_id);
        $dbConfig = $this->getConfiguracaoDB($installDTO->db_name);
        $dbSelected = $this->load->database($dbConfig, TRUE);

        $biller_id = $this->create_biller($customer, $installDTO, $dbSelected);

        $this->create_user($customer, $biller_id, $dbSelected);
        $this->updateSetting($customer, $biller_id, $installDTO, $dbSelected);
        $this->updateTipoCobranca($customer, $dbSelected);
        $this->upodateDataAgendamento($dbSelected);
        $this->insert_shop_settings($dbSelected);

        return true;
    }

    private function create_biller($customer, $installDTO, $dbSelected) {

        $invoice_footer = '✔️ '.$customer->address.' '.$customer->numero.' ' . $customer->complemento.', '.$customer->bairro.' - '.$customer->city.'/'.$customer->state.' CEP '.$customer->postal_code.'
✔️ E-mail: '.$customer->email.'
✔️ CNPJ: '.$customer->vat_no.'
✔️ Telefone: '.$customer->cf5;

        $data_biller = array(
            'group_name' => 'biller',
            'group_id' => NULL,
            'name' => $customer->nome_responsavel,//nomeres ponsavel
            'email' => $customer->email,
            'company' => $customer->name,
            'address' => $customer->address,
            'vat_no' => $customer->vat_no,
            'city' => $customer->city,
            'state' => $customer->state,
            'sexo' => $customer->sexo,
            'postal_code' => $customer->postal_code,
            'country' => $customer->country,
            'phone' => $customer->phone,
            'cf1' => $customer->cf1,
            'cf2' => $customer->cf2,
            'cf3' => $customer->cf3,
            'cf4' => $customer->cf4,
            'cf5' => $customer->cf5,//whatsapp
            'cf6' => $customer->cf6,
            'comissao' => 0,
            'invoice_footer' => $invoice_footer,
            'logo' => $installDTO->logo,
        );

        if ($dbSelected->insert('companies', $data_biller)) {
            $cid = $dbSelected->insert_id();
            return $cid;
        }
        return false;
    }

    public function create_user($customer, $biller_id, $dbSelected)
    {

        $fullName = $customer->nome_responsavel;
        $nameParts = explode(' ', $fullName);
        $password = '098f0cb0d5d1eae3c7a469d45caf801673cce7da';//TODO SENHA PADRAO

        $firstName = $nameParts[0];
        $lastName = array_pop($nameParts);
        $remainingName = implode(' ', $nameParts);

        $user_data = array(
            'first_name' => $firstName,
            'last_name' => $remainingName.' '.$lastName,
            'company' => $customer->name,
            'phone' => $customer->cf5,
            'gender' => 'male',
            'group_id' => 1,
            'biller_id' => $biller_id,
            'view_right' => 1,
            'edit_right' => 1,
            'allow_discount' => 1,
            'warehouse_id' => null,
            'username' => $customer->email,
            'password' => $password,
            'email' => $customer->email,
            'ip_address' => $this->input->ip_address(),
            'created_on' => time(),
            'last_login' => time(),
            'active' => 1
        );

        if ($dbSelected->insert('users', $user_data)) {
            $cid = $dbSelected->insert_id();
            return $cid;
        }
        return false;
    }

    private function updateSetting($customer, $biller_id, $installDTO, $dbSelected): bool
    {

        $data_setting = array(
            'site_name' => $customer->name,
            'default_biller' => $biller_id,
            'theme_color' => $installDTO->theme_color,
            'theme_color_secondary' => $installDTO->theme_color,
            'logo2' => $installDTO->logo,
        );

        $dbSelected->where('setting_id', 1);
        if ($dbSelected->update('settings', $data_setting)) {
            return true;
        }
        return false;
    }

    private function updateTipoCobranca($customer, $dbSelected)
    {
        $data_tipo_cobranca = array(
            'note' => '<p><strong><em>CHAVE CNPJ '.$customer->vat_no.'</em></strong></p>',
            'configurar_pix' => 1,
            'chave_pix' => $customer->vat_no,
            'titular' => $customer->name,
            'telefone' => $customer->cf5,
            'email' => $customer->email,
        );

        $dbSelected->where('id', 228);
        if ($dbSelected->update('tipo_cobranca', $data_tipo_cobranca)) {
            return true;
        }

        return false;
    }

    private function upodateDataAgendamento($dbSelected): bool
    {
        $ultimoDiaMes = date('Y-m-t');

        $data_agendamento = array(
            'dataSaida' => $ultimoDiaMes,
            'dataRetorno' => $ultimoDiaMes,
        );

        $dbSelected->where('id', 123);
        if ($dbSelected->update('agenda_viagem', $data_agendamento)) {
            return true;
        }

        return false;
    }

    private function insert_shop_settings($dbSelected): bool
    {
        $sql = "INSERT INTO `sma_shop_settings` (`id`, `shop_name`, `version`, `logo`, `logo_header`, `all_products`, `head_code`, `body_code`, `filtar_local_embarque`, `captar_meio_divulgacao`, `ocultar_embarques_produto`, `sections_code`, `cache_minutes`, `web_page_caching`, `ocultar_horario_saida`, `mostrar_taxas`) VALUES (1, 'CONFIGURAÇÕES DA LOJA VIRTUAL', '1.0.0', '', '', 0, '<!-- Google tag4 (gtag.js) PADRAO SAGTUR -->\n<script async src=\'https://www.googletagmanager.com/gtag/js?id=G-QEMX461YX9\'></script>\n<script>\n  window.dataLayer = window.dataLayer || [];\n  function gtag(){dataLayer.push(arguments);}\n  gtag(\'js\', new Date());\n\n  gtag(\'config\', \'G-QEMX461YX9\');\n</script>\n\n<!-- Google tag (gtag.js) PADRAO SAGTUR -->\n<script async src=\'https://www.googletagmanager.com/gtag/js?id=UA-165016020-1\'></script>\n<script>\n  window.dataLayer = window.dataLayer || [];\n  function gtag(){dataLayer.push(arguments);}\n  gtag(\'js\', new Date());\n\n  gtag(\'config\', \'UA-165016020-1\');\n</script>', NULL, 1, 1, 0, NULL, 1440, 0, 0, 0);";

        $dbSelected->query($sql);

        return true;
    }

    public function post($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        echo $output;
    }

    protected function getConfiguracaoDB($cnpjempresa): array
    {
        $db_config = array(
            'dsn'	=> '',

            //'hostname' => 'node157874-sagtur-production-db-server.jelastic.saveincloud.net:14084',
            'hostname' => '10.100.53.149',
            'username' => 'root',
            'password' => 'DOYqah54213',

            //'hostname' => 'localhost',
            //'username' => 'root',
            //'password' => '',

            'database' => $cnpjempresa,
            'dbdriver' => 'mysqli',
            'dbprefix' => 'sma_',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => TRUE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => FALSE
        );

        return $db_config;
    }

}