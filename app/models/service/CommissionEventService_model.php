<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CommissionEventService_model extends CI_Model
{

    const VALOR_COMISSAO_ALTERADO = 'Valor da Comissão Alterado';
    const COMISSAO_CANCELADA = 'Comissão Cancelada';

    const COMISSAO_GERADA = 'Comissão Gerada';

    const COMISSAO_INSERIDA_NO_FECHAMENTO = 'Comissão Inserida no Fechamento';

    const COMISSAO_REMOVIDA_DO_FECHAMENTO = 'Comissão Removida do Fechamento';

    const COMISSAO_REMOVIDA_DO_FECHAMENTO_CANCELADO = 'Comissão Removida do Fechamento Cancelado';

    const COMISSAO_APROVADA = 'Comissão Aprovada Para Pagamento';

    const COMISSAO_PAGA = 'Comissão Paga';

    const COMISSAO_PAGAMENTO_ESTORNADO = 'Comissão Pagamento Estornado';

    public function __construct() {

        parent::__construct();

        $this->load->model('model/EventComission_model', 'EventComission_model');
    }

    public function addEvent($commission_id, $commission_item_id, $event_text, $status, $user_id) {
        try {

            $event = new EventComission_model();

            $this->__iniciaTransacao();

            $event->setDate(date('Y-m-d H:i:s'));
            $event->setCommissionId($commission_id);
            $event->setCommissionItemId($commission_item_id);
            $event->setEvent($event_text);
            $event->setStatus($status);
            $event->setLog(print_r($_POST, true));
            $event->setCreatedBy($user_id);
            $event->setCreatedAt(date('Y-m-d H:i:s'));

            $event_id = $this->__save('commission_item_events', $event->__toArray());

            $this->__confirmaTransacao();

            return $event_id;

        } catch(Exception $ex) {
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

}