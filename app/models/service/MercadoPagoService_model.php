<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;

class MercadoPagoService_model extends CI_Model
{
    const MENSAGEM_BAIXA_PAGMENTO = 'Pagamento baixado pelo sistema automaticamente pelo modulo de integração MERCADO PAGO.<br/>Na data ';

    const BOLETO = "bolbradesco";
    const PIX = "pix";
    const CARTAO_CREDITO = "cartao_credito";
    const LINK_PAGAMENTO = "link_pagamento";
    const CARTAO_CREDITO_FATURA = "cartao_credito_fatura";

    public $setting;

    public function __construct() {
        parent::__construct();

        $this->load->model('dto/CobrancaDTO_model', 'CobrancaDTO_model');
        $this->load->model('dto/PessoaCobrancaDTO_model', 'PessoaCobrancaDTO_model');
        $this->load->model('dto/RetornoCobrancaDTO_model','RetornoCobrancaDTO_model');
        $this->load->model('dto/BaixaCobrancaDTO_model', 'BaixaCobrancaDTO_model');
        $this->load->model('dto/BaixaFaturaCobrancaDTO_model', 'BaixaFaturaCobrancaDTO_model');
        $this->load->model('dto/BaixaPagamentoFaturaCobrancaDTO_model', 'BaixaPagamentoFaturaCobrancaDTO_model');

        $this->load->model('service/log/LogMercadoPago_model', 'LogMercadoPago_model');

        $this->load->model('service/exeption/MercadoPagoException_model', 'MercadoPagoException_model');
        $this->load->model('service/LinkpagamentoService_model', 'LinkpagamentoService_model');

        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');

        $this->load->model('settings_model');

    }

    public function configurarIntegracao() {

        $mercadoPago = $this->settings_model->getMercadoPagoSettings();

        if ($mercadoPago->active) {

            $this->setting = $this->site->get_setting();

            if ($mercadoPago->sandbox) {
                $token = $mercadoPago->account_token_private_sandbox;
            } else {
                $token = $mercadoPago->account_token_private;
            }

            MercadoPago\SDK::setAccessToken($token);
            MercadoPago\SDK::setIntegratorId("dev_bb44ce3133ba11edac310242ac130004");//TODO NOSSO ID DE DEV NO MERCADO PAGO
            MercadoPago\SDK::setPlatformId("dev_e35fbefb331111ee90612225b209fd90");

            /*
            MercadoPago\SDK::setCorporationId("CORPORATION_ID");
            */
        }
        return null;
    }

    public function emitirCobranca($cobranca) {

        $this->configurarIntegracao();

        $tipo = $cobranca->tipoCobranca;

        if ($this->isBoleto($tipo)) return $this->boleto($cobranca);
        if ($this->isPix($tipo)) return $this->pix($cobranca);
        if ($this->isLinkPagamento($tipo)) return $this->link_pagamento($cobranca);
        if ($this->isCartaoDeCredito($tipo)) return $this->cartao_credito($cobranca);

        return new Exception("O Tipo ".$tipo.' não permite a emissão de cobranças no MercadoPago');
    }

    public function gerarCobranca($cobranca, $tipo) {

        if ($tipo == MercadoPagoService_model::CARTAO_CREDITO) {
            $payment = $this->getPayment($cobranca, $tipo, (int) $cobranca->numeroParcelas);

            $cardName = explode('#', $cobranca->cardName);

            $payment->token = $cobranca->senderHash;
            $payment->payment_method_id = $cardName[0];
            $payment->issuer_id = (int) $cardName[0];

        } else {
            $payment = $this->getPayment($cobranca, $tipo);
        }

        $uuid = $this->generateUUID();

        MercadoPago\SDK::config()->configure(['X-meli-session-id' => $cobranca->deviceId]);
        MercadoPago\SDK::addCustomTrackingParam('X-meli-session-id', $cobranca->deviceId);
        MercadoPago\SDK::config()->configure(['X-Idempotency-Key' => $uuid]);
        MercadoPago\SDK::addCustomTrackingParam('X-Idempotency-Key', $uuid);

        $payment->save();

        $dados = $this->gerarDadosFatura($cobranca, $payment, $tipo);

        return $this->tratarRetornoDaTransacao($dados, $payment, $tipo);
    }

    public function getMetodosPagamentoExcluidos() {
        return array(
            array("id" => "ticket"),
            array("id" => "debit_card"),
        );
    }

    public function link_pagamento($cobranca) {

        $preference = new MercadoPago\Preference();

        $preference->external_reference = $cobranca->fatura;
        $preference->statement_descriptor = $this->setting->site_name;
        $preference->items = $this->getItem($cobranca);
        $preference->auto_return = 'approved';
        $preference->date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+5 day", strtotime(date('y-m-d H:i:s')))).'.000-03:00';
        //$preference->payer = $this->getPayer($cobranca->pessoa);

        $preference->back_urls = array(
            'success' => base_url() . 'linkpagamento/success/' . $cobranca->fatura . '?token=' . $this->session->userdata('cnpjempresa'),
            'pending' => base_url() . 'linkpagamento/pending/' . $cobranca->fatura . '?token=' . $this->session->userdata('cnpjempresa'),
            'failure' => base_url() . 'linkpagamento/failure/' . $cobranca->fatura . '?token=' . $this->session->userdata('cnpjempresa'),
        );

        if ($this->session->userdata('cnpjempresa') == 'katatur') {
            $preference->notification_url = 'https://sistema.katatur.com.br/infrastructure/MercadoPagoController/notification?empresa=' . $this->session->userdata('cnpjempresa');
        } else {
            $preference->notification_url = 'https://sistema.sagtur.com.br/infrastructure/MercadoPagoController/notification?empresa=' . $this->session->userdata('cnpjempresa');
        }

        $preference->payment_methods = array(
            "excluded_payment_types" => $this->getMetodosPagamentoExcluidos(),
            "installments" => 12
        );

        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();

        $uuid = $this->generateUUID();

        MercadoPago\SDK::config()->configure(['X-meli-session-id' => $cobranca->deviceId]);
        MercadoPago\SDK::addCustomTrackingParam('X-meli-session-id', $cobranca->deviceId);
        MercadoPago\SDK::config()->configure(['X-Idempotency-Key' => $uuid]);
        MercadoPago\SDK::addCustomTrackingParam('X-Idempotency-Key', $uuid);

        if ($preference->save()) {
            $dados = $this->gerarDadosFatura($cobranca, $preference, MercadoPagoService_model::LINK_PAGAMENTO);
            $dados['status_detail']   = 'Created';
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = '';
        } else {
            $dtoRetornoCobranca->sucesso = false;
            $dtoRetornoCobranca->erro = 'Não foi possivel gerar o link de pagamento. Entre em contato com o administrador do sistema.';
        }

        $this->LogMercadoPago_model->info(print_r($preference, true));

        $dtoRetornoCobranca->adicionarFatura($dados);

        return $dtoRetornoCobranca;

    }

    private function getPayment($cobranca, $tipo, $installments = 1) {

        $valorProduto   = number_format ($cobranca->valorVencimento, 2, '.', '' );
        $vencimento   = implode("-",array_reverse(explode("/",$cobranca->dataVencimento)));
        $pessoaCobranca = $cobranca->pessoa;
        $dataHoje       = date('y-m-d H:i:s');

        if ($tipo == MercadoPagoService_model::PIX) {
            //$date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+30 minutes", strtotime($dataHoje))).'.000-03:00';
            $date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+10 day", strtotime($dataHoje))).'.000-03:00';
        } else if ($tipo == MercadoPagoService_model::CARTAO_CREDITO || $tipo == MercadoPagoService_model::CARTAO_CREDITO_FATURA) {
            $dataHoje = date('y-m-d H:i:s');
            $date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+5 day", strtotime($dataHoje))).'.000-03:00';
        } else if ($tipo == MercadoPagoService_model::BOLETO) {
            $date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+0 day", strtotime($vencimento))).'.000-03:00';
        } else {
            $date_of_expiration = date('Y-m-d\TH:i:s', strtotime("+5 day", strtotime($dataHoje))).'.000-03:00';
        }

        $payment = new MercadoPago\Payment();
        $payment->transaction_amount    = $valorProduto;
        $payment->description           = $cobranca->instrucoes;
        $payment->date_of_expiration    = $date_of_expiration;
        $payment->payment_method_id     = $tipo;
        $payment->external_reference    = $cobranca->fatura;
        $payment->statement_descriptor  = $this->setting->site_name;
        $payment->installments          = $installments;

        if ($this->session->userdata('cnpjempresa') == 'katatur') {
            $payment->notification_url   = 'https://sistema.katatur.com.br/infrastructure/MercadoPagoController/notification?empresa='.$this->session->userdata('cnpjempresa');
        } else {
            $payment->notification_url   = 'https://sistema.sagtur.com.br/infrastructure/MercadoPagoController/notification?empresa='.$this->session->userdata('cnpjempresa');
        }

        $payment->payer = $this->getPayer($pessoaCobranca);

        $this->LogMercadoPago_model->debug(print_r($payment, true));

        return $payment;
    }

    private function gerarDadosFatura($cobranca, $transacao, $tipo) {

        $dados = array(
            'status' => Financeiro_model::STATUS_ABERTA,
            'integracao' => 'mercadopago',
            'tipo' => $cobranca->tipoCobranca,
            'fatura' => $cobranca->fatura,
            'code' => $transacao->id,
            'log' => $this->sma->tirarAcentos(print_r($transacao, true)),
        );

        if ($tipo == MercadoPagoService_model::BOLETO) {
            $dados['link'] = $transacao->transaction_details->external_resource_url;
            $dados['installmentLink'] = $transacao->transaction_details->external_resource_url;
            $dados['barcodeNumber'] = $transacao->barcode->content;
        }

        if ($tipo == MercadoPagoService_model::PIX) {
            $qrCode = $transacao->point_of_interaction->transaction_data->qr_code;
            $dados['qr_code_base64'] = $transacao->point_of_interaction->transaction_data->qr_code_base64;
            $dados['qr_code'] = $qrCode;
            $dados['checkoutUrl'] = base_url().'pix/pagamento/'.$qrCode.'?token='.$this->session->userdata('cnpjempresa');
        }

        if ($tipo == MercadoPagoService_model::LINK_PAGAMENTO) {
            $dados['checkoutUrl'] = $transacao->init_point;
        }

        if ($tipo == MercadoPagoService_model::CARTAO_CREDITO) {
            $code = $transacao->point_of_interaction->id;
            $dados['checkoutUrl'] = base_url().'cartaocredito/pagamento/'.$code.'?token='.$this->session->userdata('cnpjempresa');
        }

        if ($tipo == MercadoPagoService_model::CARTAO_CREDITO_FATURA) {
            $dados['checkoutUrl'] = base_url().'cartaocredito/pagamento/'.$cobranca->fatura.'?token='.$this->session->userdata('cnpjempresa');
        }

        return $dados;
    }

    private function getPayer($pessoaCobranca) {

        $cpfCnpj = str_replace("-", "", str_replace(".", "", str_replace("/", "", trim($pessoaCobranca->cpfCnpj))));
        $celular = str_replace("(", "", $pessoaCobranca->celular);
        $celular = str_replace(")", "", $celular);
        $celular = str_replace("-", "", $celular);
        $ddd = substr($celular, 0, 2);
        $celular = substr($celular, 2, strlen($celular));

        $nomeCompleto = $pessoaCobranca->nome;
        $nomeCompletoArray = explode(" ", $nomeCompleto);
        $primeiroNome = '';
        $segundoNome = '';

        if (count($nomeCompletoArray) > 0) {
            $primeiroNome = $nomeCompletoArray[0];
            for($i=1;$i<count($nomeCompletoArray);$i++){
                $segundoNome.= ' '.$nomeCompletoArray[$i];
            }
        }

        $dados = array(
            "first_name" => $primeiroNome,
            "last_name" => $segundoNome,
            'email' => $pessoaCobranca->email,
            //"phone" => array(
            //    'area_code' => $ddd,
            //    'number' => $celular
            //),
            "identification" => array(
                "type" => $this->isPessoaJuridica($pessoaCobranca) ? 'CNPJ' : 'CPF',
                "number" => $cpfCnpj
            ),
        );

        $payer = new MercadoPago\Payer();
        $payer->name = $primeiroNome;
        $payer->surname = $segundoNome;
        $payer->email = $pessoaCobranca->email;
        $payer->phone = array(
            "area_code" => $ddd,
            "number" => $celular
        );

        $payer->address = array(
            "street_name" => "falsa",
            "street_number" => 123,
            "zip_code" => "78134190"
        );

        return $dados;
    }

    private function getItem($cobranca) {

        $item = new MercadoPago\Item();
        $item->title = $cobranca->instrucoes;
        $item->unit_price = number_format ($cobranca->valorVencimento, 2, '.', '' );
        $item->quantity = 1;
        $item->category_id = 'travels';//TODO PARA O TURISMO.

        return array($item);
    }

    public function boleto($cobranca) {
        return $this->gerarCobranca($cobranca, MercadoPagoService_model::BOLETO);
    }

    public function pix($cobranca) {
        return $this->gerarCobranca($cobranca, MercadoPagoService_model::PIX);
    }

    public function cartao_credito($cobranca) {

        if ($this->isCartaoTransparente($cobranca)) {
            return $this->gerarCobranca($cobranca, MercadoPagoService_model::CARTAO_CREDITO);
        } else {
            $cobranca->id = $cobranca->fatura;
            return $this->cartao_credito_fatura($cobranca);
        }
    }

    private function isCartaoTransparente($cobranca) {

        if ($cobranca->senderHash) return true;

        return false;
    }

    public function cartao_credito_fatura($cobranca) {

        $dados = $this->gerarDadosFatura($cobranca, $cobranca, MercadoPagoService_model::CARTAO_CREDITO_FATURA);

        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        $dtoRetornoCobranca->adicionarFatura($dados);

        return $dtoRetornoCobranca;
    }

    public function buscarPagamento($code) {

        $this->configurarIntegracao();

        $payment =  MercadoPago\Payment::find_by_id($code);

        if ($payment->error != null) return [];

        $cobrancaSistema = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($payment->id);

        if (empty($cobrancaSistema)) {
            //TODO se nao encontrou pelo id tenta encontrar pela fatura para dar baixa (serve para gerados pelo link)
            $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($payment->external_reference);

            if (!empty($cobrancaFatura)) {

                $retorno        = $this->MercadoPagoException_model->verificarTransacao($payment);
                $sucesso        = $retorno['sucesso'];

                if ($sucesso) { //TODO SO PODE ATUALIZAR SER NAO DEU ERRO NO PAGAMENTO
                    $this->LinkpagamentoService_model->consultaPagamento('mercadopago', $code, $cobrancaFatura->fatura, $cobrancaFatura->id);
                }
            }
        }

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = $payment->id;
        $fatura->reference = $payment->external_reference;
        $fatura->valorVencimento = $payment->transaction_amount;
        $fatura->dtVencimento = $payment->date_of_expiration;
        $fatura->log = $this->sma->tirarAcentos(print_r($payment, true));
        $baixaCobranca->adicionarFatura($fatura);

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $pagamento->id = $payment->id;
        $pagamento->status = $payment->status;
        //$pagamento->status = 'approved';//TODO
        $pagamento->dataPagamento = $payment->date_approved;
        $pagamento->valorPago = $payment->transaction_amount;
        $pagamento->formaPagamento = $payment->payment_method_id;
        $taxas =  $this->getTaxasFee($payment);
        $taxaClienteParcelamento = $this->getTaxaCliente($payment);

        $pagamento->taxa = $taxas;

        $pagamento->observacao = MercadoPagoService_model::MENSAGEM_BAIXA_PAGMENTO.' '.date('d/m/Y H:i:s'). '
                                    <br/>Foi descontado uma taxa de R$'.$taxas.' da agência 
                                    <br/>Parcelados em '.$payment->installments.'X'.'
                                    <br/>O Cliente arcou com uma taxa de juros no parcelamento com a operadora de R$'.$taxaClienteParcelamento;
        $fatura->adicionarPagamento($pagamento);

        $this->LogMercadoPago_model->info(print_r($payment, true));

        return $baixaCobranca;
    }

    private function getTaxasFee($payment) {
        $taxas = 0;
        foreach ($payment->fee_details as $fee) {

            if ($fee->type == 'mercadopago_fee') {
                $taxas += $fee->amount;
            }
        }
        return $taxas;
    }

    private function getTaxaCliente($payment) {
        $taxas = 0;
        foreach ($payment->fee_details as $fee) {

            if ($fee->type == 'financing_fee') {
                $taxas += $fee->amount;
            }
        }
        return $taxas;
    }

    private function tratarRetornoDaTransacao($dados, $transacao, $tipo) {

        $retorno        = $this->MercadoPagoException_model->verificarTransacao($transacao);
        $sucesso        = $retorno['sucesso'];
        $error          = $retorno['error'];
        $status_detail  = $retorno['status_detail'];

        if (!$sucesso) {
            if ($tipo == MercadoPagoService_model::CARTAO_CREDITO) {
                $dados['checkoutUrl'] = base_url() . 'cartaocredito/pagamento/' . $dados['fatura']. '?token=' . $this->session->userdata('cnpjempresa');
                $dados['code'] =  $dados['fatura'];
                $dtoRetornoCobranca =  $this->getDadosCobrancaDTO(true, '');
            } else {
                $dtoRetornoCobranca =  $this->getDadosCobrancaDTO($sucesso, $error);
            }
        } else {
            $dtoRetornoCobranca =  $this->getDadosCobrancaDTO($sucesso, $error);
        }

        $dados['errorProcessamento'] = $error;
        $dados['status_detail']      = $status_detail;

        $dtoRetornoCobranca->adicionarFatura($dados);

        return $dtoRetornoCobranca;
    }

    private function getDadosCobrancaDTO($sucesso, $error) {
        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $dtoRetornoCobranca->sucesso = $sucesso;
        $dtoRetornoCobranca->erro = $error;

        return $dtoRetornoCobranca;
    }

    private function isBoleto($tipo) {
        if ($tipo == 'boleto') return true;
        return false;
    }

    private function isPix($tipo) {
        if ($tipo == 'pix') return true;
        return false;
    }

    private function isLinkPagamento($tipo) {
        if ($tipo == 'link_pagamento' || $tipo == 'cartao') return true;
        return false;
    }

    private function isCartaoDeCredito($tipo) {
        if ($tipo == 'carne_cartao_transparent_mercado_pago') return true;
        return false;
    }

    public function cancelarCobranca($code) {}

    public function estornarCobranca($code) {}

    private function isPessoaJuridica($pessoaCobranca) {
        return $pessoaCobranca->tipoPessoa == 'PJ';
    }

    function generateUUID() {
        return Uuid::uuid4()->toString();
    }

}