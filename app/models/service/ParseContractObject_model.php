<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParseContractObject_model extends CI_Model
{
    private const COBRANCA_EXTRA_ASSENTO = 'Cobrança Extra de Assento';

    public function __construct()
    {
        parent::__construct();

        //dto
        $this->load->model('dto/TAGContrato_model', 'TAGContrato_model');

        $this->load->model('model/Venda_model', 'Venda_model');
        $this->load->model('companies_model');

    }

    public function parse($appModel, $tipo = 'text') {

        $clienteModelSalvo          = $this->salvarCliente($appModel->cliente);

        $statusVenda                = Venda_model::STATUS_ORCAMENTO;
        $Settings                   = $this->site->get_setting();

        $condicaoPagamentoId        = $appModel->condicaoPagamento;
        $dataVencimento             = date('Y-m-d');
        $productsTodos              = array();

        $vendedor                   = $this->site->getCompanyByID($appModel->vendedorId);
        $user                       = $this->site->getUserByBiller($appModel->vendedorId);

        if ($appModel->valorSinal > 0) {
            $condicaoPagamentoObj   = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
            $condicaoPagamento      = $this->site->getProximaCondicaoPagamento($condicaoPagamentoObj->parcelas);//TODO BUSCA A PROXIMA CONDICAO DE PAGAMENTO
            $condicaoPagamentoId    = $condicaoPagamento->id;
            $tipoCobranca           = $this->site->getTipoCobrancaById($appModel->tipoCobrancaSinal);
        } else {
            $tipoCobranca           = $this->site->getTipoCobrancaById($appModel->tipoCobranca);
        }

        if ($tipoCobranca->faturarVenda == 1) {
            $statusVenda =  Venda_model::STATUS_FATURADA;
        }

        if ($appModel->produto->apenas_cotacao) {
            $statusVenda = Venda_model::STATUS_ORCAMENTO;
        }

        if ($appModel->listaEspera == '0') {//TODO LISTA DE ESPERA POIS O ESTOQUE E ZERO
            $statusVenda = Venda_model::STATUS_LISTA_EMPERA;
        }

        $produto                = $appModel->produto;
        $programacao            = $appModel->programacao;
        $dependentes            = $appModel->dependentes;
        $adicionais             = $appModel->adicionais;

        $observacao             = 'DESTINO '.strtoupper($produto->name. ' SAIDA '.date('d/m/Y', strtotime($programacao->dataSaida)));

        if (($tipoCobranca->tipo == 'carne_cartao_transparent' ||
                $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
                $tipoCobranca->tipo == 'link_pagamento'  ||
                $tipoCobranca->tipo == 'carne_cartao') && $appModel->valorSinal == 0) {//TODO CARTAO CHECKIN TRANSPARTENTE

            $condicaoPagamentoId    = 1;//TODO CONFIGURA A CONDICAO A VISTA
            $observacao .= ' CLIENTE: '. mb_strtoupper($clienteModelSalvo->getName(), mb_internal_encoding());
        }

        if ($appModel->observacaoAdicional) {
            $observacao .= ' OBS: '.$appModel->observacaoAdicional;
        }

        $acrescimo  = $appModel->getAcrescimo();
        $desconto   = 0;

        if ($acrescimo <= 0) {
            $acrescimo = 0;
            $desconto = $appModel->getAcrescimo()*-1;
        }

        $venda = array(
            'date' 					=> date('Y-m-d H:i'),
            'reference_no' 			=> $this->site->getReference('so'),

            'customer_id' 			=> $clienteModelSalvo->getId(),
            'customer' 				=> mb_strtoupper($clienteModelSalvo->getName(), mb_internal_encoding()),
            'vendedor'              => $vendedor->name,
            'biller' 				=> $vendedor->company != '-' ? $vendedor->company : $vendedor->name,
            'biller_id' 			=> $vendedor->id,
            'note' 					=> $observacao,

            'grand_total' 			=> $appModel->total + $appModel->getAcrescimo(),
            'total' 				=> $appModel->total + $appModel->getAcrescimo(),
            'valorVencimento'       => $appModel->total + $appModel->getAcrescimo(),

            'shipping' 				=> $acrescimo,
            'order_discount'    	=> $desconto,//TODO DESCONTA TAMBEM O CUPOM SE HOUVER
            'order_discount_id' 	=> $desconto,
            'total_discount' 		=> $desconto,

            'warehouse_id'			=> $Settings->default_warehouse,
            'total_items' 			=> $appModel->totalPassageiros,
            'sale_status' 			=> $statusVenda,
            'payment_status' 		=> 'due',//TODO PAGAR
            'payment_term' 			=> '',
            'posicao_assento'       => '',
            'product_discount' 		=> 0,
            'product_tax' 			=> 0,
            'order_tax_id' 			=> 0,
            'order_tax' 			=> 0,
            'total_tax' 			=> 0,
            'paid' 					=> 0,
            'vencimento'            => $dataVencimento,
            'condicaopagamentoId'   => $condicaoPagamentoId,
            'tipoCobrancaId'        => $tipoCobranca->id,
            'previsao_pagamento'    => $dataVencimento,
            'created_by' 			=> $user->id,
            'meioDivulgacao' 	    => ($appModel->meioDivulgacao != null ? $appModel->meioDivulgacao : $this->Settings->meio_divulgacao_default),

            'venda_link'            => 1,

            'cupom_id'              => $appModel->cupom_id,
            'desconto_cupom'        => $appModel->getDescontoCupom(),

            'valor_sinal'           => $appModel->valorSinal,
            'tipo_cobranca_sinal'   => $appModel->tipoCobrancaSinal,
        );

        if ($this->pagadorComoPassageiro($produto)) {

            $produto = $this->adicionarProduto($appModel, $appModel->cliente);
            $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$produto['product_id']] = $produto;//TODO INCLUINDO O PRODUTO PARA O CLIENTE PAGADOR

            if ($clienteModelSalvo->getUltimoValorExtraAssento() > 0) {

                $produto_cobranca_extra_assento = $this->adicionar_cobranca_extra_assento($appModel, $clienteModelSalvo, $produto['product_id'], $clienteModelSalvo->getUltimoValorExtraAssento(), $this->getCode());

                $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$this->getCode()] = $produto_cobranca_extra_assento;//TODO INCLUINDO O PRODUTO PARA O CLIENTE PAGADOR
            }

            //adicionar o pagador a lista de passageiros
            $clientes[] = array(
                'cliente' => $clienteModelSalvo,
            );
        }

        if ($this->colegarIngresso($produto)) {

            $ingressos = $appModel->ingressos;

            foreach ($ingressos as $ingresso) {

                $passageiro = $appModel->cliente;

                if ($ingresso->quantidade > 0) {

                    $faixa_etaria                   = $this->settings_model->getValorFaixa($ingresso->faixaId);

                    $passageiro->faixaId            = $faixa_etaria->id;
                    $passageiro->faixaName          = $faixa_etaria->name;
                    $passageiro->descontarVaga      = $faixa_etaria->descontarVaga;
                    $passageiro->faixaEtariaValor   = ($ingresso->valor * $ingresso->quantidade);

                    $produto                        = $this->adicionarProduto($appModel, $passageiro, 0, null, 0, $ingresso->quantidade);

                    $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$produto['product_id'].'faixa'.$faixa_etaria->id] = $produto;

                }
            }
        }

        foreach ($dependentes as $dependente) {

            //$dependente = new Cliente_model();

            $dependenteModelSalvo   = $this->salvarCliente($dependente);
            $produto                = $this->adicionarProduto($appModel, $dependenteModelSalvo);

            if ($dependente->getUltimoValorExtraAssento() > 0) {

                $produto_cobranca_extra_assento = $this->adicionar_cobranca_extra_assento($appModel, $dependenteModelSalvo, $produto['product_id'], $dependente->getUltimoValorExtraAssento(), $this->getCode());

                $productsTodos['c_'.$dependenteModelSalvo->getId().'p'.$this->getCode()] = $produto_cobranca_extra_assento;//TODO INCLUINDO O PRODUTO PARA OS DEPENDENTES

            }

            $productsTodos['c_'.$dependenteModelSalvo->getId().'p'.$produto['product_id']] = $produto;//TODO INCLUINDO O PRODUTO PARA OS DEPENDENTES

            $clientes[] = array(
                'cliente' => $dependenteModelSalvo,
            );
        }

        //$this->sma->print_arrays($adicionais);

        $produtoId = $produto['product_id'];

        if (!empty($adicionais)) {
            foreach ($adicionais as $adicional) {
                for ($i=0;$i<$adicional->quantidade;$i++) {
                    foreach ($clientes as $c) {
                        $adicionalClienteModel = $c['cliente'];

                        if ($this->disponibilidadeAdicinalAcabou($adicional)) {
                            continue;
                        }

                        if ($this->faixaEtariaDiferente($adicionalClienteModel, $adicional)) {
                            continue;
                        }

                        $adicional->quantidade  = $adicional->quantidade - 1;

                        $produtoDoCliente       = $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $produtoId];
                        $produtoAdicional       = $this->site->getProductByID($adicional->produto);

                        $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $produtoId] = $this->adiconarNovoServicoAdicional($produtoAdicional, $produtoDoCliente, $adicional->valor);

                        $appModel->produto = $produtoAdicional;
                        $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $appModel->produto->id] = $this->adicionarProduto($appModel, $adicionalClienteModel, 1, $appModel->produto->id, $adicional->valor);//TODO INCLUINDO O  PRODUTO ADICIONAL DO CLIENTE PAGADOR
                    }
                }
            }
        }

        return $this->ParseContractArray_model->parse($venda, $productsTodos, $tipo);
    }

    private function getCode(): int
    {
        return rand(pow(10, 10 - 1), pow(10, 10) - 1);
    }

    private function adiconarNovoServicoAdicional($produtoAdicional, $produtoDoCliente, $valor) {

        $servicosAdicionais = $this->getServicoAdicionalArray($produtoAdicional, $valor);
        $servicosJaAdicionados = $produtoDoCliente['servicosAdicionais'];
        $servicosJaAdicionados[] = $servicosAdicionais;

        $produtoDoCliente['servicosAdicionais'] = $servicosJaAdicionados;

        return $produtoDoCliente;
    }

    private function getServicoAdicionalArray($produtoAdicional, $valor): array
    {

        $categoria = $this->site->getCategoryByID($produtoAdicional->category_id);

        $servicosAdicionais = array(
            'nome_produto' => $produtoAdicional->name,
            'codigo_produto' => $produtoAdicional->code,
            'categoria_produto' => $categoria->name,
            'produtoId' => $produtoAdicional->id,
            'valor' => $valor,
            'quantidade' => 1,
        );

        return $servicosAdicionais;
    }

    private function faixaEtariaDiferente($clienteModel, $adicional): bool
    {
        return $clienteModel->getFaixaId() != $adicional->faixaId;
    }

    private function disponibilidadeAdicinalAcabou($adicional): bool
    {
        return $adicional->quantidade == 0;
    }

    private function adicionarProduto($appModel, $clienteModel, $adicional = 0, $produtoPai = null, $valorAdicional = 0, $quantity = 1): array
    {

        //$appModel = new AppCompra_model();
        //$clienteModel = new Cliente_model();

        $produto        = $appModel->produto;
        $programacao    = $appModel->programacao;

        $tiposTransporte = $this->site->getTransportes($produto->id);
        $localEmbarque = $this->site->getLocalEmbarqueByID($appModel->localEmbarque);
        $tipoHospeagem = $this->site->getTipoHospedagemID($appModel->tipoHospedagem);

        $tipoTransporte = null;
        $nmTipoTransporte = null;

        if ($appModel->tipoTransporte) {
            $tipoTransporteObj = $this->site->getTipoTransporteID($appModel->tipoTransporte);
            $tipoTransporte = $appModel->tipoTransporte;
            $nmTipoTransporte = $tipoTransporteObj->name;

        } else {
            foreach ($tiposTransporte as $tpTransporte) {
                $tipoTransporte = $tpTransporte->id;
                $nmTipoTransporte = $tpTransporte->text;
            }
        }

        $Settings = $this->site->get_setting();
        $categoria = $this->site->getCategoryByID($produto->category_id);

        $valorHospedagem = 0;//TODO NA HORA QUE CRIAR POR TIPO DE HOSPEDAGEM
        $valorPacote = $clienteModel->getFaixaEtariaValor() != null ? $clienteModel->getFaixaEtariaValor()  : 0.00 ;
        $descontarVaga = $clienteModel->getDescontarVaga();//TODO CONTROLA SE DESCONTA VAGAS.

        if ($adicional) {
            $valorPacote    = $valorAdicional;
            $descontarVaga  = 0;
            $clienteModel->setUltimoAssento('');
        }

        $products = array(

            'itinerarios' => [],
            'transfers' => [],
            'servicosAdicionais' => [],

            'product_id'  => $produto->id,
            'product_code' => $produto->code,
            'product_name' => $produto->name,
            'product_type' => $produto->type,
            'product_comissao' => 0,

            'faixaId'       => $clienteModel->getFaixaId(),
            'faixaNome'     => $clienteModel->getFaixaName(),
            'descontarVaga' =>  $descontarVaga,

            'programacaoId'  => $appModel->programacao->id,
            'product_category' => $produto->category_id,

            'nmTipoTransporte'  => $nmTipoTransporte,
            'tipoTransporte'  => $tipoTransporte,

            'localEmbarque' => $localEmbarque->name,
            'localEmbarqueId' => $localEmbarque->id,

            'tipoHospedagem'  =>$tipoHospeagem->name,
            'tipoHospedagemId'  =>$tipoHospeagem->id,
            'nmTipoHospedagem'  => $tipoHospeagem->name,

            'origem'  => '',
            'destino'  => '',
            'reserva'  => '',
            'nmFornecedor'  => '',

            'nmCategoria'  => $categoria->name,
            'tipoDestino'  => '',

            'dateCheckOut' => $programacao->dataSaida,
            'dateCheckin'  => $programacao->horaSaida,
            'horaCheckin'  => $programacao->dataRetorno,
            'horaCheckOut' => $programacao->horaRetorno,
            'dataEmissao'  => date('Y-m-d'),

            'valorHospedagem'  => $valorHospedagem,

            'valorPorFaixaEtaria'  => $valorPacote,

            'net_unit_price' => $valorPacote,
            'unit_price' => $valorPacote,
            'subtotal' => $valorPacote,
            'real_unit_price' => $valorPacote,

            'poltronaClient' => $clienteModel->getUltimoAssento(),//TODO assento selecionado pelo cliente
            'customerClient' => $clienteModel->getId(),
            'note_item' =>  $clienteModel->getUltimaNota(),//TODO NOTA DO ITEM
            'customerClientName' => mb_strtoupper($clienteModel->getName(), mb_internal_encoding()),

            'categoriaAcomodacao'  => '',
            'regimeAcomodacao'  => '',

            'receptivo'  => '',
            'nmReceptivo'  => '',

            'nmEmpresaManual'  => '',
            'telefoneEmpresa'  => '',
            'enderecoEmpresa'  => '',

            'apresentacaoPassagem'  => '',
            'dataApresentacao'  => null,
            'horaApresentacao'  => null,

            'tarifa' =>  0,
            'taxaAdicionalFornecedor' => 0,
            'descontoTarifa' => 0,
            'acrescimoTarifa' => 0,
            'adicionalBagagem' => 0,
            'adicionalReservaAssento' => 0,
            'valorPagoFornecedor' => 0,
            'formaPagamentoFornecedorCustomer' => null,
            'bandeiraPagoFornecedor' => null,
            'numeroParcelasPagoFornecedor' => 0,
            'descricaoServicos' => '',
            'observacaoServico' => '',

            'fornecedorId' => null,
            'tipoCobrancaFornecedorId' => null,
            'condicaoPagamentoFornecedorId' => null,

            'desconto_fornecedor' => 0,
            'comissao_rav_du' => 0,
            'incentivoFornecedor' => 0,
            'outrasTaxasFornecedor' => 0,
            'taxaCartaoFornecedor' => 0,
            'valorRecebimentoValor' => 0,

            'option_id' => NULL,

            'warehouse_id' => $Settings->default_warehouse,
            'quantity' => $quantity,
            'item_tax' => 0,
            'tax_rate_id' => NULL,
            'tax' => 0,
            'discount' => 0,
            'item_discount' => 0,
            'serial_no' => '',

            'adicional' => $adicional,
            'servicoPai'  => $produtoPai,

            'cupom_id' => $appModel->cupom_id,
        );

        return $products;
    }

    private function adicionar_cobranca_extra_assento($appModel, $clienteModel,  $produtoPai, $valorExtra = 0, $code): array
    {

        $programacao    = $appModel->programacao;

        $Settings = $this->site->get_setting();

        $products = array(

            'itinerarios' => [],
            'transfers' => [],
            'servicosAdicionais' => [],

            'product_id'    => $code,
            'product_code'  => $code,
            'product_name'  => ParseContractObject_model::COBRANCA_EXTRA_ASSENTO,
            'product_type'  => 'standard',

            'product_comissao'  => 0,
            'faixaId'           => $clienteModel->getFaixaId(),
            'faixaNome'     => $clienteModel->getFaixaName(),
            'descontarVaga' =>  0,
            'programacaoId'  => $appModel->programacao->id,
            'product_category' => 'ASSENTO EXTRA',
            'nmTipoTransporte'  => '',
            'tipoTransporte'  => null,
            'localEmbarque' => null,
            'tipoHospedagem'  => null,
            'nmTipoHospedagem'  => '',
            'origem'  => '',
            'destino'  => '',
            'reserva'  => '',
            'nmFornecedor'  => '',
            'nmCategoria'  => ParseContractObject_model::COBRANCA_EXTRA_ASSENTO,
            'tipoDestino'  => '',
            'dateCheckOut' => $programacao->dataSaida,
            'dateCheckin'  => $programacao->horaSaida,
            'horaCheckin'  => $programacao->dataRetorno,
            'horaCheckOut' => $programacao->horaRetorno,
            'dataEmissao'  => date('Y-m-d'),
            'valorHospedagem'  => 0,
            'valorPorFaixaEtaria'  => $valorExtra,
            'net_unit_price' => $valorExtra,
            'unit_price' => $valorExtra,
            'subtotal' => $valorExtra,
            'real_unit_price' => $valorExtra,
            'poltronaClient' => '',
            'note_item' =>  $clienteModel->getUltimaNota(),//TODO NOTA DO ITEM
            'customerClient' => $clienteModel->getId(),
            'customerClientName' => mb_strtoupper($clienteModel->getName(), mb_internal_encoding()),
            'categoriaAcomodacao'  => '',
            'regimeAcomodacao'  => '',
            'receptivo'  => '',
            'nmReceptivo'  => '',
            'nmEmpresaManual'  => '',
            'telefoneEmpresa'  => '',
            'enderecoEmpresa'  => '',
            'apresentacaoPassagem'  => '',
            'dataApresentacao'  => null,
            'horaApresentacao'  => null,
            'tarifa' =>  0,
            'taxaAdicionalFornecedor' => 0,
            'descontoTarifa' => 0,
            'acrescimoTarifa' => 0,
            'adicionalBagagem' => 0,
            'adicionalReservaAssento' => 0,
            'valorPagoFornecedor' => 0,
            'formaPagamentoFornecedorCustomer' => null,
            'bandeiraPagoFornecedor' => null,
            'numeroParcelasPagoFornecedor' => 0,
            'descricaoServicos' => '',
            'observacaoServico' => '',
            'fornecedorId' => null,
            'tipoCobrancaFornecedorId' => null,
            'condicaoPagamentoFornecedorId' => null,
            'desconto_fornecedor' => 0,
            'comissao_rav_du' => 0,
            'incentivoFornecedor' => 0,
            'outrasTaxasFornecedor' => 0,
            'taxaCartaoFornecedor' => 0,
            'valorRecebimentoValor' => 0,
            'option_id' => NULL,
            'warehouse_id' => $Settings->default_warehouse,
            'quantity' => 1,
            'item_tax' => 0,
            'tax_rate_id' => NULL,
            'tax' => 0,
            'discount' => 0,
            'item_discount' => 0,
            'serial_no' => '',
            'adicional' => 1,
            'servicoPai'  => $produtoPai,
            'cupom_id' => $appModel->cupom_id,
        );

        return $products;
    }

    private function salvarCliente($clienteModel) {

        //$clienteModel = new Cliente_model();

        $clienteEncontrou = $this->companies_model->getVerificaCustomeByCPF($clienteModel->getVatNo());

        if ($this->encontrouClienteCadastrado($clienteEncontrou, $clienteModel->getVatNo())) {
            $clienteAtualizar = $this->preencherCliente($clienteModel);
            $novoCliente['data_alteracao'] = date('Y-m-d');

            $this->companies_model->updateCompanyWith($clienteEncontrou->id, $clienteAtualizar, []);

            $clienteModel->setId($clienteEncontrou->id);

        } else {

            $novoCliente = $this->preencherCliente($clienteModel);

            $novoCliente['group_id'] = 3;
            $novoCliente['group_name'] = 'customer';
            $novoCliente['customer_group_id'] = 1;
            $novoCliente['customer_group_name'] = 'PASSAGEIROS';
            $novoCliente['tipoPessoa'] = 'PF';
            $novoCliente['bloqueado'] = 0;
            $novoCliente['created_by'] = $this->session->userdata('user_id');//TODO AJUSTAR vir vendedor
            $novoCliente['data_cadastro'] = date('Y-m-d');

            $cid = $this->companies_model->addCompanyWith($novoCliente, []);

            $clienteModel->setId($cid);
        }

        return $clienteModel;
    }

    private function preencherCliente($clienteModel): array
    {

        //$clienteModel = new Cliente_model();

        $name = $clienteModel->getName();
        $company = $clienteModel->getCompany();
        $nomeResponsavel = $clienteModel->getNomeResponsavel() != null ? $clienteModel->getNomeResponsavel() : '';

        if ($company == null) {
            $company = $name;
        }

        $name = str_replace(['&', "'"], '', $name);
        $company = str_replace(['&', "'"], '', $company);

        $data = array(

            'name'                  => mb_strtoupper($name, mb_internal_encoding()),
            'company'               => mb_strtoupper($company, mb_internal_encoding()),
            'nome_responsavel'      => mb_strtoupper($nomeResponsavel, mb_internal_encoding()),

            'email'                 => $clienteModel->getEmail() != null ? $clienteModel->getEmail() : '',
            'phone'                 => $clienteModel->getPhone() != null ?  $clienteModel->getPhone() : '',

            'data_aniversario'      => $clienteModel->getDataAniversario(),
            'tipo_documento'        => $clienteModel->getTipoDocumento(),
            'vat_no'                => $clienteModel->getVatNo(),
            'sexo'                  => $clienteModel->getSexo(),
            'telefone_emergencia'   => $clienteModel->getTelefoneEmergencia(),
            'alergia_medicamento'   => $clienteModel->getAlergiaMedicamento(),

            'postal_code'           => $clienteModel->getPostalCode(),
            'address'               => $clienteModel->getAddress() != null ? $clienteModel->getAddress() : '',
            'bairro'                => $clienteModel->getBairro(),
            'numero'                => $clienteModel->getNumero(),
            'complemento'           => $clienteModel->getComplemento(),
            'city'                  => $clienteModel->getCity() != null ? $clienteModel->getCity()  : '',
            'state'                 => $clienteModel->getState(),
            //'country'               => $this->input->post('country'),

            'cf1'                   => $clienteModel->getCf1(),//todo rg
            'cf3'                   => $clienteModel->getCf3(),//todo orgao emissor
            'cf5'                   => $clienteModel->getCf5(),//todo whatsapp

            'social_name'           => $clienteModel->getSocialName(),
            'profession'            => $clienteModel->getProfession(),
            'doenca_informar'       => $clienteModel->getDoencaInformar(),

        );

        return $data;
    }

    private function encontrouClienteCadastrado($customer, $cpf): bool
    {
        return count($customer) > 0 && $this->sma->isCPFObrigaNovoCadastroEhValidaCPFCNJ($cpf);
    }

    private function colegarIngresso($product): bool
    {
        return $product->isApenasColetarPagador == 1;
    }

    private function pagadorComoPassageiro($product): bool
    {
        return $product->isApenasColetarPagador == 0;
    }
}