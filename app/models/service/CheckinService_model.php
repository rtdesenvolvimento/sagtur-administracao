<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CheckinService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();
    }

    public function checkin($checkinModel)
    {
        try {
            $this->__iniciaTransacao();
            $checkin_id = false;

            $checked_item = $this->CheckinRepository_model->checked($checkinModel->sale_id, $checkinModel->customer_id);

            if (!$checked_item) {
                $data = array(
                    'checkin_location_id'   => $checkinModel->checkin_location_id,
                    'sale_id'               => $checkinModel->sale_id,
                    'customer_id'           => $checkinModel->customer_id,
                    'programacao_id'        => $checkinModel->programacao_id,
                    'note'                  => $checkinModel->note,
                    'type'                  => $checkinModel->type,
                    'date'                  => date('Y-m-d H:i:s'),
                    'created_at'            => date('Y-m-d H:i:s'),
                    'created_by'            => $this->session->userdata('user_id'),
                );
                if ($this->db->insert('checkin', $data)) {
                    $checkin_id = $this->db->insert_id();
                }
            } else {
                $checkin_id = $checked_item->id;
            }

            $this->__confirmaTransacao();

            return $checkin_id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function checkin_delete($checkinModel)
    {
        if ($this->db->delete('checkin', array('sale_id' => $checkinModel->sale_id, 'customer_id' => $checkinModel->customer_id))) {
            return TRUE;
        }
        return FALSE;
    }
}