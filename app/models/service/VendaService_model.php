<?php defined('BASEPATH') OR exit('No direct script access allowed');

class VendaService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        //service
        $this->load->model('service/RoomListService_model', 'RoomListService_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');
        $this->load->model('service/FaturaService_model', 'FaturaService_model');
        $this->load->model('service/RatingsService_model', 'RatingsService_model');
        $this->load->model('service/CommissionsService_model', 'CommissionsService_model');

        $this->load->model('Refunds_model');
        $this->load->model('Gifts_model');
        $this->load->model('financeiro_model');
    }

    public function cancelar($motivoCancelamento) {
        try {

            //$motivoCancelamento = new MotivoCancelamentoVenda_model();

            $this->__iniciaTransacao();

            $motivo = $this->getMotivoCancelamento($motivoCancelamento);

            $this->CommissionsService_model->cancelarComissao($motivoCancelamento->sale_id, $motivo);

            $id     = $motivoCancelamento->sale_id;
            $inv    = $this->__getById('sales', $id);

            if ($inv->sale_status == 'cancel') {
                throw new Exception('A Venda já encontra-se <b>CANCELADA</b> e não pode ser cancelada novamente!');
            }

            $faturasContaReceber    = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
            $faturasContaPagar      = $this->financeiro_model->getParcelasFaturaByContaPagar($id);

            if ($motivoCancelamento->gerar_credito) {

                $this->gerar_credito_cliente($inv, $motivoCancelamento);

                foreach ($faturasContaReceber as $faturasCredito) {
                    $this->FinanceiroService_model->cancelar_com_lancamento_credito($faturasCredito->id, NULL);
                }
            }

            if ($motivoCancelamento->gerar_reembolso) {

                $this->gerar_reembolso($inv, $motivoCancelamento);

                foreach ($faturasContaReceber as $faturasCredito) {
                    $this->FinanceiroService_model->cancelar_com_lancamento_credito($faturasCredito->id, NULL);
                }
            }

            if ($this->nao_gerar_credito_ou_reembolso($motivoCancelamento)) {

                foreach ($faturasContaReceber as $faturasCredito) {
                    $this->FinanceiroService_model->cancelar_com_multa($faturasCredito->id, NULL);
                }

                $this->__update('conta_receber', array('receita' => $this->Settings->tipo_cobranca_reembolso_cliente_id), 'sale', $id);
            }

            foreach ($faturasContaPagar as $faturasDebito) {
                $this->FinanceiroService_model->cancelar($faturasDebito->id, NULL);
            }

            $this->RoomListService_model->excluirHospedeByVenda($id);

            $this->__update('sales', array('sale_status'=> 'cancel', 'payment_status' => 'cancel', 'motivo_cancelamento_id' => $motivoCancelamento->motivo_cancelamento_id, 'motivo_cancelamento' => $motivo, 'credited_sale' => $motivoCancelamento->gerar_credito), 'id', $id);

            $this->SaleEventService_model->cancel($id, $motivo);

            $this->cancelar_contrato($id);

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function cancelar_contrato($saleID)
    {
        try {
            $plugsignSetting = $this->site->getPlugsignSettings();
            if ($plugsignSetting->active && $plugsignSetting->token && $this->Settings->cancel_contract_by_sale) {
                $this->load->model('service/DocumentService_model', 'DocumentService_model');
                $this->DocumentService_model->cancelAllContracts($saleID);
            }
        } catch (Exception $exception) {

        }
    }

    private function gerar_credito_cliente($inv, $motivoCancelamento): void
    {
        if ($motivoCancelamento->gerar_credito && $motivoCancelamento->value == 0) {
            throw new Exception('Não é permitido gerar um crédito com valor zerado');
        }

        $this->verificar_valor_credito_reembolso($inv, $motivoCancelamento);

        $gifit = array(
            'card_no' => $motivoCancelamento->card_no,
            'value' => $motivoCancelamento->value,
            'customer_id' => $motivoCancelamento->customer_id,
            'customer' => $motivoCancelamento->customer,
            'balance' => $motivoCancelamento->value,
            'note' => $motivoCancelamento->note_credito,
            'expiry' => $motivoCancelamento->expiry,
            'created_by' => $this->session->userdata('user_id'),
            'origin_sale_id' => $inv->id,
        );

        $this->Gifts_model->addGiftCard($gifit);
    }

    /**
     * @throws Exception
     */
    private function gerar_reembolso($inv, $motivoCancelamento): void
    {

        if ($motivoCancelamento->gerar_reembolso && $motivoCancelamento->value_refund == 0) {
            throw new Exception('Não é permitido gerar um reembolso com valor zerado');
        }

        $this->verificar_valor_credito_reembolso($inv, $motivoCancelamento);

        $gifit = array(
            'card_no'       => $motivoCancelamento->card_no_refund,
            'value'         => $motivoCancelamento->value_refund,
            'customer_id'   => $motivoCancelamento->customer_id,
            'customer'      => $motivoCancelamento->customer,
            'balance'       => $motivoCancelamento->value_refund,
            'note'          => $motivoCancelamento->note_refund,
            'expiry'        => $motivoCancelamento->expiry_refund,
            'created_by'    => $this->session->userdata('user_id'),
            'origin_sale_id'=> $inv->id,
            'type'          => 'refund',
        );

        $this->Refunds_model->addRefundCard($gifit);
    }

    public function verificar_valor_credito_reembolso($inv, $motivoCancelamento)
    {

        $paid   = $inv->paid;
        $credit = $motivoCancelamento->value;
        $refund = $motivoCancelamento->value_refund;
        $total  = 0;

        if ($motivoCancelamento->gerar_credito){
            $total = $credit;
        }

        if ($motivoCancelamento->gerar_reembolso) {
            $total = $total + $refund;
        }

        if ($paid < $total) {
            throw new Exception('Não é permitido lançar um reembolso maior que o valo total pago pelo cliente na venda.');
        }
    }

    private function gerar_credito_ou_reembolso($motivoCancelamento): bool
    {
        return $motivoCancelamento->gerar_credito || $motivoCancelamento->gerar_reembolso;
    }

    private function nao_gerar_credito_ou_reembolso($motivoCancelamento): bool
    {
        return !$this->gerar_credito_ou_reembolso($motivoCancelamento);
    }

    private function estornar_pagamentos($id, $motivoEstorno = null)
    {
        $payments = $this->sales_model->getInvoicePayments($id);

        foreach ($payments as $payment) {

            if ($payment->status == 'ESTORNO') {
                continue;
            }

            $this->FaturaService_model->estornar($payment->id, $motivoEstorno);
        }
    }

    public function archived($id): bool
    {
        try {
            $this->__iniciaTransacao();

            $venda = $this->__getById('sales', $id);

            $status = $venda->archived;

            if ($status) {
                throw new Exception('A Venda já encontra-se Arquivada');
            }

            $this->__update('sales', array('archived'=> true), 'id', $id);

            $this->SaleEventService_model->archived($id);

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function ratings($avaliar_dias = TRUE, $sale_id = NULL)
    {
        try {
            $this->__iniciaTransacao();

            $ratingsArray = $this->RatingsService_model->saveAll($this->Settings->avaliar, $avaliar_dias, $sale_id);

            $this->__confirmaTransacao();

            return $ratingsArray;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function unarchive($id): bool
    {
        try {
            $this->__iniciaTransacao();

            $venda = $this->__getById('sales', $id);

            $status = $venda->archived;

            if (!$status) {
                throw new Exception('A Venda não encontra-se Arquivada!');
            }

            $this->__update('sales', array('archived'=> false), 'id', $id);

            $this->SaleEventService_model->unarchive($id);

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }
    private function getMotivoCancelamento($motivoCancelamento) {
        $motivo_cancelamento_id = $motivoCancelamento->motivo_cancelamento_id;
        $note_obs = $motivoCancelamento->note;
        $motivo = '';

        if ($motivo_cancelamento_id) {
            $motivo_cancelamento =  $this->__getById('motivo_cancelamento', $motivo_cancelamento_id);
            $motivo = $motivo_cancelamento->name;
        }

        if ($note_obs) {
            $motivo .= ' OBS: '.$note_obs;
        }

        return $motivo;
    }
}