<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use vinicinbgs\Autentique\Documents;
use vinicinbgs\Autentique\Folders;
use vinicinbgs\Autentique\Utils\Api;

class AutentiqueService_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('model/DocumentData_model');
        $this->load->model('model/DocumentSignature_model');

        $this->upload_path = 'assets'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR.'documents'.DIRECTORY_SEPARATOR.$this->session->userdata('cnpjempresa').DIRECTORY_SEPARATOR;
    }

    public function createDocument($document) {


        $AUTENTIQUE_TOKEN="2c30d37a36dc09b8f64efafbe5e1abfae257d20c684b0f028efd30e5da7f5345";

        $api = new Api('https://api.autentique.com.br/v2/graphql', 100);

        $documents = new Documents($AUTENTIQUE_TOKEN);
        $documents->setApi($api); // use only if you want to change the default timeout 60 seconds
        $documents->setSandbox("true"); // string. "true"|"false"

        $attributes = [
            'document' => [
                'name' => $document->name.'-id-'.$document->id,
                'message' => 'Mensagem customizada enviada para os emails dos signatários',
                'reminder' => 'DAILY',
                'footer' => 'BOTTOM'
            ],
            'signers' => [
                [
                    'email' => 'andrej.velho@gmail.com',
                    'name' => 'André Velho',
                    'configs' => array(
                        'cpf' => '06624498943'
                    ),
                    'action' => 'SIGN',
                    'positions' => [
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '80', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '1', // Página da ASSINATURA
                        ],
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '50', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '2', // Página da ASSINATURA
                        ],
                    ],
                ],
                [
                    //'email' => 'andre@resultatec.com.br',
                    'name' => 'Neuci Reckziegel',
                    'configs' => array(
                        'cpf' => '10639016006'
                    ),
                    'action' => 'SIGN',
                    'positions' => [
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '80', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '1', // Página da ASSINATURA
                        ],
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '50', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '2', // Página da ASSINATURA
                        ],
                    ],
                ],
            ],
            'file' => FCPATH.DIRECTORY_SEPARATOR.$this->upload_path.$document->file_name,
        ];

        $documentCreated = $documents->create($attributes);

        $documentModel = new DocumentData_model();
        $documentModel->id = $documentCreated['data']['createDocument']['id'];
        $signatures = $documentCreated['data']['createDocument']['signatures'];

        if ($this->nao_conte_erros($documentCreated)) {
            $contador = 1;
            foreach ($signatures as $signature) {
                $signatureModel = new Documentsignature_model();
                $signatureModel->id = $signature['public_id'];
                $signatureModel->name = $signature['name'];
                $signatureModel->email = $signature['email'];
                $signatureModel->action = $signature['action']['name'];
                $signatureModel->link = $signature['link']['short_link'];
                $documentModel->addSignature($signatureModel);
            }

            $this->signed($documentModel->id);

            return $documentModel;
        }

        return false;
    }

    private function nao_conte_erros($documentCreated) {

        if (array_key_exists('errors', $documentCreated)) {
            $errors = $documentCreated['errors'];
            $mensagemErro = '';
            foreach ($errors  as $erro) {
                $mensagemErro.= $erro['message'].'<br/>';
            }
            throw new Exception($mensagemErro);
        } else if ($documentCreated['message'] == 'unauthorized') {
            throw new Exception('Token inválido');
        }

        return true;
    }

    public function signed($documentId) {

        $AUTENTIQUE_TOKEN="2c30d37a36dc09b8f64efafbe5e1abfae257d20c684b0f028efd30e5da7f5345";

        $api = new Api('https://api.autentique.com.br/v2/graphql', 100);

        $documents = new Documents($AUTENTIQUE_TOKEN);
        $documents->setApi($api); // use only if you want to change the default timeout 60 seconds
        $documents->setSandbox("true"); // string. "true"|"false"

        $documentSign = $documents->signById($documentId);
    }

}