<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PlugsignService_model extends CI_Model
{
    const COMPANY_DEFAULT_PASSWORD = '5p74Kd8q_1Xr';

    const TOKEN_SAGTUR = 'mk6FovkvoFbwZ7RI9645vyNmNWHBgfRG30PzOm9tPNO4zmIKjc0JXRjWo5KDtsQLtfBfJU560l64NKkVAvqB1htDepvA4DFXVfP485olXOaif31wNMGyikH2jLy8e6o2r8ul0bLRrrjqrQU8eJmh1QyaEvgVtPmZtF7b88qM8LIgzrNjAXHsrzdjdlbfTSTVPtQtMtr7iO4vTojy1l2eoclh3Fg3w7mAU9YWIpujVa0I8CR7Xct2DtDirDmvHbsOsg2QYlSjdoqiFM8xZ0Q9x0onPbJ59gWb3d2saOszZhNS6Hkw1iLoVcZTtRnCVIp4R0nIoZAuVUm8ALmoY';

    public function __construct()
    {
        parent::__construct();

        //model to save
        $this->load->model('model/DocumentData_model');
        $this->load->model('model/DocumentSignature_model');

        //repository to save
        $this->load->model('repository/PlugsignRepository_model');

        $plugsignSettings = $this->site->getPlugsignSettings();

        if ($plugsignSettings->active && $plugsignSettings->token) {
            $this->load->library('plugsign');
            $this->plugsign->apiKey = $plugsignSettings->token;
        }

        $this->upload_path = 'assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . $this->session->userdata('cnpjempresa') . DIRECTORY_SEPARATOR;
    }

    public function createFolder($data_folder)
    {
        return $this->plugsign->createFolder($data_folder);
    }

    public function editFolder($data_folder, $id)
    {
        return $this->plugsign->editFolder($data_folder, $id);
    }

    public function deleteFolder($id)
    {
        return $this->plugsign->deleteFolder($id);
    }

    public function createDocument($document): DocumentData_model
    {
       // $document = new Document_model();
        $file = $this->upload_path . $document->file_name;

        $folder = new Folder_model($document->folder_id);

        $multipart = [
            'accessibility' => 'Everyone',
            'name'      => $document->name,
            'optimizer' => TRUE,
            'file'      => fopen($file, 'r'),
            'editablevalidate' => 3,//Página final de validação e QR Code.
        ];

        if ($folder->public_id){
            $multipart['folder'] = $folder->public_id;
        }

        try {

            $documentPlugsignCreated = $this->plugsign->createDocument($multipart);

        } catch (Exception $e) {//se nao conseguir salvar com o nome do cliente

            $multipart = [
                'accessibility' => 'Everyone',
                'name'      => $document->reference_no,
                'optimizer' => TRUE,
                'file'      => fopen($file, 'r'),
            ];

            if ($folder->public_id){
                $multipart['folder'] = $folder->public_id;
            }

            $documentPlugsignCreated = $this->plugsign->createDocument($multipart);
        }

        $documentModel = new DocumentData_model();
        $documentModel->id = $documentPlugsignCreated->id;
        $documentModel->document_key = $documentPlugsignCreated->document_key;
        $documentModel->download = 'https://app.plugsign.com.br/view/'.$documentPlugsignCreated->document_key;
        $documentModel->extension = $documentPlugsignCreated->extension;
        $documentModel->size = $documentPlugsignCreated->size;

        return $documentModel;
    }

    public function editDocument($document)
    {
        return $this->plugsign->editDocument(array('name' => $document->name), $document->document_key);
    }

    public function createCustomer($signatory)
    {

        $signatory  = new Signatures_model($signatory->id);
        $customer   = $this->site->getCompanyByID($signatory->customer_id);

        if ($this->isClienteComCPFEhEmail($signatory, $customer)) {

            $encontrouCustomerDocument = $this->PlugsignRepository_model->searchClientDocument($customer->id);

            if (!$encontrouCustomerDocument) {

                $customerPlugsing = $this->plugsign->searchCustomer($customer->email);
                $customerDocument = $this->preencherCustomerDocument($customer);

                if ($customerPlugsing->id) {
                    $this->PlugsignRepository_model->updateDocumentCustomer($customerDocument, $customerPlugsing);
                } else {
                    try {
                        $customerPlugsingCreated = $this->plugsign->createCustomer($customerDocument);
                        $this->PlugsignRepository_model->saveDocumentCustomer($customerDocument, $customerPlugsingCreated);
                    } catch (Exception $e) {
                        if (strpos($e->getMessage(), 'O email já foi escolhido') === false) {
                            throw new Exception($e->getMessage());
                        }
                    }
                }
            } else {
                try {

                    $customerPlugsing = $this->plugsign->searchCustomer($customer->email);
                    $customerDocument = $this->preencherCustomerDocument($customer);

                    if ($customerPlugsing->id) {
                        $this->plugsign->updateCustomer($customerDocument, $customer->email);
                        $this->PlugsignRepository_model->updateDocumentCustomer($customerDocument, $customerPlugsing);
                    }
                } catch (Exception $e) {}
            }
        }

    }

    private function isClienteComCPFEhEmail($signatory, $customer): bool
    {
        return $signatory->customer_id && ($customer->email && $customer->cf5);
    }

    private function preencherCustomerDocument($customer): DocumentCustomer_model
    {
        $customerDocument = new DocumentCustomer_model();

        $cpf =  $customer->vat_no; // Remove todos os caracteres que não são números
        $email = $customer->email;
        $phone = $customer->cf5;

        $customerDocument->customer_id = $customer->id;
        $customerDocument->email = $email;//TODO obrigatorio
        $customerDocument->phone = $phone;//todo obrigatorio pega o whatsapp

        $fullName = $customer->name;
        $nameParts = explode(' ', $fullName);

        $nome = $nameParts[0];
        $sobrenome = isset($nameParts[1]) ? implode(' ', array_slice($nameParts, 1)) : 'TURISMO';

        $customerDocument->name = $nome;
        $customerDocument->last_name =  $sobrenome.' DOC '.$cpf;

        $customerDocument->cpf = '';
        $customerDocument->address = $customer->address. ''. $customer->city. ' - '. $customer->state. ' - '. $customer->postal_code;
        $customerDocument->recive_email = 0;//Se o usuário vai receber e-mails do sistema, 0 = Não recebe nada, 1 = Recebe tudo, 2 = Recebe somente solicitaçõe
        $customerDocument->recive_whatsapp = 1;//Se o usuário vai receber mensagens de WhatsApp do sistema, 0 = Não recebe nada, 1 = Recebe tudo, 2 = Recebe somente solicitações. Example: 1

        $customerDocument->can_login = 1;
        $customerDocument->password =  DocumentCustomer_model::DEFAULT_PASSWORD;

        $customerDocument->created_by  = 1;
        $customerDocument->created_at  = date('Y-m-d H:i:s');

        return $customerDocument;
    }

    public function getUser($document)
    {
        if ($this->session->userdata('user_id')) {
            return $this->site->getUser($this->session->userdata('user_id'));
        } elseif ($document->sale_id) {
            return $this->site->getUserByBiller($this->site->getSaleByID($document->sale_id)->biller_id);
        }

        return null;
    }


    /**
     * @throws Exception
     */
    public function toSign($document, $signatories): DocumentData_model {

        //$document = new Document_model();

        $contactsSing   = [];
        $fieldsSing     = [];

        if (!$document->public_id){
            throw new Exception('Não é possível soliciatar assinatura sem o documento ter sido criado.');
        }

        if ($signatories == null){
            throw new Exception('Não é possível soliciatar assinatura sem signatários.');
        }

        // Percorrendo o array de $customers para adicionar seus e-mails
        foreach ($signatories as $signatory) {

            $this->createCustomer($signatory);

            if (!empty($signatory->phone)) {//prioriza o telefone
                $contactsSing[] = preg_replace('/\D/', '', $signatory->phone);
            } else if (!empty($signatory->email)) {
                $contactsSing[] = $signatory->email;
            } else {
                throw new Exception('Não é possível soliciatar assinatura sem o telefone ou e-mail do signatário.');
            }

            $fieldsSing[] = [
                array(
                    'page' => 1,
                    'type' => 'image',
                    'width' => 200,
                    'height' => 75,
                    'xPos' => -999,
                    'yPos' => -999,
                )
            ];
        }

        $documentToSign = array(
            'document_key' => $document->document_key,

            'email' => $contactsSing,//email ou Whatsapp dos signatarios
            'fields' => $fieldsSing,//campos de assinatura
            'image_selfie' => $document->solicitar_selfie,//solicitar selfie
            'image_doc   ' => $document->solicitar_documento,//solicitar documento
            'signature_type' => $document->signature_type,//Tipo da assinatura (All, Draw, Text, Upload)
            'width_page' => 800,//configuracaoes da assinatura na pagina
        );

        $documentToSign['chain'] = $document->enviar_sequencial;//1 - sequencial, 0 - paralelo

        if ($document->monitorar_validade) {
            $documentToSign['due_months'] = $document->validade_meses;
        } else {
            $documentToSign['due_months'] = 60;
        }

        if ($document->signatories_note) {
            $documentToSign['message'] = $document->signatories_note;
        } else {
            $documentToSign['message'] = 'Envio de documento para assinatura';
        }

        if ($document->auto_destruir_solicitacao) {
            if ($document->data_vencimento_contrato) {
                $documentToSign['expire_date'] = date('Y-m-d H:i:s', strtotime($document->data_vencimento_contrato));
            }
        }

        if ($document->is_observadores) {
            $observersArray = explode(',', $document->observadores);
            foreach ($observersArray as $observer) {
                $documentToSign['observer'][] = $observer;
            }
        }

        $signatures = $this->plugsign->toSign($documentToSign);

        $documentModel = new DocumentData_model();

        foreach ($signatures->signatures as $signature) {
            $signatureModel = new DocumentSignature_model();
            $signatureModel->id = $signature->signingKey;
            $signatureModel->email = $signature->email;
            $signatureModel->telefone = $signature->email;
            $documentModel->addSignature($signatureModel);
        }

        return $documentModel;
    }

    /**
     * @throws Exception
     */
    public function sign($document, $customerDocument = null)
    {
        if ($document->document_key) {

            $sign = array(
                'document_key' => $document->document_key ,
                'page' => 1,
                'xPos' => 20,
                'yPos' => 20,
                'rubric' => false,
            );

            if ($customerDocument){
                $sign['user_id'] = $customerDocument->public_id;//customer na integracao
            }

            return $this->plugsign->sign($sign);
        }

        return null;
    }

    /**
     * @throws Exception
     */
    public function sign_key($document, $signatory = null)
    {
        if ($document->document_key && $signatory->public_id) {
            return $this->plugsign->sign_key(array('signing_key' => $signatory->public_id));
        }

        return null;
    }

    public function createCompany($warehouse): bool
    {
        $this->plugsign->apiKey = PlugsignService_model::TOKEN_SAGTUR;//para criar a empresa é necessário o token da sagtur

        if (!$warehouse->map) {
            throw new Exception('Não é possível criar a empresa sem a logo da empresa.');
        }

        $multipart = array(
            'company_name' => $warehouse->name,//nome da empresa
            'name' => $warehouse->first_name,//nome do representante
            'last_name' => $warehouse->last_name,//sobrenome do representante
            'email' => $warehouse->email,
            'phone' => $warehouse->phone,
            'password' => PlugsignService_model::COMPANY_DEFAULT_PASSWORD,
            'cnpj' => $warehouse->cnpj,
            'cpf' => $warehouse->cpf,
            'address' => $warehouse->address,
            'recive_whatsapp' => $warehouse->recive_whatsapp,
            'recive_email' => $warehouse->recive_email,
            'can_login' =>  $warehouse->can_login,
            'company_brand' => fopen('assets/uploads/' . $warehouse->map, 'r'),
            'editablevalidate' => 3,//Página final de validação e QR Code.

            'plugzapi_instance' => '3D7ACA0042A4A0E77763F69CB7A0D002',
            'plugzapi_token' => 'D2A7B1790022B7F512BC5062',
            'plugzapi_account_token' => 'Fe908c5f7c5d243ba843f275b8d99af45S',

        );

        $company = $this->plugsign->createCompany($multipart);

        $this->__edit('warehouses', array('public_id' => $company->id, 'password' => PlugsignService_model::COMPANY_DEFAULT_PASSWORD), 'id', $warehouse->id);
        $this->__edit('plugsign', array('active' => 1, 'token' => $company->token), 'id', 1);

        return true;
    }

    public function updateCompany($warehouse): bool
    {
        $this->plugsign->apiKey = PlugsignService_model::TOKEN_SAGTUR;//para criar a empresa é necessário o token da sagtur

        if (!$warehouse->map) {
            throw new Exception('Não é possível criar a empresa sem a logo da empresa.');
        }

        $multipart = array(
            'company_name' => $warehouse->name,//nome da empresa
            'name' => $warehouse->first_name,//nome do representante
            'last_name' => $warehouse->last_name,//sobrenome do representante
            'email' => $warehouse->email,
            'phone' => $warehouse->phone,
            'cnpj' => $warehouse->cnpj,
            'cpf' => $warehouse->cpf,
            'address' => $warehouse->address,
            'recive_whatsapp' => $warehouse->recive_whatsapp,
            'recive_email' => $warehouse->recive_email,
            'can_login' =>  $warehouse->can_login,
            'company_brand' => fopen('assets/uploads/' . $warehouse->map, 'r'),
            'editablevalidate' => 3,//Página final de validação e QR Code.


            'plugzapi_instance' => '3D7ACA0042A4A0E77763F69CB7A0D002',
            'plugzapi_token' => 'D2A7B1790022B7F512BC5062',
            'plugzapi_account_token' => 'Fe908c5f7c5d243ba843f275b8d99af45S',
        );

        $this->plugsign->updateCompany($multipart, $warehouse->public_id);

        return true;
    }

    public function getTotalDocs($warehouseID)
    {
        //$warehouse = $this->site->getWarehouseByID($warehouseID);

        //return $this->plugsign->getTotalDocs($warehouse->public_id);

        return 0;
    }

    public function protectDocument($document, $pass_user, $pass_owner): bool
    {
        if ($document->document_key){
            $this->plugsign->protectDocument(array('document_key' => $document->document_key, 'pass_user' => $pass_user, 'pass_owner' => $pass_owner));
            return true;
        }
        return false;
    }

    public function downloadDocument($document)
    {
        if ($document->document_key){
            $this->plugsign->downloadDocument($document->document_key);
        }
        return NULL;
    }

    public function resendWhatsapp($signature)
    {
        if ($signature->public_id && $signature->phone){
            $phone = preg_replace('/\D/', '', $signature->phone);
            return $this->plugsign->resendWhatsapp(array('signing_key' => $signature->public_id, 'phone' => $phone));
        }
        return NULL;
    }

    public function resendEmail($signature)
    {
        if ($signature->public_id && $signature->email){
            $mensagem = 'Lembrete de assinatura de documento';
            return $this->plugsign->resendEmail(array('signing_key' => $signature->public_id, 'email' => $signature->email, 'message' => $mensagem));
        }
        return NULL;
    }

    public function cancelSignatory($signature)
    {
        if ($signature->public_id){
            return $this->plugsign->cancelSignatory(array('signing_key' => $signature->public_id));
        }
        return NULL;
    }

    public function sendDocumentTo($document, $message, $contact)
    {
        if (!$document->public_id){
            throw new Exception('Não é possível enviar o documento sem o documento ter sido criado.');
        }

        if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
            throw new Exception('Não é possível enviar o documento cancelado.');
        }

        $send = [
            'document_key' => $document->document_key,
            'message' => $message,
            'contact' => [$contact],
        ];
        $toSingatureSend = $this->plugsign->sendDocumentTo($send);

        return $toSingatureSend['message'];
    }

    public function delete($document): bool
    {
        if ($document->public_id){
            $this->plugsign->delete(array('id' => $document->public_id));
            return true;
        }
        return false;
    }

    public function createWebhook()
    {

        $webhooks = $this->plugsign->getWebhooks();

        $webhook_update_id = null;
        foreach ($webhooks->webhooks as $webhook) {
            if ($webhook['type'] == 'REQUESTS_UPDATED') {
                $webhook_update_id = $webhook['id'];
            }
        }

        if ($webhook_update_id)  {
            $webhook_array = array(
                'url' => 'https://sistema.sagtur.com.br/infrastructure/PlugsignController/notification?token='.$this->session->userdata('cnpjempresa'),
                'status' => true,
                'type' => 'REQUESTS_UPDATED'
            );
            $this->plugsign->updateWebhook($webhook_array, $webhook_update_id);
        } else {
            $webhook_array = array(
                'url' => 'https://sistema.sagtur.com.br/infrastructure/PlugsignController/notification?token='.$this->session->userdata('cnpjempresa'),
                'status' => true,
                'type' => 'REQUESTS_UPDATED'
            );
            $this->plugsign->createWebhook($webhook_array);
        }


    }

}
