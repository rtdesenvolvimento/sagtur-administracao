<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CobreFacilService_model extends CI_Model
{
    const MENSAGEM_BAIXA_PAGMENTO = 'Pagamento baixado pelo sistema automaticamente pelo modulo de integração COBRE FÁCIL.<br/>Na data ';

    const  BOLETO = 'bankslip';
    const  PIX = 'pix';
    const  LINK_PAGAMENTO = 'pix,bankslip,credit';
    const  CARTAO_CREDITO = 'credit';
    const  BOLETO_PIX = 'pix,bankslip';
    private $notification_rule_id;

    public function __construct() {
        parent::__construct();

        $this->load->model('dto/CobrancaDTO_model', 'CobrancaDTO_model');
        $this->load->model('dto/PessoaCobrancaDTO_model', 'PessoaCobrancaDTO_model');
        $this->load->model('dto/RetornoCobrancaDTO_model','RetornoCobrancaDTO_model');
        $this->load->model('dto/BaixaCobrancaDTO_model', 'BaixaCobrancaDTO_model');
        $this->load->model('dto/BaixaFaturaCobrancaDTO_model', 'BaixaFaturaCobrancaDTO_model');
        $this->load->model('dto/BaixaPagamentoFaturaCobrancaDTO_model', 'BaixaPagamentoFaturaCobrancaDTO_model');

        $this->load->library('cobrefacil');
    }
    public function configurarIntegracao() {

        $cobreFacilSetting = $this->settings_model->getCobreFacilSettings();

        if ($cobreFacilSetting->active) {
            $this->cobrefacil->sandbox = $cobreFacilSetting->sandbox;
            $this->cobrefacil->app_id = $cobreFacilSetting->app_id;
            $this->cobrefacil->app_secret = $cobreFacilSetting->app_secret;

            $this->notification_rule_id = $cobreFacilSetting->notification_rule_id;
        }
    }
    public function emitirCobranca($cobranca) {

        if ($this->isBoleto($cobranca->tipoCobranca)) {
            return $this->boleto($cobranca);
        } else if ($this->isPix($cobranca->tipoCobranca)) {
            return $this->pix($cobranca);
        } else if ($this->isLinkPagamento($cobranca->tipoCobranca)) {
            return $this->link_pagamento($cobranca);
        } else if ($this->isCartaoCreditoLink($cobranca->tipoCobranca)) {
            return $this->cartao_credito($cobranca);
        } else if($this->isPixBoletoPix($cobranca->tipoCobranca)) {
            return $this->boleto_pix($cobranca);
        } else if ($this->isCarne($cobranca->tipoCobranca)) {
            return $this->carne($cobranca);
        } else if ($this->isMensalidade($cobranca->tipoCobranca)) {
            return $this->mensalidade($cobranca);
        }

        $tipo = $cobranca->tipoCobranca;

        return new Exception("O Tipo ".$tipo.' não permite a emissão de cobranças no Cofre Fácil');
    }
    private function boleto($cobranca) {
        return $this->create_invoice($cobranca, CobreFacilService_model::BOLETO);
    }
    private function pix($cobranca) {
        return $this->create_invoice($cobranca, CobreFacilService_model::PIX);
    }
    private function link_pagamento($cobranca) {
        return $this->create_invoice($cobranca, CobreFacilService_model::LINK_PAGAMENTO);
    }

    public function cartao_credito($cobranca) {
        return $this->create_invoice($cobranca, CobreFacilService_model::CARTAO_CREDITO);
    }

    public function boleto_pix($cobranca) {
        return $this->create_invoice($cobranca, CobreFacilService_model::BOLETO_PIX);
    }

    public function create_invoice($cobranca, $payable_with) {

        $setting = $this->settings_model->getSettings();

        $customer = $this->get_customer($cobranca->pessoa);

        $data_invoice = array(
            'reference' => $cobranca->fatura,
            'payable_with' => $payable_with,
            'customer_id' => $customer->id,
            'due_date' => implode("-",array_reverse(explode("/",$cobranca->dataVencimento))),
            'price' => (double) number_format ($cobranca->valorVencimento, 2, '.', '' ),
            'items' => [
                array(
                    'description' => $this->get_instructions($cobranca),
                    'price' => number_format ($cobranca->valorVencimento, 2, '.', ''),
                    'quantity' => 1,
                )
            ],
            'settings' => array(
                'late_fee' => array(
                    'mode' => $setting->modoCobrancaMulta,
                    'amount' => $setting->percentualMulta,
                ),
                'interest' => array(
                    'mode' => $setting->modoCobrancaJurosMora,
                    'amount' => $setting->percentualJurosMoraDia,
                ),
                /*
                'discount' => array(
                    'mode' => 'fixed',
                    'amount' => 9.99,
                    'limit_date' => 5,
                ),*/
                'warning' => array(
                    'description' => $this->get_instructions($cobranca)
                ),
                'send_tax_invoice' => 1,
                'max_installments' => 12,
            ),
        );

        if ($this->notification_rule_id) {
            $data_invoice['notification_rule_id'] = $this->notification_rule_id;
        }

        $transaction = $this->cobrefacil->create_invoice($data_invoice);

        return $this->handle_transaction($transaction, $cobranca);
    }

    public function mensalidade($cobranca) {

        $setting = $this->settings_model->getSettings();
        $customer = $this->get_customer($cobranca->pessoa);
        $customer_sys = $this->site->getCompanyByID($cobranca->pessoa->id);
        $customer_group = $this->site->getCustomerGroupByID($customer_sys->customer_group_id);

        $dtPrimeiroVencimento   = implode("-",array_reverse(explode("/",$cobranca->dataVencimento)));
        $month = ' +'.($cobranca->numeroParcelas - 1).' month';
        $dtUltimoVencimento = date("Y-m-d", strtotime($dtPrimeiroVencimento.$month));

        $subscription_data = array(
            'customer_id' => $customer->id,
            'notification_rule_id' => $this->notification_rule_id,
            'payable_with' => $customer_group->payable_with,
            'plans_id' => $customer_group->plans_id,
            'first_due_date' => $dtPrimeiroVencimento,
            'expires_at' => $dtUltimoVencimento,
            'contract_number' =>  $cobranca->fatura,
            'reference' => 'none',
            'generate_days' => 1,//Número de dias antes do vencimento que será gerado a cobrança
            'interval_type' => 'month',
            'interval_size' => 1,
            'items' => [
                array(
                    'products_services_id' => $customer_group->products_services_id,
                    'price' => number_format ($cobranca->valorVencimento, 2, '.', ''),
                    'quantity' => 1,
                )
            ],
            'settings' => array(
                'late_fee' => array(
                    'mode' => $setting->modoCobrancaMulta,
                    'amount' => (double) $setting->percentualMulta,
                ),
                'interest' => array(
                    'mode' => $setting->modoCobrancaJurosMora,
                    'amount' => (double) $setting->percentualJurosMoraDia,
                ),
                /*
                'discount' => array(
                    'mode' => 'fixed',
                    'amount' => 9.99,
                    'limit_date' => 5,
                ),
                */
                'warning' => array(
                    'description' => $this->get_instructions($cobranca)
                ),
            ),
        );

        $subscriptions_created = $this->cobrefacil->create_subscriptions($subscription_data);

        $dtoRetornoCobranca = $this->handle_errors($subscriptions_created);

        if ($dtoRetornoCobranca->sucesso) {
            for ($i = 0; $i < (int)$cobranca->numeroParcelas; $i++) {
                $invoice_created = $this->cobrefacil->generate_invoice_subscription($subscriptions_created->data->id);
                $invoice = $invoice_created->data;
                $dtoRetornoCobranca->adicionarFatura( $this->write_invoice($cobranca, $invoice, $subscriptions_created));
            }
        }

        return $dtoRetornoCobranca;
    }

    public function carne($cobranca) {

        $setting = $this->settings_model->getSettings();
        $customer = $this->get_customer($cobranca->pessoa);

        $cobranca_carne = array(
            'customer_id' => $customer->id,
            'customer_address_id' => $customer->address->id,
            'reference' => $cobranca->fatura,
            'first_due_date' => implode("-",array_reverse(explode("/",$cobranca->dataVencimento))),
            'notification_rule_id' => $this->notification_rule_id,
            'number_installments' =>  (int) $cobranca->numeroParcelas,
            'mode_installment' => 'installment_value',
            'items' => [
                array(
                    'description' => $this->get_instructions($cobranca),
                    'price' => (int) number_format ($cobranca->valorVencimento, 2, '', ''),
                    'quantity' => 1,
                )
            ],
            'settings' => array(
                'late_fee' => array(
                    'mode' => $setting->modoCobrancaMulta,
                    'amount' => (double) $setting->percentualMulta,
                ),
                'interest' => array(
                    'mode' => $setting->modoCobrancaJurosMora,
                    'amount' => (double) $setting->percentualJurosMoraDia,
                ),
                /*
                'discount' => array(
                    'mode' => 'fixed',
                    'amount' => 9.99,
                    'limit_date' => 5,
                ),
                */
                'warning' => array(
                    'description' => $this->get_instructions($cobranca)
                ),
            ),
        );

        $transaction = $this->cobrefacil->create_booklets($cobranca_carne);

        return $this->handle_transaction($transaction, $cobranca);
    }

    private function handle_errors($transaction) {
        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();

        if (!$transaction->success) {
            $dtoRetornoCobranca->sucesso = false;

            if (!empty($transaction->errors)) {
                foreach ($transaction->errors as $error) {
                    $dtoRetornoCobranca->erro .= $error.'<br/>';
                }
            } else {
                $dtoRetornoCobranca->erro = $transaction->description != null ? $transaction->description : $transaction->message;
            }

            $dtoRetornoCobranca->data = [];
            return $dtoRetornoCobranca;
        }

        if (empty($transaction->data)) {
            $dtoRetornoCobranca->sucesso = false;
            $dtoRetornoCobranca->erro = 'Tivemos um problema ao processar a requisição.';
            $dtoRetornoCobranca->data = [];
            return $dtoRetornoCobranca;
        }

        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        return $dtoRetornoCobranca;
    }
    private function handle_transaction($transaction, $cobranca) {
-
        $dtoRetornoCobranca = $this->handle_errors($transaction);

        if (!empty($invoice->invoices)) {
            foreach ($invoice->invoices as $invoice) {
                $dtoRetornoCobranca->adicionarFatura($this->write_invoice($cobranca, $invoice, $transaction));
            }
        } else {
            $invoice = $transaction->data;
            $dtoRetornoCobranca->adicionarFatura($this->write_invoice($cobranca, $invoice, $transaction));
        }

        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        return $dtoRetornoCobranca;
    }

    public function write_invoice($cobranca, $invoice, $transaction) {
        $data_invoice = array(
            'status' => Financeiro_model::STATUS_ABERTA,
            'fatura' => $cobranca->fatura,
            'tipo' => $this->cobrefacil->tipoCobranca,
            'integracao' => 'cobrefacil',
            'status_detail' => 'created',
            'code' => $invoice->id,
            'dueDate' => $invoice->due_date,
            'installmentLink' => $invoice->url_invoice,
            'log' => $this->sma->tirarAcentos(print_r($transaction, true)),
        );

        if ($this->isBoleto($cobranca->tipoCobranca)) {
            $data_invoice['link'] = $invoice->url_bankslip;
            $data_invoice['checkoutUrl'] = $invoice->url;
        } else {
            $data_invoice['link'] = $invoice->url;
            $data_invoice['checkoutUrl'] = $invoice->url;
        }

        if ($this->isMensalidade($cobranca->tipoCobranca)) {
            $data_invoice['subscriptions_id'] = $transaction->data->id;
        }

        if ($this->isCarne($cobranca->tipoCobranca)) {
            $data_invoice['booklets_id'] = $transaction->data->id;
        }

        return $data_invoice;
    }

    public function get_customer($pessoaCobranca) {

        if ($this->isPessoaJuridica($pessoaCobranca)) {
            $customerFound =  $this->cobrefacil->search_customer_by_ein($this->getCNPJ($pessoaCobranca->cpfCnpj));
        } else {
            $customerFound =  $this->cobrefacil->search_customer_by_cpf($this->getCPF($pessoaCobranca->cpfCnpj));
        }

        if ($this->customer_not_found($customerFound)) {
            return $this->create_client($pessoaCobranca);
        } else {
            return $this->update_client($pessoaCobranca, $customerFound->data[0]->id, $customerFound->data[0]->address->id);
        }

    }

    public function customer_found($customer) {
        if ($customer->success) {
            if (!empty($customer->data)) {
                return $customer;
            }
        }
        return false;
    }

    private function customer_not_found($customer) {
        return !$this->customer_found($customer);
    }
    public function create_client($person) {

        $data_customer = array(
            'person_type' => $this->isPessoaJuridica($person) ? 2 : 1,
            'telephone' => $person->telefone,
            'cellular' => $person->celular,
            'email' => $person->email,
            //'email_cc' => '',
            'address' => array(
                'description' => 'Endereço Principal',
                'zipcode' => $this->getZipCodd($person->cep),
                'street' => $person->endereco,
                'number' => $person->numero,
                'complement' => $person->complemento,
                'neighborhood' => $person->bairro,
                'city' => $person->cidade,
                'state' => $person->estado,
            ),
        );

        if ($this->isPessoaJuridica($person)) {
            $data_customer['ein']           = $this->getCNPJ($person->cpfCnpj);
            $data_customer['company_name']  = $person->nome;
        } else {
            $data_customer['taxpayer_id']   = $this->getCPF($person->cpfCnpj);
            $data_customer['personal_name']  = $person->nome;
        }

        $customer_insert = $this->cobrefacil->create_client($data_customer);

        if ($this->customer_found($customer_insert)) {
            return $customer_insert->data;
        }

        $message_error = '';
        if (!empty($customer_insert->errors)) {
            foreach ($customer_insert->errors as $error) {
                $message_error .= $error.'<br/>';
            }
        }

        throw new Exception('Não foi possível incluir o cliente.<br/>'.$message_error);
    }

    public function update_client($person, $customer_id, $address_id) {

        $data_customer = array(
            'personal_name' => $person->nome,
            'telephone' => $person->telefone,
            'cellular' => $person->celular,
            'email' => $person->email,
            'address' => array(
                'id' => $address_id,
                'description' => 'Endereço Principal',
                'zipcode' => $this->getZipCodd($person->cep),
                'street' => $person->endereco,
                'number' => $person->numero,
                'complement' => $person->complemento,
                'neighborhood' => $person->bairro,
                'city' => $person->cidade,
                'state' => $person->estado,
            ),
        );

        if ($this->isPessoaJuridica($person)) {
            $data_customer['company_name']  = $person->nome;
        } else {
            $data_customer['personal_name']  = $person->nome;
        }

        $updated_customer = $this->cobrefacil->update_client($data_customer, $customer_id);

        if ($this->customer_found($updated_customer)) {
            return $updated_customer->data;
        }

        $message_error = '';
        if (!empty($customer_insert->errors)) {
            foreach ($customer_insert->errors as $error) {
                $message_error .= $error.'<br/>';
            }
        }

        throw new Exception('Não foi possível atualizar os dados do cliente.<br/>'.$message_error);
    }

    public function buscarPagamento($code) {

        $this->configurarIntegracao();

        $transaction =  $this->cobrefacil->invoice_details_by_id($code);

        if (!$this->handle_errors_payment($transaction)) return [];

        $invoice = $transaction->data;

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = $invoice->id;
        $fatura->reference = $invoice->reference;
        $fatura->valorVencimento = $invoice->price;
        $fatura->dtVencimento = $invoice->paid_at;
        $fatura->log = $this->sma->tirarAcentos(print_r($invoice, true));
        $baixaCobranca->adicionarFatura($fatura);

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $pagamento->id = $invoice->id;
        $pagamento->status = $invoice->status;
        $pagamento->dataPagamento = $invoice->paid_at;
        $pagamento->valorPago = $invoice->total_paid;
        $pagamento->formaPagamento = $invoice->payment_method;
        $pagamento->taxa = $invoice->fee != null ? $invoice->fee :  0;
        $pagamento->observacao = CobreFacilService_model::MENSAGEM_BAIXA_PAGMENTO . ' ' . date('d/m/Y H:i:s') . '<br/>Foi descontado uma taxa de R$'.$invoice->fee.' da agência';
        $fatura->adicionarPagamento($pagamento);

        return $baixaCobranca;
    }

    private function handle_errors_payment($transaction) {

        if (!$transaction->success) {
            return false;
        }

        if (empty($transaction->data)) {
            return false;
        }

        return true;
    }

    private function isPessoaJuridica($pessoaCobranca) {
        return $pessoaCobranca->tipoPessoa == 'PJ';
    }

    private function getCPF($cpf) {
        return str_replace ( '-', '', str_replace ( '.', '', $cpf));
    }

    private function getCNPJ($cnpj) {
        return str_replace ( '-', '', str_replace ( '.', '', str_replace ( '/', '', $cnpj)));
    }

    private function getZipCodd($zip_cod) {
        return str_replace ( '-', '', trim($zip_cod));
    }

    public function get_instructions($cobranca) {
        $customer_sys = $this->site->getCompanyByID($cobranca->pessoa->id);
        $customer_group = $this->site->getCustomerGroupByID($customer_sys->customer_group_id);


        if ($customer_group) {
            $instruction = 'CONTRATAÇÃO DO PLANO '.$customer_group->name.' - '.$cobranca->instrucoes;
        }

        return strtoupper($instruction);
    }

    public function cancelarCobranca($cobranca_id) {
        $this->cobrefacil->cancel_invoice($cobranca_id);
    }

    private function isBoleto($tipo) {
        if ($tipo == 'boleto') return true;
        return false;
    }

    private function isPix($tipo) {
        if ($tipo == 'pix') return true;
        return false;
    }

    private function isPixBoletoPix($tipo) {
        if ($tipo == 'boleto_pix') return true;
        return false;
    }

    private function isCartaoCreditoLink($tipo) {
        if ($tipo == 'cartao_credito_link') return true;
        return false;
    }

    private function isLinkPagamento($tipo) {
        if ($tipo == 'link_pagamento') return true;
        return false;
    }

    private function isCarne($tipo) {
        if ($tipo == 'carne') return true;
        return false;
    }

    private function isMensalidade($tipo) {
        if ($tipo == 'mensalidade') return true;
        return false;
    }

}