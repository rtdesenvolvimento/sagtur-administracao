<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AppCompraService_model extends CI_Model
{
    private const COBRANCA_EXTRA_ASSENTO = 'Cobrança Extra de Assento';

    public function __construct() {
        parent::__construct();

        $this->load->model('model/Venda_model', 'Venda_model');

        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');
        $this->load->model('service/BusService_model', 'BusService_model');

        $this->load->model('companies_model');
    }

    private function coletarPagador($product)
    {
        return $product->isApenasColetarPagador == 1 || $product->isApenasColetarPagador == 2;
    }

    private function colegarIngresso($product)
    {
        return $product->isApenasColetarPagador == 1;
    }

    private function pagadorComoPassageiro($product)
    {
        return $product->isApenasColetarPagador == 0;
    }

    public function salvar($appModel) {

        //$appModel = new AppCompra_model();

        $produto        = $appModel->produto;

        if ($appModel->cliente) {
            if ($appModel->cliente->getDataAniversario()) {

                $idadeDoPagador = $this->sma->calcularIdade($appModel->cliente->getDataAniversario());

                if (!$produto->permiteVendaMenorIdade && $idadeDoPagador < 18 && !$this->coletarPagador($produto)) {
                    throw  new Exception(lang("Não é permitido a venda deste serviço para menores de 18 anos como pagador desta compra.<br/>Ou verifique a data de nascimento informada e faça uma nova tentantiva em sua reserva."));
                }
            }
        }

        if ($this->coletarPagador($produto)) {
            $totalPassageiros = count($appModel->dependentes);
        } else {
            $totalPassageiros = count($appModel->dependentes) + 1;//TODO SE O PAGADOR ESTIVER JUNTO NOS ITENS SOMA ELE NO CONTADOR DE PASSAGEIROS
        }

        if ((int) $appModel->totalPassageiros != $totalPassageiros && !$this->colegarIngresso($produto)) {
            throw new Exception('Notamos uma divergência entre o total de passageiros selecionados com o número de informações de cada um dos passageiros. O Total de depassageiros selecionados foi de '. $appModel->totalPassageiros .' mas identificamos apenas os dados de '. $totalPassageiros .' passageiros. <br/>Faça uma nova tentativa de compra ou entre em contato com o Administrador. Pedimos desculpa pelo transtarno.');
        }

        $this->BusService_model->desbloquear_todos_assentos_session_card();

        return $this->criarVenda($appModel);
    }

    private function criarVenda($appModel) {

        $clienteModelSalvo = $this->salvarCliente($appModel->cliente, $appModel);

        //$appModel = new AppCompra_model();
        //$clienteModel = new Cliente_model();
        //$ingresso = new Ingresso_model();

        $statusVenda                = Venda_model::STATUS_ORCAMENTO;
        $Settings                   = $this->site->get_setting();
        $referenceNoContaReceber    = $this->site->getReference('ex');

        $condicaoPagamentoId        = $appModel->condicaoPagamento;
        $dataVencimento             = date('Y-m-d');
        $productsTodos              = array();

        $vendedor                   = $this->site->getCompanyByID($appModel->vendedorId);
        $user                       = $this->site->getUserByBiller($appModel->vendedorId);

        if ($appModel->valorSinal > 0) {
            $condicaoPagamentoObj   = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
            $condicaoPagamento      = $this->site->getProximaCondicaoPagamento($condicaoPagamentoObj->parcelas);//TODO BUSCA A PROXIMA CONDICAO DE PAGAMENTO
            $condicaoPagamentoId    = $condicaoPagamento->id;
            $tipoCobranca           = $this->site->getTipoCobrancaById($appModel->tipoCobrancaSinal);
        } else {
            $tipoCobranca           = $this->site->getTipoCobrancaById($appModel->tipoCobranca);
            $condicaoPagamento      = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
        }

        if ($tipoCobranca->faturarVenda == 1) {
            $statusVenda =  Venda_model::STATUS_FATURADA;
        }

        if ($appModel->produto->apenas_cotacao) {
            $statusVenda = Venda_model::STATUS_ORCAMENTO;
        }

        if ($appModel->listaEspera <= 0) {//TODO LISTA DE ESPERA POIS O ESTOQUE E ZERO
            $statusVenda = Venda_model::STATUS_LISTA_EMPERA;
        }

        $valoresVencimento      = $appModel->valorVencimentosParcelas;
        $dtVencimentos          = $appModel->vencimentosParcela;
        $parcelas               = $condicaoPagamento->parcelas;

        $produto                = $appModel->produto;
        $programacao            = $appModel->programacao;
        $dependentes            = $appModel->dependentes;
        $adicionais             = $appModel->adicionais;

        $observacao             = 'PLANO '. strtoupper($produto->name).'. Caso não realize o pagamento da sua mensalidade em até 10 dias seu sistema será BLOQUEADO - Em até 30 dias seu sistema será CANCELADO - Em até 60 dias sua cobrança será PROTESTADA.';

        if (($tipoCobranca->tipo == 'carne_cartao_transparent' ||
            $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
            $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
            $tipoCobranca->tipo == 'link_pagamento'  ||
            $tipoCobranca->tipo == 'carne_cartao') && $appModel->valorSinal == 0) {//TODO CARTAO CHECKIN TRANSPARTENTE

            $condicaoPagamentoId    = 1;//TODO CONFIGURA A CONDICAO A VISTA
            $dtVencimentos          = [$dataVencimento];
            $valoresVencimento      = [$appModel->total + $appModel->getAcrescimo()];
            $parcelas               = $appModel->parcelas;

            $observacao .= ' CLIENTE: '. mb_strtoupper($clienteModelSalvo->getName(), mb_internal_encoding());
        }

        if ($appModel->observacaoAdicional) {
            $observacao .= ' OBS: '.$appModel->observacaoAdicional;
        }

        $acrescimo  = $appModel->getAcrescimo();
        $desconto   = 0;

        if ($acrescimo <= 0) {
            $acrescimo = 0;
            $desconto = $appModel->getAcrescimo()*-1;
        }

        $venda = array(
            'date' 					=> date('Y-m-d H:i'),
            'reference_no' 			=> $this->site->getReference('so'),

            'customer_id' 			=> $clienteModelSalvo->getId(),
            'customer' 				=> mb_strtoupper($clienteModelSalvo->getName(), mb_internal_encoding()),
            'vendedor'              => $vendedor->name,
            'biller' 				=> $vendedor->company != '-' ? $vendedor->company : $vendedor->name,
            'biller_id' 			=> $vendedor->id,
            'note' 					=> $observacao,

            'grand_total' 			=> $appModel->total + $appModel->getAcrescimo(),
            'total' 				=> $appModel->total + $appModel->getAcrescimo(),
            'valorVencimento'       => $appModel->total + $appModel->getAcrescimo(),

            'shipping' 				=> $acrescimo,
            'order_discount'    	=> $desconto,//TODO DESCONTA TAMBEM O CUPOM SE HOUVER
            'order_discount_id' 	=> $desconto,
            'total_discount' 		=> $desconto,

            'warehouse_id'			=> $Settings->default_warehouse,
            'total_items' 			=> $appModel->totalPassageiros,
            'sale_status' 			=> $statusVenda,
            'payment_status' 		=> 'due',//TODO PAGAR
            'payment_term' 			=> '',
            'posicao_assento'       => '',
            'product_discount' 		=> 0,
            'product_tax' 			=> 0,
            'order_tax_id' 			=> 0,
            'order_tax' 			=> 0,
            'total_tax' 			=> 0,
            'paid' 					=> 0,
            'vencimento'            => $dataVencimento,
            'condicaopagamentoId'   => $condicaoPagamentoId,
            'tipoCobrancaId'        => $tipoCobranca->id,
            'previsao_pagamento'    => $dataVencimento,
            'created_by' 			=> $user->id,
            'meioDivulgacao' 	    => ($appModel->meioDivulgacao != null ? $appModel->meioDivulgacao : $this->Settings->meio_divulgacao_default),

            'venda_link'            => 1,

            'cupom_id'              => $appModel->cupom_id,
            'desconto_cupom'        => $appModel->getDescontoCupom(),

            'valor_sinal'           => $appModel->valorSinal,
            'tipo_cobranca_sinal'   => $appModel->tipoCobrancaSinal,

            'file_name_contract'    => $appModel->file_pdf,
        );

        $conta_receber = array(
            'status'                            => 'ABERTA',
            'receita'                           => 13,//TODO RECEITA PADRAO VIRA DA CONFIGURACAO

            'valor'                             => $appModel->total + $appModel->getAcrescimo(),

            'reference'                         => $referenceNoContaReceber,
            'created_by'                        => $user->id,
            'pessoa'                            => $clienteModelSalvo->getId(),
            'tipocobranca'                      => $tipoCobranca->id,
            'condicaopagamento'                 => $condicaoPagamentoId,
            'dtvencimento'                      => $dataVencimento,
            'dtcompetencia'                     => $dataVencimento,
            'warehouse'                         => $Settings->default_warehouse,
            'note'                              => $observacao,

            'cpfTitularCartao'                  => $appModel->cpfTitularCartao,//transient to cc
            'celularTitularCartao'              => $appModel->celularTitularCartao,//transient to cc
            'cepTitularCartao'                  => $appModel->cepTitularCartao,//transient to cc
            'enderecoTitularCartao'             => $appModel->enderecoTitularCartao,//transient to cc
            'numeroEnderecoTitularCartao'       => $appModel->numeroEnderecoTitularCartao,//transient to cc
            'complementoEnderecoTitularCartao'  => $appModel->complementoEnderecoTitularCartao,//transient to cc
            'bairroTitularCartao'               => $appModel->bairroTitularCartao,//transient to cc
            'cidadeTitularCartao'               => $appModel->cidadeTitularCartao,//transient to cc
            'estadoTitularCartao'               => $appModel->estadoTitularCartao,//transient to cc
            'totalParcelaPagSeguro'             => $appModel->totalParcelaPagSeguro,//transient to cc

            'senderHash'                        => $appModel->senderHash,//transient to cc
            'creditCardToken'                   => $appModel->creditCardToken,//transient to cc
            'deviceId'                          => $appModel->deviceId,//transient to cc

            'parcelas'                          => $parcelas,//transient to cc

            'cardName'                          => $appModel->cardName,//transient to cc
            'cardNumber'                        => $appModel->cardNumber,//transient to cc
            'cardExpiryMonth'                   => $appModel->cardExpiryMonth,//transient to cc
            'cardExpiryYear'                    => $appModel->cardExpiryYear,//transient to cc
            'cvc'                               => $appModel->cvc,//transient to cc
        );

        if ($this->pagadorComoPassageiro($produto)) {

            $produto = $this->adicionarProduto($appModel, $appModel->cliente);
            $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$produto['product_id']] = $produto;//TODO INCLUINDO O PRODUTO PARA O CLIENTE PAGADOR

            if ($clienteModelSalvo->getUltimoValorExtraAssento() > 0) {

                $produto_cobranca_extra_assento = $this->adicionar_cobranca_extra_assento($appModel, $clienteModelSalvo, $produto['product_id'], $clienteModelSalvo->getUltimoValorExtraAssento(), $this->getCode());

                $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$this->getCode()] = $produto_cobranca_extra_assento;//TODO INCLUINDO O PRODUTO PARA O CLIENTE PAGADOR
            }

            //adicionar o pagador a lista de passageiros
            $clientes[] = array(
                'cliente' => $clienteModelSalvo,
            );
        }

        if ($this->colegarIngresso($produto)) {

            $ingressos = $appModel->ingressos;

            foreach ($ingressos as $ingresso) {

                $passageiro = $appModel->cliente;

                if ($ingresso->quantidade > 0) {

                    $faixa_etaria                   = $this->settings_model->getValorFaixa($ingresso->faixaId);

                    $passageiro->faixaId            = $faixa_etaria->id;
                    $passageiro->faixaName          = $faixa_etaria->name;
                    $passageiro->descontarVaga      = $faixa_etaria->descontarVaga;
                    $passageiro->faixaEtariaValor   = ($ingresso->valor * $ingresso->quantidade);

                    $produto                        = $this->adicionarProduto($appModel, $passageiro, 0, null, 0, $ingresso->quantidade);

                    $productsTodos['c_'.$clienteModelSalvo->getId().'p'.$produto['product_id'].'faixa'.$faixa_etaria->id] = $produto;

                }
            }
        }

        foreach ($dependentes as $dependente) {

            //$dependente = new Cliente_model();

            $dependenteModelSalvo   = $this->salvarCliente($dependente, $appModel);
            $produto                = $this->adicionarProduto($appModel, $dependenteModelSalvo);

            if ($dependente->getUltimoValorExtraAssento() > 0) {

                $produto_cobranca_extra_assento = $this->adicionar_cobranca_extra_assento($appModel, $dependenteModelSalvo, $produto['product_id'], $dependente->getUltimoValorExtraAssento(), $this->getCode());

                $productsTodos['c_'.$dependenteModelSalvo->getId().'p'.$this->getCode()] = $produto_cobranca_extra_assento;//TODO INCLUINDO O PRODUTO PARA OS DEPENDENTES

            }

            $productsTodos['c_'.$dependenteModelSalvo->getId().'p'.$produto['product_id']] = $produto;//TODO INCLUINDO O PRODUTO PARA OS DEPENDENTES

            $clientes[] = array(
                'cliente' => $dependenteModelSalvo,
            );
        }

        //$this->sma->print_arrays($adicionais);

        $produtoId = $produto['product_id'];

        if (!empty($adicionais)) {
            foreach ($adicionais as $adicional) {
                for ($i=0;$i<$adicional->quantidade;$i++) {
                    foreach ($clientes as $c) {
                        $adicionalClienteModel = $c['cliente'];

                        if ($this->disponibilidadeAdicinalAcabou($adicional)) {
                            continue;
                        }

                        if ($this->faixaEtariaDiferente($adicionalClienteModel, $adicional)) {
                            continue;
                        }

                        $adicional->quantidade  = $adicional->quantidade - 1;

                        $produtoDoCliente       = $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $produtoId];
                        $produtoAdicional       = $this->site->getProductByID($adicional->produto);

                        $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $produtoId] = $this->adiconarNovoServicoAdicional($produtoAdicional, $produtoDoCliente, $adicional->valor);

                        $appModel->produto = $produtoAdicional;
                        $productsTodos['c_' . $adicionalClienteModel->getId() . 'p' . $appModel->produto->id] = $this->adicionarProduto($appModel, $adicionalClienteModel, 1, $appModel->produto->id, $adicional->valor);//TODO INCLUINDO O  PRODUTO ADICIONAL DO CLIENTE PAGADOR
                    }
                }
            }
        }

        //$this->sma->print_arrays($adicionais, $venda, $conta_receber, $productsTodos, $valoresVencimento, $dtVencimentos);

        $valorVencimentoParcela = $valoresVencimento;
        $dataVencimentosParcela = $dtVencimentos;

        $tiposCobrancaParcela   = $appModel->tiposCobranca;
        $movimentadores         = $appModel->movimentadores;
        $descontos              = $appModel->descontos;

        $si_return              = array();
        $payment                = [];

        $saleId = $this->sales_model->addSale(
            $venda,
            $productsTodos,
            $payment,
            $si_return,
            $conta_receber,

            $valorVencimentoParcela,
            $dataVencimentosParcela,
            $tiposCobrancaParcela,
            $movimentadores,
            $descontos
        );

        $this->SaleEventService_model->customer_manual_sale_log($saleId, 'Log de Confirmação de Compra Via Link', $Settings->termos_aceite, $user);

        $this->load->model('service/SendMessageService_model', 'SendMessageService_model');

        $message = 'Nova *CONTRATAÇÃO*%0ACliente ' . trim(mb_strtoupper($clienteModelSalvo->getName(), mb_internal_encoding())) . '.%0APlano: '. strtoupper($appModel->produto->name) .'%0AValor do Plano: R$ '.number_format($appModel->total + $appModel->getAcrescimo(), 2, ',', '.') .'%0AData da compra: '.date('d/m/Y H:i');

        $this->SendMessageService_model->send_text(urldecode($message), '554898579104');

        return $saleId;
    }

    private function getCode(){
        return rand(pow(10, 10 - 1), pow(10, 10) - 1);
    }

    private function adiconarNovoServicoAdicional($produtoAdicional, $produtoDoCliente, $valor) {

        $servicosAdicionais = $this->getServicoAdicionalArray($produtoAdicional, $valor);
        $servicosJaAdicionados = $produtoDoCliente['servicosAdicionais'];
        $servicosJaAdicionados[] = $servicosAdicionais;

        $produtoDoCliente['servicosAdicionais'] = $servicosJaAdicionados;

        return $produtoDoCliente;
    }

    private function getServicoAdicionalArray($produtoAdicional, $valor) {

        $categoria = $this->site->getCategoryByID($produtoAdicional->category_id);

        $servicosAdicionais = array(
            'nome_produto' => $produtoAdicional->name,
            'codigo_produto' => $produtoAdicional->code,
            'categoria_produto' => $categoria->name,
            'produtoId' => $produtoAdicional->id,
            'valor' => $valor,
            'quantidade' => 1,
        );

        return $servicosAdicionais;
    }

    private function faixaEtariaDiferente($clienteModel, $adicional) {
        return $clienteModel->getFaixaId() != $adicional->faixaId;
    }

    private function disponibilidadeAdicinalAcabou($adicional) {
        return $adicional->quantidade == 0;
    }

    private function adicionarProduto($appModel, $clienteModel, $adicional = 0, $produtoPai = null, $valorAdicional = 0, $quantity = 1) {

        //$appModel = new AppCompra_model();
        //$clienteModel = new Cliente_model();

        $produto        = $appModel->produto;
        $programacao    = $appModel->programacao;

        $tiposTransporte = $this->ProdutoRepository_model->getTransportes($produto->id);
        $localEmbarque = $this->site->getLocalEmbarqueByID($appModel->localEmbarque);
        $tipoHospeagem = $this->site->getTipoHospedagemID($appModel->tipoHospedagem);

        $tipoTransporte = null;
        $nmTipoTransporte = null;

        if ($appModel->tipoTransporte) {
            $tipoTransporteObj = $this->site->getTipoTransporteID($appModel->tipoTransporte);
            $tipoTransporte = $appModel->tipoTransporte;
            $nmTipoTransporte = $tipoTransporteObj->name;

        } else {
            foreach ($tiposTransporte as $tpTransporte) {
                $tipoTransporte = $tpTransporte->id;
                $nmTipoTransporte = $tpTransporte->text;
            }
        }

        $Settings = $this->site->get_setting();
        $categoria = $this->site->getCategoryByID($produto->category_id);

        $valorHospedagem = 0;//TODO NA HORA QUE CRIAR POR TIPO DE HOSPEDAGEM
        $valorPacote = $clienteModel->getFaixaEtariaValor() != null ? $clienteModel->getFaixaEtariaValor()  : 0.00 ;
        $descontarVaga = $clienteModel->getDescontarVaga();//TODO CONTROLA SE DESCONTA VAGAS.

        if ($adicional) {
            $valorPacote    = $valorAdicional;
            $descontarVaga  = 0;
            $clienteModel->setUltimoAssento('');
        }

        $products = array(

            'itinerarios' => [],
            'transfers' => [],
            'servicosAdicionais' => [],

            'product_id'  => $produto->id,
            'product_code' => $produto->code,
            'product_name' => $produto->name,
            'product_type' => $produto->type,
            'product_comissao' => 0,

            'faixaId'       => $clienteModel->getFaixaId(),
            'faixaNome'     => $clienteModel->getFaixaName(),
            'descontarVaga' =>  $descontarVaga,

            'programacaoId'  => $appModel->programacao->id,
            'product_category' => $produto->category_id,

            'nmTipoTransporte'  => $nmTipoTransporte,
            'tipoTransporte'  => $tipoTransporte,

            'localEmbarque' => $localEmbarque->id,

            'tipoHospedagem'  =>$tipoHospeagem->id,
            'nmTipoHospedagem'  => $tipoHospeagem->name,

            'origem'  => '',
            'destino'  => '',
            'reserva'  => '',
            'nmFornecedor'  => '',

            'nmCategoria'  => $categoria->name,
            'tipoDestino'  => '',

            'dateCheckOut' => $programacao->dataSaida,
            'dateCheckin'  => $programacao->horaSaida,
            'horaCheckin'  => $programacao->dataRetorno,
            'horaCheckOut' => $programacao->horaRetorno,
            'dataEmissao'  => date('Y-m-d'),

            'valorHospedagem'  => $valorHospedagem,

            'valorPorFaixaEtaria'  => $valorPacote,

            'net_unit_price' => $valorPacote,
            'unit_price' => $valorPacote,
            'subtotal' => $valorPacote,
            'real_unit_price' => $valorPacote,

            'poltronaClient' => $clienteModel->getUltimoAssento(),//TODO assento selecionado pelo cliente
            'customerClient' => $clienteModel->getId(),
            'note_item' =>  $clienteModel->getUltimaNota(),//TODO NOTA DO ITEM
            'customerClientName' => mb_strtoupper($clienteModel->getName(), mb_internal_encoding()),

            'categoriaAcomodacao'  => '',
            'regimeAcomodacao'  => '',

            'receptivo'  => '',
            'nmReceptivo'  => '',

            'nmEmpresaManual'  => '',
            'telefoneEmpresa'  => '',
            'enderecoEmpresa'  => '',

            'apresentacaoPassagem'  => '',
            'dataApresentacao'  => null,
            'horaApresentacao'  => null,

            'tarifa' =>  0,
            'taxaAdicionalFornecedor' => 0,
            'descontoTarifa' => 0,
            'acrescimoTarifa' => 0,
            'adicionalBagagem' => 0,
            'adicionalReservaAssento' => 0,
            'valorPagoFornecedor' => 0,
            'formaPagamentoFornecedorCustomer' => null,
            'bandeiraPagoFornecedor' => null,
            'numeroParcelasPagoFornecedor' => 0,
            'descricaoServicos' => '',
            'observacaoServico' => '',

            'fornecedorId' => null,
            'tipoCobrancaFornecedorId' => null,
            'condicaoPagamentoFornecedorId' => null,

            'desconto_fornecedor' => 0,
            'comissao_rav_du' => 0,
            'incentivoFornecedor' => 0,
            'outrasTaxasFornecedor' => 0,
            'taxaCartaoFornecedor' => 0,
            'valorRecebimentoValor' => 0,

            'option_id' => NULL,

            'warehouse_id' => $Settings->default_warehouse,
            'quantity' => $quantity,
            'item_tax' => 0,
            'tax_rate_id' => NULL,
            'tax' => 0,
            'discount' => 0,
            'item_discount' => 0,
            'serial_no' => '',

            'adicional' => $adicional,
            'servicoPai'  => $produtoPai,

            'cupom_id' => $appModel->cupom_id,
        );

        return $products;
    }

    private function adicionar_cobranca_extra_assento($appModel, $clienteModel,  $produtoPai, $valorExtra = 0, $code) {

        $programacao    = $appModel->programacao;

        $Settings = $this->site->get_setting();

        $products = array(

            'itinerarios' => [],
            'transfers' => [],
            'servicosAdicionais' => [],

            'product_id'    => $code,
            'product_code'  => $code,
            'product_name'  => AppCompraService_model::COBRANCA_EXTRA_ASSENTO,
            'product_type'  => 'standard',

            'product_comissao'  => 0,
            'faixaId'           => $clienteModel->getFaixaId(),
            'faixaNome'     => $clienteModel->getFaixaName(),
            'descontarVaga' =>  0,
            'programacaoId'  => $appModel->programacao->id,
            'product_category' => 'ASSENTO EXTRA',
            'nmTipoTransporte'  => '',
            'tipoTransporte'  => null,
            'localEmbarque' => null,
            'tipoHospedagem'  => null,
            'nmTipoHospedagem'  => '',
            'origem'  => '',
            'destino'  => '',
            'reserva'  => '',
            'nmFornecedor'  => '',
            'nmCategoria'  => AppCompraService_model::COBRANCA_EXTRA_ASSENTO,
            'tipoDestino'  => '',
            'dateCheckOut' => $programacao->dataSaida,
            'dateCheckin'  => $programacao->horaSaida,
            'horaCheckin'  => $programacao->dataRetorno,
            'horaCheckOut' => $programacao->horaRetorno,
            'dataEmissao'  => date('Y-m-d'),
            'valorHospedagem'  => 0,
            'valorPorFaixaEtaria'  => $valorExtra,
            'net_unit_price' => $valorExtra,
            'unit_price' => $valorExtra,
            'subtotal' => $valorExtra,
            'real_unit_price' => $valorExtra,
            'poltronaClient' => '',
            'note_item' =>  $clienteModel->getUltimaNota(),//TODO NOTA DO ITEM
            'customerClient' => $clienteModel->getId(),
            'customerClientName' => mb_strtoupper($clienteModel->getName(), mb_internal_encoding()),
            'categoriaAcomodacao'  => '',
            'regimeAcomodacao'  => '',
            'receptivo'  => '',
            'nmReceptivo'  => '',
            'nmEmpresaManual'  => '',
            'telefoneEmpresa'  => '',
            'enderecoEmpresa'  => '',
            'apresentacaoPassagem'  => '',
            'dataApresentacao'  => null,
            'horaApresentacao'  => null,
            'tarifa' =>  0,
            'taxaAdicionalFornecedor' => 0,
            'descontoTarifa' => 0,
            'acrescimoTarifa' => 0,
            'adicionalBagagem' => 0,
            'adicionalReservaAssento' => 0,
            'valorPagoFornecedor' => 0,
            'formaPagamentoFornecedorCustomer' => null,
            'bandeiraPagoFornecedor' => null,
            'numeroParcelasPagoFornecedor' => 0,
            'descricaoServicos' => '',
            'observacaoServico' => '',
            'fornecedorId' => null,
            'tipoCobrancaFornecedorId' => null,
            'condicaoPagamentoFornecedorId' => null,
            'desconto_fornecedor' => 0,
            'comissao_rav_du' => 0,
            'incentivoFornecedor' => 0,
            'outrasTaxasFornecedor' => 0,
            'taxaCartaoFornecedor' => 0,
            'valorRecebimentoValor' => 0,
            'option_id' => NULL,
            'warehouse_id' => $Settings->default_warehouse,
            'quantity' => 1,
            'item_tax' => 0,
            'tax_rate_id' => NULL,
            'tax' => 0,
            'discount' => 0,
            'item_discount' => 0,
            'serial_no' => '',
            'adicional' => 1,
            'servicoPai'  => $produtoPai,
            'cupom_id' => $appModel->cupom_id,
        );

        return $products;
    }

    private function salvarCliente($clienteModel, $appModel) {

        //$clienteModel = new Cliente_model();
        $clienteEncontrou = $this->companies_model->getVerificaCustomeByCPF($clienteModel->getVatNo());

        if ($this->encontrouClienteCadastrado($clienteEncontrou, $clienteModel->getVatNo())) {

            $clienteAtualizar = $this->preencherCliente($clienteModel);

            $produtoId = (int) $appModel->produto->id;

            if ($produtoId === 4) {
                $clienteAtualizar['customer_group_id']          = 1;
                $clienteAtualizar['customer_group_name']        = 'BASIC';
            } elseif ($produtoId === 5) {

                $clienteAtualizar['customer_group_id']          = 3;
                $clienteAtualizar['customer_group_name']        = 'PREMIUM';

            } elseif ($produtoId === 6) {
                $clienteAtualizar['customer_group_id']          = 13;
                $clienteAtualizar['customer_group_name']        = 'GOLD';
            } elseif ($produtoId === 7) {
                $clienteAtualizar['customer_group_id']          = 18;
                $clienteAtualizar['customer_group_name']        = 'BASIC + DOMINIO';
            } elseif ($produtoId === 8) {
                $clienteAtualizar['customer_group_id']          = 19;
                $clienteAtualizar['customer_group_name']        = 'PREMIUM + DOMINIO';
            } elseif ($produtoId === 9) {
                $clienteAtualizar['customer_group_id']          = 20;
                $clienteAtualizar['customer_group_name']        = 'GOLD + DOMINIO';
            } elseif ($produtoId === 13) {
                $clienteAtualizar['customer_group_id']          = 21;
                $clienteAtualizar['customer_group_name']        = 'BASIC + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 14) {
                $clienteAtualizar['customer_group_id']          = 22;
                $clienteAtualizar['customer_group_name']        = 'PREMIUM + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 15) {
                $clienteAtualizar['customer_group_id']          = 23;
                $clienteAtualizar['customer_group_name']        = 'GOLD + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 10) {
                $clienteAtualizar['customer_group_id']          = 24;
                $clienteAtualizar['customer_group_name']        = 'BASIC + ASSINATURA DIGITAL';
            } elseif ($produtoId === 11) {
                $clienteAtualizar['customer_group_id']          = 25;
                $clienteAtualizar['customer_group_name']        = 'PREMIUM + ASSINATURA DIGITAL';
            } elseif ($produtoId === 12) {
                $clienteAtualizar['customer_group_id']          = 26;
                $clienteAtualizar['customer_group_name']        = 'GOLD + ASSINATURA DIGITAL';
            } else {
                $clienteAtualizar['customer_group_id']          = 1;
                $clienteAtualizar['customer_group_name']        = 'CLIENTE';
            }

            $clienteAtualizar['tipoPessoa']          = 'PJ';
            $clienteAtualizar['group_id']            = 3;
            $clienteAtualizar['group_name']          = 'customer';
            $clienteAtualizar['bloqueado']           = 0;
            $clienteAtualizar['data_contratacao']    = date('Y-m-d');
            $clienteAtualizar['created_by']          = $this->session->userdata('user_id');//TODO AJUSTAR vir vendedor
            $clienteAtualizar['data_alteracao']      = date('Y-m-d');

            $this->companies_model->updateCompanyWith($clienteEncontrou->id, $clienteAtualizar, []);

            $clienteModel->setId($clienteEncontrou->id);

        } else {

            $novoCliente = $this->preencherCliente($clienteModel);
            $produtoId = (int) $appModel->produto->id;

            if ($produtoId === 4) {
                $novoCliente['customer_group_id']          = 1;
                $novoCliente['customer_group_name']        = 'BASIC';
            } elseif ($produtoId === 5) {

                $novoCliente['customer_group_id']          = 3;
                $novoCliente['customer_group_name']        = 'PREMIUM';

            } elseif ($produtoId === 6) {
                $novoCliente['customer_group_id']          = 13;
                $novoCliente['customer_group_name']        = 'GOLD';
            } elseif ($produtoId === 7) {
                $novoCliente['customer_group_id']          = 18;
                $novoCliente['customer_group_name']        = 'BASIC + DOMINIO';
            } elseif ($produtoId === 8) {
                $novoCliente['customer_group_id']          = 19;
                $novoCliente['customer_group_name']        = 'PREMIUM + DOMINIO';
            } elseif ($produtoId === 9) {
                $novoCliente['customer_group_id']          = 20;
                $novoCliente['customer_group_name']        = 'GOLD + DOMINIO';
            } elseif ($produtoId === 13) {
                $novoCliente['customer_group_id']          = 21;
                $novoCliente['customer_group_name']        = 'BASIC + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 14) {
                $novoCliente['customer_group_id']          = 22;
                $novoCliente['customer_group_name']        = 'PREMIUM + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 15) {
                $novoCliente['customer_group_id']          = 23;
                $novoCliente['customer_group_name']        = 'GOLD + DOMINIO + ASSINATURA DIGITAL';
            } elseif ($produtoId === 10) {
                $novoCliente['customer_group_id']          = 24;
                $novoCliente['customer_group_name']        = 'BASIC + ASSINATURA DIGITAL';
            } elseif ($produtoId === 11) {
                $novoCliente['customer_group_id']          = 25;
                $novoCliente['customer_group_name']        = 'PREMIUM + ASSINATURA DIGITAL';
            } elseif ($produtoId === 12) {
                $novoCliente['customer_group_id']          = 26;
                $novoCliente['customer_group_name']        = 'GOLD + ASSINATURA DIGITAL';
            } else {
                $novoCliente['customer_group_id']          = 1;
                $novoCliente['customer_group_name']        = 'CLIENTE';
            }

            $novoCliente['tipoPessoa']          = 'PJ';
            $novoCliente['group_id']            = 3;
            $novoCliente['group_name']          = 'customer';
            $novoCliente['bloqueado']           = 0;
            $novoCliente['data_contratacao']    = date('Y-m-d');
            $novoCliente['created_by']          = $this->session->userdata('user_id');//TODO AJUSTAR vir vendedor

            $cid = $this->companies_model->addCompanyWith($novoCliente, []);

            $clienteModel->setId($cid);
        }

        $plugsignSettings = $this->site->getPlugsignSettings();

        if ($plugsignSettings->active || $plugsignSettings->token){
            $this->create_persons_google($clienteModel->getName(), $clienteModel->getEmail(), $clienteModel->getCf5());
        }

        return $clienteModel;
    }

    private function verificarCpfOuCnpj($documento): string
    {
        $documento = preg_replace('/\D/', '', $documento);

        if (strlen($documento) === 11) {
            return 'CPF';
        } elseif (strlen($documento) === 14) {
            return 'CNPJ';
        } else {
            return 'CPF';
        }
    }

    public function create_persons_google($name, $mail, $phone)
    {
        $ddi = '55';

        $phone = str_replace('(', '', str_replace(')', '', $phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);
        $phone = trim($ddi.$phone);

        if (strlen($phone) == 13) {

            $this->load->model('model/GooglePerson_model', 'GooglePerson_model');
            $this->load->model('repository/GooglePersonRepository_model', 'GooglePersonRepository_model');

            $googlePerson = new GooglePerson_model();

            $nameParts = explode(' ', trim($name));
            $googlePerson->familyName = array_pop($nameParts);
            $googlePerson->givenName = implode(' ', $nameParts);

            $googlePerson->phoneNumbers = $phone;
            $googlePerson->emailAddresses = $mail;

            $existPerson = $this->GooglePersonRepository_model->existPerson($phone);

            if (!$existPerson) {
                return $this->GooglePersonRepository_model->save($googlePerson);
            }
        }

        return false;
    }


    private function preencherCliente($clienteModel) {

        //$clienteModel = new Cliente_model();

        $name = $clienteModel->getName();
        $company = $clienteModel->getCompany();
        $nomeResponsavel = $clienteModel->getNomeResponsavel() != null ? $clienteModel->getNomeResponsavel() : '';

        if ($company == null) {
            $company = $name;
        }

        $name = str_replace(['&', "'"], '', $name);
        $company = str_replace(['&', "'"], '', $company);

        $data = array(

            'name'                  => mb_strtoupper($name, mb_internal_encoding()),
            'company'               => mb_strtoupper($company, mb_internal_encoding()),
            'nome_responsavel'      => mb_strtoupper($nomeResponsavel, mb_internal_encoding()),

            'email'                 => $clienteModel->getEmail() != null ? $clienteModel->getEmail() : '',
            'phone'                 => $clienteModel->getPhone() != null ?  $clienteModel->getPhone() : '',

            'data_aniversario'      => $clienteModel->getDataAniversario(),
            'tipo_documento'        => $clienteModel->getTipoDocumento(),
            'vat_no'                => $clienteModel->getVatNo(),
            'sexo'                  => $clienteModel->getSexo(),
            'telefone_emergencia'   => $clienteModel->getTelefoneEmergencia(),
            'alergia_medicamento'   => $clienteModel->getAlergiaMedicamento(),

            'postal_code'           => $clienteModel->getPostalCode(),
            'address'               => $clienteModel->getAddress() != null ? $clienteModel->getAddress() : '',
            'bairro'                => $clienteModel->getBairro(),
            'numero'                => $clienteModel->getNumero(),
            'complemento'           => $clienteModel->getComplemento(),
            'city'                  => $clienteModel->getCity() != null ? $clienteModel->getCity()  : '',
            'state'                 => $clienteModel->getState(),
            //'country'               => $this->input->post('country'),

            'cf1'                   => $clienteModel->getCf1(),//todo rg
            'cf3'                   => $clienteModel->getCf3(),//todo orgao emissor
            'cf5'                   => $clienteModel->getCf5(),//todo whatsapp

            'social_name'           => $clienteModel->getSocialName(),
            'profession'            => $clienteModel->getProfession(),
            'doenca_informar'       => $clienteModel->getDoencaInformar(),

        );

        return $data;
    }

    private function encontrouClienteCadastrado($customer, $cpf): bool
    {
        return count($customer) > 0 && $this->sma->isCPFObrigaNovoCadastroEhValidaCPFCNJ($cpf);
    }

}