<?php defined('BASEPATH') OR exit('No direct script access allowed');

class CaptacaoService_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function save($captacao): int
    {
        try {
            $this->__iniciaTransacao();

            $captacao_id  = $this->__save('captacao', $captacao->__toArray());

            $this->site->updateReference('cp');

            $this->__confirmaTransacao();

            return $captacao_id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

}