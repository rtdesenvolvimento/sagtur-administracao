<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParseContractModel_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        //dto
        $this->load->model('dto/TAGContrato_model', 'TAGContrato_model');

    }

    public function parse($sales_id, $tipo = 'text') {

        //repository
        $this->load->model('repository/AgendaViagemRespository_model','AgendaViagemRespository_model');

        $contrato_tag = array();

        $venda = $this->site->getInvoiceByID($sales_id);
        $itens = $this->site->getAllInvoiceItems($sales_id, true);
        $objClientePagante = $this->site->getCompanyByID($venda->customer_id);

        $contrato_tag = $this->addTagDadosGerais($contrato_tag);
        $contrato_tag = $this->addTagDadosVendedor($contrato_tag, $venda);

        $contrato_tag = $this->addTagVenda($contrato_tag, $venda, $itens);
        $contrato_tag = $this->addTagServico($contrato_tag, $venda, $itens);

        if ($this->pagadorNaoConstaEntreOsPassageiros($venda, $itens)) {
            $contrato_tag = $this->addTagPassageiro($contrato_tag, $objClientePagante, TAGContrato_model::PAGADOR);
        } else {
            $contrato_tag = $this->addTagPassageiro($contrato_tag, $objClientePagante);
            $contrato_tag = $this->addTagPassageiro($contrato_tag, $objClientePagante, TAGContrato_model::PAGADOR);
        }

        if ($tipo == 'text') {
            $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::TB_DEPENDENTES, $this->tb_dependentesTable($itens));
        } elseif($tipo == 'document') {
            $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::TB_DEPENDENTES, $this->tb_dependentesWord($itens));
        }

        //$this->sma->print_arrays($contrato_tag);

        return $contrato_tag;
    }

    private function tb_dependentesTable($itens): string
    {
        $html = '<table class="MsoNormalTable" border="1" cellspacing="0" cellpadding="0" width="939" style="border: none;">
        <tr>
            <td colspan="4" style="border:solid windowtext 1.0pt;padding:0cm 3.5pt 0cm 3.5pt">
                <p style="text-align:center;line-height:130%"><b>CLIENTE(S) E DEPENDENTE(S) VINCULADOS A ESTE CONTRATO</b></p>
            </td>
        </tr>
        <tr>
            <td><b>Nome</b></td>
            <td><b>DOC</b></td>
            <td><b>CPF</b></td>
            <td><b>Data Nascimento</b></td>
            <td><b>Tel</b></td>
            <td><b>Tel Emergência</b></td>
            <td><b>Email</b></td>
        </tr>';

        foreach ($itens as $item) {
            $customer = $this->site->getCompanyByID($item->customerClient);

            $html .= '<tr>
                        <td>' . htmlspecialchars($customer->name) . '</td>
                        <td>' . htmlspecialchars($customer->cf1) . '</td>
                        <td>' . htmlspecialchars($customer->vat_no) . '</td>
                        <td>' . htmlspecialchars(date('d/m/Y', strtotime($customer->data_aniversario))) . '</td>
                        <td>' . htmlspecialchars($customer->cf5) . '</td>
                        <td>' . htmlspecialchars($customer->telefone_emergencia) . '</td>
                        <td>' . htmlspecialchars($customer->email) . '</td>
                    </tr>';
        }

        // Fecha a tabela
        $html .= '</table>';

        return $html;
    }


    private function tb_dependentesWord($itens): string
    {
        $wordML = 'CLIENTE(S) E DEPENDENTE(S) VINCULADOS A ESTE CONTRATO@quebra_linha_li@@quebra_linha_li@';

        foreach ($itens as $item) {
            $customer = $this->site->getCompanyByID($item->customerClient);

            $wordML .=  'Nome: '.htmlspecialchars($customer->name).'@quebra_linha_li@';

            if ($customer->cf1) {//doc
                $wordML .=  strtoupper(htmlspecialchars($customer->tipo_documento)). ': '.htmlspecialchars($customer->cf1) . ' '. htmlspecialchars($customer->cf3) .'@quebra_linha_li@';
            }

            if ($customer->vat_no) {
                $wordML .= 'CPF: ' . htmlspecialchars($customer->vat_no) . '@quebra_linha_li@';
            }

            if ($customer->data_aniversario) {
                $wordML .= 'Data de Nascimento: ' . htmlspecialchars(date('d/m/Y', strtotime($customer->data_aniversario))) . '@quebra_linha_li@';
            }

            if ($customer->vat_no) {
                $wordML .= 'Tel: ' . htmlspecialchars($customer->cf5) . '@quebra_linha_li@';
            }

            if ($customer->telefone_emergencia) {
                $wordML .= 'Tel Emergência: ' . htmlspecialchars($customer->telefone_emergencia) . '@quebra_linha_li@';
            }

            if ($customer->email) {
                $wordML .= 'Email: ' . htmlspecialchars($customer->email) . '@quebra_linha_li@';
            }

            $wordML .= '@quebra_linha_li@';
        }

        $wordML = str_replace('@quebra_linha_li@','</w:t><w:br/><w:t>', $wordML);

        return str_replace(array("\r", "\n", "\t", '  '), '', $wordML);
    }

    private function addTagDadosVendedor($contrato_tag, $venda) {
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NOME_EMPRESA_VENDEDOR, $vendedor->company);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NOME_VENDEDOR, $vendedor->name);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::CPF_CNPJ_VENDEDOR, $vendedor->vat_no);

        return $contrato_tag;
    }

    private function addTagDadosGerais($contrato_tag) {

        setlocale(LC_ALL , 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');

        $mes =  gmstrftime('%B');

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::DIA , date('d') );
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::MES , $mes);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::MES_NUMERAL , date('m'));
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::ANO , date('Y'));
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::DATA_HOJE_EXTENSO , strftime('%d de %B de %Y',strtotime(date('Y-m-d'))));

        return $contrato_tag;
    }

    private function pagadorConstaNosItem($venda, $itens) {

        foreach ($itens as $item) {
            if ($venda->customer_id == $item->customerClient) {
                return true;
            }
        }

        return false;
    }

    private function pagadorNaoConstaEntreOsPassageiros($venda, $itens) {
        return !$this->pagadorConstaNosItem($venda, $itens);
    }

    private function addTagVenda($contrato_tag, $venda, $itens) {

        $subTotal        = $venda->grand_total;
        $subTotalExtenso = $this->sma->formatMoney($subTotal).' ('.$this->sma->extenso($subTotal).') ';

        $tipoCobranca = $this->site->getTipoCobrancaById($venda->tipoCobrancaId);
        $condicaoPagamento = $this->site->getCondicaoPagamentoById($venda->condicaopagamentoId);

        $dataPrimeiroVencimento  = $venda->previsao_pagamento;
        $totalPassageiros = count($itens);

        if($dataPrimeiroVencimento) {
            $dataPrimeiroVencimento = date('d/m/Y', strtotime($dataPrimeiroVencimento));
        }

        if ($totalPassageiros > 1)  {
            $totalPassageirosExtenso =  $totalPassageiros.' passageiros';
        } else {
            $totalPassageirosExtenso =  $totalPassageiros.' passageiro';
        }

        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::DATA_VENDA, date('d/m/Y H:i', strtotime($venda->date)));
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::CODIGO_CONTRATO, $venda->reference_no);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::TOTAL_PASSAGEIROS, $totalPassageiros);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::TOTAL_PASSAGEIROS_EXTENSO, $totalPassageirosExtenso);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::SUB_TOTAL_VENDA, $this->sma->formatMoney($subTotal));
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::SUB_TOTAL_VENDA_EXTENSO, $subTotalExtenso);

        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::TIPO_COBRANCA, $tipoCobranca->name);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::CONDICAO_PAGAMENTO,  $condicaoPagamento->name);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::DATA_PRIMEIRO_VENCIMENTO, $dataPrimeiroVencimento);
        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::OBSERVACAO_VENDA,  $this->clear_tags($venda->note) );

        $pagamentos  = $this->site->getInvoicePayments($venda->id);

        $totalPagamento = 0;
        if(!empty($pagamentos)) {
            foreach ($pagamentos as $pagamento) {
                if ($pagamento->status != 'ESTORNO') {
                    $totalPagamento = $totalPagamento + $pagamento->amount;
                }
            }
        }

        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::TOTAL_PAGAMENTO, $this->sma->formatMoney(number_format($totalPagamento, 2, '.', '')));

        return $contrato_tag;
    }

    private function addTagServico($contrato_tag, $venda, $itens) {

        if ($this->pagadorNaoConstaEntreOsPassageiros($venda, $itens)) {

            $contador = 0;

            foreach ($itens as $item) {

                if ($contador == 0) {
                    $contrato_tag = $this->addTagPassageiro($contrato_tag,  $this->site->getCompanyByID($item->customerClient));
                    $contrato_tag = $this->addTagServicoItem($contrato_tag, $item, '');//TODO ASSUME A RESPONSABILIDADE DO PRIMEIRO PASSAGEIRO
                } else {
                    $contrato_tag = $this->addTagServicoItem($contrato_tag, $item, $contador);
                }

                $contador++;
            }

        } else {

            foreach ($itens as $item) {
                if ($venda->customer_id == $item->customerClient) {
                    $contrato_tag = $this->addTagServicoItem($contrato_tag, $item);
                }
            }

            $contador = 1;

            foreach ($itens as $item) {
                if ($venda->customer_id != $item->customerClient) {
                    $contrato_tag = $this->addTagServicoItem($contrato_tag, $item, $contador);
                    $contador++;
                }
            }
        }

        return $contrato_tag;
    }

    private function addTagServicoItem($contrato_tag, $item, $contador='') {

        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);
        $produto = $this->site->getProductByID($programacao->produto);
        $category = $this->site->getCategoryById($produto->category_id);

        $dataSaida    = $programacao->dataSaida;
        $dataRetorno  = $programacao->dataRetorno;

        $datasaidaExtenso = '';
        $tipo_transporte = '';

        if($dataSaida) {
            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');
            $datasaidaExtenso =  strftime('%d de %B de %Y',strtotime($dataSaida));

            $dataSaida = date('d/m/Y', strtotime($dataSaida));
        }

        if ($dataRetorno) {
            $dataRetorno = date('d/m/Y', strtotime($dataRetorno));
        }

        if ($item->localEmbarqueId) {
            $embarque =  $this->site->getLocalEmbarqueRodoviarioById($produto->id, $item->localEmbarqueId);

            if ($embarque->dataEmbarque != null && $embarque->dataEmbarque != '0000-00-00') {

                setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
                date_default_timezone_set('America/Sao_Paulo');
                $dataSaida =  strftime('%d de %B de %Y',strtotime($embarque->dataEmbarque));

                $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_EMBARQUE.''.$contador, $dataSaida);
            } else {
                $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_EMBARQUE.''.$contador, $dataSaida);
            }

            if ($embarque->horaEmbarque) {
                $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::HORA_EMBARQUE.''.$contador, date('H:i', strtotime($embarque->horaEmbarque)));
            } else {
                $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::HORA_EMBARQUE.''.$contador, date('H:i', strtotime($programacao->horaSaida)));
            }

        } else{
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_EMBARQUE.''.$contador, $dataSaida);
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::HORA_EMBARQUE.''.$contador,date('H:i', strtotime($programacao->horaSaida)));
        }

        if ($item->tipoTransporte) {
            $tipo_transporte = $this->site->getTipoHospedagemRodoviario($produto->id, $item->tipoTransporte);
        }

        if ($item->tipoHospedagemId) {
            $tipo_hospedagem = $this->site->getTipoHospedagemID($item->tipoHospedagemId);
        }

        $nomeProduto = str_replace("&", "E",  $produto->name);

        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::TIPO_FAIXA.''.$contador, lang($item->faixaNome));
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::LOCAL_EMBARQUE.''.$contador, $item->localEmbarque);

        if (!empty($tipo_hospedagem)) {
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::TIPO_HOSPEDAGEM.''.$contador, $tipo_hospedagem->name);
        } else {
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::TIPO_HOSPEDAGEM.''.$contador, 'Não possui hospedagem');
        }

        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::NOME_VIAGEM.''.$contador, $nomeProduto);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_SAIDA_VIAGEM.''.$contador, $dataSaida);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_SAIDA_VIAGEM_EXTENSO.''.$contador, $datasaidaExtenso);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DATA_RETORNO.''.$contador, $dataRetorno);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::HORA_SAIDA.''.$contador, date('H:i', strtotime($programacao->horaSaida)));
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::HORA_RETORNO.''.$contador, date('H:i', strtotime($programacao->horaRetorno)));
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::POLTRONA_PASSAGEIRO.''.$contador, $item->poltronaClient);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::TIPO_PACOTE.''.$contador, $category->name);
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::TIPO_TRANSPORTE.''.$contador, $tipo_transporte);

        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::SOBRE_A_VIAGEM.''.$contador, $this->clear_tags($produto->product_details) );
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::O_QUE_INCLUI.''.$contador, $this->clear_tags($produto->oqueInclui) );
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::ROTEIRO.''.$contador, $this->clear_tags($produto->itinerario) );
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::VALORES_E_CONDICOES.''.$contador, $this->clear_tags($produto->valores_condicoes) );
        $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::DETALHES_GERAIS_DA_VIAGEM.''.$contador, $this->clear_tags($produto->details) );

        $contrato_tag = $this->addTag($contrato_tag,TAGContrato_model::PRECO_POR_ITEM.''.$contador, number_format($item->subtotal, 2, '.', ''));

        if ($produto->isHospedagem) {
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::IS_HOSPEDAGEM.''.$contador, 'X');
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::IS_SEM_HOSPEDAGEM.''.$contador, ' ');
        } else {
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::IS_HOSPEDAGEM.''.$contador, ' ');
            $contrato_tag = $this->addTag($contrato_tag, TAGContrato_model::IS_SEM_HOSPEDAGEM.''.$contador, 'X');
        }

        $objCliente = $this->site->getCompanyByID($item->customerClient);

        if ($contador > 0) {
            $contrato_tag = $this->addTagPassageiro($contrato_tag, $objCliente, $contador);
        }

        return $contrato_tag;
    }

    private function addTagPassageiro($contrato_tag, $objCliente, $contador ='') {

        if ($objCliente->sexo == 'FEMININO')  {
            $cumprimento = 'Srª';
        } else {
            $cumprimento = 'Srº';
        }

        $data_aniversario    = $objCliente->data_aniversario;

        if($data_aniversario) {
            $data_aniversario = date('d/m/Y', strtotime($data_aniversario));
        }

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::SEXO_PASSAGEIRO.''.$contador, $objCliente->sexo);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::CUMPRIMENTO.''.$contador, $cumprimento);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NOME_PASSAGEIRO.''.$contador, $objCliente->name);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NOME_RESPONSAVEL.''.$contador, $objCliente->nome_responsavel);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::CPF_PASSAGEIRO.''.$contador, $objCliente->vat_no);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::TIPO_DOCUMENTO.''.$contador,  strtoupper($objCliente->tipo_documento));
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::RG_PASSAGEIRO.''.$contador, $objCliente->cf1);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::ORGAO_EMISSOR_RG.''.$contador,  $objCliente->cf3);

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::ENDERECO_PASSAGEIRO.''.$contador, $objCliente->address);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::BAIRRO_PASSAGEIRO.''.$contador, $objCliente->bairro);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::COMPLEMENTO_PASSAGEIRO.''.$contador, $objCliente->complemento);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NUMERO_PASSAGEIRO.''.$contador, $objCliente->numero);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::CIDADE_PASSAGEIRO.''.$contador, $objCliente->city);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::CEP_PASSAGEIRO.''.$contador, $objCliente->postal_code);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::ESTADO_PASSAGEIRO.''.$contador, $objCliente->state);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::PAIS.''.$contador, $objCliente->country);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::NATURALIDADE_PASSAGEIRO.''.$contador, $objCliente->cf4);

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::EMAIL_PASSAGEIRO.''.$contador, $objCliente->email);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::DATA_NASCIMENTO.''.$contador, $data_aniversario);

        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::TELEFONES_PASSAGEIRO.''.$contador, ($objCliente->phone.' '.$objCliente->cf5) );
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::TELEFONE_EMERGENCIAL.''.$contador, $objCliente->telefone_emergencia);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::APENAS_TELEFONE.''.$contador, $objCliente->phone);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::APENAS_CELULAR.''.$contador, $objCliente->cf5);
        $contrato_tag = $this->addTag($contrato_tag,  TAGContrato_model::PLANO_SAUDE.''.$contador, lang($objCliente->plano_saude));

        return $contrato_tag;
    }

    private function addTag($contrato_tag, $tag, $valor) {
        $contrato_tag[$tag] = $valor;

        return $contrato_tag;
    }

    function clear_tags($str) {
        $str = $this->decode_html($str);

        $str = str_replace('</p>',' ', $str);
        $str = str_replace('<br>',' ', $str);
        $str = str_replace('<li>',' - ', $str);
        $str = str_replace('<strong>',' ', $str);
        $str = str_replace('</strong>',' ', $str);
        $str = str_replace('&','E', $str);

        $str = strip_tags($str);
        $str = trim($str);

        $str = str_replace('@quebra_linha@',' ', $str);

        return $str;

    }

    public function decode_html($str): string
    {
        return html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
    }

}