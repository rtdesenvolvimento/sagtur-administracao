<?php defined('BASEPATH') OR exit('No direct script access allowed');


class MercadoPagoException_model extends CI_Model
{
    private $payment;

    public function __construct() {
        parent::__construct();

        $this->load->model('service/log/LogMercadoPago_model', 'LogMercadoPago_model');
    }

    public function verificarTransacao($payment) {

        $this->setPayment($payment);

        if ($this->getPayment()->error == '') {

            if ($this->getPayment()->status_detail == 'accredited' ||
                $this->getPayment()->status_detail == 'pending_contingency' ||
                $this->getPayment()->status_detail == 'pending_waiting_payment' ||
                $this->getPayment()->status_detail == 'pending_waiting_transfer' ||
                $this->getPayment()->status_detail == 'pending_review_manual') {

                if ($this->getPayment()->payment_type_id == 'credit_card')  {
                    if ($this->getPayment()->status_detail == 'pending_contingency' ||
                        $this->getPayment()->status_detail == 'pending_waiting_payment' ||
                        $this->getPayment()->status_detail == 'pending_waiting_transfer' ||
                        $this->getPayment()->status_detail == 'pending_review_manual') {
                        $this->LogMercadoPago_model->warning(print_r($payment, true));

                        return array(
                            'sucesso' => false,
                            'error' => $this->getStatus(),
                            'status_detail' => $this->getStatus(),
                        );
                        
                    } else {
                        $this->LogMercadoPago_model->info(print_r($payment, true));

                        return array(
                            'sucesso' => true,
                            'mensagem' => $this->getStatus(),
                            'error' => '',
                            'status_detail' => $this->getStatus(),
                        );
                    }
                } else {

                    $this->LogMercadoPago_model->info(print_r($payment, true));

                    return array(
                        'sucesso' => true,
                        'mensagem' => $this->getStatus(),
                        'error' => '',
                        'status_detail' => $this->getStatus(),
                    );
                }
            } else {

                $this->LogMercadoPago_model->warning(print_r($payment, true));

                return array(
                    'sucesso' => false,
                    'error' => $this->getStatus(),
                    'status_detail' => $this->getStatus(),
                );
            }
        } else {
            $this->LogMercadoPago_model->error(print_r($payment, true));

            return array(
                'sucesso' => false,
                'error' => $this->getErrors(),
                'status_detail' => $this->getStatus(),
            );
        }
    }

    public function getStatus() {
        $status = [
            'accredited' => 'Pronto, seu pagamento foi aprovado!.',
            'pending_contingency' => 'Estamos processando o pagamento. Não se preocupe, em menos de 2 dias úteis informaremos por e-mail se foi creditado.',
            'pending_review_manual' => 'Estamos processando seu pagamento. Não se preocupe, em menos de 2 dias úteis informaremos por e-mail se foi creditado ou se necessitamos de mais informação.',
            'pending_waiting_transfer' => "Pendente de Pagamento.",
            'pending_waiting_payment' => "Pendente de Pagamento.",

            'cc_rejected_bad_filled_card_number' => 'Revise o número do cartão.',
            'cc_rejected_bad_filled_date' => 'Revise a data de vencimento.',
            'cc_rejected_bad_filled_other' => 'Revise os dados.',
            'cc_rejected_bad_filled_security_code' => 'Revise o código de segurança do cartão.',
            'cc_rejected_blacklist' => 'Não pudemos processar seu pagamento.',
            'cc_rejected_call_for_authorize' => 'Você deve autorizar para operadora do cartão o pagamento do valor ao Mercado Pago.',
            'cc_rejected_card_disabled' => 'Ligue para o operadora do cartão para ativar seu cartão. O telefone está no verso do seu cartão.',
            'cc_rejected_card_error' => 'Não conseguimos processar seu pagamento.',
            'cc_rejected_duplicated_payment' => 'Você já efetuou um pagamento com esse valor. Caso precise pagar novamente, utilize outro cartão ou outra forma de pagamento.',
            'cc_rejected_high_risk' => 'Seu pagamento foi recusado. Escolha outra forma de pagamento. Recomendamos meios de pagamento em dinheiro.',
            'cc_rejected_insufficient_amount' => 'O cartão possui saldo insuficiente.',
            'cc_rejected_invalid_installments' => 'O cartão não processa pagamentos em parcelas. Você atingiu o limite de tentativas permitido. Escolha outro cartão ou outra forma de pagamento.',
            'cc_rejected_other_reason' => 'cartão de crédito não processa o pagamento.',
            'cc_rejected_card_type_not_allowed' => 'O pagamento foi rejeitado porque o usuário não tem a função crédito habilitada em seu cartão multiplo (débito e crédito).',
        ];

        if (array_key_exists($this->getPayment()->status_detail, $status)) {
            return $status[$this->getPayment()->status_detail];
        } else {
            return 'Houve um problema na sua requisição tente novamente: '.$this->getPayment()->status_detail;
        }
    }

    public function getErrors() {
        $error = [
            '205' => 'Digite o número do seu cartão.',
            '208' => 'Escolha um mês.',
            '209' => 'Escolha um ano.',
            '212' => 'Informe seu documento.',
            '213' => 'Informe seu documento.',
            '214' => 'Informe seu documento.',
            '220' => 'Informe seu banco emissor.',
            '221' => 'Digite o nome e sobrenome.',
            '224' => 'Digite o código de segurança.',
            'E301' => 'Há algo de errado com esse número. Digite novamente.',
            'E302' => 'Confira o código de segurança.',
            '316' => 'Por favor, digite um nome válido.',
            '322' => 'Confira seu documento.',
            '323' => 'Confira seu documento.',
            '324' => 'Confira seu documento.',
            '325' => 'Confira a data.',
            '326' => 'Confira a data.',
            '106' => 'Não pode efetuar pagamentos a usuários de outros países.',
            '109' => 'O tipo de pagamento não processa pagamentos parcelados.',
            '126' => 'Não conseguimos processar seu pagamento.',
            '129' => 'O tipo de pagamento não processa pagamentos para o valor selecionado. Escolha outro cartão ou outra forma de pagamento.',
            '145' => 'Uma das partes com a qual está tentando realizar o pagamento é um usuário de teste e a outra é um usuário real.',
            '150' => 'Você não pode efetuar pagamentos.',
            '151' => 'Você não pode efetuar pagamentos.',
            '160' => 'Não conseguimos processar seu pagamento.',
            '204' => 'O payment_method_id não está disponível nesse momento. Escolha outro cartão ou outra forma de pagamento.',
            '801' => 'Você realizou um pagamento similar há poucos instantes. Tente novamente em alguns minutos.',
            '3034' => 'Número inválido do cartão de crédito, verifique o número digitado e tente novamente.',
            '3032' => 'Código de segurança inválido (CCV).',
            '2194' => 'Data de vencimento inválida - você está lançando uma fatura com vencimento já passado.',
            '13253' => 'Usuário do mercadopago sem chave habilitada para renderização QRCode',
            '4050 ' => 'Ocorreu um erro com os dados do cliente: Verifique se o Cliente tem E-mail ou CPF cadastrado.'
        ];

        if (array_key_exists($this->getPayment()->error->causes[0]->code, $error)) {
            return $error[$this->getPayment()->error->causes[0]->code];
        } else {
            return 'Houve um problema na sua requisição. Tente Novamente. Code '.$this->getPayment()->error->causes[0]->code.' '.$this->getPayment()->error->causes[0]->description;
        }
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param mixed $payment
     */
    public function setPayment($payment): void
    {
        $this->payment = $payment;
    }
}