<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ValepayService_model extends CI_Model
{
    const MENSAGEM_BAIXA_PAGMENTO = 'Pagamento baixado pelo sistema automaticamente pelo modulo de integração https://valepay.com.br<br/>Na data ';

    private $valepay;

    public function __construct() {
        parent::__construct();

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
    }

    public function configurarIntegracao() {
        $valePaySettings = $this->settings_model->getValepaySettings();

        if ($valePaySettings->active) {
            $this->valepay              = new Valepay();
            $this->valepay->sandbox     = $valePaySettings->sandbox;
            $this->valepay->key_public  = $valePaySettings->public_key;
            $this->valepay->key_private = $valePaySettings->private_key;
        }

        return null;
    }

    public function emitirCobranca($cobranca) {
        if ($this->isCartaoCredito($cobranca->tipoCobranca, $cobranca)) {
            $cartao_created =  $this->cartao_credito($cobranca);
            return $cartao_created;
        } else if ($this->isLinkPagamento($cobranca->tipoCobranca)) {
            $link_pagamento_created =  $this->link_pagamento_sagtur($cobranca);
            return  $link_pagamento_created;

        } else if ($this->isPix($cobranca->tipoCobranca)) {
            $pix_created =  $this->pix($cobranca);
            return $pix_created;
        }
        return [];
    }
    private function link_pagamento_sagtur($cobranca) {
        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $dtoRetornoCobranca->sucesso = true;
        $dtoRetornoCobranca->erro = '';

        $data_invoice = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'fatura'                => $cobranca->fatura,
            'tipo'                  => $cobranca->tipoCobranca,
            'integracao'            => 'valepay',
            'code'                  => $cobranca->fatura,
            'errorProcessamento'    => '',
            'status_detail'         => 'Created',
            'checkoutUrl'           => base_url() . 'cartaocredito/pagamento/' . $cobranca->fatura . '?token=' . $this->session->userdata('cnpjempresa')
        );

        $dtoRetornoCobranca->adicionarFatura($data_invoice);

        return $dtoRetornoCobranca;
    }

    private function cartao_credito($cobranca) {
        return $this->transaction_start($cobranca);
    }

    private function pix($cobranca) {

        $dataHoje = date('y-m-d H:i:s');
        $vencimento   = implode("-",array_reverse(explode("/",$cobranca->dataVencimento)));
        $date_of_expiration = date('Y-m-d H:i:s', strtotime($vencimento));
        //$date_of_expiration = date('Y-m-d H:i:s', strtotime("+10 day", strtotime($dataHoje)));
        $pessoaCobranca = $cobranca->pessoa;

        $data = array(
            'amount'            => (float) number_format ($cobranca->valorVencimento, 2, '.', '' ),
            'description'       => strtoupper($cobranca->instrucoes),
            //'reference'         => $cobranca->fatura,
            'reference'         => $pessoaCobranca->nome,
            'expiration_date'   => $date_of_expiration,
            'webhook_url'       => 'https://sistema.sagtur.com.br/infrastructure/ValePayController/notification/' .  $this->session->userdata('cnpjempresa'),
            //'webhook_url'       => 'https://sagtur.free.beeceptor.com'
        );

        $dinamyc_qrcode = $this->valepay->create_pix($data);
        $dinamyc_qrcode_valid  = $this->validate_transaction($dinamyc_qrcode);

        if ($dinamyc_qrcode_valid->status) {

            $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = '';

            $data_invoice = array(
                'status'                => Financeiro_model::STATUS_ABERTA,
                'fatura'                => $cobranca->fatura,
                'tipo'                  => $cobranca->tipoCobranca,
                'integracao'            => 'valepay',
                'errorProcessamento'    => '',
                'status_detail'         => 'Created',
                'code'                  => 'pay_'.$dinamyc_qrcode->data->transaction_uuid,
                'log'                   => json_encode($dinamyc_qrcode),
            );

            $qrimage = $this->sma->qrcode_pdf_sales('text',$dinamyc_qrcode->data->qrcode ,5 , FALSE, '_'.time().'_'.$this->session->userdata('cnpjempresa'));

            $qrCode                            = $dinamyc_qrcode->data->qrcode;
            $data_invoice['qr_code_base64']    = $qrimage;
            $data_invoice['qr_code']           = $qrCode;
            $data_invoice['checkoutUrl']       = base_url().'pix/pagamento/pay_'.$dinamyc_qrcode->data->transaction_uuid.'?token='.$this->session->userdata('cnpjempresa');

            $dtoRetornoCobranca->adicionarFatura($data_invoice);

            return $dtoRetornoCobranca;
        } else {
            return $this->handler_error($dinamyc_qrcode_valid);
        }
    }

    private function getProduto($pgramacaoID) {

        $programacao    = $this->AgendaViagemService_model->getProgramacaoById($pgramacaoID);
        $produto        = $this->site->getProductByID($programacao->produto);

        if ($produto->isTaxasComissao) {
            return $produto->id;
        }

        return false;
    }

    private function transaction_start($cobranca) {

        $fatura                         = $this->financeiro_model->getFaturaById($cobranca->fatura);
        $isTaxasComissaoProductID       = $this->getProduto($fatura->programacaoId);

        if ($isTaxasComissaoProductID) {
            $configurcaoTaxa    = $this->settings_model->getTaxaConfiguracaoSettings($fatura->tipoCobranca, FALSE, $isTaxasComissaoProductID);
        } else {
            $configurcaoTaxa    = $this->settings_model->getTaxaConfiguracaoSettings($fatura->tipoCobranca);
        }

        $numero_max_parcelas_sem_juros  = $configurcaoTaxa->numero_max_parcelas_sem_juros;

        $transaction_int = array(
            'amount'            => (float) number_format ($cobranca->valorVencimento, 2, '.', '' ),
            'markup_type'       => '1',//1 - Multiplo 2-Divisor
            'markup'            => '0',//Porcentagem do markup
            'calculate_markup'  => FALSE,
            'entry_money'       => 0,//valor da entrada
            'installments'      => $cobranca->numeroParcelas,//quantidade de parcelas
            'description'       => strtoupper($cobranca->instrucoes),
            'webhook_url'       => 'https://sistema.sagtur.com.br/infrastructure/ValePayController/notification/' .  $this->session->userdata('cnpjempresa'),
        );

        if ($cobranca->numeroParcelas <= $numero_max_parcelas_sem_juros) {
            $transaction_int['product_id'] = 3;//1 - valepay 2-Lojista 3-Rav
        } else {
            $transaction_int['product_id'] = 1;//1 - valepay 2-Lojista 3-Rav
        }

        $transaction_start = $this->valepay->transaction_start($transaction_int);
        $transaction_start_valid  = $this->validate_transaction($transaction_start);

        if ($transaction_start_valid->status) {
            return $this->create_client_transaction($cobranca, $transaction_start->data->uuid);
        } else {
            return $this->handler_error($transaction_start_valid);
        }
    }

    private function create_client_transaction($cobranca, $transaction_uuid) {
        $pessoaCobranca = $cobranca->pessoa;

        $cpf        =  $pessoaCobranca->cpfCnpj;
        $celular    = $pessoaCobranca->celular;

        if ($cobranca->cpfTitularCartao) $cpf = $cobranca->cpfTitularCartao;
        if ($cobranca->celularTitularCartao) $celular = $cobranca->celularTitularCartao;

        $data_client = array(
            'transaction_uuid' => $transaction_uuid,
            'customer' => array(
                'name' => $pessoaCobranca->nome,
                'document' =>  $cpf,
                'tax_id' => $pessoaCobranca->rg,
                'mother_name' => '',//TODO NOME DA MAE
                'phone' => $pessoaCobranca->telefone,
                'cellphone' => $celular,
                'gender' => ($pessoaCobranca->sexo == 'MASCULINO' ? 'M' : 'F'),
                'birth_date' => $pessoaCobranca->dataNascimento,
                'email' => $pessoaCobranca->email,
                'address' => array(
                    'zipcode' => $cobranca->cepTitularCartao,
                    'address' => $cobranca->enderecoTitularCartao,
                    'number' => $cobranca->numeroEnderecoTitularCartao != null ? $cobranca->numeroEnderecoTitularCartao  : 'SN',
                    'complement' => $cobranca->complementoEnderecoTitularCartao,
                    'neighborhood' => $cobranca->bairroTitularCartao,
                    'city' => $cobranca->cidadeTitularCartao,
                    'state' => $cobranca->estadoTitularCartao,
                ),
            ),
        );

        $client_created         = $this->valepay->create_client_transaction($data_client);
        $client_created_valid   = $this->validate_transaction($client_created);

        if ($client_created_valid->status) {
            return $this->tokenizing_credit_card($cobranca, $transaction_uuid);
        } else {
            return $this->handler_process_error($cobranca, $client_created_valid);
        }
    }

    public function tokenizing_credit_card($cobranca, $transaction_uuid) {

        $cradNumber = str_replace(' ', '', $cobranca->cardNumber);

        $token_card = array(
            'transaction_uuid' => $transaction_uuid,
            'cardNumber' => $cradNumber,
        );

        $card_tokenized = $this->valepay->tokenizing_credit_card($token_card);
        $card_tokenized_valid   = $this->validate_transaction($card_tokenized);

        if ($card_tokenized_valid->status) {
            return $this->payment_credit_card($cobranca, $transaction_uuid, $card_tokenized);
        } else {
            return $this->handler_process_error($cobranca, $card_tokenized_valid);
        }
    }

    private function payment_credit_card($cobranca, $transaction_uuid, $card_tokenized) {
        $payment = array(
            'transaction_uuid' => $transaction_uuid,
            'paymentInfo' => array(
                'token' => $card_tokenized->data->token,
                'securityCode' => $cobranca->cvc,
                'expirationMonth' => $cobranca->cardExpiryMonth,
                'expirationYear' => $cobranca->cardExpiryYear,
                'cardholderName' => $cobranca->cardName,
                'ip_address' => $this->input->ip_address(),
                'paymentMethodId' => $card_tokenized->data->paymentMethodID,
                'soft_description' => substr($this->session->userdata('cnpjempresa'), 0, 22),
            ),
        );

        $payment_created        = $this->valepay->payment_credit_card($payment);
        $payment_created_valid  = $this->validate_transaction($payment_created);

        if ($payment_created_valid->status) {
            return  $this->handle_transaction_payment($cobranca, $payment_created, $card_tokenized->data->paymentMethodID);
        } else {
            return $this->handler_process_error($cobranca, $payment_created_valid);
        }
    }

    public function cancelarCobranca($code) {
        if (strpos($code, 'pay_') !== false) {
            $transaction_uuid = str_replace('pay_', '', $code);
            $this->valepay->transaction_cancel($transaction_uuid);
        }
    }

    private function handle_transaction_payment($cobranca, $payment_created, $operadora_cartao) {

        $dtoRetornoCobranca = $this->handle_transaction_payment_errors($payment_created);

        if ($dtoRetornoCobranca->sucesso) {
            if ($dtoRetornoCobranca->erro) {
                $dtoRetornoCobranca->adicionarFatura($this->write_invoice_error_process($cobranca, $payment_created, $dtoRetornoCobranca->erro));
            } else {
                $dtoRetornoCobranca->adicionarFatura($this->write_invoice($cobranca, $payment_created, $operadora_cartao));
            }
        } else {
            $dtoRetornoCobranca->adicionarFatura($this->write_invoice_error_process($cobranca, $payment_created));
        }

        return $dtoRetornoCobranca;
    }

    private function handler_process_error($cobranca, $transaction) {

        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $dtoRetornoCobranca->sucesso = true;

        if (!$cobranca->venda_manual) $dtoRetornoCobranca->erro = $transaction->error;

        $dtoRetornoCobranca->adicionarFatura($this->write_invoice_error_process($cobranca, $transaction));

        return $dtoRetornoCobranca;
    }
    private function handle_transaction_payment_errors($transaction) {

        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $status_payment = $transaction->data->status->status;

        if ($status_payment == 'APPROVED') {
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = '';
        } else if ($status_payment == 'CANCELED') {
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = $transaction->data->authorization;//TODO ERRO AO PROCESSAR O PAGAMENTO
        } else {
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = '';
        }
        return $dtoRetornoCobranca;
    }

    private function write_invoice_error_process($cobranca, $transaction, $error = '') {
        $data_invoice = array(
            'status'    => Financeiro_model::STATUS_ABERTA,
            'fatura'    => $cobranca->fatura,
            'tipo'      => $cobranca->tipoCobranca,
            'integracao'=> 'valepay',
            'code'      => $cobranca->fatura,
            'checkoutUrl'           => base_url() . 'cartaocredito/pagamento/' . $cobranca->fatura . '?token=' . $this->session->userdata('cnpjempresa'),
            'log'                   => json_encode($transaction),
        );

        if (!$cobranca->venda_manual) {
            $data_invoice['errorProcessamento'] = $this->getError($transaction->error);
            $data_invoice['status_detail']      = 'Error';
        } else {
            $data_invoice['errorProcessamento'] = '';
            $data_invoice['status_detail']      = 'Created';
        }

        if ($error) {
            $data_invoice['errorProcessamento'] = $this->getError($error);
            $data_invoice['status_detail']      = 'Error';
        }

        return $data_invoice;
    }

    private function getError($error) {
        //traducao dos erros conforme tabela

        return $error;
    }

    private function write_invoice($cobranca, $transaction, $operadora_cartao) {
        $data_invoice = array(
            'status' => Financeiro_model::STATUS_ABERTA,
            'fatura' => $cobranca->fatura,
            'tipo' => $cobranca->tipoCobranca,
            'integracao' => 'valepay',
            'status_detail' => 'created',
            'dueDate' => $transaction->data->payment_date != null ? $transaction->data->payment_date : date('Y-m-d'),
            'code' => $transaction->data->uuid,
            'code_sale' => $transaction->data->code_sale,
            'installments' => $transaction->data->installments,
            'operadora_cartao' => $operadora_cartao,
            'valor_financiado' => $transaction->data->total_amount,
            'log' => json_encode($transaction),
        );

        return $data_invoice;
    }

    private function handler_error($transaction) {
        $dtoRetornoCobranca = new RetornoCobrancaDTO_model();
        $dtoRetornoCobranca->data = [];
        $dtoRetornoCobranca->sucesso = false;
        $dtoRetornoCobranca->erro = $transaction->error;

        return $dtoRetornoCobranca;
    }

    public function validate_transaction($transaction) {

        $response = new stdClass();

        $error = false;
        $status = false;

        if ($transaction->status == 'error') {
            if(!empty($transaction->message)) $error = $transaction->message;
        } else if ($transaction->status == 'success' && !empty($transaction->data)) {
            $status = true;
        }

        $response->status = $status;
        $response->error = $error;

        return $response;
    }

    public function buscarPagamento($code) {
        if (strpos($code, 'pay_') !== false) {
            return $this->baixarPagamentoIntegracao($code);//pagamento baixado via integracao apenas para pix
        } else {
            return $this->baixarPagamentoRealTime($code); //pagamento na hora
        }
    }

    public function baixarPagamentoIntegracao($code) {

        $this->configurarIntegracao();

        $cobrancaSistema = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($code);

        $fatura_model = $this->financeiro_model->getFaturaById($cobrancaSistema->fatura);

        $invoice = json_decode($cobrancaSistema->log);

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = 'pay_'.$invoice->uuid;
        $fatura->reference = $cobrancaSistema->fatura;

        $fatura->valorVencimento    = $invoice->total_amount;
        $fatura->dtVencimento       = $invoice->payment_date;
        $fatura->log                = $this->sma->tirarAcentos(print_r($invoice, true));
        $baixaCobranca->adicionarFatura($fatura);

        $status = $invoice->status;

        if ($status == 'CANCELED') $status = 'CONTINUE';

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $pagamento->id              = $invoice->uuid;
        $pagamento->status          = $invoice->status;
        $pagamento->dataPagamento   = $invoice->payment_date;
        $pagamento->formaPagamento  = $invoice->payment_method;

        $pagamento->taxa            = ($invoice->total_amount - $fatura_model->valorpagar);
        $pagamento->valorPago       = $invoice->total_amount;

        $pagamento->observacao      = ValepayService_model::MENSAGEM_BAIXA_PAGMENTO . ' ' .
            date('d/m/Y H:i:s')
            .' <br/>Total do pagamento '. $invoice->total_amount.
            ' <br/> Código da Venda no ValePay: '.$invoice->code_sale.
            '<br/> Código da Transação pay_'.$invoice->uuid;

        $fatura->adicionarPagamento($pagamento);

        return $baixaCobranca;
    }

    public function baixarPagamentoRealTime($code) {
        $this->configurarIntegracao();

        $cobrancaSistema                = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($code);
        $fatura                         = $this->financeiro_model->getFaturaById($cobrancaSistema->fatura);
        $isTaxasComissaoProductID       = $this->getProduto($fatura->programacaoId);

        if ($isTaxasComissaoProductID) {
            $configurcaoTaxa    = $this->settings_model->getTaxaConfiguracaoSettings($fatura->tipoCobranca, FALSE, $isTaxasComissaoProductID);
        } else {
            $configurcaoTaxa    = $this->settings_model->getTaxaConfiguracaoSettings($fatura->tipoCobranca);
        }

        $numero_max_parcelas_sem_juros  = $configurcaoTaxa->numero_max_parcelas_sem_juros;

        $invoice = json_decode($cobrancaSistema->log);

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca->sucesso = true;

        $fatura = new BaixaFaturaCobrancaDTO_model();
        $fatura->code = $invoice->data->uuid;
        $fatura->reference = $cobrancaSistema->fatura;

        $fatura->valorVencimento    = $invoice->data->amount_return;
        $fatura->dtVencimento       = $invoice->data->payment_date;
        $fatura->log                = $this->sma->tirarAcentos(print_r($invoice, true));
        $baixaCobranca->adicionarFatura($fatura);

        $status = $invoice->data->status->status;

        if ($status == 'CANCELED') $status = 'CONTINUE';

        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $pagamento->id              = $invoice->data->uuid;
        $pagamento->status          = $invoice->data->status->status;
        $pagamento->dataPagamento   = $invoice->data->payment_date;
        $pagamento->formaPagamento  = $invoice->payment_method;

        if ($invoice->data->installments <= $numero_max_parcelas_sem_juros) {
            $pagamento->taxa            = ($invoice->data->total_amount - $invoice->data->amount_return);
            $pagamento->valorPago       = $invoice->data->total_amount;
        } else {
            $pagamento->taxa            = 0;
            $pagamento->valorPago       = $invoice->data->amount_return;
        }

        $pagamento->observacao      = ValepayService_model::MENSAGEM_BAIXA_PAGMENTO . ' ' . date('d/m/Y H:i:s') .' <br/> Parcelamento: '. $invoice->data->installments.'X de ' . $this->sma->formatMoney($invoice->data->amount_instalment) .  ' por parcela no total de ' . $this->sma->formatMoney($invoice->data->total_amount) . ' <br/> Operadora do cartão: ' . strtoupper($cobrancaSistema->operadora_cartao). ' <br/> Código de Autorização: '.$invoice->data->authorization. ' <br/>Exibido na Fatura: '.$invoice->data->sof_description;
        $fatura->adicionarPagamento($pagamento);

        return $baixaCobranca;
    }

    private function isCartaoCredito($tipo, $cobranca) {
        if ($tipo == 'cartao_credito_transparent_valepay') {
            return true;
        } else if ($this->isProcessaPagamentoViaLink($tipo, $cobranca)) {
            return true;
        }
        return false;
    }

    private function isProcessaPagamentoViaLink($tipo, $cobranca) {
        return $this->isLinkPagamento($tipo) && $cobranca->processa_pagamento_link;
    }

    private function isLinkPagamento($tipo) {
        if ($tipo == 'link_pagamento') return true;
        return false;
    }

    private function isPix($tipo) {
        if ($tipo == 'pix') return true;
        return false;
    }

}