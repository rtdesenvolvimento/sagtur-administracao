<?php defined('BASEPATH') OR exit('No direct script access allowed');

class SendMessageService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('whatsapp');

        //tokens do telefone da satgtur
        $this->whatsapp->instance       = '3D7ACA0042A4A0E77763F69CB7A0D002';
        $this->whatsapp->token          = 'D2A7B1790022B7F512BC5062';
        $this->whatsapp->client_token   = 'Fe908c5f7c5d243ba843f275b8d99af45S';

        //service
        $this->load->model('service/MessageGroupService_model', 'MessageGroupService_model');

        //repository
        $this->load->model('repository/MessageGroupRepository_model', 'MessageGroupRepository_model');
        $this->load->model('repository/GooglePersonRepository_model', 'GooglePersonRepository_model');

        //model
        $this->load->model('model/GooglePerson_model', 'GooglePerson_model');
    }

    public function send_text($message, $phone)
    {
        return $this->whatsapp->send_text($message, $phone);;
    }

    public function send_link($message, $linkUrl, $title, $linkDescription, $image,  $phone)
    {
        return $this->whatsapp->send_link($message, $linkUrl, $title, $linkDescription, $image,  $phone);;
    }

    public function send_document_pdf($document_link, $fileName,  $phone)
    {
        return $this->whatsapp->send_document_pdf($document_link, $fileName,  $phone);;
    }

    public function create_group($name_group, $name_admin, $phones = [], $programacaoID = null)
    {
        $response = $this->whatsapp->create_group($name_group, $phones);

        $group = array(
            'name' => $name_group,
            'name_phone' => $response->phone,
            'invitationLink' => $response->invitationLink,
            'programacao_id' => $programacaoID,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('user_id')
        );

        $group_id = $this->MessageGroupService_model->save($group);

        foreach ($phones as $phone) {
            $participant = array(
                'contact' => $name_admin,
                'phone' => $phone,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('user_id'),
                'administrador' => 1
            );
            $this->MessageGroupService_model->save_participant($participant, $group_id);
        }

        return $group_id;
    }

    /**
     * @throws Exception
     */
    public function add_participant_group($groupId, $phones = [])
    {
        $group = $this->MessageGroupRepository_model->getById($groupId);

        if ($group->name_phone == null) {
            throw new Exception('Grupo não encontrado no whatsapp');
        }

        $adicionado = $this->whatsapp->add_participant_group($group->name_phone, $phones);

        if ($adicionado) {
            foreach ($phones as $phone) {

                $inGroup = $this->check_member_group($groupId, $phone);
                $existParticipant = $this->MessageGroupRepository_model->getParticipant($groupId, $phone);

                $participant = array(
                    'contact' => $phone,
                    'phone' => $phone,
                    'created_at' => date('Y-m-d H:i:s'),
                    'administrador' => 0,
                    'created_by' => $this->session->userdata('user_id')
                );

                if ($inGroup) {
                    if (!$existParticipant) {
                        $this->MessageGroupService_model->save_participant($participant, $groupId);
                    }
                } else {
                    if (!$existParticipant) {
                        $this->MessageGroupService_model->save_participant_invitation($participant, $groupId);//todo enviar convite, contato bloqueado para add group
                    }
                }

            }
        }

        return $adicionado;
    }

    public function remove_participant_group($groupId, $phones = [])
    {

        $group = $this->MessageGroupRepository_model->getById($groupId);

        if ($group->name_phone == null) {
            throw new Exception('Grupo não encontrado no whatsapp');
        }

        $removido = $this->whatsapp->remove_participant_group($group->name_phone, $phones);

        if ($removido) {
            foreach ($phones as $phone) {

                $participant = $this->MessageGroupRepository_model->getParticipant($groupId, $phone);

                if ($participant) {
                    $this->MessageGroupRepository_model->delete_participant($participant->id);
                }

            }
        }

        return $removido;
    }

    public function addAdmin($groupId, $name, $phone)
    {
        $group = $this->MessageGroupRepository_model->getById($groupId);

        if ($group->name_phone == null) {
            throw new Exception('Grupo não encontrado no whatsapp');
        }

        $participant = $this->MessageGroupRepository_model->existParticipant($groupId, $phone);

        if (!$participant) {
            $participant = array(
                'contact' => $name,
                'phone' => $phone,
                'administrador' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('user_id')
            );
            return $this->MessageGroupService_model->save_participant($participant, $groupId);
        }

        return false;
    }

    public function add_admin_group($groupId, $phones = [])
    {
        $group = $this->MessageGroupRepository_model->getById($groupId);

        if ($group->name_phone == null) {
            throw new Exception('Grupo não encontrado no whatsapp');
        }

        $admin = $this->whatsapp->add_admin_group($group->name_phone, $phones);

        if ($admin) {
            foreach ($phones as $phone) {

                $participant = $this->MessageGroupRepository_model->getById($groupId, $phone);

                if ($participant) {
                    $this->MessageGroupRepository_model->add_admin_group($participant->id);
                }
            }
        }
    }

    public function group_metadata($groupId)
    {

        try {
            $group = $this->MessageGroupRepository_model->getById($groupId);

            if ($group->name_phone == null) {
                throw new Exception('Grupo não encontrado no whatsapp');
            }

            return $this->whatsapp->group_metadata($group->name_phone);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @throws Exception
     */
    public function check_member_group($groupId, $phone): bool
    {
        $metadata = $this->group_metadata($groupId);

        $normalizedPhone = $this->normalize_phone($phone);

        foreach ($metadata->participants as $participant) {

            $normalizedParticipantPhone = $this->normalize_phone($participant->phone);

            if ($normalizedParticipantPhone == $normalizedPhone) {
                return true;
            }

        }
        return false;
    }

    public function check_member_group_memory($phone, $metadata): bool
    {
        $normalizedPhone = $this->normalize_phone($phone);

        if (!empty($metadata->participants)) {
            foreach ($metadata->participants as $participant) {

                $normalizedParticipantPhone = $this->normalize_phone($participant->phone);

                if ($normalizedParticipantPhone == $normalizedPhone) {
                    return true;
                }
            }
        }
        return false;
    }

    private function normalize_phone($phone): string
    {
        $phone = preg_replace('/\D/', '', $phone);

        if (strlen($phone) > 11 && substr($phone, 0, 2) == '55') {
            $phone = substr($phone, 2);
        }

        if (strlen($phone) == 11 && substr($phone, 2, 1) == '9') {
            $phone = substr($phone, 0, 2) . substr($phone, 3);
        }

        return $phone;
    }

    /**
     * @throws Exception
     */
    public function update_group_participants($groupId, $sales, $admins)
    {

        $allContactsWhatsApp = $this->get_contacts();
        $metadata = $this->group_metadata($groupId);
        $total_customers = 0;
        $imported_google = 0;
        $imported_whatsapp = 0;
        $total_customers_whatsapp = 0;

        foreach ($sales as $sale) {

            $customer = $this->site->getCompanyByID($sale->customerClient);

            if ($customer->cf5 == '' && $customer->phone == '') {
                continue;
            }

            $phone = $customer->cf5 != '' ? $customer->cf5 : $customer->phone;
            $total_customers++;

            $ddi = '55';

            $phone = str_replace('(', '', str_replace(')', '', $phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($ddi.$phone);

            $participant = $this->MessageGroupRepository_model->existParticipant($groupId, $phone);
            $isContactWhatsApp = $this->SendMessageService_model->exist_contact($phone, $allContactsWhatsApp);

            if ($isContactWhatsApp) {
                $imported_google++;
                $imported_whatsapp++;
            } else {
                $personImportedGoogle = $this->GooglePersonRepository_model->personImported($phone);
                if ($personImportedGoogle) {
                    $imported_google++;
                }
            }

            if ($participant) {

                $inGroup = $this->check_member_group_memory($phone, $metadata);

                if ($inGroup) {
                    $total_customers_whatsapp++;
                    $this->MessageGroupService_model->update_participant(array('active' => 1, 'invitation' => 0), $participant->id);
                } elseif (!$participant->invitation) {
                    $this->MessageGroupService_model->update_participant(array('active' => 0), $participant->id);
                }

            }

            $this->create_persons_google($customer->name, $customer->email, $phone);
        }

        foreach ($admins as $admin) {

            $phone = $admin->phone;

            $phone = str_replace('(', '', str_replace(')', '', $phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($phone);

            $participant = $this->MessageGroupRepository_model->existParticipant($groupId, $phone);

            if ($participant) {

                $inGroup = $this->check_member_group_memory($phone, $metadata);

                if ($inGroup) {
                    $this->MessageGroupService_model->update_participant(array('active' => 1, 'invitation' => 0), $participant->id);
                } elseif (!$participant->invitation) {
                    $this->MessageGroupService_model->update_participant(array('active' => 0), $participant->id);
                }
            }

            $this->create_persons_google($admin->name, $admin->email, $phone);
        }

        $data_group = array(
            'total_customers' => $total_customers,
            'imported_google' => $imported_google,
            'imported_whatsapp' => $imported_whatsapp,
            'total_customers_whatsapp' => $total_customers_whatsapp
        );

        $this->MessageGroupService_model->update_group($groupId, $data_group);
    }

    public function create_persons_google($name, $mail, $phone)
    {

        if (strlen($phone) == 13) {

            $googlePerson = new GooglePerson_model();

            $nameParts = explode(' ', trim($name));
            $googlePerson->familyName = array_pop($nameParts);
            $googlePerson->givenName = implode(' ', $nameParts);

            $googlePerson->phoneNumbers = $phone;
            $googlePerson->emailAddresses = $mail;

            $existPerson = $this->GooglePersonRepository_model->existPerson($phone);

            if (!$existPerson) {
                return $this->GooglePersonRepository_model->save($googlePerson);
            }
        }

        return false;
    }

    public function queue()
    {
        $response = $this->whatsapp->queue();
        return $response;
    }

    public function get_contact($phone)
    {
        return $this->whatsapp->get_contact($phone);
    }

    public function get_contacts()
    {
        return $this->whatsapp->get_contacts();
    }

    public function exist_contact($phone, $contacts = null): bool
    {
        $normalizedPhone = $this->normalize_phone($phone);

        if ($contacts == null) {
            $contacts = $this->get_contacts();
        }

        foreach ($contacts->contacts as $contact) {
            $normalizedParticipantPhone = $this->normalize_phone($contact['phone']);
            if ($normalizedParticipantPhone== $normalizedPhone) {
                return true;
            }
        }
        return false;
    }

    public function enviar_convite($participantID)
    {
        $participant = $this->MessageGroupRepository_model->getParticipantByID($participantID);
        $group = $this->MessageGroupRepository_model->getById($participant->message_group_id);
        $biller = $this->site->getCompanyByID($this->Settings->default_biller_contract);

        $invitationLink = $group->invitationLink;
        $group_name = $this->sma->removeSpecialCharacters($group->name);
        $image = base_url() . 'assets/uploads/logos/'.$biller->logo;

        $message = 'Olá, somos da empresa *'. $this->Settings->site_name .'* 🌍✨
                    %0AEstamos nos preparando para nossa evento e criamos um grupo exclusivo no WhatsApp para compartilhar informações importantes, dicas e comunicados.
                    %0AClique aqui para entrar no grupo:
                    %0A👉 '. $invitationLink .'
                    %0AQualquer dúvida, estamos à disposição. Será um prazer viajar com você!
                    %0AAlguma dúvida entre em contato:%0ATelefone: ' . $biller->phone.'
                    %0AAtenciosamente,%0A*'. $biller->name.'*%0A*'. $this->Settings->site_name.'*';

        $message .= '%0A%0A*[MENSAGEM AUTOMÁTICA: NÃO RESPONDEMOS NESTE NÚMERO.]*';

        $message = urldecode($message);

        $this->send_link($message, $invitationLink, $group_name, $group_name, $image,  $participant->phone);

    }

    /**
     * @throws Exception
     */
    public function remove_all_participants($groupId)
    {
        $participants = $this->MessageGroupRepository_model->getAllParticipantsActive($groupId);

        foreach ($participants as $participant) {
            if ($participant->active) {
                $phones[] = $participant->phone;
            }
        }
        return $this->remove_participant_group($groupId,$phones);
    }

    /**
     * @throws Exception
     */
    public function add_all_participants($groupID, $sales, $admins)
    {

        $allContactsWhatsApp = $this->SendMessageService_model->get_contacts();
        $metadata = $this->SendMessageService_model->group_metadata($groupID);

        foreach ($sales as $sale) {

            $customer = $this->site->getCompanyByID($sale->customerClient);

            if ($customer->cf5 == '' && $customer->phone == '') {
                continue;
            }

            $phone = $customer->cf5 != '' ? $customer->cf5 : $customer->phone;

            $ddi = '55';

            $phone = str_replace('(', '', str_replace(')', '', $phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($ddi.$phone);

            $isContactWhatsApp = $this->SendMessageService_model->exist_contact($phone, $allContactsWhatsApp);
            $personImportedGoogle = $isContactWhatsApp || $this->GooglePersonRepository_model->personImported($phone);

            if ($isContactWhatsApp && $personImportedGoogle) {

                $participant = $this->MessageGroupRepository_model->existParticipant($groupID, $phone);

                if ($participant) {
                    $inGroup = $this->check_member_group_memory($phone, $metadata);
                    if (!$inGroup && !$participant->invitation) {
                        $phones[] = $phone;
                    }
                } else {
                    $phones[] = $phone;
                }
            }
        }

        foreach ($admins as $admin) {

            $phone = $admin->phone;

            $phone = str_replace('(', '', str_replace(')', '', $phone));
            $phone = str_replace('-', '', $phone);
            $phone = str_replace(' ', '', $phone);
            $phone = trim($phone);

            $isContactWhatsApp = $this->SendMessageService_model->exist_contact($phone, $allContactsWhatsApp);
            $personImportedGoogle = $isContactWhatsApp || $this->GooglePersonRepository_model->personImported($phone);

            if ($isContactWhatsApp && $personImportedGoogle) {

                $participant = $this->MessageGroupRepository_model->existParticipant($groupID, $phone);

                if ($participant) {
                    $inGroup = $this->check_member_group_memory($phone, $metadata);
                    if (!$inGroup && !$participant->invitation) {
                        $phones[] = $phone;
                    }
                } else {
                    $phones[] = $phone;
                }
            }
        }

        return $this->add_participant_group($groupID,$phones);
    }
}