<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use Ramsey\Uuid\Uuid;

class CartaocreditoService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('service/exeption/MercadoPagoException_model', 'MercadoPagoException_model');
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');
        $this->load->model('service/ValepayService_model', 'ValepayService_model');

        $this->load->model('dto/PessoaCobrancaDTO_model', 'PessoaCobrancaDTO_model');
        $this->load->model('dto/CobrancaDTO_model', 'CobrancaDTO_model');

        $this->load->model('financeiro_model');
        $this->load->model('settings_model');
        $this->load->model('sales_model');
        $this->load->model('companies_model');
    }

    function processar_pagamento($code) {

        $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($code);
        $fatura = $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);

        if ($cobrancaFatura->integracao == 'mercadopago') {
            $this->processar_pagamento_mercado_pago($cobrancaFatura, $fatura);
        } else if ($cobrancaFatura->integracao == 'valepay') {
            $this->processar_pagamento_vale_pay($cobrancaFatura, $fatura);
        }

        return $this->__getById('fatura_cobranca', $cobrancaFatura->id);
    }

    private function processar_pagamento_vale_pay($cobrancaFatura, $fatura) {

        $this->ValepayService_model->configurarIntegracao();

        $transaction_payment    = $this->ValepayService_model->emitirCobranca($this->preencherCobrancaValePay($fatura));
        $parcelasFatura         = $this->financeiro_model->getParcelaFaturaByFatura($fatura->id);

        foreach ($parcelasFatura as $parcelaFatura) {
            $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
            foreach ($transaction_payment->datas as $data) {
                $this->adicionaVinculoCobrancaFatura($data, $parcela, $cobrancaFatura);
            }
        }

        return $cobrancaFatura;
    }

    private function adicionaVinculoCobrancaFatura($data, $parcela, $cobrancaFatura) {

        if ($data['fatura'] == NULL) return false;

        $this->__edit('fatura', array('numero_transacao'=> $data['code']), 'id', $data['fatura']);
        $this->__edit('fatura_cobranca', $data, 'id', $cobrancaFatura->id);

        $contaReceber  = $this->__getById('conta_receber',  $parcela->contareceberId);

        $tipo = $data['tipo'];

        if ($data['tipo'] == 'boleto') {
            $tipo = 'Boleto';
        } else if ($data['tipo'] == 'carne') {
            $tipo = 'Carnê';
        } else if ($data['tipo'] == 'carne_cartao') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'carne_cartao_transparent') {
            $tipo = 'Cartão de Crédito Transparente';
        } else if ($data['tipo'] == 'carne_cartao_transparent_mercado_pago') {
            $tipo = 'Cartão de Crédito Transparente';
        } else if ($data['tipo'] == 'cartao_credito_transparent_valepay') {
            $tipo = 'Cartão de Crédito Transparente';
        }  else if ($data['tipo'] == 'link_pagamento') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'pix') {
            $tipo = 'PIX';
        } else if ($data['tipo'] == 'boleto_pix') {
            $tipo = 'BOLETO PIX';
        } else if ($data['tipo'] == 'cartao_credito_link') {
            $tipo = 'Link de Pagamento';
        } else if ($data['tipo'] == 'cartao_credito_link') {
            $tipo = 'Mensalidade';
        }

        $event = "Criação da Integração de Pagamento: ".strtoupper($data['integracao']).' com '.strtoupper($tipo);

        if ($data['errorProcessamento']) {
            $event .= '<br/>Erro: '.$data['errorProcessamento'];
        }

        $this->SaleEventService_model->transacao_pagamento($contaReceber->sale, $event, $data['status_detail']);

        if ($this->db->affected_rows() == '1') return true;
        return FALSE;
    }

    private function getParcelaByFatura($fatura) {
        $parcelasFatura = $this->financeiro_model->getParcelaFaturaByFatura($fatura->id);
        $parcela = null;

        foreach ($parcelasFatura as $parcelaFatura) {
            $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
        }

        return $parcela;
    }
    private function preencherCobrancaValePay($fatura) {
        $cobranca = new CobrancaDTO_model();

        $parcela        = $this->getParcelaByFatura($fatura);
        $tipoCobranca   = $this->financeiro_model->getTipoCobrancaById($parcela->tipocobranca);
        $pessoa         = $this->companies_model->getCompanyByID($fatura->pessoa);
        $numeroParcelas = $this->input->post('parcelamento_valepay');
        $instrucoes     = $this->escreverInstrucoesCobranca($parcela, $fatura, $tipoCobranca, $pessoa, $numeroParcelas);
        $email          = $this->input->post('cardholderEmail');

        //set
        $cobranca->fatura           = $fatura->id;
        $cobranca->dataVencimento   = date('d/m/Y', strtotime($fatura->dtvencimento));
        $cobranca->valorVencimento  = $fatura->valorpagar;
        $cobranca->tipoCobranca     = $tipoCobranca->tipo;
        $cobranca->instrucoes        = $instrucoes;
        $cobranca->descricaoCobranca = $instrucoes;

        //field cc
        $cobranca->senderHash = '';
        $cobranca->deviceId =  '';
        $cobranca->creditCardToken = '';

        $cobranca->numeroParcelas   = $numeroParcelas;
        $cobranca->cardName         = $this->input->post('cardholderName');
        $cobranca->cardNumber       = $this->input->post('cardNumber');
        $cobranca->cardExpiryMonth  = $this->input->post('cardExpirationMonth');
        $cobranca->cardExpiryYear   = $this->input->post('cardExpirationYear');
        $cobranca->cvc              = $this->input->post('securityCode');

        $cobranca->cpfTitularCartao         = $this->input->post('docNumber');
        $cobranca->celularTitularCartao     = $this->input->post('telNumber');
        $cobranca->cepTitularCartao         = $this->input->post('cep_titular_valepay');
        $cobranca->enderecoTitularCartao    = $this->input->post('endereco_titular_valepay');
        $cobranca->bairroTitularCartao      = $this->input->post('bairro_titular_valepay');
        $cobranca->cidadeTitularCartao      = $this->input->post('cidade_titular_valepay');
        $cobranca->estadoTitularCartao      = $this->input->post('estado_titular_valepay');
        $cobranca->numeroEnderecoTitularCartao      = $this->input->post('numero_endereco_titular_valepay');
        $cobranca->complementoEnderecoTitularCartao = $this->input->post('complemento_endereco_titular');
        $cobranca->processa_pagamento_link          = true;

        $cobranca->pessoa = $this->preencherPessoaCobrancaValepay($pessoa, $email);

        return $cobranca;
    }

    private function preencherPessoaCobrancaValepay($pessoa, $email) {
        $nome = $pessoa->name;
        $cpf = $pessoa->vat_no;
        $numero = $pessoa->numero;
        $complemento = $pessoa->complemento;
        $bairro = $pessoa->bairro;
        $endereco = $pessoa->address;
        $cidade = $pessoa->city;
        $estado = $pessoa->state;
        $cep = $pessoa->postal_code;
        $telefone = $pessoa->phone;
        $whatsApp = $pessoa->cf5;
        $tipoPessoa = $pessoa->tipoPessoa;
        $data_aniversario = $pessoa->data_aniversario;
        $sexo = $pessoa->sexo;
        $rg = $pessoa->cf1;

        $pessoaCobranca = new PessoaCobrancaDTO_model();

        $pessoaCobranca->id = $pessoa->id;
        $pessoaCobranca->nome = $nome;
        $pessoaCobranca->cpfCnpj = $cpf;
        $pessoaCobranca->email = $email;
        $pessoaCobranca->endereco = $endereco;
        $pessoaCobranca->cidade = $cidade;
        $pessoaCobranca->estado = $estado;
        $pessoaCobranca->cep = $cep;
        $pessoaCobranca->telefone = $telefone;
        $pessoaCobranca->numero = $numero;
        $pessoaCobranca->celular = $whatsApp;
        $pessoaCobranca->bairro = $bairro;
        $pessoaCobranca->complemento = $complemento;
        $pessoaCobranca->tipoPessoa = $tipoPessoa;
        $pessoaCobranca->dataNascimento = $data_aniversario;
        $pessoaCobranca->sexo = $sexo;
        $pessoaCobranca->rg = $rg;

        return $pessoaCobranca;
    }

    private function escreverInstrucoesCobranca($parcela, $fatura, $tipoCobranca, $pessoa, $nParcelas) {

        $receita            = $this->financeiro_model->getReceitaById($fatura->receita);
        $reference          = $fatura->reference;
        $tipoCobrancaTipo   =  $tipoCobranca->tipo;

        $itens =  $this->sales_model->getSaleItemByContaReceber($fatura->contas_receber);

        $instrucoes = ' CLIENTE: ' . $pessoa->name . ' ';

        foreach ($itens as $item) {
            $instrucoes .= "Serviço ".$item->product_name;
        }

        if ($fatura->note) {
            $nota = strip_tags($fatura->note);
            $nota = str_replace ( ':', ' - ', $nota);
            $nota = str_replace ( '&', ' - ', $nota);
            $instrucoes .= $nota.' ';
        }

        if ($receita->name) {
            $instrucoes .= ' (' . $receita->name . ') ';
        }

        if ($parcela == null) {
            if ($tipoCobrancaTipo == 'carne_cartao' || $tipoCobrancaTipo == 'cartao' ) {
                $instrucoes .= ' - '.$reference." - Cartão de Crédito";
            } else {
                $instrucoes .= ' - '.$reference." - Carnê em ".$nParcelas."X";
            }
        } else {
            if ($tipoCobrancaTipo == 'carne_cartao' || $tipoCobrancaTipo == 'cartao' ) {
                $instrucoes .= ' - '.$reference." - Cartão de Crédito";
            } else {
                $instrucoes .= ' - '.$reference." - Parcela " . $parcela->numeroparcela . ' de ' . $parcela->totalParcelas;
            }
        }

        return $instrucoes;
    }

    public function configurarIntegracaoMercadoPago() {

        $mercadoPago = $this->settings_model->getMercadoPagoSettings();

        if ($mercadoPago->active) {

            $this->setting = $this->site->get_setting();

            if ($mercadoPago->sandbox) {
                $token = $mercadoPago->account_token_private_sandbox;
            } else {
                $token = $mercadoPago->account_token_private;
            }

            MercadoPago\SDK::setAccessToken($token);
            MercadoPago\SDK::setIntegratorId("dev_bb44ce3133ba11edac310242ac130004");//TODO NOSSO ID DE DEV NO MERCADO PAGO
            MercadoPago\SDK::setPlatformId("dev_e35fbefb331111ee90612225b209fd90");

            /*
            MercadoPago\SDK::setCorporationId("CORPORATION_ID");
            */
        }
        return null;
    }

    private function processar_pagamento_mercado_pago($cobrancaFatura, $fatura) {
        $this->configurarIntegracaoMercadoPago();

        $payment = new MercadoPago\Payment();
        $payer = new MercadoPago\Payer();

        $payment->transaction_amount    = $fatura->valorpagar;
        $payment->description           = $fatura->product_name;
        $payment->external_reference    = $fatura->id;
        $payment->statement_descriptor  = $this->setting->site_name;
        $payment->notification_url      = 'https://sistema.sagtur.com.br/infrastructure/MercadoPagoController/notification?empresa='.$this->session->userdata('cnpjempresa');

        $payment->token = $this->input->post('MPHiddenInputToken');
        $payment->installments = $this->input->post('installments');
        $payment->payment_method_id = $this->input->post('MPHiddenInputPaymentMethod');
        $payment->issuer_id = $this->input->post('issuerId');
        $payer->email = $this->input->post('cardholderEmail');

        $payer->identification = array(
            "type" => $this->input->post('identificationType'),
            "number" => $this->input->post('docNumber'),
        );

        $uuid = $this->generateUUID();

        MercadoPago\SDK::config()->configure(['X-Idempotency-Key' => $uuid]);
        MercadoPago\SDK::addCustomTrackingParam('X-Idempotency-Key', $uuid);

        $payment->payer = $payer;

        $payment->save();

        $retorno = $this->MercadoPagoException_model->verificarTransacao($payment);
        $status_detail  = $retorno['status_detail'];
        $error          = $retorno['error'];

        $event = "Criação da Integração de Pagamento: ".strtoupper('mercadopago').' com '.strtoupper('Cartão de Crédito Transparente');

        if ($error) $event .= '<br/>Erro: '.$error;

        $parcelasFatura = $this->financeiro_model->getParcelaFaturaByFatura($fatura->id);

        foreach ($parcelasFatura as $parcelaFatura) {
            $parcela = $this->__getById('parcela', $parcelaFatura->parcela);
            $contaReceber  = $this->__getById('conta_receber',  $parcela->contareceberId);

            $this->SaleEventService_model->transacao_pagamento($contaReceber->sale, $event, $status_detail);
        }

        if ($retorno['sucesso']) {
            $this->atualizarDadosCobrancaFatura($payment->id, $fatura->id, $cobrancaFatura->id);
            return $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($payment->id);
        } else {
            $this->atualizarDadosCobrancaFaturaErrorProcessamento($retorno['error'], $cobrancaFatura->id);
            return $cobrancaFatura;
        }
    }

    function consultaPagamento($integracao, $code) {
        if ($integracao == 'pagseguro') {
            $this->FinanceiroService_model->baixarPagamentoIntegracaoByReference($integracao, $code);
        } else if ($integracao == 'valepay') {
            $this->FinanceiroService_model->baixarPagamentoIntegracao($integracao, $code);
        } else if ($integracao == 'mercadopago'){
            $this->FinanceiroService_model->baixarPagamentoIntegracao($integracao, $code);
        } else {
            $this->FinanceiroService_model->baixarPagamentoIntegracao($integracao, $code);
        }

        return $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($code);
    }

    function atualizarDadosCobrancaFatura($code, $faturaId, $cobrancaFaturaId) {
        $checkoutUrl = base_url().'cartaocredito/pagamento/'.$code.'?token='.$this->session->userdata('cnpjempresa');

        $this->__edit('fatura', array('numero_transacao'=> $code), 'id', $faturaId);
        $this->__edit('fatura_cobranca', array('code'=> $code,  'checkoutUrl' => $checkoutUrl, 'errorProcessamento' => ''), 'id', $cobrancaFaturaId);
    }

    private function atualizarDadosCobrancaFaturaErrorProcessamento($error, $cobrancaFaturaId) {
        $this->__edit('fatura_cobranca', array('errorProcessamento'=> $error), 'id', $cobrancaFaturaId);
    }

    function generateUUID() {
        return Uuid::uuid4()->toString();
    }

}