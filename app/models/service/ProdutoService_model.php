<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutoService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        //service
        $this->load->model('service/integracao/TravelTourService_model', 'TravelTourService_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/AgendaProgramacaoService_model', 'AgendaProgramacaoService_model');

        //dto
        $this->load->model("dto/AgendaViagemDTO_model", 'AgendaViagemDTO_model');

        $this->load->model('products_model');
    }

    public function adicionarProduto($produtoModel) {
        try {

            //$produtoModel = new ProdutoModel_model();

            $this->__iniciaTransacao();

            $Settings = $this->site->get_setting();

            $warehouse_qty[] = array(
                'warehouse_id' => $Settings->default_warehouse, //TODO ID DA FILIAL
                'quantity' => $produtoModel->data['quantity'],
                'avg_cost' => $produtoModel->data['price'],
                'rack' => NULL
            );

            $produtoId = $this->products_model->addProduct(
                $produtoModel->data,
                $produtoModel->items,
                $warehouse_qty,
                $produtoModel->transportes,
                $produtoModel->product_attributes,
                $produtoModel->locaisEmbarque,

                $produtoModel->tiposCobranca,
                $produtoModel->condicoesPagamento,

                $produtoModel->faixaEtariaValores,
                $produtoModel->faixaEtariaValoresServicoAdicional,
                $produtoModel->photos,
                $produtoModel->tiposHospedagem,
                $produtoModel->tiposHospedagemFaixaEtaria,
                $produtoModel->midia_data,
                $produtoModel->seo,
                $produtoModel->addresses,
                $produtoModel->servicos_inclui);

            if (!empty($produtoModel->disponibilidades)) {
                $produtoModel->programacaoDatas = $this->AgendaProgramacaoService_model->carregar_datas_programacao($produtoModel->disponibilidades, $produtoId);
            }

            $this->adicionar_product_details($produtoModel->extras, $produtoId);

            $this->AgendaViagemService_model->saveAll($produtoModel->programacaoDatas, $produtoId);

            if ($Settings->isIntegracaoSite == 1) {
                $this->publicarSite($produtoId);
            }

            $this->__confirmaTransacao();

            return $produtoId;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function adicionar_product_details($details = null, $product_id) {

        $this->db->delete('product_details', array('product_id' => $product_id));

        if ($details) {
            foreach ($details as $detail) {
                $this->db->insert('product_details', array('product_id' => $product_id, 'titulo' => $detail['name'], 'note' => $detail['note']));
            }
        }
    }

    public function editarProduto($produtoModel, $id) {

        //$produtoModel = new ProdutoModel_model();

        try {

            $this->__iniciaTransacao();
            $Settings = $this->site->get_setting();

            $warehouse_qty[] = array(
                'warehouse_id' => 1, //TODO ID DA FILIAL
                'quantity' => $produtoModel->data['quantity'],
                'avg_cost' => $produtoModel->data['price'],
                'rack' => NULL
            );

            $this->products_model->updateProduct($id,
                $produtoModel->data,
                $produtoModel->items,
                $warehouse_qty,
                $produtoModel->product_attributes,
                $produtoModel->locaisEmbarque,

                $produtoModel->tiposCobranca,
                $produtoModel->condicoesPagamento,

                $produtoModel->faixaEtariaValores,
                $produtoModel->faixaEtariaValoresServicoAdicional,
                $produtoModel->photos,
                [],
                $produtoModel->tiposHospedagem,
                $produtoModel->tiposHospedagemFaixaEtaria,
                $produtoModel->transportes,
                $produtoModel->midia_data,
                $produtoModel->seo,
                $produtoModel->addresses,
                $produtoModel->servicos_inclui);

            if ($this->isPasseio($produtoModel)) {
                if ($this->isAlteracaoProgramacao($id, $produtoModel)) {

                    $this->AgendaProgramacaoRepository_model->delete($id);//DELETE TODA AGENDA -> PROGRAMAÇAO

                    $disponibilidadesAtual = $this->AgendaViagemService_model->getProgramacao($id);

                    $produtoModel->programacaoDatas =  $this->AgendaProgramacaoService_model->carregar_datas_programacao($produtoModel->disponibilidades, $id);//TODO CARREGA AS NOVAS DATA DA PROGRAMACAO

                    $this->AgendaViagemService_model->saveAll($produtoModel->programacaoDatas, $id);

                    foreach ($disponibilidadesAtual as $disponibilidadade) {

                        $tranferiu = $this->AgendaProgramacaoService_model->transfer_all($disponibilidadade);

                        if ($this->AgendaViagemService_model->isPossuiVendas($disponibilidadade)) {
                            if (!$tranferiu) {//todo se nao conseguiu transferir e pq a data/hora da programacao mudou e nao tem onde encaixar as vendas
                                $totalVendasFaturadasOrcamentos = $disponibilidadade->getTotalVendasFaturas() + $disponibilidadade->getTotalOrcamento();
                                $this->db->update('agenda_viagem', array('vagas' => $totalVendasFaturadasOrcamentos, 'active' => FALSE), array('id' => $disponibilidadade->id));//TODO SE TIVER VENDA NA DATA, ZERA O NUMERO DE VAGAS
                            } else {
                                $this->AgendaViagemService_model->excluir($disponibilidadade->id);
                            }
                        } else {
                            $this->AgendaViagemService_model->excluir($disponibilidadade->id);
                        }
                    }

                }
            } else {
                $this->AgendaViagemService_model->saveAll($produtoModel->programacaoDatas, $id);
            }


            $this->atualizarProgramacaoDataEstoqueHospedagem($id);

            $this->adicionar_product_details($produtoModel->extras, $id);

            $this->__confirmaTransacao();

            if ($Settings->isIntegracaoSite == 1) {
                $this->publicarSite($id);
            }

            return $id;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function isAlteracaoProgramacao($id, $produtoModel) {

        $programacaoAntiga = $this->AgendaProgramacaoRepository_model->getAllByModel($id);
        $programacaoAtual  = $produtoModel->disponibilidades;

        if (empty($programacaoAntiga)) {
            return TRUE;
        }

        $iguais = true;
        foreach ($programacaoAntiga as $objeto) {
            $encontrado = false;
            foreach ($programacaoAtual as $outroObjeto) {
                if ($objeto->equals($outroObjeto)) {
                    $encontrado = true;
                    break;
                }
            }

            if (!$encontrado) {
                $iguais = false;
                break;
            }
        }

        if ($iguais && count($programacaoAntiga) == count($programacaoAtual)) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    private function atualizarProgramacaoDataEstoqueHospedagem($product_id) {
        $agendamentos = $this->AgendaViagemService_model->getProgramacao($product_id);

        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));

        foreach ($agendamentos as $agendamento) {
            if ($agendamento->getControleEstoqueHospedagem()) {
                $dataSaida = $agendamento->dataSaida;
                if (strtotime($dataSaida) >= strtotime($dataHoje)) {
                    $totalVagas = 0;
                    foreach ($agendamento->getQuartos() as $quarto) {
                        $totalVagas = $totalVagas + $quarto->estoque_total;
                    }

                    $this->db->update('agenda_viagem', array('vagas' => $totalVagas), array('id' => $agendamento->id));
                }
            }
        }
    }

    private function publicarSite($id) {
        $agendas = $this->AgendaViagemService_model->getProgramacao($id);

        foreach ($agendas as $agendamento) {
            $this->TravelTourService_model->publicarSite($agendamento->id);
        }
    }

    private function isPasseio($produtoModel)
    {
        return $produtoModel->data['passeio'];
    }

}