<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TravelTourService_model extends CI_Model
{
    private $dbSite;
    private $upload_path;
    private $url_site;

    public function __construct() {

        parent::__construct();

        $this->load->model('products_model');
        $this->load->model('repository/AgendaViagemRespository_model','AgendaViagemRespository_model');

        $ano = date('Y');
        $mes = date('m');

        $this->upload_path = 'assets/uploads/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '16384';

        $Settings = $this->site->get_setting();

        $this->upload_path = $Settings->upload_path_site.'/wp-content/uploads/'.$ano.'/'.$mes.'/';
        $this->url_site = $Settings->url_site.'/wp-content/uploads/'.$ano.'/'.$mes.'/';
    }

    public function publicarSite($agendamentoId) {
        try {

            //$this->dbSite = $this->load->database('viajackyviagens', TRUE);//TODO NOME DA BASE DO SITE
            $this->dbSite = $this->load->database('katatur_site', TRUE);//TODO NOME DA BASE DO SITE

            $this->dbSite->_trans_begin();

            $agendamento = $this->AgendaViagemRespository_model->getProgramacaoById($agendamentoId);

            $this->save_post($agendamento);

            $this->dbSite->_trans_commit();
        } catch(Exception $ex) {
            $this->dbSite->_trans_rollback();
            throw new Exception($ex->getMessage());
        }
    }

    private function save_post($agendamento) {

        $product = $this->products_model->getProductByID($agendamento->produto);

        $name               = $product->name;
        $post_name          = $this->sma->tirarAcentos($name);
        $post_name          = str_replace(' ', '_', $post_name.'_'.$product->id.'_'.$agendamento->id);

        $item = array(
            'post_author'       => 1,
            'post_date'         => date('Y-m-d H:m:s'),
            'post_date_gmt'     => date('Y-m-d H:m:s'),
            'post_content'      => $product->product_details,
            'post_title'        => strtoupper($name),
            'post_excerpt'      => $product->product_details,
            'post_status'       =>  $this->is_servico_ativo($product, $agendamento->id),
            'comment_status'    => 'closed',
            'ping_status'       => 'closed',
            'post_name'         => $post_name,
            'post_modified'     => date('Y-m-d H:m:s'),
            'post_modified_gmt' => date('Y-m-d H:m:s'),
            'post_parent'       => 0,
            'guid'              => '',
            'menu_order'        => 0,
            'post_type'         => 'tour',
            'comment_count'     => 0,
        );

        $post = $this->is_exist_post($agendamento->agenda_site_id);
        
        if (count($post) > 0)  {
            $this->dbSite->update('posts', $item, array('ID' => $post->ID));
            $postId = $post->ID;
        } else  {
            $this->dbSite->insert('posts', $item);
            $postId = $this->dbSite->insert_id();
        }

        $this->vincular_agendamento_site_sistema($postId, $agendamento->id);

        $this->tourmaster_save_tour_meta($agendamento, $product, $postId);

        return $postId;
    }

    private function is_servico_ativo($product, $programacaoId) {

        $programacaoEstoque = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));

        if ($product->enviar_site == '1' &&
            strtotime($programacaoEstoque->getDataSaida()) >= strtotime($dataHoje) &&
            $programacaoEstoque->getTotalDisponvel() > 0){
            return 'publish';
        }

        return 'trash';
    }

    private function is_exist_post($postId) {
        $q = $this->dbSite->get_where('posts', array('ID' => $postId), 1);

        if ($q->num_rows() > 0) return $q->row();

        return null;
    }

    function publish_thumbs_photos($photo, $width, $height) {

        $photo_tumbs    = explode  ('.', $photo);
        $nome_nome      = $photo_tumbs[0].'-'.$width.'x'.$height.'.'.$photo_tumbs[1];

        $this->load->library('image_lib');

        $config['image_library']    = 'gd2';
        $config['source_image']     = $this->upload_path.$photo;
        $config['new_image']        = $this->upload_path.$nome_nome;
        $config['maintain_ratio']   = TRUE;
        $config['width']            = $width;
        $config['height']           = $height;
        $config['max_size']         = '4024'; // 1 MB

        $this->image_lib->initialize($config);

        if (!$this->image_lib->resize())  echo $this->image_lib->display_errors();

        if ($this->Settings->watermark) {

            $this->image_lib->clear();

            $wm['source_image'] = $this->upload_path . $photo;
            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
            $wm['wm_type'] = 'text';
            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
            $wm['quality'] = '100';
            $wm['wm_font_size'] = '16';
            $wm['wm_font_color'] = '999999';
            $wm['wm_shadow_color'] = 'CCCCCC';
            $wm['wm_vrt_alignment'] = 'top';
            $wm['wm_hor_alignment'] = 'right';
            $wm['wm_padding'] = '10';

            $this->image_lib->initialize($wm);
            $this->image_lib->watermark();
        }
    }

    function publish_thumbs_photos_all($photo) {
        $this->publish_thumbs_photos($photo, 300, 200);
        $this->publish_thumbs_photos($photo, 1024, 682);
        $this->publish_thumbs_photos($photo, 150, 150);
        $this->publish_thumbs_photos($photo, 768, 512);
        $this->publish_thumbs_photos($photo, 700, 500);
        $this->publish_thumbs_photos($photo, 700, 450);
        $this->publish_thumbs_photos($photo, 700, 660);
        $this->publish_thumbs_photos($photo, 600, 800);
        $this->publish_thumbs_photos($photo, 1280, 580);
        $this->publish_thumbs_photos($photo, 1280, 700);
        $this->publish_thumbs_photos($photo, 900, 500);
        $this->publish_thumbs_photos($photo, 1100, 490);
        $this->publish_thumbs_photos($photo, 700, 430);
        $this->publish_thumbs_photos($photo, 550, 550);
        $this->publish_thumbs_photos($photo, 600, 600);
        $this->publish_thumbs_photos($photo, 800, 853);
        $this->publish_thumbs_photos($photo, 600, 700);
    }

    private function publish_additional_photos() {

        $this->load->library('upload');
        $photos = NULL;

        if ($_FILES['userfile']['name'][0] != "") {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 100;
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);

            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                $this->upload->initialize($config);

                if ($this->upload->do_upload()) {
                    $photos[] = array(
                        'file_name' => $this->upload->file_name,
                        'file_ext' => $this->upload->file_ext,
                        'file_type' => $this->upload->file_type,
                        'image_width' => $this->upload->image_width,
                        'image_height' => $this->upload->image_height
                    );

                    $this->publish_thumbs_photos_all($this->upload->file_name);
                }
            }

            $this->image_lib->clear();
        }

        return $photos;
    }

    function publish_featured_photos() {

        if ($_FILES['product_image']['size'] > 0) {

            $this->load->library('upload');

            if ( ! file_exists($this->upload_path)) mkdir($this->upload_path, 0700);

            $config['upload_path']      = $this->upload_path;
            $config['allowed_types']    = $this->image_types;
            $config['max_size']         = $this->allowed_file_size;
            $config['max_width']        = $this->Settings->iwidth;
            $config['max_height']       = $this->Settings->iheight;
            $config['overwrite']        = FALSE;
            $config['max_filename']     = 100;
            $config['encrypt_name']     = TRUE;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('product_image')) {
                $photo = $this->upload;

                $this->publish_thumbs_photos_all($this->upload->file_name);
            }

            $this->image_lib->clear();

            return $photo;
        }

        return null;
    }

    private function tag_image_gallery($product, $post_id) {

        $gallerys = $this->save_post_image_additional($product, $post_id);

        $photos_array = array(
            "template" => "wrapper",
            "type" => "background",
            "value" => array(
                "id" => "photos",
                "class" => "",
                "content-layout" => "boxed",
                "max-width" => "",
                "enable-space" => "disable",
                "hide-this-wrapper-in" => "none",
                "animation" => "none",
                "animation-location" => "0.8",
                "full-height" => "disable",
                "decrease-height" => "0px",
                "centering-content" => "disable",
                "background-type" => "color",
                "background-colo" => "",
                "background-image" => "",
                "background-image-style" => "cover",
                "ackground-image-position" => "center",
                "background-video-url" => "",
                "background-video-url-mp4" => "",
                "background-video-url-webm" => "",
                "background-video-url-ogg" => "",
                "background-video-image" => "",
                "background-pattern" => "pattern-1",
                "pattern-opacity" => "1",
                "parallax-speed" => "0.8",
                "overflow" => "visible",
                "border-type" => "none",
                "border-pre-spaces" => array(
                    "top" => "20px",
                    "right" => "20px",
                    "bottom" => "20px",
                    "left" => "20px",
                    "settings" => "link",
                ),
                "border-width" => array(
                    "top" => "1px",
                    "right" => "1px",
                    "bottom" => "1px",
                    "left" => "1px",
                    "settings" => "link",
                ),
                "border-color" => "#ffffff",
                "border-style" => "solid",
                "padding" => array(
                    "top" => "70px",
                    "right" => "0px",
                    "bottom" => "30px",
                    "left" => "0px",
                    "settings" => "unlink",
                ),
                "margin" => array(
                    "top" => "0px",
                    "right" => "0px",
                    "bottom" => "0px",
                    "left" => "0px",
                    "settings" => "link",
                ),
                "skin" => "Blue Icon",
            ),
            "items" => array(
                array(
                    "template" => "element",
                    "type" => "title",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "title" => "Photos",
                        "caption" => "",
                        "caption-position" => "bottom",
                        "title-link-text" => "",
                        "title-link" => "",
                        "title-link-target" => "_self",
                        "text-align" => "left",
                        "left-media-type" => "icon",
                        "left-icon" => "icon_images",
                        "left-image" => "",
                        "heading-tag" => "h6",
                        "icon-font-size" => "18px",
                        "title-font-size" => "24px",
                        "title-font-weight" => "600",
                        "title-font-style" => "normal",
                        "title-font-letter-spacing" => "0px",
                        "title-font-uppercase" => "disable",
                        "caption-font-size" => "16px",
                        "caption-font-weight" => "400",
                        "caption-font-style" => "italic",
                        "caption-font-letter-spacing" => "0px",
                        "caption-font-uppercase" => "disable",
                        "left-icon-color" => "",
                        "title-color" => "",
                        "title-link-hover-color" => "",
                        "caption-color" => "",
                        "caption-spaces" => "10px",
                        "media-margin-right" => "15px",
                        "padding-bottom" => "35px",
                    ),
                ),
                array(
                    "template" => "element",
                    "type" => "gallery",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "gallery" => $gallerys,
                        "random" => "disable",
                        "pagination" => "none",
                        "show-amount" => "20",
                        "pagination-style" => "default",
                        "pagination-align" => "default",
                        "style" => "slider",
                        "max-slider-height" => "500px",
                        "overlay" => "icon-hover",
                        "show-caption" => "disable",
                        "overlay-on-hover" => "disable",
                        "column" => "3",
                        "layout" => "fitrows",
                        "slider-navigation" => "bullet",
                        "slider-bullet-style" => "default",
                        "slider-effects" => "default",
                        "enable-direction-navigation" => "disable",
                        "thumbnail-navigation" => "below-slider",
                        "carousel-autoslide" => "enable",
                        "carousel-scrolling-item-amount" => "1",
                        "grid-slider-navigation" => "navigation",
                        "carousel-bullet-style" => "default",
                        "thumbnail-size" => "Large Landscape 3",
                        "slider-thumbnail-size" => "Portfolio Thumbnail",
                        "overlay-color" => "",
                        "overlay-opacity" => "",
                        "border-radius" => "",
                        "image-bottom-margin" => "",
                        "padding-bottom" => "30px",
                    )
                ),
            )
        );
        return $photos_array;
    }

    private function save_post_image_additional($product, $postParentId) {

        $photosMemory = $this->__getAllItens('product_photos_site', 'product_id', $product->id);

        foreach ($photosMemory as $photoM) {
            $photos_image[] = array(
                "id" => $photoM->post_id,
                "thumbnail" => $photoM->photo
            );
        }

        $photos = $this->publish_additional_photos();

        if ($photos != NULL) {
            foreach ($photos as $photo) {

                $file_name = $photo['file_name'];
                $file_ext = $photo['file_ext'];
                $file_type = $photo['file_type'];
                $image_width = $photo['image_width'];
                $image_height = $photo['image_height'];

                $post_id = $this->save_post_imagem(
                    $product,
                    $file_name,
                    $file_ext,
                    $file_type,
                    $image_width,
                    $image_height,
                    $postParentId);

                $photo_name = str_replace($file_ext, '', $file_name) . '-150x150' . $file_ext;

                $photos_image[] = array(
                    "id" => $post_id,
                    "thumbnail" => $this->url_site . $photo_name
                );

                $photos = array(
                    'product_id' => $product->id,
                    'post_id' => $post_id,
                    'photo' => $this->url_site . $photo_name
                );

                $this->__save('product_photos_site', $photos);
            }
        }

        return $photos_image;
    }

    private function save_post_imagem_destaque($product, $postParentId = '') {

        $photo = $this->publish_featured_photos();

        if ($photo == null) return NULL;

        $file_name = $photo->file_name;
        $file_ext = $photo->file_ext;
        $file_type = $photo->file_type;
        $image_width = $photo->image_width;
        $image_height = $photo->image_height;

        $postId = $this->save_post_imagem($product, $file_name, $file_ext, $file_type, $image_width, $image_height, $postParentId);

        $this->vincular_imagem_destaque_site_sitema($postId, $product->id);

        return $postId;
    }

    private function save_post_imagem($product, $photo_name, $file_ext, $file_type, $image_width, $image_height, $postParentId = '') {

        if ($photo_name) {

            $post_name  = str_replace(' ', '_', $this->sma->tirarAcentos($product->name).''.$product->id);

            $ano = date('Y');
            $mes = date('m');

            $photo_name_diretorio = $ano.'/'.$mes.'/'.$photo_name;

            $item = array(
                'post_author' => 1,
                'post_date' => date('Y-m-d H:m:s'),
                'post_date_gmt' => date('Y-m-d H:m:s'),
                'post_content' => $product->product_details,
                'post_title' => strtoupper($product->name),
                'post_excerpt' => $product->product_details,
                'post_status' => 'inherit',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => $post_name,
                'post_modified' => date('Y-m-d H:m:s'),
                'post_modified_gmt' => date('Y-m-d H:m:s'),
                'post_parent' => $postParentId,
                'guid' => $this->url_site.$photo_name,
                'menu_order' => 0,
                'post_type' => 'attachment',
                'post_mime_type' => 'image/jpeg',
                'comment_count' => 0,
            );

            $this->dbSite->insert('posts', $item);
            $postId = $this->dbSite->insert_id();

            $this->update_post_meta($postId, '_wp_attached_file',  $photo_name_diretorio);

            $photo_name_not_ext = str_replace( $file_ext, '', $photo_name);

            $attachment_metadata = array(
                "width" => $image_width,
                "height" => $image_height,
                "file" => $photo_name_diretorio,
                "sizes" => array(
                    "medium" => array(
                        "file" =>  $photo_name_not_ext.'-300x200'.$file_ext,
                        "width" => 300,
                        "height" => 200,
                        "mime-type" => $file_type
                    ),
                    "large" => array(
                        "file" =>  $photo_name_not_ext.'-1024x682'.$file_ext,
                        "width" => 1024,
                        "height" => 682,
                        "mime-type" => $file_type
                    ),
                    "thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-150x150'.$file_ext,
                        "width" => 150,
                        "height" => 150,
                        "mime-type" => $file_type
                    ),
                    "medium_large" => array(
                        "file" =>  $photo_name_not_ext.'-768x512'.$file_ext,
                        "width" => 768,
                        "height" => 512,
                        "mime-type" => $file_type
                    ),
                    "Personnel Thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-700x500'.$file_ext,
                        "width" => 700,
                        "height" => 500,
                        "mime-type" => $file_type
                    ),
                    "Portfolio Thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-700x450'.$file_ext,
                        "width" => 700,
                        "height" => 450,
                        "mime-type" => $file_type
                    ),
                    "Portfolio Thumbnail Portrait" => array(
                        "file" =>  $photo_name_not_ext.'-700x660'.$file_ext,
                        "width" => 700,
                        "height" => 660,
                        "mime-type" => $file_type
                    ),
                    "Portfolio Thumbnail Portrait 2" => array(
                        "file" =>  $photo_name_not_ext.'-600x800'.$file_ext,
                        "width" => 600,
                        "height" => 800,
                        "mime-type" => $file_type
                    ),
                    "Large Landscape" => array(
                        "file" =>  $photo_name_not_ext.'-1280x580'.$file_ext,
                        "width" => 1280,
                        "height" => 580,
                        "mime-type" => $file_type
                    ),
                    "Large Landscape 2" => array(
                        "file" =>  $photo_name_not_ext.'-1280x700'.$file_ext,
                        "width" => 1280,
                        "height" => 700,
                        "mime-type" => $file_type
                    ),
                    "Portfolio Side Description Large" => array(
                        "file" =>  $photo_name_not_ext.'-900x500'.$file_ext,
                        "width" => 900,
                        "height" => 500,
                        "mime-type" => $file_type
                    ),
                    "Blog Full Thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-1100x490'.$file_ext,
                        "width" => 1100,
                        "height" => 490,
                        "mime-type" => $file_type
                    ),
                    "Blog Column Thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-700x430'.$file_ext,
                        "width" => 700,
                        "height" => 430,
                        "mime-type" => $file_type
                    ),
                    "Personnel Thumbnail 2" => array(
                        "file" =>  $photo_name_not_ext.'-550x550'.$file_ext,
                        "width" => 550,
                        "height" => 550,
                        "mime-type" => $file_type
                    ),
                    "Personnel Square" => array(
                        "file" =>  $photo_name_not_ext.'-600x600'.$file_ext,
                        "width" => 600,
                        "height" => 600,
                        "mime-type" => $file_type
                    ),
                    "Tour Category" => array(
                        "file" =>  $photo_name_not_ext.'-800x853'.$file_ext,
                        "width" => 800,
                        "height" => 853,
                        "mime-type" =>$file_type
                    ),
                    "Tour Side Thumbnail" => array(
                        "file" =>  $photo_name_not_ext.'-600x700'.$file_ext,
                        "width" => 600,
                        "height" => 700,
                        "mime-type" => $file_type
                    ),
                ),
                "image_meta" => array(
                    "aperture" => 0,
                    "credit" => "",
                    "camera" => "",
                    "caption" => "",
                    "created_timestamp" => "0",
                    "copyright" => "",
                    "focal_length" => "0",
                    "iso" => "0",
                    "shutter_speed" => "0",
                    "title" => "",
                    "orientation" => "",
                    "keywords" => array(),
                )
            );

            $this->update_post_meta($postId, '_wp_attachment_metadata', $this->maybe_serialize($attachment_metadata) );
            $this->update_post_meta($postId, 'wp-smpro-smush-data', 'a:2:{s:5:"stats";a:8:{s:7:"percent";d:3.6283584666574970611918615759350359439849853515625;s:5:"bytes";i:49594;s:11:"size_before";i:1366844;s:10:"size_after";i:1317250;s:4:"time";d:0.5300000000000000266453525910037569701671600341796875;s:11:"api_version";s:3:"1.0";s:5:"lossy";b:0;s:9:"keep_exif";i:0;}s:5:"sizes";a:15:{s:6:"medium";O:8:"stdClass":5:{s:7:"percent";d:4.160000000000000142108547152020037174224853515625;s:5:"bytes";i:1003;s:11:"size_before";i:24096;s:10:"size_after";i:23093;s:4:"time";d:0.01000000000000000020816681711721685132943093776702880859375;}s:9:"thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.7599999999999997868371792719699442386627197265625;s:5:"bytes";i:341;s:11:"size_before";i:9059;s:10:"size_after";i:8718;s:4:"time";d:0.01000000000000000020816681711721685132943093776702880859375;}s:12:"medium_large";O:8:"stdClass":5:{s:7:"percent";d:3.939999999999999946709294817992486059665679931640625;s:5:"bytes";i:4451;s:11:"size_before";i:112876;s:10:"size_after";i:108425;s:4:"time";d:0.0299999999999999988897769753748434595763683319091796875;}s:19:"Personnel Thumbnail";O:8:"stdClass":5:{s:7:"percent";d:4.20999999999999996447286321199499070644378662109375;s:5:"bytes";i:3958;s:11:"size_before";i:94031;s:10:"size_after";i:90073;s:4:"time";d:0.0299999999999999988897769753748434595763683319091796875;}s:19:"Portfolio Thumbnail";O:8:"stdClass":5:{s:7:"percent";d:4.07000000000000028421709430404007434844970703125;s:5:"bytes";i:3463;s:11:"size_before";i:85171;s:10:"size_after";i:81708;s:4:"time";d:0.0200000000000000004163336342344337026588618755340576171875;}s:28:"Portfolio Thumbnail Portrait";O:8:"stdClass":5:{s:7:"percent";d:3.819999999999999840127884453977458178997039794921875;s:5:"bytes";i:4479;s:11:"size_before";i:117209;s:10:"size_after";i:112730;s:4:"time";d:0.040000000000000000832667268468867405317723751068115234375;}s:30:"Portfolio Thumbnail Portrait 2";O:8:"stdClass":5:{s:7:"percent";d:3.810000000000000053290705182007513940334320068359375;s:5:"bytes";i:3772;s:11:"size_before";i:98986;s:10:"size_after";i:95214;s:4:"time";d:0.0299999999999999988897769753748434595763683319091796875;}s:15:"Large Landscape";O:8:"stdClass":5:{s:7:"percent";d:3.95000000000000017763568394002504646778106689453125;s:5:"bytes";i:5135;s:11:"size_before";i:129854;s:10:"size_after";i:124719;s:4:"time";d:0.059999999999999997779553950749686919152736663818359375;}s:32:"Portfolio Side Description Large";O:8:"stdClass":5:{s:7:"percent";d:3.939999999999999946709294817992486059665679931640625;s:5:"bytes";i:4431;s:11:"size_before";i:112573;s:10:"size_after";i:108142;s:4:"time";d:0.1000000000000000055511151231257827021181583404541015625;}s:19:"Blog Full Thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.720000000000000195399252334027551114559173583984375;s:5:"bytes";i:4085;s:11:"size_before";i:109820;s:10:"size_after";i:105735;s:4:"time";d:0.0299999999999999988897769753748434595763683319091796875;}s:21:"Blog Column Thumbnail";O:8:"stdClass":5:{s:7:"percent";d:3.95000000000000017763568394002504646778106689453125;s:5:"bytes";i:3199;s:11:"size_before";i:81052;s:10:"size_after";i:77853;s:4:"time";d:0.0200000000000000004163336342344337026588618755340576171875;}s:21:"Personnel Thumbnail 2";O:8:"stdClass":5:{s:7:"percent";d:3.9900000000000002131628207280300557613372802734375;s:5:"bytes";i:3007;s:11:"size_before";i:75315;s:10:"size_after";i:72308;s:4:"time";d:0.0200000000000000004163336342344337026588618755340576171875;}s:16:"Personnel Square";O:8:"stdClass":5:{s:7:"percent";d:3.660000000000000142108547152020037174224853515625;s:5:"bytes";i:3271;s:11:"size_before";i:89320;s:10:"size_after";i:86049;s:4:"time";d:0.0299999999999999988897769753748434595763683319091796875;}s:13:"Tour Category";O:8:"stdClass":5:{s:7:"percent";d:3.779999999999999804600747665972448885440826416015625;s:5:"bytes";i:4999;s:11:"size_before";i:132268;s:10:"size_after";i:127269;s:4:"time";d:0.040000000000000000832667268468867405317723751068115234375;}s:19:"Tour Side Thumbnail";O:8:"stdClass":5:{s:7:"percent";i:0;s:5:"bytes";i:0;s:11:"size_before";i:95214;s:10:"size_after";i:95214;s:4:"time";d:0.059999999999999997779553950749686919152736663818359375;}}}');

            return $postId;
        }
        return  NULL;
    }

    function save_terms_category($category, $post_id) {
        $term_id = $this->save_terms($category->name);

        $this->save_termmeta($term_id);
        $this->save_term_taxonomy_to_category($term_id, $post_id);
    }

    function save_terms_destiny($product, $post_id) {
        $term_id = $this->save_terms($product->name);

        $this->save_termmeta($term_id);
        $this->save_term_taxonomy_to_destiny($term_id, $post_id);
    }

    function save_terms($name) {

        $slug = strtolower(str_replace(' ', '_', $this->sma->tirarAcentos($name)));

        $terms = $this->is_exist_terms($slug);

        $terms_data = array(
            'name'          => $name,
            'slug'          => $slug,
            'term_group'    => 0
        );

        if (count($terms) > 0) {
            $this->dbSite->update('terms', $terms_data, array('term_id' => $terms->term_id));
            $term_id = $terms->term_id;
        } else {
            $this->dbSite->insert('terms', $terms_data);
            $term_id = $this->dbSite->insert_id();
        }

        return $term_id;
    }

    function save_termmeta($term_id) {

        $this->delete_termmeta($term_id);

        $termmeta = array(
            'term_id'       => $term_id,
            'meta_key'      => 'thumbnail',
            'meta_value'    => ''
        );
        $this->dbSite->insert('termmeta', $termmeta);

        $termmeta = array(
            'term_id'       => $term_id,
            'meta_key'      => 'archive-title-background',
            'meta_value'    => ''
        );
        $this->dbSite->insert('termmeta', $termmeta);
    }

    function save_term_taxonomy_to_destiny($term_id, $post_id) {
        $this->save_term_taxonomy($term_id, 'tour-destination', $post_id);
    }

    function save_term_taxonomy_to_category($term_id, $post_id) {
        $this->save_term_taxonomy($term_id, 'tour_category', $post_id);
    }

    function save_term_taxonomy($term_id, $taxonomyName, $post_id) {

        $taxonomy = $this->is_exist_term_taxonomy($term_id, $taxonomyName);

        $term_taxonomy_data = array(
            'term_id'       => $term_id,
            'taxonomy'      => $taxonomyName,
            'description'   => '',
            'parent'        => 0,
            'count'         => 1,
        );

        if (count($taxonomy) > 0) {
            $this->dbSite->update('term_taxonomy', $term_taxonomy_data, array('term_taxonomy_id' => $taxonomy->term_taxonomy_id));
            $term_taxonomy_id = $taxonomy->term_taxonomy_id;

        } else {
            $this->dbSite->insert('term_taxonomy', $term_taxonomy_data);
            $term_taxonomy_id = $this->dbSite->insert_id();
        }

        $this->save_term_relationships($term_taxonomy_id, $post_id);
    }

    function save_term_relationships($term_taxonomy_id, $post_id) {

        $term_relationships = $this->is_exist_term_relationships($term_taxonomy_id, $post_id);

        $term_relationships_data = array(
            'object_id'         => $post_id,
            'term_taxonomy_id'  => $term_taxonomy_id,
            'term_order'        => 0
        );

        if (count($term_relationships) > 0) {
            $this->dbSite->update('term_relationships', $term_relationships_data, array('term_taxonomy_id' => $term_taxonomy_id , 'object_id' => $post_id));
        } else {
            $this->dbSite->insert('term_relationships', $term_relationships_data);
        }
    }

    private function delete_termmeta($term_id) {
        $this->dbSite->delete('termmeta', $data = array('term_id'=> $term_id) );
    }

    private function is_exist_term_taxonomy($term_id, $taxonomy) {
        $q = $this->dbSite->get_where('term_taxonomy', array('term_id' => $term_id, 'taxonomy'=> $taxonomy), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return null;
    }

    private function is_exist_term_relationships($term_taxonomy_id, $object_id) {
        $q = $this->dbSite->get_where('term_relationships', array('term_taxonomy_id' => $term_taxonomy_id, 'object_id' => $object_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return null;
    }

    private function is_exist_terms($post_name) {
        $q = $this->dbSite->get_where('terms', array('slug' => $post_name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return null;
    }

    function vincular_agendamento_site_sistema($postId, $productId) {
        $this->db->update('agenda_viagem', array('agenda_site_id' => $postId), array('id' => $productId));
    }

    function vincular_imagem_destaque_site_sitema($postId, $productId) {
        $this->db->update('products', array('products_image_site_id' => $postId), array('id' => $productId));
    }

    function tag_tourmaster_tour_option($agendamento, $product, $category) {

        $meta_value = array(
            "header-image" => "feature-image",
            "header-image-custom" => "",
            "header-revolution-slider-id" => "",
            "header-top-padding" => "",
            "header-bottom-padding" => "",
            "header-background-overlay-opacity" => "",
            "header-background-gradient" => "default",
            "lightbox-video-url" => "",
            "header-slider-thumbnail" => "full",
            "background-video-url" => "",
            "background-video-url-mp4" => "",
            "background-video-url-webm" => "",
            "background-video-url-ogg" => "",
            "background-video-image" => "",
            "show-wordpress-editor-content" => "enable",
            "enable-page-title" => "enable",
            "enable-header-review-number" => "enable",
            "promo-text-ribbon-text-color" => "#ffffff",
            "promo-text-ribbon-background" => "#4296ed",//TODO A COR DO BOTAO
            "sidebar-widget" => "",
            "enable-payment" => "enable",
            "payment-admin-approval" => "",
            "form-settings" => "booking",
            "form-custom-title" => "",
            "form-custom-code" => "",
            "show-price" => "enable",
            "date-selection-type" => "calendar",
            "last-minute-booking" => "",
            "deposit-booking" => "default",
            "deposit-amount" => "",
            "deposit2-amount" => "",
            "deposit3-amount" => "",
            "deposit4-amount" => "",
            "deposit5-amount" => "",
            "promo-text" => $this->sma->dataDeHojePorExtensoRetornoDiaMensAno($agendamento->dataSaida),

            "tour-price-text" => $product->valor_pacote,//TODO VALOR DESCRICAO
            //"tour-price-discount-text" => $this->sma->formatMoney($product->precoExibicaoSite, $product->simboloMoeda),
            "tour-price-discount-text" =>  number_format($product->precoExibicaoSite,2,",","."),

            "duration-text" => $category->name,
            "date-range" => $this->sma->hrsd($agendamento->dataSaida),
            "departure-location" => "Saída: ".$this->sma->hrsd($agendamento->dataSaida).' '.$agendamento->horaSaida,
            "return-location" => "Retorno: ".$this->sma->hrsd($agendamento->dataRetorno).' '.$agendamento->horaRetorno,
            "minimum-age" => "",
            "multiple-duration" => "",
            "display-single-tour-info" => "enable",
            "require-each-traveller-info" => "enable",
            "additional-traveller-fields" => "",
            "require-traveller-info-title" => "enable",
            "require-traveller-passport" => "disable",
            "tour-service" => NULL,
            "custom-excerpt" => "",
            "link-proceed-booking-to-external-url" => "",
            "external-url-text" => "",
            "enable-review" => "enable",
            "tour-type" => "single",
            "tour-timing-method" => "single",
            "date-price" => NULL,
            "group-discount-category" => NULL,
            "group-discount-apply" => NULL,
            "group-discount-per-person" => "disable",
            "enable-urgency-message" => "enable",
            "real-urgency-message" => "disable",
            "urgency-message-number-from" => "5",
            "urgency-message-number-to" => "10",
            "payment-notification-days-before-travel" => "0",
            "deposit-payment-notification-days-before-travel" => "",
            "payment-notification-mail-subject" => "",
            "payment-notification-mail-message" => "",
            "enable-payment-notification-message-admin-copy" => "enable",
            "reminder-message-days-before-travel" => "1",
            "reminder-message-mail-subject" => "Sua viagem está chegando",
            "reminder-message-mail-message" => "Ola, Informamos que sua viagem está chegando, confira a data, hora e local para se programar. Esperamos por você.",
            "enable-reminder-message-admin-copy" => "disable",
            "group-message-date" => "",
            "group-message-mail-subject" => "",
            "group-message-mail-message" => "",
            "enable-group-message-admin-copy" => "disable",
        );

        return $meta_value;
    }

    function tag_roteiro_viagem($product) {

        $tag = array(
            "template" => "wrapper",
            "type" => "background",
            "value" => array(
                "id" => "itinerary",
                "class" => "",
                "content-layout" => "boxed",
                "max-width" => "",
                "enable-space" => "disable",
                "hide-this-wrapper-in" => "none",
                "animation" => "none",
                "animation-location" => "0.8",
                "full-height" => "disable",
                "decrease-height" => "0px",
                "centering-content" => "disable",
                "background-type" => "color",
                "background-colo" => "",
                "background-image" => "",
                "background-image-style" => "cover",
                "ackground-image-position" => "center",
                "background-video-url" => "",
                "background-video-url-mp4" => "",
                "background-video-url-webm" => "",
                "background-video-url-ogg" => "",
                "background-video-image" => "",
                "background-pattern" => "pattern-1",
                "pattern-opacity" => "1",
                "parallax-speed" => "0.8",
                "overflow" => "visible",
                "border-type" => "none",
                "border-pre-spaces" => array(
                    "top" => "20px",
                    "right" => "20px",
                    "bottom" => "20px",
                    "left" => "20px",
                    "settings" => "link",
                ),
                "border-width" => array(
                    "top" => "1px",
                    "right" => "1px",
                    "bottom" => "1px",
                    "left" => "1px",
                    "settings" => "link",
                ),
                "border-color" => "#ffffff",
                "border-style" => "solid",
                "padding" => array(
                    "top" => "70px",
                    "right" => "0px",
                    "bottom" => "30px",
                    "left" => "0px",
                    "settings" => "unlink",
                ),
                "margin" => array(
                    "top" => "0px",
                    "right" => "0px",
                    "bottom" => "0px",
                    "left" => "0px",
                    "settings" => "link",
                ),
                "skin" => "Blue Icon",
            ),
            "items" => array(
                array(
                    "template" => "element",
                    "type" => "title",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "title" => "Roteiro",
                        "caption" => "",
                        "caption-position" => "bottom",
                        "title-link-text" => "",
                        "title-link" => "",
                        "title-link-target" => "_self",
                        "text-align" => "left",
                        "left-media-type" => "icon",
                        "left-icon" => "fa fa-map-signs",
                        "left-image" => "",
                        "heading-tag" => "h6",
                        "icon-font-size" => "18px",
                        "title-font-size" => "24px",
                        "title-font-weight" => "600",
                        "title-font-style" => "normal",
                        "title-font-letter-spacing" => "0px",
                        "title-font-uppercase" => "disable",
                        "caption-font-size" => "16px",
                        "caption-font-weight" => "400",
                        "caption-font-style" => "italic",
                        "caption-font-letter-spacing" => "0px",
                        "caption-font-uppercase" => "disable",
                        "title-color" => "",
                        "title-link-hover-color" => "",
                        "caption-color" => "",
                        "caption-spaces" => "10px",
                        "media-margin-right" => "15px",
                        "padding-bottom" => "35px",
                    )
                ),
                array(
                    "template" => "element",
                    "type" => "text-box",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "content" => $product->itinerario,
                        "text-align" => "left",
                        "apply-the-content-filter" => "disable",
                        "enable-p-space" => "enable",
                        "font-size" => "",
                        "content-line-height" => "",
                        "content-font-weight" => "",
                        "content-letter-spacing" => "",
                        "content-text-transform" => "",
                        "tablet-font-size" => "",
                        "mobile-font-size" => "",
                        "text-color" => "",
                        "margin-left" => "",
                        "margin-right" => "",
                        "padding-bottom" => "30px",
                    ),
                ),
            )
        );
        return $tag;
    }

    function tag_condicoes_viagem($product) {

        $tag = array(
            "template" => "wrapper",
            "type" => "background",
            "value" => array(
                "id" => "valores",
                "class" => "",
                "content-layout" => "boxed",
                "max-width" => "",
                "enable-space" => "disable",
                "hide-this-wrapper-in" => "none",
                "animation" => "none",
                "animation-location" => "0.8",
                "full-height" => "disable",
                "decrease-height" => "0px",
                "centering-content" => "disable",
                "background-type" => "color",
                "background-colo" => "",
                "background-image" => "",
                "background-image-style" => "cover",
                "ackground-image-position" => "center",
                "background-video-url" => "",
                "background-video-url-mp4" => "",
                "background-video-url-webm" => "",
                "background-video-url-ogg" => "",
                "background-video-image" => "",
                "background-pattern" => "pattern-1",
                "pattern-opacity" => "1",
                "parallax-speed" => "0.8",
                "overflow" => "visible",
                "border-type" => "none",
                "border-pre-spaces" => array(
                    "top" => "20px",
                    "right" => "20px",
                    "bottom" => "20px",
                    "left" => "20px",
                    "settings" => "link",
                ),
                "border-width" => array(
                    "top" => "1px",
                    "right" => "1px",
                    "bottom" => "1px",
                    "left" => "1px",
                    "settings" => "link",
                ),
                "border-color" => "#ffffff",
                "border-style" => "solid",
                "padding" => array(
                    "top" => "70px",
                    "right" => "0px",
                    "bottom" => "30px",
                    "left" => "0px",
                    "settings" => "unlink",
                ),
                "margin" => array(
                    "top" => "0px",
                    "right" => "0px",
                    "bottom" => "0px",
                    "left" => "0px",
                    "settings" => "link",
                ),
                "skin" => "Blue Icon",
            ),
            "items" => array(
                array(
                    "template" => "element",
                    "type" => "title",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "title" => "Valores e Condições",
                        "caption" => "",
                        "caption-position" => "bottom",
                        "title-link-text" => "",
                        "title-link" => "",
                        "title-link-target" => "_self",
                        "text-align" => "left",
                        "left-media-type" => "icon",
                        "left-icon" => "fa fa-dollar",
                        "left-image" => "",
                        "heading-tag" => "h6",
                        "icon-font-size" => "18px",
                        "title-font-size" => "24px",
                        "title-font-weight" => "600",
                        "title-font-style" => "normal",
                        "title-font-letter-spacing" => "0px",
                        "title-font-uppercase" => "disable",
                        "caption-font-size" => "16px",
                        "caption-font-weight" => "400",
                        "caption-font-style" => "italic",
                        "caption-font-letter-spacing" => "0px",
                        "caption-font-uppercase" => "disable",
                        "title-color" => "",
                        "title-link-hover-color" => "",
                        "caption-color" => "",
                        "caption-spaces" => "10px",
                        "media-margin-right" => "15px",
                        "padding-bottom" => "35px",
                    )
                ),
                array(
                    "template" => "element",
                    "type" => "text-box",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "content" => $product->valores_condicoes,
                        "text-align" => "left",
                        "apply-the-content-filter" => "disable",
                        "enable-p-space" => "enable",
                        "font-size" => "",
                        "content-line-height" => "",
                        "content-font-weight" => "",
                        "content-letter-spacing" => "",
                        "content-text-transform" => "",
                        "tablet-font-size" => "",
                        "mobile-font-size" => "",
                        "text-color" => "",
                        "margin-left" => "",
                        "margin-right" => "",
                        "padding-bottom" => "30px",
                    ),
                ),
            )
        );
        return $tag;
    }

    function tag_o_que_inclui_viagem($product) {

        $tag = array(
            "template" => "wrapper",
            "type" => "background",
            "value" => array(
                "id" => "oqueinclui",
                "class" => "",
                "content-layout" => "boxed",
                "max-width" => "",
                "enable-space" => "disable",
                "hide-this-wrapper-in" => "none",
                "animation" => "none",
                "animation-location" => "0.8",
                "full-height" => "disable",
                "decrease-height" => "0px",
                "centering-content" => "disable",
                "background-type" => "color",
                "background-colo" => "",
                "background-image" => "",
                "background-image-style" => "cover",
                "ackground-image-position" => "center",
                "background-video-url" => "",
                "background-video-url-mp4" => "",
                "background-video-url-webm" => "",
                "background-video-url-ogg" => "",
                "background-video-image" => "",
                "background-pattern" => "pattern-1",
                "pattern-opacity" => "1",
                "parallax-speed" => "0.8",
                "overflow" => "visible",
                "border-type" => "none",
                "border-pre-spaces" => array(
                    "top" => "20px",
                    "right" => "20px",
                    "bottom" => "20px",
                    "left" => "20px",
                    "settings" => "link",
                ),
                "border-width" => array(
                    "top" => "1px",
                    "right" => "1px",
                    "bottom" => "1px",
                    "left" => "1px",
                    "settings" => "link",
                ),
                "border-color" => "#ffffff",
                "border-style" => "solid",
                "padding" => array(
                    "top" => "70px",
                    "right" => "0px",
                    "bottom" => "30px",
                    "left" => "0px",
                    "settings" => "unlink",
                ),
                "margin" => array(
                    "top" => "0px",
                    "right" => "0px",
                    "bottom" => "0px",
                    "left" => "0px",
                    "settings" => "link",
                ),
                "skin" => "Blue Icon",
            ),
            "items" => array(
                array(
                    "template" => "element",
                    "type" => "title",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "title" => "O que Incluí",
                        "caption" => "",
                        "caption-position" => "bottom",
                        "title-link-text" => "",
                        "title-link" => "",
                        "title-link-target" => "_self",
                        "text-align" => "left",
                        "left-media-type" => "icon",
                        "left-icon" => "fa fa-plus",
                        "left-image" => "",
                        "heading-tag" => "h6",
                        "icon-font-size" => "18px",
                        "title-font-size" => "24px",
                        "title-font-weight" => "600",
                        "title-font-style" => "normal",
                        "title-font-letter-spacing" => "0px",
                        "title-font-uppercase" => "disable",
                        "caption-font-size" => "16px",
                        "caption-font-weight" => "400",
                        "caption-font-style" => "italic",
                        "caption-font-letter-spacing" => "0px",
                        "caption-font-uppercase" => "disable",
                        "title-color" => "",
                        "title-link-hover-color" => "",
                        "caption-color" => "",
                        "caption-spaces" => "10px",
                        "media-margin-right" => "15px",
                        "padding-bottom" => "35px",
                    )
                ),
                array(
                    "template" => "element",
                    "type" => "text-box",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "content" => $product->oqueInclui,
                        "text-align" => "left",
                        "apply-the-content-filter" => "disable",
                        "enable-p-space" => "enable",
                        "font-size" => "",
                        "content-line-height" => "",
                        "content-font-weight" => "",
                        "content-letter-spacing" => "",
                        "content-text-transform" => "",
                        "tablet-font-size" => "",
                        "mobile-font-size" => "",
                        "text-color" => "",
                        "margin-left" => "",
                        "margin-right" => "",
                        "padding-bottom" => "30px",
                    ),
                ),
            )
        );
        return $tag;
    }

    function tag_termos_viagem($product) {

        $tag = array(
            "template" => "wrapper",
            "type" => "background",
            "value" => array(
                "id" => "atencao",
                "class" => "",
                "content-layout" => "boxed",
                "max-width" => "",
                "enable-space" => "disable",
                "hide-this-wrapper-in" => "none",
                "animation" => "none",
                "animation-location" => "0.8",
                "full-height" => "disable",
                "decrease-height" => "0px",
                "centering-content" => "disable",
                "background-type" => "color",
                "background-colo" => "",
                "background-image" => "",
                "background-image-style" => "cover",
                "ackground-image-position" => "center",
                "background-video-url" => "",
                "background-video-url-mp4" => "",
                "background-video-url-webm" => "",
                "background-video-url-ogg" => "",
                "background-video-image" => "",
                "background-pattern" => "pattern-1",
                "pattern-opacity" => "1",
                "parallax-speed" => "0.8",
                "overflow" => "visible",
                "border-type" => "none",
                "border-pre-spaces" => array(
                    "top" => "20px",
                    "right" => "20px",
                    "bottom" => "20px",
                    "left" => "20px",
                    "settings" => "link",
                ),
                "border-width" => array(
                    "top" => "1px",
                    "right" => "1px",
                    "bottom" => "1px",
                    "left" => "1px",
                    "settings" => "link",
                ),
                "border-color" => "#ffffff",
                "border-style" => "solid",
                "padding" => array(
                    "top" => "70px",
                    "right" => "0px",
                    "bottom" => "30px",
                    "left" => "0px",
                    "settings" => "unlink",
                ),
                "margin" => array(
                    "top" => "0px",
                    "right" => "0px",
                    "bottom" => "0px",
                    "left" => "0px",
                    "settings" => "link",
                ),
                "skin" => "Blue Icon",
            ),
            "items" => array(
                array(
                    "template" => "element",
                    "type" => "title",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "title" => "Informações Importantes",
                        "caption" => "",
                        "caption-position" => "bottom",
                        "title-link-text" => "",
                        "title-link" => "",
                        "title-link-target" => "_self",
                        "text-align" => "left",
                        "left-media-type" => "icon",
                        "left-icon" => "fa fa-info-circle",
                        "left-image" => "",
                        "heading-tag" => "h6",
                        "icon-font-size" => "18px",
                        "title-font-size" => "24px",
                        "title-font-weight" => "600",
                        "title-font-style" => "normal",
                        "title-font-letter-spacing" => "0px",
                        "title-font-uppercase" => "disable",
                        "caption-font-size" => "16px",
                        "caption-font-weight" => "400",
                        "caption-font-style" => "italic",
                        "caption-font-letter-spacing" => "0px",
                        "caption-font-uppercase" => "disable",
                        "title-color" => "",
                        "title-link-hover-color" => "",
                        "caption-color" => "",
                        "caption-spaces" => "10px",
                        "media-margin-right" => "15px",
                        "padding-bottom" => "35px",
                    )
                ),
                array(
                    "template" => "element",
                    "type" => "text-box",
                    "value" => array(
                        "id" => "",
                        "class" => "",
                        "content" => $product->details,
                        "text-align" => "left",
                        "apply-the-content-filter" => "disable",
                        "enable-p-space" => "enable",
                        "font-size" => "",
                        "content-line-height" => "",
                        "content-font-weight" => "",
                        "content-letter-spacing" => "",
                        "content-text-transform" => "",
                        "tablet-font-size" => "",
                        "mobile-font-size" => "",
                        "text-color" => "",
                        "margin-left" => "",
                        "margin-right" => "",
                        "padding-bottom" => "30px",
                    ),
                ),
            )
        );

        return $tag;
    }

    function isPossuiRoteiro() {
        return array(
            "id" => "itinerary",
            "title" => "Roteiro",
        );
    }

    function isPossuiOQueInclui() {
        return array(
            "id" => "oqueinclui",
            "title" => "Incluí",
        );
    }

    function isPoussuiValoresCondicoes() {
        return array(
            "id" => "valores",
            "title" => "Valores",
        );
    }

    function isPossuiInformacoesAdicionais() {
        return array(
            "id" => "atencao",
            "title" => "+Info",
        );
    }

    function tourmaster_save_tour_meta( $agendamento, $product, $post_id ){

        $category = $this->products_model->getCategoryById($product->category_id);

        $this->save_terms_category($category, $post_id);
        $this->save_terms_destiny($product, $post_id);

        $gdlr_core = array(
            array(
                "template" => "wrapper",
                "type" => "background",
                "value" => array(
                    "id" => "",
                    "class" => "",
                    "ontent-layout" => "full",
                    "max-width" => "",
                    "enable-space" => "enable",
                    "hide-this-wrapper-in" => "none",
                    "animation" => "none",
                    "animation-location" => "0.8",
                    "full-height" => "",
                    "ecrease-height" => "0px",
                    "centering-content" => "disable",
                    "background-type" => "color",
                    "background-color" => "",
                    "background-image" => "",
                    "background-image-style" => "cover",
                    "background-image-position" => "center",
                    "background-video-url" => "",
                    "background-video-url-mp4" => "",
                    "background-video-url-webm" => "",
                    "background-video-url-ogg" => "",
                    "background-video-image" => "",
                    "background-pattern" => "pattern-1",
                    "pattern-opacity" => "1",
                    "parallax-speed" => "0.8",
                    "overflow" => "visible",
                    "border-type" => "none",
                    "border-pre-spaces" => array(
                        "top" => "20px",
                        "right" => "20px",
                        "bottom" => "20px",
                        "left" => "20px",
                        "settings" => "link",
                    ),
                    "border-width" => array(
                        "top" => "1px",
                        "right" => "1px",
                        "bottom" => "1px",
                        "left" => "1px",
                        "settings" => "link",
                    ),
                    "border-color" => "#ffffff",
                    "border-style" => "solid",
                    "padding" => array(
                        "top" => "0px",
                        "right" => "0px",
                        "bottom" => "0px",
                        "left" => "0px",
                        "settings" => "unlink",
                    ),
                    "margin" => array(
                        "top" => "0px",
                        "right" => "0px",
                        "bottom" => "0px",
                        "left" => "0px",
                        "settings" => "link",
                    ),
                    "skin" => "",
                ),
                "items" => array(
                    array(
                        "template" => "element",
                        "type" => "content-navigation",
                        "value" => array(
                            "id" => "",
                            "class" => "",
                            "tabs" => array(
                                $this->isPossuiRoteiro(),
                                $this->isPossuiOQueInclui(),
                                $this->isPoussuiValoresCondicoes(),
                                $this->isPossuiInformacoesAdicionais(),
                                array(
                                    "id" => "photos",
                                    "title" => "Fotos",
                                ),
                            ),
                            "background-color" => "",
                            "nable-bottom-border" => "disable",
                            "rder-color" => "",
                            "slide-bar-color" => "",
                            "padding-bottom" => "0px",
                        )
                    )
                )
            ),

            $this->tag_roteiro_viagem($product),
            $this->tag_o_que_inclui_viagem($product),
            $this->tag_condicoes_viagem($product),
            $this->tag_termos_viagem($product),
            $this->tag_image_gallery($product, $post_id),
        );

        $meta_value  = $this->maybe_serialize( $this->tag_tourmaster_tour_option($agendamento, $product, $category)  );
        $gdlr_core  = $this->maybe_serialize( $gdlr_core  );

        $this->update_post_meta($post_id, 'tourmaster-tour-option', $meta_value);
        $this->update_post_meta($post_id, 'gdlr-core-page-builder', $gdlr_core);

        $thumbnail_id = $this->save_post_imagem_destaque($product, $post_id);

        if ($thumbnail_id != null) {
            $this->update_post_meta($post_id, '_thumbnail_id', $thumbnail_id );
        } else {
            $post = $this->is_exist_post($product->products_image_site_id);
            $this->update_post_meta($post_id, '_thumbnail_id', $post->ID);
        }

        $user = $this->site->getUser($this->session->userdata('user_id'));

        $this->update_post_meta($post_id, 'tourmaster-tour-agendamento', $agendamento->id);
        $this->update_post_meta($post_id, 'tourmaster-tour-url-sistema', base_url());
        $this->update_post_meta($post_id, 'tourmaster-tour-empresa', $this->session->userdata('cnpjempresa'));
        $this->update_post_meta($post_id, 'tourmaster-tour-vendedor', $user->biller_id);//TODO ID DO VENDEDOR

        $this->update_post_meta($post_id, 'tourmaster-tour-date', $agendamento->dataSaida);
        $this->update_post_meta($post_id, 'tourmaster-tour-date-avail',  $agendamento->dataSaida);

        //$this->update_post_meta($post_id, 'tourmaster-tour-price', $this->sma->formatMoney($product->precoExibicaoSite, $product->simboloMoeda));
        $this->update_post_meta($post_id, 'tourmaster-tour-price',  number_format($product->precoExibicaoSite,2,",","."));


        $this->update_post_meta($post_id, 'tourmaster-tour-discount', 'false');
        $this->update_post_meta($post_id, 'tourmaster-tour-duration', '1');
        $this->update_post_meta($post_id, 'tourmaster-view-count', 1);
        $this->update_post_meta($post_id, 'tourmaster-max-people', '');

        $this->update_post_meta($post_id, 'slide_template', '');
        $this->update_post_meta($post_id, '_wp_old_slug', '');
        $this->update_post_meta($post_id, '_dp_original', "");
        $this->update_post_meta($post_id, 'tourmaster-tour-rating', 'a:2:{s:5:"score";i:10;s:8:"reviewer";i:1;}');
        $this->update_post_meta($post_id, 'tourmaster-reminder-message', 'enable');
        $this->update_post_meta($post_id, 'tourmaster-thumbnail-link', 'single-tour');
        $this->update_post_meta($post_id, 'tourmaster-thumbnail-video-url', '');
        $this->update_post_meta($post_id, 'tourmaster-extra-booking-info', '');
        $this->update_post_meta($post_id, 'tourmaster-contact-detail-fields', '');
        $this->update_post_meta($post_id, 'tourmaster-enquiry-form-fields', '');
        $this->update_post_meta($post_id, 'tourmaster-enquiry-form-mail-content-admin', '');
        $this->update_post_meta($post_id, 'tourmaster-enquiry-form-mail-content-customer', '');
        $this->update_post_meta($post_id, 'tourmaster-book-in-advance', '');
        $this->update_post_meta($post_id, 'tourmaster-tour-price-range', '');
        $this->update_post_meta($post_id, 'tourmaster-min-people-per-booking', '');
        $this->update_post_meta($post_id, 'tourmaster-max-people-per-booking', '');
        $this->update_post_meta($post_id, 'tourmaster-tour-cc-mail', '');
        $this->update_post_meta($post_id, 'tourmaster-payment-notification', 'enable');
        $this->update_post_meta($post_id, 'tourmaster-review-option', 'a:0:{}');
        $this->update_post_meta($post_id, 'tourmaster-tour-rating-score', '10');
    }

    function maybe_serialize( $data ) {

        if ( is_array( $data ) || is_object( $data ) ) return serialize( $data );
        
        return $data;
    }

    private function update_post_meta($post_id, $meta_key, $meta_value) {
        $this->delete_post_meta($post_id, $meta_key);
        $this->insert_post_meta($post_id, $meta_key, $meta_value);
    }

    private function delete_post_meta($post_id, $meta_key) {
        $this->dbSite->delete('postmeta', array('post_id'=> $post_id, 'meta_key'=> $meta_key) );
    }

    private function insert_post_meta($post_id, $meta_key, $meta_value) {
        $data = array(
            'post_id'       => $post_id,
            'meta_key'      => $meta_key,
            'meta_value'    => $meta_value
        );
        $this->dbSite->insert('postmeta', $data);
    }
}