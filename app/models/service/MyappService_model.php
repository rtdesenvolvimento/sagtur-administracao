<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MyappService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');

    }

    public function onibus_ativo_marcacao($proxima_viagem) {

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $programacao = $this->AgendaViagemService_model->getProgramacaoById($proxima_viagem->programacaoId);
        $exibir_marcacao_assento = false;

        if ($programacao->habilitar_marcacao_area_cliente && $this->verificar_data_marcacao_assento($programacao)) {
            $transportes =  $this->ProdutoRepository_model->getTransportesRodoviario($proxima_viagem->product_id);
            if (!empty($transportes)) {
                foreach ($transportes as $tipoTransporte) {
                    $ativo = $tipoTransporte->status == 'ATIVO' ? true : false;
                    if ($ativo && $tipoTransporte->automovel_id) {
                        $exibir_marcacao_assento = true;
                    }
                }
            }
        }
        return $exibir_marcacao_assento;
    }

    private function verificar_data_marcacao_assento($programacao) {
        $dataHoje = date('Y-m-d', strtotime(date('Y-m-d')));
        $permitir = true;

        if ($programacao->data_inicio_marcacao != null && $programacao->data_inicio_marcacao != '0000-00-00') {
            if (strtotime($programacao->data_inicio_marcacao) <= strtotime($dataHoje)) {
                $permitir = true;
            } else {
                $permitir = false;
            }
        }

        if ($programacao->data_final_marcacao != null && $programacao->data_final_marcacao != '0000-00-00') {
            if (strtotime($programacao->data_final_marcacao) >= strtotime($dataHoje)) {
                $permitir = true;
            } else {
                $permitir = false;
            }
        }

        return $permitir;
    }

}