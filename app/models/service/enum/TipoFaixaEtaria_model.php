<?php defined('BASEPATH') OR exit('No direct script access allowed');

class TipoFaixaEtaria_model extends CI_Model
{
    const ADULTO = "adulto";
    const CRIANCA = "crianca";
    const BEBE_COLO = "bebe";

    public function __construct()
    {
        parent::__construct();
    }

}