<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        //integracoes
        $this->load->model('service/signature/AutentiqueService_model');
        $this->load->model('service/signature/PlugsignService_model');

        //service
        $this->load->model('service/FolderService_model', 'FolderService_model');
        $this->load->model('service/ContractEventService_model', 'ContractEventService_model');

        //repository
        $this->load->model('repository/DocumentRepository_model', 'DocumentRepository_model');
        $this->load->model('repository/FolderRepository_model', 'FolderRepository_model');

        //model
        $this->load->model('model/Document_model', 'Document_model');
        $this->load->model('model/DocumentCustomer_model', 'DocumentCustomer_model');
        $this->load->model('model/Signatures_model', 'Signatures_model');
        $this->load->model('model/Folder_model', 'Folder_model');


        $this->upload_path = 'assets' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . $this->session->userdata('cnpjempresa') . DIRECTORY_SEPARATOR;
    }

    public function save($document = array())
    {
        try {

            $user = $this->getUser($document);

            $biller = $this->site->getCompanyByID($user->biller_id);

            $this->__iniciaTransacao();

            $reference = $this->site->getReference('doc');

            //$docuemnt = new Document_model();

            $document->document_status = Document_model::DOCUMENT_STATUS_DRAFT;
            $document->reference_no = $reference;

            $document->biller_id    = $biller->id;
            $document->biller       = $biller->name;

            $document->created_by   = $user->id;
            $document->created_at   = date('Y-m-d H:i:s');

            $document->update_by    = $user->id;

            $document_id = $this->__save('documents', $document->__toArray());

            $this->site->updateReference('doc');

            $this->ContractEventService_model->draft($user, $document_id);

            $this->__confirmaTransacao();

            return $document_id;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function rename_document($document): bool
    {
        try {

            $this->__iniciaTransacao();

            if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
                throw new Exception('Documento cancelado não pode ser renomeado');
            }

            $document->update_by =  $this->getUser($document)->id;
            $document->update_at = date('Y-m-d H:i:s');

            $documentOld = new Document_model($document->id);

            $atualizou = $this->__edit('documents', $document->__toArray(), 'id', $document->id);

            if ($documentOld->name != $document->name) {
                $this->PlugsignService_model->editDocument($document);
                $this->ContractEventService_model->rename_document($this->getUser($document), $document->id, $documentOld->name, $document->name);
            }

            $this->__confirmaTransacao();

            return $atualizou;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }
    public function edit($document): bool
    {
        try {

            //$document = new Document_model();

            $this->__iniciaTransacao();

            if ($document->id == null) {
                throw new Exception('Documento não encontrado');
            }

            $user = $this->getUser($document);

            //aqui vide configuraco
            $document->is_observadores      = $this->Settings->is_observadores;
            $document->observadores         = $this->Settings->observadores;

            $document->monitorar_validade   = $this->Settings->monitorar_validade;
            $document->validade_meses       = $this->Settings->validade_meses;
            $document->enviar_sequencial    = $this->Settings->enviar_sequencial;

            //log de alteracao
            $document->update_by = $user->id;
            $document->update_at = date('Y-m-d H:i:s');

            $atualizou = $this->__edit('documents', $document->__toArray(), 'id', $document->id);

            if ($atualizou) {

                $this->DocumentRepository_model->deleteSignaturesAll($document->id);

                foreach ($document->getSignatories() as $signature) {

                    $signature->status = Signatures_model::STATUS_UNSIGNED;
                    $signature->document_id = $document->id;
                    $signature->created_by  = $user->id;
                    $signature->created_at  = date('Y-m-d H:i:s');

                    $this->__save('document_signatures', $signature->__toArray());
                }
            }

            $this->__confirmaTransacao();

            return $atualizou;

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function addSignatory($document_id, $signature)
    {
        try {

            $this->__iniciaTransacao();

            if ($document_id == null) {
                throw new Exception('Documento não encontrado');
            }

            $document = new Document_model($document_id);

            $user = $this->getUser($document);

            $signature->status = Signatures_model::STATUS_UNSIGNED;
            $signature->document_id = $document_id;
            $signature->created_by = $user->id;
            $signature->created_at = date('Y-m-d H:i:s');

            $signatureID  = $this->__save('document_signatures', $signature->__toArray());

            $signature->id = $signatureID;

            //log de alteracao
            $document->update_by = $user->id;
            $document->update_at = date('Y-m-d H:i:s');

            $this->__edit('documents', $document->__toArray(), 'id', $document->id);

            $this->toSignSignatory($document_id, $signature);

            $this->__confirmaTransacao();

            $this->update_status_document($document_id);

            return $signatureID;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function sendSignature($document, $lanamentoInterno = TRUE): bool
    {
        try {

            $this->__iniciaTransacao();

            if ($document->id == null) {
                throw new Exception('Documento não encontrado.');
            }

            if ($document->document_status != Document_model::DOCUMENT_STATUS_DRAFT) {
                throw new Exception('Apenas documentos em rascunho podem ser enviados para assinatura, status atual do documento '.lang($document->document_status));
            }

            $autoAssinarResponsavel = false;

            if ($lanamentoInterno) {
                if ($this->autoAssinarComoResponsavelContrato()) {
                    $signatores = $document->getSignatories();
                    foreach ($signatores as $signature) {
                        if ($signature->is_biller) {
                            $document->setSignatories([$signature]);
                            $autoAssinarResponsavel = true;
                        }
                    }
                }
            }

            if ($salvo = $this->edit($document)) {

                $this->createDocument($document->id);
                $this->toSign($document->id);
                $this->autoAssinarResponsavel($document->id);

                if ($lanamentoInterno) {
                    if ($this->autoAssinarComoResponsavelContrato()) {
                        foreach ($signatores as $signature) {
                            if (!$signature->is_biller && $autoAssinarResponsavel) {
                                $this->addSignatory($document->id, $signature);
                            }
                        }
                    }
                }

            }

            $this->__confirmaTransacao();

            return $salvo;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function autoAssinarResponsavel($document_id) {

        if ($this->autoAssinarComoResponsavelContrato()) {

            $document = new Document_model($document_id);

            foreach ($document->getSignatories() as $signature) {
                if ($signature->is_biller) {
                    $this->sign($document->id, $signature->id);
                }
            }
        }
    }

    public function autoAssinarComoResponsavelContrato()
    {
        return $this->Settings->auto_assinar_biller_contrato;
    }

    public function createDocument($document_id)
    {
        $document = new Document_model($document_id);

        if ($document->document_status != Document_model::DOCUMENT_STATUS_DRAFT) {
            throw new Exception('Apenas documentos em rascunho podem ser enviados para assinatura, status atual do documento '.lang($document->document_status));
        }

        $this->createDocumentPlugsign($document);
    }

    public function protectDocument($document_id, $pass_user, $pass_owner)
    {
        return $this->PlugsignService_model->protectDocument(new Document_model($document_id), $pass_user, $pass_owner);
    }

    public function downloadDocument($document_id)
    {
        $document = new Document_model($document_id);

        $this->PlugsignService_model->downloadDocument($document);

        $this->ContractEventService_model->download_document($this->getUser($document), $document->id);
    }

    public function downloadOriginalDocument($document_id)
    {

        $document = new Document_model($document_id);
        $contentType = 'application/pdf';

        $file = $this->upload_path . $document->file_name;

        if (!file_exists($file)) {
            echo "Arquivo não encontrado.";
            exit;
        }

        $fileName = "file" . $this->getFileExtensionFromContentType($contentType);

        header('Content-Type: ' . $contentType);
        header('Content-Disposition: inline; filename="' . $fileName . '"');
        header('Content-Transfer-Encoding: binary');
        header('Accept-Ranges: bytes');
        header('Content-Length: ' . filesize($file));

        @readfile($file);
        exit;
    }

    private function getFileExtensionFromContentType($contentType): string
    {
        $map = [
            'application/pdf' => '.pdf',
            'application/msword' => '.doc',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => '.docx',
            'image/jpeg' => '.jpg',
            'image/png' => '.png',
            'image/gif' => '.gif',
        ];

        return $map[$contentType] ?? '';
    }

    public function sendDocumentTo($document_id, $message, $contact, $type)
    {
        $reenviado = $this->PlugsignService_model->sendDocumentTo(new Document_model($document_id), $message, $contact);

        if ($reenviado) {
            $message .= '<br/>Retorno: '. $reenviado;
            $this->ContractEventService_model->send_document_to($this->getUser(new Document_model($document_id)), $document_id, $contact, $message);
        }

        return  $reenviado;
    }

    public function cancel($document_id)
    {
        $document = new Document_model($document_id);

        $this->deleteDocument($document_id);//deletar o documento da integradora

        foreach ($document->getSignatories() as $signature) {
            $signature->status = Signatures_model::STATUS_CANCELED;
            $signature->canceled = true;
            $signature->date_canceled = date('Y-m-d H:i:s');
            $this->__edit('document_signatures', $signature->__toArray(), 'id', $signature->id);
        }

        $data_document = array(
            'document_status' => Document_model::DOCUMENT_STATUS_CANCELED,
            'update_by' => $this->getUser($document)->id,
            'update_at' => date('Y-m-d H:i:s'),
        );

        $this->__edit('documents', $data_document, 'id', $document->id);
    }

    public function deleteDocument($document_id): bool
    {
        $document = new Document_model($document_id);

        $this->PlugsignService_model->delete($document);

        $this->ContractEventService_model->canceled($this->getUser($document), $document->id);

        $filePath = $this->upload_path . $document->file_name;

        if (file_exists($filePath)) {
            return unlink($filePath);
        }

        return true;
    }

    public function resendWhatsapp($signatario_id)
    {
        return $this->PlugsignService_model->resendWhatsapp(new Signatures_model($signatario_id));
    }

    public function resendEmail($signatario_id)
    {
        return $this->PlugsignService_model->resendEmail(new Signatures_model($signatario_id));
    }

    public function resendToSignatories($document_id)
    {
        $document = new Document_model($document_id);

        foreach ($document->getSignatories() as $signature) {
            if ($signature->email != '') {
                $this->resendEmail($signature->id);
            } else if($signature->phone != '') {
                $this->resendWhatsapp($signature->id);
            }
        }
    }

    public function sendDocumentToWhatsApp($document_id)
    {
        $document = new Document_model($document_id);

        foreach ($document->getSignatories() as $signature) {

            if ($signature->status == Signatures_model::STATUS_REMOVED ||
                $signature->status == Signatures_model::STATUS_REJECTED ||
                $signature->status == Signatures_model::STATUS_CANCELED ) {
                return;
            }

            if (!$signature->is_biller && $document->sale_id && $document->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                $inv = $this->site->getSaleByID($document->sale_id);
                $customer = $this->site->getCompanyByID($signature->customer_id);

                if ($this->Settings->default_biller_contract) {
                    $biller = $this->site->getCompanyByID($this->Settings->default_biller_contract);
                } else {
                    $biller = $this->site->getCompanyByID($inv->biller_id);
                }

                $this->enviarMensagemWhatssAppContratoAssinado($customer, $biller, $inv, $document);
            }
        }

    }

    public function resendToSignatory($signatory_id, $document_id)
    {
        $document = new Document_model($document_id);

        foreach ($document->getSignatories() as $signature) {
            if ($signatory_id == $signature->id) {
                if ($signature->email != '') {
                    $this->resendEmail($signature->id);
                } else if($signature->phone != '') {
                    $this->resendWhatsapp($signature->id);
                }
            }
        }
    }

    public function cancelSignatory($signatario_id): bool
    {
        $signatario = new Signatures_model($signatario_id);
        $signatario->status = Signatures_model::STATUS_REMOVED;
        $signatario->removed = true;
        $signatario->date_removed = date('Y-m-d H:i:s');

        $removed = $this->PlugsignService_model->cancelSignatory($signatario);

        if ($removed) {

            $document = new Document_model($signatario->document_id);

            $this->ContractEventService_model->remove_signatory($this->getUser($document), $document->id, $signatario);

            $this->__edit('document_signatures', $signatario->__toArray(), 'id', $signatario->id);

            $this->update_status_document($signatario->document_id);
        }

        return false;
    }

    public function createDocumentPlugsign($document) {

        //$document = new Document_model();

        $documentModel = $this->PlugsignService_model->createDocument($document);

        if ($documentModel) {

            $user = $this->getUser($document);

            $data_document = array(
                'document_status'   => Document_model::DOCUMENT_STATUS_UNSIGNED,
                'public_id'         => $documentModel->id,
                'document_key'      => $documentModel->document_key,
                'url_link'          => $documentModel->download,
                'url_validate'      => 'https://app.plugsign.com.br/validate/'.$documentModel->document_key,
                'extension'         => $documentModel->extension,
                'size'              => $documentModel->size,
                'update_by'         => $user->id,
                'update_at'         => date('Y-m-d H:i:s'),
            );

            $this->editDocument($data_document, $document->id);

            $this->ContractEventService_model->unsigned($user, $document->id, 'plugsign', $documentModel->download);

            //deletar o arquivo do servidor
            $fileExtensions = ['pdf', 'docx'];

            foreach ($fileExtensions as $extension) {
                $filePath = $this->upload_path . pathinfo($document->file_name, PATHINFO_FILENAME) . '.' . $extension;
                if (file_exists($filePath)) {
                    unlink($filePath);
                }
            }

            // Excluir todos os arquivos com extensão .tmp no diretório
            $tmpFiles = glob($this->upload_path . '*.tmp');
            foreach ($tmpFiles as $tmpFile) {
                if (file_exists($tmpFile)) {
                    unlink($tmpFile);
                }
            }

        }
    }

    /**
     * @throws Exception
     */
    public function toSign($document_id) {

        $document = new Document_model($document_id);

        $documentSignatoriesCreated = $this->PlugsignService_model->toSign($document, $document->getSignatories());

        $this->toSignDocument($document, $document->getSignatories(), $documentSignatoriesCreated);
    }

    /**
     * @throws Exception
     */
    public function toSignSignatory($document_id, $signatory) {

        $document = new Document_model($document_id);

        $documentSignatoriesCreated = $this->PlugsignService_model->toSign($document, [$signatory]);

        $this->toSignDocument($document, [$signatory], $documentSignatoriesCreated);
    }

    public function sign($document_id, $signatory_id): bool
    {
        $signatory = new Signatures_model($signatory_id);

        $customerDocument = $this->PlugsignRepository_model->searchClientDocument($signatory->customer_id);

        $document = new Document_model($document_id);
        $customerDocument = new DocumentCustomer_model($customerDocument->id);

        if ($signatory->is_biller) {
            $sign = $this->PlugsignService_model->sign($document, $customerDocument);
        } else {
            $sign = $this->PlugsignService_model->sign($document, $customerDocument);
            //$sign = $this->PlugsignService_model->sign_key($document, $signatory);
        }

        if ($sign) {
            $signatory->status = Signatures_model::STATUS_SIGNED;
            $signatory->signed = true;
            $signatory->date_signature = date('Y-m-d H:i:s');
            $this->__edit('document_signatures', $signatory->__toArray(), 'id', $signatory->id);

            $this->ContractEventService_model->signed($this->getUser($document), $document->id, $signatory);
        }

        return $this->update_status_document($document_id);
    }

    private function update_status_document($document_id): bool
    {
        $document = new Document_model($document_id);

        $total_signed = 0;
        $total_signatories = 0;

        foreach ($document->getSignatories() as $signatory) {

            if ($signatory->status == 'signed') {
                $total_signed++;
            }

            if ($signatory->status != 'removed' && $signatory->status != 'rejected') {
                $total_signatories++;
            }
        }

        $faltaAssinara = ($total_signatories -  $total_signed);

        if ($faltaAssinara == 0) {
            return $this->__edit('documents',  array('document_status' =>  Document_model::DOCUMENT_STATUS_COMPLETED), 'id', $document->id);
        } else if ($faltaAssinara == $total_signatories) {
            return $this->__edit('documents',  array('document_status' =>  Document_model::DOCUMENT_STATUS_UNSIGNED), 'id', $document->id);
        } else {
            return $this->__edit('documents',  array('document_status' =>  Document_model::DOCUMENT_STATUS_PENDING), 'id', $document->id);
        }
    }

    private function toSignDocument($document, $signatories, $documentSignatoriesCreated)
    {

        if (empty($documentSignatoriesCreated)) {
            throw new Exception('Não foi possível encontrar nenhuma signatário para este documento');
        }

        $contador = 0;

        foreach ($signatories as $signature) {

            $signatureIntegracao = $documentSignatoriesCreated->getSignature($contador);

            $signature->public_id = $signatureIntegracao->id;
            $signature->url_link = 'https://app.plugsign.com.br/view/'.$document->document_key.'?signingKey='.$signatureIntegracao->id;

            $this->__edit('document_signatures', $signature->__toArray(), 'id', $signature->id);

            $this->ContractEventService_model->to_sign_document($this->getUser($document), $document->id, $signature);

            $contador++;
        }
    }

    public function createDocumentAutentique($document) {

        $documentCreated = $this->PlugsignService_model->createDocument($document);

        if ($documentCreated) {

            $data_document = array(
                'document_status'   => 'unsigned',
                'public_id'         => $documentCreated->id,
                'document_key'      => $documentCreated->document_key,
                'url_link'          => $documentCreated->download,
                'extension'         => $documentCreated->extension,
                'size'              => $documentCreated->size,
            );

            $this->editDocument($data_document, $document->id);

            foreach ($documentCreated->signatures as $signature) {
                $data_signature = array(
                    'name' => $signature->name,
                    'email' => $signature->name,
                    'action' => $signature->action,
                    'url_link' => $signature->link,
                    'public_id' => $signature->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => $this->getUser($document)->id,
                );
                $this->addSignature( $document->id, $data_signature);
            }
        }
    }

    public function editDocument($data, $id) {
        $this->__edit('documents', $data, 'id', $id);
    }

    public function addSignature($document_id, $data): bool
    {
        $data['document_id'] = $document_id;

        if ($this->db->insert("signatures", $data)) {
            return true;
        }
        return false;
    }

    public function create_company_tecnospeed_plugsign_integration($warehouseID): bool
    {
        try {

            $this->__iniciaTransacao();

            $warehouse = $this->site->getWarehouseByID($warehouseID);

            if ($warehouse->public_id) {
                $this->PlugsignService_model->updateCompany($warehouse);
            } else {
                $this->PlugsignService_model->createCompany($warehouse);
            }

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @throws Exception
     */
    public function generateSaleContract($sale_id, $file_name = null, $vendaEmEdicao = false, $forcarEmissaoContrato = false)
    {
        $inv = $this->site->getSaleByID($sale_id);

        if ($this->gerarContratoFaturarVenda() && $this->gerarContratoVendaSite()) {
            return $this->gerarDocumentSale($inv, $file_name, $vendaEmEdicao);
        } else if($this->gerarContratoFaturarVenda()) {
            if ($this->naoGerarContratoVendaSite() && $this->vendaManual($inv)) {
                return $this->gerarDocumentSale($inv, $file_name, $vendaEmEdicao);
            } elseif($vendaEmEdicao) {
                return $this->gerarDocumentSale($inv, $file_name, $vendaEmEdicao);
            }
        } elseif ($forcarEmissaoContrato) {
            return $this->gerarDocumentSale($inv, $file_name, $vendaEmEdicao);
        }

        return false;
    }

    /**
     * @throws Exception
     */
    private function gerarDocumentSale($inv, $file_name = null, $vendaEmEdicao = false)
    {
        $itens = $this->site->getAllInvoiceItems($inv->id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $programacaoID = null;

        if ($inv->sale_status == 'cancel') {
            throw new Exception('A Venda encontra-se <b>CANCELADA</b> e não pode ser gerado o contrato.');
        }

        foreach ($itens as $item) {
            $programacaoID = $item->programacaoId;
        }

        if ($programacaoID == null) {
            throw new Exception('Não foi possível encontrar a programação da venda.');
        }

        $this->cancelAllContracts($inv->id, $vendaEmEdicao);

        if ($this->Settings->default_biller_contract) {
            $biller = $this->site->getCompanyByID($this->Settings->default_biller_contract);
        } else {
            $biller = $this->site->getCompanyByID($inv->biller_id);
        }

        $document_id = $this->save($this->preencherDocumentSale($inv, $programacaoID, $customer, $biller, $file_name));

        $documentGerado = new Document_model($document_id);

        $isVendaSite = $this->vendaViaSite($inv);

        $documentGerado->addSignature($this->getSignatoryBiller($biller));
        $documentGerado->addSignature($this->getSignatoryCustomer($customer, $isVendaSite, $vendaEmEdicao));

        $this->__edit('sales', array('contract_id' => $document_id), 'id', $inv->id);

        if ($this->gerarContratoEhEnviarSignatarios()) {

            if ($this->autoAssinarComoCliente()) {
                if ($isVendaSite && !$vendaEmEdicao) {
                    $this->sendSignature($documentGerado, FALSE);
                    $this->autoAssinarCustomer($documentGerado, $inv, $customer, $biller);
                } else {
                    $this->sendSignature($documentGerado);
                }

            } else {
                $this->sendSignature($documentGerado);
            }

        } else {
            $this->edit($documentGerado);
        }

        return $document_id;
    }

    public function cancelAllContracts($sale_id, $vendaEmEdicao = false)
    {
        $ducuments = $this->DocumentRepository_model->getDocumentsBySaleID($sale_id);

        if ($vendaEmEdicao) {
            foreach ($ducuments as $documentSale) {

                if ($documentSale->document_status == Document_model::DOCUMENT_STATUS_DRAFT) {
                    $this->cancel($documentSale->id);
                }

                if ($documentSale->document_status == Document_model::DOCUMENT_STATUS_UNSIGNED ||
                    $documentSale->document_status == Document_model::DOCUMENT_STATUS_PENDING ||
                    $documentSale->document_status == Document_model::DOCUMENT_STATUS_SIGNED ||
                    $documentSale->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                    throw new Exception('O contrato desta venda já foi enviado para assinatura e não pode ser enviado novamente.');
                }
            }

        } else {
            foreach ($ducuments as $documentSale) {
                if ($documentSale->document_status != Document_model::DOCUMENT_STATUS_CANCELED) {
                    $this->cancel($documentSale->id);
                }
            }
        }
    }

    public function autoAssinarComoCliente()
    {
        return $this->Settings->auto_assinar_customer_contrato;
    }

    public function gerarContratoEhEnviarSignatarios()
    {
        return $this->Settings->enviar_contrato_signatarios_venda;
    }

    public function vendaViaSite($inv): bool
    {
        return $inv->venda_link == '1';
    }

    public function vendaManual($inv): bool
    {
       return !$this->vendaViaSite($inv);
    }

    private function preencherDocumentSale($inv, $programacaoID, $customer, $biller, $file_name = null): Document_model
    {

        $name_contract = $customer->name.' | '.$inv->reference_no;
        $name_contract = str_replace(['&', "'"], '', $name_contract);

        $document = new Document_model();
        $document->name = $name_contract;
        $document->folder_id = $this->getFolder($programacaoID, $inv->id);
        $document->sale_id = $inv->id;

        $document->signature_type = $this->Settings->signature_type;
        $document->solicitar_selfie = $this->Settings->solicitar_selfie;
        $document->solicitar_documento =  $this->Settings->solicitar_documento;
        $document->auto_destruir_solicitacao = $this->Settings->auto_destruir_solicitacao;

        if ($this->Settings->auto_destruir_solicitacao) {
            $document->data_vencimento_contrato = date('Y-m-d', strtotime("+" . $this->Settings->validade_auto_destruir_dias . " day", strtotime(date('y-m-d'))));
        }

        $itens = $this->site->getAllInvoiceItems($inv->id);

        foreach ($itens as $item) {
            $programacao = $this->site->getAgendaViagemByID($item->programacaoId);
            break;
        }

        if (!empty($programacao)) {

            $product = $this->site->getProductByID($programacao->produto);
            $name_product = $this->sma->removeSpecialCharacters($product->name);
            $name_customer = $this->sma->removeSpecialCharacters(trim($customer->name));

            $message = 'Olá *'.$name_customer.'*, segue solicitação de assinatura de contrato de *' . $this->Settings->site_name . '*. Plano: *' . $name_product . '*.';
            $message .= '%0A%0A Clique no botão abaixo da mensagem *Responder solicitação* depois em *Aceitar* e você recebera o link do contrato para realizar assinatura eletrônica.';

            if ($biller) {
                $message .= '%0A%0A Alguma dúvida entre em contato com o %0ATelefone: ' . $biller->phone.' ';
            }

            $message .= ' _[MENSAGEM AUTOMÁTICA: NÃO RESPONDEMOS NESTE NÚMERO.]_';

            $document->note = urldecode($message);
            $document->signatories_note = urldecode($message);
        }

        $document->biller_id = $inv->biller_id;
        $document->biller = $inv->biller;

        if ($file_name) {
            $document->file_name = $file_name;
        } else {
            $document->file_name = $inv->file_name_contract;
        }

        if (!$document->file_name) {
            throw new Exception('Não foi possível encontrar o arquivo pdf do contrato para enviar para assinatura.');
        }

        return $document;
    }

    private function getSignatoryCustomer($customer, $isVendaSite, $edit): Signatures_model
    {
        $signature_customer = new Signatures_model();
        $signature_customer->is_biller      = 0;
        $signature_customer->name           = $customer->name;
        $signature_customer->customer_id    = $customer->id;
        $signature_customer->customer       =  $customer->name;

        if ($this->autoAssinarComoCliente() && $isVendaSite && !$edit) {
            $signature_customer->email = $customer->email;
        } else {
            $signature_customer->phone = $customer->cf5 != null ? $customer->cf5 : $customer->phone;
            $signature_customer->email = $customer->email;
        }

        return $signature_customer;
    }

    private function getSignatoryBiller($biller): Signatures_model
    {
        $signature_biller = new Signatures_model();
        $signature_biller->name = $biller->name;
        $signature_biller->is_biller = 1;

        if ($this->autoAssinarComoResponsavelContrato()) {
            $signature_biller->email = $biller->email;
        } else {
            $signature_biller->phone = $biller->cf5 != null ? $biller->cf5 : $biller->phone;
            $signature_biller->email = $biller->email;
        }

        $signature_biller->biller_id = $biller->id;
        $signature_biller->biller =  $biller->name;

        return $signature_biller;
    }

    /**
     * @throws Exception
     */
    private function autoAssinarCustomer($documentGerado, $inv, $customer, $biller)
    {
        $documentGerado = new Document_model($documentGerado->id);

        foreach ($documentGerado->getSignatories() as $signature) {

            if ($signature->customer_id) {

                $customerDocument = $this->PlugsignRepository_model->searchClientDocument($signature->customer_id);

                if($customerDocument->public_id) {

                    $this->sign($documentGerado->id, $signature->id);

                    $phone = $customer->cf5 != null ? $customer->cf5 : $customer->phone;

                    $this->__edit('document_signatures',  array('phone' => $phone), 'id', $signature->id);

                    $this->enviarMensagemWhatssAppContratoAssinado($customer, $biller, $inv, $documentGerado);
                }
            }
        }
    }

    private function enviarMensagemWhatssAppContratoAssinado($customer, $biller, $inv, $documentGerado){

        $phone = $customer->cf5 != null ? $customer->cf5 : $customer->phone;

        if ($phone) {

            $customer_name = trim($customer->name);

            $message = 'Olá *'. $customer_name .'*, segue seu contrato *ASSINADO* de *' . $this->Settings->site_name . '*.%0AMensagem: *' . $inv->note . '.*';

            $image = '';

            if ($biller) {
                $message .= '%0A%0AAlguma dúvida entre em contato:%0ATelefone: ' . $biller->phone;
                $image = base_url() . 'assets/uploads/logos/'.$biller->logo;
            }

            $message .= '%0A%0A*Documento com validade jurídica Lei Nº 14.063 de 23 de setembro de 2020.* %0A*[MENSAGEM AUTOMÁTICA: NÃO RESPONDEMOS NESTE NÚMERO.]*';

            $message .= '%0A%0A*Link para download do contrato:*';

            //$this->sendDocumentTo($documentGerado->id, $message, $phone, 'whatsapp');

            $this->load->model('service/SendMessageService_model', 'SendMessageService_model');
            //$this->SendMessageService_model->send_text($message, '55'.$phone);

            //$contract_link = base_url().'apputil/downloadDocumentByContractID/'.$documentGerado->id.'?token='.$this->session->userdata('cnpjempresa'); ;
            //$this->SendMessageService_model->send_document_pdf($contract_link, 'CONTRATO ASSINADO DE '.$documentGerado->name, '55'.$phone);

            $this->SendMessageService_model->send_link($message, $documentGerado->url_link, $customer_name, $customer_name, $image,  '55'.$phone);

            $this->ContractEventService_model->send_document_pdf($this->getUser($documentGerado), $documentGerado->id, $phone, $message);

        }

    }

    private function getFolder($programacaoID, $sale_id)
    {
        $folder      = $this->FolderRepository_model->getFoldersByProgramacao($programacaoID);

        $programacao = $this->site->getAgendaViagemByID($programacaoID);
        $product     = $this->site->getProductByID($programacao->produto);
        $user        = $this->site->getUserByBiller($this->site->getSaleByID($sale_id)->biller_id);

        if (!$folder) {
            $folder = new Folder_model();
            $folder->name = $product->name.' | '.$this->sma->hrsd($programacao->dataSaida). ' ' .date('H:i', strtotime($programacao->horaSaida));
            $folder->programacao_id = $programacaoID;
            $folder->note = 'Criado pelo sitema';
            $folder_id = $this->FolderService_model->addFolder($folder, $user);
        } else {
            $folder = new Folder_model($folder->id);
            $folder->name = $product->name.' | '.$this->sma->hrsd($programacao->dataSaida). ' ' .date('H:i', strtotime($programacao->horaSaida));
            $this->FolderService_model->editFolder($folder);
            $folder_id = $folder->id;
        }

        return $folder_id;
    }

    public function getUser($document)
    {
        if ($this->session->userdata('user_id')) {
            $user = $this->site->getUser($this->session->userdata('user_id'));
            if ($user->biller_id) {
                return $user;
            } elseif ($document->sale_id) {
                return $this->site->getUserByBiller($this->site->getSaleByID($document->sale_id)->biller_id);
            } else {
                return $this->site->getUserByBiller($this->Settings->default_biller);
            }
        } elseif ($document->sale_id) {
            return $this->site->getUserByBiller($this->site->getSaleByID($document->sale_id)->biller_id);
        }

        return null;
    }

    public function webhook($obj)
    {
        if (!empty($obj->status)) {

            $document = $this->DocumentRepository_model->getDocumentByDocumentKey($obj->document_key);

            if ($obj->status == 'Signed') {

                $encontrouSignatorio = $this->DocumentRepository_model->getSignatoryBySigningKey($obj->signing_key);

                if ($encontrouSignatorio) {
                    $signatory = new Signatures_model($encontrouSignatorio->id);
                    $signatory->status = Signatures_model::STATUS_SIGNED;
                    $signatory->signed = true;
                    $signatory->date_signature = date('Y-m-d H:i:s');
                    $this->__edit('document_signatures', $signatory->__toArray(), 'id', $signatory->id);

                    $this->ContractEventService_model->signed($this->getUser($document), $document->id, $signatory);

                }

            } else if ($obj->status == 'Declined') {

                $encontrouSignatorio = $this->DocumentRepository_model->getSignatoryBySigningKey($obj->signing_key);

                if ($encontrouSignatorio) {
                    $signatory = new Signatures_model($encontrouSignatorio->id);
                    $signatory->status = Signatures_model::STATUS_REJECTED;
                    $this->__edit('document_signatures', $signatory->__toArray(), 'id', $signatory->id);

                    $this->ContractEventService_model->declined($this->getUser($document), $document->id, $signatory);
                }
            }

            $this->update_status_document($document->id);
        } else {
            echo 'nao encontrou';
        }
    }

    public function createWebhook()
    {
        return $this->PlugsignService_model->createWebhook();
    }

    private function gerarContratoFaturarVenda(): bool
    {
        return (bool) $this->Settings->gerar_contrato_faturamento_venda;
    }

    private function gerarContratoVendaSite(): bool
    {
        return (bool) $this->Settings->gerar_contrato_faturamento_venda_site;
    }

    private function naoGerarContratoVendaSite(): bool
    {
        return !$this->gerarContratoVendaSite();
    }
}