<?php defined('BASEPATH') OR exit('No direct script access allowed');

class HorarioTrabalhoService_model extends CI_Model
{
    public function __construct() {
        parent::__construct();

        $this->load->model('repository/AgendamentoRepository_model', 'AgendamentoRepository_model');

        $this->load->model('model/HorarioTrabalho_model', 'HorarioTrabalho_model');
    }

    public function getHorarioComercial() {
        return $this->buscarOuFalhar(1);
    }

    function buscarOuFalhar($id) {

        if(!$this->__exist('horario_atendimento', $id)) throw new Exception('Horário de atendimento não configurado corretamente!');

        return $this->__getByIdToModel('horario_atendimento', $id, new HorarioTrabalho_model());
    }

    public function agendamentosEncontrado($data, $dataAte, $horarioInicio, $horarioTermino) {
        return $this->AgendamentoRepository_model->agendamentosEncontrado($data,$dataAte,  $horarioInicio, $horarioTermino);
    }
}