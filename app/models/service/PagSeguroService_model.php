<?php defined('BASEPATH') OR exit('No direct script access allowed');

class PagSeguroService_model extends CI_Model
{

    const STATUS_CANCELADA = 'CANCELADA';
    const MOEDA = 'BRL';
    const BRA = 'BRA';
    const SN = 'SN';
    const CNPJ = 'CNPJ';
    const CPF = 'CPF';
    const MAX_AGE = (86400 * 3);
    const EMAL_SANDBOX = 'teste@sandbox.pagseguro.com.br';

    private $sadbox = true;

    public function __construct() {
        parent::__construct();

        $this->load->config('pagseguro');
        $this->load->library('pagsegurolibrary');

        $this->lang->load('pagseguro', $this->Settings->user_language);

        $this->load->model('dto/BaixaCobrancaDTO_model', 'BaixaCobrancaDTO_model');
        $this->load->model('dto/BaixaFaturaCobrancaDTO_model', 'BaixaFaturaCobrancaDTO_model');
        $this->load->model('dto/BaixaPagamentoFaturaCobrancaDTO_model', 'BaixaPagamentoFaturaCobrancaDTO_model');

        $this->load->model('settings_model');
        $this->load->model('Financeiro_model');
        $this->load->model('Sales_model');
    }

    public function configurarIntegracao() {

        $pagSeguro = $this->settings_model->getPagSeguroSettings();

        if ($pagSeguro->active) {

            $this->sadbox = $pagSeguro->sandbox;
            if ($pagSeguro->sandbox) $token = $pagSeguro->account_token_sandbox;
            else $token = $pagSeguro->account_token;

            return  new PagSeguroAccountCredentials($pagSeguro->account_email, $token);
        }

        return null;
    }

    public function emitirCobranca($cobranca) {

        $this->load->model('Financeiro_model');

        //$cobranca = new CobrancaDTO_model();
        //$pessoaCobranca = new PessoaCobrancaDTO_model();

        $paymentRequest = $this->preencherPagSeguroPayment($cobranca);

        $dados = array(
            'status' => Financeiro_model::STATUS_ABERTA,
            'tipo' => $cobranca->tipoCobranca,
            'fatura' => $cobranca->fatura,
            'integracao' => 'pagseguro',
        );

        if ($this->isBoleto($cobranca->tipoCobranca)) {
            $paymentRequest = $this->emitirBoleto($paymentRequest, $cobranca);
        }

        if ($this->isLinkPagamento($cobranca->tipoCobranca)) {
            $paymentRequest = $this->emitirLinkPagamento($paymentRequest, $cobranca);
        }

        if ($this->isCartaoDeCreditoTransparent($cobranca->tipoCobranca)) {
            $paymentRequest = $this->emitirCartaoCreditoTransparente($paymentRequest, $cobranca);

            $dados['status'] = 'PENDENTE';
        }

        try {

            if ($this->isCartaoDeCredito($cobranca->tipoCobranca)) {
                $transaction = $paymentRequest->checkout($this->configurarIntegracao());
            } else {
                $transaction = $paymentRequest->register($this->configurarIntegracao());
            }

            $dtoRetornoCobranca = new RetornoCobrancaDTO_model();

            $dados['code'] = $transaction->getCode();
            $dados['log']  = $this->sma->tirarAcentos(print_r($transaction, true));

            if ($this->isLinkPagamento($cobranca->tipoCobranca)) {
                $status_detail = 'Created';
            } else {
                $status_detail = $transaction->getStatus()->getValue();
            }

            if ($status_detail == 1) {
                $status_detail = 'Aguardando pagamento';
            } else if ($status_detail == 2) {
                $status_detail = 'Em análise';
            } else if ($status_detail == 3) {
                $status_detail = 'Paga';
            } else if ($status_detail == 4) {
                $status_detail = 'Disponível';
            } else if ($status_detail == 5) {
                $status_detail = 'Em disputa';
            } else if ($status_detail == 6) {
                $status_detail = 'Devolvida';
            } else if ($status_detail == 7) {
                $status_detail = 'Cancelada';
            }

            $dados['status_detail'] = $status_detail;

            if ($this->isBoleto($cobranca->tipoCobranca)) $dados['link'] = $transaction->getPaymentLink();
            if ($this->isDebitoOnline($cobranca->tipoCobranca)) $dados['checkoutUrl'] = $transaction->getPaymentLink();
            if ($this->isLinkPagamento($cobranca->tipoCobranca)) $dados['checkoutUrl'] = $transaction->getUrlLink();
            //if ($this->isCartaoDeCreditoTransparent($cobranca->tipoCobranca)) $dados['checkoutUrl'] = $transaction->getUrlLink();

            $dtoRetornoCobranca->adicionarFatura($dados);
            $dtoRetornoCobranca->sucesso = true;
            $dtoRetornoCobranca->erro = '';

            return $dtoRetornoCobranca;

        } catch (PagSeguroServiceException $e) {

            $erros = $e->getErrors();

            if (count($erros)  == 0) throw new Exception($e->getFormattedMessage());

            $mensagem = '<ul>';

            foreach ($erros as $erro) {
                $mensagem .= '<li>'.$this->erroPagSeguro($erro->getCode()).' - '.$erro->getCode().'</li>';
            }

            $mensagem .= '</ul>';

            throw new Exception($mensagem);
            //throw new Exception('Desculpe, não foi possível processar o pagamento. Se optou pagar com cartão de crédito verifique as informações do seu cartão de crédito! Tente novamente.<br/>Se persistir o problema entre em contato com o vendedor.');
        }
    }

    private function emitirLinkPagamento($paymentRequest, $cobranca) {
        $paymentRequest->addItem($cobranca->fatura, substr($cobranca->instrucoes, 0, 80), 1, number_format($cobranca->valorVencimento, 2, '.', '' ) );

        return $paymentRequest;
    }

    private function emitirBoleto($paymentRequest, $cobranca) {
        $pessoaCobranca = $cobranca->pessoa;

        $paymentRequest->setShippingAddress(str_replace('-', '', str_replace('.', '', $pessoaCobranca->cep))
            , $pessoaCobranca->endereco
            , ($pessoaCobranca->numero != '' ? $pessoaCobranca->numero : PagSeguroService_model::SN)
            , $pessoaCobranca->complemento
            , $pessoaCobranca->bairro
            , $pessoaCobranca->cidade
            , $pessoaCobranca->estado, PagSeguroService_model::BRA);


        if ($pessoaCobranca->telefonePagSeguro) {
            $paymentRequest->setSenderPhone($pessoaCobranca->dddTelefonePagSeguro, $pessoaCobranca->telefonePagSeguro);
        } else {
            $paymentRequest->setSenderPhone($this->getDDD($pessoaCobranca->celular), $this->getTelefone($pessoaCobranca->celular));
        }

        $paymentRequest->addItem($cobranca->fatura, substr($cobranca->instrucoes, 0, 80), 1, number_format($cobranca->valorVencimento, 2, '.', '' ) );

        LogPagSeguro::info(print_r($paymentRequest, true));

        return $paymentRequest;
    }

    private function emitirCartaoCreditoTransparente($paymentRequest, $cobranca) {

        //$cobranca = new CobrancaDTO_model();
        //$pessoaCobranca = new PessoaCobrancaDTO_model();
        //$paymentRequest = new PagSeguroPaymentRequest();

        $pessoaCobranca = $cobranca->pessoa;
        $cpf =  $pessoaCobranca->cpfCnpj;
        $celular = $pessoaCobranca->celular;

        $paymentRequest->setCreditCardToken($cobranca->creditCardToken);
        $paymentRequest->setCreditCardHolderName(strtoupper($cobranca->cardName));

        $paymentRequest->setInstallmentQuantity($cobranca->numeroParcelas);
        $paymentRequest->setInstallmentValue(number_format($cobranca->totalParcelaPagSeguro, 2, '.', ''));

        if ($cobranca->cpfTitularCartao) $cpf = $cobranca->cpfTitularCartao;
        if ($cobranca->celularTitularCartao) $celular = $cobranca->celularTitularCartao;

        $paymentRequest->setCreditCardHolderCPF($this->getCPF($cpf));
        $paymentRequest->setCreditCardHolderBirthDate(date('d/m/Y', strtotime($pessoaCobranca->dataNascimento)));//TODO
        $paymentRequest->setCreditCardHolderAreaCode($this->getDDD($celular));
        $paymentRequest->setCreditCardHolderPhone($this->getTelefone($celular));
        //$paymentRequest->setNoInterestInstallmentQuantity(1);//TODO CONFIGURACAO

        $paymentRequest->setShippingAddress(
            $cobranca->cepTitularCartao
            , $cobranca->enderecoTitularCartao
            , ($cobranca->numeroEnderecoTitularCartao != '' ? $cobranca->numeroEnderecoTitularCartao : PagSeguroService_model::SN)
            , $cobranca->complementoEnderecoTitularCartao
            , $cobranca->bairroTitularCartao
            , $cobranca->cidadeTitularCartao
            , $cobranca->estadoTitularCartao, PagSeguroService_model::BRA);

        $paymentRequest->setSenderPhone($this->getDDD($pessoaCobranca->celular), $this->getTelefone($pessoaCobranca->celular));

        $paymentRequest->addItem($cobranca->fatura, substr($cobranca->instrucoes, 0, 80), 1, number_format($cobranca->valorVencimento, 2, '.', '' ) );

        return $paymentRequest;
    }

    private function preencherPagSeguroPayment($cobranca) {
        $pessoaCobranca = $cobranca->pessoa;

        $paymentRequest = new PagSeguroPaymentRequest();
        $paymentRequest->setSenderHash($cobranca->senderHash);
        $paymentRequest->setCurrency(PagSeguroService_model::MOEDA);
        $paymentRequest->setReference($cobranca->fatura);
        $paymentRequest->setShippingType(3);
        $paymentRequest->setShippingCost(0);
        $paymentRequest->setExtraAmount(0);
        $paymentRequest->setMaxAge(self::MAX_AGE);
        $paymentRequest->setPaymentMethod($cobranca->tipoCobranca);
        $paymentRequest->setSenderName(strtoupper(substr($pessoaCobranca->nome,0,40)));
        $paymentRequest->setBankName('itau');//TODO VERIFICAR NO MOVIMENTADOR QUAL BANCO
        $paymentRequest->setNotificationURL('https://sistema.sagtur.com.br/infrastructure/PagSeguroController/notification?empresa='.$this->session->userdata('cnpjempresa'));

        if ($this->sadbox) {
            $paymentRequest->setSenderEmail(PagSeguroService_model::EMAL_SANDBOX);
        } else {
            $paymentRequest->setSenderEmail($pessoaCobranca->email);
        }

        if ($this->isPessoaJuridica($pessoaCobranca)) {
            $paymentRequest->addSenderDocument(PagSeguroService_model::CNPJ, $pessoaCobranca->cpfCnpj);
        } else {
            $paymentRequest->addSenderDocument(PagSeguroService_model::CPF, $pessoaCobranca->cpfCnpj);
        }

        return $paymentRequest;
    }

    private function getCPF($cpf) {
        return str_replace ( '-', '', str_replace ( '.', '', $cpf));
    }

    private function getTelefone($telefone) {
        $telefone = trim($telefone);
        $telefone = str_replace( '-', '', $telefone);
        $telefone = str_replace( '(', '', $telefone);
        $telefone = str_replace( ')', '', $telefone);

        return trim(substr($telefone, 2 ,strlen($telefone)));
    }

    private function  getDDD($telefone) {
        $telefone = trim($telefone);
        $telefone = str_replace( '-', '', $telefone);
        $telefone = str_replace( '(', '', $telefone);
        $telefone = str_replace( ')', '', $telefone);

        return trim(substr($telefone, 0,2));
    }

    private function isPessoaJuridica($pessoaCobranca) {
        return $pessoaCobranca->tipoPessoa == 'PJ';
    }

    private function isBoleto($tipo) {
        if ($tipo == 'boleto' || $tipo == 'carne' || $tipo == 'carne_boleto') return true;
        return false;
    }

    private function isLinkPagamento($tipo) {
        if ($tipo == 'carne_cartao') return true;
        return false;
    }

    private function isCartaoDeCredito($tipo) {
        if ($tipo == 'cartao' || $tipo == 'carne_cartao') return true;
        return false;
    }

    private function isCartaoDeCreditoTransparent($tipo) {
        if ($tipo == 'carne_cartao_transparent') return true;
        return false;
    }

    private function isDebitoOnline($tipo) {
        if ($tipo == 'debito_online') return true;
        return false;
    }

    public function cancelarCobranca($code) {
        try {
            if (!PagSeguroNotificationService::cancel($this->configurarIntegracao(), $code)) {
                throw new Exception('Não foi possível cancelar a fatura.');
            }
        } catch (PagSeguroServiceException $e) {
           // throw new Exception($e->getFormattedMessage());//todo comentado para passar no cancelamento
        }
    }

    public function estornarCobranca($code) {
        try {
            if (!PagSeguroNotificationService::refunds($this->configurarIntegracao(), $code)) {
                throw new Exception('Não foi possível cancelar a fatura.');
            }
        } catch (PagSeguroServiceException $e) {
            throw new Exception($e->getFormattedMessage());
        }
    }

    public function buscarPagamentoByCodeTransacao($code)
    {
        $transaction = false;

        if ($code) {
            $transaction = PagSeguroNotificationService::checkTransaction($this->configurarIntegracao(), $code);
        }

        if (is_object($transaction)) {
            return self::setTransacaoPagseguro($transaction);
        }
    }

    public function baixarPagamentoIntegracaoReference($reference)
    {
        $transaction = false;

        if ($reference) {
            $transaction = PagSeguroNotificationService::checkTransactionByReference($this->configurarIntegracao(), $reference);
        }

        if (is_object($transaction)) {
            return self::setTransacaoPagseguro($transaction);
        }
    }

    public function buscarDadosTransacaoByReference($reference) {
        $transaction = PagSeguroNotificationService::checkTransactionByReference($this->configurarIntegracao(), $reference);

        self::setTransacaoPagseguro($transaction);

        $this->setTransacaoPagseguro($transaction);

        return $transaction;
    }

    private function getTransaction(PagSeguroTransaction $transaction)
    {
        return array('reference' => $transaction->getReference(), 'status' => $transaction->getStatus()->getValue());
    }

    private function setTransacaoPagseguro($transaction = null)
    {

        $transactionObj = self::getTransaction($transaction);

        LogPagSeguro::info(print_r($transaction, true));

        $baixaCobranca = new BaixaCobrancaDTO_model();
        $baixaCobranca ->sucesso = true;

        $faturaCobranca = new BaixaFaturaCobrancaDTO_model();
        $faturaCobranca->code = $transaction->getCode();
        $faturaCobranca->log = $this->sma->tirarAcentos(print_r($transaction, true));
        $baixaCobranca->adicionarFatura($faturaCobranca);

        if ($transactionObj['status'] == 1) {
            $this->preencherPagamento($faturaCobranca, $transaction, 'AGUARDANDO_PAGAMENTO');
        }

        if ($transactionObj['status'] == 2) {
            $this->preencherPagamento($faturaCobranca, $transaction, 'AGUARDANDO_APROVACAO');
        }

        if ($transactionObj['status'] == 3) {
            $this->preencherPagamento($faturaCobranca, $transaction, 'CONFIRMED');
        }

        if ($transactionObj['status'] == 7) {
            $this->preencherPagamento($faturaCobranca, $transaction, 'CANCELADA');
        }

        return $baixaCobranca;
    }

    private function buscaTransactionNotificationByCodeNotification($notificationCode)
    {
        try {
            $transaction = PagSeguroNotificationService::checkTransaction($this->configurarIntegracao(), $notificationCode);
        } catch (PagSeguroServiceException $e) {
            echo $e->getFormattedMessage();
            die ();
        }

        return $transaction;
    }

    private function preencherPagamento($faturaCobranca, $transaction, $status) {

        $this->load->model('Financeiro_model');

        $paymentMethodType = new PagSeguroPaymentMethodType($transaction->getPaymentMethod()->getType()->getValue());
        $tipo = $paymentMethodType->getTypeFromValue($paymentMethodType->getValue());

        if ($tipo == 'BOLETO') {
            $taxaDeBoleto = 1;
            $valorPago  = $transaction->getGrossAmount() + $taxaDeBoleto;
            $pagamento = $this->adicionarPagamentoFatura($transaction, $valorPago, $tipo, $status);
            $faturaCobranca->adicionarPagamento($pagamento);
        } else if ($tipo == 'CREDIT_CARD') {

            $reference  = $transaction->getReference();
            $cobranca   = $this->financeiro_model->getCobrancaIntegracaoByFatura($reference);

            if ($this->isLinkPagamento($cobranca->tipo)) {
                if ($status != self::STATUS_CANCELADA) {
                    $this->__edit('fatura_cobranca', array('code' => $transaction->getCode()), 'id', $cobranca->id);
                    $pagamento = $this->adicionarPagamentoFatura($transaction, $transaction->getGrossAmount(), $tipo, $status);
                    $faturaCobranca->adicionarPagamento($pagamento);
                }
            } else {
                $this->__edit('fatura_cobranca', array('code' => $transaction->getCode()), 'id', $cobranca->id);
                $pagamento = $this->adicionarPagamentoFatura($transaction, $transaction->getGrossAmount(), $tipo, $status);
                $faturaCobranca->adicionarPagamento($pagamento);
            }
        }
    }

    private function adicionarPagamentoFatura($transaction, $valorPago, $tipo, $status) {
        $pagamento = new BaixaPagamentoFaturaCobrancaDTO_model();
        $code = $transaction->getCode();
        $dtPagamento = date('d/m/Y H:i:s');
        $taxas =  $transaction->getFeeAmount();

        $pagamento->status = $status;
        $pagamento->code = $code;
        $pagamento->dataPagamento = $dtPagamento;
        $pagamento->dataCredito = $dtPagamento;
        $pagamento->valorPago = $valorPago;
        $pagamento->taxa = $taxas;
        $pagamento->formaPagamento = $tipo;

        $pagamento->observacao = '
        Pagamento baixado pelo sistema automaticamente pelo modulo de integração PAGSEGURO. 
        <br/>Valor original R$'.$transaction->getGrossAmount().' 
        <br/>Taxas R$'.$transaction->getFeeAmount().' 
        <br/>Data do crédito '.$dtPagamento. ' 
        <br/>pagamento recebido em '.$tipo.' 
        <br/>Cód: '.$transaction->getCode().' 
        '.($transaction->getInstallmentCount() != null ? '<br/>nº parcelas '.$transaction->getInstallmentCount() : "");

        return $pagamento;
    }


    private function erroPagSeguro($eV) {
        $err = '';
        switch($eV)
        {
            case 5003: $err = "Falha de comunicação com a instituição financeira"; break;
            case 10000: $err = "Marca de cartão de crédito inválida"; break;
            case 10001: $err = "Número do cartão de crédito com comprimento inválido"; break;
            case 10002: $err = "Formato da data inválida"; break;
            case 10003: $err = "Campo de segurança CVV inválido"; break;
            case 10004: $err = "Código de verificação CVV é obrigatório"; break;
            case 10006: $err = "Campo de segurança com comprimento inválido"; break;
            case 53004: $err = "Quantidade inválida de itens"; break;
            case 53005: $err = "É necessário informar a moeda"; break;
            case 53006: $err = "Valor inválido para especificação da moeda"; break;
            case 53007: $err = "Referência inválida comprimento: {0}"; break;
            case 53008: $err = "URL de notificação inválida"; break;
            case 53009: $err = "URL de notificação com valor inválido"; break;
            case 53010: $err = "O e-mail do remetente é obrigatório"; break;
            case 53011: $err = "Email do remetente com comprimento inválido"; break;
            case 53012: $err = "Email do remetente está com valor inválido"; break;
            case 53013: $err = "O nome do remetente é obrigatório"; break;
            case 53014: $err = "Nome do remetente está com comprimento inválido"; break;
            case 53015: $err = "Nome do remetente está com valor inválido"; break;
            case 53017: $err = "Foi detectado algum erro nos dados do seu CPF"; break;
            case 53018: $err = "O código de área do remetente é obrigatório"; break;
            case 53019: $err = "Há um conflito com o código de área informado, em relação a outros dados seus"; break;
            case 53020: $err = "É necessário um telefone do remetente"; break;
            case 53021: $err = "Valor inválido do telefone do remetente"; break;
            case 53022: $err = "É necessário o código postal do endereço de entrega"; break;
            case 53023: $err = "Código postal está com valor inválido"; break;
            case 53024: $err = "O endereço de entrega é obrigatório"; break;
            case 53025: $err = "Endereço de entrega rua comprimento inválido: {0}"; break;
            case 53026: $err = "É necessário o número de endereço de entrega"; break;
            case 53027: $err = "Número de endereço de remessa está com comprimento inválido"; break;
            case 53028: $err = "No endereço de entrega há um comprimento inválido"; break;
            case 53029: $err = "O endereço de entrega é obrigatório"; break;
            case 53030: $err = "Endereço de entrega está com o distrito em comprimento inválido"; break;
            case 53031: $err = "É obrigatório descrever a cidade no endereço de entrega"; break;
            case 53032: $err = "O endereço de envio está com um comprimento inválido da cidade"; break;
            case 53033: $err = "É necessário descrever o Estado, no endereço de remessa"; break;
            case 53034: $err = "Endereço de envio está com valor inválido"; break;
            case 53035: $err = "O endereço do remetente é obrigatório"; break;
            case 53036: $err = "O endereço de envio está com o país em um comprimento inválido"; break;
            case 53037: $err = "O token do cartão de crédito é necessário"; break;
            case 53038: $err = "A quantidade da parcela é necessária"; break;
            case 53039: $err = "Quantidade inválida no valor da parcela"; break;
            case 53040: $err = "O valor da parcela é obrigatório."; break;
            case 53041: $err = "Conteúdo inválido no valor da parcela"; break;
            case 53042: $err = "O nome do titular do cartão de crédito é obrigatório"; break;
            case 53043: $err = "Nome do titular do cartão de crédito está com o comprimento inválido"; break;
            case 53044: $err = "O nome informado no formulário do cartão de Crédito precisa ser escrito exatamente da mesma forma que consta no seu cartão obedecendo inclusive, abreviaturas e grafia errada"; break;
            case 53045: $err = "O CPF do titular do cartão de crédito é obrigatório"; break;
            case 53046: $err = "O CPF do titular do cartão de crédito está com valor inválido"; break;
            case 53047: $err = "A data de nascimento do titular do cartão de crédito é necessária"; break;
            case 53048: $err = "TA data de nascimento do itular do cartão de crédito está com valor inválido"; break;
            case 53049: $err = "O código de área do titular do cartão de crédito é obrigatório"; break;
            case 53050: $err = "Código de área de suporte do cartão de crédito está com valor inválido"; break;
            case 53051: $err = "O telefone do titular do cartão de crédito é obrigatório"; break;
            case 53052: $err = "O número de Telefone do titular do cartão de crédito está com valor inválido"; break;
            case 53053: $err = "É necessário o código postal do endereço de cobrança"; break;
            case 53054: $err = "O código postal do endereço de cobrança está com valor inválido"; break;
            case 53055: $err = "O endereço de cobrança é obrigatório"; break;
            case 53056: $err = "A rua, no endereço de cobrança está com comprimento inválido"; break;
            case 53057: $err = "É necessário o número no endereço de cobrança"; break;
            case 53058: $err = "Número de endereço de cobrança está com comprimento inválido"; break;
            case 53059: $err = "Endereço de cobrança complementar está com comprimento inválido"; break;
            case 53060: $err = "O endereço de cobrança é obrigatório"; break;
            case 53061: $err = "O endereço de cobrança está com tamanho inválido"; break;
            case 53062: $err = "É necessário informar a cidade no endereço de cobrança"; break;
            case 53063: $err = "O item Cidade, está com o comprimento inválido no endereço de cobrança"; break;
            case 53064: $err = "O estado, no endereço de cobrança é obrigatório"; break;
            case 53065: $err = "No endereço de cobrança, o estado está com algum valor inválido"; break;
            case 53066: $err = "O endereço de cobrança do país é obrigatório"; break;
            case 53067: $err = "No endereço de cobrança, o país está com um comprimento inválido"; break;
            case 53068: $err = "O email do destinatário está com tamanho inválido"; break;
            case 53069: $err = "Valor inválido do e-mail do destinatário"; break;
            case 53070: $err = "A identificação do item é necessária"; break;
            case 53071: $err = "O ID do ítem está inválido"; break;
            case 53072: $err = "A descrição do item é necessária"; break;
            case 53073: $err = "Descrição do item está com um comprimento inválido"; break;
            case 53074: $err = "É necessária quantidade do item"; break;
            case 53075: $err = "Quantidade do item está irregular"; break;
            case 53076: $err = "Há um valor inválido na quantidade do item"; break;
            case 53077: $err = "O valor do item é necessário"; break;
            case 53078: $err = "O Padrão do valor do item está inválido"; break;
            case 53079: $err = "Valor do item está irregular"; break;
            case 53081: $err = "O remetente está relacionado ao receptor! Esse é um erro comum que só o lojista pode cometer ao testar como compras. O erro surge quando uma compra é realizada com os mesmos dados cadastrados para receber os pagamentos da loja ou com um e-mail que é administrador da loja"; break;
            case 53084: $err = "Receptor inválido! Esse erro decorre de quando o lojista usa dados relacionados com uma loja ou um conta do PagSeguro, como e-mail principal da loja ou o e-mail de acesso à sua conta não PagSeguro"; break;
            case 53085: $err = "Método de pagamento indisponível"; break;
            case 53086: $err = "A quantidade total do carrinho está inválida"; break;
            case 53087: $err = "Dados inválidos do cartão de crédito"; break;
            case 53091: $err = "O Hash do remetente está inválido"; break;
            case 53092: $err = "A Bandeira do cartão de crédito não é aceita"; break;
            case 53095: $err = "Tipo de transporte está com padrão inválido"; break;
            case 53096: $err = "Padrão inválido no custo de transporte"; break;
            case 53097: $err = "Custo de envio irregular"; break;
            case 53098: $err = "O valor total do carrinho não pode ser negativo"; break;
            case 53099: $err = "Montante extra inválido"; break;
            case 53101: $err = "Valor inválido do modo de pagamento. O correto seria algo do tipo default e gateway"; break;
            case 53102: $err = "Valor inválido do método de pagamento. O correto seria algo do tipo Credicard, Boleto, etc."; break;
            case 53104: $err = "O custo de envio foi fornecido, então o endereço de envio deve estar completo"; break;
            case 53105: $err = "As informações do remetente foram fornecidas, portanto o e-mail também deve ser informado"; break;
            case 53106: $err = "O titular do cartão de crédito está incompleto"; break;
            case 53109: $err = "As informações do endereço de remessa foram fornecidas, portanto o e-mail do remetente também deve ser informado"; break;
            case 53110: $err = "Banco EFT é obrigatório"; break;
            case 53111: $err = "Banco EFT não é aceito"; break;
            case 53115: $err = "Valor inválido da data de nascimento do remetente"; break;
            case 53117: $err = "Valor inválido do cnpj do remetente"; break;
            case 53122: $err = "O domínio do email do comprador está inválido. Você deve usar algo do tipo @sandbox.pagseguro.com.br"; break;
            case 53140: $err = "Quantidade de parcelas fora do limite. O valor deve ser maior que zero"; break;
            case 53141: $err = "Este remetente está bloqueado"; break;
            case 53142: $err = "O cartão de crédito está com o token inválido"; break;
        }
        return $err;
      }

}
