<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Google\Client;
use Google\Service\PeopleService;

require 'vendor/autoload.php';

class GoogleContactsService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        //model
        $this->load->model('model/GooglePerson_model', 'GooglePerson_model');

        //repository
        $this->load->model('repository/GooglePersonRepository_model', 'GooglePersonRepository_model');
    }

    public function create_person($googleService, $googlePerson)
    {

        //$googlePerson = new GooglePerson_model();
        $peopleService = new PeopleService($googleService);

        $contact = new PeopleService\Person([
            'names' => [
                ['givenName' => $googlePerson->givenName, 'familyName' => $googlePerson->familyName]
            ],
            'emailAddresses' => [
                ['value' => $googlePerson->emailAddresses]
            ],
            'phoneNumbers' => [
                ['value' => $googlePerson->phoneNumbers]
            ],
        ]);

        try {
            $createdContact = $peopleService->people->createContact($contact);
            return $this->db->update('google_persons', array('imported' => 1, 'google_person_id' => $createdContact->getResourceName()), array('id' => $googlePerson->id));
        } catch (Exception $e) {
            return $this->db->update('google_persons', array('imported' => 0, 'error' => $e->getMessage()), array('id' => $googlePerson->id));
        }
    }

    public function getGoogleService()
    {
        $client_secret = 'assets/uploads/client_secret.json';

        $client = new Client();
        $client->setApplicationName('Google Contacts Integration');
        $client->setAuthConfig($client_secret); // Substitua pelo caminho do seu arquivo de credenciais JSON
        $client->setScopes([Google_Service_PeopleService::CONTACTS]);
        $client->setRedirectUri('https://sistema.sagtur.com.br/googlecontacts/serviceGoogleCreateToken');
        $client->setAccessType('offline'); // Solicita o Refresh Token
        $client->setPrompt('consent');    // Garante que o usuário verá a solicitação para conceder o token

        $tokenPath = 'assets/uploads/token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $client->setAccessToken($accessToken);

            // Renovar o token se ele estiver expirado
            if ($client->isAccessTokenExpired()) {
                if ($client->getRefreshToken()) {
                    $accessToken = $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
                    file_put_contents($tokenPath, json_encode($accessToken));
                } else {
                    if (!$client->getAccessToken() || $client->isAccessTokenExpired() && !$client->getRefreshToken()) {
                        echo "NÃO ENCONTROU O TOKEN PARA CRIAÇÃO DOS CONTATOS - ENTRE EM CONTATO COM A SASGTUR";
                        exit;
                    }
                    exit;
                }
            }
        } else {
            // Se não há token salvo, iniciar o fluxo de autenticação
            if (!isset($_GET['code'])) {
                echo "NÃO ENCONTROU O TOKEN PARA CRIAÇÃO DOS CONTATOS - ENTRE EM CONTATO COM A SASGTUR";
                exit;
            } else {
                // Trocar o código de autorização por um token
                $accessToken = $client->fetchAccessTokenWithAuthCode($_GET['code']);
                if (isset($accessToken['error'])) {
                    echo "Erro ao obter o token: " . $accessToken['error_description'];
                    exit;
                }
                file_put_contents($tokenPath, json_encode($accessToken));
                $client->setAccessToken($accessToken);
            }
        }

        return $client;
    }

    public function export_peoples()
    {
        try {
            $googleService = $this->getGoogleService();
            $peoples = $this->GooglePersonRepository_model->getAllPersonsNotImported();

            $count = 0;

            foreach ($peoples as $person) {
                $this->create_person($googleService, $person);
                $count++;

                // Verificar se o limite de 30 chamadas foi atingido e aguardar 60 segundos
                if ($count >= 30) {
                    sleep(60); // Aguardar 1 minuto
                    $count = 0; // Resetar o contador
                }
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

}