<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LocationService_model extends CI_Model
{
    const STATUS_CANCELADA = 'cancel';

    public function __construct()
    {
        parent::__construct();
    }

    public function getById($id)
    {

        $this->db->select('locacao_veiculo.*, veiculo.photo as image, veiculo.name as name_product, veiculo.km_atual, veiculo.tipo_combustivel');
        $this->db->join('veiculo', 'veiculo.id = locacao_veiculo.veiculo_id');
        $q = $this->db->get_where('locacao_veiculo', array('locacao_veiculo.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function cancelLocation($location_id) {
        try {
            $this->__iniciaTransacao();

            $location = $this->__getById('locacao_veiculo', $location_id);

            $status = $location->locacao_status;
            $statusPagamento = $location->payment_status;

            if ($this->isLeaseCanceled($status)) throw new Exception('sale_already_canceled');
            if ($this->locacalComRecebimento($statusPagamento)) throw new Exception(lang('sale_cannot_be_canceled_with_receipts'));

            $this->__update('locacao_veiculo', array('locacao_status'=> LocationService_model::STATUS_CANCELADA, 'payment_status' => 'cancel', 'status_disponibilidade_id' => $this->Settings->status_disponibilidade_cancelado_id), 'id', $location_id);

            $this->__confirmaTransacao();

            return true;
        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    private function locacalComRecebimento($statusPagamento) {
        return $statusPagamento == 'paid' || $statusPagamento == 'partial';
    }

    private function isLeaseCanceled($status) {
        return $status == LocationService_model::STATUS_CANCELADA;
    }

    public  function addLocationVehicle($data) {
        try {
            $this->__iniciaTransacao();

            if ($this->db->insert('locacao_veiculo', $data)) {
                $location_id = $this->db->insert_id();

                if ($this->site->getReference('lo') == $data['reference_no'])  $this->site->updateReference('lo');
            }

            $this->__confirmaTransacao();

            return $location_id;

        } catch (Exception $ex){
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function addPickupVehicle($data) {
        try {
            $this->__iniciaTransacao();

            if ($this->db->insert('retirada_veiculo', $data)) {
                $retirada_veiculo_id = $this->db->insert_id();
            }

            $this->__update('locacao_veiculo', array('status_disponibilidade_id' => $this->Settings->status_disponibilidade_locado_id), 'id', $data['locacao_veiculo_id']);

            $this->__confirmaTransacao();

            return $retirada_veiculo_id;

        } catch (Exception $ex){
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function addVehicleReturn($data) {
        try {
            $this->__iniciaTransacao();

            if ($this->db->insert('devolucao_veiculo', $data)) {
                $retirada_veiculo_id = $this->db->insert_id();
            }

            $this->__update('locacao_veiculo', array('status_disponibilidade_id' => $this->Settings->status_disponibilidade_devolvido_id), 'id', $data['locacao_veiculo_id']);

            $this->__confirmaTransacao();

            return $retirada_veiculo_id;

        } catch (Exception $ex){
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

}