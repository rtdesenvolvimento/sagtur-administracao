<?php defined('BASEPATH') OR exit('No direct script access allowed');

class NotafiscalService_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //service
        $this->load->model('service/AsaasService_model', 'AsaasService_model');

        //model
        $this->load->model('model/NFAgendamento', 'NFAgendamento');
        $this->load->model('settings_model');

        //dto
        $this->load->model('dto/RetornoCobrancaDTO_model','RetornoCobrancaDTO_model');
    }

    public function salvar($nfa) {

        //$nfa = new NFAgendamento();

        try {

            if (!$this->existeFaturaAgendamento($nfa->fatura_id)) {

                $this->__iniciaTransacao();

                $nfa_id = $this->__save('nf_agendamentos', $nfa->__toArray());

                $this->__confirmaTransacao();

                return $nfa_id;
            }

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }

        return false;
    }

    public function emitir_agendamento($nfa_id): bool
    {
        try {

            $nfa = $this->__toModel($this->getAgendamentoByID($nfa_id), new NFAgendamento());

            if ($nfa->status == NFAgendamento::EMITIDA) {
                throw new Exception('Nota fiscal já emitida');
            }

            if ($nfa->status == NFAgendamento::CANCELADA) {
                throw new Exception('Nota fiscal cancelada');
            }

            $this->AsaasService_model->configurarIntegracao();

            return $this->AsaasService_model->emitirNotaFiscalAgendamento($nfa);

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function cancelar_notafiscal($nfa_id)
    {
        try {

            $nfa = $this->__toModel($this->getAgendamentoByID($nfa_id), new NFAgendamento());

            if ($nfa->status == NFAgendamento::GERADA) {
                throw new Exception('Nota fiscal apenas gerada, não é possível cancelar');
            }

            if ($nfa->status == NFAgendamento::CANCELADA) {
                throw new Exception('Nota fiscal já cancelada');
            }

            $this->AsaasService_model->configurarIntegracao();

            return $this->AsaasService_model->cancelarNotaFiscal($nfa);

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }
    }

    public function excluir_agendamento($nfa_id)
    {

        $nfa = $this->__toModel($this->getAgendamentoByID($nfa_id), new NFAgendamento());

        if ($nfa->status == NFAgendamento::AGENDADA) {
            throw new Exception('Nota fiscal agendada não pode ser excluida');
        }

        if ($nfa->status == NFAgendamento::EMITIDA) {
            throw new Exception('Nota fiscal emitida não pode ser excluida');
        }

        if ($nfa->status == NFAgendamento::CANCELADA) {
            throw new Exception('Nota fiscal cancelada não pode ser excluida');
        }

        $this->db->delete('nf_agendamentos', array('id' => $nfa_id));
    }

    public function existeFaturaAgendamento($fatura_id): bool
    {
        $this->db->where('fatura_id', $fatura_id);
        $query = $this->db->get('nf_agendamentos');
        return $query->num_rows() > 0;
    }

    public function getAgendamentoByID($nfa_id)
    {
        $this->db->where('id', $nfa_id);
        $query = $this->db->get('nf_agendamentos');
        return $query->row();
    }

}