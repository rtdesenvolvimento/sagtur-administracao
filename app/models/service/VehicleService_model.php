<?php defined('BASEPATH') OR exit('No direct script access allowed');

class VehicleService_model extends CI_Model
{
    const STATUS_CANCELADA = 'cancel';

    public function __construct()
    {
        parent::__construct();

        //services
        $this->load->model('service/AvailabilityService_model', 'AvailabilityService_model');

        //repository
        $this->load->model('repository/VehicleRepository_model', 'VehicleRepository_model');

        //model
        $this->load->model('model/ProdutoModel_model', 'ProdutoModel_model');
    }

    public function add($data, $photos = []) {
        try {
            $this->__iniciaTransacao();

            if ($this->db->insert('veiculo', $data)) {
                $vehicle_id = $this->db->insert_id();

                if ($this->site->getReference('ve') == $data['reference_no'])  $this->site->updateReference('ve');

                if ($photos) {
                    foreach ($photos as $photo) {
                        $this->db->insert('veiculo_photos', array('veiculo_id' => $vehicle_id, 'photo' => $photo));
                    }
                }
            }

            $this->__confirmaTransacao();

            return $vehicle_id;

        } catch (Exception $ex){
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }

    public function edit($data, $photos = [], $vehicle_id) {
        try {
            $this->__iniciaTransacao();

            if ($this->db->update('veiculo', $data, array('id' => $vehicle_id))) {

                if ($photos) {
                    foreach ($photos as $photo) {
                        $this->db->insert('veiculo_photos', array('veiculo_id' => $vehicle_id, 'photo' => $photo));
                    }
                }

               // $this->editProducts($vehicle_id);

                //criar disponibilidade do veiculo
                $this->AvailabilityService_model->generate_availability();
            }

            $this->__confirmaTransacao();

            return $vehicle_id;

        } catch (Exception $ex){
            $this->__cancelaTransacao();
            throw new Exception($ex->getMessage());
        }
    }


    private function editProducts($vehicle_id) {

        $vehicle = $this->VehicleRepository_model->getByID($vehicle_id);

        $data = array(
            'name'  => $this->input->post('name'),
            'unit'  => '',
        );

        $produtoModel = new ProdutoModel_model();
        $produtoModel->data = $data;

        //$produtoModel->programacaoDatas = $this->adicionarProgramacaoData($id);

        $this->ProdutoService_model->editarProduto($produtoModel, $vehicle->produto);
    }
}