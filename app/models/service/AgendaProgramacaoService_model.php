<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AgendaProgramacaoService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        //repository
        $this->load->model('repository/AgendaProgramacaoRepository_model', 'AgendaProgramacaoRepository_model');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');

    }

    public function carregar_datas_programacao($disponibilidades, $produtoId) {

         foreach ($disponibilidades as $disponibilidade) {

             //$disponibilidade = new AgendaProgramacao_model();

             $start_date = $disponibilidade->getDataSaida();
             $end_date = $disponibilidade->getDataRetorno();
             $start_hour = $disponibilidade->getHoraSaida();
             $end_hour = $disponibilidade->getHoraRetorno();
             $tipo = $disponibilidade->getTipoDisponibilidade();
             $preco = $disponibilidade->getPreco();

             $dates = $this->generate_date_hora($start_date, $end_date, $start_hour, $end_hour, $tipo);

             $disponibilidade->produto = $produtoId;
             $disponibilidade = $this->setDiaSemana($disponibilidade, $tipo);

             $agenda_programacao_id = $this->__save('agenda_programacao', $disponibilidade->__toArray());

             foreach ($dates as $data_generate) {

                 $agendaDTO = new AgendaViagemDTO_model();

                 $vagas         = $disponibilidade->vagas;
                 $doDia         = $data_generate;
                 $aoDia         = $data_generate;
                 $horaSaida     = $disponibilidade->horaSaida;
                 $horaRetorno   = $disponibilidade->horaRetorno;
                 $active        = TRUE;


                 $encontrouProgramacaoDataHora = $this->AgendaViagemRespository_model->isExistProgramacaoByData($produtoId, $doDia, $horaSaida);

                 if ($encontrouProgramacaoDataHora) {

                     /*
                     if ($encontrouProgramacaoDataHora->vagas !== $vagas) {
                         $vagas = $encontrouProgramacaoDataHora->vagas;
                     }
                     */

                     $active = $encontrouProgramacaoDataHora->active;
                 }

                 $agendaDTO->vagas  = $vagas;
                 $agendaDTO->active = $active;

                 $agendaDTO->preco = $preco;
                 $agendaDTO->doDia = $doDia;
                 $agendaDTO->aoDia = $aoDia;
                 $agendaDTO->horaSaida = $horaSaida;
                 $agendaDTO->horaRetorno = $horaRetorno;
                 $agendaDTO->agenda_programacao_id = $agenda_programacao_id;

                 $datas[] = $agendaDTO;
            }
         }

        return $datas;
    }

    private function setDiaSemana($disponibilidade, $tipo) {

        if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_DOMINGO) {
            $disponibilidade->setDomingo(true);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SEGUNDA) {
            $disponibilidade->setSegunda(TRUE);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_TERCA) {
            $disponibilidade->setTerca(TRUE);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_QUARTA) {
            $disponibilidade->setQuarta(TRUE);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_QUINTA) {
            $disponibilidade->setQuinta(TRUE);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SEXTA) {
            $disponibilidade->setSexta(TRUE);
        } else if ($tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SABADO) {
            $disponibilidade->setSabado(TRUE);
        }

        return $disponibilidade;
    }

    function generate_date_hora($start_date, $end_date, $start_hour, $end_hour, $tipo) {
        $datetimes = array();
        $current_time = strtotime($start_date);
        $end_time = strtotime($end_date);
        $weekday = $this->get_weekday_number($tipo);
        $today = strtotime(date('Y-m-d'));

        while ($current_time <= $end_time) {

            if ($current_time >= $today) {
                if (date('w', $current_time) == $weekday) {
                    $datetimes[] = date('Y-m-d', $current_time);
                } else if ($this->lancar_data_por_periodo($tipo)) {
                    $datetimes[] = date('Y-m-d', $current_time);
                }
            }

            $current_time = strtotime('+1 day', $current_time);
        }

        return $datetimes;
    }

    private function lancar_data_por_periodo($tipo): bool
    {
        return $tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_DATAS_PONTUAIS || $tipo == AgendaProgramacao_model::TIPO_DISPONIBILIDADE_PERIODOS;
    }

    private function get_weekday_number($weekday_name) {
        $weekday_name = strtolower($weekday_name);
        $days_of_week = array(
            'domingo' => 0,
            'segunda' => 1,
            'terca' => 2,
            'quarta' => 3,
            'quinta' => 4,
            'sexta' => 5,
            'sabado' => 6
        );
        if (array_key_exists($weekday_name, $days_of_week)) {
            return $days_of_week[$weekday_name];
        } else {
            return false;
        }
    }

    public function transfer_all($programacaoOrigem)
    {
        $produtoID = $programacaoOrigem->produto;
        $dataSaida = $programacaoOrigem->dataSaida;
        $horaSaida = $programacaoOrigem->horaSaida;

        $programacaoDestino = $this->AgendaViagemRespository_model->getProgramacaoByData($produtoID, $dataSaida, $horaSaida, $programacaoOrigem->id);

        if (!empty($programacaoDestino)) {

            //programacaoId
            $this->db->update('sale_items', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));
            $this->db->update('conta_receber', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));
            $this->db->update('conta_pagar', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));
            $this->db->update('parcela', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));
            $this->db->update('fatura', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));
            $this->db->update('calendar', array('programacaoId' => $programacaoDestino->id), array('programacaoId' =>  $programacaoOrigem->id));

            //programacao_Id
            $this->db->update('analytical_access_record', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));
            $this->db->update('assento_bloqueio', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));
            $this->db->update('cupom_desconto_item', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));
            $this->db->update('folders', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));
            $this->db->update('menu', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));
            $this->db->update('room_list', array('programacao_Id' => $programacaoDestino->id), array('programacao_Id' =>  $programacaoOrigem->ID));

            return true;
        }

        return false;
    }
}