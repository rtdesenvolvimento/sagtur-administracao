<?php defined('BASEPATH') OR exit('No direct script access allowed');

class LinkpagamentoService_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('service/exeption/MercadoPagoException_model', 'MercadoPagoException_model');
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');

        $this->load->model('financeiro_model');
    }


    function consultaPagamento($integracao, $code, $faturaId, $cobrancaFaturaId) {

        $this->atualizarDadosCobrancaFatura($code, $faturaId, $cobrancaFaturaId);

        $this->FinanceiroService_model->baixarPagamentoIntegracao($integracao, $code);

        return $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($code);
    }

    function atualizarDadosCobrancaFatura($code, $faturaId, $cobrancaFaturaId) {
        $checkoutUrl = base_url().'cartaocredito/pagamento/'.$code.'?token='.$this->session->userdata('cnpjempresa');

        $this->__edit('fatura', array('numero_transacao'=> $code), 'id', $faturaId);
        $this->__edit('fatura_cobranca', array('code'=> $code,  'checkoutUrl' => $checkoutUrl, 'errorProcessamento' => ''), 'id', $cobrancaFaturaId);
    }

}