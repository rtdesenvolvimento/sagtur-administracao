<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ParseContract_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();

        //dto
        $this->load->model('dto/TAGContrato_model', 'TAGContrato_model');

        //repository
        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');

        //service
        $this->load->model('service/ParseContractArray_model', 'ParseContractArray_model');
        $this->load->model('service/ParseContractModel_model', 'ParseContractModel_model');
        $this->load->model('service/ParseContractObject_model', 'ParseContractObject_model');

        $this->load->library('parser');
        $this->parser->set_delimiters('${', '}');
    }

    public function parse($config)
    {

        $contrato_tag = array();

        if ($config->type == ParseContractDTO_model::MODEL) {
            $contrato_tag = $this->ParseContractModel_model->parse($config->sale_id, $config->tipo);
        } elseif ($config->type == ParseContractDTO_model::ARRAY) {
            $contrato_tag = $this->ParseContractArray_model->parse($config->venda, $config->products, $config->tipo);
        } elseif ($config->type == ParseContractDTO_model::OBJECT) {
            $contrato_tag = $this->ParseContractObject_model->parse($config->appModel, $config->tipo);
        }

        if ($config->retorno == ParseContractDTO_model::TIPO_RETORNO_PARSE_STRING) {

            if ($config->usarContract) {
                return $this->parser->parse_string($this->ContractRepository_model->getById($config->contractID)->contract, $contrato_tag);
            } else {
                return $this->parser->parse_string($this->ContractRepository_model->getById($config->contractID)->clauses, $contrato_tag);
            }

         } elseif ($config->retorno == ParseContractDTO_model::TIPO_RETORNO_ARRAY) {
            return $contrato_tag;
        } else {
            return $contrato_tag;
        }
    }

}