<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Companies_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllBillerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'biller'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'customer'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSupplierCompanies()
    {
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyUsers($company_id)
    {
        $q = $this->db->get_where('users', array('company_id' => $company_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyByEmail($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCompanyByCpf($cpf)
    {
        $q = $this->db->get_where('companies', array('vat_no' => $cpf), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCompany($data = array())
    {
        if ($this->db->insert('companies', $data)) {
            $cid = $this->db->insert_id();
            return $cid;
        }
        return false;
    }

    public function getVerificaCustomeByCPF($cpf)
    {
        $this->db->where("vat_no",$cpf);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

    }

    public function addCompanyWith($data = array(), $photos = array())
    {
    	if ($this->db->insert('companies', $data)) {
    		$cid = $this->db->insert_id();
    		if ($photos) {
    			foreach ($photos as $photo) {
    				$this->db->insert('customer_photos', array('customer_id' => $cid, 'photo' => $photo));
    			}
    		}
    		
    		return $cid;
    	}
    	return false;
    }

    public function updateCompanyWith($id, $data = array(), $photos = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('companies', $data)) {
            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('customer_photos', array('customer_id' => $id, 'photo' => $photo));
                }
            }

            return true;
        }
        return false;
    }

    public function getCustomerPhotos($id)
    {
        $q = $this->db->get_where("customer_photos", array('customer_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function updateCompany($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('companies', $data)) {
            return true;
        }
        return false;
    }

    public function addCompanies($data = array())
    {
        if ($this->db->insert_batch('companies', $data)) {
            return true;
        }
        return false;
    }

    public function deleteCustomer($id)
    {
        if ($this->getCustomerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'customer')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSupplier($id)
    {
        if ($this->getSupplierPurchases($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'supplier')) && $this->db->delete('users', array('company_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function delete_event($id)
    {
        if ($this->db->delete('calendar', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteBiller($id)
    {
        if ($this->getBillerSales($id)) {
            return false;
        }
        if ($this->db->delete('companies', array('id' => $id, 'group_name' => 'biller'))) {
            return true;
        }
        return FALSE;
    }

    public function getBillerSuggestions($term, $limit = 10)
    {
        $this->db->select("id,  name as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'biller'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getAllCompaniesSuggestions($term, $limit = 10)
    {
        $this->db->select("id,  name text, tipoFaixaEtaria as tipo", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array(), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestions($term, $limit = 10)
    {
        $this->db->select("id,  concat(name, ' | ' , vat_no, ' | ', cf5) text, tipoFaixaEtaria as tipo, vat_no, name as customer, email, cf5 as phone ", FALSE);
        //$this->db->select("id, name text, tipoFaixaEtaria as tipo", FALSE);

        $this->db->where('(
            id LIKE "%'.$term.'%" OR 
            name LIKE "%'.$term.'%" OR 
            company LIKE "%'.$term.'%" OR 
            nome_responsavel LIKE "%'.$term.'%" OR 
            email LIKE "%'.$term.'%" OR 
            vat_no like "%'.$term.'%" OR
            cf5 like "%'.$term.'%" OR
            phone like "%'.$term.'%" OR
            cf1 like "%'.$term.'%" OR
            telefone_emergencia like "%'.$term.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(telefone_emergencia, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(cf1, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(cf5, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(phone, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term.'%" OR
            REPLACE(REPLACE(vat_no, ".", ""), "-", "") like "%'.$term.'%")');

        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestionsByID($term, $limit = 10)
    {
        $this->db->select("id, name text", FALSE);
        $this->db->where("id",$term);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getCustomerSuggestionsByIDS($term, $limit = 10,$idsNotIn,$ids)
    {
        $this->db->select("id, name text", FALSE);
        $this->db->where(" id in (".$ids.") ");
        $this->db->where(" (  name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestionsByID($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where("id",$term);
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestions($term, $limit = 10)
    {
        $this->db->select("id, 
            phone,
            address,
            city,
            state,
            postal_code,
            country,
         (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }


    public function getSupplierSuggestionsINIDS($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
        $this->db->where("id IN (".$term.")");
        $q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSupplierSuggestionsGroup($term, $limit = 10, $group = null)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(company, ' (', name, ')') END) as text", FALSE);
    	$this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%' OR company LIKE '%" . $term . "%' OR email LIKE '%" . $term . "%' OR phone LIKE '%" . $term . "%') ");
    	if ($group != null) {
    		$this->db->where("supplier_group_id = ".$group);
    	}
    	
    	$q = $this->db->get_where('companies', array('group_name' => 'supplier'), $limit);
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		
    		return $data;
    	}
    }

    public function getCustomerSales($id)
    {
        $this->db->where('customer_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getCustomerSuggestionsByCPFCNPJ($cpfCnpj, $limit = 1)
    {
        $this->db->where("vat_no",$cpfCnpj);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getCustomerSuggestionsByRG($rg, $limit = 1)
    {
        $this->db->where("cf1",$rg);
        $q = $this->db->get_where('companies', array('group_name' => 'customer'), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getBillerSales($id)
    {
        $this->db->where('biller_id', $id)->from('sales');
        return $this->db->count_all_results();
    }

    public function getSupplierPurchases($id)
    {
        $this->db->where('supplier_id', $id)->from('purchases');
        return $this->db->count_all_results();
    }

    public function addDeposit($data, $cdata)
    {
        if ($this->db->insert('deposits', $data) && 
            $this->db->update('companies', $cdata, array('id' => $data['company_id']))) {
            return true;
        }
        return false;
    }

    public function updateDeposit($id, $data, $cdata)
    {
        if ($this->db->update('deposits', $data, array('id' => $id)) && 
            $this->db->update('companies', $cdata, array('id' => $data['company_id']))) {
            return true;
        }
        return false;
    }

    public function getDepositByID($id)
    {
        $q = $this->db->get_where('deposits', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
	public function addEvent($data = array(), $retornarId=false) {
        if ($this->db->insert('calendar', $data)) {
            if ($retornarId) return $this->db->insert_id();
            return true;
        }
        return false;
    }
		
	public function updateEvent($customers, $data = array()) {
        if ($this->db->update('calendar', $data, array('customers' => $customers))) {
            return true;
        }
        return false;
    }
	
	public function getEventByCustomers($customers_id)
    {
        $q = $this->db->get_where('calendar', array('customers' => $customers_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
	
    public function deleteDeposit($id)
    {
        $deposit = $this->getDepositByID($id);
        $company = $this->getCompanyByID($deposit->company_id);
        $cdata = array(
                'deposit_amount' => ($company->deposit_amount-$deposit->amount)
            );
        if ($this->db->update('companies', $cdata, array('id' => $deposit->company_id)) &&
            $this->db->delete('deposits', array('id' => $id))) {
            return true;
        }
        return false;
    }


}
