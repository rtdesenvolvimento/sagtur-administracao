<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Db_model extends CI_Model
{

    public $mes_filter = 72;
    public $numero_limite_registros = 50;

    public function __construct()
    {
        parent::__construct();
    }

    public function getAgendamentos() {
        $this->db->select(
            "agendamento.id as id, 
                agendamento.nomeCategoria as categoria,
                agendamento.nomeProduto as servico, 
                CONCAT(".$this->db->dbprefix('agendamento').".nomeCompleto, '  ', ".$this->db->dbprefix('agendamento').".celular, ' ' , email ) as nomeCompleto, 
                agendamento.nomeTipoCobranca as cobranca, 
                agendamento.valor as valor,
                agendamento.data as data,
                agendamento.parcelas as parcelas,
                CONCAT(".$this->db->dbprefix('agendamento').".horaInicio, ' - ', ".$this->db->dbprefix('agendamento').".horaTermino) as hora,
                agendamento.status as status", FALSE);

        $this->db->order_by('id', 'desc');

        $q = $this->db->get("agendamento", $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUltimosAgendamentos() {
        $this->db->select(
            "agendamento.id as id, 
                agendamento.nomeCategoria as categoria,
                agendamento.nomeProduto as servico, 
                CONCAT(".$this->db->dbprefix('agendamento').".nomeCompleto, '  ', ".$this->db->dbprefix('agendamento').".celular, ' ' , email ) as nomeCompleto, 
                agendamento.nomeTipoCobranca as cobranca, 
                agendamento.valor as valor,
                agendamento.data as data,
                agendamento.parcelas as parcelas,
                CONCAT(".$this->db->dbprefix('agendamento').".horaInicio, ' - ', ".$this->db->dbprefix('agendamento').".horaTermino) as hora,
                agendamento.status as status", FALSE);

        $this->db->order_by('id', 'desc');

        $q = $this->db->get("agendamento", 100);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestSales($status=null)
    {

        $this->db->select('
        sales.id, 
        sales.pos,
        sales.date,
        sales.reference_no,
        companies.name as customer, 
        sale_items.product_name, 
        sales.sale_status, 
        sale_items.subtotal as grand_total, 
        sales.payment_status, 
        sales.paid');

        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) $this->db->where('sales.created_by', $this->session->userdata('user_id'));
        if ($status)  $this->db->where('sale_status', $status);

        $this->db->order_by('sales.id', 'desc');

        $this->db->join('sales', 'sales.id = sale_items.sale_id');
        $this->db->join('companies', 'companies.id = sale_items.customerClient', 'left');

        $q = $this->db->get("sale_items", $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUserById($id)
    {
        $q = $this->db->get_where('users', array('id' => $id), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    
    public function getLatestSalesProducts($id)
    {
    	if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('sales.created_by', $this->session->userdata('user_id'));
    	}
    	
    	$this->db
    	->select("date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('sale_items') . ".product_name, '__', " . $this->db->dbprefix('sale_items') . ".quantity) SEPARATOR '___') as iname, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.id as id", FALSE)
    	->from('sales')
    	->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
    	->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
    	->where('sale_items.product_id', $id)
    	->group_by('sales.id');
    	
     	$q = $this->db->get();
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    }

    public function getLatestSalesProductsAll()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('sales.created_by', $this->session->userdata('user_id'));
        }

        $this->db
            ->select("date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('sale_items') . ".product_name, '__', " . $this->db->dbprefix('sale_items') . ".quantity) SEPARATOR '___') as iname, grand_total, paid, (grand_total-paid) as balance, payment_status, {$this->db->dbprefix('sales')}.id as id", FALSE)
            ->from('sales')
            ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
            ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
            ->group_by('sales.id');

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLastestQuotes()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        $this->db->order_by('id', 'desc');
        $q = $this->db->get("quotes", $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestPurchases()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        $this->db->order_by('id', 'desc');
        $q = $this->db->get("purchases", $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestPurchasesProducts($id)
    {
    	if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
    		$this->db->where('created_by', $this->session->userdata('user_id'));
    	}
    	
    	$this->db
    	->select($this->db->dbprefix('purchases') . ".date, reference_no, " . $this->db->dbprefix('warehouses') . ".name as wname, supplier, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name, '__', " . $this->db->dbprefix('purchase_items') . ".quantity) SEPARATOR '___') as iname, grand_total, paid, (grand_total-paid) as balance, " . $this->db->dbprefix('purchases') . ".status, " .$this->db->dbprefix('purchase_items').  ".status payment_status, {$this->db->dbprefix('purchases')}.id as id", FALSE)
    	->from('purchases')
    	->join('purchase_items', 'purchase_items.purchase_id=purchases.id', 'left')
    	->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
    	->where('purchase_items.product_id', $id)
    	->group_by('purchases.id');
    	
    	$q = $this->db->get();

    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    }

    public function getLatestPurchasesProductsAll()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $this->db
            ->select($this->db->dbprefix('purchases') . ".date, reference_no, " . $this->db->dbprefix('warehouses') . ".name as wname, supplier, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name, '__', " . $this->db->dbprefix('purchase_items') . ".quantity) SEPARATOR '___') as iname, grand_total, paid, (grand_total-paid) as balance, " . $this->db->dbprefix('purchases') . ".status, " .$this->db->dbprefix('purchase_items').  ".status payment_status, {$this->db->dbprefix('purchases')}.id as id", FALSE)
            ->from('purchases')
            ->join('purchase_items', 'purchase_items.purchase_id=purchases.id', 'left')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
            ->group_by('purchases.id');

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    
    public function getExpensesReportProducts($id)
    {
    	if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
    		$this->db->where('created_by', $this->session->userdata('user_id'));
    	}
    	
    	$this->db
    	->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
    	->from('expenses')
    	->join('users', 'users.id=expenses.created_by', 'left')
    	->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
    	->where('product', $id)
    	->group_by('expenses.id');
    	
    	$q = $this->db->get();
    	
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    }
    
    
    public function getExpensesReportProductsNotSinal($id)
    {
    	if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
    		$this->db->where('created_by', $this->session->userdata('user_id'));
    	}
    	
    	$this->db
    	->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
    	->from('expenses')
    	->join('users', 'users.id=expenses.created_by', 'left')
    	->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
    	->where('product', $id)
    	->where('usar_como_sinal', 2)
    	->group_by('expenses.id');
    	
    	$q = $this->db->get();
    	
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    }

    public function getExpensesReportProductsNotSinalAll()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $this->db
            ->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->where('usar_como_sinal', 2)
            ->group_by('expenses.id');

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getExpensesReportProductsComSinal($id)
    {
    	if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
    		$this->db->where('created_by', $this->session->userdata('user_id'));
    	}
    	
    	$this->db
    	->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
    	->from('expenses')
    	->join('users', 'users.id=expenses.created_by', 'left')
    	->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
    	->where('product', $id)
    	->where('usar_como_sinal', 1)
    	->group_by('expenses.id');
    	
    	$q = $this->db->get();
    	
    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    }

    public function getExpensesReportProductsComSinalAll()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $this->db
            ->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->where('usar_como_sinal', 1)
            ->group_by('expenses.id');

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestTransfers()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }
        $this->db->order_by('id', 'desc');
        $q = $this->db->get("transfers", $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestCustomers()
    {
        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $this->db->order_by('id', 'desc');
        $q = $this->db->get_where("companies", array('group_name' => 'customer'), $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getLatestSuppliers()
    {
        $this->db->order_by('id', 'desc');
        $q = $this->db->get_where("companies", array('group_name' => 'supplier'), $this->numero_limite_registros);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPagarHoje() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento', date('Y-m-d'));

        $this->db->where('fatura.tipooperacao', 'DEBITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getPagarMes() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $this->db->where("date_format(dtvencimento, '%Y-%m') = '".date('Y-m')."' ");

        $this->db->where('fatura.tipooperacao', 'DEBITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getContaPagarAno() {

        $this->db->select('SUM(valorpagar) as valor, dtvencimento');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento >= date_sub( now( ) , INTERVAL '. $this->mes_filter .' MONTH )');
        $this->db->where('fatura.tipooperacao', 'DEBITO');
        $this->db->group_by('dtvencimento');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    public function getContaReceberAno() {

        $this->db->select('SUM(valorpagar) as valor, dtvencimento');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento >= date_sub( now( ) , INTERVAL '. $this->mes_filter .' MONTH )');
        $this->db->where('fatura.tipooperacao', 'CREDITO');
        $this->db->group_by('dtvencimento');

        $q = $this->db->get('fatura');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }

    public function getReceberHoje() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento', date('Y-m-d'));

        $this->db->where('fatura.tipooperacao', 'CREDITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getReceberMes() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $this->db->where("date_format(dtvencimento, '%Y-%m') = '".date('Y-m')."' ");

        $this->db->where('fatura.tipooperacao', 'CREDITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getReceberEmAtraso() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento < "'.date('Y-m-d').'"');

        $this->db->where('fatura.tipooperacao', 'CREDITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getPagamentoEmAtraso() {

        $this->db->select('SUM(valorpagar) as valor');

        $this->db->where('(fatura.status = "ABERTA" OR fatura.status = "PARCIAL" OR  fatura.status = "VENCIDA")');

        $this->db->where('fatura.dtvencimento < "'.date('Y-m-d').'"');

        $this->db->where('fatura.tipooperacao', 'DEBITO');
        $this->db->limit(1);

        return $this->db->get('fatura')->row();
    }

    public function getChartDataFluxoCaixaMes()
    {
        $myQuery = "SELECT FC.month,
        COALESCE(FC.CR, 0) as sales,
        COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  date_format(dtvencimento, '%Y-%m') Month,
                SUM(valorfatura) CR
                FROM " . $this->db->dbprefix('fatura') . "
                WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter. " MONTH ) AND 
                ".$this->db->dbprefix('fatura').".tipooperacao = 'CREDITO' AND
                (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                 ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                 ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                 ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FC
        LEFT JOIN ( SELECT  date_format(dtvencimento, '%Y-%m') Month,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter . " MONTH ) AND  
                        ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                         ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                        GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FD
        
        ON FC.Month = FD.Month
        GROUP BY FC.Month
        ORDER BY FC.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getChartDataFluxoCaixaMesQuitadoCredito()
    {
        $myQuery = "SELECT FC.month,
        COALESCE(FC.CR, 0) as sales,
        COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  date_format(dtvencimento, '%Y-%m') Month,
                SUM(valorfatura) CR
                FROM " . $this->db->dbprefix('fatura') . "
                WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter . " MONTH ) AND 
                ".$this->db->dbprefix('fatura').".tipooperacao = 'CREDITO' AND
                (".$this->db->dbprefix('fatura').".status = 'QUITADA')  
                  
                GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FC
        LEFT JOIN ( SELECT  date_format(dtvencimento, '%Y-%m') Month,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter . " MONTH ) AND  
                        ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'ABERTA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'PARCIAL' OR 
                         ".$this->db->dbprefix('fatura').".status = 'QUITADA' OR  
                         ".$this->db->dbprefix('fatura').".status = 'VENCIDA' ) 
                        GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FD
        
        ON FC.Month = FD.Month
        GROUP BY FC.Month
        ORDER BY FC.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getChartDataFluxoCaixaMesQuitadoDebito()
    {
        $myQuery = "SELECT FC.month,
        COALESCE(FC.CR, 0) as sales,
        COALESCE(FD.CP, 0 ) as purchases 
        FROM (  SELECT  date_format(dtvencimento, '%Y-%m') Month,
                SUM(valorfatura) CR
                FROM " . $this->db->dbprefix('fatura') . "
                WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter . " MONTH ) AND 
                ".$this->db->dbprefix('fatura').".tipooperacao = 'CREDITO' AND
                (".$this->db->dbprefix('fatura').".status = 'QUITADA')  
                  
                GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FC
        LEFT JOIN ( SELECT  date_format(dtvencimento, '%Y-%m') Month,
                        SUM(valorfatura) CP
                        FROM " . $this->db->dbprefix('fatura') . "
                        WHERE dtvencimento >= date_sub( now( ) , INTERVAL " . $this->mes_filter . " MONTH ) AND  
                        ".$this->db->dbprefix('fatura').".tipooperacao = 'DEBITO' AND
                        (".$this->db->dbprefix('fatura').".status = 'QUITADA' ) 
                        GROUP BY date_format(dtvencimento, '%Y-%m')
        ) FD
        
        ON FC.Month = FD.Month
        GROUP BY FC.Month
        ORDER BY FC.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getChartData2()
    {
        $myQuery = "SELECT S.month,
        COALESCE(S.sales, 0) as sales,
        COALESCE(P.purchases, 0 ) as purchases,
        COALESCE(S.tax1, 0) as tax1,
        COALESCE(S.tax2, 0) as tax2,
        COALESCE( P.ptax, 0 ) as ptax
        FROM (  SELECT  date_format(date, '%Y-%m') Month,
                SUM(total) Sales,
                SUM(product_tax) tax1,
                SUM(order_tax) tax2
                FROM " . $this->db->dbprefix('sales') . "
                WHERE date >= date_sub( now( ) , INTERVAL " . $this->mes_filter. " MONTH )
                GROUP BY date_format(date, '%Y-%m')) S
            LEFT JOIN ( SELECT  date_format(date, '%Y-%m') Month,
                        SUM(product_tax) ptax,
                        SUM(order_tax) otax,
                        SUM(total) purchases
                        FROM " . $this->db->dbprefix('purchases') . "
                        GROUP BY date_format(date, '%Y-%m')) P
            ON S.Month = P.Month
            GROUP BY S.Month
            ORDER BY S.Month";

        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStockValue()
    {
        $q = $this->db->query("SELECT SUM(qty*price) as stock_by_price, SUM(qty*cost) as stock_by_cost
        FROM (
            Select sum(COALESCE(" . $this->db->dbprefix('warehouses_products') . ".quantity, 0)) as qty, price, cost
            FROM " . $this->db->dbprefix('products') . "
            JOIN " . $this->db->dbprefix('warehouses_products') . " ON " . $this->db->dbprefix('warehouses_products') . ".product_id=" . $this->db->dbprefix('products') . ".id
            GROUP BY " . $this->db->dbprefix('warehouses_products') . ".id ) a");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getVendasPorVendedorAgrupadoCategoria($start_date = NULL, $end_date = NULL)
    {
        if (!$start_date) $start_date = date('Y-m-d', strtotime('first day of this month')) . ' 00:00:00';
        if (!$end_date) $end_date = date('Y-m-d', strtotime('last day of this month')) . ' 23:59:59';

        $this->db
            ->select("companies.name, SUM(subtotal) as subtotal, nmCategoria as categoria", FALSE)
            ->from('sale_items', FALSE)
            ->join('sales', 'sales.id = sale_items.sale_id', 'left')
            ->join('companies', 'companies.id = sales.biller_id', 'left')
            ->where('sales.date >=', $start_date)
            ->where('sales.date <', $end_date)
            ->group_by('categoria')
            ->limit(10000);

        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProgramacoes($term, $limit = 200, $productId = null, $group_by = NULL) {
        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            //->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.enviar_site', '1');//APENAS DISPONIVEL ONLINE
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
            //$this->db->where("(products.track_quantity = 0 OR warehouses_products.quantity > 0) AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND "
            //. "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }

        if ($productId != null && $productId != 'all') $this->db->where("products.id", $productId);
        if ($group_by != null) $this->db->group_by($group_by);

        $this->db->limit($limit);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function count_unread_captacoes() {
        $this->db->where('read', 0);
        $this->db->from('captacao');
        return $this->db->count_all_results();
    }
}
