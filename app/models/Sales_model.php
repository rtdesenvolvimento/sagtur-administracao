<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model
{

    const TIPO_DE_PAGAMENTO_RECEBIMENTO = 'Recebimento';
    const TIPO_DE_PAGAMENTO_PAGAMENTO = 'Pagamento';

    const TIPO_DE_PAGAMENTO_SAQUE = 'Saque';
    const TIPO_DE_PAGAMENTO_DEPOSITO = 'Deposito';

    const TIPO_DE_PAGAMENTO_TRANSFERENCIA = 'Transferencia';

    const TIPO_DE_PAGAMENTO_TAXAS = "Taxas";

    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/CommissionsRepository_model', 'CommissionsRepository_model');
        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/VendaService_model', 'VendaService_model');
        $this->load->model('service/CommissionEventService_model', 'CommissionEventService_model');

        $this->load->model('model/ContaPagar_model', 'ContaPagar_model');
        $this->load->model('model/MotivoCancelamentoVenda_model', 'MotivoCancelamentoVenda_model');

        //service
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');
        $this->load->model('service/BusService_model', 'BusService_model');

        $this->load->model('Roomlisthospede_model');
        $this->load->model('financeiro_model');

    }

    public function getProgramacaoAll($status=NULL, $ano=NULL, $mes=NULL)
    {

        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        if ($status) $this->db->where('products.unit', $status);
        if ($mes) $this->db->where('Month(sma_agenda_viagem.dataSaida)', $mes);
        if ($ano) $this->db->where('Year(sma_agenda_viagem.dataSaida)', $ano);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }


    public function getProgramacao($term, $warehouse_id, $limit = 200)
    {
        return $this->getProgramacoes($term, $warehouse_id, $limit);
    }

    public function getProgramacoes($term, $warehouse_id, $limit = 200, $productId = null, $group_by = NULL) {
        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            //->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.enviar_site', '1');//APENAS DISPONIVEL ONLINE
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
            //$this->db->where("(products.track_quantity = 0 OR warehouses_products.quantity > 0) AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND "
            //. "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }

        if ($productId != null && $productId != 'all') $this->db->where("products.id", $productId);
        if ($group_by != null) $this->db->group_by($group_by);

        $this->db->limit($limit);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProgramacoesVenda($term, $warehouse_id, $limit = 200, $productId = null, $group_by = NULL, $ano = NULL, $mes = NULL, $data_passeio = NULL) {
        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida')
            ->order_by('agenda_viagem.dataRetorno')->order_by('agenda_viagem.horaRetorno');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }

        if ($productId != null && $productId != 'all') {
            $this->db->where("products.id", $productId);
        }

        if ($group_by != null) {
            $this->db->group_by($group_by);
        }

        if ($ano) {
            $this->db->where("YEAR(sma_agenda_viagem.dataSaida)", $ano);
        }

        if ($mes) {
            $this->db->where("MONTH(sma_agenda_viagem.dataSaida)", $mes);
        }

        if ($data_passeio) {
            $this->db->where("sma_agenda_viagem.dataSaida", $data_passeio);
        }

        $this->db->limit($limit);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProgramacoesOrderByNameProducts($term, $warehouse_id, $limit = 200, $productId = null, $group_by = NULL) {
        $this->db->select('products.*,
            products.id,
            agenda_viagem.id programacaoId, 
            agenda_viagem.dataSaida, 
            agenda_viagem.horaSaida, 
            agenda_viagem.dataRetorno, 
            agenda_viagem.horaRetorno,
            agenda_viagem.vagas as quantity,
            categories.id as categoriaId, 
            categories.name as category_name')

            ->join('products', 'products.id=agenda_viagem.produto')
            //->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->order_by('products.name')
            ->order_by('agenda_viagem.dataSaida')->order_by('agenda_viagem.horaSaida');

        $this->db->where('agenda_viagem.dataSaida >= "'.date('Y-m-d').'" ');//SO VIAGEM QUE NAO PASSARAM
        $this->db->where('products.unit', 'Confirmado');//APENAS VIAGENS CONFIRMADAS
        $this->db->where('products.enviar_site', '1');//APENAS DISPONIVEL ONLINE
        $this->db->where('products.active', 1);//APRENTAS PRODUTO ATIVO

        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
            //$this->db->where("(products.track_quantity = 0 OR warehouses_products.quantity > 0) AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND "
            //. "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }

        if ($productId != null && $productId != 'all') $this->db->where("products.id", $productId);
        if ($group_by != null) $this->db->group_by($group_by);

        $this->db->limit($limit);

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductNames($term, $warehouse_id, $limit = 5)
    {
        $this->db->select('products.*, warehouses_products.quantity, categories.id as category_id, categories.name as category_name')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        } else {
            $this->db->where("({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");

//            $this->db->where("(products.track_quantity = 0 OR warehouses_products.quantity > 0) AND warehouses_products.warehouse_id = '" . $warehouse_id . "' AND "
                //. "({$this->db->dbprefix('products')}.name LIKE '%" . $term . "%' OR {$this->db->dbprefix('products')}.code LIKE '%" . $term . "%' OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') LIKE '%" . $term . "%')");
        }
        $this->db->limit($limit);
        $q = $this->db->get('products');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.quantity as qty, products.name as name,products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllItinerarioByItem($saleItemId)
    {
        $q = $this->db->get_where('aereo_itinerario', array('sale_item' => $saleItemId));
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getAllServicosAdicionaisByItem($saleItemId)
    {
        $q = $this->db->get_where('sale_item_adicional', array('sale_item' => $saleItemId));
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getTransferBySaleItem($saleItemId)
    {
        $q = $this->db->get_where('transfer_itinerario', array('sale_item' => $saleItemId));
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function syncQuantity($sale_id)
    {
        if ($sale_items = $this->getAllInvoiceItems($sale_id)) {
            foreach ($sale_items as $item) {
                $this->site->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->site->syncVariantQty($item->option_id, $item->warehouse_id);
                }
            }
        }
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $all = NULL)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity, product_variants.fornecedor, product_variants.totalPreco , product_variants.localizacao ')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->group_by('product_variants.id');

        if( ! $this->Settings->overselling && ! $all)  $this->db->where('warehouses_products_variants.quantity >', 0);

        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return FALSE;
    }

    public function getProductOptionsComissao($product_id, $warehouse_id, $all = NULL)
    {
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, warehouses_products_variants.quantity as quantity, product_variants.fornecedor, product_variants.totalPreco , product_variants.localizacao ')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->where('product_variants.name', 'Comissão')
            ->group_by('product_variants.id');

        if( ! $this->Settings->overselling && ! $all)  $this->db->where('warehouses_products_variants.quantity >', 0);

        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0)  return $q->row();
        return FALSE;
    }

    public function getVariantByName($name)
    {
    	$q = $this->db->get_where('variants', array('name' => $name), 1);
    	if ($q->num_rows() > 0) {
    		return $q->result();
    	}
    	return FALSE;
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {

        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }


    public function getItemByServicoAdicional($sale_id, $servicoId, $customerId) {

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id, 'product_id' => $servicoId, 'customerClient' => $customerId));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function getAllInvoiceItems($sale_id, $exibirApenasServicos=false)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            tipo_trajeto_veiculo.name as tipo_trajeto,
            products.code,
            products.isTaxasComissao,  
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tipo_trajeto_veiculo', 'products.tipo_trajeto_id=tipo_trajeto_veiculo.id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')

            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')

            //->group_by('sale_items.id')
            ->order_by('sale_items.customerClient, sale_items.adicional, sale_items.id, sale_items.tipoDestino');
        //->order_by('sale_items.product_category desc, sale_items.dateCheckin, sale_items.horaCheckin, sale_items.dateCheckOut,sale_items.horaCheckOut');

        if ($exibirApenasServicos) {
            $this->db->where('adicional', false);
        }

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllInvoiceItemsByReport($sales_id)
    {
        $this->db->select('sale_items.*,
            sale_items.id as itemid,
            tipo_transporte_rodoviario.name as tipoTransporte,
            tipo_transporte_rodoviario.id as tipoTransporteId,

            tipo_hospedagem.name as tipoHospedagem,
            tipo_hospedagem.id as tipoHospedagemId,

            local_embarque.name as localEmbarque,
            local_embarque.id as localEmbarqueId,
            
            agenda_viagem.dataSaida as dtSaida,
            agenda_viagem.horaSaida as hrSaida,
            agenda_viagem.dataRetorno as dtRetorno,
            agenda_viagem.horaRetorno as hrRetorno,
            
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            
            tipo_trajeto_veiculo.name as tipo_trajeto,
            products.code,
            products.isTaxasComissao,  
            products.oqueInclui,
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details as details,
            product_variants.name as variant')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('tipo_trajeto_veiculo', 'products.tipo_trajeto_id=tipo_trajeto_veiculo.id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')

            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('local_embarque', 'local_embarque.id=sale_items.localEmbarque', 'left')
            ->join('agenda_viagem', 'agenda_viagem.id=sale_items.programacaoId', 'left')

            ->order_by('sale_items.customerClient, sale_items.adicional, sale_items.id, sale_items.tipoDestino');

        $this->db->where_in('sale_items.sale_id', $sales_id);


        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function getAllInvoiceItemsAdicionais($sale_id)
    {
        $this->db->select('
            products.unit, 
            products.name as produtc_name, 
            products.image, 
            products.details, 
            products.product_details as product_details')

            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->group_by('products.id');

        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id, 'adicional' => true));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItemsByCustomer($sale_id, $customerId)
    {
        $this->db->select('sale_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.image, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id, 'customerClient' => $customerId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllReturnItems($return_id)
    {
        $this->db->select('return_items.*, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=return_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_items.option_id', 'left')
            ->group_by('return_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('return_items', array('return_id' => $return_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllInvoiceItemsWithDetails($sale_id)
    {
        $this->db->select('sale_items.id, sale_items.product_name, sale_items.product_code, sale_items.quantity, sale_items.serial_no, sale_items.tax, sale_items.net_unit_price, sale_items.item_tax, sale_items.item_discount, sale_items.subtotal, products.details, products.unit, product_variants.name as variant');
        $this->db->join('products', 'products.id=sale_items.product_id', 'left')
        ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
        ->group_by('sale_items.id');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoiceByID($id)
    {
        $this->db->select('sales.*, meio_divulgacao.name as meio_divulgacao_name, condicao_pagamento.name as condicaoPagamento, tipo_cobranca.name as tipoCobranca, sma_companies.name as biller_name ')
            ->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left')
            ->join('meio_divulgacao', 'meio_divulgacao.id=sales.meioDivulgacao', 'left')
            ->join('sma_companies', 'sma_companies.id=sales.biller_id', 'left');

        $q = $this->db->get_where('sales', array('sales.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInvoiceByProductAndBiller($product_id, $biller_id)
    {
        $this->db->select('sales.*, sale_items.*, sale_items.id id_item, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id, 'biller_id' => $biller_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByProduct($product_id, $programacaoId = null)
    {
        $this->db->select('sales.*, sales.id as sale_id, sale_items.*, sale_items.id id_item, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');

        if ($programacaoId) {
            $this->db->where('sale_items.programacaoId',$programacaoId);
        }

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function getTotalVendasRealizadasParaAhViagem($product_id)
    {
          $this->db->select('sum(sma_sale_items.quantity) as quantity')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left')->group_by('sale_items.product_id');
            $q = $this->db->get_where('sale_items', array('product_id' => $product_id, 'colo' => 0));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByProductOrderByPoltrona($product_id, $programacaoId, $tipoTransporte= NULL, $localEmbarque=null)
    {
        $this->db->select('sales.*, sale_items.*, 
            sale_items.id id_item, products.* ,
            companies.*, tipo_transporte_rodoviario.name as tipoTransporteRodoviario ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left')
            ->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id=sale_items.tipoTransporte', 'left');
            $this->db->order_by('sale_items.tipoTransporte, sale_items.poltronaClient, sale_items.id desc');

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $this->db->where('sale_items.programacaoId',$programacaoId);

        if ($tipoTransporte) $this->db->where('sale_items.tipoTransporte',$tipoTransporte);
        if ($localEmbarque) $this->db->where('sale_items.localEmbarque',$localEmbarque);

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscarTotalDeItensDeUmaVendaPorVenda($vendaId)
    {
        $this->db->select('sum(sma_sale_items.quantity) as quantity');
        $q = $this->db->get_where('sale_items', array('sale_id' => $vendaId));

        if ($q->num_rows() > 0) {
            return $q->row()->quantity;
        }
        return 0;
    }
    public function getInvoiceByProductOrderByCliente($product_id)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $this->db->order_by('companies.name');

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasTodosPassageiros($produtoId, $programacaoId , $tipoTrasporteId = NULL, $order_by = NULL)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        if ($order_by) {
            $this->db->order_by($order_by);
        } else {
            $this->db->order_by('companies.name');
        }

        if ($tipoTrasporteId !== null) {
            $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        }

        $this->db->where('(sales.sale_status = "faturada" OR sales.sale_status = "orcamento")');

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasSemTipoHospedagem($produtoId, $programacaoId)
    {
        $this->db->select('sales.*, sale_items.*, sale_items.id as itemId, sale_items.faixaNome as faixaNome, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left');

        $this->db->order_by('sale_items.sale_id, companies.name');

        $this->db->where("(tipoHospedagem IS NULL OR tipoHospedagem = '' OR tipoHospedagem = 0)", NULL);

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensByFaixaValores($produtoId, $programacaoId, $faixaid, $status, $tipoHospedagem= NULL)
    {
        $this->db->select('COALESCE(count(sma_sale_items.id), 0) as qtd, valor_faixa.name as faixa, valor_faixa.descontarVaga')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaId', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where('sale_items.faixaId', $faixaid);
        $this->db->where('sales.sale_status', $status);

        if ($tipoHospedagem) {
            $this->db->where('sale_items.tipoHospedagem', $tipoHospedagem);
        }

        $this->db->group_by('sale_items.faixaId');
        $this->db->order_by('valor_faixa.name');

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getItensVendasByTipoHospedagem($produtoId, $programacaoId , $tipoHospedagemId)
    {
        $this->db->select('sales.*, sale_items.*, sale_items.id as itemId, products.*, companies.*')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left');

        $this->db->order_by('sale_items.sale_id, companies.name');

        $this->db->where('sale_items.tipoHospedagem', $tipoHospedagemId);

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUltimoItemCompraCliente($clienteId) {
        $this->db->select('max(sma_sale_items.product_name) as name');
        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where("sales.payment_status in ('due','partial','paid') ");
        $this->db->where('sale_items.adicional', false);

        $q = $this->db->get_where('sale_items', array('customerClient' => $clienteId), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getNumeroDeComprasCliente($clienteId) {
        $this->db->select('COUNT(*) as numero_compras');
        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->where("sales.payment_status in ('due','partial','paid') ");
        $this->db->where('sale_items.adicional', false);

        $q = $this->db->get_where('sale_items', array('customerClient' => $clienteId), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getItensVendasFaturadas($produtoId, $programacaoId , $tipoTrasporteId=null)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        $this->db->order_by('companies.name');

        if ($tipoTrasporteId !== null)  {
            $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        }

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasFaturadasOrderByTipoTransporte($produtoId, $programacaoId , $tipoTrasporteId=null)
    {
        $this->db->select('sales.*, sale_items.*, products.*, companies.*, tipo_hospedagem.name as tipoHospedagem')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('tipo_hospedagem', 'tipo_hospedagem.id=sale_items.tipoHospedagem', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $this->db->order_by('tipo_hospedagem.id');

        if ($tipoTrasporteId !== null)  {
            $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        }

        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        $q = $this->db->get_where('sale_items', array('product_id' => $produtoId, 'programacaoId' => $programacaoId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($productId, $tipoTrasporteId, $programacaoId,  $localEmbarqueId=null)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left');

        if ($localEmbarqueId)  $this->db->where('sale_items.localEmbarque', $localEmbarqueId);

        $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.
        $this->db->where('sale_items.descontarVaga', 1);

        $this->db->order_by('companies.name');

        $q = $this->db->get_where('sale_items', array('product_id' => $productId));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItensVendasPorLocalEmbarque($productId, $tipoTrasporteId, $programacaoId,  $localEmbarqueId=null, $order_by = FALSE)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.*, valor_faixa.ocultar_faixa_relatorio')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sale_items.customerClient=companies.id', 'left')
            ->join('valor_faixa', 'valor_faixa.id=sale_items.faixaid', 'left');

        if ($localEmbarqueId) {
            $this->db->where('sale_items.localEmbarque', $localEmbarqueId);
        }

        $this->db->where('sale_items.tipoTransporte', $tipoTrasporteId);
        $this->db->where('sale_items.programacaoId', $programacaoId);
        $this->db->where('sales.sale_status','faturada');//TODO MOSTRA APENAS VENDAS FATURADAS**QUE VAI SER RECEBIDOS VALORES.

        if ($order_by) {
            $this->db->order_by($order_by);
        } else {
            $this->db->order_by('companies.name');
        }

        $q = $this->db->get_where('sale_items', array('product_id' => $productId));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByProductGroupByLocalEmbarque($product_id)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $this->db->group_by('sales.local_saida');

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscarLocaisDeEmbarquePorViagem($product_id)
    {
        $this->db->select('sales.local_saida, sale_items.product_id')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left');

        $this->db->order_by('sales.local_saida');
        $this->db->group_by('sales.local_saida');

        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByProductGroupByCliente($product_id)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $this->db->order_by('sales.local_saida');
        $this->db->group_by('sale_items.customerClient, sales.customer_id');
        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getInvoiceByProductGroupBySale($product_id)
    {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $this->db->group_by('sale_items.sale_id');
        $q = $this->db->get_where('sale_items', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function getAllSalesByCustomer($customer_id)
    {
		$q = $this->db->get_where('sales', array('customer_id' => $customer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('return_sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnBySID($sale_id)
    {
        $q = $this->db->get_where('return_sales', array('sale_id' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL)
    {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0);
        if ($option_id) {
            $this->db->where('option_id', $option_id);
        }        $this->db->group_by('id');

        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addSale($data = array(),
                            $items = array(),
                            $payment = array(),
                            $si_return = array(),
                            $conta_receber = array(),
                            $valor_vencimento_parcelas = array(),
                            $data_vencimento_parcelas = array() ,
                            $tipo_cobranca_parcelas = array() ,
                            $movimentadores = array(),
                            $desconto_parcelas = array()) {

        $status = $data['sale_status'];
        $sale_id = FALSE;

        try {

            $this->__iniciaTransacao();

            $cost = $this->site->costing($items);
            // $this->sma->print_arrays($cost);

            if ($this->db->insert('sales', $data)) {

                $sale_id = $this->db->insert_id();

                $user = $this->site->getUser($data['created_by']);
                $conta_receber['sale'] = $sale_id;

                if ($this->site->getReference('so') == $data['reference_no']) {
                    $this->site->updateReference('so');
                }

                $programacaoId = null;
                $totalItensVendaArray            = [];
                $totalItensTiposHospedagemArray  = [];

                foreach ($items as $item) {
                    if (!$item['adicional'] && $item['descontarVaga'] == '1') {
                        $programacaoId  = $item['programacaoId'];
                        $hospedagem     = $item['tipoHospedagem'];
                        $quantity       = $item['quantity'];

                        $totalItensVendaArray[$programacaoId]        = $totalItensVendaArray[$programacaoId] + $quantity;
                        $totalItensTiposHospedagemArray[$hospedagem] = $totalItensTiposHospedagemArray[$hospedagem] + $quantity;
                    }
                }

                foreach ($items as $item) {

                    $product            = $this->site->getProductByID($item['product_id']);

                    $programacaoId      = $item['programacaoId'];
                    $tipo_hospedagem    = $item['tipoHospedagem'];
                    $faixaId            = $item['faixaId'];
                    $customerClientName = $item['customerClientName'];
                    $customerClientID   = $item['customerClient'];
                    $tipoTransporteID   = $item['tipoTransporte'];
                    $poltronaClient     = $item['poltronaClient'];

                    $totalItensVenda                 = $totalItensVendaArray[$programacaoId];
                    $totalItensVendaTipoHospedagem   = $totalItensTiposHospedagemArray[$tipo_hospedagem];

                    if ($this->naoPossuiVagasSuficienteDisponivel($programacaoId, $totalItensVenda) && $status == 'faturada') {
                        throw new Exception(lang('produto_com_estoque_negativo'));
                    }

                    if ($product->controle_estoque_hospedagem) {
                        if ($this->naoPoussuiVagasSuficienteDisponivelHospedagem($programacaoId, $tipo_hospedagem, $totalItensVendaTipoHospedagem)){
                            throw new Exception(lang('produto_com_estoque_tipo_hospedagem_negativo'));
                        }
                    }

                    if ($faixaId == null) {
                        throw new Exception(lang('faixa_nao_cadastrada_na_venda').$customerClientName);
                    }

                    if ($poltronaClient) {
                        $isAssentoBloqueadoOcupado = $this->BusService_model->isAssentoBloqueadoOuMarcado($product->id, $programacaoId, $tipoTransporteID, $poltronaClient);

                        if ($isAssentoBloqueadoOcupado) {
                            throw new Exception(lang('assento_ja_bloqueado_ocultado').' Assento duplicado '.$poltronaClient);
                        }
                    }

                    if ($product->permiteVendaClienteDuplicidade == '0' && $product->viagem == 1 && !$product->isApenasColetarPagador == 1) {
                        $duplicidadeCliente = $this->verificaDuplicidadeClienteVenda($customerClientID, $programacaoId);

                        if ($duplicidadeCliente) {
                            throw new Exception(lang('cliente_duplicidade_venda').$customerClientName);
                        }
                    }
                }

                if ($data['venda_link'] ) {
                    $this->SaleEventService_model->addEvent($sale_id, 'Venda Adicionada Via Link de Reservas', $user);
                } else {
                    $this->SaleEventService_model->addEvent($sale_id, 'Venda Manual', $user);
                }

                $conta_receber['programacaoId'] = $programacaoId;

                //TODO AQUI E LANCADO O CONTA A RECEBER EM NOME DO CLIENTE
                $contaReceberId = $this->adicionarContaReceberCliente(
                    $data,
                    $status,
                    $conta_receber,
                    $valor_vencimento_parcelas,
                    $data_vencimento_parcelas,
                    $tipo_cobranca_parcelas,
                    $movimentadores,
                    $desconto_parcelas);

                $this->adicionarParcelaOrcamento(
                    $sale_id,
                    $status,
                    $data['condicaopagamentoId'],
                    $data_vencimento_parcelas,
                    $tipo_cobranca_parcelas,
                    $valor_vencimento_parcelas,
                    $movimentadores);

                foreach ($items as $item) {

                    $comissaoManual = $item['product_comissao'];//TODO comissao digitado pelo produto

                    $item['sale_id']                    = $sale_id;
                    $item['contaReceberId']             = $contaReceberId;

                    $itinerarios        = $item['itinerarios'];
                    $transfers          = $item['transfers'];
                    $servicosAdicionais = $item['servicosAdicionais'];

                    unset($item['itinerarios']);
                    unset($item['transfers']);
                    unset($item['servicosAdicionais']);

                    $this->db->insert('sale_items', $item);
                    $sale_item_id = $this->db->insert_id();

                    if ($status === 'faturada') {

                        $conta_receber['sale_item'] = $sale_item_id;

                        if ($comissaoManual > 0) {
                            //TODO ESTA DESABILITADO TEMPORARIAMENTE POS VAMOS RECRIAR O SISTEMA DE COMISSAO NO SISTEMA DO ZERO.
                            /*
                            $this->lancarComissaoDoVendedorComissaoManual(
                                $conta_receber,
                                $item['programacaoId'],
                                $data['biller_id'],
                                $comissaoManual);
                            */
                        } else {
                            /*
                            $this->lancarComissaoDoVendedorPorTipoDeProduto(
                                $conta_receber,
                                $item['programacaoId'],
                                $data['biller_id'],
                                $item['product_code'],
                                $item['subtotal']);
                            */
                        }

                        if ($item['fornecedorId'] != '') {

                            $valor = $item['valorRecebimentoValor'];

                            if ($valor > 0) {
                                $this->lancarContaPagarParaOhFornecedorProdutoManual($item,  $item['programacaoId'], $conta_receber);
                            } else if ($valor < 0) {
                                $this->lancarContaReceberParaOhFornecedorProdutoManual($item, $item['programacaoId'], $conta_receber);
                            }
                        }

                        //verificar se o produto e do tipo combo
                        if ($item['servicoPai'] != '') {

                            $comboItem = $this->products_model->getProductComboItemById($item['servicoPai'], $item['product_id']);

                            if ($comboItem->fornecedorId > 0 && $comboItem->comissao > 0) {

                                $comissaoDaAgencia = $comboItem->comissao;
                                $subTotal = $item['subtotal'];

                                $valorPagarAoFornecedorReceptivo = $subTotal - ($subTotal * ($comissaoDaAgencia / 100));

                                $this->lancarContaPagarFornecedorReceptivoDeUmServicoAdicional(
                                    $comboItem->fornecedorId ,
                                    $item['programacaoId'],
                                    $conta_receber,
                                    $valorPagarAoFornecedorReceptivo);
                            }
                        }
                    }

                    foreach ($servicosAdicionais as $servicosAdicional) {
                        $servicosAdicional['saleId'] = $sale_id;
                        $servicosAdicional['sale_item'] = $sale_item_id;

                        $this->db->insert('sale_item_adicional', $servicosAdicional);
                    }

                    foreach ($itinerarios as $itinerario) {
                        $itinerario['saleId'] = $sale_id;
                        $itinerario['sale_item'] = $sale_item_id;

                        $this->db->insert('aereo_itinerario', $itinerario);
                    }

                    foreach ($transfers as $transfer) {
                        $transfer['saleId'] = $sale_id;
                        $transfer['sale_item'] = $sale_item_id;

                        $this->db->insert('transfer_itinerario', $transfer);
                    }

                    if ($data['sale_status'] == 'completed') {
                        $item_costs = $this->site->item_costing($item);
                        foreach ($item_costs as $item_cost) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $sale_id;
                            if (!isset($item_cost['pi_overselling'])) {

                                $this->db->insert('costing', $item_cost);
                            }
                        }
                    }
                }

                if ($data['sale_status'] == 'completed') {
                    $this->site->syncPurchaseItems($cost);
                }

                if (!empty($si_return)) {
                    foreach ($si_return as $return_item) {
                        $product = $this->site->getProductByID($return_item['product_id']);
                        if ($product->type == 'combo') {
                            $combo_items = $this->site->getProductComboItems($return_item['product_id'], $return_item['warehouse_id']);
                            foreach ($combo_items as $combo_item) {
                                $this->updateCostingLine($return_item['id'], $combo_item->id, $return_item['quantity']);
                                $this->updatePurchaseItem(NULL, ($return_item['quantity'] * $combo_item->qty), NULL, $combo_item->id, $return_item['warehouse_id']);
                            }
                        } else {
                            $this->updateCostingLine($return_item['id'], $return_item['product_id'], $return_item['quantity']);
                            $this->updatePurchaseItem(NULL, $return_item['quantity'], $return_item['id']);
                        }
                    }
                    $this->db->update('sales', array('return_sale_ref' => $data['return_sale_ref'], 'surcharge' => $data['surcharge'], 'return_sale_total' => $data['grand_total'], 'return_id' => $sale_id), array('id' => $data['sale_id']));
                }

                if ($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid' && !empty($payment)) {
                    if (empty($payment['reference_no'])) {
                        $payment['reference_no'] = $this->site->getReference('pay');
                    }
                    $payment['sale_id'] = $sale_id;
                    if ($payment['paid_by'] == 'gift_card') {
                        $this->db->update('gift_cards', array('balance' => $payment['gc_balance']), array('card_no' => $payment['cc_no']));
                        unset($payment['gc_balance']);
                        $this->db->insert('payments', $payment);
                    } else {
                        if ($payment['paid_by'] == 'deposit') {
                            $customer = $this->site->getCompanyByID($data['customer_id']);
                            $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount - $payment['amount'])), array('id' => $customer->id));
                        }
                        $this->db->insert('payments', $payment);
                    }

                    if ($this->site->getReference('pay') == $payment['reference_no']) {
                        $this->site->updateReference('pay');
                    }

                    $this->site->syncSalePayments($sale_id);
                }

                $this->site->syncQuantity($sale_id);

                $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);

                if ($status === 'faturada') {
                    $this->load->model('service/CommissionsService_model', 'CommissionsService_model');
                    $this->CommissionsService_model->save($sale_id);//gerar comissao venda
                }

                $this->__confirmaTransacao();

            }
        } catch (Exception $exception) {
            $this->deleteSale($sale_id);
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }

        $this->gerarContrato($sale_id, $status);

        return $sale_id;

    }

    function adicionarParcelaOrcamento($sale_id, $status, $condicaoPagamentoId, $vencimentos, $tiposCobranca, $valoresVencimento, $movimentadores) {

        if ($status == 'orcamento') {

            $condicaoPagamento = $this->financeiro_model->getCondicaoPagamentoById($condicaoPagamentoId);
            $totalParcelas = $condicaoPagamento->parcelas;

            for ($index = 0; $index < $totalParcelas; $index++) {
                $vlVencimento = $valoresVencimento[$index];

                if ($vlVencimento > 0) {

                    $dataDigitada = $vencimentos[$index];
                    $tipoCobranca = $tiposCobranca[$index];
                    $movimentador = $movimentadores[$index];

                    $parcelas_orcamento = array(
                        'numero_parcela' => ($index + 1) . '/' . $totalParcelas,
                        'valorVencimento' => $vlVencimento,
                        'dtVencimento' => $dataDigitada,
                        'tipoCobrancaId' => $tipoCobranca,
                        'condicaoPagamentoId' => $condicaoPagamentoId,
                        'movimetnadorId' => $movimentador,
                        'saleId' => $sale_id,
                    );
                    $this->inserirParcelaOrcamento($parcelas_orcamento);
                }
            }
        }
    }

    function inserirParcelaOrcamento($data) {

        $this->db->insert('sales_parcelas_orcamento', $data);

        if ($this->db->affected_rows() == '1') return true;

        return FALSE;
    }

    private function lancarContaReceberParaOhFornecedorProdutoManual($item, $programacaoId, $conta_receber) {

        $reference = $this->site->getReference('so');
        $dtVencimento = date('Y-m-d');

        $tipoCobranca = $item['tipoCobrancaFornecedorId'];
        $valor = $item['valorRecebimentoValor'] *-1;

        $movimentador = 11;//TODO VIA CONFIGURACAO
        $receita = 16;//TODO VIA CONFIGURACAO

        $conta_receber_f = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'receita'               => $receita,
            'reference'             => $reference,
            'pessoa'                => $item['fornecedorId'],
            'tipocobranca'          => $tipoCobranca,
            'condicaopagamento'     => $item['condicaoPagamentoFornecedorId'],
            'dtvencimento'          => $dtVencimento,
            'dtcompetencia'         => $dtVencimento,
            'valor'                 => $valor,
            'programacaoId'         => $programacaoId,
            'sale'                  => $conta_receber['sale'],
            'sale_item'             => $conta_receber['sale_item'],
            'created_by'            => $conta_receber['created_by'],
            'warehouse'             => $conta_receber['warehouse'],
        );

        $contaReceber = new ContaReceber_model();
        $contaReceber->data = $conta_receber_f;
        $contaReceber->valores = [$valor];
        $contaReceber->vencimentos = [$dtVencimento];
        $contaReceber->cobrancas = [$tipoCobranca];
        $contaReceber->movimentadores = [$movimentador];
        $contaReceber->descontos = [0];

        return $this->financeiro_model->adicionarReceita($contaReceber);
    }

    private function lancarContaPagarFornecedorReceptivoDeUmServicoAdicional($fornecedorId, $programacaoId, $conta_receber, $valorPagarAoFornecedorReceptivo) {

        $reference = $this->site->getReference('so');
        $dataDigitada = date('Y-m-d');
        $dataVencimento = null;

        $tipoCobranca = 19;//TODO VIA CONFIGURACAO
        $condicaoPagamento = 1;//TODO VIA CONFIGURACAO
        $movimentador = 11;//TODO VIA CONFIGURACAO
        $despesa = 8;//TODO VIA CONFIGURACAO

        $condicaoPagamento  = $this->financeiro_model->getCondicaoPagamentoById($condicaoPagamento);
        $totalParcelas      = $condicaoPagamento->parcelas;

        $conta_pagar = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'despesa'               => $despesa,
            'reference'             => $reference,
            'tipocobranca'          => $tipoCobranca,
            'dtvencimento'          => $dataDigitada,
            'dtcompetencia'         => $dataDigitada,
            'valor'                 => $valorPagarAoFornecedorReceptivo,
            'pessoa'                => $fornecedorId,
            'programacaoId'         => $programacaoId,
            'condicaopagamento'     => $condicaoPagamento->id,
            'created_by'            => $conta_receber['created_by'],
            'warehouse'             => $conta_receber['warehouse'],
            'sale'                  => $conta_receber['sale'],
            'sale_item'             => $conta_receber['sale_item']
        );

        for($index=0;$index<$totalParcelas;$index++) {

            if ($index > 0) $dataVencimento = date("Y-m-d", strtotime($dataVencimento. " +1 month"));

            $dtVencimentoArrray[] = $dataVencimento;
            $valorRAVArray[] = $valorPagarAoFornecedorReceptivo/$totalParcelas;
            $tipoCobrancaArray[] = $tipoCobranca;
            $movimentadorArray[] = $movimentador;
            $descontoArray[] = 0;
        }

        $contaPagar = new ContaPagar_model();
        $contaPagar->data = $conta_pagar;
        $contaPagar->valores = $valorRAVArray;
        $contaPagar->vencimentos = $dtVencimentoArrray;
        $contaPagar->cobrancas = $tipoCobrancaArray;
        $contaPagar->movimentadores = $movimentadorArray;
        $contaPagar->descontos = $descontoArray;

        return $this->financeiro_model->adicionarDespesa($contaPagar);
    }

    private function lancarContaPagarParaOhFornecedorProdutoManual($item, $programacaoId, $conta_receber) {

        $reference = $this->site->getReference('so');
        $dataDigitada = date('Y-m-d');
        $dataVencimento = null;

        $tipoCobranca = $item['tipoCobrancaFornecedorId'];
        $valorRAV = $item['valorRecebimentoValor'];
        $condicaoPagamento = $item['condicaoPagamentoFornecedorId'];

        $condicaoPagamento  = $this->financeiro_model->getCondicaoPagamentoById($condicaoPagamento);
        $totalParcelas      = $condicaoPagamento->parcelas;

        $movimentador = 11;//TODO VIA CONFIGURACAO
        $despesa = 7;//TODO VIA CONFIGURACAO

        $conta_pagar = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'despesa'               => $despesa,
            'reference'             => $reference,
            'tipocobranca'          => $tipoCobranca,
            'dtvencimento'          => $dataDigitada,
            'dtcompetencia'         => $dataDigitada,
            'valor'                 => $valorRAV,
            'condicaopagamento'     => $condicaoPagamento->id,
            'programacaoId'         => $programacaoId,
            'pessoa'                => $item['fornecedorId'],
            'created_by'            => $conta_receber['created_by'],
            'warehouse'             => $conta_receber['warehouse'],
            'sale'                  => $conta_receber['sale'],
            'sale_item'             => $conta_receber['sale_item']
        );

        for($index=0;$index<$totalParcelas;$index++) {

            if ($index > 0) $dataVencimento = date("Y-m-d", strtotime($dataVencimento. " +1 month"));

            $dtVencimentoArrray[] = $dataVencimento;
            $valorRAVArray[] = $valorRAV/$totalParcelas;
            $tipoCobrancaArray[] = $tipoCobranca;
            $movimentadorArray[] = $movimentador;
            $descontoArray[] = 0;
        }

        $contaPagar = new ContaPagar_model();
        $contaPagar->data = $conta_pagar;
        $contaPagar->valores = $valorRAVArray;
        $contaPagar->vencimentos = $dtVencimentoArrray;
        $contaPagar->cobrancas = $tipoCobrancaArray;
        $contaPagar->movimentadores = $movimentadorArray;
        $contaPagar->descontos = $descontoArray;

        return $this->financeiro_model->adicionarDespesa($contaPagar);
    }

    private function lancarComissaoDoVendedorPorTipoDeProduto($conta_receber, $programacaoId, $biller_id, $product_code, $totalDaPassagem) {

        $produto                = $this->getProductByCode($product_code);
        $tipoComissaoProduto    = $produto->tipoComissao;
        $tipoCalculoComissao    = $produto->tipoCalculoComissao;
        $comissao               = 0;

        if ($tipoComissaoProduto == 'comissao_produto') {
            $percentualComissao = $produto->comissao;

            if ($tipoCalculoComissao == '1') {
                $comissao = $totalDaPassagem * ($percentualComissao / 100);
            } else {
                $comissao = $percentualComissao;
            }
        }

        if ($tipoComissaoProduto == 'comissao_vendedor') {
            $vendedor           = $this->companies_model->getCompanyByID($biller_id);
            $percentualComissao = $vendedor->comissao;
            $comissao = $totalDaPassagem * ($percentualComissao / 100);
        }

        if ($tipoComissaoProduto == 'comissao_categoria') {
            $cagegoria          = $this->getCategoryByCId($produto->category_id);
            $percentualComissao = $cagegoria->comissao;
            $comissao           = $totalDaPassagem * ($percentualComissao / 100);
        }

        if ($comissao > 0) return $this->lancarComissaoDoVendedorComissaoManual($conta_receber, $programacaoId , $biller_id, $comissao);

        return null;
    }

    private function lancarComissaoDoVendedorComissaoManual($conta_receber, $programacaoId, $biller_id, $comissao) {
        return $this->lancarComissaoDoVendedor($conta_receber, $programacaoId, $biller_id, $comissao);
    }

    private function lancarComissaoDoVendedor($conta_receber, $programacaoId, $biller_id, $comissao) {

        if ($comissao <= 0) return FALSE;

        $reference = $this->site->getReference('so');
        $dtVencimento = date('Y-m-d');

        $tipoCobranca = 19;//TODO VIA CONFIGURACAO
        $condicaoPagamento = 1;//TODO VIA CONFIGURACAO
        $despesa = 3;//TODO VIA CONFIGURACAO
        $movimentador = 11;//TODO VIA CONFIGURACAO

        $conta_pagar = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'created_by'            => $conta_receber['created_by'],
            'despesa'               => $despesa,
            'reference'             => $reference,
            'pessoa'                => $biller_id,
            'tipocobranca'          => $tipoCobranca,
            'condicaopagamento'     => $condicaoPagamento,
            'dtvencimento'          => $dtVencimento,
            'dtcompetencia'         => $dtVencimento,
            'valor'                 => $comissao,
            'warehouse'             => $conta_receber['warehouse'],
            'sale_item'             => $conta_receber['sale_item'],
            'sale'                  => $conta_receber['sale'],
            'programacaoId'         => $programacaoId
        );

        $contaPagar = new ContaPagar_model();
        $contaPagar->data = $conta_pagar;
        $contaPagar->valores = [$comissao];
        $contaPagar->vencimentos = [$dtVencimento];
        $contaPagar->cobrancas = [$tipoCobranca];
        $contaPagar->movimentadores = [$movimentador];
        $contaPagar->descontos = [0];

        return $this->financeiro_model->adicionarDespesa($contaPagar);
    }


    public function addSaleReturnId($data = array(), $items = array(), $payment = array(), $si_return = array())
    {

        $cost = $this->site->costing($items);
        // $this->sma->print_arrays($cost);

        if ($this->db->insert('sales', $data)) {
            $sale_id = $this->db->insert_id();
            if ($this->site->getReference('so') == $data['reference_no']) {
                $this->site->updateReference('so');
            }
            foreach ($items as $item) {

                $item['sale_id'] = $sale_id;
                $this->db->insert('sale_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed') {

                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        $item_cost['sale_item_id'] = $sale_item_id;
                        $item_cost['sale_id'] = $sale_id;
                        if(! isset($item_cost['pi_overselling'])) {
                            $this->db->insert('costing', $item_cost);
                        }
                    }

                }
            }

            if ($data['sale_status'] == 'completed') {
                $this->site->syncPurchaseItems($cost);
            }

            if (!empty($si_return)) {
                foreach ($si_return as $return_item) {
                    $product = $this->site->getProductByID($return_item['product_id']);
                    if ($product->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($return_item['product_id'], $return_item['warehouse_id']);
                        foreach ($combo_items as $combo_item) {
                            $this->updateCostingLine($return_item['id'], $combo_item->id, $return_item['quantity']);
                            $this->updatePurchaseItem(NULL,($return_item['quantity']*$combo_item->qty), NULL, $combo_item->id, $return_item['warehouse_id']);
                        }
                    } else {
                        $this->updateCostingLine($return_item['id'], $return_item['product_id'], $return_item['quantity']);
                        $this->updatePurchaseItem(NULL, $return_item['quantity'], $return_item['id']);
                    }
                }
                $this->db->update('sales', array('return_sale_ref' => $data['return_sale_ref'], 'surcharge' => $data['surcharge'],'return_sale_total' => $data['grand_total'], 'return_id' => $sale_id), array('id' => $data['sale_id']));
            }

            if ($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid' && !empty($payment)) {
                if (empty($payment['reference_no'])) {
                    $payment['reference_no'] = $this->site->getReference('pay');
                }
                $payment['sale_id'] = $sale_id;
                if ($payment['paid_by'] == 'gift_card') {
                    $this->db->update('gift_cards', array('balance' => $payment['gc_balance']), array('card_no' => $payment['cc_no']));
                    unset($payment['gc_balance']);
                    $this->db->insert('payments', $payment);
                } else {
                    if ($payment['paid_by'] == 'deposit') {
                        $customer = $this->site->getCompanyByID($data['customer_id']);
                        $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$payment['amount'])), array('id' => $customer->id));
                    }
                    $this->db->insert('payments', $payment);
                }
                if ($this->site->getReference('pay') == $payment['reference_no']) {
                    $this->site->updateReference('pay');
                }
                $this->site->syncSalePayments($sale_id);

            }

            $this->site->syncQuantity($sale_id);
            $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);
            return $sale_id;
        }
        return false;
    }

    public function updateSale($id, $vendaModel)
    {
        try {

            $this->__iniciaTransacao();
            //$vendaModel = new Venda_model();

            $data   = $vendaModel->data;
            $items  = $vendaModel->products;
            $status = $data['sale_status'];

            //$isAdmin = $this->sma->in_group('admin') ? TRUE : NULL;

            if ($data['payment_status'] == 'cancel') {

                if (!$this->Customer &&
                    !$this->Supplier &&
                    !$this->Owner &&
                    !$this->Admin &&
                    !$this->session->userdata('view_right')) {

                    throw new Exception('Esta venda não pode ser editada pois se encontra cancelada! Entre em contato com o Administrador da Agência para que ele faça ativação da Venda.');
                }

                $data['payment_status'] = 'due';
            }

            $item_antigo = $this->getAllInvoiceItems($id);
            $totalItensVendaAntigoArray = [];
            $totalItensVendaAtualArray  = [];

            $totalItensAntigoTiposHospedagemArray = [];
            $totalItensAtualTiposHospedagemArray  = [];

            $programacaoId =  null;

            $sale_antigo = $this->getInvoiceByID($id);

            foreach ($item_antigo as $itemA) {
                if (!$itemA->adicional && $itemA->descontarVaga == '1') {
                    $programacaoId  = $itemA->programacaoId;
                    $hospedagem     = $itemA->tipoHospedagemId;
                    $quantity       = $itemA->quantity;

                    if ($sale_antigo->sale_status == 'faturada' || $sale_antigo->sale_status == 'orcamento') {
                        $totalItensVendaAntigoArray[$programacaoId]          = $totalItensVendaAntigoArray[$programacaoId] + $quantity;
                        $totalItensAntigoTiposHospedagemArray[$hospedagem]   = $totalItensAntigoTiposHospedagemArray[$hospedagem] + $quantity;
                    }
                }
            }

            foreach ($items as $item) {
                if (!$item['adicional'] && $item['descontarVaga'] == '1') {

                    $programacaoId  = $item['programacaoId'];
                    $hospedagem     = $item['tipoHospedagem'];
                    $quantity       = $item['quantity'];

                    $totalItensVendaAtualArray[$programacaoId]          = $totalItensVendaAtualArray[$programacaoId] + $quantity;
                    $totalItensAtualTiposHospedagemArray[$hospedagem]   = $totalItensAtualTiposHospedagemArray[$hospedagem] + $quantity;
                }
            }

            foreach ($items as $item) {

                $product                = $this->site->getProductByID($item['product_id']);

                $programacaoId          = $item['programacaoId'];
                $tipo_hospedagem        = $item['tipoHospedagem'];
                $faixaId                = $item['faixaId'];
                $customerClientName     = $item['customerClientName'];
                $customerClientID       = $item['customerClient'];
                $tipoTransporteID       = $item['tipoTransporte'];
                $poltronaClient         = $item['poltronaClient'];
                $itemSaleIdOud          = $item['itemId'];

                $totalItensVendaAtual   = $totalItensVendaAtualArray[$programacaoId];
                $totalItensVendaAntigo  = $totalItensVendaAntigoArray[$programacaoId];

                $totalItensVendaAntigoTipoHospedagem  = $totalItensAntigoTiposHospedagemArray[$tipo_hospedagem];
                $totalItensVendaAtualTipoHospedagem   = $totalItensAtualTiposHospedagemArray[$tipo_hospedagem];

                if ($this->naoPossuiVagasSuficienteDisponivel($programacaoId, $totalItensVendaAtual, $totalItensVendaAntigo) && $status == 'faturada') {
                    throw new Exception(lang('produto_com_estoque_negativo'));
                }

                if ($product->controle_estoque_hospedagem) {
                    if ($this->naoPoussuiVagasSuficienteDisponivelHospedagem($programacaoId, $tipo_hospedagem, $totalItensVendaAtualTipoHospedagem, $totalItensVendaAntigoTipoHospedagem)){
                        throw new Exception(lang('produto_com_estoque_tipo_hospedagem_negativo'));
                    }
                }

                if ($faixaId == null) {
                    throw new Exception(lang('faixa_nao_cadastrada_na_venda').$customerClientName);
                }

                if ($poltronaClient) {
                    $isAssentoBloqueadoOcupado = $this->BusService_model->isAssentoBloqueadoOuMarcado($product->id, $programacaoId, $tipoTransporteID, $poltronaClient, $itemSaleIdOud);

                    if ($isAssentoBloqueadoOcupado) {
                        throw new Exception(lang('assento_ja_bloqueado_ocultado').' Assento duplicado '.$poltronaClient);
                    }
                }
            }

            $this->resetSaleActions($id);

            if ($data['sale_status'] == 'completed') {
                $cost = $this->site->costing($items);
            }

            // $this->sma->print_arrays($cost);

            $apenasEditar = $vendaModel->apenasEditar;

            if ($apenasEditar == '0') {

                if ($data['payment_status'] == 'paid' ||
                    $data['payment_status'] == 'partial' ) {
                    throw new Exception('A Venda não pode ser fatura novamente pois já possui recebimentos. <br/>Para alterar a venda e faturar é necessário
                    estornar os recebimentos do cliente e tentar novamente!');
                }

                $faturasContaReceber = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
                //$faturasContaPagar = $this->financeiro_model->getParcelasFaturaByContaPagar($id);

                foreach ($faturasContaReceber as $faturasCredito) {
                    $this->FinanceiroService_model->cancelar($faturasCredito->id, NULL);
                }

                /*
                foreach ($faturasContaPagar as $faturasDebito) {
                    $this->FinanceiroService_model->cancelar($faturasDebito->id, NULL);//todo esta comentando por enquanto para o erro vir do cancelamento da comissao
                }
                */

                $conta_receber = $vendaModel->conta_receber;
                $conta_receber['sale'] = $id;
                $conta_receber['programacaoId'] = $programacaoId;

                $contaReceberId = $this->adicionarContaReceberCliente(
                    $data,
                    $status,
                    $conta_receber,
                    $vendaModel->valorVencimentos,
                    $vendaModel->vencimentos,
                    $vendaModel->tiposCobranca,
                    $vendaModel->movimentadores,
                    $vendaModel->descontos);
            }

            if ($this->db->update('sales', $data, array('id' => $id)) &&
                $this->db->delete('sale_items', array('sale_id' => $id)) &&
                $this->db->delete('sale_item_adicional', array('saleId' => $id)) &&
                $this->db->delete('aereo_itinerario', array('saleId' => $id)) &&
                $this->db->delete('transfer_itinerario', array('saleId' => $id)) &&
                $this->db->delete('sales_parcelas_orcamento', array('saleId' => $id)) &&
                $this->db->delete('costing', array('sale_id' => $id))) {

                if ($product->permiteVendaClienteDuplicidade == '0' && $product->viagem == 1 && !$product->isApenasColetarPagador == 1) {
                    $duplicidadeCliente = $this->verificaDuplicidadeClienteVenda($customerClientID, $programacaoId);

                    if ($duplicidadeCliente) {
                        throw new Exception(lang('cliente_duplicidade_venda').$customerClientName);
                    }
                }

                if ($apenasEditar == '0') {
                    //TODO VAMOS MELHORAR ESSES EVENTOS PARA EDICAO
                    $user = $this->site->getUser($data['created_by']);
                    $this->SaleEventService_model->editEvent($id, 'Venda Editada e Faturada', $user);
                } else {
                    //TODO VAMOS MELHORAR ESSES EVENTOS PARA EDICAO
                    $user = $this->site->getUser($data['created_by']);
                    $this->SaleEventService_model->editEvent($id, 'Venda Apenas Editada', $user);
                }

                $product_id = null;

                $this->adicionarParcelaOrcamento(
                    $id,
                    $status,
                    $data['condicaopagamentoId'],
                    $vendaModel->vencimentos,
                    $vendaModel->tiposCobranca,
                    $vendaModel->valorVencimentos,
                    $vendaModel->movimentadores);

                foreach ($items as $item) {

                    $comissaoManual = $item['product_comissao'];//TODO comissao digitado pelo produto

                    $item['contaReceberId'] = $contaReceberId;
                    $item['sale_id']        = $id;
                    $item['cupom_id']       = $data['cupom_id'];//TODO CUPOM DA VENDA
                    $itemSaleIdOud          = $item['itemId'];
                    $itinerarios            = $item['itinerarios'];
                    $transfers              = $item['transfers'];
                    $servicosAdicionais     = $item['servicosAdicionais'];

                    unset($item['itinerarios']);
                    unset($item['transfers']);
                    unset($item['servicosAdicionais']);
                    unset($item['itemId']);

                    $this->db->insert('sale_items', $item);
                    $sale_item_id = $this->db->insert_id();

                    $isComHospedagem = $this->Roomlisthospede_model->getHospedagemHospedeByItemVenda($itemSaleIdOud);

                    if ($isComHospedagem) {
                        $data_hospedagem = array('itemId' =>   $sale_item_id, 'customer_id' => $item['customerClient'] );
                        $this->Roomlisthospede_model->update($isComHospedagem->id, $data_hospedagem);
                    }

                    //TODO ATUALIZANDO OS ITENS DO ITINERARIO ONDE O ITEM FOI INSERIDO
                    $this->db->update('itinerario_items', array('sale_item' => $sale_item_id), array('sale_item' => $itemSaleIdOud));

                    if ($status === 'faturada' && $apenasEditar == '0') {

                        $conta_receber['sale_item'] = $sale_item_id;

                        /*
                        if ($comissaoManual > 0) {
                            $this->lancarComissaoDoVendedorComissaoManual(
                                $conta_receber,
                                $item['programacaoId'],
                                $data['biller_id'],
                                $comissaoManual);
                        } else {
                            $this->lancarComissaoDoVendedorPorTipoDeProduto(
                                $conta_receber,
                                $item['programacaoId'],
                                $data['biller_id'],
                                $item['product_code'],
                                $item['subtotal']);
                        }
                        */

                        if ($item['fornecedorId'] != '') {

                            $valorRAV = $item['valorRecebimentoValor'];

                            if ($valorRAV > 0) {
                                $this->lancarContaPagarParaOhFornecedorProdutoManual($item,  $item['programacaoId'], $vendaModel->conta_receber);
                            } else if ($valorRAV < 0) {
                                $this->lancarContaReceberParaOhFornecedorProdutoManual($item, $item['programacaoId'], $vendaModel->conta_receber);
                            }
                        }

                        //comissao do receptivo
                        if ($item['servicoPai'] != '') {

                            $comboItem = $this->products_model->getProductComboItemById($item['servicoPai'], $item['product_id']);

                            if ($comboItem->fornecedorId > 0 && $comboItem->comissao > 0) {

                                $comissaoDaAgencia = $comboItem->comissao;
                                $subTotal = $item['subtotal'];

                                $valorPagarAoFornecedorReceptivo = $subTotal - ($subTotal * ($comissaoDaAgencia / 100));

                                $this->lancarContaPagarFornecedorReceptivoDeUmServicoAdicional(
                                    $comboItem->fornecedorId ,
                                    $item['programacaoId'],
                                    $conta_receber,
                                    $valorPagarAoFornecedorReceptivo);
                            }
                        }
                    }

                    foreach ($servicosAdicionais as $servicosAdicional) {
                        $servicosAdicional['saleId'] = $id;
                        $servicosAdicional['sale_item'] = $sale_item_id;

                        $this->db->insert('sale_item_adicional', $servicosAdicional);
                    }

                    foreach ($itinerarios as $itinerario) {
                        $itinerario['saleId'] = $id;
                        $itinerario['sale_item'] = $sale_item_id;

                        $this->db->insert('aereo_itinerario', $itinerario);
                    }

                    foreach ($transfers as $transfer) {
                        $transfer['saleId'] = $id;
                        $transfer['sale_item'] = $sale_item_id;

                        $this->db->insert('transfer_itinerario', $transfer);
                    }

                    if ($data['sale_status'] == 'completed' &&
                        $this->site->getProductByID($item['product_id'])) {
                        $item_costs = $this->site->item_costing($item);

                        foreach ($item_costs as $item_cost) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $id;
                            if (!isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        }
                    }
                }

                $purchases = $this->db->get_where('purchases', array('supplier_id' => $data['biller_id'], 'warehouse_id' => $data['warehouse_id']), 1);

                if ($purchases->num_rows() > 0) {
                    $this->db->delete('purchase_items', array('purchase_id' => $purchases->row()->id));
                }

                if ($data['sale_status'] == 'completed') {
                    $this->site->syncPurchaseItems($cost);
                }

                $this->site->syncQuantity($id);

                $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);

                if ($status === 'faturada' && $apenasEditar == '0') {
                    $this->load->model('service/CommissionsService_model', 'CommissionsService_model');
                    $this->CommissionsService_model->save($id);//gerar comissao venda
                }

                $this->__confirmaTransacao();
            }

        } catch (Exception $exception) {
            $this->__cancelaTransacao();
            throw new Exception($exception->getMessage());
        }

        $this->gerarContrato($id, $status, $apenasEditar, true);

        return true;
    }

    private function gerarContrato($saleID, $status, $apenasEditar = '0', $edit = false)
    {
        try {
            if ($status === 'faturada' && $apenasEditar == '0') {
                if ($this->Settings->gerar_contrato_faturamento_venda) {
                    $plugsignSetting = $this->site->getPlugsignSettings();
                    if ($plugsignSetting->active && $plugsignSetting->token) {
                        $this->load->model('service/DocumentService_model', 'DocumentService_model');
                        $this->DocumentService_model->generateSaleContract($saleID, null, $edit);
                    }
                }
            }
        } catch (Exception $exception) {
            $this->load->model('service/ContractEventService_model', 'ContractEventService_model');
            $this->ContractEventService_model->error_sale($saleID, $exception->getMessage());// todo registrar um erro ao emitir o contrato
        }
    }

    private function naoPossuiVagasSuficienteDisponivel($programacaoId, $totalItensVenda, $estoqueAntesAtualizar = 0) {
        if ($programacaoId) {

            $programacao    = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
            $estoqueAtual   = $programacao->getTotalDisponvel() + $estoqueAntesAtualizar;
            $possuiVaga     = $estoqueAtual - $totalItensVenda;

            if ($possuiVaga < 0) return true;
        }

        return false;
    }

    private function naoPoussuiVagasSuficienteDisponivelHospedagem($programacaoId, $tipo_hospedagem, $totalItensVenda, $estoqueAntesAtualizar = 0) {
        if ($programacaoId) {

            $programacao    = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);

            foreach ($programacao->getQuartos() as $quarto) {

                if ($quarto->tipo_hospedagem == $tipo_hospedagem) {
                    $estoqueAtual   = $quarto->estoque_atual + $estoqueAntesAtualizar;
                    $possuiVaga     = $estoqueAtual - $totalItensVenda;

                    if ($possuiVaga < 0) return true;
                }
            }
        }

        return false;
    }

    private function adicionarContaReceberCliente($data,
                                                  $status,
                                                  $conta_receber = array(),
                                                  $valor_vencimento_parcelas = array(),
                                                  $data_vencimento_parcelas = array() ,
                                                  $tipo_cobranca_parcelas = array() ,
                                                  $movimentadores = array(),
                                                  $desconto_parcelas = array()) {
        $contaReceberId = null;

        if ($status === 'faturada') {

            $valor = $conta_receber['valor'];

            if ($valor > 0) {

                //field cc
                $senderHash         = $conta_receber['senderHash'];
                $creditCardToken    = $conta_receber['creditCardToken'];
                $deviceId           = $conta_receber['deviceId'];

                $parcelas           = $conta_receber['parcelas'];

                $cardName           = $conta_receber['cardName'];
                $cardNumber         = $conta_receber['cardNumber'];
                $cardExpiryMonth    = $conta_receber['cardExpiryMonth'];
                $cardExpiryYear     = $conta_receber['cardExpiryYear'];
                $cvc                = $conta_receber['cvc'];

                $cpfTitularCartao                   = $conta_receber['cpfTitularCartao'];
                $celularTitularCartao               = $conta_receber['celularTitularCartao'];
                $cepTitularCartao                   = $conta_receber['cepTitularCartao'];
                $enderecoTitularCartao              = $conta_receber['enderecoTitularCartao'];
                $numeroEnderecoTitularCartao        = $conta_receber['numeroEnderecoTitularCartao'];
                $complementoEnderecoTitularCartao   = $conta_receber['complementoEnderecoTitularCartao'];
                $bairroTitularCartao                = $conta_receber['bairroTitularCartao'];
                $cidadeTitularCartao                = $conta_receber['cidadeTitularCartao'];
                $estadoTitularCartao                = $conta_receber['estadoTitularCartao'];
                $totalParcelaPagSeguro              = $conta_receber['totalParcelaPagSeguro'];

                unset($conta_receber['senderHash']);
                unset($conta_receber['creditCardToken']);
                unset($conta_receber['deviceId']);

                unset($conta_receber['parcelas']);

                unset($conta_receber['cardName']);
                unset($conta_receber['cardNumber']);
                unset($conta_receber['cardExpiryMonth']);
                unset($conta_receber['cardExpiryYear']);
                unset($conta_receber['cvc']);

                unset($conta_receber['cpfTitularCartao']);
                unset($conta_receber['celularTitularCartao']);
                unset($conta_receber['cepTitularCartao']);
                unset($conta_receber['enderecoTitularCartao']);
                unset($conta_receber['numeroEnderecoTitularCartao']);
                unset($conta_receber['complementoEnderecoTitularCartao']);
                unset($conta_receber['bairroTitularCartao']);
                unset($conta_receber['cidadeTitularCartao']);
                unset($conta_receber['estadoTitularCartao']);
                unset($conta_receber['totalParcelaPagSeguro']);

                $contaReceber = new ContaReceber_model();
                $contaReceber->data = $conta_receber;
                $contaReceber->valores = $valor_vencimento_parcelas;
                $contaReceber->vencimentos = $data_vencimento_parcelas;
                $contaReceber->cobrancas = $tipo_cobranca_parcelas;
                $contaReceber->movimentadores = $movimentadores;
                $contaReceber->descontos = $desconto_parcelas;

                //field cc
                $contaReceber->senderHash = $senderHash;
                $contaReceber->creditCardToken = $creditCardToken;
                $contaReceber->deviceId = $deviceId;

                $contaReceber->numeroParcelas = $parcelas;

                $contaReceber->cardName = $cardName;
                $contaReceber->cardNumber = $cardNumber;
                $contaReceber->cardExpiryMonth = $cardExpiryMonth;
                $contaReceber->cardExpiryYear = $cardExpiryYear;
                $contaReceber->cvc = $cvc;

                $contaReceber->cpfTitularCartao = $cpfTitularCartao;
                $contaReceber->celularTitularCartao = $celularTitularCartao;
                $contaReceber->cepTitularCartao = $cepTitularCartao;
                $contaReceber->enderecoTitularCartao = $enderecoTitularCartao;
                $contaReceber->numeroEnderecoTitularCartao = $numeroEnderecoTitularCartao;
                $contaReceber->complementoEnderecoTitularCartao = $complementoEnderecoTitularCartao;
                $contaReceber->bairroTitularCartao = $bairroTitularCartao;
                $contaReceber->cidadeTitularCartao = $cidadeTitularCartao;
                $contaReceber->estadoTitularCartao = $estadoTitularCartao;
                $contaReceber->totalParcelaPagSeguro = $totalParcelaPagSeguro;

                if (!$data['venda_link'] ) {
                    $contaReceber->venda_manual = true;
                } else {
                    $contaReceber->venda_manual = false;
                }

                $contaReceberId = $this->financeiro_model->adicionarReceita($contaReceber);
            }
        }

        return $contaReceberId;
    }

    public function cancelar($motivoCancelamento) {
        return $this->VendaService_model->cancelar($motivoCancelamento);
    }

    public function archived($id) {
        return $this->VendaService_model->archived($id);
    }

    public function ratings($avaliar_dias = TRUE, $id = NULL) {
        return $this->VendaService_model->ratings($avaliar_dias, $id);
    }

    public function unarchive($id) {
        return $this->VendaService_model->unarchive($id);
    }

    public function deleteSale($id)
    {

       // $this->cancelar($id);

        $sale_items = $this->resetSaleActions($id);

        $this->db->delete('purchase_items', array('source_of_sale' => $id));

        if ($this->db->delete('payments', array('sale_id' => $id)) &&
            $this->db->delete('sale_item_adicional', array('saleId' => $id)) &&
            $this->db->delete('aereo_itinerario', array('saleId' => $id)) &&
            $this->db->delete('transfer_itinerario', array('saleId' => $id)) &&
            $this->db->delete('sale_items', array('sale_id' => $id)) &&
            $this->db->delete('sales', array('id' => $id)) &&
            $this->db->delete('sales', array('sale_id' => $id)) &&
            $this->db->delete('costing', array('sale_id' => $id))) {
            $this->site->syncQuantity(NULL, NULL, $sale_items);
            return true;
        }
        return FALSE;

    }

    public function resetSaleActions($id)
    {
        $sale = $this->getInvoiceByID($id);

        if ($sale->sale_status == 'returned' || $sale->return_id || $sale->return_sale_ref) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        if ($sale->credited_sale || $sale->refunded_sale) {
            $this->session->set_flashdata('warning', lang('action_credited_sale'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        $items = $this->getAllInvoiceItems($id);

        foreach ($items as $item) {

            if ($sale->sale_status == 'completed') {
                // if ($costings = $this->getCostingLines($item->id, $item->product_id)) {
                //     $quantity = $item->quantity;
                //     foreach ($costings as $cost) {
                //         if ($cost->quantity >= $quantity) {
                //             $qty = $cost->quantity - $quantity;
                //             $bln = $cost->quantity_balance ? $cost->quantity_balance + $quantity : $quantity;
                //             $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $cost->id));
                //             $quantity = 0;
                //         } elseif ($cost->quantity < $quantity) {
                //             $qty = $quantity - $cost->quantity;
                //             $this->db->delete('costing', array('id' => $cost->id));
                //             $quantity -= $qty;
                //         }
                //         if ($quantity == 0) {
                //             break;
                //         }
                //     }
                // }
                if ($item->product_type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item->product_id, $item->warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        if($combo_item->type == 'standard') {
                            $qty = ($item->quantity*$combo_item->qty);
                            $this->updatePurchaseItem(NULL, $qty, NULL, $combo_item->id,  '1');
                        }
                    }
                } else {
                    $option_id = isset($item->option_id) && !empty($item->option_id) ? $item->option_id : NULL;
                    $this->updatePurchaseItem(NULL, $item->quantity, $item->id, $item->product_id,  '1', $option_id);
                }
            }
        }

        $this->site->syncQuantity(NULL, NULL, $items);
        $this->sma->update_award_points($sale->grand_total, $sale->customer_id, $sale->created_by, TRUE);
        return $items;
    }

    public function updatePurchaseItem($id, $qty, $sale_item_id, $product_id = NULL, $warehouse_id = NULL, $option_id = NULL)
    {
        if ($id) {
            if($pi = $this->getPurchaseItemByID($id)) {
                $pr = $this->site->getProductByID($pi->product_id);
                if ($pr->type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($pr->id, $pi->warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        if($combo_item->type == 'standard') {
                            $cpi = $this->site->getPurchasedItem(array('product_id' => $combo_item->id, 'warehouse_id' => $pi->warehouse_id, 'option_id' => NULL));
                            $bln = $pi->quantity_balance + ($qty*$combo_item->qty);
                            $this->db->update('purchase_items', array('quantity_balance' => $bln), array('id' => $combo_item->id));
                        }
                    }
                } else {
                    $bln = $pi->quantity_balance + $qty;
                    $this->db->update('purchase_items', array('quantity_balance' => $bln), array('id' => $id));
                }
            }
        } else {
            if ($sale_item_id) {
                if ($sale_item = $this->getSaleItemByID($sale_item_id)) {
                    $option_id = isset($sale_item->option_id) && !empty($sale_item->option_id) ? $sale_item->option_id : NULL;
                    $clause = array('product_id' => $sale_item->product_id, 'warehouse_id' => $sale_item->warehouse_id, 'option_id' => $option_id);
                    if ($pi = $this->site->getPurchasedItem($clause)) {
                        $quantity_balance = $pi->quantity_balance+$qty;
                        $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
                    } else {
                        $clause['purchase_id'] = NULL;
                        $clause['transfer_id'] = NULL;
                        $clause['quantity'] = 0;
                        $clause['quantity_balance'] = $qty;
                        $clause['warehouse_id'] = 1;

                        $this->db->insert('purchase_items', $clause);
                    }
                }
            } else {
                if ($product_id && $warehouse_id) {
                    $pr = $this->site->getProductByID($product_id);
                    $clause = array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
                    if ($pr->type == 'standard') {
                        if ($pi = $this->site->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance+$qty;
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
                        } else {
                            $clause['purchase_id'] = NULL;
                            $clause['transfer_id'] = NULL;
                            $clause['quantity'] = 0;
                            $clause['quantity_balance'] = $qty;
                            $clause['warehouse_id'] = 1;

                            $this->db->insert('purchase_items', $clause);
                        }
                    } elseif ($pr->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($pr->id, $warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            $clause = array('product_id' => $combo_item->id, 'warehouse_id' => $warehouse_id, 'option_id' => NULL);
                            if($combo_item->type == 'standard') {
                                if ($pi = $this->site->getPurchasedItem($clause)) {
                                    $quantity_balance = $pi->quantity_balance+($qty*$combo_item->qty);
                                    $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), $clause);
                                } else {
                                    $clause['transfer_id'] = NULL;
                                    $clause['purchase_id'] = NULL;
                                    $clause['quantity'] = 0;
                                    $clause['quantity_balance'] = $qty;
                                    $clause['warehouse_id'] = 1;

                                    $this->db->insert('purchase_items', $clause);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function atualizarDadosItemVenda($dados_migrar, $id) {
        echo $this->db->update('sale_items', $dados_migrar , array('id' => $id));
    }

    public function getPurchaseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getParcelasOrcamentoVenda($saleId)
    {
        $this->db->select('sales_parcelas_orcamento.*, tipo_cobranca.name as tipoCobranca, pos_register.name as movimentador');
        $this->db->where('saleId', $saleId);
        $this->db->order_by('dtVencimento', 'asc');

        $this->db->join('tipo_cobranca', 'tipo_cobranca.id = sales_parcelas_orcamento.tipoCobrancaId');
        $this->db->join('pos_register', 'pos_register.id = sales_parcelas_orcamento.movimetnadorId');

        $q = $this->db->get_where('sales_parcelas_orcamento');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function returnSale($data = array(), $items = array(), $payment = array())
    {

        foreach ($items as $item) {
            if ($item['product_type'] == 'combo') {
                $combo_items = $this->site->getProductComboItems($item['product_id'], $item['warehouse_id']);
                foreach ($combo_items as $combo_item) {
                    if ($costings = $this->getCostingLines($item['sale_item_id'], $combo_item->id)) {
                        $quantity = $item['quantity']*$combo_item->qty;
                        foreach ($costings as $cost) {
                            if ($cost->quantity >= $quantity) {
                                $qty = $cost->quantity - $quantity;
                                $bln = $cost->quantity_balance && $cost->quantity_balance >= $quantity ? $cost->quantity_balance - $quantity : 0;
                                $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $cost->id));
                                $quantity = 0;
                            } elseif ($cost->quantity < $quantity) {
                                $qty = $quantity - $cost->quantity;
                                $this->db->delete('costing', array('id' => $cost->id));
                                $quantity = $qty;
                            }
                        }
                    }
                    $this->updatePurchaseItem(NULL,($item['quantity']*$combo_item->qty), NULL, $combo_item->id, $item['warehouse_id']);
                }
            } else {
                if ($costings = $this->getCostingLines($item['sale_item_id'], $item['product_id'])) {
                    $quantity = $item['quantity'];
                    foreach ($costings as $cost) {
                        if ($cost->quantity >= $quantity) {
                            $qty = $cost->quantity - $quantity;
                            $bln = $cost->quantity_balance && $cost->quantity_balance >= $quantity ? $cost->quantity_balance - $quantity : 0;
                            $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $cost->id));
                            $quantity = 0;
                        } elseif ($cost->quantity < $quantity) {
                            $qty = $quantity - $cost->quantity;
                            $this->db->delete('costing', array('id' => $cost->id));
                            $quantity = $qty;
                        }
                    }
                }
                $this->updatePurchaseItem(NULL, $item['quantity'], $item['sale_item_id'], $item['product_id'], $item['warehouse_id'], $item['option_id']);
            }
        }
        // $this->sma->print_arrays($items);
        $sale_items = $this->site->getAllSaleItems($data['sale_id']);

        if ($this->db->insert('return_sales', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('re') == $data['reference_no']) {
                $this->site->updateReference('re');
            }
            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);

                if ($sale_item = $this->getSaleItemByID($item['sale_item_id'])) {
                    if ($sale_item->quantity == $item['quantity']) {
                        $this->db->delete('sale_items', array('id' => $item['sale_item_id']));
                    } else {
                        $nqty = $sale_item->quantity - $item['quantity'];
                        $tax = $sale_item->unit_price - $sale_item->net_unit_price;
                        $discount = $sale_item->item_discount / $sale_item->quantity;
                        $item_tax = $tax * $nqty;
                        $item_discount = $discount * $nqty;
                        $subtotal = $sale_item->unit_price * $nqty;
                        $this->db->update('sale_items', array('quantity' => $nqty, 'item_tax' => $item_tax, 'item_discount' => $item_discount, 'subtotal' => $subtotal), array('id' => $item['sale_item_id']));
                    }

                }
            }
            $this->calculateSaleTotals($data['sale_id'], $return_id, $data['surcharge']);
            if (!empty($payment)) {
                $payment['sale_id'] = $data['sale_id'];
                $payment['return_id'] = $return_id;
                $this->db->insert('payments', $payment);
                if ($this->site->getReference('pay') == $data['reference_no']) {
                    $this->site->updateReference('pay');
                }
                $this->site->syncSalePayments($data['sale_id']);
            }
            $this->site->syncQuantity(NULL, NULL, $sale_items);
            return true;
        }
        return false;
    }

    public function getCostingLines($sale_item_id, $product_id, $sale_id = NULL)
    {
        if ($sale_id) { $this->db->where('sale_id', $sale_id); }
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('costing', array('sale_item_id' => $sale_item_id, 'product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleItemByID($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSaleItemByContaReceber($contaReceberId)
    {
        $this->db->group_by('product_name');

        $q = $this->db->get_where('sale_items', array('contaReceberId' => $contaReceberId));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllItens()
    {
        $q = $this->db->get_where('sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscarItemDaVendaPorVendaId($saleItemId)
    {
        $this->db->select('sale_items.product_name, companies.name');

        $this->db->join('companies', 'companies.id = sale_items.customerClient');

        //$this->db->group_by('product_name');

        $q = $this->db->get_where('sale_items', array('sale_items.id' => $saleItemId));

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function calculateSaleTotals($id, $return_id, $surcharge)
    {
        $sale = $this->getInvoiceByID($id);
        $items = $this->getAllInvoiceItems($id);
        if (!empty($items)) {
            $this->sma->update_award_points($sale->grand_total, $sale->customer_id, $sale->created_by, TRUE);
            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            $total_items = 0;
            foreach ($items as $item) {
                $total_items += $item->quantity;
                $product_tax += $item->item_tax;
                $product_discount += $item->item_discount;
                $total += $item->net_unit_price * $item->quantity;
            }
            if ($sale->order_discount_id) {
                $percentage = '%';
                $order_discount_id = $sale->order_discount_id;
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = (($total + $product_tax) * (Float)($ods[0])) / 100;
                } else {
                    $order_discount = $order_discount_id;
                }
            }
            if ($sale->order_tax_id) {
                $order_tax_id = $sale->order_tax_id;
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $order_tax_details->rate;
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = (($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100;
                    }
                }
            }
            $total_discount = $order_discount + $product_discount;
            $total_tax = $product_tax + $order_tax;
            $grand_total = $total + $total_tax + $sale->shipping - $order_discount + $surcharge;
            $data = array(
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'return_id' => $return_id,
                'surcharge' => $surcharge
            );

            if ($this->db->update('sales', $data, array('id' => $id))) {
                $this->sma->update_award_points($data['grand_total'], $sale->customer_id, $sale->created_by);
                return true;
            }
        } else {
            $this->db->delete('sales', array('id' => $id));
            //$this->db->delete('payments', array('sale_id' => $id, 'return_id !=' => $return_id));
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addDelivery($data = array())
    {
        if ($this->db->insert('deliveries', $data)) {
            if ($this->site->getReference('do') == $data['do_reference_no']) {
                $this->site->updateReference('do');
            }
            return true;
        }
        return false;
    }

    public function updateDelivery($id, $data = array())
    {
        if ($this->db->update('deliveries', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getDeliveryByID($id)
    {
        $q = $this->db->get_where('deliveries', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDelivery($id)
    {
        if ($this->db->delete('deliveries', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaymentsInvoice($fatura_id) {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('fatura' => $fatura_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoicePayments($sale_id)
    {
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoicePaymentsByReports($sales_id)
    {
        $this->db->where_in('payments.sale_id', $sales_id);

        $q = $this->db->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllPayments()
    {
        $q = $this->db->get_where('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id)
    {
        $this->db->select('payments.*,
            fatura.tipooperacao as tipoOperacao');

        $this->db->join('fatura', 'fatura.id = payments.fatura');
        $q = $this->db->get_where('payments', array('payments.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentByCC($ccNo)
    {
        $this->db->select('payments.*, sales.date as dsale, sales.reference_no');

        $this->db->join('sales', 'sales.id = payments.sale_id');
        $q = $this->db->get_where('payments', array('cc_no' => $ccNo), 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function existePaymentByApprovalCode($approval_code)
    {
        $q = $this->db->get_where('payments', array('approval_code' => $approval_code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentsForSale($sale_id)
    {
        $this->db->select('payments.id as payment_id, payments.date, payments.status, payments.paid_by, payments.amount, payments.taxa, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type, attachment, fatura, note, status, motivo_estorno')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array(), $customer_id = NULL, $faturaId = NULL)
    {
		if ($data->payment_status != 'paid') {

            if ($data['approval_code']) {
                if ($this->existePaymentByApprovalCode($data['approval_code'])) {
                    return false;
                }
            }

            $valorPagamento = $data['amount'];
            $taxa           = $data['taxa'];
            $data['amount'] = $valorPagamento - $taxa;

            if ($data['cc_no']) {

                $gc     = $this->site->getGiftCardByNO($data['cc_no']);
                $saldo  =  ($gc->balance - $data['amount']);

                if ($saldo < 0) {
                    throw new Exception('Não é permitido realizar o pagamento com um valor maior que o crédito.');
                }

                $tbCobrancaPadrao  = $this->financeiro_model->getTipoCobrancaById($this->Settings->tipo_cobranca_credito_cliente_id);
                $formaPagamento    = $this->financeiro_model->getFormaPagamentoById($tbCobrancaPadrao->formapagamento);

                $data['tipoCobranca']   = $tbCobrancaPadrao->id;
                $data['movimentador']   = $tbCobrancaPadrao->conta;
                $data['formapagamento'] = $tbCobrancaPadrao->formapagamento;
                $data['paid_by']        = $formaPagamento->name;
            }

			if ($this->db->insert('payments', $data)) {

                $payment_id = $this->db->insert_id();

				if ($this->site->getReference('pay') == $data['reference_no']) {
                    $this->site->updateReference('pay');
                }

				$this->site->syncSalePayments($data['sale_id'], $taxa);

                $this->pagamentoFinanceiro($faturaId, $data, $payment_id);

                /*
				if ($data['paid_by'] == 'gift_card') {
					$gc = $this->site->getGiftCardByNO($data['cc_no']);
					$this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
				} elseif ($customer_id && $data['paid_by'] == 'deposit') {
					$customer = $this->site->getCompanyByID($customer_id);
					$this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
				}
                */

                //atualiza a carta de credito
                if ($data['cc_no']) {
                    $gc = $this->site->getGiftCardByNO($data['cc_no']);

                    $saldo  =  ($gc->balance - $data['amount']);
                    $status = 'paid';

                    if ($saldo > 0) {
                        $status = 'partial';
                    }

                    $this->db->update('gift_cards', array('balance' => $saldo, 'status' => $status), array('card_no' => $data['cc_no']));

                    //pagamento de uma conta com credito de cliente
                    if ($data['sale_id']) {
                        $this->pagamento_fatura_credito_cliente($payment_id, $gc, $valorPagamento, $data['note']);
                    }
                }

                if ($data['sale_id']) {
                    $user   = $this->site->getUser($data['created_by']);
                    $event  = "Pagamento Adicionado de R$". number_format($data['amount'], 2, ',', '.') . " Taxa de R$ ". number_format($taxa, 2, ',', '.') . " em ".$data['paid_by'];
                    $this->SaleEventService_model->pagamento($data['sale_id'], $event, $user);
                }

				return true;
			}
			return false;
		}
        return false;
    }

    private function pagamento_fatura_credito_cliente($payment_id, $gc, $valorPagamento, $note)
    {

        $tbCobrancaPadrao   = $this->financeiro_model->getTipoCobrancaById($this->Settings->tipo_cobranca_credito_cliente_id);
        $fatura             = $this->GiftRepository_model->getFaturaByGift($gc->id);
        $formaPagamento     = $this->financeiro_model->getFormaPagamentoById($tbCobrancaPadrao->formapagamento);

        $payment = array(
            'origin_payment_id' => $payment_id,
            'date'              => date('Y-m-d H:i:s'),
            'reference_no'      => $this->site->getReference('pay'),
            'sale_id'           => $gc->sale_id,
            'formapagamento'    => $tbCobrancaPadrao->formapagamento,
            'movimentador'      => $tbCobrancaPadrao->conta,
            'tipoCobranca'      => $tbCobrancaPadrao->id,
            'fatura'            => $fatura->faturaId,
            'pessoa'            => $fatura->customer_id,
            'amount'            => $valorPagamento,
            'paid_by'           => $formaPagamento->name,
            'note'              => $note,
            'cheque_no'         => '',
            'cc_no'             => '',
            'cc_holder'         => '',
            'cc_month'          => '',
            'cc_year'           => '',
            'cc_type'           => '',
            'type'              => 'sent',
            'tipo'              => Sales_model::TIPO_DE_PAGAMENTO_PAGAMENTO,
            'created_by'        => $this->session->userdata('user_id'),
        );

        $this->sales_model->addPayment($payment, $gc->customer_id, $fatura->faturaId);
    }

    function pagamentoFinanceiro($faturaId, $data, $payment_id) {

        $parcelasFatura = $this->financeiro_model->getParcelasByFatura($faturaId);

        $taxa           = (float) $data['taxa'];
        $acrescimo      = (float) $data['acrescimo'];
        $pagamento      = (float) $data['amount'];
        $dataPagamento  = $data['date'];

        $valorPagamento =  $pagamento - $acrescimo + $taxa;

        foreach ($parcelasFatura as $parcelaFatura) {

            $parcela = $this->financeiro_model->getParcelaById($parcelaFatura->parcela);

            $valorPagar             = (float) $parcela->valorpagar;
            $valorPagoAteMomento    = (float) $parcela->valorpago;

            $saldo  = (float) ($valorPagar - $valorPagamento);
            $status = Financeiro_model::STATUS_PARCIAL;

            if ($saldo <= 0) {
                $status = Financeiro_model::STATUS_QUITADA;
            }

            $atualizarParcela = array(
                'dtultimopagamento' => $dataPagamento,
                'valorpago' => $valorPagoAteMomento + $valorPagamento + $data['acrescimo'] - $data['taxa'],
                'valorpagar' => $saldo,
                'status' => $status,
                'taxas' => $parcela->taxas + $taxa,
                'totalAcrescimo' =>$parcela->totalAcrescimo + $acrescimo,
                'acrescimo' => $parcela->acrescimo + $acrescimo,
                'multa' => 0,
                'mora' => 0,
                'percentualmulta' => 0,
                'percentualmora' => 0,
                'diasatraso' => 0
            );

            $atualizarFatura = array(
                'dtultimopagamento' => $dataPagamento,
                'valorpago' => $valorPagoAteMomento + $valorPagamento + $data['acrescimo'] - $data['taxa'],
                'valorpagar' => $saldo,
                'status' => $status,
                'taxas' => $parcela->taxas + $taxa,
                'totalAcrescimo' =>$parcela->totalAcrescimo + $acrescimo,
                'acrescimo' => $parcela->acrescimo + $acrescimo
            );

            $this->financeiro_model->edit('fatura', $atualizarFatura, 'id', $faturaId);
            $this->financeiro_model->edit('parcela_fatura', array('status' => $status), 'id', $parcelaFatura->id);
            $this->financeiro_model->edit('parcela', $atualizarParcela, 'id', $parcela->id);
            $this->financeiro_model->edit('fatura_cobranca', array('status' => Financeiro_model::STATUS_PAGO) , 'code', $data['approval_code']);

            if ($parcela->contareceberId > 0) {

                $this->financeiro_model->edit('conta_receber',  array('status' => $status), 'id', $parcela->contareceberId);

                $this->baixarAgendamentoOnline($parcela->contareceberId);

                $fatura_cobranca = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($faturaId);

                if ($fatura_cobranca) {
                    $payment_data = array(
                        'data_pagamento' => $data['date'],
                        'valor_pago' => $valorPagamento,
                    );
                    $this->financeiro_model->confirmacao_pagamento($fatura_cobranca, $fatura_cobranca->code, $payment_data);
                    $this->financeiro_model->edit('fatura_cobranca', array('status' => Financeiro_model::STATUS_PAGO) , 'code', $fatura_cobranca->code);
                }

            }

            if ($parcela->contapagarId > 0) {

                if ($parcela->fechamento_comissao_id) {

                    $this->financeiro_model->edit('payments', array('fechamento_comissao_id' => $parcela->fechamento_comissao_id, 'fechamento_item_id' => $parcela->fechamento_item_id), 'id', $payment_id);

                    $this->atualizar_comissao_vendedor($parcela->fechamento_comissao_id, $parcela->fechamento_item_id, $valorPagamento, $status);
                }

                $this->financeiro_model->edit('conta_pagar',  array('status' => $status), 'id', $parcela->contapagarId );
            }
        }

        $this->atualizarMovimentadorSaldoNoPeriodo($data);
    }

    private function atualizar_comissao_vendedor($fechamento_id, $fechamento_item_id, $amount, $status)
    {
        $this->db->set('paid', 'paid+'.$amount, FALSE);
        $this->db->where('id', $fechamento_id);
        $this->db->update('close_commissions');

        $this->db->set('paid', 'paid+'.$amount, FALSE);
        $this->db->set('status', $status);
        $this->db->where('id', $fechamento_item_id);
        $this->db->update('close_commission_items');

        //log da comissao paga
        $fechamento = $this->FechamentoComissaoRepository_model->getByID($fechamento_id);
        $fechamentoItem = $this->FechamentoComissaoRepository_model->getItemFechamentoByID($fechamento_item_id);
        $itemComissao = $this->CommissionsRepository_model->getComissionsItemByID($fechamentoItem->commission_item_id);

        $note = 'Comissão paga no fechamento ('.$fechamento->reference_no.') '.$fechamento->title.' - '.$this->sma->hrsd($fechamento->dt_competencia);
        $this->CommissionEventService_model->addEvent($itemComissao->commission_id, $itemComissao->id, $note, CommissionEventService_model::COMISSAO_PAGA, $this->session->userdata('user_id'));
    }

    private function baixarAgendamentoOnline($contareceberId) {
        //$this->AgendamentoServico_model->pagar($contareceberId);
    }

    function getAgendamentoByContaReceber($contaReceber){
        $this->db->where('contaReceber',$contaReceber);
        $this->db->limit(1);
        return $this->db->get('agendamento')->row();
    }

    private function atualizarMovimentadorSaldoNoPeriodo($data) {

        $fatura = $this->financeiro_model->getFaturaById($data['fatura']);

        $valorPagamento = $data['amount'];
        $dataPagamento = $data['date'];
        $formaPagamento = $data['formapagamento'];
        $movimentador = $data['movimentador'];

        $bloqueado = 0;

        $movimentadorSaldoPeriodo = $this->getMovimentadorSaldoPeriodo($formaPagamento, $movimentador, $dataPagamento);

        //$saldoAnterior = $movimentadorSaldoPeriodo->saldo;
        $entradas = $movimentadorSaldoPeriodo->entradas;
        $saidas = $movimentadorSaldoPeriodo->saidas;

        if ($this->isCredito($fatura)) $entradas = $entradas + $valorPagamento;
        else $saidas = $saidas + $valorPagamento;

        $saldo = ($entradas - $saidas);
        $diferena = $saldo - $bloqueado;

        $dadosAtualizadosMovimentadorSaldo = array (
            'entradas' => $entradas,
            'saidas' => $saidas,
            'saldo' => $saldo,
            'bloqueado' => $bloqueado,
            'diferenca' => $diferena,
            'formapagamento' => $formaPagamento,
            'movimentador' => $movimentador,
        );

        $this->editar('movimentador_saldo_periodo',$dadosAtualizadosMovimentadorSaldo,'id', $movimentadorSaldoPeriodo->id);
    }

    private function isCredito($fatura) {
        return $fatura->tipooperacao == Financeiro_model::TIPO_OPERACAO_CREDITO;
    }

    public function getCategoryByCId($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    private function getMovimentadorSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo) {

        $periodo = date('Y-m-d', strtotime($periodo));

        $this->db->where('formapagamento',$formaPagamentoId);
        $this->db->where('movimentador',$movimentadorId);
        $this->db->where('dtmovimento',$periodo);
        $this->db->limit(1);
        $movimentadoSaldo = $this->db->get('movimentador_saldo_periodo')->row();

        if (count($movimentadoSaldo) > 0) return $movimentadoSaldo;
        else $this->adicionarMovimentadoSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo);

        return $this->getMovimentadorSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo);
    }

    function adicionarMovimentadoSaldoPeriodo($formaPagamentoId, $movimentadorId, $periodo) {

        $dadosNovoMovimentadorSaldo = array (
            'entradas' => 0,
            'saidas' => 0,
            'saldo' => 0,
            'bloqueado' => 0,
            'diferenca' => 0,
            'formapagamento' => $formaPagamentoId,
            'movimentador' => $movimentadorId,
            'dtmovimento' => $periodo
        );

        $this->db->insert('movimentador_saldo_periodo', $dadosNovoMovimentadorSaldo);

        if ($this->db->affected_rows() == '1') return $this->db->insert_id('movimentador_saldo_periodo');

        return FALSE;
    }

    function editar($table,$data,$fieldID,$ID){

        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);

        if ($this->db->affected_rows() >= 0) return TRUE;
        return FALSE;
    }

    public function addPaymentMesclar($data = array(), $payment_status = null, $customer_id = null)
    {
		if ($payment_status  != 'paid') {
			if ($this->db->insert('payments', $data)) {
				if ($this->site->getReference('pay') == $data['reference_no']) {
					$this->site->updateReference('pay');
				}
				$this->site->syncSalePayments($data['sale_id']);
				if ($data['paid_by'] == 'gift_card') {
					$gc = $this->site->getGiftCardByNO($data['cc_no']);
					$this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
				} elseif ($customer_id && $data['paid_by'] == 'deposit') {
					$customer = $this->site->getCompanyByID($customer_id);
					$this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
				}
				return true;
			}
			return true;
		}
        return true;
    }

    public function updatePayment($id, $data = array(), $customer_id = null)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncSalePayments($data['sale_id']);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                if (!$customer_id) {
                    $sale = $this->getInvoiceByID($opay->sale_id);
                    $customer_id = $sale->customer_id;
                }
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncSalePayments($opay->sale_id);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
            } elseif ($opay->paid_by == 'deposit') {
                $sale = $this->getInvoiceByID($opay->sale_id);
                $customer = $this->site->getCompanyByID($sale->customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get_where('paypal', array('id' => 1));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get_where('skrill', array('id' => 1));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllEvents($sale_id)
    {
        $this->db->order_by('id');
        $q = $this->db->get_where('sale_events', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStaff()
    {
        if (!$this->Owner) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleByPoltrona($poltrona, $variacao)
    {
    	$q = $this->db->get_where('sales', array('reference_no' => $poltrona , 'reference_no_variacao' => $variacao ), 1);
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return FALSE;
    }


    public function getSaleItensByPoltrona($product_id, $poltrona, $variacao) {
    	$this->db->select('sales.*, sale_items.*, products.* , companies.* ')
    	->join('products', 'products.id=sale_items.product_id', 'left')
    	->join('sales', 'sales.id=sale_items.sale_id', 'left')
    	->join('companies', 'sales.customer_id=companies.id', 'left');
    	$q = $this->db->get_where('sale_items', array('sale_items.product_id' => $product_id, 'sales.reference_no' => $poltrona, 'sales.reference_no_variacao' => $variacao), 1);

    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
    	return FALSE;
    }


    public function getSaleByItensByPoltrona($product_id, $poltrona, $variacao) {
        $this->db->select('sales.*, sale_items.*, products.* , companies.* ')
            ->join('products', 'products.id=sale_items.product_id', 'left')
            ->join('sales', 'sales.id=sale_items.sale_id', 'left')
            ->join('companies', 'sales.customer_id=companies.id', 'left');
        $q = $this->db->get_where('sale_items', array(
            'sale_items.product_id' => $product_id,
            'sale_items.poltronaClient' => $poltrona,
            'sales.reference_no_variacao' => $variacao), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateCostingLine($sale_item_id, $product_id, $quantity)
    {
        if ($costings = $this->getCostingLines($sale_item_id, $product_id)) {
            foreach ($costings as $cost) {
                if ($cost->quantity >= $quantity) {
                    $qty = $cost->quantity - $quantity;
                    $bln = $cost->quantity_balance && $cost->quantity_balance >= $quantity ? $cost->quantity_balance - $quantity : 0;
                    $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $cost->id));
                    $quantity = 0;
                } elseif ($cost->quantity < $quantity) {
                    $qty = $quantity - $cost->quantity;
                    $this->db->delete('costing', array('id' => $cost->id));
                    $quantity = $qty;
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProductOptions_Onibus($pid,$idVariante = null)
    {
        if ($idVariante) $this->db->where("id", $idVariante);

    	$this->db->where("product_id = ".$pid." AND ( UPPER(name) like UPPER('%Onibus%') OR UPPER(name) like UPPER('%Transporte%') ) ");
    	$q = $this->db->get('product_variants');

    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    	return FALSE;
    }

    public function buscar_Onibus_by_product($pid)
    {
        $this->db->select("product_variants.id, product_variants.name as text", FALSE);
        $this->db->where("product_id = ".$pid." AND  (  UPPER(name) like UPPER('%Onibus%') OR UPPER(name) like UPPER('%Transporte%')   ) ");
        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscar_Onibus($pid,$idVariante = null)
    {
        if ($idVariante) {
            $this->db->where("id", $idVariante);
        }

        $this->db->select("sma_product_variants.id, concat(sma_product_variants.name,' - ', sma_companies.name )  as text", FALSE);
        $this->db->where("product_id = ".$pid." AND ( UPPER(sma_product_variants.name) like UPPER('%Onibus%')  OR UPPER(sma_product_variants.name) like UPPER('%Transporte%') ) ");
        $this->db->join('companies', 'companies.id=product_variants.fornecedor', 'left');
        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function buscar_OnibusById( $idVariante = null)
    {
        $this->db->select("sma_product_variants.id, concat(sma_product_variants.name,' - ', sma_companies.name )  as text", FALSE);
        $this->db->join('companies', 'companies.id=product_variants.fornecedor', 'left');
        $this->db->where("sma_product_variants.id", $idVariante);
        $q = $this->db->get('product_variants');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantsComissao()
    {
        $this->db->where("name",'Comissão');
        $q = $this->db->get('variants');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptions_Hotel($pid,$idVariante = null)
    {

        if ($idVariante) {
            $this->db->where("id", $idVariante);
        }

    	$this->db->where("product_id = ".$pid." 
    	    AND ( UPPER(name) like UPPER('%Hotel%')  OR 
    	          UPPER(name) like UPPER('%Hostel%') OR 
    	          UPPER(name) like UPPER('%Pousada%') OR 
    	          UPPER(name) like UPPER('%Camping%')   )");

    	$q = $this->db->get('product_variants');

    	if ($q->num_rows() > 0) {
    		foreach (($q->result()) as $row) {
    			$data[] = $row;
    		}
    		return $data;
    	}
    	return FALSE;
    }

    public function buscarComissao($product_details) {

        $comissao = $this->getVariantsComissao();
        $categorias = $product_details->categorias;

        if ($categorias == 'bate_volta') {
            return $comissao->comissaoBateVolta;
        } else if ($categorias == 'hospedagem') {
            return $comissao->comissaoHospedagem;
        } else if ($categorias == 'aereo') {
            return $comissao->comissaoAereo;
        } else {
            return 0;
        }
    }

    public function lancarComissaoDoVendedor2($sale_id, $sale_item_id, $venda, $item) {

        $status = 'pending';
        $item_quantity = 1;

        $vendedor = $venda['biller_id'];
        $totalDaPassagem = $item['subtotal'];
        $product_id = $item['product_code'];
        $warehouse_id = $item['warehouse_id'];
        $clienteId = $item['customerClient'];
        $cliente = $this->site->getCompanyByID($clienteId);

        $product_details    = $this->getProductByCode($product_id);
        $comissao = $this->getProductOptionsComissao($product_details->id,  $warehouse_id , true);
        $percentualComissao = $this->buscarComissao($product_details);

        $totalComissao = $totalDaPassagem*($percentualComissao/100);

        unset($products);
        unset($data);

        $reference = $this->site->getReference('po');
        $date = date('Y-m-d H:i:s');

        if ($vendedor > 0) {
            $supplier_details = $this->site->getCompanyByID($vendedor);
            $nomeVendedor = $supplier_details->name . ' (Comissão de Venda)';
        }

        $products[] = array(
            'source_of_sale' => $sale_id,
            'sale_item_id' => $sale_item_id,
            'product_code' => $product_id,
            'customerClient' => $cliente->id,
            'customerClientName' => $cliente->name,
            'product_name' => $cliente->name,
            'product_id' => $product_details->id,
            'option_id' => $comissao->id,
            'quantity' => $item_quantity,
            'quantity_balance' => $item_quantity,
            'warehouse_id' => $warehouse_id,
            'status' => $status,
            'real_unit_cost' => $totalComissao,
            'net_unit_cost' => $totalComissao,
            'unit_cost' => $this->sma->formatDecimal($totalComissao),
            'subtotal' => $this->sma->formatDecimal($totalComissao),
            'date' => date('Y-m-d', strtotime($date)),
        );

        if (empty($products)) $this->form_validation->set_rules('product', lang("order_items"), 'required');
        else krsort($products);

        $data = array(
            'reference_no' => $reference,
            'date' => $date,
            'supplier_id' => $vendedor,
            'supplier' => $nomeVendedor,
            'warehouse_id' => $warehouse_id,
            'status' => $status,
            'grand_total' => $totalComissao,
            'total' => $this->sma->formatDecimal($totalComissao),
            'created_by' => $this->session->userdata('user_id'),
        );

        $purchases = $this->db->get_where('purchases', array('supplier_id' => $vendedor, 'warehouse_id' => $warehouse_id), 1);

        if ($purchases->num_rows() > 0) $this->updatePurchase($purchases->row()->id, $data, $products);
        else $this->addPurchase($data, $products);
    }

	public function addPurchase($data, $items)
    {

        if ($this->db->insert('purchases', $data)) {

            $purchase_id = $this->db->insert_id();

            if ($this->site->getReference('po') == $data['reference_no']) $this->site->updateReference('po');

            foreach ($items as $item) {

                $item['purchase_id'] = $purchase_id;

                $this->db->insert('purchase_items', $item);
                $this->db->update('products', array('cost' => $item['real_unit_cost']), array('id' => $item['product_id']));

                if($item['option_id']) {
                    $this->db->update('product_variants', array('cost' => $item['real_unit_cost']), array('id' => $item['option_id'], 'product_id' => $item['product_id']));
                }

                if ($data['status'] == 'received' || $data['status'] == 'returned') {
                    $this->site->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['real_unit_cost']));
                }
            }

            if ($data['status'] == 'returned') $this->db->update('purchases', array('return_purchase_ref' => $data['return_purchase_ref'], 'surcharge' => $data['surcharge'],'return_purchase_total' => $data['grand_total'], 'return_id' => $purchase_id), array('id' => $data['purchase_id']));
            if ($data['status'] == 'received' || $data['status'] == 'returned') $this->site->syncQuantity(NULL, $purchase_id);

            return true;
        }
        return false;
    }

    public function updatePurchase($id, $data, $items = array())
    {
        $oitems = $this->getAllPurchaseItems($id);
        $total  = $data['total'];

        foreach ($oitems as $item) {
            $net_unit_cost = $item->net_unit_cost;
            $item_quantity = $item->quantity;
            $total = $total + ($net_unit_cost*$item_quantity);
        }

        $data['total']          = $total;
        $data['grand_total']    = $total;

        if ($this->db->update('purchases', $data, array('id' => $id))) {
             foreach ($items as $item) {

                $item['purchase_id'] = $id;
                $this->db->insert('purchase_items', $item);

                if ($data['status'] == 'received' || $data['status'] == 'partial') {
                    $this->site->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['real_unit_cost']));
                }

            }

            $this->site->syncQuantity(NULL, NULL, $oitems);

            if ($data['status'] == 'received' || $data['status'] == 'partial') {
                $this->site->syncQuantity(NULL, $id);
                foreach ($oitems as $oitem) {
                    $this->site->updateAVCO(array('product_id' => $oitem->product_id, 'warehouse_id' => $oitem->warehouse_id, 'quantity' => (0-$oitem->quantity), 'cost' => $oitem->real_unit_cost));
                }
            }

            $this->site->syncPurchasePayments($id);
            return true;
        }

        return false;
    }


    public function getPurchaseByID($id)
    {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllPurchaseItems($purchase_id)
    {
        $this->db->select('purchase_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.unit, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=purchase_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=purchase_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
            ->group_by('purchase_items.id')
            ->order_by('id', 'asc');
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function atualizarVendaHotel($sale_id, $data) {
        $this->db->update('sales', $data, array('id' => $sale_id));
    }
    public  function atualizarLocalEmbarque($itemId, $localEmbarque) {
        $this->db->update('sale_items', array('localEmbarque' => $localEmbarque), array('id' => $itemId));
    }
    public  function atualizarTipoTransporte($itemId, $tipoTransporteId) {

        $tipoTransporte = $this->getTipoTransporte($tipoTransporteId);

        $this->db->update('sale_items', array('tipoTransporte' => $tipoTransporteId, 'nmTipoTransporte' => $tipoTransporte->name), array('id' => $itemId));
    }
    function getTipoTransporte($id) {
        $this->db->limit(1);
        $this->db->where("id", $id);
        $q = $this->db->get('tipo_transporte_rodoviario');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function atualizarHotel($sale_id,$ordem) {

        $data = array(
            'tipo_quarto'   => NULL,
            'hotel'         => NULL,
            'customer_1_id' => NULL,
            'customer_1'    => NULL,

            'hotel2'         => NULL,
            'customer_2_id' => NULL,
            'customer_2'    => NULL,

            'hotel3'         => NULL,
            'customer_3_id' => NULL,
            'customer_3'    => NULL,

            'hotel4'         => NULL,
            'customer_4_id' => NULL,
            'customer_4'    => NULL,

            'hotel5'         => NULL,
            'customer_5_id' => NULL,
            'customer_5'    => NULL,
        );

        $this->db->update('sales', $data, array('id' => $sale_id));
    }
    public function getCompanyByID($id) {
        $q = $this->db->get_where('companies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getWarehouseById($id){
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function buscarValcherEmFormaDeImagem($vendaId, $vendedorId) {

        $url = 'http://api.resultaweb.com.br/resultaservice/v1/resultaservice-gii';
        //$url = 'http://localhost:9070/resultaservice/v1/resultaservice-gii';

        $itens = $this->getAllInvoiceItemsByCustomer($vendaId, $vendedorId);

        foreach ($itens as $item) {

            $venda = $this->sales_model->getInvoiceByID($vendaId);
            $comprador = $this->getCompanyByID($vendedorId);

            $warehouse = $this->getWarehouseById($venda->warehouse_id);
            $viagem = $this->getProductByCode($warehouse->code);

            $rg = $comprador->cf1;
            $cpf = $comprador->vat_no;
            $hora_saida = $viagem->hora_saida;
            $hora_retorno = $viagem->hora_retorno;
            $data_saida = $viagem->data_saida;
            $data_retorno = $viagem->data_retorno;

            if ($hora_saida) $hora_saida = date("H:i",strtotime ($hora_saida));
            if ($hora_retorno) $hora_retorno = date("H:i", strtotime($hora_retorno));

            setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
            date_default_timezone_set('America/Sao_Paulo');

            if ($data_saida) $data_saida = strftime('%d de %B de %Y', strtotime($data_saida));
            if ($data_retorno) $data_retorno = strftime('%d de %B de %Y', strtotime($data_retorno));

            $nomeComprador = substr($comprador->name, 0, 29);

            $payload =
                array(
                    "arquivo" => "valcher-compatrips",
                    "extensao" => 'jpg',
                    "script" => array(
                        array(
                            "posX" => 44,
                            "posY" => 196,
                            "tamanhoFonte" => 25,
                            'texto' => $nomeComprador
                        ),
                        array(
                            "posX" => 392,
                            "posY" => 1075,
                            "tamanhoFonte" => 25,
                            'texto' => $nomeComprador
                        ),
                        array(
                            "posX" => 566,
                            "posY" => 200,
                            "tamanhoFonte" => 30,
                            'texto' => $rg
                        ),
                        array(
                            "posX" => 356,
                            "posY" => 1113,
                            "tamanhoFonte" => 25,
                            'texto' => $rg
                        ),
                        array(
                            "posX" => 230,
                            "posY" => 420,
                            "tamanhoFonte" => 30,
                            'texto' => $item->poltronaClient
                        ),
                        array(
                            "posX" => 375,
                            "posY" => 420,
                            "tamanhoFonte" => 30,
                            'texto' => $hora_saida
                        ),
                        array(
                            "posX" => 480,
                            "posY" => 420,
                            "tamanhoFonte" => 30,
                            'texto' => $venda->local_saida
                        ),
                        array(
                            "posX" => 187,
                            "posY" => 270,
                            "tamanhoFonte" => 21,
                            'texto' => $hora_saida
                        ),
                        array(
                            "posX" => 665,
                            "posY" => 270,
                            "tamanhoFonte" => 21,
                            'texto' => $hora_retorno
                        ),
                        array(
                            "posX" => 50,
                            "posY" => 300,
                            "tamanhoFonte" => 14,
                            'texto' => $data_saida
                        ),
                        array(
                            "posX" => 665,
                            "posY" => 300,
                            "tamanhoFonte" => 14,
                            'texto' => $data_retorno
                        ),
                        array(
                            "posX" => 366,
                            "posY" => 1150,
                            "tamanhoFonte" => 25,
                            'texto' => $cpf
                        ),
                    )
                );

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_POST, TRUE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
            curl_setopt($ch, CURLOPT_URL, $url);

            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                )
            );

            $response = curl_exec($ch);
            curl_close($ch);

            header("Content-type: image/jpeg");
            header("Content-Disposition:inline;filename=".$comprador->name.".png");

            return $response;
        }
    }


    public function verificaDuplicidadeClienteVenda($customerID, $programacaoID) {

        $this->db->select('sale_items.id as id');

        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');
        $this->db->where("sales.sale_status in ('faturada', 'orcamento') ");

        $this->db->where('adicional', false);

        $q = $this->db->get_where('sale_items', array('sale_items.customerClient' => $customerID, 'programacaoId' => $programacaoID), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function verificaClienteVenda($cpf, $programacaoId) {
        $this->db->select('sales.id as sale_id, sale_status');
        $this->db->join('sales', 'sales.id=sale_items.sale_id', 'left');
        $this->db->join('companies', 'companies.id = sale_items.customerClient');
        $this->db->where("sales.sale_status in ('faturada', 'orcamento') ");

        $q = $this->db->get_where('sale_items', array('companies.vat_no' => $cpf, 'programacaoId' => $programacaoId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function imprimir_recibo_pagamento($pagamentoId) {

        $url = 'http://api.resultaweb.com.br/resultaservice/v1/resultaservice-gip';

        $pagamento = $this->getPaymentByID($pagamentoId);
        $sale_id = $pagamento->sale_id;

        $venda = $this->getInvoiceByID($sale_id);
        $itens = $this->getAllInvoiceItems($sale_id);
        $comprador = $this->getCompanyByID($venda->customer_id);
        $warehouse = $this->getWarehouseById($venda->warehouse_id);
        $viagem = $this->getProductByCode($warehouse->code);

        $dataPagamento = $pagamento->date;
        $note = $pagamento->note;
        $valorPagamento = $pagamento->amount;
        $nomeComprador = $comprador->name;
        $quantidadePassagens = 0;

        $nomeViagem = substr($viagem->name, 0, 23);
        $nomeComprador = substr($nomeComprador, 0, 30);

        foreach ($itens as $item) $quantidadePassagens = $quantidadePassagens + $item->quantity;

        $payload =
            array(
                "arquivo"=> "recibo-glatur",
                "extensao" => 'png',
                "script" => array(
                    array(
                        "posX" => 95,
                        "posY" => 85,
                        "tamanhoFonte" => 14,
                        'texto' => $nomeComprador
                    ),
                    array(
                        "posX" => 405,
                        "posY" => 85,
                        "tamanhoFonte" => 14,
                        'texto' =>  $comprador->cf1
                    ),
                    array(
                        "posX" => 90,
                        "posY" => 127,
                        "tamanhoFonte" => 14,
                        'texto' => date("d/m/Y", strtotime($dataPagamento))
                    ),
                    array(
                        "posX" => 355,
                        "posY" => 104,
                        "tamanhoFonte" => 14,
                        'texto' =>  number_format($valorPagamento , 2, ',', ' ')
                    ),
                    array(
                        "posX" => 123,
                        "posY" => 155,
                        "tamanhoFonte" => 14,
                        'texto' => strip_tags($note)
                    ),
                    array(
                        "posX" => 80,
                        "posY" => 197,
                        "tamanhoFonte" => 14,
                        'texto' => $nomeViagem
                    ),
                    array(
                        "posX" => 340,
                        "posY" => 197,
                        "tamanhoFonte" => 14,
                        'texto' => date("d/m/Y", strtotime($viagem->data_saida))
                    ),
                    array(
                        "posX" => 640,
                        "posY" => 197,
                        "tamanhoFonte" => 14,
                        'texto' => $quantidadePassagens
                    )
                )
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
            )
        );

        $response = curl_exec($ch);
        curl_close($ch);

        header("Content-type:application/pdf");
        header("Content-Disposition:inline;filename='$viagem->name");

        return $response;
    }

    public function getAllCustomerByProgramacao($progrmacaoId) {

        $this->db->select("companies.*");
        $this->db->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->db->join('companies', 'companies.id = sale_items.customerClient');
        $this->db->where('sale_items.programacaoId', $progrmacaoId);
        $this->db->where('sma_sales.sale_status != "cancel"');
        $this->db->group_by('companies.id');

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('sma_sales');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $sales[] = $row;
            }
        }

        return $sales;
    }

    public function getAllSalesByProgramacao($progrmacaoId) {

        $this->db->select("sma_sales.id as id");
        $this->db->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->db->where('sale_items.programacaoId', $progrmacaoId);
        $this->db->where('sma_sales.sale_status != "cancel"');
        $this->db->group_by('sales.id');

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->db->where('created_by', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('sma_sales');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $sales[] = $row;
            }
        }

        return $sales;
    }

}
