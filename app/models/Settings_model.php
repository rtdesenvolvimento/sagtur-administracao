<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function updateLogo($photo)
    {
        $logo = array('logo' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function updateLoginLogo($photo)
    {
        $logo = array('logo2' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function getSettings()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormats()
    {
        $q = $this->db->get('date_format');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateSetting($data)
    {
        $this->db->where('setting_id', '1');
        if ($this->db->update('settings', $data)) {
            return true;
        }
        return false;
    }

    public function updateShopSetting($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('shop_settings', $data)) {
            return true;
        }
        return false;
    }

    public function updateCommissionSetting($data)
    {

        $this->db->where('id', '1');
        if ($this->db->update('commission_settings', $data)) {
            return true;
        }
        return false;
    }

    public function addTaxRate($data)
    {
        if ($this->db->insert('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function updateTaxRate($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcela = NULL, $produtoID = NULL)
    {
        if ($produtoID) {
            return $this->getAllCondicoesPagamentoByTipoCobrancaProduto($tipoCobrancaId, $maximoParcela, $produtoID);
        } else {$this->db->select('taxas_venda_configuracao.*, tipo_cobranca.tipo as tipoCobranca, condicao_pagamento.name as condicaoPagamento, condicao_pagamento.parcelas')
            ->join('condicao_pagamento', 'condicao_pagamento.id=taxas_venda_configuracao.condicaoPagamentoId', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id=taxas_venda_configuracao.tipoCobrancaId', 'left');

            $this->db->where('ativo', 1);

            if ($maximoParcela != null) {
                $this->db->where('condicao_pagamento.parcelas <='.$maximoParcela);
            }

            $q = $this->db->get_where('taxas_venda_configuracao', array('tipoCobrancaId' => $tipoCobrancaId) );
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
            return FALSE;
        }
    }

    private function getAllCondicoesPagamentoByTipoCobrancaProduto($tipoCobrancaId, $maximoParcela, $produto)
    {
        $this->db->select('
        tipo_cobranca_produto.taxaIntermediacao,
        tipo_cobranca_produto.taxaFixaIntermediacao,

        tipo_cobranca_condicao_produto.condicaoPagamentoId,
        tipo_cobranca_condicao_produto.tipoCobrancaId,

        tipo_cobranca_condicao_produto.valor,
        tipo_cobranca_condicao_produto.acrescimoDescontoTipo,
        tipo_cobranca_condicao_produto.tipo,

        tipo_cobranca.tipo as tipoCobranca, 
        condicao_pagamento.name as condicaoPagamento, 
        condicao_pagamento.parcelas')
            ->join('condicao_pagamento', 'condicao_pagamento.id=tipo_cobranca_condicao_produto.condicaoPagamentoId', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id=tipo_cobranca_condicao_produto.tipoCobrancaId', 'left')
            ->join('tipo_cobranca_produto', 'tipo_cobranca_produto.tipoCobrancaId=tipo_cobranca_condicao_produto.tipoCobrancaId and tipo_cobranca_produto.produtoId = tipo_cobranca_condicao_produto.produtoId', 'left');

        $this->db->where('tipo_cobranca_condicao_produto.ativo', 1);

        if ($maximoParcela != null) {
            $this->db->where('condicao_pagamento.parcelas <='.$maximoParcela);
        }

        $q = $this->db->get_where('tipo_cobranca_condicao_produto',
            array(
                'tipo_cobranca_condicao_produto.tipoCobrancaId' => $tipoCobrancaId,
                'tipo_cobranca_condicao_produto.produtoId' => $produto)
        );
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxaCobranca($id, $tipoCobrancaId)
    {
        $q = $this->db->get_where('taxas_venda_configuracao', array('taxas_venda_id' => $id, 'tipoCobrancaId' => $tipoCobrancaId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addWarehouse($data)
    {
        if ($this->db->insert('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function updateWarehouse($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function getAllCondicoesPagamentoExibirLink()
    {

        $this->db->where('status', 'Ativo');
        $this->db->where('exibirLinkCompras', '1');

        $this->db->order_by('id');
        $q = $this->db->get('condicao_pagamento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllTiposCobranca($ocultar_interna = false)
    {
        $this->db->where('status', 'Ativo');
        $this->db->where('exibirLinkCompras', 1);

        if ($ocultar_interna) {
            $this->db->where("credito_cliente", 0);
            $this->db->where("reembolso", 0);
        }

        $this->db->order_by('name');

        $q = $this->db->get('tipo_cobranca');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllWarehouses()
    {
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteTaxRate($id)
    {
        if ($this->db->delete('tax_rates', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteInvoiceType($id)
    {
        if ($this->db->delete('invoice_types', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteWarehouse($id)
    {
        if ($this->db->delete('warehouses', array('id' => $id)) && $this->db->delete('warehouses_products', array('warehouse_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addCustomerGroup($data)
    {
        if ($this->db->insert('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function updateTaxasVenda($id, $tipoCobrancaId, $data, $configuracaoTaxas) {

        $this->db->where('id', $id);

        if ($this->db->update('sma_taxas_venda', $data)) {

            if (!empty($configuracaoTaxas)) {
                $this->db->delete('taxas_venda_configuracao', array('taxas_venda_id' => $id, 'tipoCobrancaId' => $tipoCobrancaId));
                foreach ($configuracaoTaxas as $taxa) {
                    $taxa['taxas_venda_id'] = $id;
                    $this->db->insert('taxas_venda_configuracao', $taxa);
                }
            }
        }

        return true;
    }

    public function updateCustomerGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('customer_groups', $data)) {
            return true;
        }
        return false;
    }
    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllPlanosSagtur()
    {
        $q = $this->db->get('plano_sagtur');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteCustomerGroup($id)
    {
        if ($this->db->delete('customer_groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getGroups()
    {
        $this->db->where('id >', 4);
        $q = $this->db->get('groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getGroupByID($id)
    {
        $q = $this->db->get_where('groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function GroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function updatePermissions($id, $data = array())
    {
        if ($this->db->update('permissions', $data, array('group_id' => $id)) && $this->db->update('users', array('show_price' => $data['products-price'], 'show_cost' => $data['products-cost']), array('group_id' => $id))) {
            return true;
        }
        return false;
    }

    public function addGroup($data)
    {
        if ($this->db->insert("groups", $data)) {
            $gid = $this->db->insert_id();
            $this->db->insert('permissions', array('group_id' => $gid));
            return $gid;
        }
        return false;
    }

    public function updateGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("groups", $data)) {
            return true;
        }
        return false;
    }


    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByID($id)
    {
        $q = $this->db->get_where('currencies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCurrency($data)
    {
        if ($this->db->insert("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function updateCurrency($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function deleteCurrency($id)
    {
        if ($this->db->delete("currencies", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getAllCategories()
    {
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllSubCategories()
    {
        $q = $this->db->get("subcategories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubcategoryDetails($id)
    {
        $this->db->select("subcategories.code as code, subcategories.name as name, categories.name as parent")
            ->join('categories', 'categories.id = subcategories.category_id', 'left')
            ->group_by('subcategories.id');
        $q = $this->db->get_where("subcategories", array('subcategories.id' => $id));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategoriesByCategoryID($category_id)
    {
        $q = $this->db->get_where("subcategories", array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id)
    {
        $q = $this->db->get_where("categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getIconByID($id)
    {
        $q = $this->db->get_where("images_icon", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllIcon()
    {
        $q = $this->db->get("images_icon");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategoryByID($id)
    {
        $q = $this->db->get_where("subcategories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategoryByCode($code)
    {
        $q = $this->db->get_where("subcategories", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCategory($name, $code, $icon, $photo)
    {
        if ($this->db->insert("categories", array('code' => $code, 'name' => $name, 'image_icon_id' => $icon, 'image' => $photo))) {
            return true;
        }
        return false;
    }

    public function addCategories($data)
    {
        if ($this->db->insert_batch('categories', $data)) {
            return true;
        }
        return false;
    }

    public function addSubCategory($category, $name, $code, $photo)
    {
        if ($this->db->insert("subcategories", array('category_id' => $category, 'code' => $code, 'name' => $name, 'image' => $photo))) {
            return true;
        }
        return false;
    }

    public function addSubCategories($data)
    {
        if ($this->db->insert_batch('subcategories', $data)) {
            return true;
        }
        return false;
    }

    public function updateCategory($id, $data = array(), $photo)
    {
        $categoryData = array('code' => $data['code'], 'name' => $data['name'],  'image_icon_id' => $data['image_icon_id']);
        if ($photo) {
            $categoryData['image'] = $photo;
        }
        $this->db->where('id', $id);
        if ($this->db->update("categories", $categoryData)) {
            return true;
        }
        return false;
    }

    public function updateSubCategory($id, $data = array(), $photo)
    {
        $categoryData = array(
            'category_id' => $data['category'],
            'code' => $data['code'],
            'name' => $data['name'],
        );
        if ($photo) {
            $categoryData['image'] = $photo;
        }
        $this->db->where('id', $id);
        if ($this->db->update("subcategories", $categoryData)) {
            return true;
        }
        return false;
    }

    public function deleteServicoIncluso($id)
    {
        if ($this->db->delete("servicos_inclusos", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteCategory($id)
    {
        if ($this->db->delete("categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteSubCategory($id)
    {
        if ($this->db->delete("subcategories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId = NULL, $produtoID = NULL)
    {
        if ($produtoID) {
            return $this->getTaxaConfiguracaoSettingsProducts($tipoCobrancaId, $produtoID);
        } else {
            if ($condicaoPagamentoId != null) {
                $q = $this->db->get_where("taxas_venda_configuracao", array('tipoCobrancaId' => $tipoCobrancaId, 'condicaoPagamentoId' => $condicaoPagamentoId), 1);
            } else {
                $q = $this->db->get_where("taxas_venda_configuracao", array('tipoCobrancaId' => $tipoCobrancaId), 1);
            }

            if ($q->num_rows() > 0) {
                return $q->row();
            }

            return FALSE;
        }
    }

    private function getTaxaConfiguracaoSettingsProducts($tipoCobrancaId, $produtoID) {

        $q = $this->db->get_where("tipo_cobranca_produto", array('tipoCobrancaId' => $tipoCobrancaId, 'produtoId' => $produtoID), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get('paypal');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPagSeguroSettings()
    {
        $q = $this->db->get('pagseguro');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getJunoSettings()
    {
        $q = $this->db->get('juno');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCobreFacilSettings()
    {
        $q = $this->db->get('cobre_facil');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getMercadoPagoSettings()
    {
        $q = $this->db->get('mercado_pago');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAsaasSettings()
    {
        $q = $this->db->get('asaas');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPlugsignSettings()
    {
        $q = $this->db->get('plugsign');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getValepaySettings()
    {
        $q = $this->db->get('vale_pay');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getTaxaSettings()
    {
        $q = $this->db->get('taxas_venda');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getHorariosComercial()
    {
        $q = $this->db->get('horario_atendimento');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateHorariosComercial($data)
    {
        $isExisteConfiguracaoHorario = $this->getHorariosComercial();

        if (!$isExisteConfiguracaoHorario) ($this->db->insert('horario_atendimento', $data));

        $this->db->where('id', '1');
        if ($this->db->update('horario_atendimento', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updatePaypal($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('paypal', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updatePagSeguro($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('pagseguro', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updateJuno($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('juno', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updateMercadoPago($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('mercado_pago', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updateValepay($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('vale_pay', $data)) {
            return true;
        }
        return FALSE;
    }

    public function updateAsaas($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('asaas', $data)) {
            return true;
        }
        return FALSE;
    }
    public function getSkrillSettings()
    {
        $q = $this->db->get('skrill');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSkrill($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('skrill', $data)) {
            return true;
        }
        return FALSE;
    }

    public function checkGroupUsers($id)
    {
        $q = $this->db->get_where("users", array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteGroup($id)
    {
        if ($this->db->delete('groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addVariant($data)
    {
        if ($this->db->insert('variants', $data)) {
            return true;
        }
        return false;
    }

    public function updateVariant($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('variants', $data)) {
            return true;
        }
        return false;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllReceitasSuperiores()
    {
        $this->db->where('receitasuperior', '');
        $q = $this->db->get('receita');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllDespesasSuperior()
    {
        $this->db->where('despesasuperior', '');
        $q = $this->db->get('despesa');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllDREReceitas()
    {
        $this->db->where('(tipo = "(+) Receitas" OR tipo = "(+/-) Ambas" )');
        $q = $this->db->get('dre');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantByID($id)
    {
        $q = $this->db->get_where('variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteVariant($id)
    {
        if ($this->db->delete('variants', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getExpenseCategoryByID($id)
    {
        $q = $this->db->get_where("expense_categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReceitaById($id)
    {
        $q = $this->db->get_where("receita", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getLocalEmbarqueById($id)
    {
        $q = $this->db->get_where("local_embarque", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getServicoInclusoByID($id)
    {
        $q = $this->db->get_where("servicos_inclusos", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getMeioDivulgacaoById($id)
    {
        $q = $this->db->get_where("meio_divulgacao", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getConfiguracaoExtraAssento($id)
    {
        $q = $this->db->get_where("configuracao_cobranca_extra_assento", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTipoTransporteById($id)
    {
        $q = $this->db->get_where("tipo_transporte_rodoviario", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getValorFaixa($id)
    {
        $q = $this->db->get_where("valor_faixa", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTipoHospedagemById($id)
    {
        $q = $this->db->get_where("tipo_hospedagem", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getFormaPagamentoById($id)
    {
        $q = $this->db->get_where("forma_pagamento", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDespesaById($id)
    {
        $q = $this->db->get_where("despesa", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDREById($id)
    {
        $q = $this->db->get_where("dre", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTipoCobrancaId($id)
    {
        $q = $this->db->get_where("tipo_cobranca", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCondicaoPagamentoById($id)
    {
        $q = $this->db->get_where("condicao_pagamento", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTestimonial($id)
    {
        $q = $this->db->get_where("testimonial", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTeam($id)
    {
        $q = $this->db->get_where("team_site", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGallery($id)
    {
        $q = $this->db->get_where("gallery", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGalleryItens($garelly_id)
    {
        $q = $this->db->get_where("gallery_item", array('gallery_id' => $garelly_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getContaById($id)
    {
        $q = $this->db->get_where("pos_register", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCupomById($id)
    {
        $q = $this->db->get_where("cupom_desconto", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseCategoryByCode($code)
    {
        $q = $this->db->get_where("expense_categories", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function adicionarTipoCobranca($data)
    {
        if ($this->db->insert("tipo_cobranca", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarServicoIncluso($data)
    {
        if ($this->db->insert("servicos_inclusos", $data)) {
            return true;
        }
        return false;
    }


    public function adicionarConta($data)
    {
        if ($this->db->insert("pos_register", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarCupom($data)
    {
        if ($this->db->insert("cupom_desconto", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarReceita($data)
    {
        if ($this->db->insert("receita", $data)) {
            return true;
        }
        return false;
    }

    public function adicinarLocalEmbarque($data)
    {
        if ($this->db->insert("local_embarque", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarMeioDivulgacao($data)
    {
        if ($this->db->insert("meio_divulgacao", $data)) {
            return true;
        }
        return false;
    }
    public function adicionarTipoTransporte($data)
    {
        if ($this->db->insert("tipo_transporte_rodoviario", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarValorFaixa($data)
    {
        if ($this->db->insert("valor_faixa", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarTipoHospedagem($data)
    {
        if ($this->db->insert("tipo_hospedagem", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarCondicaoPagamento($data) {
        if ($this->db->insert("condicao_pagamento", $data)) {
            return true;
        }
        return false;
    }

    public function addTestimonial($data) {
        if ($this->db->insert("testimonial", $data)) {
            return true;
        }
        return false;
    }

    public function addTeam($data) {
        if ($this->db->insert("team_site", $data)) {
            return true;
        }
        return false;
    }

    public function addGallery($data) {
        if ($this->db->insert("gallery", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarFormaPagamento($data)
    {
        if ($this->db->insert("forma_pagamento", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarDespesa($data)
    {
        if ($this->db->insert("despesa", $data)) {
            return true;
        }
        return false;
    }

    public function adicionarDRE($data)
    {
        if ($this->db->insert("dre", $data)) {
            return true;
        }
        return false;
    }

    public function addExpenseCategory($data)
    {
        if ($this->db->insert("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function addExpenseCategories($data)
    {
        if ($this->db->insert_batch("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function updateExpenseCategory($id, $data = array())
    {
        if ($this->db->update("expense_categories", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateServicoIncluso($id, $data = array())
    {
        if ($this->db->update("servicos_inclusos", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateReceita($id, $data = array())
    {
        if ($this->db->update("receita", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateLocalEmbarque($id, $data = array())
    {
        if ($this->db->update("local_embarque", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateMeioDivulgacao($id, $data = array())
    {
        if ($this->db->update("meio_divulgacao", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateTipoTransporte($id, $data = array())
    {
        if ($this->db->update("tipo_transporte_rodoviario", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateValorFaixa($id, $data = array())
    {
        if ($this->db->update("valor_faixa", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateTipoHospedagem($id, $data = array())
    {
        if ($this->db->update("tipo_hospedagem", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateFormaPagamento($id, $data = array())
    {
        if ($this->db->update("forma_pagamento", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateDespesa($id, $data = array())
    {
        if ($this->db->update("despesa", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function edtiarDRE($id, $data = array())
    {
        if ($this->db->update("dre", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateTipoCobranca($id, $data = array())
    {
        if ($this->db->update("tipo_cobranca", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateCondicaoPagamento($id, $data = array())
    {
        if ($this->db->update("condicao_pagamento", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateTestimonial($id, $data = array())
    {
        if ($this->db->update("testimonial", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateTeam($id, $data = array())
    {
        if ($this->db->update("team_site", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateGallery($id, $data = array(), $photos = null)
    {
        if ($this->db->update("gallery", $data, array('id' => $id))) {
            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('gallery_item', array('gallery_id' => $id, 'photo' => $photo));
                }
            }

            return true;
        }
        return false;
    }

    public function updateConta($id, $data = array())
    {
        if ($this->db->update("pos_register", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function updateCupom($id, $data = array())
    {
        if ($this->db->update("cupom_desconto", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function hasExpenseCategoryRecord($id)
    {
        $this->db->where('category_id', $id);
        return $this->db->count_all_results('expenses');
    }

    public function deletarReceita($id)
    {
        if ($this->db->delete("receita", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarLocalEmbarque($id)
    {
        if ($this->db->delete("local_embarque", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function verificaIntegridadeLocalEmbarque($id)
    {
        $this->db->where('localEmbarque', $id);
        $verifica_sale_items = $this->db->count_all_results('sale_items');

        return $verifica_sale_items;
    }

    public function deletarMeioDivulgacao($id)
    {
        if ($this->db->delete("meio_divulgacao", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarTipoTransporte($id)
    {
        if ($this->db->delete("tipo_transporte_rodoviario", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarCupomDesconto($id)
    {
        if ($this->db->delete("cupom_desconto", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarTipoHospedagem($id)
    {
        if ($this->db->delete("tipo_hospedagem", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarFormaPagamento($id)
    {
        if ($this->db->delete("forma_pagamento", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarDespesa($id)
    {
        if ($this->db->delete("despesa", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarDRE($id)
    {
        if ($this->db->delete("dre", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarTipoCobranca($id)
    {
        if ($this->db->delete("tipo_cobranca", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarCondicaoPagamento($id)
    {
        if ($this->db->delete("condicao_pagamento", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarTestimonial($id)
    {
        if ($this->db->delete("testimonial", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarTeam($id)
    {
        if ($this->db->delete("team_site", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarGallery($id)
    {
        if ($this->db->delete("gallery", array('id' => $id)) || $this->db->delete("gallery_item", array('gallery_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarValorFaixa($id)
    {
        if ($this->db->delete("valor_faixa", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deletarConta($id)
    {
        if ($this->db->delete("pos_register", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function verificaIntegridadeReceita($id)
    {
        $this->db->where('receita', $id);
        return $this->db->count_all_results('conta_receber');
    }

    public function verificaIntegridadeDespesa($id)
    {
        $this->db->where('despesa', $id);
        return $this->db->count_all_results('conta_pagar');
    }

    public function verificaIntegridadeDRE($id)
    {
        $this->db->where('dre', $id);
        $integridadeReceita = $this->db->count_all_results('receita');

        $this->db->where('dre', $id);
        $integridadeDespesa = $this->db->count_all_results('despesa');

        return ($integridadeReceita || $integridadeDespesa);
    }

    public function verificaIntegridadeCondicaoPagamento($id)
    {
        $this->db->where('condicaopagamento', $id);
        $verificaContaPagarCondicaoPagamento = $this->db->count_all_results('conta_pagar');

        $this->db->where('condicaopagamento', $id);
        $verificaContaReceberCondicaoPagamento = $this->db->count_all_results('conta_receber');

        return ($verificaContaPagarCondicaoPagamento || $verificaContaReceberCondicaoPagamento);
    }

    public function verificaIntegridadeConta($id)
    {
        $this->db->where('movimentador', $id);
        return $this->db->count_all_results('payments');
    }


    public function verificaIntegridadeMeioDivulgacaoVenda($id) {
        $this->db->where('meioDivulgacao', $id);
        $verificaVendaMeioDivulgacao = $this->db->count_all_results('sales');

        return $verificaVendaMeioDivulgacao;

    }

    public function verificaIntegridadeTipoCobranca($id)
    {
        $this->db->where('tipocobranca', $id);
        $verificaContaPagarTipoCobranca = $this->db->count_all_results('conta_pagar');

        $this->db->where('tipocobranca', $id);
        $verificaContaReceberTipoCobranca = $this->db->count_all_results('conta_receber');

        return ($verificaContaPagarTipoCobranca || $verificaContaReceberTipoCobranca);
    }

    public function verificaIntegridadeFormaPagamento($id)
    {
        $this->db->where('formapagamento', $id);
        return $this->db->count_all_results('payments');
    }

    public function deleteExpenseCategory($id)
    {
        if ($this->db->delete("expense_categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getAllContas()
    {
        $this->db->where('status', 'open');
        $q = $this->db->get('pos_register');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCupons()
    {
        $q = $this->db->get('cupom_desconto');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllFormasPagamento()
    {
        $q = $this->db->get('forma_pagamento');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function adicionarPrecoAssentoExtra($data, $assentos = array())
    {
        if ($this->db->insert("configuracao_cobranca_extra_assento", $data)) {
            $extra_id = $this->db->insert_id();

            foreach ($assentos as $assento) {
                $assento['configuracao_extra_id'] = $extra_id;
                $this->db->insert("configuracao_cobranca_extra_assento_marcacao", $assento);
            }
            return true;
        }
        return false;
    }


    public function editarPrecoAssentoExtra($id, $data, $assentos = array())
    {
        $this->db->delete('configuracao_cobranca_extra_assento_marcacao', array('configuracao_extra_id' => $id));

        if ($this->db->update("configuracao_cobranca_extra_assento", $data, array('id' => $id))) {
            foreach ($assentos as $assento) {
                $assento['configuracao_extra_id'] = $id;
                $this->db->insert("configuracao_cobranca_extra_assento_marcacao", $assento);
            }
            return true;
        }
        return false;
    }

    public function getAssentosCobrancaExtra($configuracao_id, $andar = 1)
    {

        $this->db->select('configuracao_cobranca_extra_assento_marcacao.*, cor.cor');
        $this->db->join('cor', 'cor.id=configuracao_cobranca_extra_assento_marcacao.cor_id', 'left');

        $this->db->where('andar', $andar);
        $this->db->where('configuracao_extra_id', $configuracao_id);

        $q = $this->db->get('configuracao_cobranca_extra_assento_marcacao');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function deletarCobrancaExtra($configuracao_id) {
        $this->db->delete('configuracao_cobranca_extra_assento_marcacao', array('configuracao_extra_id' => $configuracao_id));
        $this->db->delete('configuracao_cobranca_extra_assento', array('id' => $configuracao_id));

        return true;
    }
}
