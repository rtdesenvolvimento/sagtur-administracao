<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Veiculo_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getTipoTransporte($id) {
        $q = $this->db->get_where('tipo_transporte_rodoviario', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSuggestions($term, $limit = 10)
    {
        $this->db->select("id,  name as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('veiculo', [], $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function getSuggestionsTipoTransporte($term, $limit = 10)
    {
        $this->db->select("id,  name as text", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%' OR name LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('tipo_transporte_rodoviario', [], $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }
}