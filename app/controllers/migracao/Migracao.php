<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migracao extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model("dto/AgendaViagemDTO_model", 'AgendaViagemDTO_model');

        $this->load->model('Financeiro_model');
        $this->load->model('Products_model');
        $this->load->model('Sales_model');
        $this->load->model('Companies_model');

    }

    public function index() {show_404();}


    public function importar() {
        try{

            $this->migrar();
            $this->corrigir();
            $this->corrigir_pagamento();
            $this->atribuir_faixa();

        } catch (Exception $exception){
            echo $exception;
            throw new Exception($exception);
        }
    }

    public function atribuir_faixa() {
        $clientes = $this->Companies_model->getAllCustomerCompanies();

        foreach ($clientes as $cliente) {

            $tipoFaixaEtaria = $this->sma->getTipoFaixaEtariaPessoa($cliente->data_aniversario);

            $dadosCliente = array('tipoFaixaEtaria' => $tipoFaixaEtaria);

            echo $this->db->update('companies', $dadosCliente, array('id' => $cliente->id));
        }
    }

    public function corrigir_pagamento() {

        $pagamentos = $this->Sales_model->getAllPayments();

        foreach ($pagamentos as $pagamento) {

            if ($pagamento->formapagamento == null) {

                if ($pagamento->paid_by == 'cash') {
                    $dadosPagamento = array(
                        'formapagamento' =>  2,//dinheiro
                        'tipo' => 'Recebimento',
                        'movimentador' => 11//CAIXA DA EMPRESA
                    );
                }

                if ($pagamento->paid_by == 'CC') {
                    $dadosPagamento = array(
                        'formapagamento' =>  3,//cartao de credito
                        'tipo' => 'Recebimento',
                        'movimentador' => 11//CAIXA DA EMPRESA
                    );
                }

                if ($pagamento->paid_by == 'deposit') {
                    $dadosPagamento = array(
                        'formapagamento' =>  2,//dinheiro
                        'tipo' => 'Recebimento',
                        'movimentador' => 11//CAIXA DA EMPRESA
                    );
                }

                if ($pagamento->paid_by == 'deposit2') {
                    $dadosPagamento = array(
                        'formapagamento' =>  2,//dinheiro
                        'tipo' => 'Recebimento',
                        'movimentador' => 11//CAIXA DA EMPRESA
                    );
                }

                if ($pagamento->tipoCobranca == null) {
                    $dadosPagamento['tipoCobranca'] = 25;//outros
                }

                echo $this->db->update('payments', $dadosPagamento, array('id' => $pagamento->id));
            }
        }
    }

    public function corrigir() {

        $items = $this->Sales_model->getAllItens();

        foreach ($items as $item) {
            try {
                if ($item->programacaoId != null) {
                    $programacao = $this->AgendaViagemService_model->getProgramacaoById($item->programacaoId);

                    $dados_migrar = array(
                        'dateCheckin' => $programacao->getDataSaida(),
                        'dateCheckOut' => $programacao->getDataRetorno(),
                        'horaCheckin' =>  $programacao->getHoraSaida(),
                        'horaCheckOut' => $programacao->getHoraRetorno(),
                    );

                    $this->Sales_model->atualizarDadosItemVenda($dados_migrar, $item->id);
                }
            } catch (Exception $exception) {
                echo $exception->getMessage();
            }
        }
    }

    public function corrigir_paulo_vasconcellos() {
        $items = $this->Sales_model->getAllItens();
        try {
            foreach ($items as $item) {
                $produto_code = $item->product_code;
                $name = $item->product_name;
                $programacao = $this->buscarProdutoAgendaPorCodigo($produto_code);

                if ($programacao->programacaoId == null) $programacao = $this->buscarProdutoAgendaPorNome($name);

                if ($programacao->programacaoId != null) {
                    $this->db->update('sale_items', array('programacaoId' => $programacao->programacaoId), array('id' => $item->id));
                } else {
                    echo $name.'<br/>';
                }
            }

            $this->corrigir();

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }


    function buscarProdutoAgendaPorNome($name) {

        $this->db->select('agenda_viagem.id as programacaoId');
        $this->db->where('name',$name);
        $this->db->order_by('dataSaida', 'asc');
        $this->db->order_by('horaSaida', 'asc');
        $this->db->order_by('dataRetorno', 'asc');
        $this->db->order_by('horaRetorno', 'asc');
        $this->db->limit(1);
        $this->db->join('products', 'products.id=agenda_viagem.produto');

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function buscarProdutoAgendaPorCodigo($produto_code) {

        $this->db->select('agenda_viagem.id as programacaoId');
        $this->db->where('code',$produto_code);
        $this->db->order_by('dataSaida', 'asc');
        $this->db->order_by('horaSaida', 'asc');
        $this->db->order_by('dataRetorno', 'asc');
        $this->db->order_by('horaRetorno', 'asc');
        $this->db->limit(1);
        $this->db->join('products', 'products.id=agenda_viagem.produto');

        $q = $this->db->get('agenda_viagem');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function migrar() {

        $produtos = $this->Products_model->getAllProducts();

        foreach ($produtos as $produto) {

            $agendaDTO = new AgendaViagemDTO_model();

            $vagas = $produto->quantidadePessoasViagem;
            $doDia = $produto->data_saida;
            $aoDia = $produto->data_retorno;
            $horaSaida = $produto->hora_saida;
            $horaRetorno = $produto->hora_retorno;

            $agendaDTO->produto = $produto->id;
            $agendaDTO->vagas = $vagas;
            $agendaDTO->doDia = $doDia;
            $agendaDTO->aoDia = $aoDia;
            $agendaDTO->horaSaida = $horaSaida;
            $agendaDTO->horaRetorno = $horaRetorno;

            $programacaoId = $this->AgendaViagemService_model->salvar($agendaDTO);
            $items = $this->Sales_model->getInvoiceByProduct($produto->id);

            $vendaId        = '';
            $contaReceberId = null;

            foreach ($items as $item) {

                $venda = $this->sales_model->getInvoiceByID($item->sale_id);

                if ($vendaId != $venda->id) {

                    $reference = $this->site->getReference('ex');
                    $tipoCobranca       = 25;
                    $condicaoPagamento  = 1;
                    $movimentador       = 11;
                    $dtVencimento       = $venda->vencimento != null ? $venda->vencimento : $venda->date;

                    $conta_receber = array(
                        'status' => Financeiro_model::STATUS_ABERTA,
                        'receita' => 13,//TODO RECEITA PADRAO VIRA DA CONFIGURACAO
                        'reference' => $reference,
                        'created_by' => $venda->created_by != null ? $venda->created_by : $this->session->userdata('user_id'),
                        'pessoa' => $venda->customer_id,

                        'tipocobranca' => $tipoCobranca,
                        'condicaopagamento' => $condicaoPagamento,

                        'dtvencimento' => $dtVencimento,
                        'dtcompetencia' => $dtVencimento,

                        'valor' => $venda->total,
                        'warehouse' => $venda->warehouse_id,
                        'note' => $venda->note,
                        'sale' => $venda->id,
                    );

                    //criar financeiro venda
                    $contaReceber = new ContaReceber_model();
                    $contaReceber->data = $conta_receber;
                    $contaReceber->valores = [$venda->total];
                    $contaReceber->vencimentos = [$dtVencimento];

                    $contaReceber->cobrancas = [$tipoCobranca];
                    $contaReceber->movimentadores = [$movimentador];
                    $contaReceber->descontos = [0];
                    $contaReceber->senderHash = '';

                    $contaReceberId = $this->Financeiro_model->adicionarReceita($contaReceber);
                    $vendaId = $venda->id;

                    $faturas = $this->Financeiro_model->getParcelasFaturaByContaReceber($vendaId);
                    foreach ($faturas as $fatura) {

                        $pagamentos = $this->Sales_model->getInvoicePayments($vendaId);
                        $dataPagamento = null;
                        $possuirPagamentos = false;

                        foreach ($pagamentos as $pagamento) {

                            $dataPagamento = $pagamento->date;

                            $atualizar_dados_pagamento = array(
                                'fatura'        => $fatura->id,
                                'pessoa'        => $venda->customer_id,
                                'movimentador'  => $movimentador,
                                'tipoCobranca'  => $tipoCobranca,
                            );
                            echo $this->db->update('payments', $atualizar_dados_pagamento, array('id' => $pagamento->id));
                            $possuirPagamentos = true;
                        }

                        if ($possuirPagamentos) {

                            $statusPagamento = $venda->payment_status;
                            $status = '';

                            if ($statusPagamento == 'paid') {//pago
                                $status = 'QUITADA';
                            } else if ($statusPagamento == 'due') {//a vencer
                                $status = 'ABERTA';
                            } else if ($statusPagamento == 'partial') {//parcial
                                $status = 'PARCIAL';
                            }

                            $atualizarFatura = array(
                                'dtultimopagamento' => $dataPagamento,
                                'valorpago' => $venda->paid,
                                'valorpagar' => $venda->total - $venda->paid,
                                'status' => $status,
                                'taxas' => 0,
                                'totalAcrescimo' => 0,
                                'acrescimo' => 0
                            );

                            $parcelasFatura = $this->Financeiro_model->getParcelaFaturaByFatura($fatura->id);
                            foreach ($parcelasFatura as $parcelaFatura) {

                                $parcela = $this->Financeiro_model->getParcelaById($parcelaFatura->parcela);

                                $atualizarParcela = array(
                                    'dtultimopagamento' => $dataPagamento,
                                    'valorpago' => $venda->paid,
                                    'valorpagar' => $venda->total - $venda->paid,
                                    'status' => $status,
                                    'taxas' => 0,
                                    'totalAcrescimo' => 0,
                                    'acrescimo' => 0,
                                    'multa' => 0,
                                    'mora' => 0,
                                    'percentualmulta' => 0,
                                    'percentualmora' => 0,
                                    'diasatraso' => 0
                                );

                                echo $this->db->update('fatura', $atualizarFatura, array('id' => $fatura->id));
                                echo $this->db->update('parcela', $atualizarParcela, array('id' => $parcela->id));

                                echo $this->financeiro_model->edit('parcela_fatura', array('status' => $status), 'id', $parcelaFatura->id);
                                echo $this->financeiro_model->edit('conta_receber',  array('status' => $status), 'id', $parcela->contareceberId );
                            }
                        }
                    }
                }

                $dados_migrar = array(
                    'programacaoId' => $programacaoId,
                    'dataEmissao'   => $item->date,
                    'dateCheckin'   => $doDia,
                    'dateCheckOut'  => $aoDia,
                    'horaCheckin'   => $horaSaida,
                    'horaCheckOut'  => $horaRetorno,
                    'contaReceberId'=> $contaReceberId
                );
                echo $this->Sales_model->atualizarDadosItemVenda($dados_migrar, $item->id_item);
            }
        }
    }

}