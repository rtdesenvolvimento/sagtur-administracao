<?php defined('BASEPATH') or exit('No direct script access allowed');

class Financeiroutil extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('reports', $this->Settings->user_language);
        $this->lang->load('financeiro', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->load->model('settings_model');
    }

    function tiposcobranca()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('financeiro'),
                'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('tiposcobranca')));
        $meta = array('page_title' => lang('tiposcobranca'), 'bc' => $bc);
        $this->page_construct('financeiro/tiposcobranca', $meta, $this->data);
    }

    function getTiposCobranca()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("tipo_cobranca.id as id, 
            tipo_cobranca.name, 
            pos_register.name as conta, 
            forma_pagamento.name as forma_pagamento, 
            tipo_cobranca.status, 
            tipo_cobranca.tipoExibir as tipo ")
            ->from("tipo_cobranca")
            ->join('pos_register', 'pos_register.id = tipo_cobranca.conta', 'left')
            ->join('forma_pagamento', 'forma_pagamento.id = tipo_cobranca.formapagamento', 'left')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('financeiroutil/editarTipoCobranca/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_tipo_cobranca") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_tipo_cobranca") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('financeiroutil/deletarTipoCobranca/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        $this->datatables->where("credito_cliente", 0);
        $this->datatables->where("reembolso", 0);

        if ($this->input->post('filter_type')) {
            $this->datatables->where("{$this->db->dbprefix('tipo_cobranca')}.tipoExibir", $this->input->post('filter_type') );
        }

        echo $this->datatables->generate();
    }


    function adicionarTipoCobranca()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('status', lang("status"), 'required');
        $this->form_validation->set_rules('tipo', lang("tipo"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'status' => $this->input->post('status'),
                'note' => $this->input->post('note'),
                'tipo' => $this->input->post('tipo'),
                'tipoExibir' => $this->input->post('tipoExibir'),
                'conta' => $this->input->post('conta'),
                'integracao' => $this->input->post('integracao'),
                'faturar_automatico' => $this->input->post('faturar_automatico'),
                'formapagamento' => $this->input->post('formapagamento'),
                'faturarVenda' => $this->input->post('faturarVenda'),
                'automatic_cancellation_sale' => $this->input->post('automatic_cancellation_sale'),
                'numero_dias_cancelamento' => $this->input->post('numero_dias_cancelamento'),
                'chave_pix' => $this->input->post('chave_pix'),
                'titular' => $this->input->post('titular'),
                'telefone' => $this->input->post('telefone'),
                'email' => $this->input->post('email'),
                'configurar_pix' => $this->input->post('configurar_pix'),
                'exibirLinkCompras' => TRUE,
            );
        } elseif ($this->input->post('adicionarTipoCobranca')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiroutil/tiposcobranca");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarTipoCobranca($data)) {
            $this->session->set_flashdata('message', lang("tipo_de_cobranca_adicionada_com_successo"));
            redirect("financeiroutil/tiposcobranca");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['movimentadores'] = $this->settings_model->getAllContas();
            $this->data['formaspagamento'] = $this->settings_model->getAllFormasPagamento();
            $this->load->view($this->theme . 'financeiro/adicionarTipoCobranca', $this->data);
        }
    }

    function editarTipoCobranca($id = NULL)
    {
        $tipocobranca = $this->settings_model->getTipoCobrancaId($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('status', lang("status"), 'required');
        $this->form_validation->set_rules('tipo', lang("tipo"), 'required');

        if ($this->form_validation->run() == true) {

            if ($tipocobranca->credito_cliente) {
                $this->session->set_flashdata('error', 'Não é permitido alterar o tipo de cobrança de credito de cliente, ela é de uso interno do sistema.');
                redirect("financeiroutil/tiposcobranca");
            }

            $data = array(
                'name' => $this->input->post('name'),
                'status' => $this->input->post('status'),
                'tipo' => $this->input->post('tipo'),
                'note' => $this->input->post('note'),
                'tipoExibir' => $this->input->post('tipoExibir'),
                'conta' => $this->input->post('conta'),
                'integracao' => $this->input->post('integracao'),
                'faturar_automatico' => $this->input->post('faturar_automatico'),
                'formapagamento' => $this->input->post('formapagamento'),
                'faturarVenda' => $this->input->post('faturarVenda'),
                'automatic_cancellation_sale' => $this->input->post('automatic_cancellation_sale'),
                'numero_dias_cancelamento' => $this->input->post('numero_dias_cancelamento'),
                'chave_pix' => $this->input->post('chave_pix'),
                'titular' => $this->input->post('titular'),
                'telefone' => $this->input->post('telefone'),
                'email' => $this->input->post('email'),
                'configurar_pix' => $this->input->post('configurar_pix'),
                'exibirLinkCompras' => TRUE,
            );
        } elseif ($this->input->post('editarTipoCobranca')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiroutil/tiposcobranca");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTipoCobranca($id, $data)) {
            $this->session->set_flashdata('message', lang("tipo_cobranca_atualizada_com_sucesso"));
            redirect("financeiroutil/tiposcobranca");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $tipocobranca;
            $this->data['movimentadores'] = $this->settings_model->getAllContas();
            $this->data['formaspagamento'] = $this->settings_model->getAllFormasPagamento();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/editarTipoCobranca', $this->data);
        }
    }

    function deletarTipoCobranca($id = NULL)
    {
        $tipocobranca = $this->settings_model->getTipoCobrancaId($id);

        if ($tipocobranca->credito_cliente) {
            echo 'Não é permitido alterar o tipo de cobrança de credito de cliente, ela é de uso interno do sistema.';
        }

        if ($this->settings_model->verificaIntegridadeTipoCobranca($id)) {
            $this->session->set_flashdata('error', lang("tipo_cobranca_sendo_utilizada"));
            redirect("financeiroutil/tiposcobranca", 'refresh');
        }

        if ($this->settings_model->deletarTipoCobranca($id)) {
            echo lang("tipo_cobranca_deletada_com_sucesso");
        }
    }

    function taxas()
    {
        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('system_settings'), 'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('configurar_taxas_tipos_cobranca'))
        );

        $meta = array('page_title' => lang('configurar_taxas_tipos_cobranca'), 'bc' => $bc);

        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaDebito(true);

        $this->page_construct('financeiro/taxas', $meta, $this->data);
    }

}