<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reservations extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //repository
        $this->load->model('repository/ReservationsRepository_model', 'ReservationsRepository_model');
        $this->load->model('repository/CorAgendaRepository_model', 'CorAgendaRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');

        //dto
        $this->load->model('dto/ReservationsFilterDTO_model', 'ReservationsFilterDTO_model');

        //service
        $this->load->model('service/ReservationsService_model', 'ReservationsService_model');

        //model
        $this->load->model('Meiodivulgacao_model');
        $this->load->model("products_model");

        $this->lang->load('reservations', $this->Settings->user_language);
        $this->load->library('form_validation');

    }

    public function index()
    {
        $this->data['products']         =  $this->products_model->getAllProducts('Confirmado');
        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']  = $this->Meiodivulgacao_model->getAll();
        $this->data['locais_embarque']  = $this->site->getLocaisEmbarque();
        $this->data['tipos_transporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reservations')));
        $meta = array('page_title' => lang('reservations'), 'bc' => $bc);
        $this->page_construct('reservations/index', $meta, $this->data);
    }

    public function reserve_items()
    {
        $this->data['products']         =  $this->products_model->getAllProducts('Confirmado');
        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']  = $this->Meiodivulgacao_model->getAll();
        $this->data['locais_embarque']  = $this->site->getLocaisEmbarque();
        $this->data['tipos_transporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reservations')));
        $meta = array('page_title' => lang('reservations'), 'bc' => $bc);
        $this->page_construct('reservations/reserve_items', $meta, $this->data);
    }

    public function getReservations()
    {

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $filterStatus = $this->input->post('filterStatus');
        $flDataVendaDe = $this->input->post('flDataVendaDe');
        $flDataVendaAte = $this->input->post('flDataVendaAte');
        $fdivulgacao = $this->input->post('fdivulgacao');
        $flLocalEmbarque = $this->input->post('flLocalEmbarque');
        $productID = $this->input->post('product');
        $customerID = $this->input->post('customer');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $flGuia = $this->input->post('flGuia');
        $flMotorista = $this->input->post('flMotorista');

        $this->load->library('datatables');

        $this->datatables
            ->select("
                    sma_sales.id as id, 
                    sma_sales.date, 
                    sma_sales.reference_no,
                    sma_sales.vendedor,
                    CONCAT(sma_sales.customer,'<br/>' , CONCAT( sma_sales.total_items , ' PAX<br/> ')) as iname,
                    sma_sales.sale_status, 
                    sma_sales.grand_total, 
                    sma_sales.paid, 
                    (sma_sales.grand_total-sma_sales.paid) as balance,
                    sma_sales.payment_status")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id', 'left');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId', 'left');

        $this->datatables->group_by('sales.id');

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        if ($filterStatus) {
            $this->datatables->where('sale_status', $filterStatus);
        }

        if ($fdivulgacao) {
            $this->datatables->where('meiodivulgacao', $fdivulgacao);
        }

        if ($flLocalEmbarque) {
            $this->datatables->where('sale_items.localEmbarque', $flLocalEmbarque);
        }

        if ($flGuia) {
            $this->datatables->where('sale_items.guia', $flGuia);
        }

        if ($flMotorista) {
            $this->datatables->where('sale_items.motorista', $flMotorista);
        }

        if ($flDataVendaDe) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $flDataVendaDe);
        }

        if ($flDataVendaAte) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $flDataVendaAte);
        }

        if ($productID) {
            $this->datatables->where('sale_items.product_id', $productID);
        }

        if ($customerID) {
            $this->datatables->where('sale_items.customerClient', $customerID);
        }

        if ($start_date) {
            $this->datatables->where('agenda_viagem.dataSaida>=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('agenda_viagem.dataSaida<=', $end_date);
        }

        if ($start_time) {
            $this->datatables->where('agenda_viagem.horaSaida>=', $start_time);
        }

        if ($end_time) {
            $this->datatables->where('agenda_viagem.horaSaida<=', $end_time);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getReservationsItem()
    {

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $filterStatus = $this->input->post('filterStatus');
        $flDataVendaDe = $this->input->post('flDataVendaDe');
        $flDataVendaAte = $this->input->post('flDataVendaAte');
        $fdivulgacao = $this->input->post('fdivulgacao');
        $flLocalEmbarque = $this->input->post('flLocalEmbarque');
        $productID = $this->input->post('product');
        $customerID = $this->input->post('customer');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $flGuia = $this->input->post('flGuia');
        $flMotorista = $this->input->post('flMotorista');

        $this->load->library('datatables');


        $this->datatables
            ->select("sma_sales.id as id, 
                sma_sales.date, 
                sma_sales.reference_no,
                sma_sales.vendedor,
                concat(sma_sale_items.customerClientName,'<br/><small>', sma_sale_items.faixaNome, '</small>' ) as customer,
                concat(sma_sale_items.product_name, '<br/>', DATE_FORMAT(sma_agenda_viagem.dataSaida, '%d/%m/%Y'), '<br/>', DATE_FORMAT(sma_agenda_viagem.horaSaida, '%h:%i')) as product_name,
                sma_sales.sale_status,

                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) as grand_total, 
                
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal))
                 * (sma_sales.paid*100/(sma_sales.grand_total))/100  as paid, 
                
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) -
                (sma_sale_items.subtotal*(1-((sma_sales.order_discount+sma_sales.shipping)/sma_sales.total)) * (sma_sales.paid*100/(sma_sales.total-(sma_sales.order_discount+sma_sales.shipping)))/100) as balance,
                
                sma_sales.payment_status,
                sma_sales.venda_link")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId', 'left');

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        if ($filterStatus) {
            $this->datatables->where('sale_status', $filterStatus);
        }

        if ($fdivulgacao) {
            $this->datatables->where('meiodivulgacao', $fdivulgacao);
        }

        if ($flLocalEmbarque) {
            $this->datatables->where('sale_items.localEmbarque', $flLocalEmbarque);
        }

        if ($flGuia) {
            $this->datatables->where('sale_items.guia', $flGuia);
        }

        if ($flMotorista) {
            $this->datatables->where('sale_items.motorista', $flMotorista);
        }

        if ($flDataVendaDe) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $flDataVendaDe);
        }

        if ($flDataVendaAte) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $flDataVendaAte);
        }

        if ($productID) {
            $this->datatables->where('sale_items.product_id', $productID);
        }

        if ($customerID) {
            $this->datatables->where('sale_items.customerClient', $customerID);
        }

        if ($start_date) {
            $this->datatables->where('agenda_viagem.dataSaida>=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('agenda_viagem.dataSaida<=', $end_date);
        }

        if ($start_time) {
            $this->datatables->where('agenda_viagem.horaSaida>=', $start_time);
        }

        if ($end_time) {
            $this->datatables->where('agenda_viagem.horaSaida<=', $end_time);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function reservations_os()
    {
        $this->data['products']         = $this->products_model->getAllProducts('Confirmado');
        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']  = $this->Meiodivulgacao_model->getAll();
        $this->data['locais_embarque']  = $this->site->getLocaisEmbarque();
        $this->data['tipos_transporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reservations_os')));
        $meta = array('page_title' => lang('reservations_os'), 'bc' => $bc);
        $this->page_construct('reservations/reservations_os', $meta, $this->data);
    }

    public function getReservationsOs()
    {

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $filterStatus = $this->input->post('filterStatus');
        $flDataVendaDe = $this->input->post('flDataVendaDe');
        $flDataVendaAte = $this->input->post('flDataVendaAte');
        $fdivulgacao = $this->input->post('fdivulgacao');
        $productID = $this->input->post('product');
        $customerID = $this->input->post('customer');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $start_time = $this->input->post('start_time');
        $end_time = $this->input->post('end_time');
        $flLocalEmbarque = $this->input->post('flLocalEmbarque');
        $flTipoTransporte = $this->input->post('flTipoTransporte');
        $flGuia = $this->input->post('flGuia');
        $flMotorista = $this->input->post('flMotorista');

        $this->load->library('datatables');

        $this->datatables
            ->select("sma_sales.id as id, 
                sma_sales.date, 
                sma_sales.reference_no,
                sma_sales.vendedor,
                concat(sma_sale_items.customerClientName,'<br/><small>', sma_sale_items.faixaNome, '</small>' ) as customer,
                concat(sma_sale_items.product_name, '<br/>', DATE_FORMAT(sma_agenda_viagem.dataSaida, '%d/%m/%Y'), '<br/>', DATE_FORMAT(sma_agenda_viagem.horaSaida, '%h:%i')) as product_name,
                local_embarque.name as local_embarque,
                g.name as guia,
                m.name as motorista,
                tipo_transporte_rodoviario.name as tipo_transporte,
                sma_sales.sale_status,
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) as grand_total, 
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal))
                 * (sma_sales.paid*100/(sma_sales.grand_total))/100  as paid, 
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) -
                (sma_sale_items.subtotal*(1-((sma_sales.order_discount+sma_sales.shipping)/sma_sales.total)) * (sma_sales.paid*100/(sma_sales.total-(sma_sales.order_discount+sma_sales.shipping)))/100) as balance,
                sma_sales.payment_status,
                sma_sales.venda_link,
                sale_items.id as itemid")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId', 'left');
        $this->datatables->join('local_embarque', 'local_embarque.id = sale_items.localEmbarque', 'left');
        $this->datatables->join('companies g', 'g.id = sale_items.guia', 'left');
        $this->datatables->join('companies m', 'm.id = sale_items.motorista', 'left');
        $this->datatables->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id = sale_items.tipoTransporte', 'left');

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        if ($filterStatus) {
            $this->datatables->where('sale_status', $filterStatus);
        }

        if ($fdivulgacao) {
            $this->datatables->where('meiodivulgacao', $fdivulgacao);
        }

        if ($flLocalEmbarque) {
            $this->datatables->where('sale_items.localEmbarque', $flLocalEmbarque);
        }

        if ($flTipoTransporte) {
            $this->datatables->where('sale_items.tipoTransporte', $flTipoTransporte);
        }

        if ($flGuia) {
            $this->datatables->where('sale_items.guia', $flGuia);
        }

        if ($flMotorista) {
            $this->datatables->where('sale_items.motorista', $flMotorista);
        }

        if ($flDataVendaDe) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $flDataVendaDe);
        }

        if ($flDataVendaAte) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $flDataVendaAte);
        }

        if ($productID) {
            $this->datatables->where('sale_items.product_id', $productID);
        }

        if ($customerID) {
            $this->datatables->where('sale_items.customerClient', $customerID);
        }

        if ($start_date) {
            $this->datatables->where('agenda_viagem.dataSaida>=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('agenda_viagem.dataSaida<=', $end_date);
        }

        if ($start_time) {
            $this->datatables->where('agenda_viagem.horaSaida>=', $start_time);
        }

        if ($end_time) {
            $this->datatables->where('agenda_viagem.horaSaida<=', $end_time);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function montar_os()
    {
        foreach ($_GET['ids'] as $id) {
            $local_embarque =  $_GET['local_embarque'];
            $guia_id =  $_GET['guia_id'];
            $motorista_id =  $_GET['motorista_id'];
            $tipo_transporte_id =  $_GET['tipo_transporte_id'];

            $this->ReservationsService_model->montar_os($id, $local_embarque, $guia_id, $motorista_id, $tipo_transporte_id);
        }
    }

    public function agenda_reservas() {

        $this->data['products'] = $this->products_model->getAllTours();
        $this->data['billers']  = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao'] = $this->Meiodivulgacao_model->getAll();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('agenda_reservas')));
        $meta = array('page_title' => lang('agenda_reservas'), 'bc' => $bc);

        $this->page_construct('reservations/agenda_reservas', $meta, $this->data);
    }

    public function search_reservations() {

        $productID = $this->input->get('produto_id');
        $start_time = $this->input->get('start_time');
        $end_time = $this->input->get('end_time');
        $status_sale = $this->input->get('status_sale');
        $status_payment = $this->input->get('status_payment');
        $biller = $this->input->get('biller');
        $customer = $this->input->get('customer');
        $meio_divulgacao = $this->input->get("meio_divulgacao");
        $data_venda_de = $this->input->get('data_venda_de');
        $data_venda_ate = $this->input->get('data_venda_ate');

        $filter = new ReservationsFilterDTO_model();
        $filter->product_id = $productID;
        $filter->hora_saida = $start_time;
        $filter->hora_retorno = $end_time;
        $filter->status_sale = $status_sale;
        $filter->status_payment = $status_payment;
        $filter->biller = $biller;
        $filter->customer = $customer;
        $filter->meio_divulgacao = $meio_divulgacao;
        $filter->data_venda_de = $data_venda_de;
        $filter->data_venda_ate = $data_venda_ate;
        $filter->group_by = false;

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $filter->created_by = $this->session->userdata('user_id');
        }

        $reservas = $this->ReservationsRepository_model->getAllInvoiceItems($filter);

        $output_arrays = array();

        foreach ($reservas as $reserva) {

            $cor = $this->CorAgendaRepository_model->getByID($reserva->color_agenda_id);

            $output_arrays[] = array(
                'id' => $reserva->id,
                'title' => $reserva->customer_name.' | '.$reserva->product_name.' ('.lang($reserva->sale_status).')',

                'description' => 'description for Long Event',//TODO VERIFICAR

                'start' => $reserva->dtSaida.'T'.$reserva->hrSaida,
                'end' => $reserva->dtRetorno.'T'.$reserva->hrRetorno,

                'url' =>  base_url().'salesutil/modal_view/'.$reserva->id,

                'backgroundColor' => $cor->cor,
                'borderColor' =>  $cor->cor,
            );
        }

        $this->sma->send_json($output_arrays);
    }

    public function search_reservations_actions() {

        $productID = $this->input->get('tours');
        $start_time = $this->input->get('start_time');
        $end_time = $this->input->get('end_time');
        $status_sale = $this->input->get('filterStatus');
        $status_payment = $this->input->get('situacao');
        $biller = $this->input->get('billerFilter');
        $customer = $this->input->get('customer_filter');
        $meio_divulgacao = $this->input->get("fdivulgacao");
        $data_venda_de = $this->input->get('flDataVendaDe');
        $data_venda_ate = $this->input->get('flDataVendaAte');
        $start_date = $this->input->get('flatpickr_date');
        $end_date = $this->input->get('flatpickr_date');
        $local_embarque = $this->input->get('flLocalEmbarque');
        $tipo_transporte = $this->input->get('flTipoTransporte');

        $filter = new ReservationsFilterDTO_model();
        $filter->product_id = $productID;
        $filter->data_saida = $start_date;
        $filter->data_retorno = $end_date;
        $filter->hora_saida = $start_time;
        $filter->hora_retorno = $end_time;
        $filter->status_sale = $status_sale;
        $filter->status_payment = $status_payment;
        $filter->biller = $biller;
        $filter->customer = $customer;
        $filter->meio_divulgacao = $meio_divulgacao;
        $filter->data_venda_de = $data_venda_de;
        $filter->data_venda_ate = $data_venda_ate;
        $filter->local_embarque = $local_embarque;
        $filter->tipo_transporte = $tipo_transporte;
        $filter->group_by = false;

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $filter->created_by = $this->session->userdata('user_id');
        }

        $reservas = $this->ReservationsRepository_model->getAllInvoiceItems($filter);

        if (!empty($reservas)) {

            $this->data['reservas'] = $reservas;
            $this->data['filters']  = $filter;

            $name = 'Relatório de Reservas de Passeios e Atividades.pdf';

            $html = $this->load->view($this->theme . 'reservations/reserves_sale_report', $this->data, TRUE);
            $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
        } else {
            $this->session->set_flashdata('error', lang("no_sale_selected"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function reservations_items_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {

                $productID = $this->input->post('product');
                $customer = $this->input->post('customer');
                $status_payment = $this->input->post('situacao');
                $biller = $this->input->post('billerFilter');
                $status_sale = $this->input->post('filterStatus');
                $data_venda_de = $this->input->post('flDataVendaDe');
                $data_venda_ate = $this->input->post('flDataVendaAte');
                $meio_divulgacao = $this->input->post('fdivulgacao');
                $start_time = $this->input->post('start_time');
                $end_time = $this->input->post('end_time');
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                $local_embarque = $this->input->post('flLocalEmbarque');
                $tipo_transporte = $this->input->post('flTipoTransporte');
                $guia = $this->input->post('flGuia');
                $motorista = $this->input->post('flMotorista');

                $filter = new ReservationsFilterDTO_model();
                $filter->product_id = $productID;
                $filter->hora_saida = $start_time;
                $filter->hora_retorno = $end_time;
                $filter->status_sale = $status_sale;
                $filter->status_payment = $status_payment;
                $filter->biller = $biller;
                $filter->customer = $customer;
                $filter->meio_divulgacao = $meio_divulgacao;
                $filter->data_venda_de = $data_venda_de;
                $filter->data_venda_ate = $data_venda_ate;
                $filter->data_saida = $start_date;
                $filter->data_retorno = $end_date;
                $filter->local_embarque = $local_embarque;
                $filter->tipo_transporte = $tipo_transporte;
                $filter->guia = $guia;
                $filter->motorista = $motorista;
                $filter->group_by = null;

                $sales = [];

                foreach ($_POST['val'] as $id) {
                    $invoice = $this->ReservationsRepository_model->getInvoiceByID($id);
                    $sales[$id] = $invoice;
                }

                $this->data['sales']    = $sales;
                $this->data['filters']  = $filter;

                if ($this->input->post('form_action') == 'export_pdf') {
                    $name = 'Relatório de Reservas de Passeios e Atividades.pdf';
                    $html = $this->load->view($this->theme . 'reservations/reserves_report', $this->data, TRUE);
                    $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
                } elseif($this->input->post('form_action') == 'reserves_detailed') {
                    $name = 'Relatório de Reservas de Passeios e Atividades Detalhado.pdf';
                    $html = $this->load->view($this->theme . 'reservations/reserves_detailed', $this->data, TRUE);
                    $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
                }

            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function reservations_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {

                $productID = $this->input->post('product');
                $customer = $this->input->post('customer');
                $status_payment = $this->input->post('situacao');
                $biller = $this->input->post('billerFilter');
                $status_sale = $this->input->post('filterStatus');
                $data_venda_de = $this->input->post('flDataVendaDe');
                $data_venda_ate = $this->input->post('flDataVendaAte');
                $meio_divulgacao = $this->input->post('fdivulgacao');
                $start_time = $this->input->post('start_time');
                $end_time = $this->input->post('end_time');
                $start_date = $this->input->post('start_date');
                $end_date = $this->input->post('end_date');
                $local_embarque = $this->input->post('flLocalEmbarque');
                $tipo_transporte = $this->input->post('flTipoTransporte');
                $guia = $this->input->post('flGuia');
                $motorista = $this->input->post('flMotorista');

                $filter = new ReservationsFilterDTO_model();
                $filter->product_id = $productID;
                $filter->hora_saida = $start_time;
                $filter->hora_retorno = $end_time;
                $filter->status_sale = $status_sale;
                $filter->status_payment = $status_payment;
                $filter->biller = $biller;
                $filter->customer = $customer;
                $filter->meio_divulgacao = $meio_divulgacao;
                $filter->data_venda_de = $data_venda_de;
                $filter->data_venda_ate = $data_venda_ate;
                $filter->data_saida = $start_date;
                $filter->data_retorno = $end_date;
                $filter->local_embarque = $local_embarque;
                $filter->tipo_transporte = $tipo_transporte;
                $filter->guia = $guia;
                $filter->motorista = $motorista;
                $filter->group_by = null;

                foreach ($_POST['val'] as $id) {
                    $sales[] = $this->ReservationsRepository_model->getInvoiceByID($id);
                }

                $this->data['sales']    = $sales;
                $this->data['filters']  = $filter;

                if ($this->input->post('form_action') == 'export_pdf') {
                    $name = 'Relatório de Reservas de Passeios e Atividades.pdf';
                    $html = $this->load->view($this->theme . 'reservations/reserves_report', $this->data, TRUE);
                    $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
                } elseif($this->input->post('form_action') == 'reserves_detailed') {
                    $name = 'Relatório de Reservas de Passeios e Atividades Detalhado.pdf';
                    $html = $this->load->view($this->theme . 'reservations/reserves_detailed', $this->data, TRUE);
                    $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');
                } elseif($this->input->post('form_action') == 'export_excel') {
                    $this->relatorio_geral_ferroviario_excel($sales, $filter);
                } elseif($this->input->post('form_action') == 'export_sales_excel') {
                    $this->relatorio_geral_venda_ferroviario_excel($sales, $filter);
                }

            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function relatorio_geral_ferroviario_excel($sales, $filters)
    {

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Relatório de Atividades');
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('Data'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('Saída'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('Retorno'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('Assento'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('Passageiro'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('Nº Voucher.'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('E-mail'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('Contato'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('Atividade'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('Faixa'));

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->SetCellValue('J1', lang('Receber'));
        }

        $row = 2;

        foreach ($sales as $sale) {

            $itens = $this->ReservationsRepository_model->getAllInvoiceItems($filters, $sale->id);

            foreach ($itens as $item) {

                $customer = $this->site->getCompanyByID($item->customerClient);
                $totalAbertoPorDependente = '';

                $whatsApp = $customer->cf5;
                $phone = $customer->phone;
                $telefone_emergencia = $customer->telefone_emergencia;
                $email = $customer->email;
                $contato = '';

                if ($whatsApp) {
                    $contato = $whatsApp;
                }
                if ($phone) {
                    $contato = $contato . ' ' . $phone;
                }
                if ($telefone_emergencia) {
                    $contato = $contato . ' ' . $telefone_emergencia;
                }

                if ($this->Settings->show_payment_report_shipment) {

                    $totalPagarItem = $item->subtotal;
                    $acrescimo      = $item->shipping;
                    $desconto       = $item->order_discount;
                    $totalVenda     = $item->grand_total - $acrescimo + $desconto;

                    $totalRealPagoPorDependente = $totalPagarItem * (($item->paid*100/$totalVenda)/100);
                    $totalAberto   =  $totalPagarItem - $totalRealPagoPorDependente;

                    if ($totalAberto > 0) {
                        $totalAbertoPorDependente = $this->sma->formatMoney($totalAberto);

                        if ($acrescimo > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Acres: '.$this->sma->formatMoney($acrescimo).'</small>';
                        }

                        if ($desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Desc:'.$this->sma->formatMoney($desconto).'</small>';
                        }

                        if ($acrescimo > 0 || $desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Pagar:'.$this->sma->formatMoney($totalAberto + $acrescimo - $desconto).'</small>';
                        }

                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                } else {
                    if ($row->customer_id == $row->customerClient) {
                        $totalEmAberto = $item->grand_total - $item->paid;
                        if ($totalEmAberto > 0) {
                            $totalAbertoPorDependente = $this->sma->formatMoney($totalEmAberto);
                        } else {
                            $totalAbertoPorDependente = 'PAGO';
                        }
                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                }

                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($item->dtSaida));
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $this->sma->hf($item->hrSaida));
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->hf($item->hrRetorno));
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $item->poltronaClient);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->name);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sale->reference_no);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $email);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $contato);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $item->product_name);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $item->faixaNome);

                if ($this->Settings->show_payment_report_shipment) {
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $totalAbertoPorDependente);
                }

                $row++;
            }
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(45);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(45);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(45);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(45);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(45);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(45);

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(45);
        }

        // Definindo o estilo de fonte para as colunas
        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 15,
            ],
        ];

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        } else {
            $this->excel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO PASSAGEIROS POR ATIVIDADE EMISSÃO '.date('Y/m/d H:i:s');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function relatorio_geral_venda_ferroviario_excel($sales, $filters)
    {

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Relatório de Atividades');
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('Data da Venda'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('Data da Atividade'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('Saída'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('Retorno'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('Atividade'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('Nº Voucher.'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('Passageiro'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('E-mail'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('Contato'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('Forma de Pagamento'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('Valor'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('Taxas'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('N° de PAX'));

        $row = 2;

        foreach ($sales as $sale) {

            $itens = $this->ReservationsRepository_model->getAllInvoiceItems($filters, $sale->id);
            $faturas = $this->site->getParcelasFaturaByContaReceber($sale->id);
            $tipoCobranca = $this->site->getTipoCobrancaById($sale->tipoCobrancaId);

            $product_name = 'LISTA DE ESPERA';
            $totalTaxas = 0;
            $hrSaida = '';
            $hrRetorno = '';
            $dtSaida = '';

            foreach ($itens as $item) {
                $product_name = $item->product_name;
                $hrSaida = $item->hrSaida;
                $hrRetorno = $item->hrRetorno;
                $dtSaida = $this->sma->hrsd($item->dtSaida);
            }

            foreach ($faturas as $fatura) {
                $totalTaxas += $fatura->taxas;
            }

            $customer = $this->site->getCompanyByID($sale->customer_id);

            $whatsApp = $customer->cf5;
            $phone = $customer->phone;
            $telefone_emergencia = $customer->telefone_emergencia;
            $email = $customer->email;
            $contato = '';

            if ($whatsApp) {
                $contato = $whatsApp;
            }
            if ($phone) {
                $contato = $contato . ' ' . $phone;
            }
            if ($telefone_emergencia) {
                $contato = $contato . ' ' . $telefone_emergencia;
            }


            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($sale->date));
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $dtSaida);
            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->hf($hrSaida));
            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $this->sma->hf($hrRetorno));
            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product_name);
            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sale->reference_no);

            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $email);
            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $contato);

            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $tipoCobranca->name);
            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $sale->grand_total);
            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalTaxas);
            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $sale->total_items);

            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);

        // Definindo o estilo de fonte para as colunas
        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 13,
            ],
        ];

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        } else {
            $this->excel->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO PASSAGEIROS POR ATIVIDADE EMISSÃO '.date('Y/m/d H:i:s');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

}
