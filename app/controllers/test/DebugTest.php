<?php
class DebugTest extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('unit_test');
    }

    function index(){

        $dataOrigem = '14/07/1994';
        //$data = date("d/m/Y", strtotime($data));
        $dataFormatada = implode("-",array_reverse(explode("/",$dataOrigem)));

        echo 'Original '.$dataOrigem. ' formatada '.$dataFormatada;
    }
}