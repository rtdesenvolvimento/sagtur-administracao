<?php
class SalesTest extends MY_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('unit_test');
    }

    function index(){
        $this->unit->run($this->soma(1,1), 2 , "Testando a soma de dois numeros", ' O resultado esperado é de 2');
        $this->load->view($this->theme.'/test/test');
    }

    private function gerarContaPagarClienteTest() {

    }

    private function gerarComissaoVendedorTest() {

    }

    private function cadastrarProdutoAdicionadoManualmenteVendaTest() {

    }
}
