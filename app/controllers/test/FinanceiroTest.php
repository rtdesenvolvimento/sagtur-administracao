<?php

class FinanceiroTest extends MY_Controller {

    function __construct() {

        parent::__construct();
        $this->load->model('financeiro_model');

        //implementa o model
        $this->load->library('unit_test');
    }

    function index(){
        $this->unit->run($this->soma(1,1), 2, "Testando a soma de dois numeros", ' O resultado esperado é de 2');
        $this->load->view($this->theme.'/test/test');
    }

    private function calcularJurosDaParcelaPorAtrasoTest() {

    }

    public function soma($numero1, $numero2) {
        return $numero1 + $numero2;
    }


}