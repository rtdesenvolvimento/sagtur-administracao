<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site_settings extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('welcome');
        }

        $this->lang->load('settings', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->model('settings_model');

        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '16384';
    }

    function testimonial()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('site_settings'),
                'page' => lang('site_settings')),
            array('link' => '#', 'page' => lang('testimonials')));
        $meta = array('page_title' => lang('testimonials'), 'bc' => $bc);
        $this->page_construct('settings/testimonial', $meta, $this->data);
    }

    function getTestimonial()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, photo, name, profession")
            ->from("testimonial")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('site_settings/editTestimonial/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_testimonial") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_testimonial") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('site_settings/deletarTestimonial/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addTestimonial()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'profession' => $this->input->post('profession'),
                'testimonial' => $this->input->post('testimonial'),
            );

            if ($_FILES['photo']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . '/';
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

                $data['photo'] = $photo;
            }

        } elseif ($this->input->post('addTestimonial')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/testimonial");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addTestimonial($data)) {
            $this->session->set_flashdata('message', lang("testimonial_adicionado_com_sucesso"));
            redirect("site_settings/testimonial");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/addTestimonial', $this->data);
        }
    }

    function editTestimonial($id = NULL)
    {
        $testimonial = $this->settings_model->getTestimonial($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'profession' => $this->input->post('profession'),
                'testimonial' => $this->input->post('testimonial'),
            );

            if ($_FILES['photo']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . '/';
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

                $data['photo'] = $photo;
            }
        } elseif ($this->input->post('editTestimonial')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/testimonial");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTestimonial($id, $data)) {
            $this->session->set_flashdata('message', lang("testimonial_atualizado_com_sucesso"));
            redirect("site_settings/testimonial");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['testimonial'] = $testimonial;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/editTestimonial', $this->data);
        }
    }

    function deletarTestimonial($id = NULL)
    {
        if ($this->settings_model->deletarTestimonial($id)) {
            echo lang("testimonial_deletada_com_sucesso");
        }
    }

    function team()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('site_settings'),
                'page' => lang('site_settings')),
            array('link' => '#', 'page' => lang('team')));
        $meta = array('page_title' => lang('team'), 'bc' => $bc);
        $this->page_construct('settings/team', $meta, $this->data);
    }

    function getTeam()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, photo, name, office")
            ->from("team_site")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('site_settings/editTeam/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_team") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_team") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('site_settings/deletarTeam/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addTeam()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'office' => $this->input->post('office'),
                'facebook' => $this->input->post('facebook'),
                'instagram' => $this->input->post('instagram'),
                'testimonial' => $this->input->post('testimonial'),
            );

            if ($_FILES['photo']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . '/';
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

                $data['photo'] = $photo;
            }

        } elseif ($this->input->post('addTeam')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/team");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addTeam($data)) {
            $this->session->set_flashdata('message', lang("team_adicionado_com_sucesso"));
            redirect("site_settings/team");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/addTeam', $this->data);
        }
    }

    function editTeam($id = NULL)
    {
        $team  = $this->settings_model->getTeam($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'office' => $this->input->post('office'),
                'facebook' => $this->input->post('facebook'),
                'instagram' => $this->input->post('instagram'),
                'testimonial' => $this->input->post('testimonial'),
            );

            if ($_FILES['photo']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . '/';
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('photo')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

                $data['photo'] = $photo;
            }
        } elseif ($this->input->post('editTeam')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/team");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTeam($id, $data)) {
            $this->session->set_flashdata('message', lang("team_atualizado_com_sucesso"));
            redirect("site_settings/team");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['team'] = $team;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/editTeam', $this->data);
        }
    }

    function deletarTeam($id = NULL)
    {
        if ($this->settings_model->deletarTeam($id)) {
            echo lang("team_deletada_com_sucesso");
        }
    }

    function gallery()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('site_settings'),
                'page' => lang('site_settings')),
            array('link' => '#', 'page' => lang('gallery')));
        $meta = array('page_title' => lang('gallery'), 'bc' => $bc);
        $this->page_construct('settings/gallery', $meta, $this->data);
    }

    function getGallery()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name")
            ->from("gallery")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('site_settings/editGallery/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_gallery") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_gallery") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('site_settings/deletarGallery/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addGallery()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
            );

        } elseif ($this->input->post('addGallery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/gallery");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addGallery($data)) {
            $this->session->set_flashdata('message', lang("gallery_adicionado_com_sucesso"));
            redirect("site_settings/gallery");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/addGallery', $this->data);
        }
    }

    function editGallery($id = NULL)
    {
        $gallery  = $this->settings_model->getGallery($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
            );

            $photos = $this->galerry_item();

        } elseif ($this->input->post('editGallery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("site_settings/gallery");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateGallery($id, $data, $photos)) {
            $this->session->set_flashdata('message', lang("gallery_atualizado_com_sucesso"));
            redirect("site_settings/gallery");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['gallery'] = $gallery;
            $this->data['images'] = $this->settings_model->getGalleryItens($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'settings/editGallery', $this->data);
        }
    }

    private function galerry_item() {

        $photos = null;
        $this->load->library('upload');

        if ($_FILES['userfile']['name'][0] == "") return NULL;

        $config['upload_path']      = $this->upload_path;
        $config['allowed_types']    = $this->image_types;
        $config['max_size']         = '4096';//4MB;
        $config['max_width']        = $this->Settings->iwidth;
        $config['max_height']       = $this->Settings->iheight;
        $config['overwrite']        = FALSE;
        $config['encrypt_name']     = TRUE;
        $config['max_filename']     = 25;

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("site_settings/gallery");
            } else {

                $pho = $this->upload->file_name;
                $photos[] = $pho;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $pho;
                $config['new_image'] = $this->thumbs_path . $pho;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;

                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $pho;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
            }
        }

        $config = NULL;
        $_FILES = $files;

        return $photos;
    }

    public function delete_image_gallery($id = NULL)
    {
        if ($this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $id || die(json_encode(array('error' => 1, 'msg' => lang('no_image_selected'))));
            $this->db->delete('gallery_item', array('id' => $id));
            die(json_encode(array('error' => 0, 'msg' => lang('image_deleted'))));
        }
        die(json_encode(array('error' => 1, 'msg' => lang('ajax_error'))));
    }

    function deletarGallery($id = NULL)
    {
        if ($this->settings_model->deletarGallery($id)) {
            echo lang("gallery_deletada_com_sucesso");
        }
    }
}