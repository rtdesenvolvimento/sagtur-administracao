<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contapagar extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('financeiro', $this->Settings->user_language);
        $this->lang->load('reports', $this->Settings->user_language);

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '16384';
    }

    public function index()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) {
            $start_date = date('Y-m-d', $data_incio);
        }

        if (!$end_date) {
            $end_date = date('Y-m-d', $data_fim);
        }

        $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaDespesa();
        $this->data['planoDespesas'] = $this->site->getAllDespesasSuperiores();
        $this->data['billers']       = $this->site->getAllCompanies('biller');

        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['ano'] = date('Y');
        $this->data['mes'] = date('m');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('contas_pagar')));
        $meta = array('page_title' => lang('contas_pagar'), 'bc' => $bc);
        $this->page_construct('conta_pagar/index', $meta, $this->data);

    }

    function getDespesas() {

        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_fatura_link = anchor('faturas/editarFatura/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //<li class="divider"></li>
        //<li>'.$edit_fatura_link.'</li>

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                {$this->db->dbprefix('fatura')}.product_name,
                {$this->db->dbprefix('fatura')}.strDespesas as despesa,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.valorpago, 
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'DEBITO');

        if ($this->input->post('start_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$this->input->post('start_date')}' ");
        }

        if ($this->input->post('end_date')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$this->input->post('end_date')}' ");
        }

        if ($this->input->post('filterTipoCobranca')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        }

        if ($this->input->post('filterDespesa')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.despesa", $this->input->post('filterDespesa') );
        }

        if ($this->input->post('filterProgramacao')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        }

        if ($this->input->post('filterDataPagamentoDe')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento >= '{$this->input->post('filterDataPagamentoDe')}' ");
        }

        if ($this->input->post('filterDataPagamentoAte')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento <= '{$this->input->post('filterDataPagamentoAte')}' ");
        }

        if ($this->input->post('filterCustomer')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.pessoa", $this->input->post('filterCustomer'));
        }

        if ($this->input->post('filterBiller')) {
            $this->datatables->join('sales', 'sales.id = fatura.sale_id', 'left');

            $this->datatables->where("{$this->db->dbprefix('sales')}.biller_id", $this->input->post('filterBiller'));
        }

        if ($this->input->post('filterStatus')) {
            if ($this->input->post('filterStatus') == 'VENCIDA') {
                $this->datatables->where("DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) < 0");
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL')");
            } else {
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status", $this->input->post('filterStatus') );
            }
        } else {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL', 'QUITADA')");
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function adicionarContaPagar()
    {
        $this->load->model('model/ContaPagar_model', 'ContaPagar_model');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('valor', lang("valor"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('despesa', lang("despesa"), 'required');
        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->site->getReference('ex');
            $vencimentos = $this->input->post('dtVencimentoParcela');
            $tiposCobranca = $this->input->post('tipocobrancaParcela');
            $valorVencimento = $this->input->post('valorVencimentoParcela');
            $movimentadores = $this->input->post('movimentadorParcela');
            $descontoParcela = $this->input->post('descontoParcela');

            $data = array(
                'status' => ContaPagar_model::STATUS_ABARTA,
                'reference' => $reference,
                'created_by' => $this->session->userdata('user_id'),
                'pessoa' => $this->input->post('pessoa', true),
                'programacaoId' => $this->input->post('programacaoId', true),
                'tipocobranca' => $this->input->post('tipocobranca', true),
                'despesa' => $this->input->post('despesa', true),
                'condicaopagamento' => $this->input->post('condicaopagamento', true),
                'dtvencimento' => $this->input->post('dtvencimento', true),
                'dtcompetencia' => $this->input->post('dtvencimento', true),
                'valor' => $this->input->post('valor'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
                'palavra_chave' => $this->input->post('palavra_chave', true),
                'numero_documento' => $this->input->post('numero_documento', true)
            );

            $data['attachment'] = $this->uploadArquivo();

            $contaPagar = new ContaPagar_model();
            $contaPagar->data = $data;
            $contaPagar->valores = $valorVencimento;
            $contaPagar->vencimentos = $vencimentos;
            $contaPagar->cobrancas = $tiposCobranca;
            $contaPagar->movimentadores = $movimentadores;
            $contaPagar->descontos = $descontoParcela;

            //$this->sma->print_arrays($data);

        } elseif ($this->input->post('adicionarContaPagar')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            try {

                $this->load->model('financeiro_model');

                $this->financeiro_model->adicionarDespesa($contaPagar);

                $this->session->set_flashdata('message', lang("financeiro_conta_pagar_adicionada_com_sucesso"));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['programacoes'] = $this->site->getProgramacaoAll('','');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['despesas'] = $this->site->getAllDespesasSuperiores();
            $this->data['tiposCobranca'] = $this->site->getAllTiposCobranca(false, true);
            $this->data['condicoesPagamento'] = $this->site->getAllCondicoesPagamento();
            $this->data['movimentadores'] = $this->site->getAllContas();

            $this->load->view($this->theme . 'conta_pagar/add', $this->data);
        }
    }

    private function uploadArquivo()
    {
        if ($_FILES['userfile']['size'] > 0) {

            $this->load->library('upload');

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->digital_file_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            return $this->upload->file_name;
        }
        return '';
    }

}