<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pix extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->init();

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');

        $this->load->model('sales_model');
        $this->load->model('financeiro_model');
        $this->load->model('products_model');
    }

    public function index() {show_404();}

    private function view($qr_code) {

        $cobrancaFatura = $this->getCobrancaFatura($qr_code);

        $fatura = $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);
        $venda = $this->sales_model->getInvoiceByID($fatura->sale_id);

        if (!empty($venda)) {
            $this->data['venda'] =  $venda;
            $this->data['vendedor'] =  $this->site->getCompanyByID($venda->biller_id);
        }

        $this->data['fatura'] =  $fatura;
        $this->data['cobrancaFatura'] = $cobrancaFatura;
        $this->data['cliente'] = $this->site->getCompanyByID($fatura->pessoa);;

        if ($this->faturaAberta($cobrancaFatura)) {
            $this->load->view($this->theme . 'pix/pagamento', $this->data);
        } else if ($this->faturaPendente($cobrancaFatura)) {
            $this->load->view($this->theme . 'pix/confirmacao', $this->data);
        } else if ($this->faturaQuitada($cobrancaFatura) ) {

            if ($venda && $this->isEmailAindaNaoEnviado($venda->id)) $this->enviar_email($venda);

            $this->load->view($this->theme . 'pix/confirmacao', $this->data);
        } else if ($this->faturaCancelada($cobrancaFatura)) {
            $this->load->view($this->theme . 'pix/error', $this->data);
        } else {
            show_404();
        }
    }

    function pagamento($qr_code) {
        $this->view($qr_code);
    }

    function confirmacao($qr_code) {
        $this->view($qr_code);
    }

    public function error($qr_code) {
        $this->view($qr_code);
    }

    function consulta($integracao, $code) {

        //$this->FinanceiroService_model->baixarPagamentoIntegracao($integracao, $code);//TODO VERIFICAR SE O PROCESSAMENTO CONFIRMOU O PIX

        $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($code);

        if (!empty($cobrancaFatura)) {
            $this->sma->send_json($cobrancaFatura);
        }

        return [];
    }

    function enviar_email($venda)
    {
        $this->load->library('parser');

        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $customer = $this->site->getCompanyByID($venda->customer_id);

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
        } else {
            $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
        }

        $parse_data = array(
            'reference_number' => $venda->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->name,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
        );

        $message = $this->parser->parse_string($sale_temp, $parse_data, true);

        $subject = 'RESERVA ' . $this->Settings->site_name . ' CLIENTE ' . $customer->name;
        $to = $customer->email;
        $cc = $vendedor->email;

        $attachment = $this->pdf($venda->id, null, 'S');

        if ($this->Settings->is_email_queue) {
            $this->sma->send_email_queue($this->db, 'sales', 'create', $venda->id, $to, $subject, $message, null, null, $attachment, $cc, '');
        } else {
            $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, '');
            $this->SaleEventService_model->send_mail($venda->id);
            delete_files($attachment);
        }

    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";

        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento'] = $this->sales_model->getParcelasOrcamentoVenda($id);
        $this->data['inv'] = $inv;

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
        $return = $this->sales_model->getReturnBySID($id);

        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {

            if ($inv->sale_status == 'orcamento') {
                $showWatermarkImage = base_url('assets/images/orcamento2.png');
                /*
                $this->sma->generate_pdf_showWatermarkImage(
                    $html,
                    $name,
                    false,
                    $this->data['biller']->invoice_footer,
                    $margin_bottom = null,
                    $header = null,
                    $margin_top = null,
                    $orientation = 'P',
                    $showWatermarkImage);*/
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            } else {
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    private function getFatura($qr_code) {

        $cobrancaFatura = $this->getCobrancaFatura($qr_code);

        return $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);
    }

    private function getCobrancaFatura($qr_code) {

        if (stristr($qr_code, 'pay_') ) {
            $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($qr_code);
        } else {
            $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByQrCode($qr_code);
        }

        if (empty($cobrancaFatura)) show_404();

        return $cobrancaFatura;
    }

    private function cancelarFatura($fatura) {
        return $fatura->status == 'ABERTA';
    }

    private function cancelarVenda($fatura) {

        $venda = $this->sales_model->getInvoiceByID($fatura->sale_id);
        $cancelar = $fatura->sale_id && !empty($venda);

        if ($cancelar) return $venda->sale_status == 'faturada';

        return false;
    }

    private function faturaAberta($cobrancaFatura) {
        return $cobrancaFatura->status == 'ABERTA';
    }

    private function faturaCancelada($cobrancaFatura) {
        return $cobrancaFatura->status == 'CANCELADA';
    }

    private function faturaQuitada($cobrancaFatura) {
        return $cobrancaFatura->status == 'QUITADA';
    }

    private function faturaPendente($cobrancaFatura) {
        return $cobrancaFatura->status == 'PENDENTE';
    }

    private function isEmailEnviado($sale_id) {
        return $this->SaleEventService_model->isEmailEnviado($sale_id);
    }

    private function isEmailAindaNaoEnviado($sale_id) {
        return !$this->isEmailEnviado($sale_id);
    }

    private function init() {

        if ($this->input->get('token') != ''){
            $this->session->set_userdata(array('cnpjempresa' => $this->input->get('token')));
        } else {
            if ($this->session->userdata('cnpjempresa') == '') {
                $this->session->set_userdata(array('cnpjempresa' => $this->uri->segment(2)));
            }
        }

        $this->Settings = $this->site->get_setting();
        $this->data['Settings'] = $this->Settings;

        if ($this->Settings->status == 0) {
            redirect('login');
        }

        $sd = $this->site->getDateFormat($this->Settings->dateformat);

        $dateFormats = array(
            'js_sdate' => $sd->js,
            'php_sdate' => $sd->php,
            'mysq_sdate' => $sd->sql,
            'js_ldate' => $sd->js . ' hh:ii',
            'php_ldate' => $sd->php . ' H:i',
            'mysql_ldate' => $sd->sql . ' %H:%i'
        );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m']= $this->m;
        $this->data['v'] = $this->v;
    }
}