<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Orcamento extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->Settings = $this->site->get_setting();
        $this->data['Settings'] = $this->Settings;

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $sd = $this->site->getDateFormat($this->Settings->dateformat);

        $dateFormats = array(
            'js_sdate' => $sd->js,
            'php_sdate' => $sd->php,
            'mysq_sdate' => $sd->sql,
            'js_ldate' => $sd->js . ' hh:ii',
            'php_ldate' => $sd->php . ' H:i',
            'mysql_ldate' => $sd->sql . ' %H:%i'
        );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m']= $this->m;
        $this->data['v'] = $this->v;

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');

        $this->load->model('service/enum/TipoFaixaEtaria_model', 'TipoFaixaEtaria_model');

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/AppCompraService_model', 'AppCompraService_model');
        $this->load->model('service/VendaService_model', 'VendaService_model');

        $this->load->model('model/Cliente_model', 'Cliente_model');
        $this->load->model('model/AppCompra_model', 'AppCompra_model');

        $this->load->model('settings_model');
        $this->load->model('sales_model');
        $this->load->model('financeiro_model');
        $this->load->model('products_model');
    }

    public function index() {show_404();}

    public function andrevelho($id = NULL, $vendedor = NULL) {

        $this->session->set_userdata(array('cnpjempresa' => 'andrevelho'));

        $this->open($id, $vendedor);
    }

    public function evoluirviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'evoluirviagens'));

        $this->open($id, $vendedor);
    }

    public function augustusturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'augustusturismo'));

        $this->open($id, $vendedor);
    }

    public function yellowfun($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'yellowfun'));

        $this->open($id, $vendedor);
    }

    public function roadtrip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'roadtrip'));

        $this->open($id, $vendedor);
    }

    public function tripdajana($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tripdajana'));

        $this->open($id, $vendedor);
    }

    public function marcosexcursoeseeventos($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'marcosexcursoeseeventos'));

        $this->open($id, $vendedor);
    }

    public function glatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'glatur'));

        $this->open($id, $vendedor);
    }

    public function brotherstrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'brotherstrips'));

        $this->open($id, $vendedor);
    }

    public function happyviagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'happyviagem'));

        $this->open($id, $vendedor);
    }

    public function realitytur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'realitytur'));

        $this->open($id, $vendedor);
    }

    public function amigostur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'amigostur'));

        $this->open($id, $vendedor);
    }

    public function nessaturviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'nessaturviagens'));

        $this->open($id, $vendedor);
    }

    public function crisandtriptours($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'crisandtriptours'));

        $this->open($id, $vendedor);
    }

    public function pipaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'pipaturismo'));

        $this->open($id, $vendedor);
    }

    public function mandala($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mandala'));

        $this->open($id, $vendedor);
    }

    public function muralhatour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'muralhatour'));

        $this->open($id, $vendedor);
    }

    public function eliteagtur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'eliteagtur'));

        $this->open($id, $vendedor);
    }

    public function connecttrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'connecttrips'));

        $this->open($id, $vendedor);
    }

    public function dryexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dryexcursoes'));

        $this->open($id, $vendedor);
    }

    public function katatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'katatur'));

        $this->open($id, $vendedor);
    }

    public function lvtour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'lvtour'));

        $this->open($id, $vendedor);
    }

    public function plenitude($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'plenitude'));

        $this->open($id, $vendedor);
    }

    public function sicatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'sicatur'));

        $this->open($id, $vendedor);
    }

    public function demonstracaosagtur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'demonstracaosagtur'));

        $this->open($id, $vendedor);
    }

    public function thetravel($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'thetravel'));

        $this->open($id, $vendedor);
    }

    public function semprevix($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'semprevix'));

        $this->open($id, $vendedor);
    }

    public function vitatourviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vitatourviagens'));

        $this->open($id, $vendedor);
    }

    public function rootstrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rootstrips'));

        $this->open($id, $vendedor);
    }

    public function csviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'csviagens'));

        $this->open($id, $vendedor);
    }

    public function gusmaoviagemeturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'gusmaoviagemeturismo'));

        $this->open($id, $vendedor);
    }

    public function ablturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'ablturismo'));

        $this->open($id, $vendedor);
    }

    public function alcturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'alcturismo'));

        $this->open($id, $vendedor);
    }

    public function angulotravel($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'angulotravel'));

        $this->open($id, $vendedor);
    }

    public function msaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'msaturismo'));

        $this->open($id, $vendedor);
    }

    public function viagenscomguia($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viagenscomguia'));

        $this->open($id, $vendedor);
    }

    public function tripforever($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tripforever'));

        $this->open($id, $vendedor);
    }

    public function italiaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'italiaturismo'));

        $this->open($id, $vendedor);
    }

    public function penochao($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'penochao'));

        $this->open($id, $vendedor);
    }

    public function livreacessoturismoeviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'livreacessoturismoeviagens'));

        $this->open($id, $vendedor);
    }

    public function penaareia($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'penaareia'));

        $this->open($id, $vendedor);
    }

    public function enjoypromocoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'enjoypromocoes'));

        $this->open($id, $vendedor);
    }

    public function leehtourviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'leehtourviagens'));

        $this->open($id, $vendedor);
    }

    public function urbanos($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'urbanos'));

        $this->open($id, $vendedor);
    }

    public function legacyturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'legacyturismo'));

        $this->open($id, $vendedor);
    }

    public function thirolpasseios($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'thirolpasseios'));

        $this->open($id, $vendedor);
    }

    public function vivitour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vivitour'));

        $this->open($id, $vendedor);
    }

    public function belutur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'belutur'));

        $this->open($id, $vendedor);
    }

    public function viajecomigomm($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajecomigomm'));

        $this->open($id, $vendedor);
    }

    public function maistravel($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'maistravel'));

        $this->open($id, $vendedor);
    }

    public function boanovaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'boanovaturismo'));

        $this->open($id, $vendedor);
    }

    public function mpwturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mpwturismo'));

        $this->open($id, $vendedor);
    }

    public function fenixxturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'fenixxturismo'));

        $this->open($id, $vendedor);
    }

    public function brasilviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'brasilviagens'));

        $this->open($id, $vendedor);
    }

    public function cksturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'cksturismo'));

        $this->open($id, $vendedor);
    }

    public function vemcomigotrip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vemcomigotrip'));

        $this->open($id, $vendedor);
    }

    public function biahviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'biahviagens'));

        $this->open($id, $vendedor);
    }

    public function azevedoadventuretour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'azevedoadventuretour'));

        $this->open($id, $vendedor);
    }

    public function goldtourtrip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'goldtourtrip'));

        $this->open($id, $vendedor);
    }

    public function inovacaoviagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'inovacaoviagem'));

        $this->open($id, $vendedor);
    }

    public function bhexpressoturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'bhexpressoturismo'));

        $this->open($id, $vendedor);
    }

    public function lagunasulturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'lagunasulturismo'));

        $this->open($id, $vendedor);
    }

    public function formatrip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'formatrip'));

        $this->open($id, $vendedor);
    }

    public function agenciamonteiro($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'agenciamonteiro'));

        $this->open($id, $vendedor);
    }

    public function viajacky($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajacky'));

        $this->open($id, $vendedor);
    }

    public function turdans($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'turdans'));

        $this->open($id, $vendedor);
    }

    public function tripsampaexcursao($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tripsampaexcursao'));

        $this->open($id, $vendedor);
    }

    public function viajandobrasilafora($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajandobrasilafora'));

        $this->open($id, $vendedor);
    }

    public function lovefortrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'lovefortrips'));

        $this->open($id, $vendedor);
    }

    public function nanaturviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'nanaturviagens'));

        $this->open($id, $vendedor);
    }

    public function paulovasconcellos($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'paulovasconcellos'));

        $this->open($id, $vendedor);
    }

    public function riosexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'riosexcursoes'));

        $this->open($id, $vendedor);
    }

    public function alexturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'alexturismo'));

        $this->open($id, $vendedor);
    }

    public function grazzyexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'grazzyexcursoes'));

        $this->open($id, $vendedor);
    }

    public function lpexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'lpexcursoes'));

        $this->open($id, $vendedor);
    }

    public function mochilaselfie($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mochilaselfie'));

        $this->open($id, $vendedor);
    }

    public function brasiltripstour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'brasiltripstour'));

        $this->open($id, $vendedor);
    }

    public function aldeiaexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'aldeiaexcursoes'));

        $this->open($id, $vendedor);
    }

    public function manniadiviagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'manniadiviagem'));

        $this->open($id, $vendedor);
    }

    public function uppertrip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'uppertrip'));

        $this->open($id, $vendedor);
    }

    public function msturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'msturismo'));

        $this->open($id, $vendedor);
    }

    public function marcinhotur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'marcinhotur'));

        $this->open($id, $vendedor);
    }

    public function henritravel($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'henritravel'));

        $this->open($id, $vendedor);
    }

    public function hanstour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'hanstour'));

        $this->open($id, $vendedor);
    }

    public function gpviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'gpviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function eebturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'eebturismo'));

        $this->open($id, $vendedor);
    }

    public function vinitur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vinitur'));

        $this->open($id, $vendedor);
    }

    public function mariliaviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mariliaviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function vivalavidatripstour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vivalavidatripstour'));

        $this->open($id, $vendedor);
    }

    public function viajecomanegaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajecomanegaturismo'));

        $this->open($id, $vendedor);
    }

    public function tonapistatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tonapistatur'));

        $this->open($id, $vendedor);
    }

    public function guirro($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'guirro'));

        $this->open($id, $vendedor);
    }

    public function tatatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tatatur'));

        $this->open($id, $vendedor);
    }

    public function aeeviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'aeeviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function stylosturismoeviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'stylosturismoeviagens'));

        $this->open($id, $vendedor);
    }

    public function bellatour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'bellatour'));

        $this->open($id, $vendedor);
    }

    public function bmsadventure($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'bmsadventure'));

        $this->open($id, $vendedor);
    }

    public function oliveirastrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'oliveirastrips'));

        $this->open($id, $vendedor);
    }

    public function doisforasteiros($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'doisforasteiros'));

        $this->open($id, $vendedor);
    }

    public function diversaotour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'diversaotour'));

        $this->open($id, $vendedor);
    }

    public function excursoesdeinhamais($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'excursoesdeinhamais'));

        $this->open($id, $vendedor);
    }

    public function raiodesolexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'raiodesolexcursoes'));

        $this->open($id, $vendedor);
    }

    public function raiodesoltrilhas($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'raiodesoltrilhas'));

        $this->open($id, $vendedor);
    }

    public function egowtur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'egowtur'));

        $this->open($id, $vendedor);
    }

    public function kellustour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'kellustour'));

        $this->open($id, $vendedor);
    }

    public function rtdsouza($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rtdsouza'));

        $this->open($id, $vendedor);
    }

    public function excursoesbrunamoraes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'excursoesbrunamoraes'));

        $this->open($id, $vendedor);
    }

    public function triprapviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'triprapviagens'));

        $this->open($id, $vendedor);
    }

    public function plenasviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'plenasviagens'));

        $this->open($id, $vendedor);
    }

    public function mendesviagenstur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mendesviagenstur'));

        $this->open($id, $vendedor);
    }

    public function ganeshaadventureviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'ganeshaadventureviagens'));

        $this->open($id, $vendedor);
    }

    public function thaistourviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'thaistourviagens'));

        $this->open($id, $vendedor);
    }

    public function grturismoeviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'grturismoeviagens'));

        $this->open($id, $vendedor);
    }

    public function grturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'grturismo'));

        $this->open($id, $vendedor);
    }

    public function happyexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'happyexcursoes'));

        $this->open($id, $vendedor);
    }

    public function jaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'jaturismo'));

        $this->open($id, $vendedor);
    }

    public function magictour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'magictour'));

        $this->open($id, $vendedor);
    }

    public function peacetrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'peacetrips'));

        $this->open($id, $vendedor);
    }

    public function pajeturexcursoesmaraba($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'pajeturexcursoesmaraba'));

        $this->open($id, $vendedor);
    }

    public function viajatrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajatrips'));

        $this->open($id, $vendedor);
    }

    public function itaviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'itaviagens'));

        $this->open($id, $vendedor);
    }

    public function euphoriatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'euphoriatur'));

        $this->open($id, $vendedor);
    }

    public function viagensturismosp($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viagensturismosp'));

        $this->open($id, $vendedor);
    }

    public function rbiturismopasseioseeviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rbiturismopasseioseeviagens'));

        $this->open($id, $vendedor);
    }

    public function blessedtriip($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'blessedtriip'));

        $this->open($id, $vendedor);
    }

    public function dugeviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dugeviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function mieventoseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mieventoseturismo'));

        $this->open($id, $vendedor);
    }

    public function libertytur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'libertytur'));

        $this->open($id, $vendedor);
    }

    public function hkturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'hkturismo'));

        $this->open($id, $vendedor);
    }

    public function novotempoturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'novotempoturismo'));

        $this->open($id, $vendedor);
    }

    public function loniturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'loniturismo'));

        $this->open($id, $vendedor);
    }

    public function herediaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'herediaturismo'));

        $this->open($id, $vendedor);
    }

    public function excursoesroturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'excursoesroturismo'));

        $this->open($id, $vendedor);
    }

    public function waleturexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'waleturexcursoes'));

        $this->open($id, $vendedor);
    }

    public function javoltour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'javoltour'));

        $this->open($id, $vendedor);
    }

    public function maeefilhosturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'maeefilhosturismo'));

        $this->open($id, $vendedor);
    }

    public function eufuiviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'eufuiviagens'));

        $this->open($id, $vendedor);
    }

    public function prediletaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'prediletaturismo'));

        $this->open($id, $vendedor);
    }

    public function turismarco($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'turismarco'));

        $this->open($id, $vendedor);
    }

    public function helenagerraviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'helenagerraviagens'));

        $this->open($id, $vendedor);
    }

    public function abrangeturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'abrangeturismo'));

        $this->open($id, $vendedor);
    }

    public function felipeexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'felipeexcursoes'));

        $this->open($id, $vendedor);
    }

    public function riccioviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'riccioviagens'));

        $this->open($id, $vendedor);
    }

    public function marleneturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'marleneturismo'));

        $this->open($id, $vendedor);
    }

    public function sanmartinviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'sanmartinviagens'));

        $this->open($id, $vendedor);
    }

    public function espacoturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'espacoturismo'));

        $this->open($id, $vendedor);
    }

    public function carolviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'carolviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function excursoesnell($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'excursoesnell'));

        $this->open($id, $vendedor);
    }

    public function viajeseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajeseturismo'));

        $this->open($id, $vendedor);
    }

    public function upexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'upexcursoes'));

        $this->open($id, $vendedor);
    }

    public function turismojs($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'turismojs'));

        $this->open($id, $vendedor);
    }

    public function conectatour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'conectatour'));

        $this->open($id, $vendedor);
    }

    public function rbrturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rbrturismo'));

        $this->open($id, $vendedor);
    }

    public function alexturismomanhucu($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'alexturismomanhucu'));

        $this->open($id, $vendedor);
    }

    public function dallastur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dallastur'));

        $this->open($id, $vendedor);
    }

    public function companheirosroots($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'companheirosroots'));

        $this->open($id, $vendedor);
    }

    public function aletur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'aletur'));

        $this->open($id, $vendedor);
    }

    public function jptour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'jptour'));

        $this->open($id, $vendedor);
    }

    public function alfviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'alfviagens'));

        $this->open($id, $vendedor);
    }

    public function sergiotur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'sergiotur'));

        $this->open($id, $vendedor);
    }

    public function katourviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'katourviagens'));

        $this->open($id, $vendedor);
    }

    public function curtindoavidabrasilafora($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'curtindoavidabrasilafora'));

        $this->open($id, $vendedor);
    }

    public function luturismoviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'luturismoviagens'));

        $this->open($id, $vendedor);
    }

    public function jfaviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'jfaviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function infinitytripstour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'infinitytripstour'));

        $this->open($id, $vendedor);
    }

    public function vemcomagenteviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vemcomagenteviagens'));

        $this->open($id, $vendedor);
    }

    public function leletur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'leletur'));

        $this->open($id, $vendedor);
    }

    public function viajandopelomundotur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viajandopelomundotur'));

        $this->open($id, $vendedor);
    }

    public function barrilviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'barrilviagens'));

        $this->open($id, $vendedor);
    }

    public function guiademontanha($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'guiademontanha'));

        $this->open($id, $vendedor);
    }

    public function viagens4lovers($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'viagens4lovers'));

        $this->open($id, $vendedor);
    }

    public function riocariocatour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'riocariocatour'));

        $this->open($id, $vendedor);
    }

    public function qualityturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'qualityturismo'));

        $this->open($id, $vendedor);
    }

    public function riscandoomapa($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'riscandoomapa'));

        $this->open($id, $vendedor);
    }

    public function goodvibespelomundo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'goodvibespelomundo'));

        $this->open($id, $vendedor);
    }

    public function angelacardosoviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'angelacardosoviagens'));

        $this->open($id, $vendedor);
    }

    public function bdaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'bdaturismo'));

        $this->open($id, $vendedor);
    }

    public function dannytur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dannytur'));

        $this->open($id, $vendedor);
    }

    public function duplabagagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'duplabagagem'));

        $this->open($id, $vendedor);
    }

    public function carpediemviagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'carpediemviagem'));

        $this->open($id, $vendedor);
    }

    public function trip4fun($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'trip4fun'));

        $this->open($id, $vendedor);
    }

    public function partiutrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'partiutrips'));

        $this->open($id, $vendedor);
    }

    public function jalapaoeletrizanteturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'jalapaoeletrizanteturismo'));

        $this->open($id, $vendedor);
    }

    public function daviturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'daviturismo'));

        $this->open($id, $vendedor);
    }

    public function riosviagenseturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'riosviagenseturismo'));

        $this->open($id, $vendedor);
    }

    public function atmutur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'atmutur'));

        $this->open($id, $vendedor);
    }

    public function paradise($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'paradise'));

        $this->open($id, $vendedor);
    }

    public function gturviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'gturviagens'));

        $this->open($id, $vendedor);
    }

    public function depalmasturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'depalmasturismo'));

        $this->open($id, $vendedor);
    }

    public function classeviagemeturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'classeviagemeturismo'));

        $this->open($id, $vendedor);
    }

    public function ecoturismodivinopolis($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'ecoturismodivinopolis'));

        $this->open($id, $vendedor);
    }

    public function tfturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tfturismo'));

        $this->open($id, $vendedor);
    }

    public function luluturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'luluturismo'));

        $this->open($id, $vendedor);
    }

    public function newdreamstur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'newdreamstur'));

        $this->open($id, $vendedor);
    }

    public function fenixpasseioseaventuras($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'fenixpasseioseaventuras'));

        $this->open($id, $vendedor);
    }

    public function excomturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'excomturismo'));

        $this->open($id, $vendedor);
    }

    public function codigodeviagem($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'codigodeviagem'));

        $this->open($id, $vendedor);
    }

    public function enjoyviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'enjoyviagens'));

        $this->open($id, $vendedor);
    }

    public function dnexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dnexcursoes'));

        $this->open($id, $vendedor);
    }

    public function atmviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'atmviagens'));

        $this->open($id, $vendedor);
    }

    public function neiaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'neiaturismo'));

        $this->open($id, $vendedor);
    }

    public function limaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'limaturismo'));

        $this->open($id, $vendedor);
    }

    public function elshadaytur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'elshadaytur'));

        $this->open($id, $vendedor);
    }

    public function friendsviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'friendsviagens'));

        $this->open($id, $vendedor);
    }

    public function trictour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'trictour'));

        $this->open($id, $vendedor);
    }

    public function avesvoandoalto($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'avesvoandoalto'));

        $this->open($id, $vendedor);
    }

    public function moguitur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'moguitur'));

        $this->open($id, $vendedor);
    }

    public function saochicoturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'saochicoturismo'));

        $this->open($id, $vendedor);
    }

    public function tremdaalegriaviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'tremdaalegriaviagens'));

        $this->open($id, $vendedor);
    }

    public function rturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rturismo'));

        $this->open($id, $vendedor);
    }

    public function newtimetrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'newtimetrips'));

        $this->open($id, $vendedor);
    }

    public function gbtour($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'gbtour'));

        $this->open($id, $vendedor);
    }

    public function kltourviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'kltourviagens'));

        $this->open($id, $vendedor);
    }

    public function schimyteventos($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'schimyteventos'));

        $this->open($id, $vendedor);
    }

    public function daniviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'daniviagens'));

        $this->open($id, $vendedor);
    }

    public function busolesteturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'busolesteturismo'));

        $this->open($id, $vendedor);
    }

    public function iansturturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'iansturturismo'));

        $this->open($id, $vendedor);
    }

    public function natsturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'natsturismo'));

        $this->open($id, $vendedor);
    }

    public function mtkturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mtkturismo'));

        $this->open($id, $vendedor);
    }

    public function mroyalviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'mroyalviagens'));

        $this->open($id, $vendedor);
    }

    public function ventosdeluztrips($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'ventosdeluztrips'));

        $this->open($id, $vendedor);
    }

    public function paraviverturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'paraviverturismo'));

        $this->open($id, $vendedor);
    }

    public function dedeviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'dedeviagens'));

        $this->open($id, $vendedor);
    }

    public function nenaviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'nenaviagens'));

        $this->open($id, $vendedor);
    }

    public function maisviagensturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'maisviagensturismo'));

        $this->open($id, $vendedor);
    }

    public function azevedoturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'azevedoturismo'));

        $this->open($id, $vendedor);
    }

    public function vamosdepenaestrada($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'vamosdepenaestrada'));

        $this->open($id, $vendedor);
    }

    public function rosadosventos($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'rosadosventos'));

        $this->open($id, $vendedor);
    }

    public function slviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'slviagens'));

        $this->open($id, $vendedor);
    }

    public function sophiaturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'sophiaturismo'));

        $this->open($id, $vendedor);
    }

    public function aramastur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'aramastur'));

        $this->open($id, $vendedor);
    }

    public function jvexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'jvexcursoes'));

        $this->open($id, $vendedor);
    }

    public function leotavaresturismo($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'leotavaresturismo'));

        $this->open($id, $vendedor);
    }

    public function peguesuamochila($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'peguesuamochila'));

        $this->open($id, $vendedor);
    }

    public function romeroexcursoes($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'romeroexcursoes'));

        $this->open($id, $vendedor);
    }

    public function apviagens($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'apviagens'));

        $this->open($id, $vendedor);
    }

    public function open($id, $vendedor) {

        try {

            $programacao = $this->AgendaViagemService_model->getProgramacaoById($id);

            if (!$id || !$vendedor || !$programacao) show_404();

            $this->data['configuracaoGeral'] = $this->site->get_setting();

            $this->data['programacao'] = $programacao;

            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobrancaExibirLinkCompra();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamentoExibirLink();
            $this->data['vendedor'] = $this->site->getCompanyByID($vendedor);
            $this->data['produto'] = $this->site->getProductByID($programacao->getProduto());

            $this->data['embarques'] = $this->ProdutoRepository_model->getLocaisEmbarque($programacao->getProduto());
            $this->data['combo_items'] = $this->products_model->getProductComboItems($programacao->getProduto());

            $this->data['faixaEtariaValoresAdulto'] = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto(), 'adulto');
            $this->data['faixaEtariaValoresCrianca'] = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto(), 'crianca');
            $this->data['faixaEtariaValoresBebe'] = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto(), 'bebe');

            $this->data['tiposHospedagem'] = $this->ProdutoRepository_model->getTipoHospedagem($programacao->getProduto());
            $this->data['tiposHospedagemFaixaEtaria'] = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagem($programacao->getProduto());

            $this->data['taxas'] = $this->settings_model->getTaxaSettings();
            $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();

            if ($programacao->getTotalDisponvel() > 0) $this->load->view($this->theme . 'appcompra/orcamento', $this->data);
            else $this->load->view($this->theme . 'appcompra/esgotado', $this->data);

        } catch (Exception $exception) {
            show_404();
        }
    }

    public function add() {

        $clientePrincipal = new Cliente_model();
        $appModel = new AppCompra_model();

        $nome = $this->input->post('nome');
        $sexo = $this->input->post('sexo');
        $cpf = $this->input->post('cpf');
        $rg = $this->input->post('rg');
        $celular = $this->input->post('celular');
        $telefone = $this->input->post('telefone');
        $telefone_emergencia = $this->input->post('telefone_emergencia');
        $alergia_medicamento = $this->input->post('alergia_medicamento');
        $email = $this->input->post('email');
        $dia = $this->input->post('dia');
        $mes = $this->input->post('mes');
        $ano = $this->input->post('ano');

        $clientePrincipal->setName($nome);
        $clientePrincipal->setSexo($sexo);
        $clientePrincipal->setVatNo($cpf);
        $clientePrincipal->setCf1($rg);
        $clientePrincipal->setCf5($celular);
        $clientePrincipal->setPhone('');
        $clientePrincipal->setTelefoneEmergencia($telefone_emergencia);
        $clientePrincipal->setEmail($email);
        $clientePrincipal->setAlergiaMedicamento($alergia_medicamento);
        $clientePrincipal->setDataAniversario($ano.'-'.$mes.'-'.$dia);
        $clientePrincipal->setTipoFaixaEtaria(TipoFaixaEtaria_model::ADULTO);

        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));

        $appModel->cliente = $clientePrincipal;
        $appModel->localEmbarque = $this->input->post('localEmbarque');
        $appModel->tipoHospedagem = $this->input->post('tipoHospedagem');

        $appModel->tipoCobranca = $this->input->post('tipoCobranca');
        $appModel->condicaoPagamento = $this->input->post('condicaoPagamento');
        $appModel->observacaoAdicional = $this->input->post('observacao');

        $appModel->total = $this->input->post('subTotal');
        $appModel->totalComTaxas = $this->input->post('subTotalTaxas');

        $appModel->valorHospedagemAdulto = $this->input->post('valorHospedagemAdulto');
        $appModel->valorHospedagemCrianca = $this->input->post('valorHospedagemCrianca');
        $appModel->valorHospedagemBebe = $this->input->post('valorHospedagemBebe');

        $appModel->valorPacoteAdulto = $this->input->post('valorPacoteAdulto');
        $appModel->valorPacoteCrianca = $this->input->post('valorPacoteCrianca');
        $appModel->valorPacoteBebe = $this->input->post('valorPacoteBebe');

        $appModel->vendedorId = $this->input->post('vendedor');

        $appModel->programacao =  $programacao;
        $appModel->produto = $this->site->getProductByID($programacao->produto);

        $appModel->vencimentosParcela = $this->input->post('dtVencimentos');
        $appModel->valorVencimentosParcelas = $this->input->post('valorVencimentos');
        $appModel->descontos = $this->input->post('descontos');
        $appModel->tiposCobranca = $this->input->post('tiposCobranca');
        $appModel->movimentadores = $this->input->post('movimentadores');

        $appModel->qtdBebesColo =  $this->input->post('qtdBebesColo');
        $appModel->qtdCriancas =  $this->input->post('qtdCriancas');
        $appModel->qtdAdultos =  $this->input->post('qtdAdultos');

        $senderHash = htmlspecialchars($this->input->post('senderHash', true));
        $creditCardToken = htmlspecialchars($this->input->post('creditCardToken', true));
        $parcelas = $this->input->post('parcelas', true);
        $cardName = $this->input->post('name', true);

        $appModel->senderHash = $senderHash;
        $appModel->creditCardToken = $creditCardToken;
        $appModel->parcelas = $parcelas;
        $appModel->cardName = $cardName;

        $appModel->isOrcamento = true;

        $i = isset($_POST['nomeDependente']) ? sizeof($_POST['nomeDependente']) : 0;

        for ($r = 0; $r < $i; $r++) {
            $dependente = new Cliente_model();
            $nomeDependente = $_POST['nomeDependente'][$r];

            if ($nomeDependente != '') {

                $dependente->setName($nomeDependente);
                $dependente->setSexo($_POST['sexoDependente'][$r]);
                $dependente->setVatNo($_POST['cpfDependente'][$r]);
                $dependente->setCf1($_POST['rgDependente'][$r]);
                $dependente->setCf5($_POST['celularDependente'][$r]);
                $dependente->setTelefoneEmergencia($_POST['telefone_emergenciaDependente'][$r]);
                $dependente->setAlergiaMedicamento($_POST['alergia_medicamentoDependente'][$r]);
                $dependente->setTipoFaixaEtaria(($_POST['tipoFaixaEtaria'][$r]));

                $dependente->setEmail('');
                $dependente->setPhone('');
                $dependente->setCity('');

                $dia = $_POST['diaDependente'][$r];
                $mes = $_POST['mesDependente'][$r];
                $ano = $_POST['anoDependente'][$r];

                $dependente->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                $appModel->adicionarDependente($dependente);
            }
        }

        try {

            $vendaId = $this->AppCompraService_model->salvar($appModel);
            $venda = $this->sales_model->getInvoiceByID($vendaId);

            if ($clientePrincipal->getEmail() != '') {
                $this->enviar_email($venda, $clientePrincipal->getEmail());
            }

            redirect(base_url()."orcamento/confirmacaoDeReserva/".$vendaId.'/'.$programacao->id.'/'.$this->input->post('vendedor').'?token='.$this->session->userdata('cnpjempresa'));

        } catch (Exception $exception) {

            $vendedor = $this->site->getCompanyByID($this->input->post('vendedor'));
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));

            $this->data['erroProcessaPagamento'] = $exception->getMessage();
            $this->data['produto'] = $this->site->getProductByID($programacao->produto);

            $this->data['vendedor'] = $vendedor;
            $this->data['programacao'] = $programacao;
            $this->data['configuracaoGeral'] = $this->site->get_setting();

            $this->load->view($this->theme . 'appcompra/erro_pagamento_cartao', $this->data);
        }
    }

    function enviar_email($venda, $email)
    {
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $configuracaoGeral = $this->site->get_setting();

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
        } else {
            $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
        }

        $customer = $this->site->getCompanyByID($venda->customer_id);
        $this->load->library('parser');

        $parse_data = array(
            'reference_number' => $venda->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->name,
            'site_link' => base_url(),
            'site_name' => $configuracaoGeral->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $configuracaoGeral->logo2 . '" alt="' . $configuracaoGeral->site_name . '"/>',
        );

        $message = $this->parser->parse_string($sale_temp, $parse_data);

        $subject = '🚌 🌎 RESERVA DE PASSAGEM ' . $configuracaoGeral->site_name . ' CLIENTE ' . $customer->name;
        $to = $email . ',' . $vendedor->email;

        $attachment = $this->pdf($venda->id, null, 'S');

        $this->sma->send_email($to, $subject, $message, null, null, $attachment, '', '');
    }

    function confirmacaoDeReserva($vendaId,$programacaoId, $vendedorId) {

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $vendedor = $this->site->getCompanyByID($vendedorId);

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);
        $tipoCobranca =  $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;
        $this->data['vendedor'] = $vendedor;
        $this->data['faturas'] = $faturas;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['configuracaoGeral'] = $this->site->get_setting();

        $this->load->view($this->theme . 'appcompra/confirmacao_orcamento', $this->data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";

        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento'] = $this->sales_model->getParcelasOrcamentoVenda($id);
        $this->data['inv'] = $inv;

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
        $return  = $this->sales_model->getReturnBySID($id);

        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'sales/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {

            if ($inv->sale_status == 'orcamento') {
                $showWatermarkImage = base_url('assets/images/orcamento.png');

                /*
                $this->sma->generate_pdf_showWatermarkImage(
                    $html,
                    $name,
                    false,
                    $this->data['biller']->invoice_footer,
                    $margin_bottom = null,
                    $header = null,
                    $margin_top = null,
                    $orientation = 'P',
                    $showWatermarkImage);*/
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            } else {
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    public function confirmacao($programacaoId, $vendedor, $vendaId)  {

        $programacao = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $vendedor = $this->site->getCompanyByID($vendedor);
        $tipoCobranca =  $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['configuracaoGeral'] = $this->site->get_setting();
        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;

        $this->data['produto'] = $this->site->getProductByID($programacao->getProduto());
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['vendedor'] =  $vendedor;

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
        $cobranca = NULL;

        foreach ($faturas as $fat) {
            $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
        }

        $this->data['cobranca'] = $cobranca;
        $this->data['fatura'] = $fat;

        if ($tipoCobranca->tipo == 'carne_cartao_transparent') {//TODO CARTAO CHECKIN TRANSPARTENTE
            //sleep(1);//TODO AGUARDAR PAGSEGURO CONFIRMAR O PAGAMENTO

            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
            $cobranca = NULL;

            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
            }

            if ($fat->status == 'CANCELADA') {
                $this->load->view($this->theme . 'appcompra/finalizacao', $this->data);
            } else if ($fat->status == 'ABERTA') {
                $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
            } else if($fat->status == 'PAGO') {
                $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
            }

        } else {
            $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
        }
    }

    function abrirWhatsAppConfirmacao($vendedor, $venda, $fatura, $cobranca) {

        $configuracao = $this->site->get_setting();
        $phone = $vendedor->phone;
        $cliente = $this->site->getCompanyByID($venda->customer_id);

        $phone = str_replace ( '(', '', str_replace ( ')', '', $phone));
        $phone = str_replace ( '-', '',  $phone);
        $phone = str_replace ( ' ', '',  $phone);

        $inv_items = $this->sales_model->getAllInvoiceItems($venda->id);

        foreach ($inv_items as $item) {
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);
            $produto = $this->site->getProductByID($programacao->produto);
            $localEmbarque = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($item->product_id, $item->localEmbarqueId);
        }
        $text = '';

        $text .= '*👋🏻 Olá me chamo '.$cliente->name.'*';
        $text .= '%0A';
        $text .= '%0AAcabo de preencher o link para concluir minha reserva.';
        $text .= '%0A';

        if ($venda->total_items > 0) {
            $text .= '%0A*✔️ Passageiro(s)*';
            foreach ($inv_items as $item) {
                $dependente = $this->site->getCompanyByID($item->customerClient);

                $text .= '%0A✅ Nome:  *'.$dependente->name.' ['.$dependente->tipoFaixaEtaria.']*';
                $text .= '%0A📞 Celular:  '.$dependente->phone.' '.$dependente->cf5;
                $text .= '%0A📧 E-mail '.$dependente->email;
                $text .= '%0A🎫 CPF '.$dependente->vat_no;
                $text .= '%0A';
            }
        }

        $text .= '%0A*✔️ Dados da Viagem*';
        $text .= '%0A📌'.$produto->name;

        if ($produto->category_id != 14) {
            $text .= '%0A📆 Data Saída: ' . $this->sma->hrsd($programacao->dataSaida);
            $text .= '%0A📆 Data Retorno: ' . $this->sma->hrsd($programacao->dataRetorno) . ' ' . date('H:i', strtotime($programacao->horaRetorno));
        }

        $text .= '%0A🚍 Local Embarque '.$localEmbarque->name;

        if (date('H:i', strtotime($localEmbarque->horaEmbarque)) != '00:00' ) {
            $text .= ' Previsão de Horário de Embarque '.date('H:i', strtotime($localEmbarque->horaEmbarque));
        }

        $text .= '%0A';

        $text .= '%0A*✔️ Dados da Reserva*';
        $text .= '%0A🔖 Código '.$venda->reference_no;
        $text .= '%0A🛍️ Emissao '.$this->sma->hrld($venda->date);
        $text .= '%0A💰 Total da compra R$ '.$this->sma->formatMoney($venda->total);
        $text .= '%0A👥 Total de Passageiros '.$venda->total_items.' pessoa(as)';

        if ($configuracao->frase_site != '') {
            $text .= '%0A*'.$configuracao->frase_site.'*';
        } else {
            $text .= '%0A*🚌 Seu Sonho. Sua Viagem. Nós Realizamos.✈️*';
        }

        $eols = array(",","\n","\r","\r\n");

        $text = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('°', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('&', $this->getEspacoHtmlWhatsApp(), $text);

        $text = str_replace($eols,$this->getEspacoHtmlWhatsApp(),$text);

        header('Location: https://api.whatsapp.com/send?phone=55'.trim($phone).'&text='.$text);exit;
    }

    private function  getEspacoHtmlWhatsApp() {
        return '%20';
    }

    public function getDependenteAdulto() {
        $contador = $this->input->get('contador');
        $this->getDependentes($contador, 'Adulto');
    }

    public function getDependenteCriancas() {
        $contador = $this->input->get('contador');
        $this->getDependentes($contador, 'Criança');
    }

    public function getDependenteBebe() {
        $contador = $this->input->get('contador');

        $html = '<h3 class="main_question"><strong>3/6</strong>Dados do '.$contador.'º - Bebê de colo</h3>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-6">
                                <input type="tel" class="form-control" name="cpfDependente[]" placeholder="CPF" class="required">
                                <span style="color:red;">Deixe em branco se não possuir cpf</span>
                                <input type="hidden" name="tipoFaixaEtaria[]" value="bebe" />
                            </div>
                            <div class="col-6">
                                <input type="tel" class="form-control" name="rgDependente[]" placeholder="R.G / Certidão de Nascimento" class="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control" name="nomeDependente[]" placeholder="Nome Completo" class="required" required="required">
                        </div>
                        <div class="col-6">
                            <input type="tel" class="form-control" name="celularDependente[]" placeholder="Celular com (DDD)">                
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group radio_input">
                                <label class="container_radio">Masculino
                                    <input type="radio" name="sexoDependente[]" value="MASCULINO" class="required">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container_radio">Feminino
                                    <input type="radio" name="sexoDependente[]" value="FEMININO" class="required">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>';
        echo $html;
    }

    private function getDependentes($contador, $tipo) {

        $html = '<h3 class="main_question"><strong>3/6</strong>Dados do '.$contador.'º Passageiro - '.$tipo.'</h3>
                    <div class="form-group">
                        <div class="row">
                           <div class="col-6">';

        if ($tipo == 'Criança') {
            $html .= '            <input type="tel" class="form-control" name="cpfDependente[]" placeholder="CPF"> 
                                    <span style="color:red;">Deixe em branco se não possuir cpf</span>
                                    <input type="hidden" name="tipoFaixaEtaria[]" value="crianca" />';
        } else {
            $html .= '            <input type="tel" class="form-control" name="cpfDependente[]" placeholder="CPF" class="required" required="required">
                                  <input type="hidden" name="tipoFaixaEtaria[]" value="adulto" />';
        }

        $html .= '          </div>
                            <div class="col-6">
                                <input type="tel" class="form-control" name="rgDependente[]" placeholder="R.G / Certidão de Nascimento" class="required" required="required">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <input type="text" class="form-control" name="nomeDependente[]" placeholder="Nome Completo" class="required" required="required">
                        </div>
                        <div class="col-6">
                            <input type="tel" class="form-control" name="celularDependente[]" placeholder="Celular com (DDD)">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group radio_input">
                                <label class="container_radio">Masculino
                                    <input type="radio" name="sexoDependente[]" value="MASCULINO" class="required">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="container_radio">Feminino
                                    <input type="radio" name="sexoDependente[]" value="FEMININO" class="required">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                    </div>';
        echo $html;
    }

    public function condicoes() {

        $tipoCobrancaId = $this->input->get('tipoCobrancaId');
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->get('programacaoId'));
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($tipoCobrancaId);
        $tipo = $tipoCobranca->tipo;

        $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId);
        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento =  date('Y-m-d', strtotime("-".$diasMaximoPagamentoAntesViagem." day", strtotime($programacao->dataSaida)));
        $dataMaximaParaRealizarPagamento = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao);
        $dtVencimento = null;
        $maximoParcelas = 0;

        for ($i = 0; $i < 15; $i++) {
            $dtVencimento = $this->getDataProximoVencimentoCondicoes($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento);
            if ($dtVencimento != $dataMaximaParaRealizarPagamento && strtotime($dtVencimento) < strtotime($dataMaximaParaRealizarPagamento)) {
                $maximoParcelas++;
            } else {
                if (date('Y-m', strtotime( $dtVencimento)) == date('Y-m', strtotime($dataMaximaParaRealizarPagamento))) {
                    $maximoParcelas++;
                }
            }
        }

        if ($maximoParcelas == 0) $maximoParcelas = 1;

        if ($tipo == 'carne_cartao' ||
            $tipo == 'cartao'||
            //$tipo == 'carne'||
            $i == 0) {
            $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId);
        } else {
            $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcelas);
        }

        $this->sma->send_json($condicoesPagamento);
    }

    public function getParcelamento() {

        $condicaoPagamentoId = $this->input->get('condicaoPagamentoId');
        $tipoCobrancaId = $this->input->get('tipoCobrancaId');
        $subTotal = $this->input->get('subTotal');

        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->get('programacaoId'));
        $condicaoPagamento = $this->financeiro_model->getCondicaoPagamentoById($condicaoPagamentoId);

        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($tipoCobrancaId);
        $totalParcelas = $condicaoPagamento != null ? $condicaoPagamento->parcelas : 1;
        $tipo = $tipoCobranca->tipo;

        $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId);
        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento =  date('Y-m-d', strtotime("-".$diasMaximoPagamentoAntesViagem." day", strtotime($programacao->dataSaida)));
        $dataMaximaParaRealizarPagamento = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao);

        $memorizaPrimeiroVencimento = null;
        $dtVencimento = null;
        $html = '';

        for ($i = 0; $i < $totalParcelas; $i++) {

            $readonly = '';

            $dtVencimento = $this->getDataProximoVencimento($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento);

            if ($i == 0) {
                $memorizaPrimeiroVencimento = $dtVencimento;
                $readonly = 'readonly="readonly"';
            }

            if ($tipo == 'carne') {
                $readonly = 'readonly="readonly"';
            }

            $html .= '
               <div class="row">
                    <div class="col-1" style="line-height: 35px;">'.($i + 1).'X</div>
                    <div class="col-7"><input type="date" original="' . $dtVencimento . '" diasAntesDaViagem="' . $diasMaximoPagamentoAntesViagem . '" dataMaximaPagamento="' . $dataMaximaParaRealizarPagamento . '" class="form-control" value="' . $dtVencimento . '" name="dtVencimentos[]" '.$readonly.' required="required"></div>
                    <div class="col-4" style="line-height: 35px;">
                        <input type="hidden" value="' . $subTotal / $totalParcelas . '" name="valorVencimentos[]">
                        <input type="hidden" value="' . $tipoCobranca->id . '" name="tiposCobranca[]">
                        <input type="hidden" value="' . $tipoCobranca->conta . '" name="movimentadores[]">
                        <input type="hidden" value="0" name="descontos[]">
                        ' . $this->sma->formatMoney($subTotal / $totalParcelas) . '
                    </div>
              </div>';
        }

        $html = $this->getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem,  $programacao, $totalParcelas);

        echo $html;
    }

    public function getDataProximoVencimentoCondicoes($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento) {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+".$diasAvancaPrimeiroVencimento." day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento
            $dtProximoVencimento =  date('Y-m-d', strtotime("-".$diasAvancaPrimeiroVencimento." day", strtotime($dtProximoVencimento)));//tira obriagao do dia minimo
        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }
        return $dtProximoVencimento;
    }

    public function getDataProximoVencimento($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento) {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+".$diasAvancaPrimeiroVencimento." day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento
            $dtProximoVencimento =  date('Y-m-d', strtotime("-".$diasAvancaPrimeiroVencimento." day", strtotime($dtProximoVencimento)));//tira obriagao do dia minimo
        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }

        //Se a data maxima para pagar ja passou entao atribuir a proxima data
        if (strtotime($dtMaximaDePagamento) < strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = $dtMaximaDePagamento;
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = date('Y-m-d');
        }

        return $dtProximoVencimento;
    }

    private function getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao) {

        if ($diasMaximoPagamentoAntesViagem > 0) {
            $dtMaximaDePagamento =  date('Y-m-d', strtotime("-".$diasMaximoPagamentoAntesViagem." day", strtotime($programacao->dataSaida)));
        } else {
            $dtMaximaDePagamento = $programacao->dataSaida;//data da saida da viagem
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtMaximaDePagamento)) {
            $dtMaximaDePagamento = date('Y-m-d');
        }

        return $dtMaximaDePagamento;
    }

    private function getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem, $programacao, $totalParcelas) {

        $html .= '<div class="row" style="margin-top: 10px;">';
        $html .= '<div class="col-12" style="color: #f43e61;">';

        if ($diasMaximoPagamentoAntesViagem > 0) {

            $dtMaximaDePagamento =  date('Y-m-d', strtotime("-".$diasMaximoPagamentoAntesViagem." day", strtotime($programacao->dataSaida)));

            if (strtotime(date('Y-m-d')) < strtotime($dtMaximaDePagamento)) {

                if ($totalParcelas > 1) {
                    $html .= '<b>*Atenção:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                    $html .= '<b>*Atenção: Apenas</b> após o pagamento da primeira parcela (do sinal) com vencimento mínimo para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b>
                                sua reserva <b>será  confirmada.</b> O <b>NÃO pagamento do sinal</b> poderá acarretar no <b>cancelamento</b> da reserva.';

                    $html .= '<br/><br/><b>*Atenção: </b>No caso de parcelamento o pagamento total deverá ser <b>quitado até ' . $diasMaximoPagamentoAntesViagem . ' dias antes da viagem</b>. Data máxima para pagamento: <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>.';
                } else {
                    $html .= '<b>*Atenção: Apenas</b> após o pagamento da parcela com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b>
                                sua reserva <b>será  confirmada.</b> O <b>NÃO pagamento do sinal</b> poderá acarretar no <b>cancelamento</b> da reserva. <br/><b>*Atenção:</b> Data máxima para pagamento: <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>';
                }
            } else {
                $html .= '<br/><br/><b>*Atenção: </b>A viagem saíra ainda esta semana e a <b>quitação total do débito deverá ser feita ainda hoje para confirmação da compra. <br/><b>*Atenção:</b> Data máxima para pagamento: <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b></b>';
            }

        } else {
            if ($totalParcelas > 1) {
                $html .= '<b>*Atenção:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                $html .= '<b>Apenas após o pagamento</b> da primeira parcela (sinal) com vencimento mínimo para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b>
                            sua reserva <b>será  confirmada.</b> O <b>não pagamento do sinal</b> poderá acarretar no <b>cancelamento</b> da reserva.';
            } else {
                $html .= '<b>*Atenção: Apenas</b> após o pagamento da parcela com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b>
                                sua reserva <b>será  confirmada.</b> O <b>NÃO pagamento do sinal</b> poderá acarretar no <b>cancelamento</b> da reserva.';
            }
        }

        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function vagas() {
        $programacaoEstoque = $this->AgendaViagemService_model->getProgramacaoById($this->input->get('programacaoId'));
        echo $programacaoEstoque->getTotalDisponvel();
    }
}