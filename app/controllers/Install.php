<?php defined('BASEPATH') or exit('No direct script access allowed');

class Install extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('service/InstallService_model', 'InstallService_model');
        $this->load->model('dto/InstallDTO_model', 'InstallDTO_model');

        $this->lang->load('install', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->upload_path = 'assets/uploads/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '16384';
    }

    function install($customerID) {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('install')));
        $meta = array('page_title' => lang('install'), 'bc' => $bc);
        $customer = $this->site->getCompanyByID($customerID);

        if ($this->InstallService_model->databaseExists($customer->name_db)) {
            $this->session->set_flashdata('info', lang("client_exists") . $this->getDadosAcessso($customer));
        }

        $this->data['customer'] = $customer;

        $this->page_construct('install/install', $meta, $this->data);
    }

    public function install_action($customerID)
    {
        try {

            $this->form_validation->set_rules('database', lang("database"), 'required|min_length[3]');
            $this->form_validation->set_rules('site_logo', lang("site_logo"), 'xss_clean');

            if ($this->form_validation->run() == true) {

                $customer = $this->site->getCompanyByID($customerID);

                $installDTO = new InstallDTO_model();
                $installDTO->db_name = $this->input->post('database');
                $installDTO->theme_color = $this->input->post('theme_color');
                $installDTO->nome_empresa = $customer->name;
                $installDTO->customer_id = $customer->id;
                $installDTO->logo = 'sagtur.png';
                $installDTO->criar_subdominio = $this->input->post('criar_subdominio') == 1;

                $this->InstallService_model->install($installDTO);

                $this->session->set_flashdata('info', lang("install_success") . $this->getDadosAcessso($customer));

                redirect("install/install/".$customerID);
            }

        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect("install/install/".$customerID);
        }

    }

    private function getDadosAcessso($customer): string
    {
        $cnpj = preg_replace('/[^0-9]/', '', $customer->vat_no);
        $link = '';

        $link .= '<br/><br/><b>Link de Acesso ao Painel de Controle:</b><br/><br/>
                        https://sistema.sagtur.com.br/login?CNPJ='.$cnpj.'<br/>
                        Responsável: '.$customer->nome_responsavel.'<br/>
                        Usuário: '.$customer->email.'<br/>
                        Senha: Trocar001<br/> ';

        $link .= '<br/><br/><b>Link de Acesso ao Site:</b><br/>
                        https://'.$customer->name_db.'.suareservaonline.com.br<br/>';

        $link .= '
                        <b>ACESSO AO SISTEMA:</b>
                        <form action="https://sistema.sagtur.com.br/auth/login" method="post" accept-charset="utf-8" target="_blank">
                            <input type="hidden" name="token" value="d774f9a4314a1220962db98e187aa972" />
                            <input type="hidden" name="cnpjempresa" value="'.$cnpj.'" />
                            <input type="hidden" name="identity" value="andre@resultatec.com.br" />
                            <input type="hidden" name="password" value="2078404abC@!" />
                            <button type="submit"> '.$customer->name.' </button>
                        </form>';

        return $link;
    }

    protected function logoteste()
    {
        if ($_FILES['site_logo']['size'] > 0) {

            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path . 'logos/';
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = 500;
            $config['max_height'] = 500;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('site_logo')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            return $this->upload->file_name;
        }

        return NULL;
    }

}?>