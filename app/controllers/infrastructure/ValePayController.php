<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ValePayController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository_model');

        $this->load->model('financeiro_model');

        if ($this->session->userdata('cnpjempresa') == '')  $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
    }

    function index()
    {
        show_404();
    }


    public function notification()
    {

        $json_body = file_get_contents('php://input');

        $obj = json_decode($json_body);

        if (!empty($obj->uuid)) {

            echo  $this->session->userdata('cnpjempresa').'<br/>';

            $uuid = 'pay_'.$obj->uuid;

            echo $uuid;

            $cobrancaSistema = $this->FinanceiroRepository_model->buscaCobrancaByCodeIntegracao($uuid);

            print_r($cobrancaSistema);

            if (!empty($cobrancaSistema)) {
                $this->financeiro_model->edit('fatura_cobranca', array('log' => $json_body),'id', $cobrancaSistema->id);
                $this->FinanceiroService_model->baixarPagamentoIntegracao('valepay', $uuid);
            }

        } else {
            echo '0';
        }

        return TRUE;
    }

}