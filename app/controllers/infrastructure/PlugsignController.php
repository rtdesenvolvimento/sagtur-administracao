<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PlugsignController extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('cnpjempresa') == '') {
            $this->session->set_userdata(array('cnpjempresa' => $_GET['empresa']));
        }

        $this->load->model('service/DocumentService_model', 'DocumentService_model');
    }

    function index()
    {
        show_404();
    }

    public function notification()
    {

        echo 'listening...';

        $json_body = file_get_contents('php://input');

        $obj = json_decode($json_body);

        if (!empty($obj->document_key) && !empty($obj->signing_key)) {
            $this->DocumentService_model->webhook($obj);
        } else {
            return FALSE;
        }

        return TRUE;
    }

}