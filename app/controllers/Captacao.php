<?php defined('BASEPATH') or exit('No direct script access allowed');

class Captacao extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('repository/CaptacaoRespository_model', 'CaptacaoRespository_model');
        $this->load->model('repository/InteresseCaptacaoRepository_model', 'InteresseCaptacaoRepository_model');

        $this->lang->load('captacoes', $this->Settings->user_language);
    }

    public function index() {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('captacoes')));
        $meta = array('page_title' => lang('captacoes'), 'bc' => $bc);

        $this->data['billers']      = $this->site->getAllCompanies('biller');
        $this->data['interesses']   = $this->InteresseCaptacaoRepository_model->getAll();

        $this->page_construct('captacao/index', $meta, $this->data);
    }

    public function getCaptacoes()
    {
        $this->load->library('datatables');
        $attrib = array('target' => 'newBlank');

        $whatsapp_send_message_link = anchor('captacao/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);
        $read_link = "<a href='#' class='po' title='" . lang("read_capture") . "' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger cp-read' href='" . site_url('captacao/read/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-envelope'></i> "
            . lang('read_capture') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li class="divider"></li>
                    <li>' . $read_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("captacao.id as id,
                date, 
                captacao.reference_no, 
                captacao.name, 
                cell_phone, 
                interesse_captacao.name as interesse,
                concat(sma_products.name, ' ', DATE_FORMAT(sma_agenda_viagem.dataSaida, '%d/%m/%Y')) as product_name,
                plantao.name as origem, 
                forma_atendimento.name as forma_atendimento, 
                companies.name as biller,
                captacao.status,
                departments.name as department, 
                captacao.read")
            ->from("captacao")
            ->join('companies', 'companies.id = captacao.biller_id', 'left')
            ->join('plantao', 'plantao.id = captacao.plantao_id')
            ->join('departments', 'departments.id = captacao.department_id', 'left')
            ->join('forma_atendimento', 'forma_atendimento.id = captacao.forma_atendimento_id')
            ->join('interesse_captacao', 'interesse_captacao.id = captacao.interesse_id')
            ->join('agenda_viagem', 'agenda_viagem.id = captacao.programacao_id', 'left')
            ->join('products', 'products.id = captacao.product_id', 'left');

        $this->datatables->where('read', $this->input->post('filterRead'));

        if ($this->input->post('billerFilter')) {
            $this->datatables->where('captacao.biller_id', $this->input->post('billerFilter'));
        }

        if ($this->input->post('filteInteresseCaptacao')) {
            $this->datatables->where('captacao.interesse_id', $this->input->post('filteInteresseCaptacao'));
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('captacao.biller_id', $this->session->userdata('biller_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function whatsapp_message($id = null) {

        $captacao = $this->CaptacaoRespository_model->getByID($id);

        $phone = str_replace('(', '', str_replace(')', '', $captacao->cell_phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        header('Location: https://api.whatsapp.com/send?phone=55' . trim($phone));
    }

    public function read($id)
    {
        if ($this->CaptacaoRespository_model->read($id)) {
            echo lang("Captacao Lida Com Sucesso!");
        }
    }

}?>