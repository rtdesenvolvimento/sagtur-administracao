<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_settings extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('welcome');
        }

        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/PageRepository_model', 'PageRepository_model');

        $this->lang->load('shop', $this->Settings->user_language);

        $this->load->model('settings_model');
        $this->load->model('shop_model');

        $this->lang->load('settings', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png';
        $this->allowed_file_size    = '2048';//2MB

        $this->output->delete_all_cache();//TODO LIMPAR CACHE
    }

    function index()
    {

        $this->form_validation->set_rules('head_code', lang('head_code'), 'trim');

        if ($this->form_validation->run() == true) {

            $data = array(
                'head_code' => $_POST['head_code'],
                'body_code' => $_POST['body_code'],
                'sections_code' => $_POST['sections_code'],
                'ocultar_horario_saida' => $_POST['ocultar_horario_saida'],
                'filtar_local_embarque' => $_POST['filtar_local_embarque'],
                'captar_meio_divulgacao' => $_POST['captar_meio_divulgacao'],
                'ocultar_embarques_produto' =>  $_POST['ocultar_embarques_produto'],
                'web_page_caching' => $_POST['web_page_caching'],
                'cache_minutes' => $_POST['cache_minutes'],
                'mostrar_taxas' => $_POST['mostrar_taxas'],

                'number_packages_per_line' => $_POST['number_packages_per_line'],
                'pagination_type' => $_POST['pagination_type'],

                //'is_paginate' => $_POST['is_paginate'],
                'is_rotate' => $_POST['is_rotate'],
                'max_packages_line' => $_POST['max_packages_line'],
                'arredondar_bordas_img' => $_POST['arredondar_bordas_img'],
                'nao_distorcer_img' => $_POST['nao_distorcer_img'],
                'atividades_recomendadas' => $_POST['atividades_recomendadas'],

            );
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateShopSetting($data)) {
            $this->session->set_flashdata('message', lang('setting_updated'));
            redirect("shop_settings");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('shop_settings')));
            $meta = array('page_title' => lang('shop_settings'), 'bc' => $bc);

            $this->page_construct('shop_settings/index', $meta, $this->data);
        }
    }

    function pages()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('shop_settings'),
                'page' => lang('shop_settings')),
            array('link' => '#', 'page' => lang('pages')));
        $meta = array('page_title' => lang('pages'), 'bc' => $bc);
        $this->page_construct('shop_settings/pages', $meta, $this->data);
    }

    function getPages()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("page.id as id, menu.name as menu, page.titulo as name, page.data_publicacao, page.data_despublicacao, page.active as active")
            ->from("page")
            ->join('menu', 'menu.id = page.menu_id', 'left')
            ->order_by('page.titulo desc')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('shop_settings/editPage/$1') . "'class='tip' title='" . lang("editar_page") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_page") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('shop_settings/deletarPage/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addPage()
    {
        $this->form_validation->set_rules('titulo', lang("titulo"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $conteudo = $this->input->post('conteudo', false);

            $data = array(
                'active' => $this->input->post('active'),
                'titulo' => $this->input->post('titulo'),
                'menu_id' => $this->input->post('menu_id'),
                'data_publicacao' => $this->input->post('data_publicacao'),
                'data_despublicacao' => ($this->input->post('data_despublicacao') != '' ? $this->input->post('data_despublicacao') : null),
                'conteudo' => $conteudo,
            );

            $photo = $this->addPhotoPage();

            if ($photo)  $data['photo'] = $photo;

        } elseif ($this->input->post('addPage')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("shop_settings/pages");
        }

        if ($this->form_validation->run() == true && $this->shop_model->addPage($data)) {
            $this->session->set_flashdata('message', lang("page_adicionada_com_sucesso"));
            redirect("shop_settings/pages");
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('shop_settings'), 'page' => lang('shop_settings')), array('link' => '#', 'page' => lang('adicionar_page')));
            $meta = array('page_title' => lang('adicionar_page'), 'bc' => $bc);

            $this->data['menusPai'] = $this->MenuRepository_model->getAllMenusPai();
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->page_construct('shop_settings/addPage', $meta, $this->data);
        }
    }

    function editPage($id)
    {
        $page = $this->PageRepository_model->getById($id);

        $this->form_validation->set_rules('titulo', lang("titulo"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $conteudo = $this->input->post('conteudo', false);

            $data = array(
                'active' => $this->input->post('active'),
                'titulo' => $this->input->post('titulo'),
                'menu_id' => $this->input->post('menu_id'),
                'data_publicacao' => $this->input->post('data_publicacao'),
                'data_despublicacao' => ($this->input->post('data_despublicacao') != '' ? $this->input->post('data_despublicacao') : null),
                'conteudo' => $conteudo,
            );

            $photo = $this->addPhotoPage();

            if ($photo)  $data['photo'] = $photo;

        } elseif ($this->input->post('editPage')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("shop_settings/editPage/".$id);
        }

        if ($this->form_validation->run() == true && $this->shop_model->editPage($id, $data)) {
            $this->session->set_flashdata('message', lang("page_editado_com_sucesso"));
            redirect("shop_settings/editPage/".$id);
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('shop_settings'), 'page' => lang('shop_settings')), array('link' => '#', 'page' => lang('adicionar_page')));
            $meta = array('page_title' => lang('adicionar_page'), 'bc' => $bc);

            $this->data['menusPai'] = $this->MenuRepository_model->getAllMenusPai();
            $this->data['page'] = $page;
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->page_construct('shop_settings/editPage', $meta, $this->data);
        }
    }

    private function addPhotoPage() {

        $this->load->library('upload');

        if ($_FILES['photo']['size'] > 0) {

            $config['upload_path']      = $this->upload_path;
            $config['allowed_types']    = $this->image_types;
            $config['max_size']         = $this->allowed_file_size;
            $config['max_width']        = $this->Settings->iwidth;
            $config['max_height']       = $this->Settings->iheight;
            $config['overwrite']        = FALSE;
            $config['max_filename']     = 25;
            $config['encrypt_name']     = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('photo')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            return $this->upload->file_name;
        }

        return false;
    }

    function deletarPage($id = NULL)
    {

        if ($this->shop_model->deletarPage($id)) {
            echo lang("page_deletado_com_sucesso");
        }
    }

    function menus()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('shop_settings'),
                'page' => lang('shop_settings')),
            array('link' => '#', 'page' => lang('menus')));
        $meta = array('page_title' => lang('menus'), 'bc' => $bc);
        $this->page_construct('shop_settings/menus', $meta, $this->data);
    }

    function adicionarMenu()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('type', lang("type"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'new_page' => $this->input->post('new_page'),
                'url_page' => $this->input->post('url_page'),
                'page_id' => $this->input->post('page_id'),
                'product_id' => $this->input->post('product_id'),
                'programacao_id' => $this->input->post('programacao_id'),
                'menu_pai' => $this->input->post('menu_pai'),
                'active' => $this->input->post('active'),
            );
        } elseif ($this->input->post('adicionarMenu')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("shop_settings/menus");
        }

        if ($this->form_validation->run() == true && $this->shop_model->adicionarMenu($data)) {
            $this->session->set_flashdata('message', lang("menu_adicionado_com_sucesso"));
            redirect("shop_settings/menus");
        } else {
            $this->data['menusPai'] = $this->MenuRepository_model->getAllMenusPai();
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'shop_settings/adicionarMenu', $this->data);
        }
    }
    function editarMenu($id)
    {
        $menu = $this->MenuRepository_model->getById($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('type', lang("type"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'new_page' => $this->input->post('new_page'),
                'url_page' => $this->input->post('url_page'),
                'page_id' => $this->input->post('page_id'),
                'product_id' => $this->input->post('product_id'),
                'programacao_id' => $this->input->post('programacao_id'),
                'menu_pai' => $this->input->post('menu_pai'),
                'active' => $this->input->post('active'),
            );
        } elseif ($this->input->post('adicionarMenu')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("shop_settings/menus");
        }

        if ($this->form_validation->run() == true && $this->shop_model->editarMenu($id, $data)) {
            $this->session->set_flashdata('message', lang("menu_editado_com_sucesso"));
            redirect("shop_settings/menus");
        } else {
            $this->data['menu'] = $menu;
            $this->data['menusPai'] = $this->MenuRepository_model->getAllMenusPai();
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'shop_settings/editarMenu', $this->data);
        }
    }

    function getMenus()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("menu.id as id, pai.name as pai, menu.name as name, menu.type as type, menu.active as active")
            ->from("menu")
            ->join('menu pai', 'pai.id = menu.menu_pai', 'left')
            ->order_by('pai.name desc')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('shop_settings/editarMenu/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_menu") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_menu") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('shop_settings/deletarMenu/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function deletarMenu($id = NULL)
    {
        if ($this->shop_model->verificaIntegridadeMenu($id)) {
            $this->session->set_flashdata('error', lang("menu_sendo_utilizada"));
            redirect("shop_settings/menus", 'refresh');
        }

        if ($this->shop_model->deletarMenu($id)) {
            echo lang("menu_deletado_com_sucesso");
        }
    }

    public function sitemap()
    {
        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $menusPai = $this->MenuRepository_model->getAllMenusPai();

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->group_data_website = $this->Settings->receptive;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);
        $map = '<?xml version="1.0" encoding="UTF-8" ?>';

        $map .= '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
        $map .= '<url>';
        $map .= '<loc>' . $this->Settings->url_site_domain . '</loc> ';
        $map .= '<priority>1.0</priority>';
        $map .= '<changefreq>weekly</changefreq>';
        $map .= '</url>';

        //promocao
        $map .= '<url>';
        $map .= '<loc>' . $this->Settings->url_site_domain .  '/promocao</loc> ';
        $map .= '<priority>0.9</priority>';
        $map .= '</url>';

        if (!empty($programacoes)) {
            foreach ($programacoes as $programacao) {

                if ($this->Settings->receptive) {

                    $dataSaida = $programacao->getDataSaida();
                    $horaSaida = $programacao->getHoraSaida();

                    $dataP = implode('-', array_reverse(explode('-', $dataSaida)));
                    $horaP = explode(':', $horaSaida)[0] . '_' . explode(':', $horaSaida)[1];

                    $description = $this->sma->slug($programacao->name).'-data_'.$dataP.'h'.$horaP.'p'.$programacao->produtoId;
                } else {
                    $description = $this->sma->slug($programacao->name).'-uid'.$programacao->id;
                }

                $map .= '<url>';
                $map .= '<loc>' . $this->Settings->url_site_domain .'/pacote/'. $description. '/'. $this->Settings->default_biller . '</loc> ';
                $map .= '<priority>0.8</priority>';
                $map .= '<changefreq>weekly</changefreq>';
                $map .= '</url>';
            }
        }

        foreach ($menusPai as $menu) {

            $pages = $this->MenuRepository_model->getAllPagesByMenu($menu->id);

            if(!empty($pages)) {
                foreach ($pages as $page) {
                    $map .= '<url>';
                    $map .= '<loc>' . $this->Settings->url_site_domain.'/page/'.$this->sma->slug($page->titulo) . '</loc> ';
                    $map .= '<priority>0.7</priority>';
                    $map .= '<changefreq>weekly</changefreq>';
                    $map .= '</url>';
                }
            }
        }

        //contato
        $map .= '<url>';
        $map .= '<loc>' . $this->Settings->url_site_domain .  '/contato</loc> ';
        $map .= '<priority>0.6</priority>';
        $map .= '<changefreq>weekly</changefreq>';
        $map .= '</url>';

        //galeria
        $map .= '<url>';
        $map .= '<loc>' . $this->Settings->url_site_domain .  '/galeria</loc> ';
        $map .= '<priority>0.5</priority>';
        $map .= '<changefreq>weekly</changefreq>';
        $map .= '</url>';

        //sobre
        $map .= '<url>';
        $map .= '<loc>' . $this->Settings->url_site_domain .  '/sobre</loc> ';
        $map .= '<priority>0.4</priority>';
        $map .= '<changefreq>weekly</changefreq>';
        $map .= '</url>';

        $map .= '</urlset>';

        $roots_to_file = 'assets/uploads';

        file_put_contents($roots_to_file.'/sitemap_' . $this->session->userdata('cnpjempresa') . '.xml', $map);

        $sitemapUrl = $this->Settings->url_site_domain . '/sitemap_' . $this->session->userdata('cnpjempresa') . '.xml';

        $this->ensureSitemapInRobots($roots_to_file. '/robots.txt', $sitemapUrl);

        header('Location: ' . $sitemapUrl);
        exit;
    }

    private function ensureSitemapInRobots($robotsFilePath, $sitemapUrl) {

        if (!file_exists($robotsFilePath)) {
            file_put_contents($robotsFilePath, "Sitemap: $sitemapUrl\n");
        }

        $robotsContent = file($robotsFilePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        $sitemapExists = false;
        foreach ($robotsContent as $line) {
            if (trim($line) === "Sitemap: $sitemapUrl") {
                $sitemapExists = true;
                break;
            }
        }

        if (!$sitemapExists) {
            file_put_contents($robotsFilePath, "\nSitemap: $sitemapUrl", FILE_APPEND);
        }
    }

}