<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index(){show_404();}

    /**
     * Run Cron Job - Nao tem funcao especifica, apenas para testes
     */
    function run()
    {
        $this->load->model('cron_model');
        if ($m = $this->cron_model->run_cron()) {
            echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
            echo '<br/><p>Cron job successfully run.<br/>' . $m . '</p>';
            echo '</body></html>';
        } else {
            echo 'Cron job failed!';
        }
    }

    /**
     * Run Periodical - Envio de e-mails periodicos
     */
    public function run_email_queue()
    {
        $this->load->model('cron_model');
        $m = $this->cron_model->run_email_queue();

        echo '<!doctype html><html><head><title>Cron Job Email Queue</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style>';
        echo '<br/><p>Cron Job Email Queue successfully run.<br/>' . $m . '</p>';
        echo '</body></html>';
    }

    /**
     * Run Desbloqueio de Assentos - Desbloqueia os assentos que estao travados no link de reservas
     */
    function run_desbloqueio_assento()
    {
        $this->load->model('cron_model');
        if ($m = $this->cron_model->run_desbloqueio_assento()) {
            echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
            echo '<br/><p>Cron job successfully run.<br/>' . $m . '</p>';
            echo '</body></html>';
        } else {
            echo 'Cron job failed!';
        }
    }

    /**
     * Run Bloqueia e Desbloqueia Clientes inadimplementes
     */
    function run_bloqueio_clientes_falta_pagamento()
    {
        $this->load->model('cron_model');
        if ($m = $this->cron_model->run_bloqueio_clientes_falta_pagamento()) {
            echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
            echo '<br/><p>Cron bloqueio clientes job successfully run.<br/>' . $m . '</p>';
            echo '</body></html>';
        } else {
            echo 'Cron job failed!';
        }
    }


    /**
     * Run Diary - Cancelamento das vendas automaticamente e envio de e-mails das avaliacoes
     */
    public function run_diary()
    {
        $this->load->model('cron_model');
        $m = $this->cron_model->run_diary();

        echo '<!doctype html><html><head><title>Cron Job Diary</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style>';
        echo '<br/><p>Cron Diary job successfully run.<br/>' . $m . '</p>';
        echo '</body></html>';
    }

    /**
     * Run Create Google Contacts - Cria os contatos no Google Contacts
     */
    public function run_create_google_contacts()
    {
        $this->load->model('cron_model');
        $m = $this->cron_model->run_create_google_contacts();

        echo '<!doctype html><html><head><title>Cron Job Periodical</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style>';
        echo '<br/><p>Cron Diary job Periodical successfully run.<br/>' . $m . '</p>';
        echo '</body></html>';
    }

    function diary()
    {
        $otherdb = $this->load->database('demonstracaosagtur', TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();
        $this->post_run_diary('demonstracaosagtur');

        foreach ($dbs as $db_name)
        {
            if ($this->isTableIgnore($db_name)) {
                $otherdbInner       = $this->load->database($db_name, TRUE);
                $configuracaoGeral  = $this->getSettingsDB($otherdbInner);

                if ($configuracaoGeral->status != null) {
                    if ($configuracaoGeral->status == 1) {
                        $this->post_run_diary($db_name);
                    }
                }
            }
        }
    }

    function create_google_contacts()
    {
        $otherdb = $this->load->database('demonstracaosagtur', TRUE);
        $dbutil = $this->load->dbutil($otherdb, true);
        $dbs = $dbutil->list_databases();
        $this->post_create_google_contacts('demonstracaosagtur');

        foreach ($dbs as $db_name)
        {
            if ($this->isTableIgnore($db_name)) {
                $otherdbInner       = $this->load->database($db_name, TRUE);
                $configuracaoGeral  = $this->getSettingsDB($otherdbInner);

                if ($configuracaoGeral->status != null) {
                    if ($configuracaoGeral->status == 1) {
                        if ($this->getPlugSignActive($otherdbInner) ) {
                            $this->post_create_google_contacts($db_name);
                        }
                    }
                }
            }
        }
    }

    public function post_run_diary($db = NULL) {
        echo $this->post(base_url().'cron/run_diary?token='.$db);
    }

    public function post_create_google_contacts($db = NULL) {
        echo $this->post(base_url().'cron/run_create_google_contacts?token='.$db);
    }

    public function post($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        echo $output;
    }

    public function getSettingsDB($otherdbInner)
    {
        $q = $otherdbInner->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPlugSignActive($otherdbInner)
    {
        try {
            $plugsignSetting = $this->getPlugsignSettings($otherdbInner);

            if ($plugsignSetting->active && $plugsignSetting->token) {
                return true;
            } else {
                return false;
            }

        } catch(Exception $e) {
            return false;
        }
    }

    public function getPlugsignSettings($otherdbInner)
    {
        $q = $otherdbInner->get('plugsign');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    function run_optimaze_table()
    {
        $this->load->model('cron_model');
        if ($m = $this->cron_model->run_optimaze_table()) {
            echo '<!doctype html><html><head><title>Cron Job</title><style>p{background:#F5F5F5;border:1px solid #EEE; padding:15px;}</style></head><body>';
            echo '<br/><p>Corn job successfully run.<br/>' . $m . '</p>';
            echo '</body></html>';
        } else {
            echo 'Corn job failed!';
        }

    }

    function log_corn($msg, $val = NULL)
    {
        $this->load->library('logs');
        return (bool)$this->logs->write('corn', $msg, $val);
    }

    private function isTableIgnore($db)
    {
        return  ($db != 'information_schema' && $db != 'mysql' && $db != 'performance_schema' && $db != 'phpmyadmin' && $db != 'sys' && $db != '_session_database' && $db != 'sagtur_administrativo');
    }

}
