<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('products', $this->Settings->user_language);
        $this->load->library('form_validation');

        //model
        $this->load->model('products_model');
        $this->load->model('db_model');
        $this->load->model('settings_model');
        $this->load->model('Roomlist_model');

        //$this->load->model('financeiro_model');

        $this->load->model('model/ProdutoModel_model', 'ProdutoModel_model');
        $this->load->model('model/AgendaProgramacao_model', 'AgendaProgramacao_model');

        //service
        $this->load->model('service/ProdutoService_model', 'ProdutoService_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //repository
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');
        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ValorFaixaRepository_model', 'ValorFaixaRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');
        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/CorAgendaRepository_model', 'CorAgendaRepository_model');
        $this->load->model('repository/TipoTrajetoVeiculoRepository_model', 'TipoTrajetoVeiculoRepository_model');
        $this->load->model('repository/AgendaProgramacaoRepository_model', 'AgendaProgramacaoRepository_model');
        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');
        $this->load->model('repository/AutomovelRepository_model', 'AutomovelRepository_model');

        //dto
        $this->load->model("dto/AgendaViagemDTO_model", 'AgendaViagemDTO_model');

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '2048';//2MB
        $this->popup_attributes     = array('width' => '900', 'height' => '600', 'window_name' => 'sma_popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
    }

    function index($warehouse_id = NULL)
    {

        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses']   = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse']    = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses']   = NULL;
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse']    = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('products/index', $meta, $this->data);
    }

    function arquivados($warehouse_id = NULL)
    {

        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses']   = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse']    = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses']   = NULL;
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse']    = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products_arquivados')));
        $meta = array('page_title' => lang('products_arquivados'), 'bc' => $bc);
        $this->page_construct('products/arquivados', $meta, $this->data);
    }

    function getProducts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');

        if ($warehouse_id == 'all') $warehouse_id = NULL;

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $detail_link = anchor('products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $arquivar_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("arquivar_produto") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . site_url('products/arquivar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('arquivar_produto') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $detail_link . '</li>
			<li><a href="' . site_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>
			<li><a href="' . site_url('products/duplicar/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>';



        $action .= '	<li class="divider"></li>
       		            <li>' . $arquivar_link . '</li>
       		    </ul> 
       	    </div></div>';

        if ($warehouse_id) {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as name, " . $this->db->dbprefix('categories') . ".name as cname, cost as cost, precoExibicaoSite as price, COALESCE(wp.quantity, 0) as quantity, DATE_FORMAT(data_saida, '%d/%m/%Y') , wp.rack as rack, alert_quantity", FALSE)
                ->from('products');
            if ($this->Settings->display_all_products) {
                $this->datatables->join("( SELECT * from {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id}) wp", 'products.id=wp.product_id', 'left');
            } else {
                $this->datatables->join('warehouses_products wp', 'products.id=wp.product_id', 'left')
                    ->where('wp.warehouse_id', $warehouse_id);
            }

            $this->datatables->join('categories', 'products.category_id=categories.id', 'left')
                ->group_by("products.id");
        } else {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as name, " . $this->db->dbprefix('categories') . ".name as cname, cost as cost, precoExibicaoSite as price, COALESCE(quantity, 0) as quantity,  DATE_FORMAT(data_saida, '%d/%m/%Y')   , '' as rack, alert_quantity", FALSE)
                ->from('products')
                ->join('categories', 'products.category_id=categories.id', 'left')
                ->group_by("products.id")->order_by('products.data_saida');
        }

        if (!$this->Owner && !$this->Admin) {
            if (!$this->session->userdata('show_cost')) {
                $this->datatables->unset_column("cost");
            }
            if (!$this->session->userdata('show_price')) {
                $this->datatables->unset_column("price");
            }
        }

        $this->db->where('viagem', 1);
        $this->db->where('unit in ("Confirmado","Inativo") ');

        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    function getProductsArquivados($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');

        if ($warehouse_id == 'all') $warehouse_id = NULL;

        if ((! $this->Owner || ! $this->Admin) && ! $warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $detail_link = anchor('products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $arquivar_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("desarquivar_produto") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . site_url('products/desarquivar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('desarquivar_produto') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $detail_link . '</li>
			<li><a href="' . site_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>
			<li><a href="' . site_url('products/duplicar/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>';

        $action .= '<li><a href="' . site_url() . 'assets/uploads/$2" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i> '
            . lang('view_image') . '</a></li>';

        $action .= '	<li class="divider"></li>
       		            <li>' . $arquivar_link . '</li>
            </ul>
		</div></div>';

        if ($warehouse_id) {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as name, " . $this->db->dbprefix('categories') . ".name as cname, cost as cost, precoExibicaoSite as price, COALESCE(wp.quantity, 0) as quantity, DATE_FORMAT(data_saida, '%d/%m/%Y') , wp.rack as rack, alert_quantity", FALSE)
                ->from('products');
            if ($this->Settings->display_all_products) {
                $this->datatables->join("( SELECT * from {$this->db->dbprefix('warehouses_products')} WHERE warehouse_id = {$warehouse_id}) wp", 'products.id=wp.product_id', 'left');
            } else {
                $this->datatables->join('warehouses_products wp', 'products.id=wp.product_id', 'left')
                    ->where('wp.warehouse_id', $warehouse_id);
            }

            $this->datatables->join('categories', 'products.category_id=categories.id', 'left')
                ->group_by("products.id");
        } else {
            $this->datatables
                ->select($this->db->dbprefix('products') . ".id as productid, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as name, " . $this->db->dbprefix('categories') . ".name as cname, cost as cost, precoExibicaoSite as price, COALESCE(quantity, 0) as quantity,  DATE_FORMAT(data_saida, '%d/%m/%Y')   , '' as rack, alert_quantity", FALSE)
                ->from('products')
                ->join('categories', 'products.category_id=categories.id', 'left')
                ->group_by("products.id")->order_by('products.data_saida');
        }

        if (!$this->Owner && !$this->Admin) {
            if (!$this->session->userdata('show_cost')) {
                $this->datatables->unset_column("cost");
            }
            if (!$this->session->userdata('show_price')) {
                $this->datatables->unset_column("price");
            }
        }

        $this->db->where('viagem', 1);
        $this->db->where('unit', 'Arquivado');

        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }


    function bus($product_id, $tipoTransporteId = null, $programacaoId = null)
    {	
		$product = $this->site->getProductByID($product_id);

        if (!$product_id || !$product || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $tipoTransporte = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);

        $this->data['tipoTransporte']   = $tipoTransporte;
        $this->data['programacaoId']    = $programacaoId;
        $this->data['product']          = $product;
        $this->data['programacao']      = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $this->data['assentos']         = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id);
        $this->data['assentos2']        = $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id, 2);

        $this->data['bloqueados']       = $this->BusRepository_model->getAssentosBloqueados($product_id, $programacaoId, $tipoTransporte->id);

        if ($tipoTransporte->automovel_id) {
            $this->load->view($this->theme . 'bus/marcacao', $this->data);
        } else {
            $this->load->view($this->theme . 'products/bus', $this->data);
        }
    }

	function bus_select($product_id)
    {
		$product = $this->site->getProductByID($product_id);
        if (!$product_id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
		
		$onibus = $this->products_model->getProductOptions_Onibus($product_id);
		$bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
		$this->data['product']      = $product;
		$this->data['listOnibus']   = $onibus;
		$meta = array('page_title'  => lang('products'), 'bc' => $bc);
 		$this->load->view($this->theme . 'products/bus_select', $this->data);
    }

    function bus_select_client($product_id)
    {
        $product = $this->site->getProductByID($product_id);
        if (!$product_id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $onibus = $this->products_model->getProductOptions_Onibus($product_id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $this->data['product'] = $product;
        $this->data['listOnibus']  = $onibus;
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->load->view($this->theme . 'products/bus_select_client', $this->data);
    }

    function set_rack($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('rack', lang("rack_location"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $data = array('rack' => $this->input->post('rack'),
                'product_id' => $product_id,
                'warehouse_id' => $warehouse_id,
            );
        } elseif ($this->input->post('set_rack')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products");
        }

        if ($this->form_validation->run() == true && $this->products_model->setRack($data)) {
            $this->session->set_flashdata('message', lang("rack_set"));
            redirect("products/" . $warehouse_id);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['product'] = $this->site->getProductByID($product_id);
            $wh_pr = $this->products_model->getProductQuantity($product_id, $warehouse_id);
            $this->data['rack'] = $wh_pr['rack'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/set_rack', $this->data);
        }
    }

    function product_barcode($product_code = NULL, $bcs = 'code128', $height = 60)
    {
        return "<img src='" . site_url('products/gen_barcode/' . $product_code . '/' . $bcs . '/' . $height) . "' alt='{$product_code}' class='bcimg' />";
    }

    function barcode($product_code = NULL, $bcs = 'code128', $height = 60)
    {
        return site_url('products/gen_barcode/' . $product_code . '/' . $bcs . '/' . $height);
    }

    function gen_barcode($product_code = NULL, $bcs = 'code128', $height = 60, $text = 1)
    {
        $drawText = ($text != 1) ? FALSE : TRUE;
        $this->load->library('zend');
        $this->zend->load('Zend/Barcode');
        $barcodeOptions = array('text' => $product_code, 'barHeight' => $height, 'drawText' => $drawText, 'factor' => 1);
        $rendererOptions = array('imageType' => 'png', 'horizontalPosition' => 'center', 'verticalPosition' => 'middle');
        $imageResource = Zend_Barcode::render($bcs, 'image', $barcodeOptions, $rendererOptions);
        return $imageResource;

    }

    function single_barcode($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);

        $product = $this->products_model->getProductByID($product_id);
        $currencies = $this->site->getAllCurrencies();

        $this->data['product'] = $product;
        $options = $this->products_model->getProductOptionsWithWH($product_id);
        if( ! $options) {
            $options = $this->products_model->getProductOptions($product_id);
        }
        $table = '';
        if (!empty($options)) {
            $r = 1;
            foreach ($options as $option) {
                $quantity = $option->wh_qty;
                $warehouse = $this->site->getWarehouseByID(($option->quantity <= 0) ? $this->Settings->default_warehouse :$option->warehouse_id);
                $table .= '<h3 class="'.($option->quantity ? '' : 'text-danger').'">'.$warehouse->name.' ('.$warehouse->code.') - '.$product->name.' - '.$option->name.' ('.lang('quantity').': '.$quantity.')</h3>';
                $table .= '<table class="table table-bordered barcodes"><tbody><tr>';
                for($i=0; $i < $quantity; $i++) {

                    $table .= '<td style="width: 20px;"><table class="table-barcode"><tbody><tr><td colspan="2" class="bold">' . $this->Settings->site_name . '</td></tr><tr><td colspan="2">' . $product->name . ' - '.$option->name.'</td></tr><tr><td colspan="2" class="text-center bc">' . $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', 60) . '</td></tr>';
                    foreach ($currencies as $currency) {
                        $table .= '<tr><td class="text-left">' . $currency->code . '</td><td class="text-right">' . $this->sma->formatMoney($product->price * $currency->rate) . '</td></tr>';
                    }
                    $table .= '</tbody></table>';
                    $table .= '</td>';
                    $table .= ((bool)($i & 1)) ? '</tr><tr>' : '';

                }
                $r++;
                $table .= '</tr></tbody></table><hr>';
            }
        } else {
            $table .= '<table class="table table-bordered barcodes"><tbody><tr>';
            $num = $product->quantity;
            for ($r = 1; $r <= $num; $r++) {
                if ($r != 1) {
                    $rw = (bool)($r & 1);
                    $table .= $rw ? '</tr><tr>' : '';
                }
                $table .= '<td style="width: 20px;"><table class="table-barcode"><tbody><tr><td colspan="2" class="bold">' . $this->Settings->site_name . '</td></tr><tr><td colspan="2">' . $product->name . '</td></tr><tr><td colspan="2" class="text-center bc">' . $this->product_barcode($product->code, $product->barcode_symbology, 60) . '</td></tr>';
                foreach ($currencies as $currency) {
                    $table .= '<tr><td class="text-left">' . $currency->code . '</td><td class="text-right">' . $this->sma->formatMoney($product->price * $currency->rate) . '</td></tr>';
                }
                $table .= '</tbody></table>';
                $table .= '</td>';
            }
            $table .= '</tr></tbody></table>';
        }

        $this->data['table'] = $table;

        $this->data['page_title'] = lang("print_barcodes");
        $this->load->view($this->theme . 'products/single_barcode', $this->data);
    }

    function single_label($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);

        $product = $this->products_model->getProductByID($product_id);
        $currencies = $this->site->getAllCurrencies();

        $this->data['product'] = $product;
        $options = $this->products_model->getProductOptionsWithWH($product_id);

        $table = '';
        if (!empty($options)) {
            $r = 1;
            foreach ($options as $option) {
                $quantity = $option->wh_qty;
                $warehouse = $this->site->getWarehouseByID($option->warehouse_id);
                $table .= '<h3 class="'.($option->quantity ? '' : 'text-danger').'">'.$warehouse->name.' ('.$warehouse->code.') - '.$product->name.' - '.$option->name.' ('.lang('quantity').': '.$quantity.')</h3>';
                $table .= '<table class="table table-bordered barcodes"><tbody><tr>';
                for($i=0; $i < $quantity; $i++) {
                    if ($i % 4 == 0 && $i > 3) {
                        $table .= '</tr><tr>';
                    }
                    $table .= '<td style="width: 20px;"><table class="table-barcode"><tbody><tr><td colspan="2" class="bold">' . $this->Settings->site_name . '</td></tr><tr><td colspan="2">' . $product->name . ' - '.$option->name.'</td></tr><tr><td colspan="2" class="text-center bc">' . $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', 30) . '</td></tr>';
                    foreach ($currencies as $currency) {
                        $table .= '<tr><td class="text-left">' . $currency->code . '</td><td class="text-right">' . $this->sma->formatMoney($product->price * $currency->rate) . '</td></tr>';
                    }
                    $table .= '</tbody></table>';
                    $table .= '</td>';
                }
                $r++;
                $table .= '</tr></tbody></table><hr>';
            }
        } else {
            $table .= '<table class="table table-bordered barcodes"><tbody><tr>';
            $num = $product->quantity;
            for ($r = 1; $r <= $num; $r++) {
                $table .= '<td style="width: 20px;"><table class="table-barcode"><tbody><tr><td colspan="2" class="bold">' . $this->Settings->site_name . '</td></tr><tr><td colspan="2">' . $product->name . '</td></tr><tr><td colspan="2" class="text-center bc">' . $this->product_barcode($product->code, $product->barcode_symbology, 30) . '</td></tr>';
                foreach ($currencies as $currency) {
                    $table .= '<tr><td class="text-left">' . $currency->code . '</td><td class="text-right">' . $this->sma->formatMoney($product->price * $currency->rate) . '</td></tr>';
                }
                $table .= '</tbody></table>';
                $table .= '</td>';
                if ($r % 4 == 0 && $r > 3) {
                    $table .= '</tr><tr>';
                }
            }
            $table .= '</tr></tbody></table>';
        }

        $this->data['table'] = $table;
        $this->data['page_title'] = lang("barcode_label");
        $this->load->view($this->theme . 'products/single_label', $this->data);
    }

    function single_label2($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);

        $pr = $this->products_model->getProductByID($product_id);
        $currencies = $this->site->getAllCurrencies();

        $this->data['product'] = $pr;
        $options = $this->products_model->getProductOptionsWithWH($product_id);
        $html = "";

        if (!empty($options)) {
            foreach ($options as $option) {
                for ($r = 1; $r <= $option->wh_qty; $r++) {
                    $html .= '<div class="labels"><strong>' . $pr->name . ' - '.$option->name.'</strong><br>' . $this->product_barcode($pr->code . $this->Settings->barcode_separator . $option->id, 'code128', 25) . '<br><span class="price">'.lang('price') .': ' .$this->Settings->default_currency. ' ' . $this->sma->formatMoney($pr->price) . '</span></div>';
                }
            }
        } else {
            for ($r = 1; $r <= $pr->quantity; $r++) {
                $html .= '<div class="labels"><strong>' . $pr->name . '</strong><br>' . $this->product_barcode($pr->code, $pr->barcode_symbology, 25) . '<br><span class="price">'.lang('price') .': ' .$this->Settings->default_currency. ' ' . $this->sma->formatMoney($pr->price) . '</span></div>';
            }
        }

        $this->data['html'] = $html;
        $this->data['page_title'] = lang("barcode_label");
        $this->load->view($this->theme . 'products/single_label2', $this->data);
    }

    function print_barcodes($product_id = NULL)
    {
        $this->sma->checkPermissions('barcode', true);

        $this->form_validation->set_rules('style', lang("style"), 'required');

        if ($this->form_validation->run() == true) {

            $style = $this->input->post('style');
            $bci_size = ($style == 10 || $style == 12 ? 50 : ($style == 14 || $style == 18 ? 30 : 20));
            $currencies = $this->site->getAllCurrencies();
            $s = isset($_POST['product']) ? sizeof($_POST['product']) : 0;
            if ($s < 1) {
                $this->session->set_flashdata('error', lang('no_product_selected'));
                redirect("products/print_barcodes");
            }
            for ($m = 0; $m < $s; $m++) {
                $pid = $_POST['product'][$m];
                $quantity = $_POST['quantity'][$m];
                $product = $this->products_model->getProductWithCategory($pid);
                
                if ($variants = $this->products_model->getProductOptions($pid)) {
                    foreach ($variants as $option) {
                        if ($this->input->post('vt_'.$product->id.'_'.$option->id)) {
                            $barcodes[] = array(
                                'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                                'name' => $this->input->post('product_name') ? $product->name.' - '.$option->name : FALSE,
                                'image' => $this->input->post('product_image') ? $product->image : FALSE,
                                'barcode' => $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', $bci_size),
                                'price' => $this->input->post('price') ?  $this->sma->formatMoney($option->price != 0 ? $option->price : $product->price) : FALSE,
                                'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                                'category' => $this->input->post('category') ? $product->category : FALSE,
                                'currencies' => $this->input->post('currencies'),
                                'variants' => $this->input->post('variants') ? $variants : FALSE,
                                'quantity' => $quantity
                                );
                        }
                    }
                } else {
                    $barcodes[] = array(
                        'site' => $this->input->post('site_name') ? $this->Settings->site_name : FALSE,
                        'name' => $this->input->post('product_name') ? $product->name : FALSE,
                        'image' => $this->input->post('product_image') ? $product->image : FALSE,
                        'barcode' => $this->product_barcode($product->code, $product->barcode_symbology, $bci_size),
                        'price' => $this->input->post('price') ?  $this->sma->formatMoney($product->price) : FALSE,
                        'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                        'category' => $this->input->post('category') ? $product->category : FALSE,
                        'currencies' => $this->input->post('currencies'),
                        'variants' => FALSE,
                        'quantity' => $quantity
                        );
                }

            }
            $this->data['barcodes'] = $barcodes;
            $this->data['currencies'] = $currencies;
            $this->data['style'] = $style;
            $this->data['items'] = false;
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        } else {

            if ($this->input->get('purchase') || $this->input->get('transfer')) {
                if ($this->input->get('purchase')) {
                    $purchase_id = $this->input->get('purchase', TRUE);
                    $items = $this->products_model->getPurchaseItems($purchase_id);
                } elseif ($this->input->get('transfer')) {
                    $transfer_id = $this->input->get('transfer', TRUE);
                    $items = $this->products_model->getTransferItems($transfer_id);
                }
                if ($items) {
                    foreach ($items as $item) {
                        if ($row = $this->products_model->getProductByID($item->product_id)) {
                            $selected_variants = false;
                            if ($variants = $this->products_model->getProductOptions($row->id)) {
                                foreach ($variants as $variant) {
                                    $selected_variants[$variant->id] = isset($pr[$row->id]['selected_variants'][$variant->id]) && !empty($pr[$row->id]['selected_variants'][$variant->id]) ? 1 : ($variant->id == $item->option_id ? 1 : 0);
                                }
                            }
                            $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $item->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                        }
                    }
                    $this->data['message'] = lang('products_added_to_list');
                }
            }

            if ($this->input->get('category')) {
                if ($products = $this->products_model->getCategoryProducts($this->input->get('category'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }

            if ($this->input->get('subcategory')) {
                if ($products = $this->products_model->getSubCategoryProducts($this->input->get('subcategory'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }

            $this->data['items'] = isset($pr) ? json_encode($pr) : false;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);

        }
    }

    /* ------------------------------------------------------- */
    function add($id = NULL)
    {

        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->load->library('upload');

        $warehouses     = $this->site->getAllWarehouses();
        $tiposCobranca  = $this->site->getAllTiposCobrancaDebito(true);

        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');
		
        if ($this->form_validation->run() == true) {

            $this->form_validation->set_rules('name', lang('name'), 'trim|required');

            $produtoModel = new ProdutoModel_model();

            $details    =  $this->input->post('details');
            $active     = $this->input->post('active');
            $unit       = 'Confirmado';
            $isBateVolta = $this->input->post('isValorPorFaixaEtaria');
            $isControleEstoquePorTipoHospedagem = $this->input->post('controle_estoque_hospedagem');
            $type_calendar  = $this->input->post('type_calendar');
            $isPasseio      = FALSE;

            if (!$active) {
                $unit = 'Inativo';
            }

            if ($this->input->post('receptivo') == 'receptivo') {
                $type_calendar  = 1;//calendario
                $isPasseio      = TRUE;
            }

            if ($isBateVolta) {
                $isControleEstoquePorTipoHospedagem = false;
            }

			$data = array(
                'code' 				            => rand(1000,100000),
                'type'				            => 'standard',
                'barcode_symbology'             => 'ean13',
                'viagem' 			            => 1,
                'unit' 				            => $unit,
                'enviar_site' 		            => $this->input->post('enviar_site'),
                'apenas_cotacao' 		        => $this->input->post('apenas_cotacao') ,
                'quantidadePessoasViagem' 		=> $this->input->post("quantidadePessoasViagem"),
                'quantity'                      => $this->input->post("quantidadePessoasViagem"),
                'name' 				            => $this->input->post('name'),
                'tempo_viagem' 		            => $this->input->post('tempo_viagem'),
                'alertar_polcas_vagas' 		    => $this->input->post('alertar_polcas_vagas'),
                'alertar_ultimas_vagas' 		=> $this->input->post('alertar_ultimas_vagas'),
                'price_pacote_zero_cinco' 		=> $this->input->post('price_pacote_zero_cinco'),
                'price_pacote_seis_onze' 		=> $this->input->post('price_pacote_seis_onze'),
                'price_pacote_acima_sessenta'   => $this->input->post('price_pacote_acima_sessenta'),
                'percentual_desconto_a_vista' 	=> $this->input->post('percentual_desconto_a_vista'),
                'categorias' 		            => $this->input->post('categorias'),
                'instrucoescontrato' 		    => $this->input->post('instrucoescontrato'),
                'tipo_transporte' 		        => $this->input->post('tipo_transporte'),
                'itinerario' 		            => $this->input->post('itinerario'),
                'oqueInclui' 		            => $this->input->post('oqueInclui'),
                'valores_condicoes'             => $this->input->post('valores_condicoes'),
                'duracao_pacote'                => $this->input->post('duracao_pacote'),
                'numero_pessoas' 	            => $this->input->post('numero_pessoas'),
                'status' 			            => $this->input->post('status'),
				'data_saida' 		            => $this->input->post('data_saida'),
				'origem' 			            => $this->input->post('origem'),
				'previsao_chegada' 	            => $this->input->post('previsao_chegada'),
				'data_chegada' 		            => $this->input->post('data_chegada'),
				'destino' 			            => $this->input->post('destino'),
				'data_retorno'		            => $this->input->post('data_retorno'),
                'hora_saida'		            => $this->input->post('hora_saida'),
                'hora_retorno'		            => $this->input->post('hora_retorno'),
                'margem_padrao' 	            => $this->input->post('margem_padrao'),
                'simbolo_moeda' 	            => $this->input->post('simbolo_moeda'),
                'valor_pacote' 	                => $this->input->post('valor_pacote'),
                'simboloMoeda'                  => $this->input->post('simboloMoeda'),
                'precoExibicaoSite' 	        => $this->input->post('precoExibicaoSite'),
                'local_retorno' 	            => $this->input->post('local_retorno'),
                'hora_chegada' 	                => $this->input->post('hora_chegada'),
                'category_id' 		            => $this->input->post('category'),
                'subcategory_id' 	            => $this->input->post('subcategory') ,
                'cost' 				            => $this->sma->formatDecimal($this->input->post('price')),
                'price'	 			            => $this->sma->formatDecimal($this->input->post('price')),
                'tax_rate' 			            => $this->input->post('tax_rate'),
                'tax_method' 		            => $this->input->post('tax_method'),
				'alert_quantity' 	            => 1,
                'track_quantity' 	            => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' 			            => $details,
                'product_details' 	            => $this->input->post('product_details'),
                'supplier1' 		            => $this->input->post('supplier'),
                'supplier1price' 	            => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' 		            => $this->input->post('supplier_2'),
                'supplier2price' 	            => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' 		            => $this->input->post('supplier_3'),
                'supplier3price' 	            => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' 		            => $this->input->post('supplier_4'),
                'supplier4price' 	            => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' 		            => $this->input->post('supplier_5'),
                'supplier5price' 	            => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' 				            => $this->input->post('cf1'),
                'cf2' 				            => $this->input->post('cf2'),
                'cf3' 				            => $this->input->post('cf3'),
                'cf4' 				            => $this->input->post('cf4'),
                'cf5' 				            => $this->input->post('cf5'),
                'cf6' 				            => $this->input->post('cf6'),
                'alert_stripe' 		            => $this->input->post('alert_stripe'),
                'promotion' 		            => $this->input->post('promotion'),
                'promo_price' 		            => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' 		            => $this->sma->fsd($this->input->post('start_date')),
                'end_date' 			            => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no'             => $this->input->post('supplier_part_no'),
                'supplier2_part_no'             => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no'             => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no'             => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no'             => $this->input->post('supplier_5_part_no'),
                'isServicoOnline'               => $this->input->post('isServicoOnline'),
                'controle_estoque_hospedagem'   => $isControleEstoquePorTipoHospedagem,
                'isTransporteTuristico'         => $this->input->post('isTransporteTuristico'),
                'isHospedagem'                  => $this->input->post('isHospedagem'),
                'isServicosAdicionais'          => $this->input->post('isServicosAdicionais'),
                'isValorPorFaixaEtaria'         => $this->input->post('isValorPorFaixaEtaria'),
                'isApenasColetarPagador'        => $this->input->post('isApenasColetarPagador'),
                'isEmbarque'                    => $this->input->post('isEmbarque'),
                'isComissao'                    => $this->input->post('isComissao'),
                'permitirListaEmpera'           => $this->input->post('permitirListaEmpera'),
                'active'                        => $this->input->post('active'),
                'captarEnderecoLink'            => $this->input->post('captarEnderecoLink'),
                'permiteVendaMenorIdade'        => $this->input->post('permiteVendaMenorIdade') == null ? '0' : $this->input->post('permiteVendaMenorIdade'),
                'permiteVendaClienteDuplicidade'=> $this->input->post('permiteVendaClienteDuplicidade') == null ? '0' : $this->input->post('permiteVendaClienteDuplicidade'),
                'isTaxasComissao'               => $this->input->post('isTaxasComissao') == null ? 0 : $this->input->post('isTaxasComissao'),
                'tipoComissao'                  => $this->input->post('tipoComissao'),
                'tipoCalculoComissao'           => $this->input->post('tipoCalculoComissao'),
                'comissao'                      => $this->input->post('comissao'),
                'destaque'                      => $this->input->post('destaque'),

                'color_agenda_id'               => $this->input->post('color_agenda_id'),
                'tipo_trajeto_id'               => $this->input->post('tipo_trajeto_id'),
                'duracao_atividade'             => $this->input->post('duracao_atividade'),
                'tipo_duracao_atividade'        => $this->input->post('tipo_duracao_atividade'),
                'cat_precificacao'              => $this->input->post('cat_precificacao'),
                'desc_duracao'                  => $this->input->post('desc_duracao'),
                'product_action'                => $this->input->post('product_action'),
                'product_action_text'           => $this->input->post('product_action_text'),
                'contract_id'                   => $this->input->post('contract_id'),

                'permite_pagamento_total'       => $this->input->post('permite_pagamento_total'),
                'obriga_sinal'                  => $this->input->post('obriga_sinal'),
                'avaliar'                       => $this->input->post('avaliar') != null ? $this->input->post('avaliar') : false,
                'avaliar_dias'                  => $this->input->post('avaliar_dias') != null ? $this->input->post('avaliar_dias') : 2,

                'type_calendar'                 => $type_calendar,
                'passeio'                       => $isPasseio,
            );

			$image = $this->adicionarFotoPrincipal();

			if ($image) {
                $data['image'] = $image;
            }

            $produtoModel->data = $data;
            $produtoModel->items = $this->adicionarServicosAdicionais();
            $produtoModel->photos =  $this->adicionarFotosAdicionais();
            $produtoModel->product_attributes = $this->adicionarAtributosDaViagem($data);
            $produtoModel->faixaEtariaValores = $this->adicionarFaixaEtariaValor();
            $produtoModel->servicos_inclui = $this->adicionarServicosInclusos();
            $produtoModel->extras = $this->adicionarExtra();
            $produtoModel->locaisEmbarque = $this->adicionarLocaisEmbarque();
            $produtoModel->condicoesPagamento = $this->adicionarCondicaoPagamentoProduto();
            $produtoModel->tiposCobranca = $this->adicionarTipoCobrancaProduto($tiposCobranca);
            $produtoModel->transportes = $this->adicionarTransportes();
            $produtoModel->tiposHospedagem = $this->adicionarTipoHospedagemViagem();
            $produtoModel->tiposHospedagemFaixaEtaria = $this->adicionarFaixaEtariaTipoHospedagem();
            $produtoModel->faixaEtariaValoresServicoAdicional = $this->adicionarFaixaEtariaValoresServicoAdicional();
            $produtoModel->midia_data = $this->midia_data();
            $produtoModel->seo = $this->seo_data();
            $produtoModel->addresses = $this->addresses_data();

            $produtoModel->programacaoDatas = $this->adicionarProgramacaoData();
            $produtoModel = $this->adicionarDisponibilidadeRecorrente($produtoModel);

            //$this->sma->print_arrays($data, $_POST);
        }

        if ($this->form_validation->run() == true) {
            $produtoId = $this->ProdutoService_model->adicionarProduto($produtoModel);

            $this->output->delete_all_cache();//TODO LIMPAR CACHE

            $this->session->set_flashdata('message', lang("product_added"));

            redirect('products/edit/'.$produtoId);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['product'] = $id ? $this->products_model->getProductByID($id) : NULL;
            $this->data['tiposCobranca'] = $tiposCobranca;
            $this->data['condicoesPagamento'] = $this->site->getAllCondicoesPagamento();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();
            $this->data['destinos'] = $this->site->getDestinos();
            $this->data['fornecedores'] = $this->products_model->getAllCustomerCompanies();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $id ? $this->products_model->getAllWarehousesWithPQ($id) : NULL;
            $this->data['tiposTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
            $this->data['tiposQuarto'] = $this->TipoQuartoRepository_model->getTiposQuarto();
            $this->data['hospedagens'] = $this->products_model->getVariacaoHospedagens();
            $this->data['combo_items'] = ($id && $this->data['product']->type == 'combo') ? $this->products_model->getProductComboItems($id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $this->data['valorFaixas'] = $this->ValorFaixaRepository_model->getValoreFaixas();
            $this->data['configuracoesExtraAssento'] = $this->products_model->getConfiguracoesExtraAssento();
            $this->data['condicoesPagamento']   = $this->settings_model->getAllCondicoesPagamentoExibirLink();
            $this->data['cores']   = $this->CorAgendaRepository_model->getAll();
            $this->data['tiposTrajeto']   = $this->TipoTrajetoVeiculoRepository_model->getAll();
            $this->data['servicos_incluso'] = $this->site->getAllServicosIncluso();
            $this->data['contracts'] = $this->ContractRepository_model->getAll();

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('products'), 'page' => lang('products')),
                array('link' => '#', 'page' => lang('add_product'))
            );

            $meta = array('page_title' => lang('add_product'), 'bc' => $bc);
            $this->page_construct('products/add', $meta, $this->data);
        }
    }

    private function adicionarTransportes() {
        $a = sizeof($_POST ['tipoTransporte']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['tipoTransporte'] [$r] )) {
                $tiposTransporte [] = array (
                    'tipoTransporte' => $_POST ['tipoTransporte'] [$r],
                    'configuracao_assento_extra_id' => $_POST ['configuracao_assento_extra_id'] [$r],
                    'habilitar_selecao_link' => $this->adicionarTransporteAtivoLinkReserva($_POST ['tipoTransporte'] [$r]),
                    'status' => $this->adicionarTransporteAtivo($_POST ['tipoTransporte'] [$r]),
                );
            }
        }
        return $tiposTransporte;
    }

    private function adicionarTransporteAtivo($tipoTransporte) {
        $a = sizeof($_POST ['tipoTransporte']);
        $ativo      = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId    = $_POST ['ativarTipoTransporte'] [$r];
            if ($ativoId == $tipoTransporte) $ativo = 'ATIVO';
        }
        return $ativo;
    }

    private function adicionarTransporteAtivoLinkReserva($tipoTransporte) {
        $a = sizeof($_POST ['tipoTransporte']);
        $ativo      = 0;

        for($r = 0; $r <= $a; $r ++) {
            $ativoId    = $_POST ['habilitar_selecao_link'] [$r];
            if ($ativoId == $tipoTransporte) $ativo = 1;
        }
        return $ativo;
    }

    private function adicionarFaixaEtariaValoresServicoAdicional() {

        $a = sizeof ( $_POST ['servicoAdicionalId'] );

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['servicoAdicionalId'] [$r] )) {

                $valor  = $_POST ['servicoAdicionalValor'] [$r];
                $status = $_POST ['isAtivoServicoAdicional'] [$r];

                if ($status == 'true') {
                    $servicosAdicionaisFaixaEtaria[] = $this->getAdicionarFaixaEtariaServicoAdicionalArray($valor, $_POST['servicoAdicionalId'] [$r], 'ATIVO',  $_POST['faixaIdServicoAdicional'] [$r]);
                } else {
                    $servicosAdicionaisFaixaEtaria[] = $this->getAdicionarFaixaEtariaServicoAdicionalArray(0, $_POST['servicoAdicionalId'] [$r], 'INATIVO', $_POST['faixaIdServicoAdicional'] [$r]);
                }

            }
        }

        return $servicosAdicionaisFaixaEtaria;
    }

    private function getAdicionarFaixaEtariaServicoAdicionalArray($valor, $servicoAdicionalId, $ativo, $faixaId) {
        $tiposHospedagemFaixaEtaria = array (
            'valor' => $valor,
            'servico' => $servicoAdicionalId,
            'status' => $ativo,
            'tipo' => '',
            'faixaId' => $faixaId,
        );

        return $tiposHospedagemFaixaEtaria;
    }


    private function adicionarFaixaEtariaValorAtivo($tipo) {
        $a = sizeof($_POST ['tipoFaixaEtariaValorConfigure']);
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoFaixaId = $_POST ['ativarTipoFaixaEtaria'] [$r];

            if ($ativoFaixaId == $tipo) $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarExtra() {
        $a = sizeof($_POST ['attr_nome_extra']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['attr_nome_extra'] [$r] )) {
                $extras [] = array (
                    'name' => $_POST ['attr_nome_extra'] [$r],
                    'note' => $_POST ['attr_note_extra'] [$r],
                );
            }
        }
        return $extras;
    }

    private function adicionarLocaisEmbarque() {
        $a = sizeof($_POST ['localEmbarque']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['localEmbarque'] [$r] )) {
                $locaisEmbarque [] = array (
                    'localEmbarque' => $_POST ['localEmbarque'] [$r],
                    'horaEmbarque' => $_POST ['horaEmbarque'] [$r],
                    'dataEmbarque' => $_POST ['dataEmbarque'] [$r],
                    'note' => $_POST ['noteEmbarque'] [$r],
                    'status' => $this->adicionarLocaisEmbarqueAtivo($_POST ['localEmbarque'] [$r])
                );
            }
        }
        return $locaisEmbarque;
    }

    private function adicionarLocaisEmbarqueAtivo($localEmbarque) {
        $a = sizeof($_POST ['localEmbarque']);
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativoLocalEmbarque'] [$r];

            if ($ativoId == $localEmbarque)  $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarTipoCobrancaProduto($tiposCobranca) {
        foreach ($tiposCobranca as $tipoCobranca) {
            $tbCobranca_data [] = array (
                'tipoCobrancaId' => $tipoCobranca->id,
                'status' => $_POST['ativoTipoCobrancaProduto'.$tipoCobranca->id] == null ? FALSE : TRUE,
                'diasAvancaPrimeiroVencimento' => $_POST['diasAvancaPrimeiroVencimento'.$tipoCobranca->id] == null ? 0 : $_POST['diasAvancaPrimeiroVencimento'.$tipoCobranca->id],
                'diasMaximoPagamentoAntesViagem' => $_POST['diasMaximoPagamentoAntesViagem'.$tipoCobranca->id] == null ? 0 : $_POST['diasMaximoPagamentoAntesViagem'.$tipoCobranca->id],
                'taxaIntermediacao' => $_POST['taxaIntermediacao'.$tipoCobranca->id] == null ? 0 : $_POST['taxaIntermediacao'.$tipoCobranca->id],
                'taxaFixaIntermediacao' => $_POST['taxaFixaIntermediacao'.$tipoCobranca->id] == null ? 0 : $_POST['taxaFixaIntermediacao'.$tipoCobranca->id],
                'numero_max_parcelas' => $_POST['numero_max_parcelas'.$tipoCobranca->id] == null ? 12 : $_POST ['numero_max_parcelas'.$tipoCobranca->id],
                'numero_max_parcelas_sem_juros' => $_POST['numero_max_parcelas_sem_juros'.$tipoCobranca->id] == null ?  0 :  $_POST ['numero_max_parcelas_sem_juros'.$tipoCobranca->id],
                'assumir_juros_parcelamento' => $_POST ['assumir_juros_parcelamento'.$tipoCobranca->id] == null ? 12 : $_POST ['assumir_juros_parcelamento'.$tipoCobranca->id],
                'exibirNaSemanaDaViagem' => $_POST ['exibirNaSemanaDaViagem'.$tipoCobranca->id] == 1 ? 1 : 0,
                'usarTaxasPagSeguro' => $_POST ['usarTaxasPagSeguro'.$tipoCobranca->id] == 1 ? 1 : 0,
                'sinal'                     => $_POST['sinal'.$tipoCobranca->id] == null ?  0.00 :  $_POST ['sinal'.$tipoCobranca->id],
                'tipo_sinal'                => $_POST['tipo_sinal'.$tipoCobranca->id],
                'acrescimo_desconto_sinal'  => $_POST['acrescimo_desconto_sinal'.$tipoCobranca->id],
                'valor_acres_desc_sinal'    => $_POST['valor_acres_desc_sinal'.$tipoCobranca->id] == null ?  0.00 :  $_POST ['valor_acres_desc_sinal'.$tipoCobranca->id],
                'tipo_acres_desc_sinal'     => $_POST['tipo_acres_desc_sinal'.$tipoCobranca->id],
                'is_sinal'                  => !($_POST['is_sinal' . $tipoCobranca->id] == null),
            );
        }

        return $tbCobranca_data;
    }


    private function adicionarCondicaoPagamentoProduto() {
        $a = sizeof($_POST ['tipoCobrancaCondicaoPagamento']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['tipoCobrancaCondicaoPagamento'] [$r] )) {
                $tiposCobranca [] = array (
                    'tipoCobrancaId' => $_POST ['tipoCobrancaId'] [$r],
                    'condicaoPagamentoId' => $_POST ['condicaoPagamentoId'] [$r],
                    'acrescimoDescontoTipo' => $_POST ['acrescimoDescontoCondicaoPagamento'] [$r],
                    'tipo' => $_POST ['tipoCobrancaCondicaoPagamento'] [$r],
                    'valor' => $_POST ['precoCondicaoPagamento'] [$r],
                    'ativo' => $this->adicionarTipoCobrancaCondicaoPagamentoAtivo($_POST ['tipoCobrancaId'] [$r].'_'.$_POST ['condicaoPagamentoId'] [$r]),
                );
            }
        }
        return $tiposCobranca;
    }


    private function adicionarTipoCobrancaCondicaoPagamentoAtivo($tipoCobrancaCondicaoPagamento) {
        $a = sizeof($_POST ['tipoCobrancaId']);
        $ativo = FALSE;

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativoCondicaoPagamento'] [$r];

            if ($ativoId == $tipoCobrancaCondicaoPagamento) {
                $ativo = TRUE;
            }
        }

        return $ativo;
    }

    private function adicionarServicosInclusos() {

        $a = sizeof($_POST ['servicoIncluiId']);

        for ($r = 0; $r <= $a; $r++) {
            if (isset ($_POST ['servicoIncluiId'] [$r])) {

                $inclusos [] = array(
                    'active' => $this->adicionarServicosInclusosAtivo($_POST['servicoIncluiId'] [$r]),
                    'service_id' => $_POST['servicoIncluiId'] [$r],
                );
            }
        }

        return $inclusos;
    }

    private function adicionarServicosInclusosAtivo($tipo) {
        $a = sizeof($_POST ['servicoIncluiId']);
        $ativo = false;

        for($r = 0; $r <= $a; $r ++) {
            $ativoServicoId = $_POST ['ativarServicoInclui'] [$r];

            if ($ativoServicoId == $tipo) {
                $ativo = true;
            }
        }

        return $ativo;
    }


    private function adicionarFaixaEtariaValor() {

        if ($this->input->post('isValorPorFaixaEtaria')) {
            $a = sizeof($_POST ['tipoFaixaEtariaValorConfigure']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['tipoFaixaEtariaValorConfigure'] [$r])) {

                    $faixaEtariaValores [] = array(
                        'faixaId' => $_POST['valorFaixaId'] [$r],
                        'valor' => $_POST['valorFaixaEtariaValorConfigure'] [$r],
                        'status' => $this->adicionarFaixaEtariaValorAtivo($_POST['valorFaixaId'] [$r]),
                        'tipo' => $_POST['tipoFaixaEtariaValorConfigure'] [$r],

                        'comissao' => 0,
                        'tipoComissao' => '0',
                        'idadeInicio' => 0,
                        'idadeFinal' => 0,
                        'note' => $_POST['notaFaixaEtaria'] [$r],
                    );
                }
            }

            return $faixaEtariaValores;
        }

        return [];
    }

    private function adicionarFaixaEtariaTipoHospedagem() {

        if ($this->input->post('isHospedagem')) {

            $a = sizeof($_POST ['faixaTipoHospedagemId']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['faixaTipoHospedagemId'] [$r])) {

                    $tiposHospedagemFaixaEtaria [] = array(
                        'faixaId' => $_POST['valorFaixaIdHospedagem'] [$r],
                        'tipoHospedagem' => $_POST['faixaTipoHospedagemId'] [$r],
                        'valor' => $_POST['valorFaixaEtariaValorHospedagem'] [$r],
                        'tipo' => $_POST['tipoFaixaEtariaValorHospedagem'] [$r],
                        'status' => $this->adicionarTipoHospedagemFaixaValorAtivo($_POST ['faixaTipoHospedagemId'] [$r], $_POST['valorFaixaIdHospedagem'] [$r]),

                        'comissao' => 0,
                        'tipoComissao' => '',
                    );
                }
            }
            return $tiposHospedagemFaixaEtaria;
        }

        return [];
    }

    private function adicionarTipoHospedagemFaixaValorAtivo($tipoHospedagemId, $faixaId) {
        $a = sizeof ( $_POST ['faixaTipoHospedagemId'] );
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativarValorHospedagem'] [$r];

            if ($ativoId == $tipoHospedagemId.'_'.$faixaId) {
                $ativo = 'ATIVO';
            }
        }

        return $ativo;
    }

    private function adicionarTipoHospedagemViagem() {

        if ($this->input->post('isHospedagem')) {

            $a = sizeof($_POST ['attr_name']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['attr_name'] [$r])) {
                    $tiposHospedagem[] = array(
                        'preco' => $_POST ['attr_price'] [$r],
                        'tipoHospedagem' => $_POST ['tipoHospedagemId'] [$r],
                        'estoque' => $_POST ['estoque_hospedagem'] [$r],
                        'status' => $this->adicionarTipoHospedagemAtivo($_POST ['tipoHospedagemId'] [$r]),
                    );
                }
            }
            return $tiposHospedagem;
        }

        return [];
    }

    private function getTotalVagasPorTipoHospedagem() {

        if ($this->input->post('isHospedagem') && $this->input->post('controle_estoque_hospedagem') ) {
            $a = sizeof($_POST ['attr_name']);
            $estoque = 0;
            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['attr_name'] [$r])) {
                    $estoque = $estoque + $_POST ['qtd_pessoas_hospedagem'] [$r];
                }
            }
            return $estoque;
        }

        return 0;
    }

    private function adicionarTipoHospedagemAtivo($tipoHospedagem) {
        $a = sizeof ( $_POST ['attr_name'] );
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativoTipoHospedagem'] [$r];

            if ($ativoId == $tipoHospedagem) $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarAtributosDaViagem($data) {

        $Settings = $this->site->get_setting();

        $product_attributes [] = array (
            'name' => 'Passagem',//TODO VARIACAO DA PASSAGEM
            'warehouse_id' => $Settings->default_warehouse,//TODO ID DA FILIAL
            'quantity' => $data['quantity'],
            'cost' => $data['cost'],
            'price' => $data['price'],
            'fornecedor' => 0,
            'totalPoltrona' => 0,
            'totalPessoasConsiderar' => 0,
            'localizacao' => null,
            'totalPreco' => null,
        );

        $a = sizeof ( $_POST ['attr_name'] );
        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['attr_name'] [$r] )) {
                $product_attributes [] = array (
                    'name' => $_POST ['attr_name'] [$r],
                    'warehouse_id' => $Settings->default_warehouse,//TODO ID DA FILIAL
                    'quantity' => 0,
                    'cost' => $_POST ['attr_cost'] [$r],
                    'price' => $_POST ['attr_price'] [$r],
                    'fornecedor' => $_POST ['attr_fornecedor'] [$r],
                    'totalPoltrona' => $_POST ['att_totalPoltrona'] [$r],
                    'totalPessoasConsiderar' => $_POST ['att_totalPessoasConsiderar'] [$r],
                    'localizacao' => $_POST ['attr_localizacao'] [$r],
                    'totalPreco' => $_POST ['attr_totalPreco'] [$r]
                );
            }
        }
        return $product_attributes;
    }

    private function adicionarServicosAdicionais() {
        $c           = sizeof($_POST['combo_item_id']) - 1;
        for ($r = 0; $r <= $c; $r++) {
            if (isset($_POST['combo_item_code'][$r]) &&
                isset($_POST['combo_item_id'][$r]) &&
                isset($_POST['combo_item_price'][$r])) {
                $items[] = [
                    'quantity'      => 1,
                    'item_code'     => $_POST['combo_item_code'][$r],
                    'unit_price'    => $_POST['combo_item_price'][$r],
                    'item_id'       => $_POST['combo_item_id'][$r],
                    'comissao'      => $_POST['servicoAdicionalValorComissao'][$r],
                    'fornecedorId'  => $_POST['servicoAdicionalFornecedor'][$r],
                ];
            }
        }

        return $items;
    }

    private function adicionarFotosAdicionais() {

        $photos = null;
        $this->load->library('upload');

        if ($_FILES['userfile']['name'][0] == "") return NULL;

        $config['upload_path'] = $this->upload_path;
        $config['allowed_types'] = $this->image_types;

        if ($this->Settings->isIntegracaoSite == 1) {
            $config['max_size'] = '1024';
        } else {
            $config['max_size'] = $this->allowed_file_size;
        }

        $config['max_width'] = $this->Settings->iwidth;
        $config['max_height'] = $this->Settings->iheight;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $config['max_filename'] = 25;

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/add");
            } else {

                $pho = $this->upload->file_name;
                $photos[] = $pho;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $pho;
                $config['new_image'] = $this->thumbs_path . $pho;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;

                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $pho;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
            }
        }

        $config = NULL;
        $_FILES = $files;

        return $photos;
    }

    private function adicionarFotoPrincipal($add = true) {

        $this->load->library('upload');

        if ($add) $photo = 'no_image.png';
        else $photo = false;

        if ($_FILES['product_image']['size'] > 0) {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('product_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/add");
            }

            $photo = $this->upload->file_name;

            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';

                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }

        return $photo;
    }

    function getValoresPorFaixaEtaria() {

        $productId = $this->input->get('productId');
        $programacaoId = $this->input->get('programacaoId');
        $retorno = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($productId);

        if ($retorno == null) {
            $this->sma->send_json([]);
        } else  {
            if ($this->Settings->receptive) {
                $product = $this->site->getProductByID($productId);
                if ($product->cat_precificacao == 'preco_por_data') {
                    $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
                    foreach ($retorno as $data) {
                        $data->text = $data->name.' '.$this->sma->formatMoney($programacao->preco);
                        $data->valor = $programacao->preco;
                    }
                }
            }
            $this->sma->send_json($retorno);
        }
    }

    function getLocaisEmbarque() {

        $productId  = $this->input->get('productId');
        $product    = $this->products_model->getProductByID($productId);
        $isPasseio  = $product->passeio;//TODO EXIBE TODOS OS EMBARQUE PARA PACOTES DO RECEPTIVO

        if ($isPasseio) {
            $this->sma->send_json($this->site->getLocaisEmbarque());
        } else {
            $this->sma->send_json($this->ProdutoRepository_model->getLocaisEmbarque($productId));
        }
    }

    function getTransportes() {
        $productId = $this->input->get('productId');
        $this->sma->send_json($this->ProdutoRepository_model->getTransportes($productId));
    }

    function getFaixasTipoHospedagemProduto() {
        $productId = $this->input->get('productId');

        $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getFaixasTipoHospedagemProduto($productId);

        $this->sma->send_json($valoresPorTipoHospedagem);
    }

    function getTipoHospedagem() {

        $productId      = $this->input->get('productId');
        $faixaId        = $this->input->get('faixaId');
        $programacaoId  = $this->input->get('programacao_id');

        $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagem($productId, $faixaId);

        if (!$valoresPorTipoHospedagem) {
            $valoresPorTipoHospedagem = $this->ProdutoRepository_model->getTipoHospedagem($productId);
            $this->sma->send_json($this->getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId));
        } else {
            $this->sma->send_json($this->getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId));
        }
    }

    private function getTipoHospedagemConsultaDisponibilidade($valoresPorTipoHospedagem, $faixaId, $programacaoId) {
        $data = [];

        //$faixaEtaria = $this->ValorFaixaRepository_model->getById($faixaId);

        foreach ($valoresPorTipoHospedagem as $tipo_hospedagem) {

            $agenda = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
            $vagas  = $this->AgendaViagemService_model->vagas($programacaoId, $agenda->getControleEstoqueHospedagem(), $tipo_hospedagem->id);
            $label  = '';

            $totalQuartosDisponivel = 0;

            foreach ($agenda->getQuartos() as $quarto) {
                if ($quarto->tipo_hospedagem == $tipo_hospedagem->id) {
                    $totalQuartosDisponivel = $quarto->qtd_quartos_disponivel;
                }
            }

            if ($agenda->getControleEstoqueHospedagem()) {
                $label .= ' ('.$vagas.' Vaga(s) disponíveis | Quarto(s) Disponíveis: '.$totalQuartosDisponivel.')';
            } else {
                if ($agenda->getTotalDisponvel() > 1) {
                    $label .=  '  ('.$agenda->getTotalDisponvel() .' Vagas disponível)';
                } else {
                    $label .= ' (' . $agenda->getTotalDisponvel() . ' Vaga disponível)';
                }
            }

            $tipo_hospedagem->text = $tipo_hospedagem->text.$label;

            /*
            if ($agenda->getControleEstoqueHospedagem()) {
                if ($vagas > 0 || !$faixaEtaria->descontarVaga) {
                    $data[] = $tipo_hospedagem;
                }
            } else {
                $data[] = $tipo_hospedagem;
            }
            */

            $data[] = $tipo_hospedagem;
        }

        return $data;
    }

    function getServicosAdicionais() {

        $productId = $this->input->get('productId');
        $faixaId = $this->input->get('faixaId');

        $servicos = $this->ProdutoRepository_model->getServicosAdicionais($productId);

        foreach ($servicos as $servico) {
            $adicionais = $this->ProdutoRepository_model->getServicosAdicionaisPorFaixaEtaria($productId, $servico->id, $faixaId);

            foreach ($adicionais as $adicional) {
                $pr[] = array(
                    'id' => $adicional->id,
                    'text' => $adicional->text,
                    'code' => $adicional->code,
                    'price' => $adicional->price,
                    'category_id' => $adicional->category_id,
                    'category' => $adicional->category,
                );
            }

        }
        $this->sma->send_json($pr);
    }

    function suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function suggestions_adicionais()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductAdicionaisNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function get_suggestions()
    {
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductsForPrinting($term);
        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptions($row->id);
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => $variants);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function addByAjax()
    {
        if (!$this->mPermissions('add')) {
            exit(json_encode(array('msg' => lang('access_denied'))));
        }
        if ($this->input->get('token') && $this->input->get('token') == $this->session->userdata('user_csrf') && $this->input->is_ajax_request()) {
            $product = $this->input->get('product');
            if (!isset($product['code']) || empty($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_is_required'))));
            }
            if (!isset($product['name']) || empty($product['name'])) {
                exit(json_encode(array('msg' => lang('product_name_is_required'))));
            }
            if (!isset($product['category_id']) || empty($product['category_id'])) {
                exit(json_encode(array('msg' => lang('product_category_is_required'))));
            }
            if (!isset($product['unit']) || empty($product['unit'])) {
                exit(json_encode(array('msg' => lang('product_unit_is_required'))));
            }
            if (!isset($product['price']) || empty($product['price'])) {
                exit(json_encode(array('msg' => lang('product_price_is_required'))));
            }
            if (!isset($product['cost']) || empty($product['cost'])) {
                exit(json_encode(array('msg' => lang('product_cost_is_required'))));
            }
            if ($this->products_model->getProductByCode($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_already_exist'))));
            }
            if ($row = $this->products_model->addAjaxProduct($product)) {
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $pr = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'qty' => 1, 'cost' => $row->cost, 'name' => $row->name, 'tax_method' => $row->tax_method, 'tax_rate' => $tax_rate, 'discount' => '0');
                $this->sma->send_json(array('msg' => 'success', 'result' => $pr));
            } else {
                exit(json_encode(array('msg' => lang('failed_to_add_product'))));
            }
        } else {
            json_encode(array('msg' => 'Invalid token'));
        }

    }

    /* -------------------------------------------------------- */
    function edit($id = NULL)
    {

        $this->sma->checkPermissions();
        $this->load->helper('security');

        if ($this->input->post('id')) $id = $this->input->post('id');

        $product        = $this->site->getProductByID($id);
        $tiposCobranca  = $this->site->getAllTiposCobrancaDebito(true);

		if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->form_validation->run('products/add') == true) {

            $produtoModel = new ProdutoModel_model();

            $details    =  $this->input->post('details');
            $active     = $this->input->post('active');
            $unit       = 'Confirmado';
            $isBateVolta = $this->input->post('isValorPorFaixaEtaria');
            $isControleEstoquePorTipoHospedagem = $this->input->post('controle_estoque_hospedagem');
            $type_calendar = $this->input->post('type_calendar');
            $isPasseio = FALSE;

            if (!$active) {
                $unit = 'Inativo';
            }

            if ($this->input->post('receptivo') == 'receptivo') {
                $isPasseio = TRUE;
                $type_calendar = 1;//calendario
            }

            if ($isBateVolta) {
                $isControleEstoquePorTipoHospedagem = false;
            }

            $data = array(
                'quantidadePessoasViagem' 		=> $this->input->post("quantidadePessoasViagem"),
                'quantity'                      => $this->input->post("quantidadePessoasViagem"),
                'name' 				            => $this->input->post('name'),
                'unit'                          => $unit,
                'tempo_viagem' 		            => $this->input->post('tempo_viagem'),
                'enviar_site' 		            => $this->input->post('enviar_site'),
                'apenas_cotacao' 		        => $this->input->post('apenas_cotacao'),
                'alertar_polcas_vagas' 		    => $this->input->post('alertar_polcas_vagas'),
                'alertar_ultimas_vagas' 		=> $this->input->post('alertar_ultimas_vagas'),
                'price_pacote_zero_cinco' 		=> $this->input->post('price_pacote_zero_cinco'),
                'price_pacote_seis_onze' 		=> $this->input->post('price_pacote_seis_onze'),
                'price_pacote_acima_sessenta'   => $this->input->post('price_pacote_acima_sessenta'),
                'percentual_desconto_a_vista' 	=> $this->input->post('percentual_desconto_a_vista'),
                'categorias' 		            => $this->input->post('categorias'),
                'instrucoescontrato' 		    => $this->input->post('instrucoescontrato'),
                'tipo_transporte' 		        => $this->input->post('tipo_transporte'),
                'itinerario' 		            => $this->input->post('itinerario'),
                'oqueInclui' 		            => $this->input->post('oqueInclui'),
                'valores_condicoes'             => $this->input->post('valores_condicoes'),
                'duracao_pacote'                => $this->input->post('duracao_pacote'),
                'numero_pessoas' 	            => $this->input->post('numero_pessoas'),
                'status' 			            => $this->input->post('status'),
                'data_saida' 		            => $this->input->post('data_saida'),
                'origem' 			            => $this->input->post('origem'),
                'previsao_chegada' 	            => $this->input->post('previsao_chegada'),
                'data_chegada' 		            => $this->input->post('data_chegada'),
                'destino' 			            => $this->input->post('destino'),
                'data_retorno'		            => $this->input->post('data_retorno'),
                'hora_saida'		            => $this->input->post('hora_saida'),
                'hora_retorno'		            => $this->input->post('hora_retorno'),
                'margem_padrao' 	            => $this->input->post('margem_padrao'),
                'simbolo_moeda' 	            => $this->input->post('simbolo_moeda'),
                'valor_pacote' 	                => $this->input->post('valor_pacote'),
                'precoExibicaoSite' 	        => $this->input->post('precoExibicaoSite'),
                'simboloMoeda'                  => $this->input->post('simboloMoeda'),
                'local_retorno' 	            => $this->input->post('local_retorno'),
                'hora_chegada' 	                => $this->input->post('hora_chegada'),
                'category_id' 		            => $this->input->post('category'),
                'subcategory_id' 	            => $this->input->post('subcategory') ,
                'cost' 				            => $this->sma->formatDecimal($this->input->post('price')),
                'price'	 			            => $this->sma->formatDecimal($this->input->post('price')),
                'tax_rate' 			            => $this->input->post('tax_rate'),
                'tax_method' 		            => $this->input->post('tax_method'),
                'alert_quantity' 	            => 1,
                'track_quantity' 	            => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' 			            => $details,
                'product_details' 	            => $this->input->post('product_details'),
                'supplier1' 		            => $this->input->post('supplier'),
                'supplier1price' 	            => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' 		            => $this->input->post('supplier_2'),
                'supplier2price' 	            => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' 		            => $this->input->post('supplier_3'),
                'supplier3price' 	            => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' 		            => $this->input->post('supplier_4'),
                'supplier4price' 	            => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' 		            => $this->input->post('supplier_5'),
                'supplier5price' 	            => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' 				            => $this->input->post('cf1'),
                'cf2' 				            => $this->input->post('cf2'),
                'cf3' 				            => $this->input->post('cf3'),
                'cf4' 				            => $this->input->post('cf4'),
                'cf5' 				            => $this->input->post('cf5'),
                'cf6' 				            => $this->input->post('cf6'),
                'alert_stripe' 		            => $this->input->post('alert_stripe'),
                'promotion' 		            => $this->input->post('promotion'),
                'promo_price' 		            => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' 		            => $this->sma->fsd($this->input->post('start_date')),
                'end_date' 			            => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no'             => $this->input->post('supplier_part_no'),
                'supplier2_part_no'             => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no'             => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no'             => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no'             => $this->input->post('supplier_5_part_no'),
                'isServicoOnline'               => $this->input->post('isServicoOnline'),
                'controle_estoque_hospedagem'   => $isControleEstoquePorTipoHospedagem,
                'isTransporteTuristico'         => $this->input->post('isTransporteTuristico'),
                'isHospedagem'                  => $this->input->post('isHospedagem'),
                'isServicosAdicionais'          => $this->input->post('isServicosAdicionais'),
                'isValorPorFaixaEtaria'         => $this->input->post('isValorPorFaixaEtaria'),
                'permiteVendaMenorIdade'        => $this->input->post('permiteVendaMenorIdade'),
                'permiteVendaClienteDuplicidade'=> $this->input->post('permiteVendaClienteDuplicidade'),
                'isTaxasComissao'               => $this->input->post('isTaxasComissao') == null ? 0 : $this->input->post('isTaxasComissao'),
                'tipoComissao'                  => $this->input->post('tipoComissao'),
                'tipoCalculoComissao'           => $this->input->post('tipoCalculoComissao'),
                'comissao'                      => $this->input->post('comissao'),
                'isApenasColetarPagador'        => $this->input->post('isApenasColetarPagador'),
                'isEmbarque'                    => $this->input->post('isEmbarque'),
                'isComissao'                    => $this->input->post('isComissao'),
                'permitirListaEmpera'           => $this->input->post('permitirListaEmpera'),
                'active'                        => $this->input->post('active'),
                'captarEnderecoLink'            => $this->input->post('captarEnderecoLink'),
                'destaque'                      => $this->input->post('destaque'),

                'color_agenda_id'               => $this->input->post('color_agenda_id'),
                'tipo_trajeto_id'               => $this->input->post('tipo_trajeto_id'),
                'duracao_atividade'             => $this->input->post('duracao_atividade'),
                'tipo_duracao_atividade'        => $this->input->post('tipo_duracao_atividade'),
                'cat_precificacao'              => $this->input->post('cat_precificacao'),
                'desc_duracao'                  => $this->input->post('desc_duracao'),
                'product_action'                => $this->input->post('product_action'),
                'product_action_text'           => $this->input->post('product_action_text'),
                'contract_id'                   => $this->input->post('contract_id'),

                'permite_pagamento_total'       => $this->input->post('permite_pagamento_total'),
                'obriga_sinal'                  => $this->input->post('obriga_sinal'),
                'avaliar'                       => $this->input->post('avaliar') != null ? $this->input->post('avaliar') : false,
                'avaliar_dias'                  => $this->input->post('avaliar_dias') != null ? $this->input->post('avaliar_dias') : 2,

                'hospedagem_id'                 => $this->input->post('hospedagem_id'),

                'type_calendar'                 => $type_calendar,
                'passeio'                       => $isPasseio,
            );

            $image = $this->adicionarFotoPrincipal(false);

            if ($image) {
                $data['image'] = $image;
            }

            $produtoModel->data = $data;
            $produtoModel->extras = $this->adicionarExtra();
            $produtoModel->items = $this->adicionarServicosAdicionais();
            $produtoModel->photos =  $this->adicionarFotosAdicionais();
            $produtoModel->product_attributes = $this->adicionarAtributosDaViagem($data);

            $produtoModel->faixaEtariaValores = $this->adicionarFaixaEtariaValor();
            $produtoModel->servicos_inclui = $this->adicionarServicosInclusos();

            $produtoModel->locaisEmbarque = $this->adicionarLocaisEmbarque();
            $produtoModel->condicoesPagamento = $this->adicionarCondicaoPagamentoProduto();
            $produtoModel->tiposCobranca = $this->adicionarTipoCobrancaProduto($tiposCobranca);
            $produtoModel->transportes = $this->adicionarTransportes();
            $produtoModel->tiposHospedagem = $this->adicionarTipoHospedagemViagem();
            $produtoModel->tiposHospedagemFaixaEtaria = $this->adicionarFaixaEtariaTipoHospedagem();
            $produtoModel->faixaEtariaValoresServicoAdicional = $this->adicionarFaixaEtariaValoresServicoAdicional();
            $produtoModel->midia_data = $this->midia_data();
            $produtoModel->seo = $this->seo_data();
            $produtoModel->addresses = $this->addresses_data();

            //disponbilidade de datas
            $produtoModel->programacaoDatas = $this->adicionarProgramacaoData();
            $produtoModel = $this->adicionarDisponibilidadeRecorrente($produtoModel);
        }

        if ($this->form_validation->run() == true &&
            $this->ProdutoService_model->editarProduto($produtoModel, $id)) {

            $this->output->delete_all_cache();//TODO LIMPAR CACHE

            $agendamentos = $this->AgendaViagemService_model->getProgramacao($id);

            if (count($agendamentos) > 0) {
                $this->session->set_flashdata('message', lang("product_updated"));
            } else {
                $this->session->set_flashdata('warning', lang("product_updated_not_date"));
            }

 			redirect('products/edit/'.$id);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['fornecedores'] = $this->products_model->getAllCustomerCompanies();
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['destinos'] = $this->site->getDestinos();
            $this->data['product_variants'] = $this->products_model->getProductOptions($id);

            $combo_items = $this->products_model->getProductComboItems($id);

            foreach ($combo_items as $itemCombo) {

                $valoresPorFaixaEtaria = $this->ProdutoRepository_model->getServicosAdicionaisFaixaEtariaRodoviario($id, $itemCombo->id);

                foreach ($valoresPorFaixaEtaria as $valorPorFaixaEtaria) {
                    $valores[] = array(
                        'servico' => $valorPorFaixaEtaria->servico,
                        'faixaId' => $valorPorFaixaEtaria->faixaId,
                        'valor' => $valorPorFaixaEtaria->valor,
                        'status' => $valorPorFaixaEtaria->status == 'ATIVO' ? 'true' : 'false'
                    );
                }

                $itemCombo->valores = $valores;
            }

            $agendamentos               = $this->AgendaViagemService_model->getProgramacao($id);
            $transporteRodoviarioAtivo  =  $this->ProdutoRepository_model->getTransportes($id);
            $valoresBateVoltaAtivo      = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($id);
            $valoresHospedagemAtivo     =  $this->ProdutoRepository_model->getTipoHospedagem($id);
            $locaisEmbarqueAtivo        = $this->ProdutoRepository_model->getLocaisEmbarque($id);
            $tiposCobrancaAtivo         = $this->site->getAllFormasPagamentoProdutoExibirLinkCompra($id);

            $warning = '';


            if (!$product->passeio) {//TODO SE NAO FOR PASSEIO MOSTRA MENSAGEM DE TRANSPORTE E LOCAL DE EMBARQUE
                if (!$transporteRodoviarioAtivo) $warning .= lang("product_not_transporte").'<br/>';
                if (!$locaisEmbarqueAtivo) $warning .= lang("product_not_local_embarque").'<br/>';
            }

            if (count($agendamentos) == 0) {
                $warning = lang("product_not_date").'<br/>';
            }
            if (!$valoresBateVoltaAtivo && !$valoresHospedagemAtivo) {
                $warning .= lang("product_not_hospedagem").'<br/>';
            }

            if ($product->isTaxasComissao && !$tiposCobrancaAtivo) {
                $warning .= lang("product_not_tipos_cobranca").'<br/>';
            }

            if ($warning) {
                $this->session->set_flashdata('error', $warning);
            }

            $this->data['product'] = $product;
            $this->data['combo_items'] = $combo_items;
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();
            $this->data['tiposTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
            $this->data['tiposQuarto'] = $this->TipoQuartoRepository_model->getTiposQuarto($product->hospedagem_id);
            $this->data['hospedagens'] = $this->products_model->getVariacaoHospedagens();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['images'] = $this->products_model->getProductPhotos($id);
            $this->data['imagesFotos'] = $this->products_model->getProductPhotosSite($id);
            $this->data['datasAgendadas'] = $this->AgendaViagemService_model->getProgramacao($id);
            $this->data['transportes'] =  $this->ProdutoRepository_model->getTransportesRodoviario($id);
            $this->data['product_details'] = $this->ProductDetailsRepository_model->getAll($id);
            $this->data['valorFaixas'] = $this->ValorFaixaRepository_model->getValoreFaixas();
            $this->data['configuracoesExtraAssento'] = $this->products_model->getConfiguracoesExtraAssento();
            $this->data['tiposCobranca']        = $tiposCobranca;
            $this->data['condicoesPagamento']   = $this->settings_model->getAllCondicoesPagamentoExibirLink();
            $this->data['cores']   = $this->CorAgendaRepository_model->getAll();
            $this->data['tiposTrajeto']   = $this->TipoTrajetoVeiculoRepository_model->getAll();
            $this->data['agenda_programacoes'] = $this->AgendaProgramacaoRepository_model->getAll($id);
            $this->data['servicos_incluso'] = $this->site->getAllServicosIncluso();
            $this->data['contracts'] = $this->ContractRepository_model->getAll();

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('products'), 'page' => lang('products')),
                array('link' => '#', 'page' => lang('edit_product'))
            );

            $meta = array('page_title' => lang('edit_product'), 'bc' => $bc);
            $this->page_construct('products/edit', $meta, $this->data);
        }
    }

    /* -------------------------------------------------------- */
    function duplicar($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');

        if ($this->input->post('id')) $id = $this->input->post('id');

        $product = $this->site->getProductByID($id);
        $tiposCobranca = $this->site->getAllTiposCobrancaDebito(true);

        if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->form_validation->run('products/add') == true) {

            $produtoModel = new ProdutoModel_model();

            $details =  $this->input->post('details');
            $active = $this->input->post('active');
            $type_calendar = $this->input->post('type_calendar');
            $unit = 'Confirmado';
            $isPasseio = FALSE;

            if ($this->input->post('receptivo') == 'receptivo') {
                $isPasseio = TRUE;
                $type_calendar = 1;//calendario
            }

            if (!$active) {
                $unit = 'Inativo';
            }

            $data = array(
                'quantidadePessoasViagem' 		=> $this->input->post("quantidadePessoasViagem"),
                'quantity'                      => $this->input->post("quantidadePessoasViagem"),
                'name' 				            => $this->input->post('name'),
                'unit'                          => $unit,
                'tempo_viagem' 		            => $this->input->post('tempo_viagem'),
                'enviar_site' 		            => $this->input->post('enviar_site'),
                'apenas_cotacao' 		        => $this->input->post('apenas_cotacao'),
                'alertar_polcas_vagas' 		    => $this->input->post('alertar_polcas_vagas'),
                'alertar_ultimas_vagas' 		=> $this->input->post('alertar_ultimas_vagas'),
                'price_pacote_zero_cinco' 		=> $this->input->post('price_pacote_zero_cinco'),
                'price_pacote_seis_onze' 		=> $this->input->post('price_pacote_seis_onze'),
                'price_pacote_acima_sessenta'   => $this->input->post('price_pacote_acima_sessenta'),
                'percentual_desconto_a_vista' 	=> $this->input->post('percentual_desconto_a_vista'),
                'categorias' 		            => $this->input->post('categorias'),
                'instrucoescontrato' 		    => $this->input->post('instrucoescontrato'),
                'tipo_transporte' 		        => $this->input->post('tipo_transporte'),
                'itinerario' 		            => $this->input->post('itinerario'),
                'oqueInclui' 		            => $this->input->post('oqueInclui'),
                'valores_condicoes'             => $this->input->post('valores_condicoes'),
                'duracao_pacote'                => $this->input->post('duracao_pacote'),
                'numero_pessoas' 	            => $this->input->post('numero_pessoas'),
                'status' 			            => $this->input->post('status'),
                'data_saida' 		            => $this->input->post('data_saida'),
                'origem' 			            => $this->input->post('origem'),
                'previsao_chegada' 	            => $this->input->post('previsao_chegada'),
                'data_chegada' 		            => $this->input->post('data_chegada'),
                'destino' 			            => $this->input->post('destino'),
                'data_retorno'		            => $this->input->post('data_retorno'),
                'hora_saida'		            => $this->input->post('hora_saida'),
                'hora_retorno'		            => $this->input->post('hora_retorno'),
                'margem_padrao' 	            => $this->input->post('margem_padrao'),
                'simbolo_moeda' 	            => $this->input->post('simbolo_moeda'),
                'valor_pacote' 	                => $this->input->post('valor_pacote'),
                'precoExibicaoSite' 	        => $this->input->post('precoExibicaoSite'),
                'simboloMoeda'                  => $this->input->post('simboloMoeda'),
                'local_retorno' 	            => $this->input->post('local_retorno'),
                'hora_chegada' 	                => $this->input->post('hora_chegada'),
                'category_id' 		            => $this->input->post('category'),
                'subcategory_id' 	            => $this->input->post('subcategory') ,
                'cost' 				            => $this->sma->formatDecimal($this->input->post('price')),
                'price'	 			            => $this->sma->formatDecimal($this->input->post('price')),
                'tax_rate' 			            => $this->input->post('tax_rate'),
                'tax_method' 		            => $this->input->post('tax_method'),
                'alert_quantity' 	            => 1,
                'track_quantity' 	            => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' 			            => $details,
                'product_details' 	            => $this->input->post('product_details'),
                'supplier1' 		            => $this->input->post('supplier'),
                'supplier1price' 	            => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' 		            => $this->input->post('supplier_2'),
                'supplier2price' 	            => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' 		            => $this->input->post('supplier_3'),
                'supplier3price' 	            => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' 		            => $this->input->post('supplier_4'),
                'supplier4price' 	            => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' 		            => $this->input->post('supplier_5'),
                'supplier5price' 	            => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' 				            => $this->input->post('cf1'),
                'cf2' 				            => $this->input->post('cf2'),
                'cf3' 				            => $this->input->post('cf3'),
                'cf4' 				            => $this->input->post('cf4'),
                'cf5' 				            => $this->input->post('cf5'),
                'cf6' 				            => $this->input->post('cf6'),
                'promotion' 		            => $this->input->post('promotion'),
                'promo_price' 		            => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' 		            => $this->sma->fsd($this->input->post('start_date')),
                'end_date' 			            => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no'             => $this->input->post('supplier_part_no'),
                'supplier2_part_no'             => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no'             => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no'             => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no'             => $this->input->post('supplier_5_part_no'),
                'isServicoOnline'               => $this->input->post('isServicoOnline'),
                'controle_estoque_hospedagem'   => $this->input->post('controle_estoque_hospedagem'),
                'isTransporteTuristico'         => $this->input->post('isTransporteTuristico'),
                'isHospedagem'                  => $this->input->post('isHospedagem'),
                'isServicosAdicionais'          => $this->input->post('isServicosAdicionais'),
                'isValorPorFaixaEtaria'         => $this->input->post('isValorPorFaixaEtaria'),
                'isTaxasComissao'               => $this->input->post('isTaxasComissao') == null ? 0 : $this->input->post('isTaxasComissao'),
                'tipoComissao'                  => $this->input->post('tipoComissao'),
                'tipoCalculoComissao'           => $this->input->post('tipoCalculoComissao'),
                'comissao'                      => $this->input->post('comissao'),
                'destaque'                      => $this->input->post('destaque'),

                'isApenasColetarPagador'        => $this->input->post('isApenasColetarPagador'),
                'isEmbarque'                    => $this->input->post('isEmbarque'),
                'isComissao'                    => $this->input->post('isComissao'),
                'permitirListaEmpera'           => $this->input->post('permitirListaEmpera'),
                'active'                        => $this->input->post('active'),
                'captarEnderecoLink'            => $this->input->post('captarEnderecoLink'),
                'code' 				            => rand(1000,100000),
                'type'				            => 'standard',
                'barcode_symbology'             => 'ean13',
                'viagem' 			            => 1,
                'permiteVendaMenorIdade'        => $this->input->post('permiteVendaMenorIdade') == null ? '0' : $this->input->post('permiteVendaMenorIdade'),
                'permiteVendaClienteDuplicidade'=> $this->input->post('permiteVendaClienteDuplicidade') == null ? '0' : $this->input->post('permiteVendaClienteDuplicidade'),
                'image'                         => $product->image,

                'color_agenda_id'               => $this->input->post('color_agenda_id'),
                'tipo_trajeto_id'               => $this->input->post('tipo_trajeto_id'),
                'duracao_atividade'             => $this->input->post('duracao_atividade'),
                'tipo_duracao_atividade'        => $this->input->post('tipo_duracao_atividade'),
                'cat_precificacao'              => $this->input->post('cat_precificacao'),
                'desc_duracao'                  => $this->input->post('desc_duracao'),
                'product_action'                => $this->input->post('product_action'),
                'product_action_text'           => $this->input->post('product_action_text'),
                'contract_id'                   => $this->input->post('contract_id'),

                'type_calendar'                 => $type_calendar,
                'passeio'                       => $isPasseio,
            );

            $produtoModel->data = $data;

            $imagens = $this->products_model->getProductPhotos($id);
            $photos = null;

            foreach ($imagens as $imgAdicional) {
                $photos[] = $imgAdicional->photo;
            }

            $produtoModel->photos =  $photos;

            $image = $this->adicionarFotoPrincipal(false);

            if ($image) {
                $data['image'] = $image;
            }

            $produtoModel->data = $data;
            $produtoModel->extras = $this->adicionarExtra();
            $produtoModel->items = $this->adicionarServicosAdicionais();
            $produtoModel->product_attributes = $this->adicionarAtributosDaViagem($data);
            $produtoModel->faixaEtariaValores = $this->adicionarFaixaEtariaValor();
            $produtoModel->servicos_inclui = $this->adicionarServicosInclusos();
            $produtoModel->locaisEmbarque = $this->adicionarLocaisEmbarque();
            $produtoModel->condicoesPagamento = $this->adicionarCondicaoPagamentoProduto();
            $produtoModel->tiposCobranca = $this->adicionarTipoCobrancaProduto($tiposCobranca);
            $produtoModel->transportes = $this->adicionarTransportes();
            $produtoModel->tiposHospedagem = $this->adicionarTipoHospedagemViagem();
            $produtoModel->tiposHospedagemFaixaEtaria = $this->adicionarFaixaEtariaTipoHospedagem();
            $produtoModel->faixaEtariaValoresServicoAdicional = $this->adicionarFaixaEtariaValoresServicoAdicional();
            $produtoModel->midia_data = $this->midia_data();
            $produtoModel->seo = $this->seo_data();
            $produtoModel->addresses = $this->addresses_data();

            $produtoModel->programacaoDatas = $this->adicionarProgramacaoData();
            $produtoModel = $this->adicionarDisponibilidadeRecorrente($produtoModel);
        }

        if ($this->form_validation->run() == true) {

            $produtoId = $this->ProdutoService_model->adicionarProduto($produtoModel);

            $this->output->delete_all_cache();//TODO LIMPAR CACHE

            $this->session->set_flashdata('message', lang("product_added"));

            redirect('products/edit/'.$produtoId);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['fornecedores'] = $this->products_model->getAllCustomerCompanies();
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['destinos'] = $this->site->getDestinos();
            $this->data['product_variants'] = $this->products_model->getProductOptions($id);

            $combo_items = $this->products_model->getProductComboItems($id);

            foreach ($combo_items as $itemCombo) {

                $valoresPorFaixaEtaria = $this->ProdutoRepository_model->getServicosAdicionaisFaixaEtariaRodoviario($id, $itemCombo->id);

                foreach ($valoresPorFaixaEtaria as $valorPorFaixaEtaria) {
                    $valores[] = array(
                        'servico' => $valorPorFaixaEtaria->servico,
                        'faixaId' => $valorPorFaixaEtaria->faixaId,
                        'valor' => $valorPorFaixaEtaria->valor,
                        'status' => $valorPorFaixaEtaria->status == 'ATIVO' ? 'true' : 'false'
                    );
                }

                $itemCombo->valores = $valores;
            }

            $agendamentos               = $this->AgendaViagemService_model->getProgramacao($id);
            $transporteRodoviarioAtivo  =  $this->ProdutoRepository_model->getTransportes($id);
            $valoresBateVoltaAtivo      = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($id);
            $valoresHospedagemAtivo     =  $this->ProdutoRepository_model->getTipoHospedagem($id);
            $locaisEmbarqueAtivo        = $this->ProdutoRepository_model->getLocaisEmbarque($id);

            $warning = '';

            if (count($agendamentos) == 0) $warning = lang("product_not_date").'<br/>';
            if (!$transporteRodoviarioAtivo) $warning .= lang("product_not_transporte").'<br/>';
            if (!$valoresBateVoltaAtivo && !$valoresHospedagemAtivo) $warning .= lang("product_not_hospedagem").'<br/>';
            if (!$locaisEmbarqueAtivo) $warning .= lang("product_not_local_embarque").'<br/>';

            if ($warning) $this->session->set_flashdata('warning', $warning);

            $this->data['product']      = $product;
            $this->data['combo_items']  = $combo_items;
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();
            $this->data['tiposTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
            $this->data['tiposQuarto'] = $this->TipoQuartoRepository_model->getTiposQuarto();
            $this->data['hospedagens'] = $this->products_model->getVariacaoHospedagens();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['images'] = $this->products_model->getProductPhotos($id);
            $this->data['imagesFotos'] = $this->products_model->getProductPhotosSite($id);
            $this->data['transportes'] =  $this->ProdutoRepository_model->getTransportesRodoviario($id);
            $this->data['valorFaixas'] = $this->ValorFaixaRepository_model->getValoreFaixas();
            $this->data['tiposCobranca']        = $this->site->getAllTiposCobrancaDebito(true);
            $this->data['condicoesPagamento']   = $this->settings_model->getAllCondicoesPagamentoExibirLink();
            $this->data['cores']   = $this->CorAgendaRepository_model->getAll();
            $this->data['tiposTrajeto']   = $this->TipoTrajetoVeiculoRepository_model->getAll();
            $this->data['servicos_incluso'] = $this->site->getAllServicosIncluso();
            $this->data['contracts'] = $this->ContractRepository_model->getAll();

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('products'), 'page' => lang('products')),
                array('link' => '#', 'page' => lang('duplicate_product'))
            );

            $meta = array('page_title' => lang('duplicate_product'), 'bc' => $bc);
            $this->page_construct('products/duplicar', $meta, $this->data);
        }
    }

    private function addresses_data() {
        $addresses_data = array(
            'cep' => $this->input->post('cep'),
            'endereco' => $this->input->post('address'),
            'numero' => $this->input->post('numero'),
            'complemento' => $this->input->post('complemento'),
            'bairro' => $this->input->post('bairro'),
            'cidade' => $this->input->post('city'),
            'estado' => $this->input->post('state'),
            'pais' => $this->input->post('country'),
        );

        return $addresses_data;
    }

    private function seo_data() {
        $seo_data = array(
            'tag_title' => $this->input->post('tag_title'),
            'meta_tag_description' => $this->input->post('meta_tag_description'),
            'url_product' => '',
            'key_words' => '',
        );

        return $seo_data;
    }

    public function midia_data() {
        $midia_data = array(
            'url_video' => $this->input->post('url_video'),
        );

        return $midia_data;
    }

    public function excluirProgramacaoData($agendamentoId, $produto) {
        try {
            $this->AgendaViagemService_model->excluir($agendamentoId);

            $this->session->set_flashdata('message', lang("agendamento_excluido_com_sucesso"));

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }

        redirect(base_url()."products/edit/".$produto);
    }

    public function adicionarProgramacaoData() {

        $k 	= isset($_POST['doDia']) ? sizeof($_POST['doDia']) : 0;

        $vagasPorTipoDeHospedage = $this->getTotalVagasPorTipoHospedagem();

        for ($j = 0; $j < $k; $j++) {

            $agendaDTO = new AgendaViagemDTO_model();

            $vagas = $_POST['vagas'][$j];
            $doDia = $_POST['doDia'][$j];
            $aoDia = $_POST['aoDia'][$j];
            $horaSaida = $_POST['horaSaida'][$j];
            $horaRetorno = $_POST['horaRetorno'][$j];

            if ($vagas == '' || $doDia == '') continue;

            if ($this->input->post('controle_estoque_hospedagem')) {
                $agendaDTO->vagas = $vagasPorTipoDeHospedage;
            } else {
                $agendaDTO->vagas = $vagas;
            }

            $agendaDTO->doDia = $doDia;
            $agendaDTO->aoDia = $aoDia;
            $agendaDTO->horaSaida = $horaSaida;
            $agendaDTO->horaRetorno = $horaRetorno;

            $datas[] = $agendaDTO;
        }

        return $datas;
    }

    private function adicionarDisponibilidadeRecorrente($produtoModel) {

        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("domingo", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_DOMINGO, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("segunda", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SEGUNDA, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("terca", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_TERCA, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("quarta", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_QUARTA, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("quinta", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_QUINTA, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("sexta", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SEXTA, $produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteSemana("sabado", AgendaProgramacao_model::TIPO_DISPONIBILIDADE_SABADO, $produtoModel);

        $produtoModel = $this->adicionarDisponibilidadeRecorrentePeriodos($produtoModel);
        $produtoModel = $this->adicionarDisponibilidadeRecorrenteDatasPontuais($produtoModel);

        return $produtoModel;
    }

    private function adicionarDisponibilidadeRecorrentePeriodos($produtoModel) {
        $a = sizeof($_POST ['vagas_periodos']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['vagas_periodos'] [$r] ) && $_POST ['vagas_periodos'] [$r]) {

                $disponibilidade = new AgendaProgramacao_model();
                $disponibilidade->setTipoDisponibilidade(AgendaProgramacao_model::TIPO_DISPONIBILIDADE_PERIODOS);
                $disponibilidade->setTitulo($_POST ['descricao_periodos'] [$r]);
                $disponibilidade->setDataSaida($_POST ['data_inicio_periodos'] [$r]);
                $disponibilidade->setDataRetorno($_POST ['data_final_periodos'] [$r]);
                $disponibilidade->setHoraSaida($_POST ['hora_inicio_periodos'] [$r]);
                $disponibilidade->setHoraRetorno($_POST ['hora_final_periodos'] [$r]);
                $disponibilidade->setPreco($_POST ['price_periodos'] [$r]);
                $disponibilidade->setVagas($_POST ['vagas_periodos'] [$r]);

                $produtoModel->adicionarDisponibilidadeRecorrente($disponibilidade);
            }
        }

        return $produtoModel;
    }

    private function adicionarDisponibilidadeRecorrenteDatasPontuais($produtoModel) {
        $a = sizeof($_POST ['vagas_pontual']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['vagas_pontual'] [$r] ) && $_POST ['vagas_pontual'] [$r]) {

                $disponibilidade = new AgendaProgramacao_model();
                $disponibilidade->setTipoDisponibilidade(AgendaProgramacao_model::TIPO_DISPONIBILIDADE_DATAS_PONTUAIS);
                $disponibilidade->setTitulo($_POST ['descricao_data_pontual'] [$r]);
                $disponibilidade->setDataSaida($_POST ['data_inicio_pontual'] [$r]);
                $disponibilidade->setDataRetorno($_POST ['data_inicio_pontual'] [$r]);
                $disponibilidade->setHoraSaida($_POST ['hora_inicio_pontual'] [$r]);
                $disponibilidade->setHoraRetorno($_POST ['hora_final_pontual'] [$r]);
                $disponibilidade->setVagas($_POST ['vagas_pontual'] [$r]);
                $disponibilidade->setPreco($_POST ['price_pontual'] [$r]);

                $produtoModel->adicionarDisponibilidadeRecorrente($disponibilidade);
            }
        }

        return $produtoModel;
    }

    private function adicionarDisponibilidadeRecorrenteSemana($semana, $tipo, $produtoModel) {
        $a = sizeof($_POST ['hora_inicio_'.$semana]);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['hora_inicio_'.$semana] [$r] ) && $_POST ['hora_inicio_'.$semana] [$r]) {

                $disponibilidade = new AgendaProgramacao_model();

                $disponibilidade->setTipoDisponibilidade($tipo);
                $disponibilidade->setDataSaida($_POST ['data_inicio_'.$semana] [$r]);
                $disponibilidade->setDataRetorno($_POST ['data_final_'.$semana] [$r]);
                $disponibilidade->setHoraSaida($_POST ['hora_inicio_'.$semana] [$r]);
                $disponibilidade->setHoraRetorno($_POST ['hora_final_'.$semana] [$r]);
                $disponibilidade->setPreco($_POST ['price_'.$semana] [$r]);
                $disponibilidade->setVagas($_POST ['vagas_'.$semana] [$r]);

                $produtoModel->adicionarDisponibilidadeRecorrente($disponibilidade);
            }
        }

        return $produtoModel;
    }

    /* ---------------------------------------------------------------- */
    function import_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');

                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("products/import_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'name', 'category_code', 'unit', 'cost', 'price', 'alert_quantity', 'tax_rate', 'tax_method', 'subcategory_code', 'variants', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                //$this->sma->print_arrays($final);
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if ( ! $this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        if ($catd = $this->products_model->getCategoryByCode(trim($csv_pr['category_code']))) {
                            $pr_code[] = trim($csv_pr['code']);
                            $pr_name[] = trim($csv_pr['name']);
                            $pr_cat[] = $catd->id;
                            $pr_variants[] = trim($csv_pr['variants']);
                            $pr_unit[] = trim($csv_pr['unit']);
                            $tax_method[] = $csv_pr['tax_method'] == 'exclusive' ? 1 : 0;
                            $prsubcat = $this->products_model->getSubcategoryByCode(trim($csv_pr['subcategory_code']));
                            $pr_subcat[] = $prsubcat ? $prsubcat->id : NULL;
                            $pr_cost[] = trim($csv_pr['cost']);
                            $pr_price[] = trim($csv_pr['price']);
                            $pr_aq[] = trim($csv_pr['alert_quantity']);
                            $tax_details = $this->products_model->getTaxRateByName(trim($csv_pr['tax_rate']));
                            $pr_tax[] = $tax_details ? $tax_details->id : NULL;
                            $cf1[] = trim($csv_pr['cf1']);
                            $cf2[] = trim($csv_pr['cf2']);
                            $cf3[] = trim($csv_pr['cf3']);
                            $cf4[] = trim($csv_pr['cf4']);
                            $cf5[] = trim($csv_pr['cf5']);
                            $cf6[] = trim($csv_pr['cf6']);
                        } else {
                            $this->session->set_flashdata('error', lang("check_category_code") . " (" . $csv_pr['category_code'] . "). " . lang("category_code_x_exist") . " " . lang("line_no") . " " . $rw);
                            redirect("products/import_csv");
                        }
                    }

                    $rw++;
                }
            }

            $ikeys = array('code', 'name', 'category_id', 'unit', 'cost', 'price', 'alert_quantity', 'tax_rate', 'tax_method', 'subcategory_id', 'variants', 'cf1', 'cf2', 'cf3', 'cf4', 'cf5', 'cf6');

            $items = array();
            foreach (array_map(null, $pr_code, $pr_name, $pr_cat, $pr_unit, $pr_cost, $pr_price, $pr_aq, $pr_tax, $tax_method, $pr_subcat, $pr_variants, $cf1, $cf2, $cf3, $cf4, $cf5, $cf6) as $ikey => $value) {
                $items[] = array_combine($ikeys, $value);
            }

            //$this->sma->print_arrays($items);
        }

        if ($this->form_validation->run() == true && $this->products_model->add_products($items)) {
            $this->session->set_flashdata('message', lang("products_added"));
            redirect('products');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('import_products_by_csv')));
            $meta = array('page_title' => lang('import_products_by_csv'), 'bc' => $bc);
            $this->page_construct('products/import_csv', $meta, $this->data);

        }
    }

    /* ------------------------------------------------------------------ */

    function update_price()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');

                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("products/update_price");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'price');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if (!$this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " " . $rw);
                        redirect("products/update_price");
                    }
                    $rw++;
                }
            }

        }

        if ($this->form_validation->run() == true && !empty($final)) {
            $this->products_model->updatePrice($final);
            $this->session->set_flashdata('message', lang("price_updated"));
            redirect('products');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['userfile'] = array('name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('update_price_csv')));
            $meta = array('page_title' => lang('update_price_csv'), 'bc' => $bc);
            $this->page_construct('products/update_price', $meta, $this->data);
        }
    }

    /* ------------------------------------------------------------------------------- */
    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $produto = $this->site->getProductByID($id);
        $code = $produto->code;

        if ($this->products_model->deleteProduct($id , $code)) {
            if($this->input->is_ajax_request()) {
                echo lang("product_deleted"); die();
            }
            $this->session->set_flashdata('message', lang('product_deleted'));
            redirect('welcome');
        }
    }

    /* ------------------------------------------------------------------------------- */
    function arquivar($id = NULL)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->products_model->arquivar($id)) {
            if($this->input->is_ajax_request()) {
                echo lang("product_arquivado"); die();
            }
            $this->session->set_flashdata('message', lang('product_arquivado'));
            redirect('products');
        }
    }

    /* ------------------------------------------------------------------------------- */
    function desarquivar($id = NULL)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->products_model->desarquivar($id)) {
            if($this->input->is_ajax_request()) {
                echo lang("product_desarquivar"); die();
            }
            $this->session->set_flashdata('message', lang('product_desarquivar'));
            redirect('products');
        }

    }

    /* ----------------------------------------------------------------------------- */

    function quantity_adjustments()
    {
        $this->sma->checkPermissions('adjustments');

        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $data['warehouses'] = $this->site->getAllWarehouses();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('quantity_adjustments')));
        $meta = array('page_title' => lang('quantity_adjustments'), 'bc' => $bc);
        $this->page_construct('products/quantity_adjustments', $meta, $this->data);
    }

    function getadjustments($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('adjustments');

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('adjustments') . ".id as did, " . $this->db->dbprefix('adjustments') . ".product_id as productid, " . $this->db->dbprefix('adjustments') . ".date as date, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as pname, " . $this->db->dbprefix('product_variants') . ".name as vname, " . $this->db->dbprefix('adjustments') . ".quantity as quantity, ".$this->db->dbprefix('adjustments') . ".type, " . $this->db->dbprefix('warehouses') . ".name as wh");
            $this->db->from('adjustments');
            $this->db->join('products', 'products.id=adjustments.product_id', 'left');
            $this->db->join('product_variants', 'product_variants.id=adjustments.option_id', 'left');
            $this->db->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');
            $this->db->group_by("adjustments.id")->order_by('adjustments.date desc');
            if ($product) {
                $this->db->where('adjustments.product_id', $product);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('quantity_adjustments'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('product_variant'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('type'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('warehouse'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->pname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->vname);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->quantity);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($data_row->type));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->wh);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $filename = lang('quantity_adjustments');
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                if ($pdf) {
                    $styleArray = array(
                        'borders' => array(
                            'allborders' => array(
                                'style' => PHPExcel_Style_Border::BORDER_THIN
                            )
                        )
                    );
                    $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                    $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                    require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                    $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                    $rendererLibrary = 'MPDF';
                    $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                    if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                        die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                            PHP_EOL . ' as appropriate for your directory structure');
                    }

                    header('Content-Type: application/pdf');
                    header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                    header('Cache-Control: max-age=0');

                    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                    $objWriter->save('php://output');
                    exit();
                }
                if ($xls) {
                    ob_clean();
                    header('Content-Type: application/vnd.ms-excel');
                    header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                    header('Cache-Control: max-age=0');
                    ob_clean();
                    $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                    $objWriter->save('php://output');
                    exit();
                }
            }

            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_adjustment") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' id='a__$1' href='" . site_url('products/delete_adjustment/$2') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";

            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('adjustments') . ".id as did, " . $this->db->dbprefix('adjustments') . ".product_id as productid, " . $this->db->dbprefix('adjustments') . ".date as date, " . $this->db->dbprefix('products') . ".image as image, " . $this->db->dbprefix('products') . ".code as code, " . $this->db->dbprefix('products') . ".name as pname, " . $this->db->dbprefix('product_variants') . ".name as vname, " . $this->db->dbprefix('adjustments') . ".quantity as quantity, ".$this->db->dbprefix('adjustments') . ".type, " . $this->db->dbprefix('warehouses') . ".name as wh");
            $this->datatables->from('adjustments');
            $this->datatables->join('products', 'products.id=adjustments.product_id', 'left');
            $this->datatables->join('product_variants', 'product_variants.id=adjustments.option_id', 'left');
            $this->datatables->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');
            $this->datatables->group_by("adjustments.id");
            $this->datatables->add_column("Actions", "<div class='text-center'><a href='" . site_url('products/edit_adjustment/$1/$2') . "' class='tip' title='" . lang("edit_adjustment") . "' data-toggle='modal' data-target='#myModal'><i class='fa fa-edit'></i></a> " . $delete_link . "</div>", "productid, did");
            if ($product) {
                $this->datatables->where('adjustments.product_id', $product);
            }
            $this->datatables->unset_column('did');
            $this->datatables->unset_column('productid');
            $this->datatables->unset_column('image');

            echo $this->datatables->generate();
        }
    }

    function add_adjustment($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);

        $this->form_validation->set_rules('type', lang("type"), 'required');
        $this->form_validation->set_rules('quantity', lang("quantity"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $data = array(
                'date' => $date,
                'product_id' => $product_id,
                'type' => $this->input->post('type'),
                'quantity' => $this->input->post('quantity'),
                'warehouse_id' => $this->input->post('warehouse'),
                'option_id' => $this->input->post('option') ? $this->input->post('option') : NULL,
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id')
                );

            if (!$this->Settings->overselling && $this->input->post('type') == 'subtraction') {
                if ($this->input->post('option')) {
                    if($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($this->input->post('option'), $this->input->post('warehouse'))) {
                        if ($op_wh_qty->quantity < $data['quantity']) {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                if($wh_qty = $this->products_model->getProductQuantity($product_id, $this->input->post('warehouse'))) {
                    if ($wh_qty['quantity'] < $data['quantity']) {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } else {
                    $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }

        } elseif ($this->input->post('adjust_quantity')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('products');
        }

        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data)) {
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            redirect('products/quantity_adjustments');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $product = $this->site->getProductByID($product_id);
            if($product->type != 'standard') {
                $this->session->set_flashdata('error', lang('quantity_x_adjuste').' ('.lang('product_type').': '.lang($product->type).')');
                die('<script>window.location.replace("'.$_SERVER["HTTP_REFERER"].'");</script>');
            }
            $this->data['product'] = $product;
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['options'] = $this->products_model->getProductOptions($product_id);
            $this->data['product_id'] = $product_id;
            $this->data['warehouse_id'] = $warehouse_id;
            $this->load->view($this->theme . 'products/add_adjustment', $this->data);

        }
    }

    function edit_adjustment($product_id = NULL, $id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        if ($this->input->get('product_id')) {
            $product_id = $this->input->get('product_id');
        }
        $this->form_validation->set_rules('type', lang("type"), 'required');
        $this->form_validation->set_rules('quantity', lang("quantity"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = NULL;
            }

            $data = array(
                'product_id' => $product_id,
                'type' => $this->input->post('type'),
                'quantity' => $this->input->post('quantity'),
                'warehouse_id' => $this->input->post('warehouse'),
                'option_id' => $this->input->post('option') ? $this->input->post('option') : NULL,
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'updated_by' => $this->session->userdata('user_id')
                );
            if ($date) {
                $data['date'] = $date;
            }

            if (!$this->Settings->overselling && $this->input->post('type') == 'subtraction') {
                $dp_details = $this->products_model->getAdjustmentByID($id);
                if ($this->input->post('option')) {
                    $op_wh_qty = $this->products_model->getProductWarehouseOptionQty($this->input->post('option'), $this->input->post('warehouse'));
                    $old_op_qty = $op_wh_qty->quantity + $dp_details->quantity;
                    if ($old_op_qty < $data['quantity']) {
                        $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                        redirect('products');
                    }
                }
                $wh_qty = $this->products_model->getProductQuantity($product_id, $this->input->post('warehouse'));
                $old_quantity = $wh_qty['quantity'] + $dp_details->quantity;
                if ($old_quantity < $data['quantity']) {
                    $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                    redirect('products/quantity_adjustments');
                }
            }

        } elseif ($this->input->post('edit_adjustment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('products/quantity_adjustments');
        }

        if ($this->form_validation->run() == true && $this->products_model->updateAdjustment($id, $data)) {
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            redirect('products/quantity_adjustments');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['product'] = $this->site->getProductByID($product_id);
            $this->data['options'] = $this->products_model->getProductOptions($product_id);
            $this->data['damage'] = $this->products_model->getAdjustmentByID($id);
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['id'] = $id;
            $this->data['product_id'] = $product_id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/edit_adjustment', $this->data);
        }
    }

    function delete_adjustment($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->products_model->deleteAdjustment($id)) {
            echo lang("adjustment_deleted");
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    function modal_view($id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        
        $pr_details = $this->site->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }

        $this->data['hoteis']       = $this->products_model->getProductOptions_Hotel($id);
        $this->data['onibus']       = $this->products_model->getProductOptions_Onibus($id);
        $this->data['product'] = $pr_details;
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->products_model->getSubCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->load->view($this->theme.'products/modal_view', $this->data);
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index');
        $this->load->model('db_model');
        
        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->products_model->getSubCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->data['sold'] = $this->products_model->getSoldQty($id);
        $this->data['purchased'] = $this->products_model->getPurchasedQty($id);

        $this->data['hoteis']       = $this->products_model->getProductOptions_Hotel($id);
        $this->data['onibus']       = $this->products_model->getProductOptions_Onibus($id);

        $this->data['sales']        = $this->db_model->getLatestSalesProducts($id);
        $this->data['purchases']    = $this->db_model->getLatestPurchasesProducts($id);
        $this->data['outrasDespesas'] = $this->db_model->getExpensesReportProductsNotSinal($id);
        $this->data['outrasDespesasSinal'] = $this->db_model->getExpensesReportProductsComSinal($id);
        
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => $pr_details->name));
        $meta = array('page_title' => $pr_details->name, 'bc' => $bc);
        $this->page_construct('products/view', $meta, $this->data);
    }

    function fluxo_caixa()
    {
        $this->sma->checkPermissions('index');
        $this->load->model('db_model');

        $this->data['sold'] = $this->products_model->getSoldQtyAll();
        $this->data['purchased'] = $this->products_model->getPurchasedQtyAll();
        $this->data['sales'] = $this->db_model->getLatestSalesProductsAll();
        $this->data['purchases'] = $this->db_model->getLatestPurchasesProductsAll();
        $this->data['outrasDespesas'] = $this->db_model->getExpensesReportProductsNotSinalAll();
        $this->data['outrasDespesasSinal'] = $this->db_model->getExpensesReportProductsComSinalAll();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => 'Fluxo de Caixa'));
        $meta = array('page_title' =>'Fluxo de Caixa', 'bc' => $bc);
        $this->page_construct('products/fluxo_caixa', $meta, $this->data);
    }

    function pdf($id = NULL, $view = NULL)
    {
        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->products_model->getSubCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load->view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'products/pdf', $this->data, TRUE);
            $this->sma->generate_pdf($html, $name);
        }
    }
	
    function getSubCategories($category_id = NULL)
    {
        if ($rows = $this->products_model->getSubCategoriesForCategoryID($category_id)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    function product_actions($wh = NULL)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'sync_quantity') {

                    foreach ($_POST['val'] as $id) {
                        $this->site->syncQuantity(NULL, NULL, NULL, $id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("products_quantity_sync"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteProduct($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("products_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'labels') {

                    foreach ($_POST['val'] as $id) {
                        $row = $this->products_model->getProductByID($id);
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }

                    $this->data['items'] = isset($pr) ? json_encode($pr) : false;
                    $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
                    $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
                    $this->page_construct('products/print_barcodes', $meta, $this->data);

                } elseif ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Products');
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('category_code'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('unit'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('cost'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('price'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('alert_quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('tax_rate'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('tax_method'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('subcategory_code'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('product_variants'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('pcf1'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('pcf2'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('pcf3'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('pcf4'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('pcf5'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('pcf6'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $product = $this->products_model->getProductDetail($id);
                        $variants = $this->products_model->getProductOptions($id);
                        $product_variants = '';
                        if ($variants) {
                            foreach ($variants as $variant) {
                                $product_variants .= trim($variant->name) . '|';
                            }
                        }
                        $quantity = $product->quantity;
                        if ($wh) {
                            if($wh_qty = $this->products_model->getProductQuantity($id, $wh)) {
                                $quantity = $wh_qty['quantity'];
                            } else {
                                $quantity = 0;
                            }
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $product->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $product->category_code);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $product->unit);
                        if ($this->Owner || $this->Admin || $this->session->userdata('show_cost')) {
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product->cost);
                        }
                        if ($this->Owner || $this->Admin || $this->session->userdata('show_price')) {
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $product->price);
                        }
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $quantity);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $product->alert_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $product->tax_rate_code);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $product->tax_method ? lang('exclusive') : lang('inclusive'));
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $product->subcategory_code);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $product_variants);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $product->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $product->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $product->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $product->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $product->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, $product->cf6);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_product_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function delete_image($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $id || die(json_encode(array('error' => 1, 'msg' => lang('no_image_selected'))));
            $this->db->delete('product_photos', array('id' => $id));
            die(json_encode(array('error' => 0, 'msg' => lang('image_deleted'))));
        }
        die(json_encode(array('error' => 1, 'msg' => lang('ajax_error'))));
    }

    public function delete_image_site($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $id || die(json_encode(array('error' => 1, 'msg' => lang('no_image_selected'))));
            $this->db->delete('product_photos_site', array('id' => $id));
            die(json_encode(array('error' => 0, 'msg' => lang('image_deleted'))));
        }
        die(json_encode(array('error' => 1, 'msg' => lang('ajax_error'))));
    }

    public function getProdutoFilterStatus($status) {
        $product = $this->products_model->getProductsByUnit($status);
        echo json_encode($product);
    }

    public function planta() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('planta')));
        $meta = array('page_title' => lang('plantas'), 'bc' => $bc);
        $this->data['plantas'] = $this->site->getAllVisao();
        $this->page_construct('products/planta', $meta, $this->data);
    }

    public function addPlanta() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_planta')));
        $meta = array('page_title' => lang('add_planta'), 'bc' => $bc);
        $this->page_construct('products/addPlanta', $meta, $this->data);
    }

    public function editarPlanta($id) {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_planta')));
        $meta = array('page_title' => lang('edit_planta'), 'bc' => $bc);
        $this->data['result'] = $this->site->getVisaoAutomovel($id);
        $this->page_construct('products/editPlanta', $meta, $this->data);
    }

    public function savePlanta($id=NULL) {

        $this->load->helper('security');
        $this->load->library('upload');

        if ($_FILES['planta']['size'] > 0) {

            $this->upload_path = 'plantas/';
            $this->thumbs_path = 'plantas/thumbs/';

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('planta')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/addPlanta");
            }

            $photo = $this->upload->file_name;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }

        if ($photo) {
            $visao = array(
                'name' => $this->input->post('name'),
                'descricao' => $this->input->post('descricao'),
                'andares' => $this->input->post('andares'),
                'totalPoltrona' => $this->input->post('totalPoltrona'),
                'width' => $this->input->post('width'),
                'planta' => $photo,
                'height' => $this->input->post('height')
            );
        } else {
            $visao = array(
                'name' => $this->input->post('name'),
                'descricao' => $this->input->post('descricao'),
                'andares' => $this->input->post('andares'),
                'totalPoltrona' => $this->input->post('totalPoltrona'),
                'width' => $this->input->post('width'),
                'height' => $this->input->post('height')
            );
        }

        if ($id) {
            $this->products_model->editarPlanta($id, $visao);
        } else {
            $this->products_model->addPlanta($visao);
        }
        redirect(base_url().'products/planta');
    }

    public function iframePontosPlantaVeiculo($visal_automovel_id) {
        $visao = $this->site->getVisaoAutomovel($visal_automovel_id);
        $this->data['visao'] = $visao;
        $this->data['posicoes'] = $this->site->getPosicaoAutomovel($visal_automovel_id);
        $this->load->view($this->theme . 'products/iframePontosPlantaVeiculo', $this->data);
    }

    public function configurar_planta($visal_automovel_id=null) {
        $visao = $this->site->getVisaoAutomovel($visal_automovel_id);
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc     = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_product')));
        $meta   = array('page_title' => lang('add_product'), 'bc' => $bc);
        $this->data['visal_automovel_id'] = $visal_automovel_id;
        $this->data['visao'] = $visao;
        $this->page_construct('products/configurar_planta', $meta, $this->data);
    }

    function configurarAcentosViagem($product_id) {
        $product = $this->site->getProductByID($product_id);
        if (!$product_id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $onibus = $this->products_model->getOptionOnibus($product_id);
        $this->data['product']      = $product;
        $this->data['onibus']       = $onibus;

        $visao                      = $this->site->getVariantsByName($onibus->name);
        $visal_automovel_id         = $visao->planta;

        $this->data['visao']    = $this->site->getVisaoAutomovel($visal_automovel_id);;
        $this->data['posicoes'] = $this->site->getPosicaoAutomovel($visal_automovel_id);

        $this->load->view($this->theme . 'products/configurarAcentosViagem', $this->data);
    }

    public function addPosicaoPlanta() {

        $tipo               = $this->input->post('tipo');
        $name               = $this->input->post('name');
        $client_x           = $this->input->post('client_x');
        $client_y           = $this->input->post('client_y');
        $visal_automovel_id = $this->input->post('visal_automovel_id');

        $dispcsicao     = array (
            'visao_automovel_id'    => $visal_automovel_id,
            'tipo'                  => $tipo,
            'name'                  => $name,
            'client_x'              => $client_x - 10,
            'client_y'              => $client_y - 15,
        );

        $this->products_model->addDisposicaoAcentosAutomovel($dispcsicao);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function excluirPosicaoPlanta($id) {
        $this->products_model->deletarPosicaoAutomovel($id);
    }

    public function excluirPlanta($id) {
        $this->products_model->deletarPlanta($id);
        redirect($_SERVER["HTTP_REFERER"]);
    }


    function adicionarsite($id) {
        $this->TravelTourService_model->save($id);
    }

    function meios_divulgacao()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('products'),
                'page' => lang('products')),
            array('link' => '#', 'page' => lang('meios_divulgacao')));
        $meta = array('page_title' => lang('meios_divulgacao'), 'bc' => $bc);
        $this->page_construct('products/meios_divulgacao', $meta, $this->data);
    }

    function getMeiosDivulgacao()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("id, id as code, name, active")
            ->from("meio_divulgacao")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarMeioDivulgacao/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_receita") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_receita") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarMeioDivulgacao/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarMeioDivulgacao()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
            );
        } elseif ($this->input->post('adicionarMeioDivulgacao')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/meios_divulgacao");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarMeioDivulgacao($data)) {
            $this->session->set_flashdata('message', lang("meio_divulgacao_adicionado_com_sucesso"));
            redirect("products/meios_divulgacao");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/adicionarMeioDivulgacao', $this->data);
        }
    }

    function editarMeioDivulgacao($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'active' => $this->input->post('active'),
            );
        } elseif ($this->input->post('editarMeioDivulgacao')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/meios_divulgacao");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateMeioDivulgacao($id, $data)) {
            $this->session->set_flashdata('message', lang("meio_divulgacao_atualizado_com_sucesso"));
            redirect("products/meios_divulgacao");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['meioDivulgacao'] = $this->settings_model->getMeioDivulgacaoById($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/editarMeioDivulgacao', $this->data);
        }
    }

    function deletarMeioDivulgacao($id = NULL) {

        if ($this->settings_model->verificaIntegridadeMeioDivulgacaoVenda($id)) {
            $this->session->set_flashdata('error', lang("meio_divulgacao_sendo_utilizada"));
            echo lang("meio_divulgacao_sendo_utilizada");
        }

        if ($this->settings_model->deletarMeioDivulgacao($id)) {
            echo lang("meio_divulgacao_deletado_com_sucesso");
        }
    }

    function locais_embarque()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('products'),
                'page' => lang('products')),
            array('link' => '#', 'page' => lang('locais_embarque')));
        $meta = array('page_title' => lang('locais_embarque'), 'bc' => $bc);
        $this->page_construct('products/locais_embarque', $meta, $this->data);
    }

    function getLocaisEmbarqueCadatro()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("id, id as code, name, active")
            ->from("local_embarque")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarLocalEmbarque/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_receita") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_receita") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarLocalEmbarque/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarLocalEmbarque()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
                'cep' => $this->input->post('cep'),
                'endereco' => $this->input->post('endereco'),
                'numero' => $this->input->post('numero'),
                'complemento' => $this->input->post('complemento'),
                'bairro' => $this->input->post('bairro'),
                'country' => $this->input->post('country'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
            );
        } elseif ($this->input->post('adicionarLocalEmbarque')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicinarLocalEmbarque($data)) {
            $this->session->set_flashdata('message', lang("local_embarque_adicionado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/adicionarLocalEmbarque', $this->data);
        }
    }

    function editarLocalEmbarque($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
                'cep' => $this->input->post('cep'),
                'endereco' => $this->input->post('endereco'),
                'numero' => $this->input->post('numero'),
                'complemento' => $this->input->post('complemento'),
                'bairro' => $this->input->post('bairro'),
                'country' => $this->input->post('country'),
                'cidade' => $this->input->post('cidade'),
                'estado' => $this->input->post('estado'),
            );
        } elseif ($this->input->post('editarLocalEmbarque')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/locais_embarque");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateLocalEmbarque($id, $data)) {
            $this->session->set_flashdata('message', lang("local_embarque_atualizado_com_sucesso"));
            redirect("products/locais_embarque");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['localEmbarque'] = $this->settings_model->getLocalEmbarqueById($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/editarLocalEmbarque', $this->data);
        }
    }

    function deletarLocalEmbarque($id = NULL) {

        if ($this->settings_model->verificaIntegridadeLocalEmbarque($id)) {
            echo lang("local_embarque_sendo_utilizada");
        } else {
            if ($this->settings_model->deletarLocalEmbarque($id)) {
                echo lang("local_embarque_deletado_com_sucesso");
            }
        }
    }

    function tipos_hospedagem()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('products'),
                'page' => lang('products')),
            array('link' => '#', 'page' => lang('tipos_hospedagem')));
        $meta = array('page_title' => lang('tipos_hospedagem'), 'bc' => $bc);
        $this->page_construct('products/tipos_hospedagem', $meta, $this->data);
    }

    function getTiposHospedagem()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("tipo_hospedagem.id as id, tipo_hospedagem.id as code, tipo_hospedagem.name, companies.name as fornecedor")
            ->from("tipo_hospedagem")
            ->join('companies', 'companies.id=tipo_hospedagem.fornecedor', 'left')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarTipoHospedagem/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_tipo_hospedagem") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_tipo_hospedagem") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarTipoHospedagem/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarTipoHospedagem()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'ocupacao' => $this->input->post('ocupacao'),
                'fornecedor' => $this->input->post('fornecedor'),
                'note' => $this->input->post('note'),
            );
        } elseif ($this->input->post('adicionarTipoHospedagem')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/tipos_hospedagem");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarTipoHospedagem($data)) {
            $this->session->set_flashdata('message', lang("tipo_hospedagem_adicionada_com_sucesso"));
            redirect("products/tipos_hospedagem");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/adicionarTipoHospedagem', $this->data);
        }
    }

    function editarTipoHospedagem($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'ocupacao' => $this->input->post('ocupacao'),
                'fornecedor' => $this->input->post('fornecedor'),
                'note' => $this->input->post('note'),
            );
        } elseif ($this->input->post('editarTipoHospedagem')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/tipos_hospedagem");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTipoHospedagem($id, $data)) {
            $this->session->set_flashdata('message', lang("tipo_hospedagem_atualizado_com_sucesso"));
            redirect("products/tipos_hospedagem");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['tipoHospedagem'] = $this->settings_model->getTipoHospedagemById($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/editarTipoHospedagem', $this->data);
        }
    }

    function deletarTipoHospedagem($id = NULL) {
        if ($this->settings_model->deletarTipoHospedagem($id)) {
            echo lang("tipo_hospedagem_deletado_com_sucesso");
        }
    }

    function tipostransporte()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('products'),
                'page' => lang('products')),
            array('link' => '#', 'page' => lang('tipos_transporte')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('products/tipostransporte', $meta, $this->data);
    }

    function getTiposTransporte()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("tipo_transporte_rodoviario.id as id, tipo_transporte_rodoviario.name as name, automovel.name as mapa")
            ->from("tipo_transporte_rodoviario")
            ->join('automovel', 'automovel.id = tipo_transporte_rodoviario.automovel_id', 'left')
            ->order_by('tipo_transporte_rodoviario.name')
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarTipoTransporte/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_tipo_transporte") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_tipo_transporte") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarTipoTransporte/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarTipoTransporte()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'totalPoltronas' => $this->input->post('totalPoltronas'),
                'automovel_id' => $this->input->post('automovel_id'),
            );
        } elseif ($this->input->post('adicionarTipoTransporte')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/tipostransporte");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarTipoTransporte($data)) {
            $this->session->set_flashdata('message', lang("tipo_transporte_adicionado_com_sucesso"));
            redirect("products/tipostransporte");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['automoveis'] = $this->AutomovelRepository_model->getAll();
            $this->load->view($this->theme . 'products/adicionarTipoTransporte', $this->data);
        }
    }

    function editarTipoTransporte($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'totalPoltronas' => $this->input->post('totalPoltronas'),
                'automovel_id' => $this->input->post('automovel_id'),
            );
        } elseif ($this->input->post('editarTipoTransporte')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/tipostransporte");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateTipoTransporte($id, $data)) {
            $this->session->set_flashdata('message', lang("tipo_transporte_atualizado_com_sucesso"));
            redirect("products/tipostransporte");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['tipoTransporte'] = $this->settings_model->getTipoTransporteById($id);
            $this->data['automoveis'] = $this->AutomovelRepository_model->getAll();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/editarTipoTransporte', $this->data);
        }
    }

    function deletarTipoTransporte($id = NULL) {
        if ($this->settings_model->deletarTipoTransporte($id)) {
            echo lang("tipo_transporte_deletado_com_sucesso");
        }
    }

    function valores_faixa()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('products'),
                'page' => lang('products')),
            array('link' => '#', 'page' => lang('valor_faixa')));
        $meta = array('page_title' => lang('valor_faixa'), 'bc' => $bc);
        $this->page_construct('products/valorfaixa', $meta, $this->data);
    }

    function getValoresFaixa()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, concat(name,' ', note) as name")
            ->from("valor_faixa")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarValorFaixa/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_valor_faixa") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_valor_faixa") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarValorFaixa/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        $this->datatables->where('uso_interno', false);

        echo $this->datatables->generate();
    }

    function adicionarValorFaixa()
    {
        $this->form_validation->set_rules('name', $this->lang->line("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'note' => $this->input->post('note'),
                'formato_cpf' => $this->input->post('formato_cpf'),
                //'tipo' => $this->input->post('tipo'),
                'descontarVaga' => $this->input->post('descontarVaga') != null ? $this->input->post('descontarVaga')  : FALSE,
                'obrigaCPF' => $this->input->post('obrigaCPF') != null ? $this->input->post('obrigaCPF')  : FALSE,
                'faixaDependente' => $this->input->post('faixaDependente') != null ? $this->input->post('faixaDependente')  : FALSE,
                'captarCPF' => $this->input->post('captarCPF') != null ? $this->input->post('captarCPF')  : FALSE,
                'captarDataNascimento' => $this->input->post('captarDataNascimento') != null ? $this->input->post('captarDataNascimento')  : FALSE,
                'captarDocumento' => $this->input->post('captarDocumento') != null ? $this->input->post('captarDocumento')  : FALSE,
                'captarOrgaoEmissor' => $this->input->post('captarOrgaoEmissor') != null ? $this->input->post('captarOrgaoEmissor')  : FALSE,
                'captarEmail' => $this->input->post('captarEmail') != null ? $this->input->post('captarEmail')  : FALSE,
                'captarProfissao' => $this->input->post('captarProfissao') != null ? $this->input->post('captarProfissao')  : FALSE,
                'captarInformacoesMedicas' => $this->input->post('captarInformacoesMedicas') != null ? $this->input->post('captarInformacoesMedicas')  : FALSE,
                'captarNomeSocial' => $this->input->post('captarNomeSocial') != null ? $this->input->post('captarNomeSocial')  : FALSE,
                'obrigaOrgaoEmissor' => $this->input->post('obrigaOrgaoEmissor') != null ? $this->input->post('obrigaOrgaoEmissor')  : FALSE,
                'staff' => $this->input->post('staff') != null ? $this->input->post('staff')  : FALSE,
                'captar_observacao' => $this->input->post('captar_observacao') != null ? $this->input->post('captar_observacao')  : FALSE,
                'placeholder_observacao' => $this->input->post('placeholder_observacao') != null ? $this->input->post('placeholder_observacao')  : FALSE,

            );
        } elseif ($this->input->post('adicionarValorFaixa')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarValorFaixa($data)) {
            $this->session->set_flashdata('message', lang("valor_faixa_adicionado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/adicionarValorFaixa', $this->data);
        }
    }

    function editarValorFaixa($id = NULL)
    {
        $this->form_validation->set_rules('name', $this->lang->line("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'active' => $this->input->post('active'),
                'note' => $this->input->post('note'),
                'formato_cpf' => $this->input->post('formato_cpf'),
                //'tipo' => $this->input->post('tipo'),
                'descontarVaga' => $this->input->post('descontarVaga') != null ? $this->input->post('descontarVaga')  : FALSE,
                'obrigaCPF' => $this->input->post('obrigaCPF') != null ? $this->input->post('obrigaCPF')  : FALSE,
                'faixaDependente' => $this->input->post('faixaDependente') != null ? $this->input->post('faixaDependente')  : FALSE,
                'captarCPF' => $this->input->post('captarCPF') != null ? $this->input->post('captarCPF')  : FALSE,
                'captarDataNascimento' => $this->input->post('captarDataNascimento') != null ? $this->input->post('captarDataNascimento')  : FALSE,
                'captarDocumento' => $this->input->post('captarDocumento') != null ? $this->input->post('captarDocumento')  : FALSE,
                'captarOrgaoEmissor' => $this->input->post('captarOrgaoEmissor') != null ? $this->input->post('captarOrgaoEmissor')  : FALSE,
                'captarEmail' => $this->input->post('captarEmail') != null ? $this->input->post('captarEmail')  : FALSE,
                'captarProfissao' => $this->input->post('captarProfissao') != null ? $this->input->post('captarProfissao')  : FALSE,
                'captarInformacoesMedicas' => $this->input->post('captarInformacoesMedicas') != null ? $this->input->post('captarInformacoesMedicas')  : FALSE,
                'captarNomeSocial' => $this->input->post('captarNomeSocial') != null ? $this->input->post('captarNomeSocial')  : FALSE,
                'obrigaOrgaoEmissor' => $this->input->post('obrigaOrgaoEmissor') != null ? $this->input->post('obrigaOrgaoEmissor')  : FALSE,
                'staff' => $this->input->post('staff') != null ? $this->input->post('staff')  : FALSE,
                'captar_observacao' => $this->input->post('captar_observacao') != null ? $this->input->post('captar_observacao')  : FALSE,
                'placeholder_observacao' => $this->input->post('placeholder_observacao') != null ? $this->input->post('placeholder_observacao')  : FALSE
            );
        } elseif ($this->input->post('editarValorFaixa')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateValorFaixa($id, $data)) {
            $this->session->set_flashdata('message', lang("valor_faixa_atualizado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['valorFaixa'] = $this->settings_model->getValorFaixa($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/editarValorFaixa', $this->data);
        }
    }

    function deletarValorFaixa($id = NULL)
    {
        if ($this->settings_model->deletarValorFaixa($id)) {
            echo lang("valor_faixa_deletada_com_sucesso");
        }
    }

    function cupons()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('system_settings'), 'page' => lang('products')),
            array('link' => '#', 'page' => lang('cupons')));

        $meta = array('page_title' => lang('cupons'), 'bc' => $bc);
        $this->page_construct('products/cupons', $meta, $this->data);
    }

    function getCupons()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, codigo, valor")
            ->from("cupom_desconto")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('products/editarCupom/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_cupom") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_cupom") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deletarCupom/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarCupom()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('codigo', lang("codigo"), 'is_unique[cupom_desconto.codigo]|required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active'        => $this->input->post('active'),
                'name'          => $this->input->post('name', false),
                'codigo'        => strtoupper(trim($this->input->post('codigo', false))),
                'tipo_desconto' => $this->input->post('tipo_desconto'),
                'descricao'     => $this->input->post('descricao'),
                'valor'         => $this->input->post('valor'),
                'all_products'  => $this->input->post('all_products'),
                'dataInicio'    => $this->input->post('dataInicio'),
                'dataFinal'     => $this->input->post('dataFinal'),
                'quantidade_disponibilizada'    => $this->input->post('quantidade_disponibilizada'),
            );
        } elseif ($this->input->post('adicionarCupom')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarCupom($data)) {
            $this->session->set_flashdata('message', lang("cupom_adicionado_com_successo"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'products/adicionarCupom', $this->data);
        }
    }

    function editarCupom($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        $cupom = $this->settings_model->getCupomById($id);

        if ($this->form_validation->run() == true) {
            $data = array(
                'active'        => $this->input->post('active'),
                'name'          => $this->input->post('name', false),
                'codigo'        => strtoupper(trim($this->input->post('codigo', false))),
                'tipo_desconto' => $this->input->post('tipo_desconto'),
                'descricao'     => $this->input->post('descricao'),
                'valor'         => $this->input->post('valor'),
                'all_products'  => $this->input->post('all_products'),
                'dataInicio'    => $this->input->post('dataInicio'),
                'dataFinal'     => $this->input->post('dataFinal'),
                'quantidade_disponibilizada'    => $this->input->post('quantidade_disponibilizada'),
            );
        } elseif ($this->input->post('editarCupom')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/cupons");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCupom($id, $data)) {
            $this->session->set_flashdata('message', lang("cupom_editado_com_successo"));
            redirect("products/cupons");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['cupom'] = $cupom;
            $this->load->view($this->theme . 'products/editarCupom', $this->data);
        }
    }

    function deletarCupom($id = NULL) {
        if ($this->settings_model->deletarCupomDesconto($id)) {
            echo lang("cupom_desconto_deletado_com_sucesso");
        }
    }

    function servicos_incluso()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('servicos_incluso')));
        $meta = array('page_title' => lang('servicos_incluso'), 'bc' => $bc);
        $this->page_construct('products/servicos_incluso', $meta, $this->data);
    }

    function getServicosIncluso()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("servicos_inclusos.id as id, images_icon.icon as image, servicos_inclusos.name")
            ->from("servicos_inclusos")
            ->join('images_icon', 'images_icon.id=servicos_inclusos.image_icon_id', 'left')
            ->add_column("Actions", "<div class=\"text-center\"> <a href='" . site_url('products/editarServicoIncluso/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_servico_incluso") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_servico_incluso") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/deleteServicoIncluso/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function adicionarServicoIncluso()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'note' => $this->input->post('note'),
                'image_icon_id' => $this->input->post('image_icon_id'),
            );
        } elseif ($this->input->post('adicionarServicoIncluso')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarServicoIncluso($data)) {
            $this->session->set_flashdata('message', lang("servico_incluso_adicionado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['icons']    = $this->settings_model->getAllIcon();
            $this->load->view($this->theme . 'products/adicionarServicoIncluso', $this->data);
        }
    }

    function editarServicoIncluso($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'note' => $this->input->post('note'),
                'image_icon_id' => $this->input->post('image_icon_id'),
            );
        } elseif ($this->input->post('editarServicoIncluso')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateServicoIncluso($id, $data)) {
            $this->session->set_flashdata('message', lang("servico_incluso_atualizado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['servicoIncluso'] = $this->settings_model->getServicoInclusoByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['icons']    = $this->settings_model->getAllIcon();
            $this->load->view($this->theme . 'products/editarServicoIncluso', $this->data);
        }
    }

    function deleteServicoIncluso($id = NULL) {
        if ($this->settings_model->deleteServicoIncluso($id)) {
            echo lang("servico_incluso_deletado_com_sucesso");
        }
    }

    function categories()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('categories')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('products/categories', $meta, $this->data);
    }

    function getCategories()
    {
        $print_barcode = anchor('products/print_barcodes/?category=$1', '<i class="fa fa-print"></i>', 'title="'.lang('print_barcodes').'" class="tip"');

        $this->load->library('datatables');
        $this->datatables
            ->select("categories.id as id, images_icon.icon as image, categories.code, categories.name")
            ->from("categories")
            ->join('images_icon', 'images_icon.id=categories.image_icon_id', 'left')
            ->add_column("Actions", "<div class=\"text-center\"> <a href='" . site_url('products/edit_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_category") . "'><i class=\"fa fa-edit\"></i></a></div>", "id");
            //->add_column("Actions", "<div class=\"text-center\"> <a href='" . site_url('products/edit_category/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_category") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_category") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('products/delete_category/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");

        echo $this->datatables->generate();
    }

    function add_category()
    {

        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|is_unique[categories.code]|required');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $name = $this->input->post('name');
            $code = $this->input->post('code');
            $icon = $this->input->post('image_icon_id');

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                //$data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            } else {
                $photo = NULL;
            }
        } elseif ($this->input->post('add_category')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/categories");
        }

        if ($this->form_validation->run() == true && $this->settings_model->addCategory($name, $code, $icon, $photo)) {
            $this->session->set_flashdata('message', lang("category_added"));
            redirect("products/categories");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

            $this->data['name'] = array('name' => 'name',
                'id' => 'name',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('name'),
            );
            $this->data['code'] = array('name' => 'code',
                'id' => 'code',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('code'),
            );

            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['icons']    = $this->settings_model->getAllIcon();
            $this->load->view($this->theme . 'products/add_category', $this->data);
        }
    }

    function edit_category($id = NULL)
    {
        $this->load->helper('security');
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $pr_details = $this->settings_model->getCategoryByID($id);

        if ($this->input->post('code') != $pr_details->code) {
            $this->form_validation->set_rules('code', lang("category_code"), 'is_unique[categories.code]');
        }

        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');
        $this->form_validation->set_rules('userfile', lang("category_image"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $data = array('code' => $this->input->post('code'),
                'name'          => $this->input->post('name'),
                'image_icon_id' => $this->input->post('image_icon_id'),
            );
            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                //$data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            } else {
                $photo = NULL;
            }
        } elseif ($this->input->post('edit_category')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("products/categories");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCategory($id, $data, $photo)) {
            $this->session->set_flashdata('message', lang("category_updated"));
            redirect("products/categories");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $category = $this->settings_model->getCategoryByID($id);
            $this->data['name'] = array('name' => 'name',
                'id' => 'name',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('name', $category->name),
            );
            $this->data['code'] = array('name' => 'code',
                'id' => 'code',
                'type' => 'text',
                'class' => 'form-control',
                'required' => 'required',
                'value' => $this->form_validation->set_value('code', $category->code),
            );

            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['category'] = $pr_details;
            $this->data['icons'] = $this->settings_model->getAllIcon();
            $this->load->view($this->theme . 'products/edit_category', $this->data);
        }
    }

    function delete_category($id = NULL)
    {

        if ($this->settings_model->getSubCategoriesByCategoryID($id)) {
            $this->session->set_flashdata('error', lang("category_has_subcategory"));
            redirect("products/categories");
        }

        if ($this->settings_model->deleteCategory($id)) {
            echo lang("category_deleted");
        }
    }

}
