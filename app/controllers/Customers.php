<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif|pdf|doc|docx';
        $this->allowed_file_size = '16384';
        $this->popup_attributes = array('width' => '900', 'height' => '600', 'window_name' => 'sma_popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');

        $this->lang->load('customers', $this->Settings->user_language);

        $this->load->library('form_validation');
        $this->load->model('companies_model');
    }

    function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers')));
        $meta = array('page_title' => lang('customers'), 'bc' => $bc);
        //$this->page_construct('customers/index_custom', $meta, $this->data);
        $this->page_construct('customers/index', $meta, $this->data);
    }

    function bloqueados($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('customers_bloqueados')));
        $meta = array('page_title' => lang('customers_bloqueados'), 'bc' => $bc);
        $this->page_construct('customers/bloqueados', $meta, $this->data);
    }

    function getCustomers()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("id, 
                company, 
                name, 
                email, 
                concat(phone,' ',cf5, ' ' , telefone_emergencia) as phone , 
                city, 
                customer_group_name, 
                vat_no, 
                deposit_amount, 
                award_points")
            ->from("companies")
            ->where('group_name', 'customer')
            ->where('active', 1)
            ->add_column("Actions", "<div class=\"text-center\"> 
             <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a>
             <a class=\"tip\" title='" . lang("install") . "' href='" . site_url('install/install/$1') . "'><i class=\"fa fa-database\"></i></a>
             </div>", "id");

        // ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("list_users") . "' href='" . site_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> <a class=\"tip\" title='" . lang("add_user") . "' href='" . site_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-user-plus\"></i></a> <a class=\"tip\" title='" . lang("list_deposits") . "' href='" . site_url('customers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-money\"></i></a> <a class=\"tip\" title='" . lang("add_deposit") . "' href='" . site_url('customers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus\"></i></a> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        // ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("list_users") . "' href='" . site_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> <a class=\"tip\" title='" . lang("add_user") . "' href='" . site_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-user-plus\"></i></a> <a class=\"tip\" title='" . lang("list_deposits") . "' href='" . site_url('customers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-money\"></i></a> <a class=\"tip\" title='" . lang("add_deposit") . "' href='" . site_url('customers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus\"></i></a> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_customer") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('customers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            //$this->db->where('created_by', $this->session->userdata('user_id'));
        }

        echo $this->datatables->generate();
    }

    function getCustomersCustom()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("id, 
                name,
                company, 
                nome_responsavel, 
                email, 
                concat(phone,' ',cf5, ' ' , telefone_emergencia) as phone , 
                city, 
                customer_group_name, 
                vat_no")
            ->from("companies")
            ->where('group_name', 'customer')
            ->where('active', 1)
            ->add_column("Actions", "<div class=\"text-center\"> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        // ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("list_users") . "' href='" . site_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> <a class=\"tip\" title='" . lang("add_user") . "' href='" . site_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-user-plus\"></i></a> <a class=\"tip\" title='" . lang("list_deposits") . "' href='" . site_url('customers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-money\"></i></a> <a class=\"tip\" title='" . lang("add_deposit") . "' href='" . site_url('customers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus\"></i></a> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        // ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("list_users") . "' href='" . site_url('customers/users/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-users\"></i></a> <a class=\"tip\" title='" . lang("add_user") . "' href='" . site_url('customers/add_user/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-user-plus\"></i></a> <a class=\"tip\" title='" . lang("list_deposits") . "' href='" . site_url('customers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-money\"></i></a> <a class=\"tip\" title='" . lang("add_deposit") . "' href='" . site_url('customers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-plus\"></i></a> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_customer") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('customers/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            //$this->db->where('created_by', $this->session->userdata('user_id'));
        }

        echo $this->datatables->generate();
    }

    function getCustomersSearh()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("id,name, email, concat(phone,' ',cf5, ' ' , telefone_emergencia) as phone, vat_no, concat(UPPER(tipo_documento), ' ' , cf1) as document")
            ->from("companies")
            ->where('group_name', 'customer')
            ->add_column("Actions", "", "id");

        $term_filter = $this->input->post('input_search_customer');

        if ($term_filter) {

            $term_filter = str_replace(['.', '-', '(', ')'], '', $term_filter); // Remove '.' and '-' from the search term

            $this->datatables->where('(name LIKE "%'.$term_filter.'%" OR 
            email LIKE "%'.$term_filter.'%" OR 
            company LIKE "%'.$term_filter.'%" OR
            nome_responsavel LIKE "%'.$term_filter.'%" OR 
            vat_no like "%'.$term_filter.'%" OR
            cf5 like "%'.$term_filter.'%" OR
            phone like "%'.$term_filter.'%" OR
            cf1 like "%'.$term_filter.'%" OR
            telefone_emergencia like "%'.$term_filter.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(telefone_emergencia, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term_filter.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(cf1, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term_filter.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(cf5, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term_filter.'%" OR
            REPLACE(REPLACE(REPLACE(REPLACE(phone, "(", ""), ")", ""), "-", ""), " ", "") like "%'.$term_filter.'%" OR
            REPLACE(REPLACE(vat_no, ".", ""), "-", "") like "%'.$term_filter.'%")');
        }

        if ($this->Settings->restrict_user &&
            !$this->Owner && !$this->Admin) {
            //$this->db->where('created_by', $this->session->userdata('user_id'));
        }

        echo $this->datatables->generate();
    }

    function getCustomersBloqueados()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("id, company, name, email, concat(phone,' ',cf5, ' ' , telefone_emergencia) as phone , city, 
            customer_group_name, vat_no, deposit_amount, award_points, data_cancelamento")
            ->from("companies")
            ->where('group_name', 'customer')
            ->where('active', 0)
            ->add_column("Actions", "<div class=\"text-center\"> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        if ($this->input->post('ano_cancelamento')) {
            $this->datatables->where('YEAR(data_cancelamento)', $this->input->post('ano_cancelamento'));        }


        echo $this->datatables->generate();
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['customer'] = $this->companies_model->getCompanyByID($id);
        $this->data['images']   = $this->companies_model->getCustomerPhotos($id);


        $this->load->view($this->theme.'customers/view',$this->data);
    }

    function search($id = NULL, $prefix = '')
    {
        $this->data['prefix'] = $prefix;
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['customer'] = $this->companies_model->getCompanyByID($id);

        $this->load->view($this->theme.'customers/search',$this->data);
    }

    function add()
    {
        $this->sma->checkPermissions(false, true);
        $this->load->helper('security');
        $this->load->library('upload');

        if ($this->form_validation->run('companies/add') == true) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $data = array(
                'group_id'              => '3',
                //'name'                  => mb_strtoupper($this->input->post('name'), mb_internal_encoding()),
                //'company'               => mb_strtoupper($this->input->post('name'), mb_internal_encoding()),
                'name'                  => $this->input->post('name'),
                'company'               => $this->input->post('name'),
                'email'                 => $this->input->post('email'),
                'group_name'            => 'customer',
                'customer_group_id'     => $this->input->post('customer_group'),
                'customer_group_name'   => $cg->name,
                'data_aniversario'      => $this->input->post('data_aniversario'),
                'tipo_documento'        => $this->input->post('tipo_documento'),
                'validade_rg_passaporte'=> $this->input->post('validade_rg_passaporte'),
                'address'               => $this->input->post('address'),
                'vat_no'                => $this->input->post('vat_no'),
                'city'                  => $this->input->post('city'),
                'state'                 => $this->input->post('state'),
                'sexo'                  => $this->input->post('sexo'),
                'postal_code'           => $this->input->post('postal_code'),
                'country'               => $this->input->post('country'),
                'phone'                 => $this->input->post('phone'),
                'cf1'                   => $this->input->post('cf1'),
                'cf2'                   => $this->input->post('cf2'),
                'cf3'                   => $this->input->post('cf3'),
                'cf4'                   => $this->input->post('cf4'),
                'cf5'                   => $this->input->post('cf5'),
                'cf6'                   => $this->input->post('cf6'),
                'observacao'            => $this->input->post('observacao'),
                'plano_saude'           => $this->input->post('plano_saude'),
                'alergia_medicamento'   => $this->input->post('alergia_medicamento'),
                'doenca_informar'       => $this->input->post('doenca_informar'),
                'telefone_emergencia'   => $this->input->post('telefone_emergencia'),
                'social_name'           => $this->input->post('social_name'),
                'profession'            => $this->input->post('profession'),
                'idioma'                => $this->input->post('idioma'),
                'nome_responsavel'      => $this->input->post('nome_responsavel'),
                'data_contratacao'      => $this->input->post('data_contratacao'),
                'data_cancelamento'      => $this->input->post('data_cancelamento'),
                'tipoPessoa'   => $this->input->post('tipoPessoa'),
                'numero'   => $this->input->post('numero'),
                'complemento'   => $this->input->post('complemento'),
                'bairro'   => $this->input->post('bairro'),
                'created_by' => $this->session->userdata('user_id'),
                'data_cadastro' => date('Y-m-d'),
            );

        } elseif ($this->input->post('add_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        //uploads
        if ($_FILES['customers_image']['size'] > 0) {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('customers_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("customers");
            }

            $photo = $this->upload->file_name;
            $data['image'] = $photo;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }


        if ($_FILES['userfile']['name'][0] != "") {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 25;
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("customers");
                } else {

                    $pho = $this->upload->file_name;
                    $photos[] = $pho;

                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->upload_path . $pho;
                    $config['new_image'] = $this->thumbs_path . $pho;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = $this->Settings->twidth;
                    $config['height'] = $this->Settings->theight;

                    $this->image_lib->initialize($config);

                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }

                    if ($this->Settings->watermark) {
                        $this->image_lib->clear();
                        $wm['source_image'] = $this->upload_path . $pho;
                        $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                        $wm['wm_type'] = 'text';
                        $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                        $wm['quality'] = '100';
                        $wm['wm_font_size'] = '16';
                        $wm['wm_font_color'] = '999999';
                        $wm['wm_shadow_color'] = 'CCCCCC';
                        $wm['wm_vrt_alignment'] = 'top';
                        $wm['wm_hor_alignment'] = 'right';
                        $wm['wm_padding'] = '10';
                        $this->image_lib->initialize($wm);
                        $this->image_lib->watermark();
                    }
                    $this->image_lib->clear();
                }
            }
            $config = NULL;
        } else {
            $photos = NULL;
        }

        if ($this->form_validation->run() == true && $cid = $this->companies_model->addCompanyWith($data, $photos)) {

            $phone = $this->input->post('cf5') != null ? $this->input->post('cf5') : $this->input->post('phone');

            $this->create_persons_google($data['name'], $data['email'], $phone);

            if ($this->input->post('data_aniversario') != null) {
                $data_event = array(
                    'title' 		=> 'Aniversario '.$this->input->post('name'),
                    'customers' 	=> $cid,
                    'start'	 		=> $this->sma->fld($this->input->post('data_aniversario')),
                    'end' 			=> $this->sma->fld($this->input->post('data_aniversario')),
                    'description' 	=> 'De os parabens para '.$this->input->post('name').' hoje e seu aniversario',
                    'color' 		=>  '#1d7010',
                    'user_id' 	=> $this->session->userdata('user_id')
                );
                //$this->companies_model->addEvent($data_event);
            }

            $this->session->set_flashdata('message', lang("customer_added"));
            $ref = isset($_SERVER["HTTP_REFERER"]) ? explode('?', $_SERVER["HTTP_REFERER"]) : NULL;
            redirect($ref[0] . '?customer=' . $cid);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();

            $this->load->view($this->theme . 'customers/add_by_admin', $this->data);
            //$this->load->view($this->theme . 'customers/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);
        $this->load->helper('security');
        $this->load->library('upload');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);

        if ($this->form_validation->run('companies/add') == true) {
            $cg = $this->site->getCustomerGroupByID($this->input->post('customer_group'));
            $data = array(
                //'name'                  => mb_strtoupper($this->input->post('name'), mb_internal_encoding()),
                //'company'               => mb_strtoupper($this->input->post('name'), mb_internal_encoding()),
                'name'                  => $this->input->post('name'),
                'active'                => $this->input->post('active'),
                'company'               => $this->input->post('name'),
                'email'                 => $this->input->post('email'),
                'group_id'              => '3',
                'group_name'            => 'customer',
                'customer_group_id'     => $this->input->post('customer_group'),
                'customer_group_name'   => $cg->name,
                'data_aniversario'      => $this->input->post('data_aniversario'),
                'tipo_documento'        => $this->input->post('tipo_documento'),
                'validade_rg_passaporte'=> $this->input->post('validade_rg_passaporte'),
                'address'               => $this->input->post('address'),
                'vat_no'                => $this->input->post('vat_no'),
                'city'                  => $this->input->post('city'),
                'state'                 => $this->input->post('state'),
                'postal_code'           => $this->input->post('postal_code'),
                'country'               => $this->input->post('country'),
                'phone'                 => $this->input->post('phone'),
                'sexo'                  => $this->input->post('sexo'),
                'cf1'                   => $this->input->post('cf1'),
                'cf2'                   => $this->input->post('cf2'),
                'cf3'                   => $this->input->post('cf3'),
                'cf4'                   => $this->input->post('cf4'),
                'cf5'                   => $this->input->post('cf5'),
                'cf6'                   => $this->input->post('cf6'),
                'bloqueado'             => $this->input->post('bloqueado'),
                'motivo_bloqueio'       => $this->input->post('motivo_bloqueio'),
                'observacao'            => $this->input->post('observacao'),
                'award_points'          => $this->input->post('award_points'),
                'plano_saude'           => $this->input->post('plano_saude'),
                'alergia_medicamento'   => $this->input->post('alergia_medicamento'),
                'doenca_informar'       => $this->input->post('doenca_informar'),
                'telefone_emergencia'   => $this->input->post('telefone_emergencia'),
                'social_name'           => $this->input->post('social_name'),
                'profession'            => $this->input->post('profession'),
                'idioma'                => $this->input->post('idioma'),
                'nome_responsavel'      => $this->input->post('nome_responsavel'),
                'data_contratacao'      => $this->input->post('data_contratacao'),
                'data_cancelamento'     => $this->input->post('data_cancelamento'),
                'name_db'               => $this->input->post('name_db'),
                'tipoPessoa'   => $this->input->post('tipoPessoa'),
                'numero'   => $this->input->post('numero'),
                'complemento'   => $this->input->post('complemento'),
                'bairro'   => $this->input->post('bairro'),
                'data_alteracao' => date('Y-m-d'),
            );
        } elseif ($this->input->post('edit_customer')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        //uploads
        if ($_FILES['customers_image']['size'] > 0) {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('customers_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("customers");
            }

            $photo = $this->upload->file_name;
            $data['image'] = $photo;
            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            }

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';
                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }

        if ($_FILES['userfile']['name'][0] != "") {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 25;
            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);
            for ($i = 0; $i < $cpt; $i++) {

                $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("customers");
                } else {

                    $pho = $this->upload->file_name;
                    $photos[] = $pho;

                    $this->load->library('image_lib');
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = $this->upload_path . $pho;
                    $config['new_image'] = $this->thumbs_path . $pho;
                    $config['maintain_ratio'] = TRUE;
                    $config['width'] = $this->Settings->twidth;
                    $config['height'] = $this->Settings->theight;

                    $this->image_lib->initialize($config);

                    if (!$this->image_lib->resize()) {
                        echo $this->image_lib->display_errors();
                    }

                    if ($this->Settings->watermark) {
                        $this->image_lib->clear();
                        $wm['source_image'] = $this->upload_path . $pho;
                        $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                        $wm['wm_type'] = 'text';
                        $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                        $wm['quality'] = '100';
                        $wm['wm_font_size'] = '16';
                        $wm['wm_font_color'] = '999999';
                        $wm['wm_shadow_color'] = 'CCCCCC';
                        $wm['wm_vrt_alignment'] = 'top';
                        $wm['wm_hor_alignment'] = 'right';
                        $wm['wm_padding'] = '10';
                        $this->image_lib->initialize($wm);
                        $this->image_lib->watermark();
                    }
                    $this->image_lib->clear();
                }
            }
            $config = NULL;
        } else {
            $photos = NULL;
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompanyWith($id, $data, $photos)) {

            $phone = $this->input->post('cf5') != null ? $this->input->post('cf5') : $this->input->post('phone');

            $this->create_persons_google($data['name'], $data['email'], $phone);

            if ($this->input->post('data_aniversario') != null) {
                $calendar = $this->companies_model->getEventByCustomers($id);
                if ($calendar->id != null) {
                    $data_event = array(
                        'title' 		=> 'Aniversario '.$this->input->post('name'),
                        'customers' 	=> $id,
                        'start'	 		=> $this->sma->fld($this->input->post('data_aniversario')),
                        'end' 			=> $this->sma->fld($this->input->post('data_aniversario')),
                        'description' 	=> 'De os parabens para '.$this->input->post('name').' hoje e seu aniversario',
                        'color' 		=>  '#1d7010',
                        'user_id' 	=> $this->session->userdata('user_id')
                    );
                    //$this->companies_model->updateEvent($id, $data_event);
                } else {
                    $data_event = array(
                        'title' 		=> 'Aniversario '.$this->input->post('name'),
                        'customers' 	=> $id,
                        'start'	 		=> $this->sma->fld($this->input->post('data_aniversario')),
                        'end' 			=> $this->sma->fld($this->input->post('data_aniversario')),
                        'description' 	=> 'De os parabens para '.$this->input->post('name').' hoje e seu aniversario',
                        'color' 		=>  '#1d7010',
                        'user_id' 	=> $this->session->userdata('user_id')
                    );
                    //$this->companies_model->addEvent($data_event);
                }
            }

            $this->session->set_flashdata('message', lang("customer_updated").' contador '.$calendar->id);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['customer'] = $company_details;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();

            $this->load->view($this->theme . 'customers/edit_by_admin', $this->data);
            //$this->load->view($this->theme . 'customers/edit', $this->data);
        }
    }

    public function create_persons_google($name, $mail, $phone)
    {
        $ddi = '55';

        $phone = str_replace('(', '', str_replace(')', '', $phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);
        $phone = trim($ddi.$phone);

        if (strlen($phone) == 13) {

            $this->load->model('model/GooglePerson_model', 'GooglePerson_model');
            $this->load->model('repository/GooglePersonRepository_model', 'GooglePersonRepository_model');

            $googlePerson = new GooglePerson_model();

            $nameParts = explode(' ', trim($name));
            $googlePerson->familyName = array_pop($nameParts);
            $googlePerson->givenName = implode(' ', $nameParts);

            $googlePerson->phoneNumbers = $phone;
            $googlePerson->emailAddresses = $mail;

            $existPerson = $this->GooglePersonRepository_model->existPerson($phone);

            if (!$existPerson) {
                return $this->GooglePersonRepository_model->save($googlePerson);
            }
        }

        return false;
    }


    function users($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }


        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->data['users'] = $this->companies_model->getCompanyUsers($company_id);
        $this->load->view($this->theme . 'customers/users', $this->data);

    }

    function add_user($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        $this->form_validation->set_rules('email', lang("email_address"), 'is_unique[users.email]');
        $this->form_validation->set_rules('password', lang('password'), 'required|min_length[8]|max_length[20]|matches[password_confirm]');
        $this->form_validation->set_rules('password_confirm', lang('confirm_password'), 'required');

        if ($this->form_validation->run('companies/add_user') == true) {
            $active = $this->input->post('status');
            $notify = $this->input->post('notify');
            list($username, $domain) = explode("@", $this->input->post('email'));
            $email = strtolower($this->input->post('email'));
            $password = $this->input->post('password');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'phone' => $this->input->post('phone'),
                'gender' => $this->input->post('gender'),
                'company_id' => $company->id,
                'company' => $company->company,
                'group_id' => 3
            );
            $this->load->library('ion_auth');
        } elseif ($this->input->post('add_user')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, $active, $notify)) {
            $this->session->set_flashdata('message', lang("user_added"));
            redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'customers/add_user', $this->data);
        }
    }

    function corrigir_cpf() {
        $clientes = $this->companies_model->getAllCustomerCompanies();

        foreach ($clientes as $cliente) {
            $cpf = $cliente->vat_no;

            if ($cpf !== '' && $cpf !== null) {

                $cpf = str_replace('.', '', $cpf);
                $cpf = str_replace('-', '', $cpf);

                $pattern = '/^([[:digit:]]{3})([[:digit:]]{3})([[:digit:]]{3})([[:digit:]]{2})$/';
                $replacement = '$1.$2.$3-$4';

                $cdata = array(
                    'vat_no' =>  preg_replace($pattern, $replacement, $cpf)
                );

                $this->db->update('companies', $cdata, array('id' => $cliente->id));
            }
        }
    }

    function ate($texto) {

        $resultado = explode("CPF", $texto)[0];
        $resultado =  explode("Nasc", $resultado)[0];
        $resultado =  explode("NASC", $resultado)[0];
        $resultado =  explode("D.N", $resultado)[0];
        $resultado =  explode("RG", $resultado)[0];
        $resultado =  explode("CNPJ", $resultado)[0];
        $resultado =  explode("CNH", $resultado)[0];
        $resultado =  explode("TEL", $resultado)[0];

        $resultado = str_replace(":", "", $resultado);
        $resultado = trim($resultado);
        $letrafinal = substr($resultado, -1);

        if ($letrafinal == '-') {
            $resultado = substr($resultado, 0, strlen($resultado)-1);
        }

        return trim($resultado);
    }

    function extrair_dependente($texto) {
        // $texto = 'PEDRO DA TRINDADE SILVA - C.N.:FL.104 - LV.N°AE00021 - ORDEM:6104 - NASC.:06/05/2007';
        $texto = utf8_encode($texto);

        $nome = explode("-", $texto)[0];
        $rg =  explode("RG", $texto)[1];
        $cpf =  explode("CPF", $texto)[1];
        $nascimento =  explode("NASC.:", $texto)[1];
        $cnpj =  explode("CNPJ", $texto)[1];
        $cnh =  explode("CNH", $texto)[1];
        $folha = explode("C.N", $texto)[1];

        if (!$nascimento) $nascimento =  explode("Nasc.:", $texto)[1];
        if (!$nascimento) $nascimento =  explode("D.N", $texto)[1];
        if (!$nascimento) $nascimento =  explode("Nasc:", $texto)[1];
        if (!$nascimento) $nascimento =  explode("NASC:", $texto)[1];
        if (!$nascimento) $nascimento =  explode("NACS:", $texto)[1];
        if (!$nascimento) $nascimento =  explode("nasc:", $texto)[1];

        /*
        echo '<pre>LINHA: '.$texto.'</pre>';
        echo '<pre>R.G: '.$this->ate($rg).'</pre>';
        echo '<pre>CNH: '.$this->ate($cnh).'</pre>';
        echo '<pre>FOLHA: '.$this->ate($folha).'</pre>';
        */

        $dependente['name'] = $nome;
        $dependente['observacao'] = $texto;
        $dependente['email'] = '';
        $dependente['phone'] = '';
        $dependente['city'] = '';
        $dependente['address'] = '';
        $dependente['group_id'] = 3;
        $dependente['group_name'] = 'customer';
        $dependente['customer_group_id'] = 1;
        $dependente['customer_group_name'] = 'PASSAGEIROS';

        if ($this->ate($cnpj) != '') {
            $dependente['tipoPessoa'] = 'PJ';
            $dependente['company'] = $nome;
            $dependente['vat_no'] = $this->ate($cnpj);
        } else {
            $dependente['tipoPessoa'] = 'PF';
            $dependente['vat_no'] = $this->ate($cpf);
            $dependente['company'] = '';
        }

        if ($this->ate($nascimento) != '') {
            $dependente['data_aniversario'] = implode("-",array_reverse(explode("/",$this->ate($nascimento) )));
        }

        if ($this->ate($rg) != '') {
            $dependente['tipo_documento'] = 'rg';
            $dependente['cf1'] = $this->ate($rg);
        }

        if ($this->ate($cnh) != '') {
            $dependente['tipo_documento'] = 'CNH';
            $dependente['cf1'] = $this->ate($rg);
        }

        if ($this->ate($folha) != '') {
            $dependente['tipo_documento'] = 'CN';
            $dependente['cf1'] = $this->ate($rg);
        }

        $this->companies_model->addCompany($dependente);
    }

    function importar_dependente($texto) {
        return $texto != '' && $texto != '------';
    }

    function import_csv()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('warning', lang("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {

                $this->load->library('upload');

                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = TRUE;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload('csv_file')) {

                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("customers");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ";")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }

                array_shift($arrResult);

                $keys = array(
                    //'tipoPessoa',
                    'company',
                    'name',
                    'email',
                    'phone',
                    'address',
                    'address1',
                    'address2',
                    'bairro',
                    'city',
                    'state',
                    'postal_code',
                    'country',
                    'vat_no',
                    'cf1',
                    'cf2',
                    'cf3',
                    'cf4',
                    'cf5',
                    'cf6',
                    'data_aniversario',
                    'telefone_emergencia',
                    'observacao',
                    'plano_saude',
                    'alergia_medicamento',
                    'doenca_informar'
                    //'data_cadastro',
                );
                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }

                $rw = 2;
                foreach ($final as $csv) {

                    /*$customerEncontrado = $this->companies_model->getCompanyByCpf($csv['vat_no']);
                    if ($customerEncontrado) {

                        $address = $csv['address'];
                        $address1 = $csv['address1'];
                        $address2 = $csv['address2'];

                        $csv['address'] = utf8_encode($address.' '.$address1.' / '.$address2);
                        $csv['city'] = utf8_encode($csv['city']);

                        $this->companies_model->updateCompanyWith($customerEncontrado->id, $csv, null);
                    }
                    */
                    $rw++;
                }

                foreach ($final as $record) {

                    $cpf      = $record['vat_no'];

                    //if ($cpf === '') continue;

                    $cpf = str_replace('O', '0', $cpf);

                    $address0 = $record['address'];
                    $address1 = $record['address1'];
                    $address2 = $record['address2'];
                    $whatsapp = $record['cf5'];

                    $record['vat_no'] = $this->formatar_cpf_cnpj($cpf);//TODO CUIDADO DEPENDE DOS DADOS DO ARQUIVO
                    //$record['cf1'] = $this->formatar_rg($record['cf1']);//TODO CUIDADO DEPENDE DOS DADOS DO ARQUIVO

                    $data_aniversario = $record['data_aniversario'];
                    //$data_cadastro = $record['data_cadastro'];
                    $observacao = $record['observacao'];

                    $company = $record['company'];
                    $name = $record['name'];
                    $cf2 = $record['cf2'];
                    $state = $record['state'];
                    $city = $record['city'];
                    $cf1 = $record['cf1'];
                    $bairro = $record['bairro'];

                    $cf1 = str_replace('.','', $cf1);
                    $cf1 = str_replace('-','', $cf1);

                    $company = utf8_encode($company);
                    $name = utf8_encode($name);
                    $cf2 = utf8_encode($cf2);
                    $state = utf8_encode($state);
                    $city = utf8_encode($city);
                    $bairro = utf8_encode($bairro);
                    $observacao = utf8_encode($observacao);

                    $address = utf8_encode($address0.' '.$address1.' / '.$address2);
                    //$data_aniversario = date("Y-m-d", strtotime($data_aniversario));
                    $data_aniversario = implode("-",array_reverse(explode("/",$data_aniversario)));
                    //$data_aniversario = implode("-",array_reverse(explode(".",$data_aniversario)));

                    //$data_cadastro = implode("-",array_reverse(explode("/",$data_cadastro)));

                    $whatsapp = $this->formatarTelefone($whatsapp);

                    $record['company'] = $company;
                    $record['name'] = $name;
                    $record['address'] = $address;
                    $record['cf1'] = $cf1;
                    $record['cf2'] = $cf2;
                    $record['state'] = $state;
                    $record['city'] = $city;
                    $record['bairro'] = $bairro;
                    $record['observacao'] = $observacao;
                    $record['cf5'] = $whatsapp;

                    $record['group_id'] = 3;
                    $record['group_name'] = 'customer';
                    $record['customer_group_id'] = 1;
                    $record['customer_group_name'] = 'PASSAGEIROS';
                    $record['data_aniversario'] = $data_aniversario;
                    $record['data_cadastro'] = date('Y-m-d');
                    //$record['data_cadastro'] = $data_cadastro;

                    $record['plano_saude'] = utf8_encode($record['plano_saude']);
                    $record['alergia_medicamento'] = utf8_encode($record['alergia_medicamento']);
                    $record['doenca_informar'] = utf8_encode($record['doenca_informar']);

                    /*
                    $customerEncontrado = $this->companies_model->getCompanyByCpf($record['vat_no']);
                    if (!$customerEncontrado) {
                        $data[] = $record;
                    }
                    */

                    $data[] = $record;
                }
            }

        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->companies_model->addCompanies($data)) {
                $this->session->set_flashdata('message', lang("customers_added"));
                redirect('customers');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'customers/import', $this->data);
        }
    }

    function formatarTelefone($numero) {
        // Remove todos os caracteres que não são números
        $numeroLimpo = preg_replace('/\D/', '', $numero);

        // Verifica se o número tem 11 dígitos (caso comum para números com DDD e nono dígito)
        if (strlen($numeroLimpo) == 11) {
            $ddd = substr($numeroLimpo, 0, 2);
            $parte1 = substr($numeroLimpo, 2, 5);
            $parte2 = substr($numeroLimpo, 7, 4);

            return "($ddd) $parte1-$parte2";
        }

        // Verifica se o número tem 10 dígitos (números sem o nono dígito)
        if (strlen($numeroLimpo) == 10) {
            $ddd = substr($numeroLimpo, 0, 2);
            $parte1 = substr($numeroLimpo, 2, 4);
            $parte2 = substr($numeroLimpo, 6, 4);

            return "($ddd) $parte1-$parte2";
        }

        // Se o número não tiver 10 ou 11 dígitos, retorna o número original
        return $numero;
    }

    private function formatar_rg($rg) {
        $doc =  str_replace("RG", "", $rg);
        $doc = trim($doc);

        return $doc;
    }

    private function formatar_cpf_cnpj($doc) {

        $doc = preg_replace("/[^0-9]/", "", $doc);
        $doc = str_replace("-", "", $doc);
        $doc = str_replace(".", "", $doc);
        $doc = trim($doc);

        $qtd = strlen($doc);

        if($qtd >= 11) {

            if($qtd === 11 ) {

                $docFormatado = substr($doc, 0, 3) . '.' .
                    substr($doc, 3, 3) . '.' .
                    substr($doc, 6, 3) . '-' .
                    substr($doc, 9, 2);
            } else {
                $docFormatado = substr($doc, 0, 2) . '.' .
                    substr($doc, 2, 3) . '.' .
                    substr($doc, 5, 3) . '/' .
                    substr($doc, 8, 4) . '-' .
                    substr($doc, -2);
            }

            return $docFormatado;

        } else {
            return $doc;
        }
    }

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->session->set_flashdata('error', lang('customer_x_deleted'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }

        if ($this->companies_model->deleteCustomer($id)) {
            echo lang("customer_deleted");
        } else {
            $this->session->set_flashdata('warning', lang('customer_x_deleted_have_sales'));
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 0);</script>");
        }
    }

    function suggestions($term = NULL, $limit = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getCustomerSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    function suggestionsAll($term = NULL, $limit = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getAllCompaniesSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    function suggestionsByID($term = NULL, $limit = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getCustomerSuggestionsByID($term, $limit);
        $this->sma->send_json($rows);
    }

    function suggestionsByIDS($term = NULL, $limit = NULL, $ids = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }

        $idsNotIn   = $this->input->get('idsNotIn', TRUE);
        $ids        = $this->input->get('ids', TRUE);

        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getCustomerSuggestionsByIDS($term, $limit, $idsNotIn, $ids);
        $this->sma->send_json($rows);
    }

    function getCustomer($id = NULL)
    {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->name)));
    }

    function get_customer_details($id = NULL)
    {
        $this->sma->send_json($this->companies_model->getCompanyByID($id));
    }

    function get_award_points($id = NULL)
    {
        $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array('ca_points' => $row->award_points));
    }

    function export_all_actions_numero_compras()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->load->model('sales_model');

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('customer'));
        $this->excel->getActiveSheet()->SetCellValue('A1', 'First Name');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'E-mail Address');
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('address'));
        $this->excel->getActiveSheet()->SetCellValue('E1', 'Home City');
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('state'));
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Home Postal Code');
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('country'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('vat_no'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ccf1'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('ccf2'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ccf3'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('ccf4'));
        $this->excel->getActiveSheet()->SetCellValue('N1', 'Mobile Phone');
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ccf6'));
        $this->excel->getActiveSheet()->SetCellValue('P1', lang('data_cadastro'));
        $this->excel->getActiveSheet()->SetCellValue('Q1', lang('Nascimento'));
        $this->excel->getActiveSheet()->SetCellValue('R1', lang('Nascimento Mes'));
        $this->excel->getActiveSheet()->SetCellValue('S1', lang('Ultimo Pacote'));
        $this->excel->getActiveSheet()->SetCellValue('T1', lang('Total de vendas'));
        $this->excel->getActiveSheet()->SetCellValue('U1', lang('group'));

        $row = 2;

        $customers = $this->companies_model->getAllCustomerCompanies();

        foreach ($customers as $customer) {

            $product_name = $this->sales_model->getUltimoItemCompraCliente($customer->id);
            $total_compras = $this->sales_model->getNumeroDeComprasCliente($customer->id);

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->email);
            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->address);
            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->state);
            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->postal_code);
            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->country);
            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->vat_no);
            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->cf1);
            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf2);
            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf3);
            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf4);
            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf5);
            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf6);
            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->data_cadastro);
            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->data_aniversario);
            $this->excel->getActiveSheet()->SetCellValue('R' . $row, date('F', strtotime($customer->data_aniversario)));
            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $product_name->name);
            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $total_compras->numero_compras);
            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $customer->customer_group_name);

            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'customers_' . date('Y_m_d_H_i_s');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function export_all_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('companies'));

        $query  = $this->db->get_where('companies', array('group_name' => 'customer'));
        $fields = $query->list_fields(); // Obtém os nomes das colunas

        $col = 0;
        foreach ($fields as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, lang($field));
            $col++;
        }

        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }

        foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $filename = 'customers_' . date('Y_m_d_H_i_s') . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }


    function customer_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {

                $this->load->model('sales_model');

                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deleteCustomer($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('customers_x_deleted_have_sales'));
                    } else {
                        $this->session->set_flashdata('message', lang("customers_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', 'First Name');
                    $this->excel->getActiveSheet()->SetCellValue('B1', 'E-mail Address');
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', 'Home City');
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', 'Home Postal Code');
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('ccf1'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('ccf2'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('ccf3'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('ccf4'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', 'Mobile Phone');
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('ccf6'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('data_cadastro'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('product'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('group'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $product_name = $this->sales_model->getUltimoItemCompraCliente($id);

                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->address);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->state);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->country);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf6);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->data_cadastro);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $product_name->name);
                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->customer_group_name);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'customers_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once(APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php");
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_customer_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function deposits($company_id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['company'] = $this->companies_model->getCompanyByID($company_id);
        $this->load->view($this->theme . 'customers/deposits', $this->data);

    }

    function get_deposits($company_id = NULL)
    {
        $this->sma->checkPermissions('deposits');
        $this->load->library('datatables');
        $this->datatables
            ->select("deposits.id as id, date, amount, paid_by, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by", false)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->where($this->db->dbprefix('deposits').'.company_id', $company_id)
            ->add_column("Actions", "<div class=\"text-center\"><a class=\"tip\" title='" . lang("deposit_note") . "' href='" . site_url('customers/deposit_note/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-file-text-o\"></i></a> <a class=\"tip\" title='" . lang("edit_deposit") . "' href='" . site_url('customers/edit_deposit/$1') . "' data-toggle='modal' data-target='#myModal2'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_deposit") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('customers/delete_deposit/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id")
            ->unset_column('id');
        echo $this->datatables->generate();
    }

    function add_deposit($company_id = NULL)
    {
        $this->sma->checkPermissions('deposits', true);

        if ($this->input->get('id')) {
            $company_id = $this->input->get('id');
        }
        $company = $this->companies_model->getCompanyByID($company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $company->id,
                'created_by' => $this->session->userdata('user_id'),
            );

            $cdata = array(
                'deposit_amount' => ($company->deposit_amount+$this->input->post('amount'))
            );

        } elseif ($this->input->post('add_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->addDeposit($data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_added"));
            redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->load->view($this->theme . 'customers/add_deposit', $this->data);
        }
    }

    function edit_deposit($id = NULL)
    {
        $this->sma->checkPermissions('deposits', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $deposit = $this->companies_model->getDepositByID($id);
        $company = $this->companies_model->getCompanyByID($deposit->company_id);

        if ($this->Owner || $this->Admin) {
            $this->form_validation->set_rules('date', lang("date"), 'required');
        }
        $this->form_validation->set_rules('amount', lang("amount"), 'required|numeric');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $deposit->date;
            }
            $data = array(
                'date' => $date,
                'amount' => $this->input->post('amount'),
                'paid_by' => $this->input->post('paid_by'),
                'note' => $this->input->post('note'),
                'company_id' => $deposit->company_id,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => $date = date('Y-m-d H:i:s'),
            );

            $cdata = array(
                'deposit_amount' => (($company->deposit_amount-$deposit->amount)+$this->input->post('amount'))
            );

        } elseif ($this->input->post('edit_deposit')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect('customers');
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateDeposit($id, $data, $cdata)) {
            $this->session->set_flashdata('message', lang("deposit_updated"));
            redirect("customers");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['company'] = $company;
            $this->data['deposit'] = $deposit;
            $this->load->view($this->theme . 'customers/edit_deposit', $this->data);
        }
    }

    public function delete_deposit($id)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->companies_model->deleteDeposit($id)) {
            echo lang("deposit_deleted");
        }
    }

    public function deposit_note($id = null)
    {
        $this->sma->checkPermissions('deposits', true);
        $deposit = $this->companies_model->getDepositByID($id);
        $this->data['customer'] = $this->companies_model->getCompanyByID($deposit->company_id);
        $this->data['deposit'] = $deposit;
        $this->data['page_title'] = $this->lang->line("deposit_note");
        $this->load->view($this->theme . 'customers/deposit_note', $this->data);
    }

    public function delete_image($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $id || die(json_encode(array('error' => 1, 'msg' => lang('no_image_selected'))));
            $this->db->delete('customer_photos', array('id' => $id));
            die(json_encode(array('error' => 0, 'msg' => lang('image_deleted'))));
        }
        die(json_encode(array('error' => 1, 'msg' => lang('ajax_error'))));
    }

    public function validarCPF($cpfCnpj = null){

        if ($cpfCnpj == null) $cpfCnpj= $input->get('cpfCnpj');

        if ($cpfCnpj) {
            echo json_encode($this->companies_model->getCustomerSuggestionsByCPFCNPJ($cpfCnpj));
        } else {
            echo json_encode([]);
        }
    }

    public function validarRG($rg = null) {
        if ($rg) {
            echo json_encode($this->companies_model->getCustomerSuggestionsByCPFCNPJ($rg));
        } else {
            echo json_encode([]);
        }
    }

    public function wscliente() {

        $cnpj = $this->input->post('cnpj');

        $url = "https://www.receitaws.com.br/v1/cnpj/".$cnpj;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if(!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $result = curl_exec($ch);
        curl_close($ch);

        header('Content-Type: application/json');
        echo json_encode($result);
    }
}
