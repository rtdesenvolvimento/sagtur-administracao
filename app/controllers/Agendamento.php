<?php defined('BASEPATH') or exit('No direct script access allowed');

class Agendamento extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('products', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->model('products_model', 'products_model');
        $this->load->model('financeiro_model', 'financeiro_model');

        $this->load->model('model/Agendamento_model', 'Agendamento_model');

        $this->load->model('dto/AgendamentoFilter_DTO_model', 'AgendamentoFilter_DTO_model');

        $this->load->model('service/AgendamentoServico_model', 'AgendamentoServico_model');
        $this->load->model('service/HorarioTrabalhoService_model', 'HorarioTrabalhoService_model');

        $this->load->model('repository/AgendamentoRepository_model', 'AgendamentoRepository_model');
    }

    function verificaUsuarioLogado() {
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
    }

    function index($action = NULL) {

        $this->verificaUsuarioLogado();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;

        $bc = array(
            array('link' => base_url(),
                'page' => lang('home')
            ),
            array('link' => '#', 'page' => lang('billers')));
        $meta = array('page_title' => lang('billers'), 'bc' => $bc);

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        $start_date = date('d/m/Y', $data_incio);
        $end_date = date('d/m/Y', $data_fim);

        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;

        $this->data['servicos'] = $this->products_model->getAllProductsConfirmed();

        $this->page_construct('agendamento/index', $meta, $this->data);
    }

    function agenda() {

        $this->verificaUsuarioLogado();

        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['servicos'] = $this->products_model->getAllProductsConfirmed();

        $this->load->view($this->theme . 'agendamento/agenda', $this->data);
    }

    function agendar() {

        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['servicos'] = $this->products_model->getAllProductsAtivo();
        $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();
        $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();

        $this->load->view($this->theme . 'agendamento/agendamento', $this->data);
    }

    function getAgendamentos() {

        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("cancelar_agendamento") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger' id='a__$1' href='" . site_url('agendamento/cancelar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('cancelar_agendamento') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li><a href="' . site_url('agendamento/editarAgendamento/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> ' . lang('editar_agendamento') . '</a></li>';

        $ver_pagamento_link = '<a href="'.base_url().'agendamento/pagamentos/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Ver Pagamentos</a>';
        $adicionar_pagamento_link = '<a href="'.base_url().'agendamento/adicionarPagamento/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Adicionar Pagamento</a>';
        $pdf = '<a href="'.base_url().'agendamento/pdf/$1"><i class="fa fa-file-pdf-o"></i> Voucher do Ensaio</a>';

        $action .= ' 
                <li>' . $pdf . '</li>
 				<li class="divider"></li>
			    <li>' . $ver_pagamento_link . '</li>
			    <li>' . $adicionar_pagamento_link . '</li>
				<li class="divider"></li>
				<li>' . $delete_link . '</li>
			</ul>
		</div></div>';

        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $start_hour = $this->input->get('start_hour') ? $this->input->get('start_hour') : NULL;
        $end_hour = $this->input->get('end_hour') ? $this->input->get('end_hour') : NULL;

        $produto_filter = $this->input->get('produto_filter') ? $this->input->get('produto_filter') : NULL;
        $status = $this->input->get('statusFilter') ? $this->input->get('statusFilter') : NULL;
        $nomeClienteFilter = $this->input->get('nomeClienteFilter') ? $this->input->get('nomeClienteFilter') : NULL;

        if ($start_date) {
            $start_date = $this->sma->fsd($start_date);
            $end_date = $this->sma->fsd($end_date);
        }

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $agendamentoFilter = new AgendamentoFilter_DTO_model();
        $agendamentoFilter->setStartDate($start_date);
        $agendamentoFilter->setEndDate($end_date);
        $agendamentoFilter->setStartHour($start_hour);
        $agendamentoFilter->setEndHour($end_hour);
        $agendamentoFilter->setProduct($produto_filter);
        $agendamentoFilter->setStatus($status);
        $agendamentoFilter->setName($nomeClienteFilter);

        echo $this->AgendamentoRepository_model->getAgendamentosDatatables($action, $agendamentoFilter);
    }

    function confirmarAgendamento() {

        try {

            $agendamento = new Agendamento_model();

            $senderHash = htmlspecialchars($this->input->post('senderHash', true));
            $creditCardToken = htmlspecialchars($this->input->post('creditCardToken', true));

            $cpf = $this->input->post('cpf', true) != null ? $this->input->post('cpf', true) : $this->input->post('cpfTitular', true);

            $diaNascimento = $this->input->post('diaNascimento', true);
            $mesNascimento = $this->input->post('mesNascimento', true);
            $anoNascimento = $this->input->post('anoNascimento', true);

            $dataNascimento =  $anoNascimento.'-'.$mesNascimento.'-'.$diaNascimento;
            $parcelas = $this->input->post('parcelas', true);

            if ($parcelas == null) $parcelas = 1;

            $agendamento->setProduto($this->input->post('servicoId', true));
            $agendamento->setTipoCobranca($this->input->post('tipoCobranca', true));
            $agendamento->setNomeCompleto($this->input->post('nomeCompleto', true));
            $agendamento->setCelular($this->input->post('celular', true));
            $agendamento->setEmail($this->input->post('email', true));
            $agendamento->setData($this->input->post('dataAgendamento', true));
            $agendamento->setDataAte($this->input->post('dataAgendamento', true));
            $agendamento->setHoraInicio($this->input->post('horarios', true));
            $agendamento->setCardName($this->input->post('card_name', true));
            $agendamento->setValor($this->input->post('price', true));
            $agendamento->setDataNascimento($dataNascimento);
            $agendamento->setIsBloqueado(false);
            $agendamento->setCpf($cpf);
            $agendamento->setParcelas($parcelas);
            $agendamento->setSenderHash($senderHash);
            $agendamento->setCreditCardToken($creditCardToken);

            $agendamentoId = $this->AgendamentoServico_model->salvar($agendamento);

            $this->enviarEmail($agendamentoId);

            $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($agendamento->getTipoCobranca());

            if ($tipoCobranca->tipo == 'dinheiro') $this->abrirWhatsAppConfirmacao($agendamento);

            else  {
                redirect('agendamento/confirmacao/'.$agendamentoId);
            }

        } catch (Exception $exception) {
            //echo $exception->getMessage();

            $this->data['memoryTipoCobranca'] = $this->financeiro_model->getTipoCobrancaById($agendamento->getTipoCobranca());
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['servicos'] = $this->products_model->getAllProductsAtivo();
            $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();
            $this->data['memoryAgendameto'] = $agendamento;
            $this->data['produto'] = $produto = $this->site->getProductByID($agendamento->getProduto());

            $this->data['error'] = $exception->getMessage();

            $this->load->view($this->theme . 'agendamento/agendamento', $this->data);
        }
    }

    function abrirWhatsAppConfirmacao($agendamento) {

        $Settings = $this->site->get_setting();
        $biller = $this->site->getCompanyByID($Settings->default_biller);
        $endereco = '%0A📍Endereço do estudio '.$biller->address . '-' . $biller->city . '/'.$biller->state.'-'.$biller->postal_code;
        $phone = $biller->phone;

        $phone = str_replace ( '(', '', str_replace ( ')', '', $phone));
        $phone = str_replace ( '-', '',  $phone);
        $phone = str_replace ( ' ', '',  $phone);

        $text = '*👋🏻 Olá%20me%20chamo%20'.$agendamento->getNomeCompleto().'%20acabo%20de%20reservar%20meu%20horário%20para%20o%20ensaio%20'.$agendamento->getNomeProduto().'*%0A📆%20Data%20'. date('d/m/Y', strtotime($agendamento->data)).'%0A⏰%20Hora%20'.$agendamento->horaInicio.'%20-%20'.$agendamento->horaTermino.'%0A📞%20Celular%20'.$agendamento->getCelular().'%0A📧%20E-mail%20'.$agendamento->getEmail().'%0A💰%20Pagamento%20em%20'.$agendamento->getNomeTipoCobranca().$endereco;

        header('Location: https://api.whatsapp.com/send?phone=55'.trim($phone).'&text='.$text);exit;
    }
    function confirmacao($id) {

        $agendamentoModel = $this->AgendamentoServico_model->openAgendamentoModel($id);

        $this->data['tipoCobranca'] = $this->financeiro_model->getTipoCobrancaById($agendamentoModel->getTipoCobranca());
        $this->data['cobranca']    = $this->AgendamentoServico_model->buscarCobranca($agendamentoModel);
        $this->data['agendamento']  = $this->AgendamentoRepository_model->getAgendamentoById($id);

        $this->load->view($this->theme . 'agendamento/agendamentoConfirmacao', $this->data);
    }

    function adicionarAgendamento() {

        $this->verificaUsuarioLogado();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nomeCompleto', lang("nomeCompleto"), 'required');
        $this->form_validation->set_rules('data', lang("data"), 'required');

        if ($this->form_validation->run() == true) {

            $agendamento = new Agendamento_model();

            $isBloqueado = $this->input->post('isBloqueado', true);

            if ($isBloqueado === '1') $isBloqueado = TRUE;
            else $isBloqueado = FALSE;

            $agendamento->setProduto($this->input->post('produto', true));
            $agendamento->setTipoCobranca($this->input->post('tipoCobranca', true));
            $agendamento->setNomeCompleto($this->input->post('nomeCompleto', true));
            $agendamento->setCelular($this->input->post('celular', true));
            $agendamento->setEmail($this->input->post('email', true));
            $agendamento->setData($this->input->post('data', true));
            $agendamento->setDataAte($this->input->post('dataAte', true));
            $agendamento->setHoraInicio($this->input->post('horaInicio', true));
            $agendamento->setHoraTermino($this->input->post('horaTermino', true));
            $agendamento->setCpf( $this->input->post('cpf', true) );
            $agendamento->setIsBloqueado($isBloqueado);
            $agendamento->setSenderHash('');
            $agendamento->setCreditCardToken('');
            $agendamento->setCardName('');

        } elseif ($this->input->post('submit')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            try{
                $agendamentoId = $this->AgendamentoServico_model->salvar($agendamento, false);

                $this->enviarEmail($agendamentoId);

                $this->session->set_flashdata('message', lang("agendamento_adicionado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['servicos'] = $this->products_model->getAllProductsConfirmed();
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['error'] = validation_errors();

            $this->load->view($this->theme . 'agendamento/adicionarAgendamento', $this->data);
        }
    }

    function adicionarAgendamentoBloqueio() {

        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['error'] = validation_errors();

        $this->load->view($this->theme . 'agendamento/adicionarAgendamentoBloqueio', $this->data);
    }

    function editarAgendamento($id) {

        $this->verificaUsuarioLogado();

        $this->load->library('form_validation');

        $this->form_validation->set_rules('nomeCompleto', lang("nomeCompleto"), 'required');
        $this->form_validation->set_rules('data', lang("data"), 'required');

        if ($this->form_validation->run() == true) {
            $agendamento = new Agendamento_model();

            $id = $this->input->post('id', true);
            $isBloqueado = $this->input->post('isBloqueado', true);

            if ($isBloqueado === '1') $isBloqueado = TRUE;
            else $isBloqueado = FALSE;

            $agendamento->setProduto($this->input->post('produto', true));
            $agendamento->setStatus($this->input->post('status', true));
            $agendamento->setTipoCobranca($this->input->post('tipoCobranca', true));
            $agendamento->setNomeCompleto($this->input->post('nomeCompleto', true));
            $agendamento->setCelular($this->input->post('celular', true));
            $agendamento->setEmail($this->input->post('email', true));
            $agendamento->setData($this->input->post('data', true));
            $agendamento->setDataAte($this->input->post('dataAte', true));
            $agendamento->setHoraInicio($this->input->post('horaInicio', true));
            $agendamento->setHoraTermino($this->input->post('horaTermino', true));
            $agendamento->setCpf( $this->input->post('cpf', true) );
            $agendamento->setContaReceber($this->input->post('contaReceber', true));
            $agendamento->setDataReserva($this->input->post('dataReserva', true));
            $agendamento->setIsBloqueado($isBloqueado);
            $agendamento->setSenderHash('');
            $agendamento->setCreditCardToken('');
            $agendamento->setCardName('');

        } elseif ($this->input->post('submit')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            try{

                $isHorarioAlterado = $this->AgendamentoServico_model->editar($id, $agendamento);

                if ($isHorarioAlterado) $this->enviarEmail($id, true);

                $this->session->set_flashdata('message', lang("agendamento_editado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['agendamento'] = $this->AgendamentoRepository_model->getAgendamentoById($id);
            $this->data['servicos'] = $this->products_model->getAllProductsConfirmed();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['error'] = validation_errors();
            $this->load->view($this->theme . 'agendamento/editarAgendamento', $this->data);
        }
    }

    function cancelar($id) {
        try{

            $this->AgendamentoServico_model->cancelar($id);

            $this->enviarEmail($id);

            $this->session->set_flashdata('message', lang("agendamento_editado_com_sucesso"));
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function enviarEmail($id, $horarioAlterado = false) {

        $agendamentoModel = $this->AgendamentoServico_model->openAgendamentoModel($id);
        $status = $agendamentoModel->getStatus();

        if ($agendamentoModel->getEmail() == null) return false;
        if ($status == Agendamento_model::STATUS_BLOQUEADO) return false;

        if ($status == Agendamento_model::STATUS_CANCELADA) {
            $arquivo = 'cancelamento.html';
            $subject = '📷 ⚠️CANCELAMENTO DO ENSAIO ⚠️' . $this->Settings->site_name;
        } else if ($horarioAlterado) {
            $arquivo = 'agendamentoAlterado.html';
            $subject = '📷 ENSAIO ⚠️COM HORÁRIO ALTERADO ⚠️' . $this->Settings->site_name;
        } else {
            $arquivo = 'agendamento.html';
            $subject = '📷 ENSAIO AGENDADO ' . $this->Settings->site_name;
        }

        $message = $this->converterTemplateToMessage($id, $agendamentoModel, $arquivo);

        $to = $agendamentoModel->getEmail().','.$this->Settings->default_email;

        $attachment = $this->pdf($id, null, 'S', $horarioAlterado);

        $this->sma->send_email($to, $subject, $message, null, null, $attachment, '', '');
    }

    private function converterTemplateToMessage($id, $agendamentoModel , $arquivo)  {
        $this->load->library('parser');

        $parse_data = array(
            'reference_number' => $id,
            'contact_person' => $agendamentoModel->getNomeCompleto(),
            'company' => $agendamentoModel->getNomeCompleto(),
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/logo.png" alt="' . $this->Settings->site_name . '"/>',
        );

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/'.$arquivo)) {
            $temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/'.$arquivo);
        } else {
            $temp = file_get_contents('./themes/default/views/email_templates/'.$arquivo);
        }

        return $this->parser->parse_string($temp, $parse_data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null, $isHorarioAlterado = false)
    {
        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $Settings = $this->site->get_setting();

        $agendamento = $this->AgendamentoRepository_model->getAgendamentoById($id);
        $status = $agendamento->status;

        $this->data['biller'] = $this->site->getCompanyByID($Settings->default_biller);
        $this->data['warehouse'] = $this->site->getWarehouseByID($Settings->default_warehouse);
        $this->data['customer'] = $this->site->getCompanyByID($agendamento->cliente);
        $this->data['product']  = $this->products_model->getProductByID($agendamento->produto);
        $this->data['agendamento'] = $agendamento;

        $name = lang("agendamento") . "_" . str_replace('/', '_', $agendamento->id) . ".pdf";
        $html = $this->load->view($this->theme . 'agendamento/pdf', $this->data, true);

        if ($status == Agendamento_model::STATUS_CANCELADA) $showWatermarkImage = base_url('assets/images/cancelado.png');
        else if ($isHorarioAlterado)  $showWatermarkImage = base_url('assets/images/novohorario.png');
        else $showWatermarkImage = base_url('assets/images/agendado.png');

        if ($view) {
            $this->load->view($this->theme . 'agendamento/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer,  $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P', $showWatermarkImage);
        } else {
            $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer,  $margin_bottom = null, $header = null, $margin_top = null, $orientation = 'P', $showWatermarkImage );
        }
    }

    public function pagamentos($agendamentoId = null)
    {
        $this->data['payments'] = $this->AgendamentoServico_model->getPagamentosByFatura($agendamentoId);
        $this->load->view($this->theme . 'financeiro/pagamentos', $this->data);
    }

    public function adicionarPagamento($agendamentoId) {
        redirect('faturas/adicionarPagamento/'.$this->AgendamentoServico_model->adicionarPagamento($agendamentoId));
    }

    function getHorariosConsulta($data) {

        $horarioComercial = $this->HorarioTrabalhoService_model->getHorarioComercial();

        $html = '<div class="row" style="margin-bottom: 25px;">';
        $html .= $this->getHorarioPelaManha($data, $horarioComercial, true);
        $html .= $this->getHorarioPelaTarde($data, $horarioComercial, true);
        $html .= '</div>';

        echo $html;
    }

    function getHorarios($data) {

        $horarioComercial = $this->HorarioTrabalhoService_model->getHorarioComercial();

        $html = '<div class="row" style="margin-bottom: 25px;">';
        $html .= $this->getHorarioPelaManha($data, $horarioComercial, false);
        $html .= $this->getHorarioPelaTarde($data, $horarioComercial, false);
        $html .= '</div>';

        echo $html;
    }

    private function montarHorario($data, $duracao, $intervaloServico, $horaInicio, $horaFinal, $tipo, $isConsulta) {

        $intervalo = ' +'.$duracao.' minutes';
        $data = date('Y-m-d H:i:s', strtotime($data.' '.$horaInicio));

        $html = '<div class="col-md-12" style="margin-top: 10px;"><h2 class="StepTitle" style="text-align: center;font-size: 22px;">'.$tipo.'</h2></div>';
        $isAcrescerIntervalo = false;

        while (strtotime((date('H:i:s', strtotime($data)))) <= $horaFinal) {

            $html .= '<div class="col-md-12">';
            $html .= $this->getHora($data , date('Y-m-d H:i:s', strtotime($data.' +'.$duracao .' minutes')), $isConsulta);
            $html .= '</div>';

            if ($intervalo > 0 && $isAcrescerIntervalo == false) {
                $intervalo = ' +'.($duracao + $intervaloServico).' minutes';
                $isAcrescerIntervalo = true;
            } else {
                $intervalo = ' +'.$duracao .' minutes';
                $isAcrescerIntervalo = false;
            }

            $data = date('Y-m-d H:i:s', strtotime($data.$intervalo));
        }
        return $html;
    }

    private function getHorarioPelaManha($data, $horarioComercial, $isConsulta) {
        $horarioInicioManha = $horarioComercial->getHoraInicioManha($data);
        $horarioFinalManha  = strtotime($horarioComercial->getHoraFinalManha($data));
        $duracao = $horarioComercial->getDuracao();
        $intervalo = $horarioComercial->getIntervalo();

        return $this->montarHorario($data, $duracao,  $intervalo, $horarioInicioManha, $horarioFinalManha, 'Manhã', $isConsulta);
    }

    private function getHorarioPelaTarde($data, $horarioComercial, $isConsulta) {
        $horaInicioTarde = $horarioComercial->getHoraInicioTarde($data);
        $horaFinalTarde  = strtotime($horarioComercial->getHoraFinalTarde($data));
        $duracao = $horarioComercial->getDuracao();
        $intervalo = $horarioComercial->getIntervalo();

        return $this->montarHorario($data, $duracao, $intervalo,  $horaInicioTarde, $horaFinalTarde, 'Tarde', $isConsulta);
    }

    private function getHora($dataNew, $dataNewAte, $tipo) {

        $html = '';
        $Settings = $this->site->get_setting();

        $horaInicio = date('H:i:s', strtotime($dataNew));
        $horaFinal = date('H:i:s', strtotime($dataNewAte));
        $dataNew = date('Y-m-d', strtotime($dataNew));
        $biller = $this->site->getCompanyByID($Settings->default_biller);
        $endereco = '%0A📍'.$biller->address . '-' . $biller->city . '/'.$biller->state.'-'.$biller->postal_code;

        $agendamentosEncontratos = $this->HorarioTrabalhoService_model->agendamentosEncontrado($dataNew, $dataNew, $horaInicio, $horaFinal);

        $html .= '    <div class="col-sm-12" style="border: 1px solid #cccccc;">';
        $html .= '      <label class="radio-inline">';

        if (!$agendamentosEncontratos)  {

            $line_through = '';

            if ($tipo == 'consulta'){
                if ($this->isHorarioDoCafeDaTarde($horaInicio)) {
                    $line_through = 'text-decoration: line-through;';
                    $html .= '<input type="radio" value="'.$horaInicio.'" checked="checked" readonly class="square-red"> ';
                } else {
                    $html .= '<input type="radio" value="'.$horaInicio.'" checked="checked" readonly class="square-green"> ';
                }
            } else {
                if ($this->isHorarioDoCafeDaTarde($horaInicio) || $this->dataMenorQueDataDeHoje($dataNew)) {
                    $line_through = 'text-decoration: line-through;';
                    $html .= '<input type="radio" value="'.$horaInicio.'" checked="checked" readonly class="square-red"> ';
                } else {
                    $html .= '<input type="radio" value="'.$horaInicio.'" name="horarios" class="square-green"> ';
                }
            }

            $html .= '          <span style="'.$line_through.'font-size: 16px;"> '.$horaInicio. 'h até '.$horaFinal.'h</span>';
        } else {
            $html .= '<input type="radio" value="" checked="checked" readonly class="square-red"> ';
            $html .= '          <span style="text-decoration: line-through;font-size: 16px;"> '.$horaInicio. 'h até '.$horaFinal.'h</span>';
        }

        $html .= '      </label>';

        foreach ($agendamentosEncontratos as $agendamentoEncontrato) {
            if ($tipo != 'consulta') break;

            $status = $agendamentoEncontrato->status;
            $dataAgendamento = $agendamentoEncontrato->data;
            $horaInicio = $agendamentoEncontrato->horaInicio;
            $horaTermino = $agendamentoEncontrato->horaTermino;
            $celular = $agendamentoEncontrato->celular;
            $nomeProduto= $agendamentoEncontrato->nomeProduto;
            $status = $this->getStatus($status);

            $hora = $horaInicio . ' - ' . $horaTermino;

            $texto = '*Ensaio%20Agendado:*%0A👋🏻Olá,%20não%20esqueça%20do%20seu%20ensaio%20'.$nomeProduto.'%0A📆%20' . date('d/m/Y', strtotime($dataAgendamento)) . '%0A⏰%20às%20' . $hora . $endereco .'%20%0ATudo certo para este dia?🥰';
            $linkWhatsApp = $wz = 'https://api.whatsapp.com/send?phone=55' . $celular . '&text=' . $texto;

            $html .= '<div class="table-responsive">
                <table class="table table-bordered table-hover" id="sample-table-1">
                    <thead>
                        <tr>
                            <th>Cliente</th>
                            <th>Pacote</th>
                            <th>Cobrança</th>
                            <th>Valor</th>
                            <th>Cadastro</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>' . $agendamentoEncontrato->nomeCompleto.'<br/><small style="font-size: 11px;">⏰ '.$hora. '</small></td>
                            <td>' . $agendamentoEncontrato->servico . '</td>
                            <td>' . $agendamentoEncontrato->cobranca . '</td>
                            <td>' . $this->sma->formatMoney($agendamentoEncontrato->valor) . '</td>
                            <td>' . $this->sma->hrsd($agendamentoEncontrato->dataReserva) . '</td>
                            <td>' . $status . '</td>
                            <td><a href="' . $linkWhatsApp . '" target="_blank"><img style="max-width: 50px;" src="' . base_url() . '/assets/rapido/images/whatsapp.png"></a></td>
                        </tr>
                        </tbody>
                        </table>
                    </div>';
        }

        $html .= '    </div>';

        return $html;
    }

    private function getStatus($status) {
        if ($status === Agendamento_model::STATUS_PAGO) $status = '<span class="label label-sm label-success">Pago</span>';
        if ($status === Agendamento_model::STATUS_AGUARDANDO_PAGAMENTO) $status = '<span class="label label-sm label-danger">Á Pagar</span>';
        if ($status === Agendamento_model::STATUS_CANCELADA) $status = '<span class="label label-sm label-inverse">Cancelado</span>';
        if ($status === Agendamento_model::STATUS_ORCAMENTO) $status = '<span class="label label-sm label-warning">Orçamento</span>';
        if ($status === Agendamento_model::STATUS_REALIZADO) $status = '<span class="label label-sm label-info">Realizado</span>';
        if ($status === Agendamento_model::STATUS_BLOQUEADO) $status = '<span class="label label-sm label-warning">Bloqueado</span>';

        return $status;
    }

    public function isHorarioDoCafeDaTarde($horaInicio) {
        return $horaInicio === HorarioTrabalho_model::HORARIO_CAFE_DA_TARDE;
    }

    public function dataMenorQueDataDeHoje($data) {
        $dataHoje = date('Y-m-d');

        return  $data < $dataHoje;
    }
}?>