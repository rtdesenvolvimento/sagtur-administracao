<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Administracao extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('customers', $this->Settings->user_language);
        $this->load->model('companies_model');

        //$this->output->cache(5);
    }

    function index()
    {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('administracao')));
        $meta = array('page_title' => lang('administracao'), 'bc' => $bc);

        $customers = $this->getAllCompanies();
        $data = [];

        foreach ($customers as $customer) {

            $cnpj = preg_replace('/[^0-9]/', '', $customer->vat_no);
            $cg = $this->site->getCustomerGroupByID($customer->customer_group_id);

            if ($customer->name_db && $customer->name_db != 'DESBLOQUEADO') {

                $db_config = $this->getConfiguracaoDB($customer->name_db);
                $db = $this->load->database($db_config, TRUE);
                $configuracaoGeral = $this->get_setting($db);

                $dadosFatura = $this->getDataUltimoVencimentoAberta($customer->id);
                $dataUltimaVenda = $this->getDataultimaVenda($db)->ultimaVenda;
                $totalVendas = $this->getTotalDeVendas($db)->totalVendas;
                $qtdVendas = $this->getQuantidadeVendasAno($db)->qtd;
                $totalUsuarios = $this->getTotalUsuarios($db)->qtd;
                $dataUltimoPagamentoMesCorrente = $this->getDataUltimoVencimentoQuitada($customer->id)->ultimoVencimentoFatura;

                $data[] = array(
                    'name' => $customer->name,
                    'status' => $configuracaoGeral->status,
                    'versao' => $configuracaoGeral->version,
                    'googleAnalytics' => $configuracaoGeral->googleAnalytics,
                    'dataUltimaVenda' => $dataUltimaVenda,
                    'totalVendas' => $totalVendas,
                    'qtdVendas' => $qtdVendas,
                    'totalUsuarios' => $totalUsuarios,
                    'nome_grupo' => $cg->name,
                    'dataUltimoVencimento' =>  $dadosFatura->ultimoVencimentoFatura,
                    'statusUltimoVencimento' => $dadosFatura->status,
                    'dataUltimoPagamentoMesCorrente' => $dataUltimoPagamentoMesCorrente,
                    'cnpj' => $cnpj
                );
            } else {
                $data[] = array(
                    'name' => '<span style="color: rgba(255,0,0,0.66);">(NÃO ENCONTRAMOS)</span>' .$customer->name,
                    'status' => '',
                    'versao' => '',
                    'googleAnalytics' => '',
                    'dataUltimaVenda' => '',
                    'totalVendas' => '',
                    'totalUsuarios' => 0,
                    'nome_grupo' => $cg->name,
                    'dataUltimoVencimento' => '',
                    'statusUltimoVencimento' => 'NAO ENCONTRADO',
                    'dataUltimoPagamentoMesCorrente' => null,
                    'cnpj' => $cnpj
                );
            }
        }

        $this->data['customers'] = $data;

        $this->page_construct('administracao/index', $meta, $this->data);
    }


    function acesso()
    {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('administracao')));
        $meta = array('page_title' => lang('administracao'), 'bc' => $bc);

        $customers = $this->getAllCompanies();
        $empresas = $this->getEmpresas();

        foreach ($customers as $customer) {
            $cnpj = $customer->vat_no;

            $cnpj = str_replace('-', '', $cnpj);
            $cnpj = str_replace('/', '', $cnpj);
            $cnpj = str_replace('.', '', $cnpj);

            $cnpjempresa = $empresas[$cnpj];

            if ($this->encontrou($cnpjempresa, $cnpj)) {
                $data[] = array(
                    'name' => $customer->name,
                    'cnpj' => $cnpj
                );
            } else {
                $data[] = array(
                    'name' => '(NÃO ENCONTRAMOS) '.$customer->name,
                    'cnpj' => $cnpj
                );
            }
        }

        $this->data['customers'] = $data;

        $this->page_construct('administracao/acesso', $meta, $this->data);
    }


    public function naoEncontrou($cnpjempresa, $cnpj) {
        return $cnpjempresa == $cnpj;
    }

    public function encontrou($cnpjempresa, $cnpj) {
        return !$this->naoEncontrou($cnpjempresa, $cnpj);
    }


    private function getEmpresas(): array
    {

        // Consulta no banco de dados para pegar todos os name_db
        $this->db->select('vat_no, name_db');
        $this->db->from('companies');
        $query =  $this->db->get();

        $empresas = [];
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                // Normaliza o CNPJ removendo caracteres especiais
                $cnpj = preg_replace('/[^0-9]/', '', $row->vat_no);

                if ($row->name_db != '') {
                    $empresas[$cnpj] = $row->name_db;
                }

            }
        }

        return $empresas;
    }


    public function inativar() {

        $empresas = $this->getEmpresas();

        $cnpj = $this->input->get('cnpj');
        $cnpjempresa = $empresas[$cnpj];
        $db_config = $this->getConfiguracaoDB($cnpjempresa);

        $db = $this->load->database($db_config, TRUE);
        $configuracaoGeral = $this->get_setting($db);
        $data = array('status' => 0);

        $this->updateStatusSettings($configuracaoGeral->setting_id, $data, $db);
    }

    public function ativar() {
        $empresas = $this->getEmpresas();

        $cnpj = $this->input->get('cnpj');
        $cnpjempresa = $empresas[$cnpj];
        $db_config = $this->getConfiguracaoDB($cnpjempresa);

        $db = $this->load->database($db_config, TRUE);

        $configuracaoGeral = $this->get_setting($db);

        $data = array('status' => 1);

        $this->updateStatusSettings($configuracaoGeral->setting_id, $data, $db);
    }

    private function getConfiguracaoDB($cnpjempresa): array
    {
        $db_config = array(
            'dsn'	=> '',
            //'hostname' => 'node157874-sagtur-production-db-server.jelastic.saveincloud.net:14084',
            'hostname' => '10.100.53.149',
            'username' => 'root',
            'password' => 'DOYqah54213',
            'database' => $cnpjempresa,
            'dbdriver' => 'mysqli',
            'dbprefix' => 'sma_',
            'pconnect' => FALSE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => TRUE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => FALSE
        );

        return $db_config;
    }

    public function modal_view($cnpj = null)
    {
        $empresas = $this->getEmpresas();

        $db = $this->getDB($cnpj);
        $configuracaoGeral = $this->get_setting($db);

        $this->data['configuracaoGeral'] = $configuracaoGeral;
        $this->data['nome_empresa'] = $empresas[$cnpj];
        $this->data['users'] = $this->getUsers($cnpj);
        $this->load->view($this->theme . 'administracao/modal_view', $this->data);
    }

    public function updateStatusSettings($id, $data = array(), $db = NULL)
    {
        $db->where('setting_id', $id);
        if ($db->update('settings', $data)) {
            return true;
        }
        return false;
    }

    private function get_setting($db) {
        $q = $db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function getUsers($cnpj) {

        $db = $this->getDB($cnpj);

        $db->select("users.group_id as tipo, users.*, companies.*", FALSE);
        $db->join('companies', 'users.biller_id=companies.id');
        $db->where('users.active', 1);
        $db->where('users.email not in("andre@resultatec.com.br")');
        $db->order_by('name');

        $q = $db->get('users');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;

    }

    public function getDB($cnpj) {

        $empresas = $this->getEmpresas();
        $cnpjempresa = $empresas[$cnpj];
        $db_config = $this->getConfiguracaoDB($cnpjempresa);
        $db = $this->load->database($db_config, TRUE);
        return $db;
    }

    public function getDataultimaVenda($db)
    {
        $db->select('max(sma_sales.date) ultimaVenda', false);

        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalDeVendas($db)
    {
        $db->select('sum(sma_sales.grand_total) totalVendas', false);

        $db->where("date_format(date, '%Y') = '".date('Y')."' ");
        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuantidadeVendasAno($db)
    {
        $db->select('count(sma_sales.id) qtd', false);
        $db->where("date_format(date, '%Y') = '".date('Y')."' ");
        $db->where('sale_status','faturada');

        $q = $db->get_where('sales', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalUsuarios($db)
    {
        $db->select('count(sma_users.id) qtd', false);

        $db->where('active', 1);

        $q = $db->get_where('users', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalAutomoveis($db)
    {
        $db->select('count(sma_automovel.id) qtd', false);

        $q = $db->get_where('automovel', array(), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies() {

        $this->db->order_by('name');

        $q = $this->db->get_where('companies', array('group_name' => 'customer', 'active' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getDataUltimoVencimentoAberta($pessoaId)
    {
        $this->db->select('min(sma_fatura.dtvencimento) ultimoVencimentoFatura, max(sma_fatura.status) as status', false);

        $this->db->where('status', 'ABERTA');

        $q = $this->db->get_where('fatura', array('pessoa' => $pessoaId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDataUltimoVencimentoQuitada($pessoaId)
    {
        $this->db->select('max(sma_fatura.dtvencimento) ultimoVencimentoFatura', false);

        $this->db->where('status', 'QUITADA');
        $this->db->where('MONTH(dtvencimento) = MONTH(CURDATE())');
        $this->db->where('YEAR(dtvencimento) = YEAR(CURDATE())');

        $q = $this->db->get_where('fatura', array('pessoa' => $pessoaId), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}