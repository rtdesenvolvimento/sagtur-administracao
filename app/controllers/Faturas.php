<?php defined('BASEPATH') or exit('No direct script access allowed');

class Faturas extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('financeiro', $this->Settings->user_language);
        $this->lang->load('reports', $this->Settings->user_language);

        //service
        $this->load->model('service/MaloteService_model', 'MaloteService_model');

        //repository
        $this->load->model('repository/GiftRepository_model', 'GiftRepository_model');
        $this->load->model('repository/MaloteRepository_model', 'MaloteRepository_model');
        $this->load->model('repository/Fatura_Repository_model', 'Fatura_Repository_model');

        //models
        $this->load->model('model/Malote_model', 'Malote_model');
        $this->load->model('model/MaloteFatura_model', 'MaloteFatura_model');
        $this->load->model('model/Fatura_model', 'Fatura_model');

        //models
        $this->load->model('settings_model');

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '16384';
    }

    public function adicionarPagamento($faturaId = null)
    {
        $this->load->library('form_validation');

        if ($this->input->get('id')) {
            $faturaId = $this->input->get('id');
        }

        $fatura = $this->site->getFaturaById($faturaId);

        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('formapagamento', lang("formapagamento"), 'required');
        $this->form_validation->set_rules('movimentador', lang("movimentador"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            //if ($this->input->post('paid_by') == 'deposit') {
            //  $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            // $customer_id = $sale->customer_id;
            //  if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
            // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            // redirect($_SERVER["HTTP_REFERER"]);
            //  }
            // } else {
            $customer_id = null;
            // }

            /*
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            */

            $this->load->model('sales_model');

            $date           = $this->sma->fld(trim($this->input->post('date')));
            $formaPagamento = $this->site->getFormaPagamentoById($this->input->post('formapagamento'));
            $faturaParcela  = $this->site->getParcelaOneByFatura($faturaId);
            $parcela        = $this->site->getParcelaById($faturaParcela->parcela);

            $payment = array(
                'date'              => $date,
                'formapagamento'    => $this->input->post('formapagamento'),
                'movimentador'      => $this->input->post('movimentador'),
                'tipoCobranca'      => $this->input->post('tipoCobrancaPagamento'),
                'fatura'            => $fatura->id,
                'pessoa'            => $fatura->pessoa,
                'paid_by'           => $formaPagamento->name,
                'amount'            => $this->input->post('amount-paid'),
                'reference_no'      => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'cheque_no'         => $this->input->post('cheque_no'),
                'cc_no'             => $this->input->post('gift_card_no'),
                'cc_holder'         => $this->input->post('pcc_holder'),
                'cc_month'          => $this->input->post('pcc_month'),
                'cc_year'           => $this->input->post('pcc_year'),
                'cc_type'           => $this->input->post('pcc_type'),
                'note'              => $this->input->post('note'),
                'created_by'        => $this->session->userdata('user_id'),
            );

            if ($parcela->contareceberId) {//vendas
                $contaFinanceiro   = $this->site->getContaReceberById($parcela->contareceberId);
                $payment['sale_id'] = $contaFinanceiro->sale;
            }

            if ($fatura->tipooperacao == 'CREDITO') {
                $payment['tipo'] = Sales_model::TIPO_DE_PAGAMENTO_RECEBIMENTO;
                $payment['type'] = 'received';
            } else {
                $payment['tipo'] = Sales_model::TIPO_DE_PAGAMENTO_PAGAMENTO;
                $payment['type'] = 'sent';
            }

            $payment['attachment'] = $this->upload_arquivo_payments();

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('adicionarPagamento')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {
            try {

                $this->sales_model->addPayment($payment, $customer_id, $faturaId);

                $this->session->set_flashdata('message', lang("payment_added"));

                redirect($_SERVER["HTTP_REFERER"]);
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $tipoCobranca                   = $this->site->getTipoCobrancaById($fatura->tipoCobranca);

            $this->data['fatura']           = $fatura;
            $this->data['payment_ref']      = $this->site->getReference('pay');
            $this->data['tiposCobranca']    = $this->site->getAllTiposCobranca();
            $this->data['movimentadores']   = $this->settings_model->getAllContas();
            $this->data['formaspagamento']  = $this->settings_model->getAllFormasPagamento();
            $this->data['formaPagamentoId'] = $tipoCobranca->formapagamento;
            $this->data['gift_card']        = $this->GiftRepository_model->getGiftByFatura($fatura->id);
            $this->data['gifts_card']       = $this->GiftRepository_model->getGiftCardByCustomer($fatura->pessoa);
            $this->data['modal_js']         = $this->site->modal_js();

            $this->load->view($this->theme . 'faturas/adicionarPagamento', $this->data);
        }
    }

    private function upload_arquivo_payments()
    {
        $photo = null;
        if ($_FILES['userfile']['size'] > 0) {

            $this->load->library('upload');

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->digital_file_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $photo = $this->upload->file_name;
        }

        return $photo;
    }

    public function editarFatura($id = null)
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('valorfatura', lang("valorfatura"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');
        $this->form_validation->set_rules('movimentador', lang("movimentador"), 'required');

        if ($this->form_validation->run() == true) {

            $faturaModel = new Fatura_model();
            $faturaModel->faturaId = $this->input->post('id', true);
            $faturaModel->dtvencimento = $this->input->post('dtvencimento', true);
            $faturaModel->tipocobranca = $this->input->post('tipocobranca', true);
            $faturaModel->valorfatura = $this->input->post('valorfatura', true);
            $faturaModel->pessoa = $this->input->post('pessoa', true);
            $faturaModel->receita = $this->input->post('receita', true);
            $faturaModel->totalAcrescimo = $this->input->post('totalAcrescimo', true);
            $faturaModel->totalDesconto = $this->input->post('totalDesconto', true);
            $faturaModel->valorpagar = $this->input->post('valorpagar', true);
            $faturaModel->nota = $this->input->post('note', true);
            $faturaModel->movimentador = $this->input->post('movimentador', true);

        } elseif ($this->input->post('editarFatura')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run()) {
            try {
                $this->load->model('financeiro_model');

                $this->financeiro_model->editarFatura($faturaModel);

                $fatura = $this->site->getFaturaById($this->input->post('id', true));
                $cobranca = $this->site->getCobrancaIntegracaoByFatura($fatura->id);

                if (count($cobranca) > 0) {
                    $link = '';

                    if ($cobranca->link) $link .= '<a href="' . $cobranca->link . '" target="_blank"><i class="fa fa-barcode"></i> Clique aqui para abrir a fatura</a><br/>';
                    if ($cobranca->checkoutUrl) $link .= '<a href="' . $cobranca->checkoutUrl . '" target="_blank"><i class="fa fa-link"></i> Clique aqui para abrir o link de pagamento</a><br/>';

                    $this->session->set_flashdata('info', lang("fatura_editada_com_sucesso_gerado_link_cobranca") . $link);

                } else {
                    $this->session->set_flashdata('message', lang('fatura_editada_com_sucesso'));
                }

            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if ($this->data['error'] != '') {
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['fatura']               = $this->site->getFaturaById($id);
            $this->data['warehouses']           = $this->site->getAllWarehouses();
            $this->data['receitas']             = $this->site->getAllReceitasSuperiores();
            $this->data['despesas']             = $this->site->getAllDespesasSuperiores();
            $this->data['tiposCobranca']        = $this->site->getAllTiposCobranca();
            $this->data['condicoesPagamento']   = $this->site->getAllCondicoesPagamento();
            $this->data['movimentadores']       = $this->site->getAllContas();

            $this->load->view($this->theme . 'faturas/editarFatura', $this->data);
        }
    }

    public function buscaTipoCobrancaById($id)
    {
        echo json_encode($this->site->getTipoCobrancaById($id));
    }

    public function baixa_selecao_actions()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'baixa_selecao') {

                    try {
                        $malote = new Malote_model();

                        $malote->date = date('Y-m-d');
                        $malote->created_by = $this->session->userdata('user_id');
                        $malote->created_at = date('Y-m-d H:i:s');

                        $totalFatura = 0;
                        $totalPago = 0;

                        foreach ($_POST['val'] as $id) {
                            $fatura = $this->site->getFaturaById($id);

                            if (($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') && $fatura->valorpagar > 0) {
                                $maloteFatura = new MaloteFatura_model();
                                $maloteFatura->fatura_id    = $fatura->id;
                                $maloteFatura->valor_fatura = $fatura->valorfatura;
                                $maloteFatura->valor_pagar  = $fatura->valorpagar;

                                $totalFatura = $totalFatura + $fatura->valorfatura;
                                $totalPago = $totalPago + $fatura->valorpago;

                                $malote->addFatura($maloteFatura);
                            }
                        }

                        $malote->total = $totalFatura;
                        $malote->total_pago = $totalPago;

                        $malote_id = $this->MaloteService_model->save($malote);

                        redirect('faturas/baixa_fatura_selecao/' . $malote_id);

                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } elseif ($this->input->post('form_action') == 'export_excel') {
                    $this->export_excel();
                } elseif ($this->input->post('form_action') == 'nf_agendamento') {
                    $this->gerar_nf_agendamento();
                } else if($this->input->post('form_action') == 'cancelar_fatura_em_lote') {

                    $this->load->model('financeiro_model');

                    foreach ($_POST['val'] as $faturaId) {
                        $this->FinanceiroService_model->cancelar($faturaId, 'Cancelamento de Fatura em Lote');
                    }

                    $this->session->set_flashdata('message', lang("faturas_canceladas_com_sucesso"));

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_fatura_baixa_selecao_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    private function gerar_nf_agendamento()
    {
        if (!empty($_POST['val'])) {

            try {

                $this->load->model('model/NFAgendamento', 'NFAgendamento');
                $this->load->model('service/NotafiscalService_model', 'NotafiscalService_model');

                foreach ($_POST['val'] as $id) {

                    $fatura = $this->site->getFaturaById($id);

                    if (($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') && $fatura->valorpagar > 0 && $fatura->numero_transacao != '') {

                        $instrucoes = '';
                        if ($fatura->note) {
                            $nota = strip_tags($fatura->note.' - '.$fatura->product_name);
                            $nota = str_replace ( ':', ' - ', $nota);
                            $nota = str_replace ( '&', ' - ', $nota);
                            $instrucoes .= $nota.' ';
                        }

                        $nfa = new NFAgendamento();

                        $nfa->status = NFAgendamento::GERADA;
                        $nfa->type_nf = 'nfs';

                        $nfa->code = $fatura->numero_transacao;
                        $nfa->customer_id = $fatura->pessoa;
                        $nfa->fatura_id = $fatura->id;
                        $nfa->externalReference = $fatura->id;
                        $nfa->valor_nf = $fatura->valorfatura;
                        $nfa->effectiveDate = date('Y-m-d');
                        $nfa->observations = $instrucoes;
                        $nfa->deductions = 0;

                        $nfa->municipalServiceCode = '2.01';
                        $nfa->municipalServiceName = '2.01 - Serviços de pesquisas e desenvolvimento de qualquer natureza.';
                        $nfa->updatePayment = 0;

                        $this->NotafiscalService_model->salvar($nfa);
                    }
                }

                redirect('notafiscal/agendamento');

            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
    }

    public function export_excel()
    {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('companies'));

        $query  = $this->Fatura_Repository_model->getFaturasByFilter($_POST['val']);
        $fields = $query->list_fields();

        $col = 0;
        foreach ($fields as $field) {
            $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, lang($field));
            $col++;
        }

        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $this->excel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }
            $row++;
        }

        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 12,
            ],
        ];


        foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
            $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
            $this->excel->getActiveSheet()->getStyle($columnID . '1')->applyFromArray($styleArray);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

        $filename = 'faturas_' . date('Y_m_d_H_i_s') . '.xls';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    public function baixa_fatura_selecao($malote_id)
    {

        $malote = $this->MaloteRepository_model->getByID($malote_id);
        $this->data['tiposCobranca']    = $this->site->getAllTiposCobranca();
        $this->data['movimentadores']   = $this->settings_model->getAllContas();
        $this->data['formaspagamento']  = $this->settings_model->getAllFormasPagamento();
        $this->data['malote']           = $malote;
        $this->data['created_by']       = $this->site->getUser($malote->created_by);
        $this->data['updated_by']       = $malote->updated_by ? $this->site->getUser($malote->updated_by) : null;
        $this->data['tipoCobrancaDefoult'] = $this->site->getTipoCobrancaById($this->Settings->tipo_cobranca_fechamento_id);

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('baixa_fatura_selecao'))
        );

        $meta = array('page_title' => lang('baixa_fatura_selecao'), 'bc' => $bc);
        $this->page_construct('faturas/baixaFaturaSelecao', $meta, $this->data);
    }


    function getFaturasMalote($malote_id) {

        $this->load->library('datatables');

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento as data_vencimento,
                {$this->db->dbprefix('companies')}.name, 
                {$this->db->dbprefix('fatura')}.product_name,
                {$this->db->dbprefix('fatura')}.strDespesas as despesa,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipo_cobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.valorpago, 
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id")
            ->from('fatura')
            ->join('malote_faturas', 'malote_faturas.fatura_id = fatura.id')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca', 'left')
            ->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL', 'QUITADA')");

        $this->datatables->where('malote_faturas.malote_id', $malote_id);

        echo $this->datatables->generate();
    }

    public function baixaFaturaSelecaoPagar()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('tipoCobrancaPagamento', lang("tipo_cobranca_financeiro"), 'required');
        $this->form_validation->set_rules('movimentador', lang("conta_destino"), 'required');
        $this->form_validation->set_rules('formapagamento', lang("forma_pagamento"), 'required');

        if ($this->form_validation->run() == true) {

            $this->load->model('sales_model');

            $faturasMalote  = $this->MaloteRepository_model->getFaturasByMalote($this->input->post('malote_id'));
            $date           = $this->sma->fld(trim($this->input->post('data_pagamento')));
            $formaPagamento = $this->site->getFormaPagamentoById($this->input->post('formapagamento'));

            $customer_id = null;
            $attachment = $this->upload_arquivo_payments();

            foreach ($faturasMalote as $faturaMalote) {

                $fatura         = $this->site->getFaturaById($faturaMalote->fatura_id);
                $faturaParcela  = $this->site->getParcelaOneByFatura($faturaMalote->fatura_id);
                $parcela        = $this->site->getParcelaById($faturaParcela->parcela);

                if ($fatura->status == 'QUITADA') {
                    continue;
                }

                $payment = array(
                    'date'              => $date,
                    'reference_no'      => $this->site->getReference('pay'),
                    'formapagamento'    => $this->input->post('formapagamento'),
                    'movimentador'      => $this->input->post('movimentador'),
                    'tipoCobranca'      => $this->input->post('tipoCobrancaPagamento'),
                    'fatura'            => $fatura->id,
                    'pessoa'            => $fatura->pessoa,
                    'paid_by'           => $formaPagamento->name,
                    'amount'            => $fatura->valorpagar,
                    'cheque_no'         => $this->input->post('cheque_no'),
                    'cc_no'             => $this->input->post('gift_card_no'),
                    'cc_holder'         => $this->input->post('pcc_holder'),
                    'cc_month'          => $this->input->post('pcc_month'),
                    'cc_year'           => $this->input->post('pcc_year'),
                    'cc_type'           => $this->input->post('pcc_type'),
                    'note'              => $this->input->post('note'),
                    'attachment'        => $attachment,
                    'created_by'        => $this->session->userdata('user_id'),
                );

                if ($parcela->contareceberId) {//vendas
                    $contaFinanceiro   = $this->site->getContaReceberById($parcela->contareceberId);
                    $payment['sale_id'] = $contaFinanceiro->sale;
                }

                if ($fatura->tipooperacao == 'CREDITO') {
                    $payment['tipo'] = Sales_model::TIPO_DE_PAGAMENTO_RECEBIMENTO;
                    $payment['type'] = 'received';
                } else {
                    $payment['tipo'] = Sales_model::TIPO_DE_PAGAMENTO_PAGAMENTO;
                    $payment['type'] = 'sent';
                }

                try {

                    $this->sales_model->addPayment($payment, $customer_id, $fatura->id);

                } catch (Exception $exception) {
                    $this->session->set_flashdata('error', $exception->getMessage());
                }
            }

            $malote = $this->MaloteRepository_model->openMaloteModel($this->input->post('malote_id'));

            $malote->movimentador_id = $this->input->post('movimentador');
            $malote->forma_pagamento_id = $this->input->post('formapagamento');
            $malote->tipo_cobranca_id = $this->input->post('tipoCobrancaPagamento');
            $malote->dt_fechamento = date('Y-m-d H:i:s');
            $malote->note = $this->input->post('note');

            $malote->updated_by = $this->session->userdata('user_id');
            $malote->updated_at = date('Y-m-d H:i:s');
            $malote->status = 'Fechado';

            $this->MaloteService_model->edit($malote, $this->input->post('malote_id'));

            $this->session->set_flashdata('message', lang("payment_added"));

            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}