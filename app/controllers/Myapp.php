<?php defined('BASEPATH') or exit('No direct script access allowed');

class Myapp extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        //repository
        $this->load->model('repository/MyappRepository_model', 'MyappRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');

        //service
        $this->load->model('service/MyappService_model', 'MyappService_model');
    }

    public function index() {
        $this->data['vendedor'] = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->load->view($this->theme . 'myapp/index', $this->data);
    }

    public function meus_dados($customer_id = null) {

        $this->data['customer'] = $this->site->getCompanyByID($customer_id);

        $this->load->view($this->theme . 'myapp/meus_dados', $this->data);
    }

    public function proximas_viagens($customer_id = null) {

        $this->data['customer'] = $this->site->getCompanyByID($customer_id);
        $this->data['sales']    = $this->MyappRepository_model->getAllInvoiceItems($customer_id);

        $this->load->view($this->theme . 'myapp/proximas_viagens', $this->data);
    }

    public function viagens_passadas($customer_id = null) {
        $this->data['customer'] = $this->site->getCompanyByID($customer_id);
        $this->data['sales'] = $this->MyappRepository_model->getAllInvoiceItems($customer_id, null, FALSE);

        $this->load->view($this->theme . 'myapp/viagens_passadas', $this->data);
    }

    public function servico($product_id, $sale_id) {

        $this->data['inv'] = $this->site->getInvoiceByID($sale_id);
        $this->data['product'] = $this->site->getProductByID($product_id);

        $this->load->view($this->theme . 'myapp/servico', $this->data);
    }

    public function minhas_faturas($customer_id = null) {

        $this->data['customer'] = $this->site->getCompanyByID($customer_id);
        $this->data['sales'] = $this->MyappRepository_model->getAllInvoiceItems($customer_id);

        $this->load->view($this->theme . 'myapp/minhas_faturas', $this->data);
    }

    public function faturas($customer_id, $sale_id) {

        $this->data['customer'] = $this->site->getCompanyByID($customer_id);
        $this->data['sales'] = $this->MyappRepository_model->getAllInvoiceItems($customer_id, $sale_id);

        $this->load->view($this->theme . 'myapp/faturas', $this->data);
    }

    public function viagens_checkin($customer_id = null) {
        $this->data['customer'] = $this->site->getCompanyByID($customer_id);
        $this->data['sales'] = $this->MyappRepository_model->getAllInvoiceItems($customer_id);

        $this->load->view($this->theme . 'myapp/viagens_checkin', $this->data);
    }

    public function transportes($customer_id, $sale_id)  {

        $proxima_viagem = $this->MyappRepository_model->getProximaViagem($customer_id, $sale_id);

        $this->data['tiposTransporte']  = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
        $this->data['product_id']       = $proxima_viagem->product_id;
        $this->data['customer_id']      = $customer_id;
        $this->data['sale_id']          = $sale_id;

        $this->load->view($this->theme . 'myapp/transportes', $this->data);
    }

    public function marcar_assento($sale_id, $customer_id, $tipoTransporteID) {

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $proxima_viagem = $this->MyappRepository_model->getProximaViagem($customer_id, $sale_id);
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($proxima_viagem->programacaoId);

        $this->data['proxima_viagem']   = $proxima_viagem;
        $this->data['programacao']      = $programacao;
        $this->data['tipoTransporteID'] = $tipoTransporteID;
        $this->data['tipoTransporte']   = $this->site->getTipoTransporteID($tipoTransporteID);
        $this->data['product']          = $this->site->getProductByID($proxima_viagem->product_id);
        $this->data['customer']         = $this->site->getCompanyByID($proxima_viagem->customerClient);

        if ($proxima_viagem->pagador == $proxima_viagem->customerClient) {
            $this->data['items']      = $this->MyappRepository_model->getProximaViagemBySaleID($proxima_viagem->sale_id);
            $this->load->view($this->theme . 'myapp/marcar_assento_customer_dependente', $this->data);
        } else {
            if ($programacao->permitir_marcacao_dependente) {
                $this->data['items']      = $this->MyappRepository_model->getProximaViagemBySaleID($proxima_viagem->sale_id);
                $this->load->view($this->theme . 'myapp/marcar_assento_customer_dependente', $this->data);
            } else {
                $this->load->view($this->theme . 'myapp/marcar_assento_customer', $this->data);
            }
        }
    }

    public function logar() {

        $cpf = $this->input->get("cpf");
        $dataNascimento = $this->input->get("data_nascimento");
        $email = $this->input->get("email");

        list($dia, $mes, $ano) = explode("/", $dataNascimento);
        $dataNascimento = "$ano-$mes-$dia";

        $this->db->select('companies.id as id, companies.name as nome, companies.email as email, companies.cf5 as celular');
        $this->db->limit(1);
        $this->db->where('vat_no', $this->db->escape_str($cpf));

        if ($this->Settings->usar_email_area_cliente) {
            $this->db->where('email', $this->db->escape_str($email));
        }

        if ($this->Settings->usar_dtnascimento_area_cliente) {
            $this->db->where('data_aniversario', $this->db->escape_str($dataNascimento));
        }

        $q = $this->db->get('companies');

        if ($q->num_rows() > 0) {
            $user = $q->row();

            $proxima_viagem = $this->MyappRepository_model->getProximaViagem($user->id);
            $exibir_marcacao_assento = false;

            if ($proxima_viagem) {
                $exibir_marcacao_assento = $this->MyappService_model->onibus_ativo_marcacao($proxima_viagem);
            }

            return $this->sma->send_json( array('user' => $user, 'retorno' => true, 'habilitar_marcacao' => $exibir_marcacao_assento));
        }

        return $this->sma->send_json(array('user' => false, 'retorno' => false, 'habilitar_marcacao' => false));
    }

}