<?php defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //dto
        $this->load->model('dto/VendaFilterDTO_model', 'VendaFilterDTO_model');
        $this->load->model("dto/AgendaViagemDTO_model", 'AgendaViagemDTO_model');
        $this->load->model('dto/CupomDesconto_DTO_model', 'CupomDesconto_DTO_model');
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //repository
        $this->load->model('repository/AgendaViagemRespository_model','AgendaViagemRespository_model');
        $this->load->model("repository/ProdutoRepository_model", 'ProdutoRepository_model');
        $this->load->model('repository/ValorFaixaRepository_model', 'ValorFaixaRepository_model');
        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/CupomDescontoRepository_model', 'CupomDescontoRepository_model');
        $this->load->model('repository/CorAgendaRepository_model', 'CorAgendaRepository_model');

        //model
        $this->load->model('model/AgendaViagem_model', 'AgendaViagem_model');

        $this->load->model('products_model');
        $this->load->model('Roomlist_model');
        $this->load->model('db_model');
        $this->load->model('reports_model');
        $this->load->model('settings_model');
        //$this->load->model('financeiro_model');
        //$this->load->model("sales_model");

        $this->lang->load('schedules', $this->Settings->user_language);

        $this->load->library('form_validation');
    }

    function index() {

        $this->data['products'] = $this->products_model->getAllProductsTravel();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['statusFilter'] = 'Confirmado';
        $this->data['anoFilter'] = date('Y');
        $this->data['mesFilter'] = 'PROXIMAS';

        if ($this->Settings->receptive) {
            $date = new DateTime(date('Y-m-d'));
            $firstDay = $date->modify('first day of this month')->format('Y-m-d');
            $lastDay = $date->modify('last day of this month')->format('Y-m-d');

            //$this->data['start_date_filter'] = $firstDay;
            //$this->data['end_date_filter'] = $lastDay;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('agenda')));
        $meta = array('page_title' => lang('detalhes_servico'), 'bc' => $bc);
        $this->page_construct('agenda/index', $meta, $this->data);
    }

    public function consulta_disponibilidade() {

        $this->data['products'] = $this->products_model->getAllTours();
        $this->data['modal_js'] = $this->site->modal_js();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('consulta_disponibilidade')));
        $meta = array('page_title' => lang('consulta_disponibilidade'), 'bc' => $bc);

        $this->page_construct('agenda/consulta_disponibilidade', $meta, $this->data);
    }


    public function availability($productID, $year = NULL, $month = NULL) {

        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }

        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => site_url('agenda/availability/'.$productID),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
		{heading_row_start}<tr>{/heading_row_start}
		{heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
		{heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
		{heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
		{heading_row_end}</tr>{/heading_row_end}
		{week_row_start}<tr>{/week_row_start}
		{week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
		{week_row_end}</tr>{/week_row_end}
		{cal_row_start}<tr class="days">{/cal_row_start}
		{cal_cell_start}<td class="day">{/cal_cell_start}
		{cal_cell_content}
		<div class="day_num">{day}</div>
		<div class="content">{content}</div>
		{/cal_cell_content}
		{cal_cell_content_today}
		<div class="day_num highlight">{day}</div>
		<div class="content">{content}</div>
		{/cal_cell_content_today}
		{cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
		{cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
		{cal_cell_blank}&nbsp;{/cal_cell_blank}
		{cal_cell_end}</td>{/cal_cell_end}
		{cal_row_end}</tr>{/cal_row_end}
		{table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);

        $filter = new ProgramacaoFilter_DTO_model();

        if ($productID) {
            $filter->produto = $productID;
        }

        $filter->group_data_website = false;
        $filter->mes = $month;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);
        $programacoesArray = [];

        if (!empty($programacoes)) {
            foreach ($programacoes as $programacao) {
                $dataSaida = date('m-d', strtotime($programacao->dataSaida));

                if (isset($programacoesArray[$dataSaida])) {
                    $programacoesArray[$dataSaida]['vagas'] += $programacao->vagas;
                    $programacoesArray[$dataSaida]['estoque'] += $programacao->estoque;
                } else {
                    $programacoesArray[$dataSaida] = [
                        'vagas' => $programacao->vagas,
                        'estoque' => $programacao->estoque
                    ];
                }
            }
        }

        if (!empty($programacoes)) {
            foreach ($programacoes as $programacao) {

                $product = $this->site->getProductByID($programacao->produto);
                $dia = date('d', strtotime($programacao->dataSaida));
                $dataSaida = date('m-d', strtotime($programacao->dataSaida));

                $totalVagas = $programacoesArray[$dataSaida]['vagas'];
                $totalEstoque = $programacoesArray[$dataSaida]['estoque'];
                $restam = $totalVagas - $totalEstoque;

                if ($product->cat_precificacao == 'preco_por_data') {
                    $preco_item = $programacao->preco;

                    $daily_programacoes[$dia] = "
                    <table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'>
                        <tr>
                            <td>" . lang("estoque") . "</td>
                            <td>" . $totalVagas . "</td>
                        </tr>
                        <tr>
                            <td>" . lang("vendidos") . "</td>
                            <td>" . $restam . "</td>
                        </tr>
                         <tr>
                            <td style='font-weight: 700;'>" . lang("restam") . "</td>
                            <td style='font-weight: 700;'>" . $totalEstoque. "</td>
                        </tr>
                        <tr>
                            <td style='font-weight: 700;'>" . lang("Preço") . "</td>
                            <td style='font-weight: 700;'>" . $this->sma->formatMoney($preco_item). "</td>
                        </tr>
                    </table>";

                } else {
                    $daily_programacoes[$dia] = "
                    <table class='table table-bordered table-hover table-striped table-condensed data' style='margin:0;'>
                        <tr>
                            <td>" . lang("estoque") . "</td>
                            <td>" . $totalVagas . "</td>
                        </tr>
                        <tr>
                            <td>" . lang("vendidos") . "</td>
                            <td>" . $restam . "</td>
                        </tr>
                         <tr>
                            <td style='font-weight: 700;'>" . lang("restam") . "</td>
                            <td style='font-weight: 700;'>" . $totalEstoque. "</td>
                        </tr>
                    </table>";
                }

            }
        } else {
            $daily_programacoes = array();
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_programacoes);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['product'] = $this->products_model->getProductByID($productID);;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('availability')));
        $meta = array('page_title' => lang('availability'), 'bc' => $bc);

        //$this->page_construct('agenda/availability', $meta, $this->data);

        $this->load->view($this->theme . 'agenda/availability', $this->data);

    }


    public function search_tours($productID = null) {

        $filter = new ProgramacaoFilter_DTO_model();

        if ($productID) {
            $filter->produto = $productID;
        }

        $filter->group_data_website = false;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

        $output_arrays = array();

        foreach ($programacoes as $programacao) {
            $cor = $this->CorAgendaRepository_model->getByID($programacao->color_agenda_id);

            if ($this->Settings->open_excel_disponibilidade) {
                $output_arrays[] = array(
                    'id' => $programacao->id,
                    'title' => $programacao->name.' | Tínhamos '.$programacao->vagas.' vagas disponíveis, mas já vendemos '. ($programacao->vagas - $programacao->estoque) .'. Restam agora ' . $programacao->estoque . ' vagas',
                    //'title' => $programacao->name.' | '.$programacao->vagas.'/'.$programacao->estoque.' Vagas ',

                    'start' => $programacao->dataSaida.'T'.$programacao->horaSaida,
                    'end' => $programacao->dataRetorno.'T'.$programacao->horaRetorno,

                    'url' => site_url('sales/relatorio_geral_ferroviario_excel/'.$programacao->produtoId.'/'.$programacao->id),
                    //'url' => $this->Settings->url_site_domain.'/carrinho_compra/'.$programacao->id.'/'.$user->biller_id,

                    'backgroundColor' => $cor->cor,
                    'borderColor' => $cor->cor,
                    //'overlap' => true,
                    //'display' => 'background',
                );
            } else {
                $output_arrays[] = array(
                    'id' => $programacao->id,

                    'title' => $programacao->name.' | Tínhamos '.$programacao->vagas.' vagas disponíveis, mas já vendemos '. ($programacao->vagas - $programacao->estoque) .'. Restam agora ' . $programacao->estoque . ' vagas',
                    //'title' => $programacao->name.' | '.$programacao->vagas.'/'.$programacao->estoque.' Vagas',

                    'start' => $programacao->dataSaida.'T'.$programacao->horaSaida,
                    'end' => $programacao->dataRetorno.'T'.$programacao->horaRetorno,

                    'url' => site_url('salesutil/sale_items/'.$programacao->id),
                    //'url' => $this->Settings->url_site_domain.'/carrinho_compra/'.$programacao->id.'/'.$user->biller_id,

                    'backgroundColor' => $cor->cor,
                    'borderColor' => $cor->cor,
                    //'overlap' => true,
                    //'display' => 'background',
                );
            }

        }

        $this->sma->send_json($output_arrays);
    }

    function getProgramacoes()
    {
        $user =  $this->db_model->getUserById($this->session->userdata('user_id'));
        $biller_id = $user->biller_id;

        if (!$user->biller_id) {
            $biller_id =  $this->Settings->default_biller;
        }

        $this->load->library('datatables');

        $link_pacote = '<a href="' . $this->Settings->url_site_domain.'/carrinho/$1/'.$biller_id. '" target="_blank"><i class="fa fa-usd"></i> '
            . lang('abrir_link_pacote') . '</a>';

        $link_loja = '<a href="' . $this->Settings->url_site_domain.'/'.$biller_id. '" target="_blank"><i class="fa fa-shopping-cart"></i> '
            . lang('abrir_link_loja') . '</a>';

        $link_qr_code = '<a href="' . site_url('agenda/qr_code/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-qrcode"></i> ' . lang('abrir_link_pacote') . '</a>';

        if ($this->Settings->avaliar) {
            $link_avaliacao = '<li class="divider"></li><li><a href="' . site_url('agenda/link_avaliacao/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-star"></i> ' . lang('abrir_link_avaliacao') . '</a></li>';
        }

        $link_atribuir_cupom = '<a href="' . site_url('agenda/atribuir_cupom/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-ticket"></i> ' . lang('atribuir_cupom') . '</a>';
        $link_add_cupom = '<a href="' . site_url('products/adicionarCupom/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> ' . lang('adicionar_cupom') . '</a>';

        $link_configurar_assento = '<a href="' . site_url('agenda/habilitar_marcacao_assento/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-check-circle"></i> ' . lang('habilitar_marcacao_assento') . '</a>';

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
		    <li>' . $link_qr_code . '</li>
		    <li class="divider"></li>
			<li><a href="' . site_url('products/edit/$2') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>
			<li><a href="' . site_url('agenda/editByProduto/$1') . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> ' . lang('editar_data') . '</a></li>
		    ' . $link_avaliacao . '
		    <li class="divider"></li>
  		    <li>' . $link_add_cupom . '</li>
  		    <li>' . $link_atribuir_cupom . '</li>
		    <li class="divider"></li>
  		    <li>' . $link_configurar_assento . '</li>
			<li class="divider"></li>';

        $action .= '<li><a href="' . site_url('salesutil/sale_items/$1') . '" target="_blank"><i class="fa fa-bar-chart"></i> ' . lang('relatorios_servico') . '</a></li>';

        $action .=	'</ul>
		</div></div>';

        $this->datatables
                ->select(
                    $this->db->dbprefix('agenda_viagem') . ".id as agendaid,".
                    $this->db->dbprefix('products') . ".id as productid,".
                    $this->db->dbprefix('products') . ".image as image,".
                    $this->db->dbprefix('products') . ".active,".
                    $this->db->dbprefix('categories') . ".name as category,".
                    $this->db->dbprefix('products') . ".name,".
                    "concat(".$this->db->dbprefix('agenda_viagem') . ".dataSaida, ' ' , sma_agenda_viagem.horaSaida) as dataSaida,".
                    $this->db->dbprefix('agenda_viagem') . ".vagas,".
                    "(select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status = 'orcamento' and sma_sale_items.adicional = 0  ) as orcamento,".
                    "(select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status = 'faturada' and sma_sale_items.adicional = 0 ) as vendas,".
                    "(select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status = 'faturada' and sma_sale_items.adicional = 0 and sma_sale_items.descontarVaga = 0) as colo,".
                    "(sma_agenda_viagem.vagas -  (select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.descontarVaga = 1 and sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status in ('faturada', 'orcamento') and sma_sale_items.adicional = 0  )) as disponivel,".

                    "(select sum(sma_sales.grand_total) as grand_total from sma_sales where sma_sales.id in ( select sma_sale_items.sale_id from sma_sale_items where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id) and sma_sales.sale_status in ('orcamento','faturada')) as grand_total,".
                    "(select sum(sma_sales.paid) as paid from sma_sales where sma_sales.id in ( select sma_sale_items.sale_id from sma_sale_items where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id) and sma_sales.sale_status in ('orcamento','faturada')) as paid,".
                    "(select sum(sma_sales.grand_total) as grand_total from sma_sales where sma_sales.id in ( select sma_sale_items.sale_id from sma_sale_items where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id) and sma_sales.sale_status in ('orcamento','faturada')) -
                     (select sum(sma_sales.paid) as paid from sma_sales where sma_sales.id in ( select sma_sale_items.sale_id from sma_sale_items where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id) and sma_sales.sale_status in ('orcamento','faturada')) as balance"

                    , FALSE)
                ->from('agenda_viagem')
                ->join('products', 'agenda_viagem.produto=products.id', 'left')
                ->join('categories', 'products.category_id=categories.id', 'left');

        $ano = $this->input->post('filter_ano') != null ? $this->input->post('filter_ano')  : date('Y');
        $mes = $this->input->post('filter_mes') != null ? $this->input->post('filter_mes') : 'PROXIMAS';

        $filter_status = $this->input->post('filter_status');
        $filter_produto = $this->input->post('filter_produto');

        $filter_start_date = $this->input->post('filter_start_date');
        $filter_end_date = $this->input->post('filter_end_date');

        if ($ano) $this->db->where("YEAR(dataSaida)", $ano);

        if ($mes == 'PROXIMAS') {
            $this->db->where("dataSaida>='".date('Y-m-d')."'");
        } else if ($mes != 'all') {
            $this->db->where("MONTH(dataSaida)",$mes);
        }

        if ($filter_start_date) {
            $this->db->where("dataSaida>='".$filter_start_date."'");
        }

        if ($filter_end_date) {
            $this->db->where("dataSaida<='".$filter_end_date."'");
        }

        if ($filter_status) $this->db->where("products.unit", $filter_status);
        if ($filter_produto) $this->db->where("products.id", $filter_produto);

        $this->db->where('viagem', 1);

        $this->db->order_by('dataSaida asc');

        $this->datatables->add_column("Actions", $action, "agendaid, productid, image, code, name");
        echo $this->datatables->generate();
    }

    function modal_view($programacaoId = NULL, $productId = NULL)
    {

        $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $vendaFilter = new VendaFilterDTO_model();
        $vendaFilter->setProgramacaoId($programacaoId);

        $this->data['product']          = $this->products_model->getProductByID($productId);
        $this->data['sales']            = $this->reports_model->getSalesProgramacao($vendaFilter);
        $this->data['programacao']      = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $this->data['tiposQuarto']      = $this->TipoQuartoRepository_model->getTiposQuarto();
        $this->data['valorFaixas']      = $this->ValorFaixaRepository_model->getValoreFaixas();

        $this->load->view($this->theme.'agenda/modal_view', $this->data);
    }

    function link_avaliacao($programacaoId)
    {
        $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $programacaoEstoque = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $product            = $this->site->getProductByID($programacaoEstoque->produto);

        $this->data['programacao']  = $programacaoEstoque;
        $this->data['product']      = $product;
        $this->data['url']          = $this->Settings->url_site_domain.'/rate/'.$programacaoId;;

        $this->load->view($this->theme.'agenda/link_avaliacao', $this->data);
    }

    function qr_code($programacaoId)
    {
        $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $user       =  $this->db_model->getUserById($this->session->userdata('user_id'));
        $biller_id  = $user->biller_id;

        $programacaoEstoque = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $product            = $this->site->getProductByID($programacaoEstoque->produto);

        if (!$user->biller_id) {
            $biller_id =  $this->Settings->default_biller;
        }

        $description    = $this->sma->slug($product->name).'-uid'.$programacaoEstoque->id;
        $url            = $this->Settings->url_site_domain.'/pacote/'. $description .'/'.$biller_id;

        $vendaFilter = new VendaFilterDTO_model();
        $vendaFilter->setProgramacaoId($programacaoId);

        $this->data['programacao']  = $programacaoEstoque;
        $this->data['product']      = $product;
        $this->data['url']          = $url;

        $this->load->view($this->theme.'agenda/qr_code', $this->data);
    }

    /* ------------------------------------------------------- */
    function abrir($productID = NULL)
    {

        if ($productID == null) {
            $this->data['produtos'] = $this->products_model->getAllProducts('Confirmado');
        }
        else {
            $this->data['produtos'] = $this->products_model->getAllProducts();
        }

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->produto = $productID;
        $filter->enviar_site = FALSE;
        $filter->apenas_agendas_ativa = FALSE;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

        $this->data['produto']          = $this->products_model->getProductByID($productID);
        $this->data['transportes']      = $this->ProdutoRepository_model->getTransportesRodoviario($productID);
        $this->data['datasAgendadas']   = $programacoes;

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('products'), 'page' => lang('agenda')),
            array('link' => '#', 'page' => lang('agenda'))
        );

        $meta = array('page_title' => lang('add_product'), 'bc' => $bc);
        $this->page_construct('agenda/abrir', $meta, $this->data);
    }

    function abrir_modal()
    {
        $this->data['produtos'] = $this->products_model->getAllProducts('Confirmado');
        $this->load->view($this->theme . 'agenda/abrir_modal', $this->data);
    }

    public function abrir_modal_programacao($productID = NULL) {

        if ($productID == null) {
            $this->data['produtos'] = $this->products_model->getAllProducts('Confirmado');
        }
        else {
            $this->data['produtos'] = $this->products_model->getAllProducts();
        }

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->produto = $productID;
        $filter->enviar_site = FALSE;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

        $this->data['produto']          = $this->products_model->getProductByID($productID);
        $this->data['transportes']      = $this->ProdutoRepository_model->getTransportesRodoviario($productID);
        $this->data['datasAgendadas']   = $programacoes;

        $this->load->view($this->theme . 'agenda/abrir_modal_programacao', $this->data);
    }


    public function agendar() {

        $produto = $this->input->post('produto');

        try {

            $k 	= isset($_POST['doDia']) ? sizeof($_POST['doDia']) : 0;

            for ($j = 0; $j < $k; $j++) {
                $agendaDTO = new AgendaViagemDTO_model();

                $vagas = $_POST['vagas'][$j];
                $doDia = $_POST['doDia'][$j];
                $aoDia = $_POST['aoDia'][$j];
                $horaSaida = $_POST['horaSaida'][$j];
                $horaRetorno = $_POST['horaRetorno'][$j];

                if ($vagas == '' || $doDia == '') continue;

                $agendaDTO->produto = $produto;
                $agendaDTO->vagas = $vagas;
                $agendaDTO->doDia = $doDia;
                $agendaDTO->aoDia = $aoDia;
                $agendaDTO->horaSaida = $horaSaida;
                $agendaDTO->horaRetorno = $horaRetorno;

                $this->AgendaViagemService_model->salvar($agendaDTO);

                $this->output->delete_all_cache();//TODO LIMPAR CACHE

            }

            $this->session->set_flashdata('message', lang("agendamento_adicionado_com_sucesso"));
        } catch (Exception $exception) {
            echo $exception;
            $this->session->set_flashdata('error', $exception->getMessage());
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    function edit($id = NULL)
    {
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->form_validation->set_rules('doDia', lang("data_do_dia"), 'required');
        $this->form_validation->set_rules('vagas', lang("vagas"), 'required');

        if ($this->form_validation->run('agenda/edit') == true) {

            $agendaDTO = new AgendaViagemDTO_model();

            $agendaDTO->produto = $this->input->post('produto');
            $agendaDTO->vagas = $this->input->post('vagas');
            $agendaDTO->doDia = $this->input->post('doDia');
            $agendaDTO->aoDia = $this->input->post('aoDia');
            $agendaDTO->horaSaida = $this->input->post('horaSaida');
            $agendaDTO->horaRetorno = $this->input->post('horaRetorno');
            $agendaDTO->active = $this->input->post('active');

            $agendaDTO = $this->adicionarCupomDesconto($agendaDTO);
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->AgendaViagemService_model->editar($agendaDTO, $id);

                $this->output->delete_all_cache();//TODO LIMPAR CACHE

                $this->session->set_flashdata('message', lang("agendamento_editado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['agenda'] = $this->AgendaViagemRespository_model->getProgramacaoById($id);
            $this->load->view($this->theme . 'agenda/edit', $this->data);
        }
    }

    function atribuir_cupom($id = NULL)
    {
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->form_validation->set_rules('produto', lang("produto"), 'required');

        if ($this->form_validation->run() == true) {

            $agendaDTO = new AgendaViagemDTO_model();
            $agendaDTO->produto = $this->input->post('produto');

            $agendaDTO = $this->adicionarCupomDesconto($agendaDTO);
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->AgendaViagemService_model->editar_cupom_desconto($agendaDTO, $id);

                $this->session->set_flashdata('message', lang("cupom_editado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $agenda = $this->AgendaViagemRespository_model->getProgramacaoById($id);

            $this->data['error']    = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['cupons']   = $this->settings_model->getAllCupons();
            $this->data['agenda']   =  $agenda;
            $this->data['product']  = $this->site->getProductByID($agenda->produto);
            $this->load->view($this->theme . 'agenda/atribuir_cupom', $this->data);
        }
    }

    function habilitar_marcacao_assento($id = NULL)
    {
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->form_validation->set_rules('doDia', lang("data_do_dia"), 'required');
        $this->form_validation->set_rules('vagas', lang("vagas"), 'required');

        if ($this->form_validation->run() == true) {

            $agendaDTO = new AgendaViagemDTO_model();

            $agendaDTO->produto = $this->input->post('produto');
            $agendaDTO->vagas = $this->input->post('vagas');
            $agendaDTO->doDia = $this->input->post('doDia');
            $agendaDTO->aoDia = $this->input->post('aoDia');
            $agendaDTO->horaSaida = $this->input->post('horaSaida');
            $agendaDTO->horaRetorno = $this->input->post('horaRetorno');
            $agendaDTO->habilitar_marcacao_area_cliente = $this->input->post('habilitar_marcacao_area_cliente');
            $agendaDTO->permitir_marcacao_dependente = $this->input->post('permitir_marcacao_dependente');
            $agendaDTO->data_inicio_marcacao = $this->input->post('data_inicio_marcacao');
            $agendaDTO->data_final_marcacao = $this->input->post('data_final_marcacao');
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->AgendaViagemService_model->editar($agendaDTO, $id);

                $this->session->set_flashdata('message', lang("agendamento_editado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $agenda = $this->AgendaViagemRespository_model->getProgramacaoById($id);
            $transportes =  $this->ProdutoRepository_model->getTransportesRodoviario($agenda->produto);
            $exibir_marcacao_assento = false;

            if (!empty($transportes)) {
                foreach ($transportes as $tipoTransporte) {
                    $ativo = $tipoTransporte->status == 'ATIVO' ? true : false;
                    if ($ativo && $tipoTransporte->automovel_id) {
                        $exibir_marcacao_assento = true;
                    }
                }
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['cupons'] = $this->settings_model->getAllCupons();
            $this->data['agenda'] =  $agenda;
            $this->data['product']  = $this->site->getProductByID($agenda->produto);
            $this->data['exibir_marcacao_assento'] = $exibir_marcacao_assento;

            $this->load->view($this->theme . 'agenda/habilitar_marcacao_assento', $this->data);
        }
    }

    function editByProduto($id = NULL)
    {
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->form_validation->set_rules('doDia', lang("data_do_dia"), 'required');
        $this->form_validation->set_rules('vagas', lang("vagas"), 'required');

        if ($this->form_validation->run('agenda/edit') == true) {

            $agendaDTO = new AgendaViagemDTO_model();

            $agendaDTO->produto = $this->input->post('produto');
            $agendaDTO->vagas = $this->input->post('vagas');
            $agendaDTO->doDia = $this->input->post('doDia');
            $agendaDTO->aoDia = $this->input->post('aoDia');
            $agendaDTO->horaSaida = $this->input->post('horaSaida');
            $agendaDTO->horaRetorno = $this->input->post('horaRetorno');
            $agendaDTO->active = $this->input->post('active');
            $agendaDTO->preco = $this->input->post('preco');

            $agendaDTO->habilitar_marcacao_area_cliente = $this->input->post('habilitar_marcacao_area_cliente');
            $agendaDTO->permitir_marcacao_dependente = $this->input->post('permitir_marcacao_dependente');
            $agendaDTO->data_inicio_marcacao = $this->input->post('data_inicio_marcacao');
            $agendaDTO->data_final_marcacao = $this->input->post('data_final_marcacao');
        }

        if ($this->form_validation->run() == true) {
            try {
                $this->AgendaViagemService_model->editar($agendaDTO, $id);

                $this->session->set_flashdata('message', lang("agendamento_editado_com_sucesso"));
            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $agenda = $this->AgendaViagemRespository_model->getProgramacaoById($id);
            $transportes =  $this->ProdutoRepository_model->getTransportesRodoviario($agenda->produto);
            $exibir_marcacao_assento = false;

            if (!empty($transportes)) {
                foreach ($transportes as $tipoTransporte) {
                    $ativo = $tipoTransporte->status == 'ATIVO' ? true : false;
                    if ($ativo && $tipoTransporte->automovel_id) {
                        $exibir_marcacao_assento = true;
                    }
                }
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();

            $this->data['cupons'] = $this->settings_model->getAllCupons();
            $this->data['agenda'] =  $agenda;
            $this->data['exibir_marcacao_assento'] = $exibir_marcacao_assento;
            $this->data['product'] = $this->site->getProductByID($agenda->produto);
            $this->load->view($this->theme . 'agenda/editByProduto', $this->data);
        }
    }

    private function adicionarCupomDesconto($agendaDTO) {
        $a = sizeof($_POST ['cupomId']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['cupomId'] [$r] )) {

                $cupom = new CupomDesconto_DTO_model();

                $cupom->setCupom($_POST ['cupomId'] [$r]);
                $cupom->setQuantidadeDisponibilizada( $_POST ['quantidadeCupom'] [$r]);
                $cupom->setDataInicio($_POST ['dataInicio_cupom'] [$r]);
                $cupom->setDataFinal($_POST ['dataFinal_cupom'] [$r]);
                $cupom->setStatus($this->adicionarCupomDescontoAtivo($_POST ['cupomId'] [$r]));

                $agendaDTO->adicionarCupom($cupom);
            }
        }
        return $agendaDTO;
    }

    private function adicionarCupomDescontoAtivo($cupomId) {
        $a = sizeof($_POST ['cupomId']);
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $cupomIdAtivo = $_POST ['ativarCupom'] [$r];

            if ($cupomIdAtivo == $cupomId) {
                $ativo = 'ATIVO';
            }
        }

        return $ativo;
    }

    public function excluir($agendamentoId, $produto) {
        try {
            $this->AgendaViagemService_model->excluir($agendamentoId);

            $this->session->set_flashdata('message', lang("agendamento_excluido_com_sucesso"));

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
        }

        $this->abrir($produto);
    }

    public function view_lancamento_agenda_data() {
        $this->load->view($this->theme . 'agenda/view_lancamento_agenda_data', $this->data);
    }

    public function buscarProdutos() {
        $status = $this->input->get('status');
        $this->sma->send_json($this->products_model->getAllProducts($status));
    }

    function relatorio_cupom($programacao_id, $cupom_id)
    {
        $itens_pedidos = $this->CupomDescontoRepository_model->findAllPassageirosByCupom($programacao_id, $cupom_id);

        if (!$programacao_id || !$itens_pedidos || !$cupom_id) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $cupom = $this->CupomDescontoRepository_model->getById($cupom_id);
        $this->data['cupom'] = $cupom;
        $this->data['itens'] = $itens_pedidos;
        $name = 'RELATÓRIO DE CUPONS USADOS' . strtoupper($cupom->name) . '.pdf';

        $html = $this->load->view($this->theme . 'agenda/relatorio_cupom', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

}?>