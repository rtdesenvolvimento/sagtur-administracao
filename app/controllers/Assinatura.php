<?php defined('BASEPATH') or exit('No direct script access allowed');

require_once 'vendor/autoload.php';

use vinicinbgs\Autentique\Documents;
use vinicinbgs\Autentique\Folders;


class Assinatura extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function listById($documentId) {
        $AUTENTIQUE_TOKEN="230bbf135fc9a54f21bca5889f4c08eb948c7807508158efb54b538bf987f1f3";

        $documents = new Documents($AUTENTIQUE_TOKEN);

        $document = $documents->listById($documentId);

        print_r($document);

    }

    public function listAllDocuments() {

        $AUTENTIQUE_TOKEN="c4ddd0123abd38ee75ff8df9794e0412a764d041c6cfecee25e612ee90121524";

        $documents = new Documents($AUTENTIQUE_TOKEN);

        $documentsPaginated = $documents->listAll(1); // if not isset $page is equal 1

        print_r($documentsPaginated);

    }

    public function createDocument() {

        $AUTENTIQUE_TOKEN="c4ddd0123abd38ee75ff8df9794e0412a764d041c6cfecee25e612ee90121524";

        $documents = new Documents($AUTENTIQUE_TOKEN);

        $attributes = [
            'document' => [
                'name' => 'andrevelho-novoteste dfd ddf a.txt',
                'message' => 'aqui vai a mensagem',
                "reminder" => "DAILY",
            ],
            'signers' => [
                [
                    'email' => 'resultatec@gmail.com',
                    'name' => 'Diego Fernando Hoffmann',
                    'action' => 'SIGN',
                    'positions' => [
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '80', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '1', // Página da ASSINATURA
                        ],
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '50', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '2', // Página da ASSINATURA
                        ],
                    ],
                ],
                [
                    //'email' => 'andre@resultatec.com.br',
                    'name' => 'Neuci Reckziegel',
                    'action' => 'SIGN',
                    'positions' => [
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '80', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '1', // Página da ASSINATURA
                        ],
                        [
                            'x' => '50', // Posição do Eixo X da ASSINATURA (0 a 100)
                            'y' => '50', // Posição do Eixo Y da ASSINATURA (0 a 100)
                            'z' => '2', // Página da ASSINATURA
                        ],
                    ],
                ],
            ],
            'file' => 'E:\\Curriculo_JAQUELINE_ALMEIDA_VELHO_2022.pdf',
        ];

        $documentCreated = $documents->create($attributes);

        print_r($documentCreated);

    }

    public function signed($documentId) {

        $AUTENTIQUE_TOKEN="c4ddd0123abd38ee75ff8df9794e0412a764d041c6cfecee25e612ee90121524";

        $documents = new Documents($AUTENTIQUE_TOKEN);

        $documentSign = $documents->signById($documentId);

        print_r($documentSign);
    }

    public function create_folder() {
        $AUTENTIQUE_TOKEN="230bbf135fc9a54f21bca5889f4c08eb948c7807508158efb54b538bf987f1f3";

        $attributes = [
            "folder" => [
                "name" => "Documentos Arquivados",
            ],
        ];

        $folder = new Folders($AUTENTIQUE_TOKEN);

        $folder_created = $folder->create($attributes);

        print_r($folder_created);
    }
}
