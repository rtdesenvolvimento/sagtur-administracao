<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

       // $this->Settings = $this->site->get_setting();
        // $this->data['Settings'] = $this->Settings;

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/ValorFaixaRepository_model', 'ValorFaixaRepository_model');

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
    }

    public function index() {
        $this->open($this->Settings->default_biller);
    }

    public function open($vendedor) {

        try {

            if (!$vendedor) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $filter = new ProgramacaoFilter_DTO_model();

            $mes = $this->input->get('mes') == null ? date('m') : ( $this->input->get('mes') != 'all' ? $this->input->get('mes') : null);
            $produto = $this->input->get('destino');

            $filter->produto = $produto;
            $filter->mes = $mes;

            $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

            $this->data['programacoes'] =  $programacoes;

            if ($filter->produto) {
                $filter = new ProgramacaoFilter_DTO_model();
                $filter->mes = $mes;
                $this->data['programacoesFilter'] = $this->AgendaViagemService_model->getAllProgramacao($filter);
            } else {
                $this->data['programacoesFilter'] = $programacoes;
            }

            $this->data['vendedor'] = $this->site->getCompanyByID($vendedor);
            $this->data['destinoFilter'] = $produto;
            $this->data['mesFilter'] = $mes;

            $this->data['tiposQuarto']      = $this->TipoQuartoRepository_model->getTiposQuarto();
            $this->data['valorFaixas']      = $this->ValorFaixaRepository_model->getValoreFaixas();

            $this->load->view($this->theme . 'appcompra/estoque', $this->data);

        } catch (Exception $exception) {
            show_error($exception->getMessage(), 404, $exception->getMessage());
        }
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

}