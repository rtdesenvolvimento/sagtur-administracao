<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Linkcompra extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->lang->load('sales', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->model('sales_model');
        $this->load->model('products_model');
        $this->load->model('companies_model');
    }

    public function index(){
        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['vendedores']   = $this->site->getAllCompanies('biller');
        $this->load->view($this->theme . 'link_compra/link_compra', $this->data);
    }

    public function pdf($id) {

        $inv = $this->sales_model->getInvoiceByID($id);
        $warehouses = $this->products_model->getWarehouseByID($inv->warehouse_id);
        $products = $this->products_model->getProductByCode($warehouses->code);

        $this->data['inv']              = $inv;
        $this->data['product_detalhes'] = $products;
        $this->data['customer']         = $this->companies_model->getCompanyByID($inv->customer_id);
        $this->data['products']         = $this->products_model->getAllProducts();
        $this->data['vendedores']       = $this->site->getAllCompanies('biller');
        $this->load->view($this->theme . 'link_compra/link_compra_pdf', $this->data);
    }

    public function reserva($produtoId=null,$vendedorId){

        $this->data['produtoId']     = $produtoId;
        $this->data['vendedorId']     = $vendedorId;

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['vendedores']   = $this->site->getAllCompanies('biller');
        $this->load->view($this->theme . 'link_compra/link_compra', $this->data);
    }

    public function reserva_vendedor($vendedorId) {
        $this->data['vendedorId']     = $vendedorId;

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['vendedores']   = $this->site->getAllCompanies('biller');
        $this->load->view($this->theme . 'link_compra/link_compra', $this->data);
    }

    function getVerificaCustomeByCPF($cpf){
        $this->sma->send_json($this->companies_model->getVerificaCustomeByCPF($cpf));
    }

    function link_compra_finalizacao($vendedor) {
        $this->data['vendedor'] = $vendedor;
        $this->load->view($this->theme . 'link_compra/link_compra_finalizacao', $this->data);
    }

    function buscarLocalEmbarque($id) {
        $origem = $this->products_model->getProductByID($id)->origem;
        $origem = strip_tags($origem);
        $this->sma->send_json($origem);
    }

    function pacotes($vendedor) {
        $this->data['vendedor'] = $vendedor;
        $this->data['servicos'] = $this->products_model->getAllProductsConfirmedLink();
        $this->load->view($this->theme . 'link_compra/pacotes', $this->data);
    }
}