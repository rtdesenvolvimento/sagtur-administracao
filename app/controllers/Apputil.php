<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpWord\IOFactory;
use Dompdf\Dompdf;

class Apputil extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('settings_model');

        $this->upload_path = 'assets/uploads/documents/'.$this->session->userdata('cnpjempresa');
    }

    public function condicoes()
    {
        $produtoID              = $this->input->get('produtoID');
        $istaxasComissaoAtivo   = $this->input->get('istaxasComissaoAtivo');
        $tipoCobrancaId         = $this->input->get('tipoCobrancaId');
        $tipo                   = $this->input->get('tipo');
        $dataSaida              = $this->input->get('data_Saida');

        if ($istaxasComissaoAtivo) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, FALSE , $produtoID);
        } else {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId);
        }

        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento   = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento             = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        $dataMaximaParaRealizarPagamento = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida);

        $dtVencimento   = null;
        $maximoParcelas = 0;

        for ($i = 0; $i < 25; $i++) {
            $dtVencimento = $this->getDataProximoVencimentoCondicoes($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo);
            if ($dtVencimento != $dataMaximaParaRealizarPagamento && strtotime($dtVencimento) < strtotime($dataMaximaParaRealizarPagamento)) {
                $maximoParcelas++;
            } else {
                if (date('Y-m', strtotime($dtVencimento)) == date('Y-m', strtotime($dataMaximaParaRealizarPagamento))) {
                    $maximoParcelas++;
                }
            }
        }

        if ($maximoParcelas == 0) {
            $maximoParcelas = 1;
        }

        if ($tipo == 'carne_cartao' || $tipo == 'cartao' || $i == 0) {
            if ($istaxasComissaoAtivo) {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, NULL, $produtoID);
            } else {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId);
            }
        } else {
            if ($istaxasComissaoAtivo) {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcelas, $produtoID);
            } else {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcelas);
            }
        }

        $this->sma->send_json($condicoesPagamento);
    }

    private function getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida)
    {

        if ($diasMaximoPagamentoAntesViagem > 0) {
            $dtMaximaDePagamento = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        } else {
            $dtMaximaDePagamento = $dataSaida;//data da saida da viagem
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtMaximaDePagamento)) {
            $dtMaximaDePagamento = date('Y-m-d');
        }

        return $dtMaximaDePagamento;
    }

    public function getDataProximoVencimentoCondicoes($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo)
    {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+" . $diasAvancaPrimeiroVencimento . " day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento


            if ($tipo != 'carne') {
                //se nao for carne os proximos vencimentos volta para o dia da compra
                $dtProximoVencimento = date('Y-m-d', strtotime("-" . $diasAvancaPrimeiroVencimento . " day", strtotime($dtProximoVencimento)));//tira obriacao do dia minimo
            }

        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }
        return $dtProximoVencimento;
    }

    public function getParcelamento()
    {

        $produtoID              = $this->input->get('produtoID');
        $condicaoPagamentoId    = $this->input->get('condicaoPagamentoId');
        $numero_parcelas        = $this->input->get('numero_parcelas');
        $tipoCobrancaId         = $this->input->get('tipoCobrancaId');
        $subTotal               = $this->input->get('subTotal');
        $tipo                   = $this->input->get('tipo');
        $conta                  = $this->input->get('conta');
        $dataSaida              = $this->input->get('data_Saida');
        $istaxasComissaoAtivo   = $this->input->get('istaxasComissaoAtivo');

        //sinal
        $valorSinal             = $this->input->get('valorSinal');
        $tipoCobrancaSinal      = $this->input->get('tipoCobrancaSinal');
        $contaMovimentadorSinal = $this->input->get('contaMovimentadorSinal');
        $nome_cobranca_sinal    = $this->input->get('nome_cobranca_sinal');

        $subTotal               = $subTotal - $valorSinal;

        $totalParcelas          = $numero_parcelas != null ? $numero_parcelas : 1;

        if ($istaxasComissaoAtivo) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId, $produtoID);
        } else {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId);
        }

        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento   = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento             = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        $dataMaximaParaRealizarPagamento = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida);

        $memorizaPrimeiroVencimento = null;
        $dtVencimento   = null;
        $contadoParcela = 1;
        $html           = '';

        if ($valorSinal > 0) {

            $dtVencimentoSinal = date('Y-m-d');

            $memorizaPrimeiroVencimento = $dtVencimentoSinal;

            $html .= '
               <div class="row div_parcelas" style="border-bottom: 1px solid #dcdcdc;margin-bottom: 25px;">
                    <div class="col-1" style="line-height: 35px;"></div>
                    <div class="col-7" style="line-height: 20px;text-align: right;margin-bottom: 15px;">Sinal no ' . $nome_cobranca_sinal.':</div>
                    <div class="col-4" style="line-height: 20px;text-align: right;">'. $this->sma->formatMoney($valorSinal) .'</div>
                    <div class="col-7" style="display: none;">
                        <input type="date" 
                                original="' . $dtVencimentoSinal . '" 
                                diasAntesDaViagem="' . $diasMaximoPagamentoAntesViagem . '" 
                                dataMaximaPagamento="' . $dataMaximaParaRealizarPagamento . '" 
                                value="' . $dtVencimentoSinal . '" 
                                name="dtVencimentos[]" 
                                readonly="readonly" 
                                required="required">
                    </div>
                    <div class="col-4" style="line-height: 35px;">
                        <input type="hidden" value="' . $valorSinal . '" name="valorVencimentos[]">
                        <input type="hidden" value="' . $tipoCobrancaSinal . '"         name="tiposCobranca[]">
                        <input type="hidden" value="' . $contaMovimentadorSinal . '"    name="movimentadores[]">
                        <input type="hidden" value="0"                                  name="descontos[]">
                    </div>
              </div>';
        }

        for ($i = 0; $i < $totalParcelas; $i++) {

            $readonly = '';

            $dtVencimento = $this->getDataProximoVencimento($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo);

            if ($i == 0 && $valorSinal == 0) {
                $memorizaPrimeiroVencimento = $dtVencimento;
                $readonly = 'readonly="readonly"';
            }

            if ($tipo == 'carne') {
                $readonly = 'readonly="readonly"';
            }

            $html .= '
               <div class="row div_parcelas">
                    <div class="col-1" style="line-height: 35px;">' . $contadoParcela . 'X</div>
                    <div class="col-7" style="text-align: center;">
                        <input type="date" 
                            style="text-align: center;width: 100%;"
                            original="' . $dtVencimento . '" 
                            diasAntesDaViagem="' . $diasMaximoPagamentoAntesViagem . '" 
                            dataMaximaPagamento="' . $dataMaximaParaRealizarPagamento . '" 
                            value="' . $dtVencimento . '"
                            name="dtVencimentos[]" ' . $readonly . ' 
                            required="required">
                    </div>
                    <div class="col-4" style="line-height: 35px;text-align: right;">
                        <input type="hidden" value="' . $subTotal / $totalParcelas . '" name="valorVencimentos[]">
                        <input type="hidden" value="' . $tipoCobrancaId . '" name="tiposCobranca[]">
                        <input type="hidden" value="' . $conta . '" name="movimentadores[]">
                        <input type="hidden" value="0" name="descontos[]">
                        ' . $this->sma->formatMoney($subTotal / $totalParcelas) . '
                    </div>
              </div>';

            $contadoParcela++;
        }

        $html = $this->getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem, $dataSaida, $totalParcelas);

        echo $html;
    }

    public function getDataProximoVencimento($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo)
    {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+" . $diasAvancaPrimeiroVencimento . " day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {

            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento
            $dtProximoVencimento = date('Y-m-d', strtotime("+".$diasAvancaPrimeiroVencimento." days", strtotime($dtProximoVencimento)));

            if ($tipo != 'carne') {
                //se nao for carne os proximos vencimentos volta para o dia da compra
                $dtProximoVencimento = date('Y-m-d', strtotime("-" . $diasAvancaPrimeiroVencimento . " day", strtotime($dtProximoVencimento)));//tira obriacao do dia minimo
            }

        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }

        //Se a data maxima para pagar ja passou entao atribuir a proxima data
        if (strtotime($dtMaximaDePagamento) < strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = $dtMaximaDePagamento;
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = date('Y-m-d');
        }

        return $dtProximoVencimento;
    }

    private function getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem, $dataSaida, $totalParcelas)
    {

        $html .= '<div class="row" style="margin-top: 25px;">';
        $html .= '<div class="col-12">';
        $html .= '<div class="cart_destaque">';

        if ($diasMaximoPagamentoAntesViagem > 0) {

            $dtMaximaDePagamento = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));

            if (strtotime(date('Y-m-d')) < strtotime($dtMaximaDePagamento)) {

                if ($totalParcelas > 1) {
                    $html .= '<b>*Leia-me:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                    $html .= '<b>*Leia-me: Apenas</b> após o pagamento da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';

                    $html .= '<br/><br/><b>*Leia-me: </b>No caso de parcelamento o pagamento total deverá ser <b>quitado até ' . $diasMaximoPagamentoAntesViagem . ' dias antes da viagem</b>. Data máxima para pagamento <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>';
                } else {
                    $html .= '<b>*Leia-me: Apenas</b> após o pagamento da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
                }
            } else {
                $html .= '<br/><br/><b>*Leia-me: </b>A viagem saíra ainda esta semana e a <b>quitação total do débito deverá ser feita ainda hoje para confirmação da compra.</b> Data máxima para pagamento <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>';
            }

        } else {
            if ($totalParcelas > 1) {
                $html .= '<b>*Leia-me:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                $html .= '<b>Apenas após o pagamento</b> da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
            } else {
                $html .= '<b>*Leia-me: Apenas</b> após o pagamento da parcela com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
            }
        }

        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<br/><br/><br/><br/>';

        return $html;
    }

    function getVerificaCustomeByCPF(){

        $cpf                    =  $this->input->get('cpf');
        $permiteDuplicidade     = $this->input->get('permite_duplicidade');
        $programacaoID          = $this->input->get('programacaoId');
        $encontrouDuplicidade   = FALSE;

        if (!$permiteDuplicidade) {
            $encontrouDuplicidade = $this->site->verificaClienteVenda($cpf, $programacaoID);
        }

        $pessoa = $this->site->getVerificaCPF($cpf);

        $this->sma->send_json(array('pessoa' => $pessoa, 'verifica_duplicidade' => $encontrouDuplicidade));
    }

    public function vagas()
    {
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $programacaoId               = $this->input->get('programacaoId');
        $tipo_hospedagem             = $this->input->get('tipo_hospedagem');
        $controle_estoque_hospedagem = $this->input->get('controle_estoque_hospedagem') == '1';

        echo $this->AgendaViagemService_model->vagas($programacaoId, $controle_estoque_hospedagem, $tipo_hospedagem);
    }

    public function simulation_installments_valepay() {

        $valePaySettings = $this->settings_model->getValepaySettings();

        if ($valePaySettings->active) {

            $valepay              = new Valepay();
            $valepay->sandbox     = $valePaySettings->sandbox;
            $valepay->key_public  = $valePaySettings->public_key;
            $valepay->key_private = $valePaySettings->private_key;

            $valor_total_taxas    = $this->input->get('valor_total_taxas');
            $tipo_cobranca        = $this->input->get('tipo_cobranca');
            $produtoID            = $this->input->get('produtoID');
            $istaxasComissaoAtivo = $this->input->get('istaxasComissaoAtivo');

            if ($istaxasComissaoAtivo) {
                $configurcaoTaxa      = $this->settings_model->getTaxaConfiguracaoSettings($tipo_cobranca, NULL, $produtoID);
            } else {
                $configurcaoTaxa      = $this->settings_model->getTaxaConfiguracaoSettings($tipo_cobranca);
            }

            $numeroMaxParcelas              = $configurcaoTaxa->numero_max_parcelas;
            $numero_max_parcelas_sem_juros  = $configurcaoTaxa->numero_max_parcelas_sem_juros;

            $emmiter = array(
                'amount'            => $valor_total_taxas,
                'markup_type'       => '1',//1 - Multiplo 2-Divisor
                'markup'            => '0',//Porcentagem do markup
                'calculate_markup'  => FALSE,
                'entry_money'       => 0,//valor da entrada
            );

            $rav = array(
                'amount'            => $valor_total_taxas,
                'entry_money'       => 0,//valor da entrada
            );

            $simulacoes = $valepay->simulation_installments_by_customer($emmiter);

            if ($this->validate_transaction_simulation_installments_valepay($simulacoes)) {

                if ($numero_max_parcelas_sem_juros > 0) {
                    $simulacoes_rav = $valepay->simulation_rav($rav);

                    $contador = 0;
                    foreach ($simulacoes_rav->data as $simulacao_rav) {
                        if ($simulacao_rav->parcela <= $numero_max_parcelas_sem_juros) {
                            $simulacoes->data[$contador] =  $simulacoes_rav->data[$contador];
                            $simulacoes->data[$contador]->sem_juros = true;
                        }
                        $contador++;
                    }
                }

                $contador = 0;
                foreach ($simulacoes->data as $simulacao) {
                    $parcela = $simulacao->parcela;
                    if ($parcela > $numeroMaxParcelas) {
                        unset($simulacoes->data[$contador]);
                    }

                    $contador++;
                }

                $this->sma->send_json($simulacoes);
            } else {
                $this->sma->send_json([]);
            }

        } else {
            $this->sma->send_json([]);
        }
    }

    private function validate_transaction_simulation_installments_valepay($transaction) {

        $response = new stdClass();

        $error = false;
        $status = false;

        if ($transaction->status == 'error') {
            if(!empty($transaction->message)) $error = $transaction->message;
        } else if ($transaction->status == 'success' && !empty($transaction->data)) {
            $status = true;
        }

        $response->status = $status;
        $response->error = $error;

        return $response;
    }

    public function createLead()
    {
        try {

            //service
            $this->load->model('service/CaptacaoService_model', 'CaptacaoService_model');

            //model
            $this->load->model('model/Captacao_model', 'Captacao_model');

            //repository
            $this->load->model('repository/CaptacaoRespository_model', 'CaptacaoRespository_model');
            $this->load->model('repository/DepartmentRepository_model', 'DepartmentRepository_model');

            $source         = $this->input->get('source');//vemdo chatwhatsapp
            $name           = $this->input->get('name');
            $phoneNumber    = $this->input->get('phoneNumber');
            $url            = $this->input->get('urlSite');
            $biller_id      = $this->input->get('biller_id');
            $programacao_id = $this->input->get('programacao_id');
            $product_id     = $this->input->get('product_id');
            $department_id  = $this->input->get('department_id');
            $message        = $this->input->get('message');

            $captacao   = new Captacao_model();

            $biller     = $this->site->getCompanyByID($biller_id);
            $user       = $this->site->getUserByBillerActive($biller_id);

            $captacao->reference_no = $this->site->getReference('cp');
            //TODO CRIAR O CONTATO CATPACAO NA LISTA

            //detalhes
            $captacao->date             = date('Y-m-d H:i:s');
            $captacao->programacao_id   = $programacao_id;
            $captacao->product_id       = $product_id;

            if ($biller) {
                $captacao->biller       = $biller->name;
                $captacao->biller_id    = $biller_id;
            }

            if ($user) {
                $created_by = $user->id;
            } else {
                $user       = $this->site->getUserByBillerActive($this->Settings->default_biller);
                $created_by = $user->id;
            }

            //configuraocao
            $captacao->plantao_id           = $this->Settings->plantao_default_id;
            $captacao->meio_divulgacao_id   = $this->Settings->meio_default_divulgacao_id;
            $captacao->forma_atendimento_id = $this->Settings->forma_default_atendimento_id;
            $captacao->interesse_id         = $this->Settings->interesse_default_id;
            $captacao->department_id        = $department_id;//setor

            //data
            $captacao->name             = $name;
            $captacao->cell_phone       = $phoneNumber;
            $captacao->url              = $url;
            $captacao->note             = '';//TODO

            //log
            $captacao->created_at       = date('Y-m-d H:i:s');
            $captacao->created_by       = $created_by;

            $captacao_id = $this->CaptacaoService_model->save($captacao);

            if ($captacao_id) {
                $captacaoSalva = $this->CaptacaoRespository_model->getByID($captacao_id);

                if ($captacaoSalva->department_id) {
                    $department = $this->DepartmentRepository_model->getByID($captacaoSalva->department_id);

                    $retorno = 'Olá '.$captacaoSalva->name.'! Registramos seu Atendimento em nosso sistema, logo entraremos em contato pelo telefone '.$captacaoSalva->cell_phone;

                    if ($department->open_whatsapp) {
                        echo $this->sma->send_json(array('success' => true, 'result' => $retorno, 'open_whatsapp' => true , 'telefone'=> $department->telefone));
                    } else {
                        echo $this->sma->send_json(array('success' => true, 'result' => $retorno, 'open_whatsapp' => false));
                    }

                } else {
                    $retorno = 'Olá '.$captacaoSalva->name.'! Registramos seu Atendimento em nosso sistema, logo entraremos em contato pelo telefone '.$captacaoSalva->cell_phone;

                    echo $this->sma->send_json(array('success' => true, 'result' => $retorno, 'open_whatsapp' => false));
                }
            } else {
                echo $this->sma->send_json(array('success' => false, 'error' => 'Não foi possível registrar o atendimento'));
            }

        } catch (Exception $exception) {
            echo $this->sma->send_json(array('success' => false, 'error' => $exception->getMessage()));
        }

    }

    public function search_schedule() {

        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $product_id = $this->input->get('product_id', false);
        $date_shedule = $this->input->get('date_shedule', false);

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->produto = $product_id;
        $filter->date_shedule = $date_shedule;
        $filter->enviar_site = false;

        $this->sma->send_json($this->AgendaViagemService_model->getAllProgramacao($filter));
    }

    public function buscarProgramacao() {

        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $status = $this->input->get('status');
        $ano = $this->input->get('ano');
        $mes = $this->input->get('mes') != 'Todos' ? $this->input->get('mes')  : null;

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->ano = $ano;
        $filter->mes = $mes;
        $filter->status = $status;
        $filter->ocultarViagensPassadas = false;
        $filter->verificarConfiguracaoOcultarProdutosSemEstoque = false;

        $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

        foreach ($programacoes as $programacao) {

            $totalQuartosDisponivel = 0;

            if ($this->Settings->receptive) {
                $label   =  $this->sma->hrsd($programacao->dataSaida). ' ás ' . $this->sma->hf($programacao->horaSaida) . ' - '.$programacao->name;
            } else {
                $label   =  $this->sma->hrsd($programacao->dataSaida). ' - '.$programacao->name;
            }

            foreach ($programacao->getQuartos() as $quarto) {
                $totalQuartosDisponivel += $quarto->qtd_quartos_disponivel;
            }

            if ($programacao->getControleEstoqueHospedagem() ) {
                $label .= ' ('.$programacao->getTotalDisponvel().' Vaga(s) disponíveis | Quarto(s) Disponíveis: '.$totalQuartosDisponivel.')';
            } else {
                if ($programacao->getTotalDisponvel() > 1) {
                    $label .=  '  ('.$programacao->getTotalDisponvel() .' Vagas disponível)';
                } else {
                    $label .= ' (' . $programacao->getTotalDisponvel() . ' Vaga disponível)';
                }
            }

            $pgs[] = array(
                'id' => $programacao->programacaoId,
                'label' => $label
            );
        }

        $this->sma->send_json($pgs);
    }

    public function createPDF()
    {
        try {

            $data_file_name = $this->gerar_contrato();

            if ($data_file_name) {
                $this->sma->send_json(array('file_name' => $data_file_name['file_name'], 'file_name_docx' => $data_file_name['file_name_docx'], 'extensao' => $data_file_name['extensao'],'success' => true));
            } else {
                $this->sma->send_json(array('success' => false, 'error' => 'Não foi possível gerar o contrato'));
            }
        } catch (Exception $exception) {
            echo $this->sma->send_json(array('success' => false, 'error' => $exception->getMessage()));
        }
    }

    public function gerar_contrato(): ?array
    {
        $this->load->model('service/ParseContract_model', 'ParseContract_model');
        $this->load->model('dto/ParseContractDTO_model', 'ParseContractDTO_model');
        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');

        $config = new ParseContractDTO_model();
        $config->type = ParseContractDTO_model::OBJECT;
        $config->appModel = $this->createApp();

        $contractID = $config->appModel->produto->contract_id;

        if ($contractID == null) {
            $contractID = $this->Settings->contract_id;
        }

        $config->contractID = $contractID;

       $contractConfig = $this->ContractRepository_model->getById($contractID);

       $config->tipo = $contractConfig->type;

        if ($contractConfig->type == 'text') {
            return $this->gerar_contrato_pdf($config);
        } else if ($contractConfig->type == 'document') {

            if ($contractConfig->usar_clausulas_site) {
                return $this->gerar_contrato_word_clauses($config, $contractConfig);
            } else {
                return $this->gerar_contrato_word($config, $contractConfig);
            }

        } else {
            return null;
        }

    }

    private function gerar_contrato_pdf($config): array
    {
        $biller = $this->site->getCompanyByID($this->Settings->default_biller);

        $this->data['converted_contract'] = $this->ParseContract_model->parse($config);

        $html = $this->load->view($this->theme . 'contracts/gerar_contrato', $this->data, true);

        $filename = md5(uniqid(mt_rand())) . ".pdf";

        $file_name = $this->sma->gerar_contrato_pdf($html, $filename, 'S', $this->data['biller']->invoice_footer, null, null, null, 'P', base_url() . 'assets/uploads/logos/'.$biller->logo);

        return array('file_name' => $file_name,  'file_name_docx' => $file_name, 'extensao' => 'pdf');
    }

    private function gerar_contrato_word($config, $contractConfig): array
    {
        $this->load->helper('margedocx');

        $config->retorno = ParseContractDTO_model::TIPO_RETORNO_ARRAY;

        $tagsArray = $this->ParseContract_model->parse($config);

        $arquivo_entrada    = $this->upload_path.'/'.$contractConfig->file_name;

        $filename = md5(uniqid(mt_rand()));

        $filename_docx = $filename . ".docx";
        $filename_html = $filename . ".html";
        $filename_pdf  = $filename . ".pdf";

        $arquivo_saida_docx = $this->upload_path.'/'.$filename_docx;
        $arquivo_saida_html = $this->upload_path.'/'.$filename_html;

        marge_docx($arquivo_entrada, $arquivo_saida_docx, $tagsArray);

        $phpWord = IOFactory::load($arquivo_saida_docx);
        $phpWord->save($arquivo_saida_html, 'HTML');

        $html_gerado = file_get_contents($arquivo_saida_html);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html_gerado);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $output = $dompdf->output();

        if (!file_exists($this->upload_path . '/')) {
            mkdir($this->upload_path, 0777, true);
        }

        file_put_contents($this->upload_path . '/' . $filename_pdf, $output);

        unlink($arquivo_saida_html);

        return array('file_name' => $filename_pdf, 'file_name_docx' => $filename_docx, 'extensao' => 'docx');
    }

    private function gerar_contrato_word_clauses($config, $contractConfig): array
    {

        $this->load->helper('margedocx');

        $config->retorno = ParseContractDTO_model::TIPO_RETORNO_ARRAY;

        $tagsArray = $this->ParseContract_model->parse($config);

        $arquivo_entrada    = $this->upload_path.'/'.$contractConfig->file_name;

        $filename = md5(uniqid(mt_rand()));

        $filename_docx = $filename . ".docx";
        $filename_pdf  = $filename . ".pdf";

        $arquivo_saida_docx = $this->upload_path.'/'.$filename_docx;

        marge_docx($arquivo_entrada, $arquivo_saida_docx, $tagsArray);

        $config->retorno = ParseContractDTO_model::TIPO_RETORNO_PARSE_STRING;
        $config->usarContract = false;
        $config->tipo = 'text';

        $this->data['converted_contract'] = $this->ParseContract_model->parse($config);

        $biller             = $this->site->getCompanyByID($this->Settings->default_biller);
        $config->retorno    = ParseContractDTO_model::TIPO_RETORNO_PARSE_STRING;
        $html               = $this->load->view($this->theme . 'contracts/gerar_contrato', $this->data, true);

        $filename_generation = $this->sma->gerar_contrato_pdf($html, $filename_pdf, 'S', $this->data['biller']->invoice_footer, null, null, null, 'P', base_url() . 'assets/uploads/logos/'.$biller->logo);

        return array('file_name' => $filename_generation, 'file_name_docx' => $filename_docx, 'extensao' => 'docx');

    }


    private function createApp(): AppCompra_model
    {
        //model
        $this->load->model('model/Cliente_model', 'Cliente_model');
        $this->load->model('model/ProdutosAdicionais_model', 'Adicionais');
        $this->load->model('model/AppCompra_model', 'AppCompra_model');
        $this->load->model('model/Ingresso_model', 'Ingresso_model');

        //repository
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');

        $clientePrincipal   = new Cliente_model();
        $appModel           = new AppCompra_model();

        $programacao            = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));
        $product                = $this->site->getProductByID($programacao->produto);
        $tipoCobrancaVerifica   = $this->site->getTipoCobrancaById($this->input->post('tipoCobranca'));

        if ($tipoCobrancaVerifica->integracao == 'valepay') {
            $cardName = $this->input->post('name_card_valepay', true);
            $cpfTitular = $this->input->post('cpf_titular_valepay');
            $celularTitular = $this->input->post('tel_titular_valepay');
            $cepTitular = $this->input->post('cep_titular_valepay');
            $enderecoTitular = $this->input->post('endereco_titular_valepay');
            $numeroEnderecoTitular = $this->input->post('numero_endereco_titular_valepay');
            $complementoEnderecoTitular = $this->input->post('complemento_endereco_titular');
            $bairroTitular = $this->input->post('bairro_titular_valepay');
            $cidadeTitular = $this->input->post('cidade_titular_valepay');
            $estadoTitular = $this->input->post('estado_titular_valepay');
            $parcelas = $this->input->post('parcelamento_valepay', true);

        } else {
            $cardName = $this->input->post('nameCartaoPagSeguro', true);
            $cpfTitular = $this->input->post('cpfTitular');
            $celularTitular = $this->input->post('telTitular');
            $cepTitular = $this->input->post('cepTitular');
            $enderecoTitular = $this->input->post('enderecoTitular');
            $numeroEnderecoTitular = $this->input->post('numeroEnderecoTitular');
            $complementoEnderecoTitular = $this->input->post('complementoEnderecoTitular');
            $bairroTitular = $this->input->post('bairroTitular');
            $cidadeTitular = $this->input->post('cidadeTitular');
            $estadoTitular = $this->input->post('estadoTitular');

            if ($this->input->post('installments', true) > 0) {
                $parcelas = $this->input->post('installments', true);
            } else {
                $parcelas = $this->input->post('parcelas', true);
            }
        }

        $i = isset($_POST['nomeDependente']) ? sizeof($_POST['nomeDependente']) : 0;
        $sequencialResponsavelCompra    = 1;
        $contadorClientes               = 1;

        if ($this->coletarPagador($product)) {

            $i = isset($_POST['nomeDependentePagador']) ? sizeof($_POST['nomeDependentePagador']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $clientePrincipal->setName($_POST['nomeDependentePagador'][$r]);
                $clientePrincipal->setCompany($_POST['company_name'][$r]);
                $clientePrincipal->setNomeResponsavel($_POST['nome_responsavel'][$r]);
                $clientePrincipal->setEmail($_POST['emailDependentePagador'][$r]);
                $clientePrincipal->setSexo($_POST['sexoDependentePagador'][$r]);
                $clientePrincipal->setVatNo($_POST['cpfDependentePagador'][$r]);
                $clientePrincipal->setCf5($_POST['celularDependentePagador'][$r]);
                $clientePrincipal->setTelefoneEmergencia($_POST['telefone_emergenciaDependentePagador'][$r]);
                $clientePrincipal->setAlergiaMedicamento($_POST['alergia_medicamentoDependentePagador'][$r]);
                $clientePrincipal->setTipoDocumento(($_POST['tipo_documentoDependentePagador'][$r]));
                $clientePrincipal->setCf1($_POST['rgDependentePagador'][$r]);
                $clientePrincipal->setCf3(($_POST['orgaoEmissorDependentePagador'][$r]));
                $clientePrincipal->setSocialName(($_POST['social_namePagador'][$r]));
                $clientePrincipal->setProfession(($_POST['profissaoDependentePagador'][$r]));
                $clientePrincipal->setDoencaInformar(($_POST['doenca_informarPagador'][$r]));
                $clientePrincipal->setUltimaNota(($_POST['observacao_item'][$r]));
                $clientePrincipal->setUltimoAssento('');
                $clientePrincipal->setUltimoValorExtraAssento(0);

                $dia = $_POST['diaDependentePagador'][$r];
                $mes = $_POST['mesDependentePagador'][$r];
                $ano = $_POST['anoDependentePagador'][$r];

                $clientePrincipal->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                $cep = $this->input->post('cep');
                $endereco = $this->input->post('endereco');
                $numeroEndereco = $this->input->post('numeroEndereco');
                $complementoEndereco = $this->input->post('complementoEndereco');
                $bairro = $this->input->post('bairro');
                $cidade = $this->input->post('cidade');
                $estado = $this->input->post('estado');

                $clientePrincipal->setPostalCode($cep);
                $clientePrincipal->setAddress($endereco);
                $clientePrincipal->setNumero($numeroEndereco);
                $clientePrincipal->setComplemento($complementoEndereco);
                $clientePrincipal->setBairro($bairro);
                $clientePrincipal->setCity($cidade);
                $clientePrincipal->setState($estado);

                $appModel->cliente = $clientePrincipal;

            }
        } else {
            for ($r = 0; $r < $i; $r++) {

                $responsavelPelaCompra = $_POST['responsavelPelaCompra'][$r];

                if ($responsavelPelaCompra == 'SIM') {//TODO VERIFICA O RESPONSAVEL PELA COMPRA ESTA COM ERRO AQUI

                    $sequencialResponsavelCompra  = $contadorClientes;

                    $clientePrincipal->setName($_POST['nomeDependente'][$r]);
                    $clientePrincipal->setCompany($_POST['company_name'][$r]);
                    $clientePrincipal->setNomeResponsavel($_POST['nome_responsavel'][$r]);
                    $clientePrincipal->setEmail($_POST['emailDependente'][$r]);
                    $clientePrincipal->setSexo($_POST['sexoDependente'][$r]);
                    $clientePrincipal->setVatNo($_POST['cpfDependente'][$r]);
                    $clientePrincipal->setCf5($_POST['celularDependente'][$r]);
                    $clientePrincipal->setTelefoneEmergencia($_POST['telefone_emergenciaDependente'][$r]);
                    $clientePrincipal->setAlergiaMedicamento($_POST['alergia_medicamentoDependente'][$r]);

                    $clientePrincipal->setFaixaId(($_POST['tipoFaixaEtariaId'][$r]));
                    $clientePrincipal->setFaixaName(($_POST['tipoFaixaEtariaNome'][$r]));
                    $clientePrincipal->setFaixaEtariaValor(($_POST['tipoFaixaEtariaValor'][$r]));
                    $clientePrincipal->setDescontarVaga(($_POST['descontarVaga'][$r]));

                    $clientePrincipal->setTipoDocumento(($_POST['tipo_documentoDependente'][$r]));
                    $clientePrincipal->setCf1($_POST['rgDependente'][$r]);
                    $clientePrincipal->setCf3(($_POST['orgaoEmissorDependente'][$r]));

                    $clientePrincipal->setSocialName(($_POST['social_name'][$r]));
                    $clientePrincipal->setProfession(($_POST['profissaoDependente'][$r]));
                    $clientePrincipal->setDoencaInformar(($_POST['doenca_informar'][$r]));
                    $clientePrincipal->setUltimoAssento(($_POST['assento'][$r]));
                    $clientePrincipal->setUltimoValorExtraAssento(($_POST['valor_extra_assento'][$r]));
                    $clientePrincipal->setUltimaNota(($_POST['observacao_item'][$r]));

                    $dia = $_POST['diaDependente'][$r];
                    $mes = $_POST['mesDependente'][$r];
                    $ano = $_POST['anoDependente'][$r];

                    $clientePrincipal->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                    $cep = $this->input->post('cep');
                    $endereco = $this->input->post('endereco');
                    $numeroEndereco = $this->input->post('numeroEndereco');
                    $complementoEndereco = $this->input->post('complementoEndereco');
                    $bairro = $this->input->post('bairro');
                    $cidade = $this->input->post('cidade');
                    $estado = $this->input->post('estado');

                    $clientePrincipal->setPostalCode($cep);
                    $clientePrincipal->setAddress($endereco);
                    $clientePrincipal->setNumero($numeroEndereco);
                    $clientePrincipal->setComplemento($complementoEndereco);
                    $clientePrincipal->setBairro($bairro);
                    $clientePrincipal->setCity($cidade);
                    $clientePrincipal->setState($estado);

                    $appModel->cliente = $clientePrincipal;
                }

                $contadorClientes++;
            }
        }

        if ($this->colegarIngresso($product)) {

            foreach ($_POST as $chave => $qtdIngressos) {
                if (strpos($chave, "Clientes_") === 0) {

                    $partes         = explode("_", $chave);
                    $faixaId        = $partes[1];
                    $preco          = $partes[2];
                    $descontarVaga  = $partes[3];
                    $hospedagem     = $partes[4];

                    $ingresso = new Ingresso_model();
                    $ingresso->faixaId          = $faixaId;
                    $ingresso->quantidade       = $qtdIngressos;
                    $ingresso->valor            = $preco;
                    $ingresso->descontarVaga    = $descontarVaga;

                    $appModel->addIngressos($ingresso);
                }
            }
        }

        $appModel->localEmbarque = $this->input->post('localEmbarque');
        $appModel->tipoHospedagem = $this->input->post('tipoHospedagem');
        $appModel->tipoTransporte = $this->input->post('tipoTransporteId');

        $appModel->tipoCobranca = $this->input->post('tipoCobranca');
        $appModel->condicaoPagamento = $this->input->post('condicaoPagamento');
        $appModel->observacaoAdicional = $this->input->post('observacao');

        $appModel->total = $this->input->post('subTotal');
        $appModel->totalComTaxas = $this->input->post('subTotalTaxas');

        //sinal
        $appModel->valorSinal = (double) $this->input->post('valorSinal');
        $appModel->tipoCobrancaSinal = $this->input->post('tipoCobrancaSinal');

        $appModel->valorHospedagemAdulto = $this->input->post('valorHospedagemAdulto');
        $appModel->valorHospedagemCrianca = $this->input->post('valorHospedagemCrianca');
        $appModel->valorHospedagemBebevalorHospedagemBebe = $this->input->post('valorHospedagemBebe');

        $appModel->valorPacoteAdulto = $this->input->post('valorPacoteAdulto');
        $appModel->valorPacoteCrianca = $this->input->post('valorPacoteCrianca');
        $appModel->valorPacoteBebe = $this->input->post('valorPacoteBebe');

        $appModel->vendedorId = $this->input->post('vendedor');

        $appModel->programacao = $programacao;
        $appModel->produto =  $product;

        $appModel->vencimentosParcela = $this->input->post('dtVencimentos');
        $appModel->valorVencimentosParcelas = $this->input->post('valorVencimentos');
        $appModel->descontos = $this->input->post('descontos');
        $appModel->tiposCobranca = $this->input->post('tiposCobranca');
        $appModel->movimentadores = $this->input->post('movimentadores');

        $appModel->totalPassageiros = $this->input->post('totalPassageiros');

        $appModel->cupom_id = $this->input->post("cupom_desconto_id");
        $appModel->desconto_cupom = $this->input->post("valor_cupom_desconto");

        $appModel->listaEspera = $this->input->post("listaEmpera");

        $senderHash = htmlspecialchars($this->input->post('senderHash', true));
        $creditCardToken = htmlspecialchars($this->input->post('creditCardToken', true));
        $deviceId = htmlspecialchars($this->input->post('deviceId', true));

        $appModel->meioDivulgacao = $this->input->post('meioDivulgacao');

        $appModel->totalParcelaPagSeguro = $this->input->post('totalParcelaPagSeguro');

        $appModel->cpfTitularCartao = $cpfTitular;
        $appModel->celularTitularCartao = $celularTitular;
        $appModel->cepTitularCartao = $cepTitular;
        $appModel->enderecoTitularCartao = $enderecoTitular;
        $appModel->numeroEnderecoTitularCartao = $numeroEnderecoTitular;
        $appModel->complementoEnderecoTitularCartao = $complementoEnderecoTitular;
        $appModel->bairroTitularCartao = $bairroTitular;
        $appModel->cidadeTitularCartao = $cidadeTitular;
        $appModel->estadoTitularCartao = $estadoTitular;

        $appModel->parcelas             = $parcelas;
        $appModel->cardName             = $cardName;
        $appModel->cardNumber           = $this->input->post('card_number_valepay', true);
        $appModel->cardExpiryMonth      = $this->input->post('card_expiry_mm_valepay', true);
        $appModel->cardExpiryYear       = $this->input->post('card_expiry_yyyy_valepay', true);
        $appModel->cvc                  = $this->input->post('cvc_valepay', true);

        //token cartao de credito
        $appModel->senderHash       = $senderHash;
        $appModel->creditCardToken  = $creditCardToken;
        $appModel->deviceId         = $deviceId;

        $i = isset($_POST['nomeDependente']) ? sizeof($_POST['nomeDependente']) : 0;
        $contadorClientes = 1;

        for ($r = 0; $r < $i; $r++) {
            $dependente = new Cliente_model();
            $nomeDependente = $_POST['nomeDependente'][$r];

            if ($nomeDependente != '' && $contadorClientes != $sequencialResponsavelCompra) {

                $dependente->setName($nomeDependente);
                $dependente->setSexo($_POST['sexoDependente'][$r]);
                $dependente->setVatNo($_POST['cpfDependente'][$r]);
                $dependente->setCf5($_POST['celularDependente'][$r]);
                $dependente->setTelefoneEmergencia($_POST['telefone_emergenciaDependente'][$r]);
                $dependente->setAlergiaMedicamento($_POST['alergia_medicamentoDependente'][$r]);
                $dependente->setEmail($_POST['emailDependente'][$r]);

                $dependente->setFaixaId(($_POST['tipoFaixaEtariaId'][$r]));
                $dependente->setFaixaName(($_POST['tipoFaixaEtariaNome'][$r]));
                $dependente->setFaixaEtariaValor(($_POST['tipoFaixaEtariaValor'][$r]));
                $dependente->setDescontarVaga(($_POST['descontarVaga'][$r]));

                $dependente->setTipoDocumento(($_POST['tipo_documentoDependente'][$r]));
                $dependente->setCf1($_POST['rgDependente'][$r]);
                $dependente->setCf3(($_POST['orgaoEmissorDependente'][$r]));
                $dependente->setSocialName(($_POST['social_name'][$r]));
                $dependente->setProfession(($_POST['profissaoDependente'][$r]));
                $dependente->setDoencaInformar(($_POST['doenca_informar'][$r]));
                $dependente->setUltimoAssento(($_POST['assento'][$r]));
                $dependente->setUltimoValorExtraAssento(($_POST['valor_extra_assento'][$r]));
                $dependente->setUltimaNota(($_POST['observacao_item'][$r]));

                $dia = $_POST['diaDependente'][$r];
                $mes = $_POST['mesDependente'][$r];
                $ano = $_POST['anoDependente'][$r];

                $dependente->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                $appModel->adicionarDependente($dependente);
            }

            $contadorClientes++;
        }

        $j = isset($_POST['qtdAdicionais']) ? sizeof($_POST['qtdAdicionais']) : 0;
        for ($r = 0; $r < $j; $r++) {

            $servicoId       = $_POST['servicosAdicionais'][$r];
            $valorAdicional  = $_POST['valorServicoAdicional'][$r];
            $qtd             = $_POST['qtdAdicionais'][$r];
            $faixaIdAdiconal = $_POST['faixaIdAdiconal'][$r];

            $adicional = new ProdutosAdicionais_model();
            $adicional->produto = $servicoId;
            $adicional->quantidade = $qtd;
            $adicional->valor = $valorAdicional;
            $adicional->faixaId = $faixaIdAdiconal;
            $adicional->tipo = ProdutosAdicionais_model::ADULTO;

            $appModel->addAdicionais($adicional);
        }

        return $appModel;
    }

    private function coletarPagador($product): bool
    {
        return $product->isApenasColetarPagador == 1 || $product->isApenasColetarPagador == 2;
    }

    private function colegarIngresso($product): bool
    {
        return $product->isApenasColetarPagador == 1;
    }

    public function downloadDocument($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            if ($inv->contract_id == null) {
                throw new Exception('O documento não foi gerado.');
            }

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            if ($document->document_status == Document_model::DOCUMENT_STATUS_DRAFT) {
                $this->DocumentService_model->downloadOriginalDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_UNSIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_SIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_PENDING
                || $document->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                $this->DocumentService_model->downloadDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
                throw new Exception('O documento foi cancelado.');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function downloadDocumentByContractID($contractID)
    {
        try {

            if ($contractID == null) {
                throw new Exception('O documento não foi gerado.');
            }

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($contractID);

            if ($document->document_status == Document_model::DOCUMENT_STATUS_DRAFT) {
                $this->DocumentService_model->downloadOriginalDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_UNSIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_SIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_PENDING
                || $document->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                $this->DocumentService_model->downloadDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
                throw new Exception('O documento foi cancelado.');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');


        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $inv = $this->site->getInvoiceByID($id);

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";

        $faturas = $this->site->getParcelasFaturaByContaReceber($id);
        $return  = $this->site->getReturnBySID($id);

        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->site->getPaymentsForSale($id);
        $this->data['biller']   = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user']     = $this->site->getUser($inv->created_by);
        $this->data['warehouse']= $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento'] = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->site->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->site->getAllReturnItems($return->id) : null;
        $this->data['inv'] = $inv;

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            if ($inv->sale_status == 'orcamento') {
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'lista_espera') {
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/lista_espera.png'));
            } else {
                return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
            }
        } else {
            if ($inv->sale_status == 'orcamento') {
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else  if ($inv->sale_status == 'lista_espera') {
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/lista_espera.png'));
            } else {
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    public function maisatividades($category_id, $vendedorId)
    {
        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        $vendedor    = $this->site->getCompanyByID($vendedorId);

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->category = $category_id;
        $filter->enviar_site = false;
        $filter->limit = 8;
        $filter->order_by_custom = 'RAND()';
        $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

        $this->data['vendedor']      = $vendedor;
        $this->data['destaques']    =  $this->AgendaViagemService_model->getAllProgramacao($filter);

        $this->load->view($this->theme . 'website/cart/mais_atividades', $this->data);
    }

    public function wscliente() {

        $cnpj = $this->input->post('cnpj');

        $url = "https://www.receitaws.com.br/v1/cnpj/".$cnpj;
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        if(!empty($post)) {
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        }
        $result = curl_exec($ch);
        curl_close($ch);

        header('Content-Type: application/json');
        echo json_encode($result);
    }
}