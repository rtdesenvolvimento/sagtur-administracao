<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Departments extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->library('form_validation');

        //repository
        $this->load->model('repository/DepartmentRepository_model', 'DepartmentRepository_model');

        $this->lang->load('departments', $this->Settings->user_language);
    }

    public function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('departments')));
        $meta = array('page_title' => lang('departments'), 'bc' => $bc);

        $this->page_construct('departments/index', $meta, $this->data);
    }

    function getDepartments()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("departments.id as id, name")
            ->from("departments")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('departments/edit/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("edit_department") . "'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    function add()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
                'telefone' => $this->input->post('telefone'),
                'open_whatsapp' => $this->input->post('open_whatsapp'),
            );
        } elseif ($this->input->post('addDepartment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("departments");
        }

        if ($this->form_validation->run() == true && $this->DepartmentRepository_model->add($data)) {
            $this->session->set_flashdata('message', lang("department_adicionado_com_sucesso"));
            redirect("departments");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'departments/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active' => $this->input->post('active'),
                'name' => $this->input->post('name'),
                'telefone' => $this->input->post('telefone'),
                'open_whatsapp' => $this->input->post('open_whatsapp'),
            );
        } elseif ($this->input->post('editDepartment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("departments");
        }

        if ($this->form_validation->run() == true && $this->DepartmentRepository_model->updateDepartment($id, $data)) {
            $this->session->set_flashdata('message', lang("department_atualizado_com_sucesso"));
            redirect("departments");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['department'] = $this->DepartmentRepository_model->getByID($id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'departments/edit', $this->data);
        }
    }

}