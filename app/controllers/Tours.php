<?php defined('BASEPATH') or exit('No direct script access allowed');

class Tours extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('tours', $this->Settings->user_language);

        $this->load->library('form_validation');

        //service
        $this->load->model('service/ProdutoService_model', 'ProdutoService_model');

        //repository
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');
        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/ValorFaixaRepository_model', 'ValorFaixaRepository_model');

        //model
        $this->load->model('model/ProdutoModel_model', 'ProdutoModel_model');

        //model
        $this->load->model('sales_model');
        $this->load->model('products_model');
        $this->load->model('financeiro_model');

        //configs
        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '2048';//2MB
        $this->popup_attributes     = array('width' => '900', 'height' => '600', 'window_name' => 'sma_popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
    }

    function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('tours')));
        $meta = array('page_title' => lang('tours'), 'bc' => $bc);
        $this->page_construct('tours/index', $meta, $this->data);
    }

    function getTours()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');

        $detail_link = anchor('products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $arquivar_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("arquivar_produto") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . site_url('products/arquivar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('arquivar_produto') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
		<ul class="dropdown-menu pull-right" role="menu">
			<li>' . $detail_link . '</li>
			<li><a href="' . site_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>
			<li><a href="' . site_url('products/duplicar/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>';

        $action .= '	<li class="divider"></li>
       		            <li>' . $arquivar_link . '</li>
       		    </ul> 
       	    </div></div>';

        $this->datatables
            ->select($this->db->dbprefix('products') . ".id as productid, " .
                $this->db->dbprefix('products') . ".image as image, " .
                $this->db->dbprefix('products') . ".code as code, " .
                $this->db->dbprefix('products') . ".name as name, " .
                $this->db->dbprefix('categories') . ".name as cname", FALSE)
            ->from('products')
            ->join('categories', 'products.category_id=categories.id', 'left')
            ->order_by('products.name');

        $this->db->where('viagem', 1);
        $this->db->where('unit in ("Confirmado","Inativo") ');

        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    /* ------------------------------------------------------- */
    function add($id = NULL)
    {
        $this->sma->checkPermissions();

        $this->load->helper('security');
        $this->load->library('upload');

        $warehouses = $this->site->getAllWarehouses();

        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $this->form_validation->set_rules('name', lang('name'), 'trim|required');

            $produtoModel = new ProdutoModel_model();

            $details =  $this->input->post('details');
            $active = $this->input->post('active');
            $unit = 'Confirmado';

            if (!$active) $unit = 'Inativo';

            $data = array(
                'code' 				            => rand(1000,100000),
                'type'				            => 'standard',
                'barcode_symbology'             => 'ean13',
                'viagem' 			            => 1,
                'unit' 				            => $unit,
                'enviar_site' 		            => $this->input->post('enviar_site'),
                'apenas_cotacao' 		        => $this->input->post('apenas_cotacao') ,
                'quantidadePessoasViagem' 		=> $this->input->post("quantidadePessoasViagem"),
                'quantity'                      => $this->input->post("quantidadePessoasViagem"),
                'name' 				            => $this->input->post('name'),
                'tempo_viagem' 		            => $this->input->post('tempo_viagem'),
                'alertar_polcas_vagas' 		    => $this->input->post('alertar_polcas_vagas'),
                'alertar_ultimas_vagas' 		=> $this->input->post('alertar_ultimas_vagas'),
                'price_pacote_zero_cinco' 		=> $this->input->post('price_pacote_zero_cinco'),
                'price_pacote_seis_onze' 		=> $this->input->post('price_pacote_seis_onze'),
                'price_pacote_acima_sessenta'   => $this->input->post('price_pacote_acima_sessenta'),
                'percentual_desconto_a_vista' 	=> $this->input->post('percentual_desconto_a_vista'),
                'categorias' 		            => $this->input->post('categorias'),
                'instrucoescontrato' 		    => $this->input->post('instrucoescontrato'),
                'tipo_transporte' 		        => $this->input->post('tipo_transporte'),
                'itinerario' 		            => $this->input->post('itinerario'),
                'oqueInclui' 		            => $this->input->post('oqueInclui'),
                'valores_condicoes'             => $this->input->post('valores_condicoes'),
                'duracao_pacote'                => $this->input->post('duracao_pacote'),
                'numero_pessoas' 	            => $this->input->post('numero_pessoas'),
                'status' 			            => $this->input->post('status'),
                'data_saida' 		            => $this->input->post('data_saida'),
                'origem' 			            => $this->input->post('origem'),
                'previsao_chegada' 	            => $this->input->post('previsao_chegada'),
                'data_chegada' 		            => $this->input->post('data_chegada'),
                'destino' 			            => $this->input->post('destino'),
                'data_retorno'		            => $this->input->post('data_retorno'),
                'hora_saida'		            => $this->input->post('hora_saida'),
                'hora_retorno'		            => $this->input->post('hora_retorno'),
                'margem_padrao' 	            => $this->input->post('margem_padrao'),
                'simbolo_moeda' 	            => $this->input->post('simbolo_moeda'),
                'valor_pacote' 	                => $this->input->post('valor_pacote'),
                'simboloMoeda'                  => $this->input->post('simboloMoeda'),
                'precoExibicaoSite' 	        => $this->input->post('precoExibicaoSite'),
                'local_retorno' 	            => $this->input->post('local_retorno'),
                'hora_chegada' 	                => $this->input->post('hora_chegada'),
                'category_id' 		            => $this->input->post('category'),
                'subcategory_id' 	            => $this->input->post('subcategory') ,
                'cost' 				            => $this->sma->formatDecimal($this->input->post('price')),
                'price'	 			            => $this->sma->formatDecimal($this->input->post('price')),
                'tax_rate' 			            => $this->input->post('tax_rate'),
                'tax_method' 		            => $this->input->post('tax_method'),
                'alert_quantity' 	            => 1,
                'track_quantity' 	            => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' 			            => $details,
                'product_details' 	            => $this->input->post('product_details'),
                'supplier1' 		            => $this->input->post('supplier'),
                'supplier1price' 	            => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' 		            => $this->input->post('supplier_2'),
                'supplier2price' 	            => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' 		            => $this->input->post('supplier_3'),
                'supplier3price' 	            => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' 		            => $this->input->post('supplier_4'),
                'supplier4price' 	            => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' 		            => $this->input->post('supplier_5'),
                'supplier5price' 	            => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' 				            => $this->input->post('cf1'),
                'cf2' 				            => $this->input->post('cf2'),
                'cf3' 				            => $this->input->post('cf3'),
                'cf4' 				            => $this->input->post('cf4'),
                'cf5' 				            => $this->input->post('cf5'),
                'cf6' 				            => $this->input->post('cf6'),
                'alert_stripe' 		            => $this->input->post('alert_stripe'),
                'promotion' 		            => $this->input->post('promotion'),
                'promo_price' 		            => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' 		            => $this->sma->fsd($this->input->post('start_date')),
                'end_date' 			            => $this->sma->fsd($this->input->post('end_date')),
                'supplier1_part_no'             => $this->input->post('supplier_part_no'),
                'supplier2_part_no'             => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no'             => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no'             => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no'             => $this->input->post('supplier_5_part_no'),
                'isServicoOnline'               => $this->input->post('isServicoOnline'),
                'isTransporteTuristico'         => $this->input->post('isTransporteTuristico'),
                'isServicosAdicionais'          => $this->input->post('isServicosAdicionais'),
                'isApenasColetarPagador'        => $this->input->post('isApenasColetarPagador'),
                'isEmbarque'                    => $this->input->post('isEmbarque'),
                'isComissao'                    => $this->input->post('isComissao'),
                'permitirListaEmpera'           => $this->input->post('permitirListaEmpera'),
                'active'                        => $this->input->post('active'),
                'captarEnderecoLink'            => $this->input->post('captarEnderecoLink'),
                'permiteVendaMenorIdade'        => $this->input->post('permiteVendaMenorIdade') == null ? '0' : $this->input->post('permiteVendaMenorIdade'),
                'permiteVendaClienteDuplicidade'=> $this->input->post('permiteVendaClienteDuplicidade') == null ? '0' : $this->input->post('permiteVendaClienteDuplicidade'),
                'isTaxasComissao'               => $this->input->post('isTaxasComissao'),
                'tipoComissao'                  => $this->input->post('tipoComissao'),
                'tipoCalculoComissao'           => $this->input->post('tipoCalculoComissao'),
                'comissao'                      => $this->input->post('comissao'),

                'isValorPorFaixaEtaria'         => TRUE,
            );

            $image = $this->adicionarFotoPrincipal(true);

            if ($image) $data['image'] = $image;

            if (!isset($items)) $items = NULL;

            $produtoModel->data = $data;
            $produtoModel->photos =  $this->adicionarFotosAdicionais();
            $produtoModel->items = $this->adicionarServicosAdicionais();
            $produtoModel->product_attributes = $this->adicionarAtributosDaViagem($data);
            $produtoModel->faixaEtariaValores = $this->adicionarFaixaEtariaValor();

            $produtoModel->locaisEmbarque = $this->adicionarLocaisEmbarque();
            $produtoModel->tiposCobranca = $this->adicionarTiposCobranca();
            $produtoModel->transportes = $this->adicionarTransportes();

            $produtoModel->tiposHospedagem = $this->adicionarTipoHospedagemViagem();
            $produtoModel->tiposHospedagemFaixaEtaria = $this->adicionarFaixaEtariaTipoHospedagem();

            $produtoModel->faixaEtariaValoresServicoAdicional = $this->adicionarFaixaEtariaValoresServicoAdicional();
            $produtoModel->programacaoDatas = [];

            //$this->sma->print_arrays($data, $warehouse_qty, $product_attributes);
        }

        if ($this->form_validation->run() == true && $tour_id = $this->ProdutoService_model->adicionarProduto($produtoModel)) {
            $this->session->set_flashdata('message', lang("product_added"));
            redirect('tours/edit/'.$tour_id);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $warehouses;
            $this->data['product'] = $id ? $this->products_model->getProductByID($id) : NULL;
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();
            $this->data['destinos'] = $this->site->getDestinos();
            $this->data['fornecedores'] = $this->products_model->getAllCustomerCompanies();
            $this->data['warehouses_products'] = $id ? $this->products_model->getAllWarehousesWithPQ($id) : NULL;
            $this->data['tiposTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTiposTransporteRodoviario();
            $this->data['tiposQuarto'] = $this->TipoQuartoRepository_model->getTiposQuarto();
            $this->data['hospedagens'] = $this->products_model->getVariacaoHospedagens();
            $this->data['combo_items'] = ($id && $this->data['product']->type == 'combo') ? $this->products_model->getProductComboItems($id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $this->data['valorFaixas'] = $this->ValorFaixaRepository_model->getValoreFaixas();

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('tours'), 'page' => lang('tours')),
                array('link' => '#', 'page' => lang('add_tour'))
            );

            $meta = array('page_title' => lang('add_tour'), 'bc' => $bc);
            $this->page_construct('tours/add', $meta, $this->data);
        }
    }

    private function adicionarFotoPrincipal($add=true) {

        $this->load->library('upload');

        if ($add) $photo = 'no_image.png';
        else $photo = false;

        if ($_FILES['product_image']['size'] > 0) {

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->image_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $config['encrypt_name'] = TRUE;

            $this->upload->initialize($config);

            if (!$this->upload->do_upload('product_image')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/add");
            }

            $photo = $this->upload->file_name;

            $this->load->library('image_lib');
            $config['image_library'] = 'gd2';
            $config['source_image'] = $this->upload_path . $photo;
            $config['new_image'] = $this->thumbs_path . $photo;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $this->Settings->twidth;
            $config['height'] = $this->Settings->theight;
            $this->image_lib->clear();
            $this->image_lib->initialize($config);

            if (!$this->image_lib->resize()) echo $this->image_lib->display_errors();

            if ($this->Settings->watermark) {
                $this->image_lib->clear();
                $wm['source_image'] = $this->upload_path . $photo;
                $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                $wm['wm_type'] = 'text';
                $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                $wm['quality'] = '100';
                $wm['wm_font_size'] = '16';
                $wm['wm_font_color'] = '999999';
                $wm['wm_shadow_color'] = 'CCCCCC';
                $wm['wm_vrt_alignment'] = 'top';
                $wm['wm_hor_alignment'] = 'right';
                $wm['wm_padding'] = '10';

                $this->image_lib->initialize($wm);
                $this->image_lib->watermark();
            }
            $this->image_lib->clear();
            $config = NULL;
        }

        return $photo;
    }

    private function adicionarFotosAdicionais() {

        $photos = null;
        $this->load->library('upload');

        if ($_FILES['userfile']['name'][0] == "") return NULL;

        $config['upload_path'] = $this->upload_path;
        $config['allowed_types'] = $this->image_types;
        $config['max_size'] = $this->allowed_file_size;
        $config['max_width'] = $this->Settings->iwidth;
        $config['max_height'] = $this->Settings->iheight;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $config['max_filename'] = 25;

        $files = $_FILES;
        $cpt = count($_FILES['userfile']['name']);

        for ($i = 0; $i < $cpt; $i++) {

            $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
            $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
            $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
            $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect("products/add");
            } else {

                $pho = $this->upload->file_name;
                $photos[] = $pho;

                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $pho;
                $config['new_image'] = $this->thumbs_path . $pho;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;

                $this->image_lib->initialize($config);

                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }

                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $pho;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'right';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
            }
        }

        $config = NULL;
        $_FILES = $files;

        return $photos;
    }

    private function adicionarServicosAdicionais() {
        $c           = sizeof($_POST['combo_item_id']) - 1;
        for ($r = 0; $r <= $c; $r++) {
            if (isset($_POST['combo_item_code'][$r]) &&
                isset($_POST['combo_item_id'][$r]) &&
                isset($_POST['combo_item_price'][$r])) {
                $items[] = [
                    'quantity'      => 1,
                    'item_code'     => $_POST['combo_item_code'][$r],
                    'unit_price'    => $_POST['combo_item_price'][$r],
                    'item_id'       => $_POST['combo_item_id'][$r],
                    'comissao'      => $_POST['servicoAdicionalValorComissao'][$r],
                    'fornecedorId'  => $_POST['servicoAdicionalFornecedor'][$r],
                ];
            }
        }

        return $items;
    }

    private function adicionarAtributosDaViagem($data) {

        $Settings = $this->site->get_setting();

        $product_attributes [] = array (
            'name' => 'Passagem',//TODO VARIACAO DA PASSAGEM
            'warehouse_id' => $Settings->default_warehouse,//TODO ID DA FILIAL
            'quantity' => $data['quantity'],
            'cost' => $data['cost'],
            'price' => $data['price'],
            'fornecedor' => 0,
            'totalPoltrona' => 0,
            'totalPessoasConsiderar' => 0,
            'localizacao' => null,
            'totalPreco' => null,
        );

        $a = sizeof ( $_POST ['attr_name'] );
        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['attr_name'] [$r] )) {
                $product_attributes [] = array (
                    'name' => $_POST ['attr_name'] [$r],
                    'warehouse_id' => $Settings->default_warehouse,//TODO ID DA FILIAL
                    'quantity' => 0,
                    'cost' => $_POST ['attr_cost'] [$r],
                    'price' => $_POST ['attr_price'] [$r],
                    'fornecedor' => $_POST ['attr_fornecedor'] [$r],
                    'totalPoltrona' => $_POST ['att_totalPoltrona'] [$r],
                    'totalPessoasConsiderar' => $_POST ['att_totalPessoasConsiderar'] [$r],
                    'localizacao' => $_POST ['attr_localizacao'] [$r],
                    'totalPreco' => $_POST ['attr_totalPreco'] [$r]
                );
            }
        }
        return $product_attributes;
    }

    private function adicionarFaixaEtariaValor() {

        if ($this->input->post('isValorPorFaixaEtaria')) {
            $a = sizeof($_POST ['tipoFaixaEtariaValorConfigure']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['tipoFaixaEtariaValorConfigure'] [$r])) {

                    $faixaEtariaValores [] = array(
                        'faixaId' => $_POST['valorFaixaId'] [$r],
                        'valor' => $_POST['valorFaixaEtariaValorConfigure'] [$r],
                        'status' => $this->adicionarFaixaEtariaValorAtivo($_POST['valorFaixaId'] [$r]),
                        'tipo' => $_POST['tipoFaixaEtariaValorConfigure'] [$r],

                        'comissao' => 0,
                        'tipoComissao' => '0',
                        'idadeInicio' => 0,
                        'idadeFinal' => 0,
                        'note' => $_POST['notaFaixaEtaria'] [$r],
                    );
                }
            }

            return $faixaEtariaValores;
        }

        return [];
    }


    private function adicionarFaixaEtariaValorAtivo($tipo) {
        $a = sizeof($_POST ['tipoFaixaEtariaValorConfigure']);
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoFaixaId = $_POST ['ativarTipoFaixaEtaria'] [$r];

            if ($ativoFaixaId == $tipo) $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarLocaisEmbarque() {
        $a = sizeof($_POST ['localEmbarque']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['localEmbarque'] [$r] )) {
                $locaisEmbarque [] = array (
                    'localEmbarque' => $_POST ['localEmbarque'] [$r],
                    'horaEmbarque' => $_POST ['horaEmbarque'] [$r],
                    'dataEmbarque' => $_POST ['dataEmbarque'] [$r],
                    'note' => $_POST ['noteEmbarque'] [$r],
                    'status' => $this->adicionarLocaisEmbarqueAtivo($_POST ['localEmbarque'] [$r])
                );
            }
        }
        return $locaisEmbarque;
    }

    private function adicionarLocaisEmbarqueAtivo($localEmbarque) {
        $a = sizeof($_POST ['localEmbarque']);
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativoLocalEmbarque'] [$r];

            if ($ativoId == $localEmbarque)  $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarTiposCobranca() {
        $a = sizeof($_POST ['tipoCobrancaCondicaoPagamento']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['tipoCobrancaCondicaoPagamento'] [$r] )) {
                $ativo = $_POST ['ativoCondicaoPagamento'] [$r];

                if ($ativo == '1') $ativo = 'ATIVO';
                else $ativo = 'INATIVO';

                $locaisEmbarque [] = array (
                    'tipoValor' => $_POST ['tipoCobrancaCondicaoPagamento'] [$r],
                    'tipo' => $_POST ['acrescimoDescontoCondicaoPagamento'] [$r],
                    'valor' => $_POST ['precoCondicaoPagamento'] [$r],
                    'formaPagamento' => $_POST ['formaPagamento'] [$r],
                    'tipoCobranca' => $_POST ['tipoCobranca'] [$r],
                    'status' => $ativo
                );
            }
        }
        return $locaisEmbarque;
    }

    private function adicionarTransportes() {
        $a = sizeof($_POST ['tipoTransporte']);

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['tipoTransporte'] [$r] )) {
                $tiposTransporte [] = array (
                    'tipoTransporte' => $_POST ['tipoTransporte'] [$r],
                    'status' => $this->adicionarTransporteAtivo($_POST ['tipoTransporte'] [$r])
                );
            }
        }
        return $tiposTransporte;
    }

    private function adicionarTransporteAtivo($tipoTransporte) {
        $a = sizeof($_POST ['tipoTransporte']);
        $ativo      = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId    = $_POST ['ativarTipoTransporte'] [$r];
            if ($ativoId == $tipoTransporte) $ativo = 'ATIVO';
        }
        return $ativo;
    }

    private function adicionarTipoHospedagemViagem() {

        if ($this->input->post('isHospedagem')) {

            $a = sizeof($_POST ['attr_name']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['attr_name'] [$r])) {
                    $tiposHospedagem[] = array(
                        'preco' => $_POST ['attr_price'] [$r],
                        'tipoHospedagem' => $_POST ['tipoHospedagemId'] [$r],
                        'estoque' => $_POST ['estoque_hospedagem'] [$r],
                        'status' => $this->adicionarTipoHospedagemAtivo($_POST ['tipoHospedagemId'] [$r]),
                    );
                }
            }
            return $tiposHospedagem;
        }

        return [];
    }

    private function adicionarTipoHospedagemAtivo($tipoHospedagem) {
        $a = sizeof ( $_POST ['attr_name'] );
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativoTipoHospedagem'] [$r];

            if ($ativoId == $tipoHospedagem) $ativo = 'ATIVO';
        }

        return $ativo;
    }

    private function adicionarFaixaEtariaTipoHospedagem() {

        if ($this->input->post('isHospedagem')) {

            $a = sizeof($_POST ['faixaTipoHospedagemId']);

            for ($r = 0; $r <= $a; $r++) {
                if (isset ($_POST ['faixaTipoHospedagemId'] [$r])) {

                    $tiposHospedagemFaixaEtaria [] = array(
                        'faixaId' => $_POST['valorFaixaIdHospedagem'] [$r],
                        'tipoHospedagem' => $_POST['faixaTipoHospedagemId'] [$r],
                        'valor' => $_POST['valorFaixaEtariaValorHospedagem'] [$r],
                        'tipo' => $_POST['tipoFaixaEtariaValorHospedagem'] [$r],
                        'status' => $this->adicionarTipoHospedagemFaixaValorAtivo($_POST ['faixaTipoHospedagemId'] [$r], $_POST['valorFaixaIdHospedagem'] [$r]),

                        'comissao' => 0,
                        'tipoComissao' => '',
                    );
                }
            }
            return $tiposHospedagemFaixaEtaria;
        }

        return [];
    }

    private function adicionarTipoHospedagemFaixaValorAtivo($tipoHospedagemId, $faixaId) {
        $a = sizeof ( $_POST ['faixaTipoHospedagemId'] );
        $ativo = 'INATIVO';

        for($r = 0; $r <= $a; $r ++) {
            $ativoId = $_POST ['ativarValorHospedagem'] [$r];

            if ($ativoId == $tipoHospedagemId.'_'.$faixaId) {
                $ativo = 'ATIVO';
            }
        }

        return $ativo;
    }

    private function adicionarFaixaEtariaValoresServicoAdicional() {

        $a = sizeof ( $_POST ['servicoAdicionalId'] );

        for($r = 0; $r <= $a; $r ++) {
            if (isset ( $_POST ['servicoAdicionalId'] [$r] )) {

                $valor  = $_POST ['servicoAdicionalValor'] [$r];
                $status = $_POST ['isAtivoServicoAdicional'] [$r];

                if ($status == 'true') {
                    $servicosAdicionaisFaixaEtaria[] = $this->getAdicionarFaixaEtariaServicoAdicionalArray($valor, $_POST['servicoAdicionalId'] [$r], 'ATIVO',  $_POST['faixaIdServicoAdicional'] [$r]);
                } else {
                    $servicosAdicionaisFaixaEtaria[] = $this->getAdicionarFaixaEtariaServicoAdicionalArray(0, $_POST['servicoAdicionalId'] [$r], 'INATIVO', $_POST['faixaIdServicoAdicional'] [$r]);
                }

            }
        }

        return $servicosAdicionaisFaixaEtaria;
    }

    private function getAdicionarFaixaEtariaServicoAdicionalArray($valor, $servicoAdicionalId, $ativo, $faixaId) {
        $tiposHospedagemFaixaEtaria = array (
            'valor' => $valor,
            'servico' => $servicoAdicionalId,
            'status' => $ativo,
            'tipo' => '',
            'faixaId' => $faixaId,
        );

        return $tiposHospedagemFaixaEtaria;
    }

    public function tour() {

        $this->data['products'] = $this->products_model->getAllTours();
        $this->data['billers']  = $this->site->getAllCompanies('biller');
        $this->data['locais_embarque'] = $this->site->getLocaisEmbarque();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('tours')));
        $meta = array('page_title' => lang('tours'), 'bc' => $bc);
        $this->page_construct('tours/index', $meta, $this->data);
    }

    public function getTours2()
    {

        $this->load->library('datatables');

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("sma_sales.id as id,
                    concat(sma_agenda_viagem.dataSaida, ' ', sma_agenda_viagem.horaRetorno) as date, 
                    local_embarque.name as local_embarque,
                    sale_items.product_name as procuct,
                    sma_sales.reference_no,
                    sma_sales.vendedor,
                    sale_items.customerClientName as customer,
                    sma_sales.sale_status, 
                    sale_items.subtotal as grand_total, 
                    (((sma_sales.paid*100)/sma_sales.grand_total)/100 * sma_sale_items.subtotal) as paid,
                    sma_sale_items.subtotal - (((sma_sales.paid*100)/sma_sales.grand_total)/100 * sma_sale_items.subtotal) as balance,
                    sma_sales.payment_status")
            ->from('sale_items');
        $this->datatables->join('sales', 'sales.id = sale_items.sale_id');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId');
        $this->datatables->join('local_embarque', 'local_embarque.id = sale_items.localEmbarque', 'left');

        if ($this->input->post('filterProduct')) $this->datatables->where("sale_items.product_id", $this->input->post('filterProduct'));
        if ($this->input->post('filterDateFrom')) $this->datatables->where("sma_agenda_viagem.dataSaida>=", $this->input->post('filterDateFrom'));
        if ($this->input->post('filterDateUp')) $this->datatables->where("sma_agenda_viagem.dataSaida<=", $this->input->post('filterDateUp'));
        if ($this->input->post('filterSatusSale')) $this->datatables->where("sale_status", $this->input->post('filterSatusSale'));
        if ($this->input->post('filterSatusPayment')) $this->datatables->where("payment_status", $this->input->post('filterSatusPayment'));
        if ($this->input->post('filterDateSaleFrom')) $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $this->input->post('filterDateSaleFrom'));
        if ($this->input->post('filterDateSaleUp'))$this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $this->input->post('filterDateSaleUp'));
        if ($this->input->post('filterBiller')) $this->datatables->where('biller_id', $this->input->post('filterBiller'));
        if ($this->input->post('filterLocalEmbarque')) $this->datatables->where('biller_id', $this->input->post('filterLocalEmbarque'));


        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

}