<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Document extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->load('document', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->model('document_model');

        $this->upload_path          = 'assets/uploads/documents/'.$this->session->userdata('cnpjempresa');
        $this->file_types           = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '4096';//4MB tamanho maximo do arquivo
    }

    function index()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('documents')));
        $meta = array('page_title' => lang('documents'), 'bc' => $bc);
        $this->page_construct('documents/index', $meta, $this->data);
    }

    function getDocuments() {
        $this->load->library('datatables');

        $eventos_venda = '<a href="'.base_url().'sales/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('sales/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = "<a href='#' class='po' title='<b>" . lang("cancelar_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/cancelar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('cancelar_sale') . "</a>";

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("document.id as id,
                    document.name as doc,
                    document.created_at as date,
                    document.document_status as status,
                    document.document_status as st")
            ->from('document');

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function addDocument() {

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $data = array(
                'document_status' => 'CREATED',
                'name' => $this->input->post('name'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
                'created_at' => date('Y-m-d H:i:s')
            );

            if ($_FILES['document']['size'] > 0) {

                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path . '/';
                $config['allowed_types'] = $this->file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!file_exists($this->upload_path . '/')) {
                    mkdir($this->upload_path , 0777, true);
                }

                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $data['file_name'] = $this->upload->file_name;
            }

        } elseif ($this->input->post('addDocument')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        try {
            if ($this->form_validation->run() == true && $this->document_model->save($data)) {
                $this->session->set_flashdata('message', lang("document_successfully_added"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'documents/addDocument', $this->data);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}