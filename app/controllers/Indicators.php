<?php defined('BASEPATH') or exit('No direct script access allowed');

class Indicators extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->model('service/IndicatorsService_model');
    }

    function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('indicators')));
        $meta = array('page_title' => lang('indicators'), 'bc' => $bc);

        $arr_ano_aterior = $this->IndicatorsService_model->ARR_Total(date('Y', strtotime('-1 year')));
        $arr = $this->IndicatorsService_model->ARR_Total(date('Y'));

        $this->data['arr_ano_aterior'] = $arr_ano_aterior;
        $this->data['arr'] = $arr;

        $this->data['percentualCrescimento'] = $this->crescimento_ano_anterior($arr_ano_aterior, $arr);
        $this->data['arr_chart'] = $this->IndicatorsService_model->ARR_Grafico(date('Y'));
        $this->data['arr_chart_group'] = $this->IndicatorsService_model->ARR_AgrupadoPorGrupo(date('Y'));
        $this->data['active_customers'] = $this->IndicatorsService_model->activeCustomers();
        $this->data['inactive_customers'] = $this->IndicatorsService_model->inactiveCustomers();
        $this->data['customers_last_year'] = $this->IndicatorsService_model->customersLastYear(date('Y'));

        $this->page_construct('indicators/index', $meta, $this->data);
    }

    private function crescimento_ano_anterior($arr_ano_aterior, $arr)
    {
        if ($arr_ano_aterior > 0) {
            $percentualCrescimento = (($arr - $arr_ano_aterior) / $arr_ano_aterior) * 100;
        } else {
            $percentualCrescimento = 0; // Caso o valor do ano anterior seja zero
        }

        return $percentualCrescimento;
    }

}?>