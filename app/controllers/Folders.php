<?php defined('BASEPATH') or exit('No direct script access allowed');

class Folders extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $plugsignSettings = $this->site->getPlugsignSettings();

        if (!$plugsignSettings->active || !$plugsignSettings->token){
            $this->session->set_flashdata('error', lang('plugsign_not_configured'));
            redirect('welcome');
        }

        $this->lang->load('documents', $this->Settings->user_language);

        //repository
        $this->load->model('repository/FolderRepository_model', 'FolderRepository_model');
        $this->load->model('repository/DocumentRepository_model', 'DocumentRepository_model');

        //service
        $this->load->model('service/FolderService_model', 'FolderService_model');

        //model
        $this->load->model('model/Folder_model', 'Folder_model');
        $this->load->model('model/Document_model', 'Document_model');
        $this->load->model('model/Signatures_model', 'Signatures_model');

        //dto
        $this->load->model('dto/DocumentFilterDTO_model', 'DocumentFilterDTO_model');
        $this->load->model('dto/FolderFilterDTO_model', 'FolderFilterDTO_model');

        $this->load->library('form_validation');
    }

    function index() {

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('folders')));
        $meta = array('page_title' => lang('folders'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('folders/index', $meta, $this->data);
    }

    function folders_search()
    {
        $filter = new FolderFilterDTO_model();

        $filter->statusDocument = $this->input->get('statusDocument');
        $filter->biller_id = $this->input->get('billerID');
        $filter->strFolder = $this->input->get('strFolder');

        $this->data['folders'] = $this->FolderRepository_model->getAll($filter);

        $this->load->view($this->theme . 'folders/folders_search', $this->data);
    }

    function folder_open($folderID) {

        $folder = new Folder_model($folderID);

        $this->data['folder'] =  $folder;

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => base_url().'/folders', 'page' => lang('folders')),
            array('link' => '#', 'page' => $folder->name)
        );

        $meta = array('page_title' => lang('folders'), 'bc' => $bc);

        if ($this->input->get('statusDocument')) {
            $this->data['statusDocument'] =$this->input->get('statusDocument');
        }

        if ($this->input->get('billerID')) {
            $this->data['billerID'] =$this->input->get('billerID');
        }

        if ($this->input->get('strFolder')) {
            $this->data['strFolder'] =$this->input->get('strFolder');
        }

        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('folders/folder_open', $meta, $this->data);
    }

    public function documents_search($folderID)
    {
        $filter = new DocumentFilterDTO_model();
        $filter->statusDocument = $this->input->get('statusDocument');
        $filter->biller_id = $this->input->get('billerID');
        $filter->strSignatory = $this->input->get('strFolder');
        $filter->folder_id = $folderID;

        $this->data['documents'] = $this->DocumentRepository_model->getAll($filter);

        $this->load->view($this->theme . 'folders/documents_search', $this->data);
    }

    public function addFolder() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $folder = new Folder_model();
            $folder->name = $this->input->post('name');

        } elseif ($this->input->post('addDocument')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        try {

            if ($this->form_validation->run() == true && $this->FolderService_model->addFolder($folder)) {
                $this->session->set_flashdata('message', lang("folder_successfully_added"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'folders/addFolder', $this->data);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function editFolder($folderID) {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        $folder = new Folder_model($folderID);

        if ($this->form_validation->run() == true) {

            $folder->name = $this->input->post('name');

        } elseif ($this->input->post('editFolder')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        try {

            if ($this->form_validation->run() == true && $this->FolderService_model->editFolder($folder)) {
                $this->session->set_flashdata('message', lang("folder_successfully_edited"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();

                $this->data['folder'] = $folder;
                $this->load->view($this->theme . 'folders/editFolder', $this->data);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function deleteFolder($folderID)
    {
        try {

            $this->FolderService_model->deleteFolder($folderID);

            $this->session->set_flashdata('message', lang("folder_deleted_successfully"));

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) {
                die();
            }
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

}?>