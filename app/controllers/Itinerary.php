<?php defined('BASEPATH') or exit('No direct script access allowed');

class Itinerary extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('itinerary', $this->Settings->user_language);
        $this->load->library('form_validation');

        //repository
        $this->load->model('repository/TipoTrajetoVeiculoRepository_model', 'TipoTrajetoVeiculoRepository_model');

        $this->load->model('itinerary_model');
        $this->load->model('products_model');
    }

    public function index() {

        $this->data['products'] = $this->products_model->getAllTours();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('order_service')));
        $meta = array('page_title' => lang('order_service'), 'bc' => $bc);
        $this->page_construct('itinerary/index', $meta, $this->data);
    }

    function add()
    {
        $this->form_validation->set_rules('dataEmbarque', lang("date_itinerary"), 'required');
        $this->form_validation->set_rules('horaEmbarque', lang("horaEmbarque"), 'required');
        $this->form_validation->set_rules('fornecedor', lang("provider_service"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->site->getReference('os');

            $data = array(
                'reference_no'   => $reference,
                'dataEmbarque' => $this->input->post('dataEmbarque'),
                'horaEmbarque' => $this->input->post('horaEmbarque'),
                'fornecedor' => $this->input->post('fornecedor'),
                'motorista' => $this->input->post('motorista'),
                'guia' => $this->input->post('guia'),
                'veiculo' => $this->input->post('veiculo'),
                'tipo_transporte_id' => $this->input->post('tipo_transporte_id'),
                'tipo_trajeto_id'   => $this->input->post('tipo_trajeto_id'),
                'retornoPrevisto'   => $this->input->post('retornoPrevisto'),
                'product_id'   => $this->input->post('product_id'),
                'note' => $this->input->post('note'),
                'executivo'   => $this->input->post('executivo'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('user_id'),
            );

        } elseif ($this->input->post('submit')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("itinerary");
        }
        if ($this->form_validation->run() == true) {
            $id = $this->itinerary_model->addItinerary($data);
            $this->session->set_flashdata('message', lang("itinerary_added"));
            redirect('itinerary/mount/'.$id);
        } else {
            $this->data['error']        = validation_errors();
            $this->data['modal_js']     = $this->site->modal_js();
            $this->data['products']     = $this->products_model->getAllTours();
            $this->data['tiposTrajeto'] = $this->TipoTrajetoVeiculoRepository_model->getAll();

            $this->load->view($this->theme . 'itinerary/add', $this->data);
        }
    }

    public function edit($id) {

        if ($this->input->get('id'))  {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('dataEmbarque', lang("date_itinerary"), 'required');
        $this->form_validation->set_rules('horaEmbarque', lang("horaEmbarque"), 'required');
        $this->form_validation->set_rules('fornecedor', lang("provider_service"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'dataEmbarque' => $this->input->post('dataEmbarque'),
                'horaEmbarque' => $this->input->post('horaEmbarque'),
                'fornecedor' => $this->input->post('fornecedor'),
                'motorista' => $this->input->post('motorista'),
                'guia' => $this->input->post('guia'),
                'veiculo' => $this->input->post('veiculo'),
                'tipo_transporte_id' => $this->input->post('tipo_transporte_id'),
                'tipo_trajeto_id'   => $this->input->post('tipo_trajeto_id'),
                'retornoPrevisto'   => $this->input->post('retornoPrevisto'),
                'product_id'   => $this->input->post('product_id'),
                'executivo'   => $this->input->post('executivo'),
                'note' => $this->input->post('note'),
                'update_at' => date('Y-m-d H:i:s'),
            );
        } elseif ($this->input->post('editEtinerary')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("itinerary");
        }

        if ($this->form_validation->run() == true && $this->itinerary_model->updateItinerary($id, $data)) {
            $this->session->set_flashdata('message', lang("itinerary_update"));
            redirect('itinerary/mount/'.$id);
        } else {
            $this->data['error']        = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['itinerary']    = $this->site->getItineraryByID($id);
            $this->data['tiposTrajeto'] = $this->TipoTrajetoVeiculoRepository_model->getAll();
            $this->data['products']     = $this->products_model->getAllTours();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'itinerary/edit', $this->data);
        }
    }

    public function mount($id) {

        $this->data['products']     = $this->products_model->getAllTours();
        $this->data['itinerary']    = $this->site->getItineraryByID($id);
        $this->data['tiposTrajeto'] = $this->TipoTrajetoVeiculoRepository_model->getAll();

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('itinerary'), 'page' => lang('order_service')),
            array('link' => '#', 'page' => lang('set_up_itinerary'))
        );

        $meta = array('page_title' => lang('set_up_itinerary'), 'bc' => $bc);
        $this->page_construct('itinerary/mount', $meta, $this->data);
    }

    public function getItineraries() {

        $this->load->library('datatables');
        $edit_link      = anchor('itinerary/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_itinerary'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $mount_link     = anchor('itinerary/mount/$1', '<i class="fa fa-car"></i> ' . lang('mount_itinerary'), '');
        $report_link     = anchor('itinerary/report_os/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('report_itinerary'), 'target="_blank"');
        $report_details_link     = anchor('itinerary/report_detail_itinerary/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('report_detail_itinerary'), 'target="_blank"');

        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_itinerary") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('itinerary/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('delete_itinerary') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $mount_link . '</li>
                    <li class="divider"></li>
                    <li>' . $report_link . '</li>
                    <li>' . $report_details_link . '</li>
                    <li class="divider"></li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("itinerario.id as id,
                    itinerario.reference_no,
                    concat(sma_itinerario.dataEmbarque, ' ', sma_itinerario.horaEmbarque) as date, 
                    f.name as fornecedor,
                    tipo_transporte_rodoviario.name as veiculo,
                    m.name as motorista,
                    g.name as guia,
                    products.name,
                    itinerario.status")
            ->from('itinerario');

        $this->datatables->join('companies f', 'f.id = itinerario.fornecedor', 'left');
        $this->datatables->join('companies m', 'm.id = itinerario.motorista', 'left');
        $this->datatables->join('companies g', 'g.id = itinerario.guia', 'left');
        $this->datatables->join('tipo_transporte_rodoviario', 'tipo_transporte_rodoviario.id = itinerario.tipo_transporte_id', 'left');
        $this->datatables->join('products', 'products.id = itinerario.product_id', 'left');

        if ($this->input->post('filterDateFrom')) {
            $this->datatables->where('itinerario.dataEmbarque', $this->input->post('filterDateFrom'));
        }

        if ($this->input->post('filterProducts')) {
            $this->datatables->where('itinerario.product_id', $this->input->post('filterProducts'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getTours()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("sale_items.id as id,
                    DATE_FORMAT(sma_sale_items.hora_embarque, '%H:%i')  as hour, 
                    local_embarque.name as local_embarque,
                    sale_items.product_name as procuct,
                    sale_items.customerClientName as customer,
                    sale_items.subtotal as grand_total, 
                    (((sma_sales.paid*100)/sma_sales.grand_total)/100 * sma_sale_items.subtotal) as paid,
                    sma_sale_items.subtotal - (((sma_sales.paid*100)/sma_sales.grand_total)/100 * sma_sale_items.subtotal) as balance,
                    sma_sales.payment_status, 
                    sale_items.sale_id")
            ->from('sale_items');

        $this->datatables->join('sales', 'sales.id = sale_items.sale_id');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId');
        $this->datatables->join('local_embarque', 'local_embarque.id = sale_items.localEmbarque', 'left');

        $this->datatables->where('sale_items.id not in (select sale_item from sma_itinerario_items) ');

        if ($this->input->post('filterProduct')) {
            $this->datatables->where("sale_items.product_id", $this->input->post('filterProduct'));
        }

        if ($this->input->post('filterDateTour')) {
            $this->datatables->where("sma_agenda_viagem.dataSaida=", $this->input->post('filterDateTour'));
        }

        if ($this->input->post('filterHourTour')) {
            $this->datatables->where("sma_agenda_viagem.horaSaida>=", $this->input->post('filterHourTour'));
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a onclick=\"addItem('$1');\" style='cursor: pointer;'><i class=\"fa fa-plus\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    public function getItens($itinerario_id)
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("itinerario_items.id as id,
                    DATE_FORMAT(sma_sale_items.hora_embarque, '%H:%i') as hour, 
                    local_embarque.name as local_embarque,
                    sale_items.product_name as procuct,
                    sale_items.customerClientName as customer, 
                    sale_items.sale_id")
            ->from('itinerario_items');

        $this->datatables->join('sale_items', 'sale_items.id = itinerario_items.sale_item');
        $this->datatables->join('sales', 'sales.id = sale_items.sale_id');
        $this->datatables->join('local_embarque', 'local_embarque.id = sale_items.localEmbarque', 'left');

        $this->datatables->where('itinerario_items.itineario', $itinerario_id);

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a onclick=\"removeItem('$1');\" style='cursor: pointer;' ><i class=\"fa fa-trash-o\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_item() {

        $itinerary_id   = $this->input->get('itinerary_id');
        $sale_item      = $this->input->get('sale_item');

        $data_item = array(
          'sale_item' => $sale_item,
        );

        return $this->itinerary_model->addItem($itinerary_id, $data_item);
    }

    public function remove_item() {
        $itinerary_item = $this->input->get('itinerary_item');

        return $this->itinerary_model->removeItem($itinerary_item);
    }

    public function delete($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        try {
            if ($this->itinerary_model->delete($id)) {
                if ($this->input->is_ajax_request()) {
                    echo lang("itinerary_deleted");
                }
                die();
            }
            redirect('itinerary');

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) echo  $exception->getMessage();
            die();
        }
    }

    function report_os($id)
    {
        $this->data['itinerary'] = $this->itinerary_model->getById($id);
        $this->data['itens']     = $this->itinerary_model->getItens($id);
        $this->data['biller']    = $this->site->getCompanyByID($this->Settings->default_biller);

        $name = 'RELATÓRIO GERAL DE PASSAGEIROS.pdf';

        $html = $this->load->view($this->theme . 'itinerary/report_itinerary', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function report_detail_itinerary($id)
    {
        $this->data['itinerary'] = $this->itinerary_model->getById($id);
        $this->data['itens']     = $this->itinerary_model->getItens($id, true);
        $this->data['biller']    = $this->site->getCompanyByID($this->Settings->default_biller);

        $name = 'RELATÓRIO GERAL DE ORDEM DE SERVIÇO DETALHADO.pdf';

        $html = $this->load->view($this->theme . 'itinerary/report_detail_itinerary', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
        //$this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>', $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');

    }
}