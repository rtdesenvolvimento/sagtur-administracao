<?php defined('BASEPATH') or exit('No direct script access allowed');

class Cart extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('sales_model');
    }

    public function index() {
        $this->view($this->Settings->default_biller);
    }

    function calendar() {
        $filter = new ProgramacaoFilter_DTO_model();
        $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);


        $this->load->view($this->theme . 'website/calendar', $this->data);
    }

    function view($product_id = null) {
        try {

            //if ($vendedorId == null) $vendedorId = $this->Settings->default_biller;

            $vendedor   = $this->site->getCompanyByID(44017);
            $user       = $this->site->getUserByBillerActive(44017);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $this->data['vendedor'] = $vendedor;
            $this->data['product']  = $this->site->getProductByID($product_id);
            $this->data['images']   = $this->products_model->getProductPhotosLimit($product_id, $this->Settings->quantity_thumbs);

            $this->load->view($this->theme . 'website/view', $this->data);

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

}?>