<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Roomlist extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner && !$this->Admin) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');

        $this->lang->load('sales', $this->Settings->user_language);
        $this->lang->load('reports', $this->Settings->user_language);

        $this->load->model('Roomlisttipohospedagem_model');
        $this->load->model('sales_model');
        $this->load->model('Roomlist_model');

        $this->load->model('service/RoomListService_model', 'RoomListService_model');
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');

    }

    function index()
    {
        show_404();
    }

    function montar($roomlistId) {

        $roomList = $this->Roomlist_model->getById($roomlistId);
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($roomList->programacao_id);

        $this->data['roomList'] = $roomList;
        $this->data['productId'] = $programacao->produto;
        $this->data['programacaoId'] = $programacao->id;
        $this->data['tiposHospedagem'] =  $this->ProdutoRepository_model->getTipoHospedagemRodoviarioSomenteAtivo($programacao->produto);
        $this->data['tiposHospedagemHotel'] =  $this->TipoQuartoRepository_model->getTiposQuarto();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('roomlist'), 'bc' => $bc);

        $this->page_construct('roomlist/index', $meta, $this->data);
    }

    function montar_old($roomlistId) {

        $roomList = $this->Roomlist_model->getById($roomlistId);
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($roomList->programacao_id);

        $this->data['roomList'] = $roomList;
        $this->data['productId'] = $programacao->produto;
        $this->data['programacaoId'] = $programacao->id;
        $this->data['tiposHospedagem'] =  $this->ProdutoRepository_model->getTipoHospedagemRodoviarioSomenteAtivo($programacao->produto);
        $this->data['tiposHospedagemHotel'] =  $this->TipoQuartoRepository_model->getTiposQuarto();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('roomlist'), 'bc' => $bc);

        $this->page_construct('roomlist/montar_old', $meta, $this->data);
    }

    function salvarTipoHospedagem() {

        $tipoHospedagemId = $this->input->get('tipoHospedagemId');
        $roomlist_id = $this->input->get('roomlist_id');

        $data = array(
            'room_list_id' => $roomlist_id,
            'tipo_hospedagem_id' => $tipoHospedagemId
        );

        $this->sma->send_json($this->RoomListService_model->salvarTipoHospedagem($data));
    }

    function salvarHospede() {

        $roomListTipoHospedagemId = $this->input->get('roomListTipoHospedagem_id');
        $customerId = $this->input->get('customer');
        $itemId = $this->input->get('itemId');
        $sale_id = $this->input->get('sale_id');

        $data = array(
            'room_list_hospedagem_id' => $roomListTipoHospedagemId,
            'customer_id' => $customerId,
            'itemId' => $itemId,
            'sale_id' => $sale_id,
        );

        $this->sma->send_json($this->RoomListService_model->salvarHospede($data));
    }

    function excluirHospede($id) {
        $this->sma->send_json( array('retorno'=>$this->RoomListService_model->excluirHospede($id)));
    }

    function excluirTipoHospedagem($id) {
        $this->sma->send_json( array('retorno'=>$this->RoomListService_model->excluirTipoHospedagem($id)));
    }

    function relatorio($roomlistId) {

        $roomList = $this->Roomlist_model->getById($roomlistId);
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($roomList->programacao_id);

        if (!$roomlistId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $hospedes = $this->Roomlisthospede_model->getAllHospedes($roomlistId);
        $product = $this->site->getProductByID($programacao->produto);

        $this->data['roomList'] = $roomList;
        $this->data['hospedes'] = $hospedes;
        $this->data['programacao'] = $programacao;
        $this->data['product'] = $product;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'Relatório de Rooming List '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'roomlist/relatorio', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_old($roomlistId) {

        $roomList = $this->Roomlist_model->getById($roomlistId);
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($roomList->programacao_id);

        if (!$roomlistId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $hospedes = $this->Roomlisthospede_model->getAllHospedes_old($roomlistId);
        $product = $this->site->getProductByID($programacao->produto);

        $this->data['roomList'] = $roomList;
        $this->data['hospedes'] = $hospedes;
        $this->data['programacao'] = $programacao;
        $this->data['product'] = $product;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'Relatório de Rooming List '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'roomlist/relatorio', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function adicionarRoomList($programacaoId)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'fornecedor' => $this->input->post('fornecedor'),
                'programacao_id' =>$programacaoId,
            );
        } elseif ($this->input->post('adicionarRoomList')) {
            $this->session->set_flashdata('error', validation_errors());

            redirect("products");
        }

        if ($this->form_validation->run() == true) {
            $roomListId = $this->RoomListService_model->adicionarRoomList($data);

            $this->session->set_flashdata('message', lang("room_list_adicionado_com_sucesso"));

            redirect("roomlist/montar/".$roomListId);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['programacaoId'] = $programacaoId;
            $this->load->view($this->theme . 'roomlist/adicionarRoomList', $this->data);
        }

    }

    function editarRoomList($id = NULL, $programacaoId = NULL)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'note' =>  $this->input->post('note'),
                'fornecedor' => $this->input->post('fornecedor'),
                'programacao_id' =>$programacaoId,
            );
        } elseif ($this->input->post('editarRoomList')) {
            $this->session->set_flashdata('error', validation_errors());

            redirect("products");
        }

        if ($this->form_validation->run() == true &&
            $this->RoomListService_model->editarRoomList($id, $data)) {
            $this->session->set_flashdata('message', lang("room_list_editado_com_sucesso"));

            redirect("roomlist/montar/".$id);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['programacaoId'] = $programacaoId;
            $this->data['roomList'] = $this->Roomlist_model->getById($id);
            $this->load->view($this->theme . 'roomlist/editarRoomList', $this->data);
        }
    }

    function excluir($id = NULL) {
        if ($this->RoomListService_model->excluir($id)) {

            $this->session->set_flashdata('message', lang("room_list_excluido_com_sucesso"));

            redirect('products');
        }
    }
}
