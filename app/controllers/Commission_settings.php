<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Commission_settings extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('welcome');
        }

        $this->lang->load('settings', $this->Settings->user_language);

        $this->load->model('settings_model');

        $this->load->library('form_validation');
    }

    function index()
    {

        $this->form_validation->set_rules('despesa_fechamento_id', lang("despesa"), 'required');
        $this->form_validation->set_rules('tipo_cobranca_fechamento_id', lang("tipo_cobranca"), 'required');

        if ($this->form_validation->run() == true) {

            $data = array(
                'dia_fechamento_comissao' => $_POST['dia_fechamento_comissao'],
                'despesa_fechamento_id' => $_POST['despesa_fechamento_id'],
                'tipo_cobranca_fechamento_id' => $_POST['tipo_cobranca_fechamento_id'],
            );
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateCommissionSetting($data)) {
            $this->session->set_flashdata('message', lang('setting_updated'));
            redirect("commission_settings");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('commission_settings')));
            $meta = array('page_title' => lang('commission_settings'), 'bc' => $bc);

            $this->data['despesas'] = $this->site->getAllDespesasSuperiores();
            $this->data['tiposCobranca'] = $this->site->getAllTiposCobranca(false, true);

            $this->page_construct('commission_settings/index', $meta, $this->data);
        }
    }


}