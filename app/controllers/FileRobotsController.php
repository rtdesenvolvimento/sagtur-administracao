<?php defined('BASEPATH') OR exit('No direct script access allowed');

class FileRobotsController extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function getRobots() {

        $path = 'assets/uploads/robots.txt';

        if (file_exists($path)) {
            $contents = file_get_contents($path);
            header('Content-Type: text/plain');
            echo $contents;
        } else {
            echo 'File not found';
        }
    }
}