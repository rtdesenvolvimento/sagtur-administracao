<?php

class Assistants extends MY_Controller
{
    private $apiKey = "pcsk_6RmbXY_AHeAebyGYpKVSRu3mShcnvCneasuitJeRyWryVsdXhJWCaFFGC7vrikpsu8dno2";
    private $endpoint = "https://prod-1-data.ke.pinecone.io/assistant/chat/sagtur-assistant";

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('welcome');
        }

        $this->lang->load('assistants', $this->Settings->user_language);
    }

    public function index()
    {
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('assistant')));
        $meta = array('page_title' => lang('assistant'), 'bc' => $bc);
        $this->page_construct('assistants/chat_view', $meta, $this->data);

    }

    public function sendMessage()
    {
        $message = $this->input->get('message');

        if (isset($message)) {
            // Monta o payload
            $payload = [
                "messages" => [
                    [
                        "role" => "user",
                        "content" => $message
                    ]
                ],
                "stream" => false,
                "model" => "gpt-4o"
            ];

            // Realiza a requisição cURL
            $curl = curl_init($this->endpoint);

            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_POST => true,
                CURLOPT_HTTPHEADER => [
                    "Api-Key: {$this->apiKey}",
                    "Content-Type: application/json"
                ],
                CURLOPT_POSTFIELDS => json_encode($payload)
            ]);

            $response = curl_exec($curl);

            if (curl_errno($curl)) {
                $error = curl_error($curl);
                curl_close($curl);
                return $this->response->setStatusCode(ResponseInterface::HTTP_INTERNAL_SERVER_ERROR)
                    ->setJSON(['error' => "Request Error: $error"]);
            }

            curl_close($curl);

            return $this->sma->send_json($response);
        } else {
            return $this->sma->send_json(array('error' => 'No message provided.'));
        }
    }
}
