<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contract_settings extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('welcome');
        }

        $this->load->library('form_validation');

        $this->load->model('Contractsettings_model');

        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');

        $this->lang->load('contract_settings', $this->Settings->user_language);

    }

    function index()
    {

        $this->form_validation->set_rules('note', lang('note'), 'trim');

        if ($this->form_validation->run() == true) {

            $gerar_contrato_faturamento_venda = $this->input->post('gerar_contrato_faturamento_venda');

            if ($gerar_contrato_faturamento_venda == 1) {
                $enviar_contrato_signatarios_venda = $this->input->post('enviar_contrato_signatarios_venda');
                $auto_assinar_customer_contrato = $this->input->post('auto_assinar_customer_contrato');
            } else {
                $enviar_contrato_signatarios_venda = 0;
                $auto_assinar_customer_contrato = 0;
            }

            $data = array(
                'monitorar_validade' => $_POST['monitorar_validade'],
                'validade_meses' => $_POST['validade_meses'],
                'auto_destruir_solicitacao' => $_POST['auto_destruir_solicitacao'],
                'validade_auto_destruir_dias' => $_POST['validade_auto_destruir_dias'],
                'solicitar_selfie' => $_POST['solicitar_selfie'],
                'solicitar_documento' => $_POST['solicitar_documento'],
                'signature_type' => $_POST['signature_type'],
                'is_observadores' => $_POST['is_observadores'],
                'observadores' => $_POST['observadores'],
                'enviar_sequencial' => $_POST['enviar_sequencial'],
                'contract_id' => $_POST['contract_id'],
                'gerar_contrato_faturamento_venda' => $gerar_contrato_faturamento_venda,
                'enviar_contrato_signatarios_venda' => $enviar_contrato_signatarios_venda,
                'auto_assinar_customer_contrato' => $auto_assinar_customer_contrato,
                'auto_assinar_biller_contrato' => $_POST['auto_assinar_biller_contrato'],
                'default_biller_contract' => $_POST['default_biller_contract'],
                'gerar_contrato_faturamento_venda_site' => $_POST['gerar_contrato_faturamento_venda_site'],
                'cancel_contract_by_sale' => $_POST['cancel_contract_by_sale'],
                'note' => $_POST['note'],
            );
        }

        if ($this->form_validation->run() == true && $this->Contractsettings_model->updateContractSetting($data)) {
            $this->session->set_flashdata('message', lang('setting_updated'));
            redirect("contract_settings");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('contract_settings')));
            $meta = array('page_title' => lang('contract_settings'), 'bc' => $bc);

            $this->data['contracts'] = $this->ContractRepository_model->getAll();
            $this->data['billers'] = $this->site->getAllCompanies('biller');

            $this->page_construct('contract_settings/index', $meta, $this->data);
        }
    }

    public function plugsign()
    {
        $this->form_validation->set_rules('active', $this->lang->line('activate'), 'trim');

        if ($this->form_validation->run() == true) {
            $data = array(
                'active' => $this->input->post('active'),
                'token' => $this->input->post('account_token_plugsign', false),
            );
        }

        if ($this->form_validation->run() == true && $this->Contractsettings_model->updatePlugsign($data)) {
            $this->session->set_flashdata('message', $this->lang->line('plugsign_setting_updated'));
            redirect("contract_settings/plugsign");
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['plugsign'] = $this->Contractsettings_model->getPlugsignSettings();

            $bc = array(
                array('link' => base_url(), 'page' => lang('home')),
                array('link' => site_url('system_settings'), 'page' => lang('contract_settings')),
                array('link' => '#', 'page' => lang('plugsign_settings'))
            );

            $meta = array('page_title' => lang('plugsign_settings'), 'bc' => $bc);
            $this->page_construct('contract_settings/plugsign', $meta, $this->data);
        }
    }

}