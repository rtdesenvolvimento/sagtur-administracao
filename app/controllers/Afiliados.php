<?php defined('BASEPATH') or exit('No direct script access allowed');

class Afiliados extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }


    }

    function index($action = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('afiliados')));
        $meta = array('page_title' => lang('afiliados'), 'bc' => $bc);
        $this->page_construct('afiliados/index', $meta, $this->data);
    }

    function getAfiliados()
    {
        $this->sma->checkPermissions('index');
        $this->load->library('datatables');
        $this->datatables
            ->select("id, 
            company, 
            name, email,
            concat(phone,' ',cf5, ' ' , telefone_emergencia) as phone , 
            city, customer_group_name, 
            vat_no, deposit_amount, 
            award_points")
            ->from("companies")
            ->where('group_name', 'customer')
            ->add_column("Actions", "<div class=\"text-center\"> <a class=\"tip\" title='" . lang("edit_customer") . "' href='" . site_url('customers/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> </div>", "id");

        echo $this->datatables->generate();
    }


}?>