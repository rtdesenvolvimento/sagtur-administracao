<?php defined('BASEPATH') or exit('No direct script access allowed');

class Appview extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //services
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/AnalyticalService_model', 'AnalyticalService_model');
        $this->load->model('service/BusService_model', 'BusService_model');

        //repositories
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');
        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');

        $this->load->model('settings_model');
        $this->load->model('Meiodivulgacao_model');

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');
        $this->load->model('dto/AnalyticalDTO_model', 'AnalyticalDTO_model');
    }

    public function open($id, $vendedorId)
    {
        try {

            $programacao = $this->AgendaViagemService_model->getProgramacaoById($id);
            $vendedor    = $this->site->getCompanyByID($vendedorId);
            $user        = $this->site->getUserByBillerActive($vendedorId);

            if (!$programacao) {
                $this->show_page_error('Produto não encontrado', 'Produto não encontrado');
            }

            if (!$programacao->active) {
                $this->show_page_error('A Data para Este Produto está inativo no momento', 'Produto fora da venda.');
            }

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $produto =  $this->site->getProductByID($programacao->getProduto());

            if (!$produto->enviar_site) {
                //show_error('Este produto não está mais dipónivel para compra', 404, 'Produto fora da venda.');//todo futurametne vamos criar uma flag para deixar o link disponivel apenas privado
            }

            if (!$produto->active) {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            if ($produto->unit == 'Arquivado') {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $this->show_page_error('As vendas para esse produto já foi encerrado', 'Produto fora da venda.');
            }

            if ($produto->isTaxasComissao) {
                $this->data['tiposCobranca'] = $this->site->getAllFormasPagamentoProdutoExibirLinkCompra($produto->id);
            } else {
                $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaExibirLinkCompra();
            }

            $transporteDisponivel = $this->BusService_model->getPlantaDisponivelMarcacao($programacao->getProduto(), $id);

            $this->data['configuracaoGeral']    = $this->site->get_setting();
            $this->data['categoria']            = $this->site->getCategoryById($produto->category_id);
            $this->data['opcionais']            = $this->site->getProdutosAdicionais($programacao->getProduto());
            $this->data['meiosDivulgacao']      = $this->Meiodivulgacao_model->getAllActive();
            $this->data['embarques']            = $this->ProdutoRepository_model->getLocaisEmbarque($programacao->getProduto());
            $this->data['tiposHospedagem']      = $this->ProdutoRepository_model->getTipoHospedagem($programacao->getProduto());
            $this->data['configPagSeguro']      = $this->settings_model->getPagSeguroSettings();
            $this->data['configMercadoPago']    = $this->settings_model->getMercadoPagoSettings();
            $this->data['faixaEtariaValores']   = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto());
            $this->data['menusPai']             = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']            = $this->MenuRepository_model->isPhotosGallery();
            $this->data['sessionCode']          = $this->sma->getTokenPagSeguro();

            $this->data['programacao']          = $programacao;
            $this->data['vendedor']             = $vendedor;
            $this->data['produto']              = $produto;
            $this->data['transporteDisponivel'] = $transporteDisponivel;

            if ($transporteDisponivel) {
                if ($transporteDisponivel->automovel_id) {
                    $cobrancaExtraConfig = $this->TipoTransporteRodoviarioRepository_model->getTransportesRodoviarioProduto($produto->id, $transporteDisponivel->id);
                    $rotulos = $this->BusRepository_model->getlRotuloCobrancaExtra($cobrancaExtraConfig->configuracao_assento_extra_id);
                    $this->data['rotulos'] =  $rotulos;
                }
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $filter->produto = $produto->id;
            $filter->enviar_site = false;
            //$filter->group_by_date = true;

            $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);

            if ($this->Settings->receptive) {

                if ($this->Settings->atividades_recomendadas) {
                    $this->data['destaques'] = $this->getAtividadesRelacionadas($produto->category_id);
                }

                $this->load->view($this->theme . 'website/cart/shopping_cart_dates1', $this->data);
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO

                if ($this->Settings->web_page_caching) {
                    $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
                }

            } else if ($this->Settings->use_product_landing_page) {

                if ($this->Settings->atividades_recomendadas) {
                    $this->data['destaques'] = $this->getAtividadesRelacionadas($produto->category_id);
                }

                $this->load->view($this->theme . 'website/cart/shopping_cart_dates1', $this->data);//TODO ABRE A LANGING PAGE
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO

                if ($this->Settings->web_page_caching) {
                    $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
                }

            } else {
                if ($programacao->getTotalDisponvel() > 0 || $produto->permitirListaEmpera) {
                    $this->load->view($this->theme . 'appcompra/index', $this->data);
                    $this->save_shopping_cart_access($produto->id, $id);//TODO LOG DE ACESSO

                } else {
                    $this->load->view($this->theme . 'appcompra/esgotado', $this->data);
                    $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO
                }
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    private function getAtividadesRelacionadas($category_id)
    {

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->category = $category_id;
        $filter->enviar_site = false;
        $filter->limit = 8;
        $filter->order_by_custom = 'RAND()';
        $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

        return $this->AgendaViagemService_model->getAllProgramacao($filter);

    }

    public function carrinho_compra($id, $vendedorId) {
        try {

            $programacao    = $this->AgendaViagemService_model->getProgramacaoById($id);
            $vendedor       = $this->site->getCompanyByID($vendedorId);
            $user           = $this->site->getUserByBillerActive($vendedorId);

            if (!$programacao) {
                $this->show_page_error('Produto não encontrado', 'Produto não encontrado');
            }

            if (!$programacao->active) {
                $this->show_page_error('A Data para Este Produto está inativo no momento', 'Produto fora da venda.');
            }

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $produto =  $this->site->getProductByID($programacao->getProduto());

            if (!$produto->enviar_site) {
                //show_error('Este produto não está mais dipónivel para compra', 404, 'Produto fora da venda.');//todo futurametne vamos criar uma flag para deixar o link disponivel apenas privado
            }

            if (!$produto->active) {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            if ($produto->unit == 'Arquivado') {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $this->show_page_error('As vendas para esse produto já foi encerrado', 'Produto fora da venda.');
            }

            if ($produto->isTaxasComissao) {
                $this->data['tiposCobranca'] = $this->site->getAllFormasPagamentoProdutoExibirLinkCompra($produto->id);
            } else {
                $this->data['tiposCobranca'] = $this->site->getAllTiposCobrancaExibirLinkCompra();
            }

            $transporteDisponivel = $this->BusService_model->getPlantaDisponivelMarcacao($programacao->getProduto(), $id);

            $this->data['meiosDivulgacao']      = $this->Meiodivulgacao_model->getAllActive();
            $this->data['embarques']            = $this->ProdutoRepository_model->getLocaisEmbarque($programacao->getProduto());
            $this->data['tiposHospedagem']      = $this->ProdutoRepository_model->getTipoHospedagem($programacao->getProduto());
            $this->data['opcionais']            = $this->site->getProdutosAdicionais($programacao->getProduto());
            $this->data['sessionCode']          = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro']      = $this->settings_model->getPagSeguroSettings();
            $this->data['configMercadoPago']    = $this->settings_model->getMercadoPagoSettings();
            $this->data['faixaEtariaValores']   = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto());
            $this->data['configuracaoGeral']    = $this->site->get_setting();
            $this->data['programacao']          = $programacao;
            $this->data['vendedor']             = $vendedor;
            $this->data['produto']              = $produto;
            $this->data['transporteDisponivel'] =  $transporteDisponivel;

            if ($transporteDisponivel) {
                if ($transporteDisponivel->automovel_id) {
                    $cobrancaExtraConfig = $this->TipoTransporteRodoviarioRepository_model->getTransportesRodoviarioProduto($produto->id, $transporteDisponivel->id);
                    $rotulos = $this->BusRepository_model->getlRotuloCobrancaExtra($cobrancaExtraConfig->configuracao_assento_extra_id);
                    $this->data['rotulos'] =  $rotulos;
                }
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $filter->produto = $produto->id;
            $filter->group_by_date = true;
            $filter->enviar_site = false;

            $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);

            if ($programacao->getTotalDisponvel() > 0 || $produto->permitirListaEmpera) {
                $this->load->view($this->theme . 'appcompra/index', $this->data);
                $this->save_shopping_cart_access($produto->id, $id);//TODO LOG DE ACESSO
            } else {
                $this->load->view($this->theme . 'appcompra/esgotado', $this->data);
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function product_lookup($description, $vendedorId) {

        $uid = $this->searh_pacote_by_date($description);

        $this->open($uid, $vendedorId);
    }

    public function shopping_cart($description, $vendedorId) {

        $uid = $this->searh_pacote_by_date($description);

        $this->carrinho_compra($uid, $vendedorId);
    }

    private function searh_pacote_by_date($uri)
    {
        if (strpos($uri, 'uid') !== false) {
            preg_match('/uid(\d+)/', $uri, $matches);
            $uid = $matches[1];
        } elseif (strpos($uri, 'data') !== false) {

            preg_match('/data_(\d{2})-(\d{2})-(\d{4})h(\d{2}_\d{2})p(\d+)/', $uri, $matches);
            $data = $matches[3] . '-' . $matches[2] . '-' . $matches[1];
            $hora = str_replace('_', ':', $matches[4]);
            $produtoID = $matches[5];

            $programacao = $this->AgendaViagemRespository_model->getProgramacaoByData($produtoID, $data, $hora);

            if (empty($programacao)) {
                $programacao = $this->AgendaViagemRespository_model->getProximaProgramcaoByData($produtoID);
            }

            if (empty($programacao)) {
                $this->show_page_error('Ops! Problema para abrir esse produto. Verifique com Agência!', 'Não encontramos a data vinculada a esse produto.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $programacao = $this->AgendaViagemRespository_model->getProximaProgramcaoByData($produtoID);
            }

            $uid =  $programacao->id;

        } else {
            $uid = $uri;
        }

        return $uid;
    }

    private function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

    private function record_product_access($product_id = null, $programacao_id = null) {
        $analytical = new AnalyticalDTO_model();
        $analytical->type= AnalyticalDTO_model::PAGE_VIEW_PRODUCT;
        $analytical->product_id = $product_id;
        $analytical->programacao_id = $programacao_id;
        $this->AnalyticalService_model->registry_access($analytical);
    }

    private function save_shopping_cart_access($product_id = null, $programacao_id = null) {
        $analytical = new AnalyticalDTO_model();
        $analytical->type= AnalyticalDTO_model::PRODUCT_ADDED_CART;
        $analytical->product_id = $product_id;
        $analytical->programacao_id = $programacao_id;
        $this->AnalyticalService_model->registry_access($analytical);
    }

}