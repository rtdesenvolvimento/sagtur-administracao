<?php defined('BASEPATH') or exit('No direct script access allowed');

class Checkins extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //service
        $this->load->model('service/CheckinService_model', 'CheckinService_model');

        //repository
        $this->load->model('repository/CheckinRepository_model', 'CheckinRepository_model');

        //model
        $this->load->model('model/Checkin_model', 'Checkin_model');

        $this->lang->load('checkins', $this->Settings->user_language);
    }

    function index()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('checkins'), 'page' => lang('checkin')),
            array('link' => '#', 'page' => lang('entry')));

        $this->data['products'] = $this->CheckinRepository_model->getAllProducts();

        $this->data['statusFilter'] = 'Confirmado';
        $this->data['anoFilter']    = date('Y');
        $this->data['mesFilter']    = 'PROXIMAS';

        $meta = array('page_title' => lang('checkin'), 'bc' => $bc);
        $this->page_construct('checkins/index', $meta, $this->data);
    }

    function getProgramacoes()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select(
                $this->db->dbprefix('agenda_viagem') . ".id as agendaid,".
                $this->db->dbprefix('products') . ".image as image,".
                $this->db->dbprefix('products') . ".name,".
                "concat(".$this->db->dbprefix('agenda_viagem') . ".dataSaida, ' ' , sma_agenda_viagem.horaSaida) as dataSaida,".
                "(select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status = 'faturada' and sma_sale_items.adicional = 0) as vendas,".
                "(select count(sma_checkin.id) as quantity from sma_checkin where sma_checkin.programacao_id = sma_agenda_viagem.id) as confirmados,".
                "(select count(sma_sale_items.quantity) as quantity from sma_sale_items, sma_sales where sma_sale_items.sale_id = sma_sales.id and sma_sale_items.programacaoId = sma_agenda_viagem.id and sma_sales.sale_status = 'faturada' and sma_sale_items.adicional = 0) - (select count(sma_checkin.id) as quantity from sma_checkin where sma_checkin.programacao_id = sma_agenda_viagem.id) as pendentes
                ", FALSE)
            ->from('agenda_viagem')
            ->join('products', 'agenda_viagem.produto=products.id', 'left');

        $ano = $this->input->post('filter_ano') != null ? $this->input->post('filter_ano')  : date('Y');
        $mes = $this->input->post('filter_mes') != null ? $this->input->post('filter_mes') : 'PROXIMAS';

        $filter_status      = $this->input->post('filter_status');
        $filter_produto     = $this->input->post('filter_produto');
        $filter_start_date  = $this->input->post('filter_start_date');
        $filter_end_date    = $this->input->post('filter_end_date');

        if ($ano) {
            $this->db->where("YEAR(dataSaida)", $ano);
        }

        if ($mes == 'PROXIMAS') {
            $this->db->where("dataSaida>='".date('Y-m-d')."'");
        } else if ($mes != 'all') {
            $this->db->where("MONTH(dataSaida)",$mes);
        }

        if ($filter_start_date) {
            $this->db->where("dataSaida>='".$filter_start_date."'");
        }

        if ($filter_end_date) {
            $this->db->where("dataSaida<='".$filter_end_date."'");
        }

        if ($filter_status) {
            $this->db->where("products.unit", $filter_status);
        }

        if ($filter_produto) {
            $this->db->where("products.id", $filter_produto);
        }

        $this->db->where('viagem', 1);

        $this->db->order_by('dataSaida asc');

        echo $this->datatables->generate();
    }

    function entrance($programacaoId)
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('checkins'), 'page' => lang('checkin')),
            array('link' => '#', 'page' => lang('entrance')));
        $meta = array('page_title' => lang('checkin'), 'bc' => $bc);

        $this->data['programacaoId'] = $programacaoId;
        $this->data['embarques']     = $this->site->getLocaisEmbarque();
        $this->data['transportes']   = $this->site->getTiposTransporteRodoviario();;

        $this->page_construct('checkins/entrance', $meta, $this->data);
    }

    public function getEntrances($programacaoId)
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("sale_items.id as id,
                sma_sale_items.poltronaClient as assento,
                concat('<b>', sma_companies.name,'</b><br/><small>', sma_sale_items.faixaNome, '</small>') as customer,
                 concat((select sma_checkin.id from sma_checkin where sma_checkin.customer_id = sma_sale_items.customerClient and sma_checkin.sale_id = sma_sale_items.sale_id)) as checkin")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->datatables->join('companies', 'companies.id=sale_items.customerClient', 'left');

        $this->datatables->where('sale_items.adicional', false);//NAO MOSTRA ADICIONAIS NESTE RELATORIO
        $this->datatables->where('sale_items.programacaoId', $programacaoId);

        $tipoTransporteId   = $this->input->post('tipo_transporte');
        $localEmbarqueID    = $this->input->post('local_embarque');

        if ($tipoTransporteId) {
            $this->datatables->where("tipoTransporte", $tipoTransporteId);
        }

        if ($localEmbarqueID) {
            $this->datatables->where("localEmbarque", $localEmbarqueID);
        }

        $this->datatables->where("payment_status in ('due','partial','paid') ");

       // $this->db->order_by('sale_items.sale_id');

        echo $this->datatables->generate();
    }

    function checkin($sale_item_id = null)
    {
        if (!$sale_item_id) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $item       = $this->CheckinRepository_model->getItem($sale_item_id);
        $inv        = $this->CheckinRepository_model->getInvoiceByID($item->sale_id);
        $inv_items  = $this->CheckinRepository_model->getAllInvoiceItems($item->sale_id);
        $customer   = $this->site->getCompanyByID($item->customerClient);

        $checked_item = $this->CheckinRepository_model->checked($item->sale_id, $item->customerClient);

        $this->data['checked']  = $checked_item;
        $this->data['item']     = $item;
        $this->data['inv']      = $inv;
        $this->data['customer'] = $customer;
        $this->data['itens']    = $inv_items;

        $this->data['error']    = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->load->view($this->theme . 'checkins/checkin', $this->data);
    }

    function checkin_qrcode($sale_id, $programacaoId)
    {
        if (!$sale_id) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $inv        = $this->CheckinRepository_model->getInvoiceByID($sale_id);
        $inv_items  = $this->CheckinRepository_model->getAllInvoiceItems($sale_id);
        $item       = null;

        foreach ($inv_items as $it) {
            if ($it->customerClient == $inv->customer_id) {
                $item = $it;
            }
        }

        if ($item == null) {
            foreach ($inv_items as $it) {
                $item = $it;
            }
        }

        if ($programacaoId === $item->programacaoId) {

            $customer       = $this->site->getCompanyByID($item->customerClient);
            $checked_item   = $this->CheckinRepository_model->checked($item->sale_id, $item->customerClient);

            if (count($inv_items) == 1 && !$checked_item) {
                $this->data['do_checkin']  = $item;
            } else if (count($inv_items) == 1 && $checked_item) {
                $this->data['already_registered']  = true;
            }

            $this->data['checked']  = $checked_item;
            $this->data['item']     = $item;
            $this->data['inv']      = $inv;
            $this->data['customer'] = $customer;
            $this->data['itens']    = $inv_items;

        } else {
            $this->data['checkin_another_trip'] = true;
        }

        $this->data['error']    = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->load->view($this->theme . 'checkins/checkin', $this->data);
    }


    function checkin_update($sale_item_id = null)
    {
        if (!$sale_item_id) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $item       = $this->CheckinRepository_model->getItem($sale_item_id);
        $inv        = $this->CheckinRepository_model->getInvoiceByID($item->sale_id);
        $inv_items  = $this->CheckinRepository_model->getAllInvoiceItems($item->sale_id);
        $customer   = $this->site->getCompanyByID($item->customerClient);

        $checked_item = $this->CheckinRepository_model->checked($item->sale_id, $item->customerClient);

        $this->data['checked']  = $checked_item;
        $this->data['item']     = $item;
        $this->data['inv']      = $inv;
        $this->data['customer'] = $customer;
        $this->data['itens']    = $inv_items;

        $this->load->view($this->theme . 'checkins/checkin_update', $this->data);
    }

    function checkin_action($id)
    {
        try {

            if (!$id) {
                $this->session->set_flashdata('error', lang('prduct_not_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $item  = $this->CheckinRepository_model->getItem($id);

            $checkinModel = new Checkin_model();
            $checkinModel->checkin_location_id = null;
            $checkinModel->sale_id          = $item->sale_id;
            $checkinModel->customer_id      = $item->customerClient;
            $checkinModel->programacao_id   = $item->programacaoId;
            $checkinModel->type             = Checkin_model::IN;
            $checkinModel->note             = '';

            $resultado = $this->CheckinService_model->checkin($checkinModel);

            if ($resultado) {
                $this->sma->send_json(array('checkin' => true, 'resultado' =>  $this->CheckinRepository_model->getById($resultado)));
            } else {
                $this->sma->send_json(array('checkin' => false, 'resultado' => null));
            }

        } catch(Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function checkin_delete($id)
    {
        if (!$id) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $item  = $this->CheckinRepository_model->getItem($id);

        $checkinModel = new Checkin_model();
        $checkinModel->checkin_location_id = null;
        $checkinModel->sale_id      = $item->sale_id;
        $checkinModel->customer_id  = $item->customerClient;

        $resultado = $this->CheckinService_model->checkin_delete($checkinModel);

        $this->sma->send_json(array('checkin' => $resultado, 'resultado' => null));

    }

}?>