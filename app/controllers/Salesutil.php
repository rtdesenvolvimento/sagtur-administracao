<?php defined('BASEPATH') or exit('No direct script access allowed');

class Salesutil extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        //repository
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/RatingRepository_model', 'RatingRepository_model');
        $this->load->model('repository/CupomDescontoRepository_model', 'CupomDescontoRepository_model');

        //models
        $this->load->model('Meiodivulgacao_model');
        $this->load->model('Roomlist_model');

        $this->lang->load('sales', $this->Settings->user_language);

        $this->data['logo'] = true;
    }

    public function salesFilter($programacaoId = null, $unit = NULL, $ano = NULL, $mes = NULL)
    {

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($ano == null) $ano = date('Y');
        if ($mes == null) $mes = date('m');
        if ($mes == 'Todos') $mes = '';

        if ($programacaoId != null && $programacaoId != 'all') {
            $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

            $programacao = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);

            $this->data['product']          = $this->site->getProductByID($programacao->produto);
            $this->data['adicionais']       = $this->site->getProductComboItems($programacao->produto, null);
            $this->data['transportes']      = $this->ProdutoRepository_model->getTransportesRodoviario($programacao->produto);
            $this->data['programacao']      = $programacao;
            $this->data['programacaoId']    = $programacao->id;
        }

        $flDataVendaDe = date('Y-01-01');
        $flDataVendaAte = date('Y-12-31');

        $this->data['unit'] = $unit;
        $this->data['ano'] = $ano;
        $this->data['mes'] = $mes;

        //$this->data['flDataVendaDe'] = $flDataVendaDe;
        //$this->data['flDataVendaAte'] = $flDataVendaAte;

        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']  = $this->Meiodivulgacao_model->getAll();
        $this->data['productId']        = $programacaoId;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/index', $meta, $this->data);
    }
    public function sale_items($programacaoId = null, $unit = NULL, $ano = NULL, $mes = NULL) {

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($ano == null) $ano = date('Y');
        if ($mes == null) $mes = date('m');
        if ($mes == 'Todos') $mes = '';

        if ($programacaoId != null && $programacaoId != 'all') {

            //service
            $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

            $programacao                    = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);

            $this->data['product']          = $this->site->getProductByID($programacao->produto);
            $this->data['adicionais']       = $this->site->getProductComboItems($programacao->produto, null);
            $this->data['transportes']      = $this->ProdutoRepository_model->getTransportesRodoviario($programacao->produto);

            $this->data['programacao']      = $programacao;
            $this->data['programacaoId']    = $programacao->id;
        }

        $flDataVendaDe = date('Y-01-01');
        $flDataVendaAte = date('Y-12-31');

        $this->data['unit'] = $unit;
        $this->data['ano'] = $ano;
        $this->data['mes'] = $mes;

        //$this->data['flDataVendaDe'] = $flDataVendaDe;
        //$this->data['flDataVendaAte'] = $flDataVendaAte;

        $this->data['billers']          = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']  = $this->Meiodivulgacao_model->getAll();
        $this->data['productId']        = $programacaoId;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/sale_items', $meta, $this->data);
    }


    public function getSales($programacaoId=NULL)
    {

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $fechamento_comissao_link = anchor('salesutil/modal_view_fechamento_commissions/$1', '<i class="fa fa-bar-chart-o"></i> ' . lang('modal_view_fechamento_commissions'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $archived_link = "<a href='#' class='po' title='<b>" . lang("archived_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/archived/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-archive'></i> "
            . lang('archived_sale') . "</a>";

        $gerar_comissao_link = "<a href='#' class='po' title='<b>" . lang("generate_sales_commission") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/generate_sales_commission/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-paypal'></i> "
            . lang('generate_sales_commission') . "</a>";

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                     ';

        $plugsignSetting = $this->site->getPlugsignSettings();

        if ($plugsignSetting->active && $plugsignSetting->token) {


            $action .= '<li class="divider"></li>';

            $action .= '<li>'. anchor('salesutil/open_contract_events/$1', '<i class="fa fa-archive"></i> ' . lang('contract_events'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"') .'</li>';


            $action .= "<li><a href='#' class='po' title='<b>" . lang("generate_sale_contract") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('contracts/generateSaleContract/$1') . "'>"
                . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-pencil'></i> "
                . lang('generate_sale_contract') . "</a></li>";

            $action .= '<li>'. anchor('salesutil/downloadDocument/$1', '<i class="fa fa-download"></i> ' . lang('download_document'), $attrib) .'</li>';
            $action .= '<li>'. anchor('salesutil/to_share_contract/$1', '<i class="fa fa-share-alt"></i> ' . lang('to_share'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"') .'</li>';
            $action .= '<li>'. anchor('salesutil/to_share_validate_contract/$1', '<i class="fa fa-list"></i> ' . lang('to_share_validate'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"') .'</li>';
            $action .= '<li>'. anchor('salesutil/abrirContrato/$1', '<i class="fa fa-eye"></i> ' . lang('abrir_contrato'), $attrib) .'</li>';

        }

        $action .= ' <li class="divider"></li>
                    <li>' . $archived_link . '</li>
                    <li class="divider"></li>
                    <li>' . $gerar_comissao_link . '</li>
                    <li>' . $fechamento_comissao_link . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $filterOpcional = $this->input->post('filterOpcional');
        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $filterStatus = $this->input->post('filterStatus');
        $flDataVendaDe = $this->input->post('flDataVendaDe');
        $flDataVendaAte = $this->input->post('flDataVendaAte');
        $fdivulgacao = $this->input->post('fdivulgacao');

        if ($programacaoId == 'all') {
            $programacaoId = NULL;
        }

        $this->load->library('datatables');

        if ($programacaoId) {
            $this->datatables
                ->select("sma_sales.id as id, 
                    sma_sales.date, 
                    sma_sales.reference_no,
                    sma_sales.vendedor,
                    sma_sales.customer,
                    sma_sales.sale_status, 
                    sma_sales.grand_total, 
                    sma_sales.paid, 
                    (sma_sales.grand_total-sma_sales.paid) as balance,
                    sma_sales.payment_status")
                ->from('sma_sales');

            $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
            $this->datatables->where('sale_items.programacaoId', $programacaoId);

            if ($filterOpcional) {
                $this->datatables->where('product_id', $filterOpcional);
            }

            $this->datatables->group_by('sales.id');
        } else {
            $this->datatables
                ->select("sales.id as id, 
                    date, 
                    reference_no, 
                    vendedor, 
                    customer,
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status")
                ->from('sales');

            $this->datatables->where('archived', 0);//TODO APENAS VENDAS NÃO ARQUIVADAS
        }

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        if ($filterStatus) {
            $this->datatables->where('sale_status', $filterStatus);
        }

        if ($fdivulgacao) {
            $this->datatables->where('meiodivulgacao', $fdivulgacao);
        }

        if ($flDataVendaDe) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $flDataVendaDe);
        }

        if ($flDataVendaAte) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $flDataVendaAte);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getSaleItems($programacaoId=NULL)
    {
        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $fechamento_comissao_link = anchor('salesutil/modal_view_fechamento_commissions/$1', '<i class="fa fa-bar-chart-o"></i> ' . lang('modal_view_fechamento_commissions'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $archived_link = "<a href='#' class='po' title='<b>" . lang("archived_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/archived/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-archive'></i> "
            . lang('archived_sale') . "</a>";

        $gerar_comissao_link = "<a href='#' class='po' title='<b>" . lang("generate_sales_commission") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/generate_sales_commission/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-paypal'></i> "
            . lang('generate_sales_commission') . "</a>";

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $archived_link . '</li>
                     <li class="divider"></li>
                    <li>' . $gerar_comissao_link . '</li>
                    <li>' . $fechamento_comissao_link . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $filterOpcional = $this->input->post('filterOpcional');
        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $filterStatus = $this->input->post('filterStatus');
        $flDataVendaDe = $this->input->post('flDataVendaDe');
        $flDataVendaAte = $this->input->post('flDataVendaAte');
        $fdivulgacao = $this->input->post('fdivulgacao');
        $programacaoId = $programacaoId != 'all' ? $programacaoId : NULL;

        $this->load->library('datatables');

        $this->datatables
            ->select("sma_sales.id as id, 
                sma_sales.date, 
                sma_sales.reference_no,
                sma_sales.vendedor,
                concat(sma_sale_items.customerClientName,'<br/><small>', sma_sale_items.faixaNome, '</small>' ) as customer,
                concat(sma_sale_items.product_name, '<br/>', DATE_FORMAT(sma_agenda_viagem.dataSaida, '%d/%m/%Y'), ' ', DATE_FORMAT(sma_agenda_viagem.horaSaida, '%h:%i')) as product_name,
                sma_sales.sale_status,

                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) as grand_total, 
                
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal))
                 * (sma_sales.paid*100/(sma_sales.grand_total))/100  as paid, 
                
                (sma_sale_items.subtotal + (sma_sale_items.subtotal-sma_sale_items.subtotal*(1-((sma_sales.order_discount)/sma_sales.total)))*-1+(sma_sale_items.subtotal*(1+((sma_sales.shipping)/sma_sales.total))-sma_sale_items.subtotal)) -
                (sma_sale_items.subtotal*(1-((sma_sales.order_discount+sma_sales.shipping)/sma_sales.total)) * (sma_sales.paid*100/(sma_sales.total-(sma_sales.order_discount+sma_sales.shipping)))/100) as balance,
                
                sma_sales.payment_status,
                sma_sales.venda_link")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->datatables->join('agenda_viagem', 'agenda_viagem.id = sale_items.programacaoId', 'left');

        $this->datatables->where('sale_items.adicional', false);//NAO MOSTRA ADICIONAIS NESTE RELATORIO

        if ($filterOpcional) {
            $this->datatables->where('product_id', $filterOpcional);
        }

        if ($programacaoId) {
            $this->datatables->where('sale_items.programacaoId', $programacaoId);
        }

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        if ($filterStatus) {
            $this->datatables->where('sale_status', $filterStatus);
        }

        if ($fdivulgacao) {
            $this->datatables->where('meiodivulgacao', $fdivulgacao);
        }

        if ($flDataVendaDe) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") >=', $flDataVendaDe);
        }

        if ($flDataVendaAte) {
            $this->datatables->where('DATE_FORMAT(date,"%Y-%m-%d") <=', $flDataVendaAte);
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if ($programacaoId == NULL) {
            $this->datatables->where('archived', 0);//TODO APENAS VENDAS NÃO ARQUIVADAS
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function modal_view($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');
        $this->load->model('repository/CommissionsRepository_model', 'CommissionsRepository_model');

        $fechamento =  $this->FechamentoComissaoRepository_model->getFechamentoBySaleID($id);

        $inv = $this->site->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }

        if ($fechamento){
            $this->data['fechamento']       = $fechamento;
            $this->data['itensFechamento']  = $this->FechamentoComissaoRepository_model->getAllItensFechamento($fechamento->id, null, $id);
        } else {
            $this->data['itensComissao']        = $this->CommissionsRepository_model->getAllCommissionItensByComissionSaleID($id);
        }

        $this->data['inv']                  = $inv;
        $this->data['customer']             = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller']               = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by']           = $this->site->getUser($inv->created_by);
        $this->data['updated_by']           = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse']            = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento']    = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['faturas']              = $this->site->getParcelasFaturaByContaReceber($id);
        $this->data['payments']             = $this->site->getPaymentsForSale($id);
        $this->data['rows']                 = $this->site->getAllInvoiceItems($id);
        $this->data['return_sale']          = $inv->return_id ? $this->site->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows']          = $inv->return_id ? $this->site->getAllInvoiceItems($inv->return_id) : NULL;

        $this->load->view($this->theme . 'sales/modal_view', $this->data);
    }

    public function modal_view_canceled($id = null)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->site->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }

        $this->data['customer']     = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller']       = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by']   = $this->site->getUser($inv->created_by);
        $this->data['updated_by']   = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse']    = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv']          = $inv;

        $this->data['parcelasOrcamento']    = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['faturas']              = $this->site->getParcelasFaturaByContaReceberCanceled($id);
        $this->data['payments']             = $this->site->getPaymentsForSale($id);
        $this->data['rows']                 = $this->site->getAllInvoiceItems($id);

        $this->data['return_sale'] = $inv->return_id ? $this->site->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->site->getAllInvoiceItems($inv->return_id) : NULL;

        $this->load->view($this->theme . 'sales/modal_view_canceled', $this->data);
    }

    public function events($sale_id)
    {
        $this->data['events'] = $this->site->getAllEvents($sale_id);
        $this->data['inv'] = $this->site->getInvoiceByID($sale_id);

        $this->load->view($this->theme . 'sales/events', $this->data);
    }

    public function motivo_cancelamento($id)
    {
        $this->load->model('repository/MotivoCancelamentoRepository_model', 'MotivoCancelamentoRepository_model');

        $this->data['inv'] = $this->site->getInvoiceByID($id);
        $this->data['motivos_cancelamento'] = $this->MotivoCancelamentoRepository_model->getAll();

        $this->load->view($this->theme . 'sales/motivoCancelamento', $this->data);
    }

    public function modal_view_fechamento_commissions($sale_id = null)
    {

        $this->load->model('repository/FechamentoComissaoRepository_model', 'FechamentoComissaoRepository_model');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $fechamento =  $this->FechamentoComissaoRepository_model->getFechamentoBySaleID($sale_id);
        $this->data['biller']  = $this->site->getCompanyByID($this->Settings->default_biller);

        if ($fechamento) {

            $this->data['fechamento']       = $fechamento;
            $this->data['created_by']       = $this->site->getUser($fechamento->created_by);
            $this->data['updated_by']       = $fechamento->updated_by ? $this->site->getUser($fechamento->updated_by) : null;

            $this->data['itensFechamento']  = $this->FechamentoComissaoRepository_model->getAllItensFechamento($fechamento->id);
            $this->data['faturas']          = $this->FechamentoComissaoRepository_model->getFaturasByFechamento($fechamento->id);
            $this->data['payments']         = $this->FechamentoComissaoRepository_model->getPaymentsByFechamento($fechamento->id);

            $this->load->view($this->theme . 'commissions/modal_view', $this->data);
        } else {
            $this->load->view($this->theme . 'commissions/modal_view_blank', $this->data);
        }
    }

    public function impressao_carne_asaas($cod_installment)
    {
        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $paymentBook = $this->CobrancaService_model->impressao_carne_asaas($cod_installment);

        if (!empty($paymentBook)) {

            header('Content-Type: application/pdf');
            header('Content-Disposition: inline; filename="paymentBook.pdf"');
            header('Content-Length: ' . strlen($paymentBook));

            echo $paymentBook;
        } else {
            echo "Erro ao gerar o PDF.";
        }
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->site->getInvoiceByID($id);

        $this->data['barcode']  = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->site->getPaymentsForSale($id);
        $this->data['biller']   = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user']     = $this->site->getUser($inv->created_by);
        $this->data['warehouse']= $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv']      = $inv;

        $faturas = $this->site->getParcelasFaturaByContaReceber($id);
        $return  = $this->site->getReturnBySID($id);

        $this->data['parcelasOrcamento'] = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->site->getAllInvoiceItems($id);
        $this->data['adicionais'] = $this->site->getAllInvoiceItemsAdicionais($id);
        $this->data['return_items'] = $return ? $this->site->getAllReturnItems($return->id) : null;
        //$this->data['paypal'] = $this->sales_model->getPaypalSettings();
        //$this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            if ($inv->sale_status == 'orcamento') {
                $name = lang("orcamento") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'cancel') {
                $name = lang("orcamento") .'_'.lang("cancel"). "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/cancelado.png'));
            } else {
                $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
            }
        } else {
            if ($inv->sale_status == 'orcamento') {
                $name = lang("orcamento") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'cancel') {
                $name = lang("sale") . '_'. lang("cancel") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/cancelado.png'));
            } else {
                $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    public function downloadDocument($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            if ($inv->contract_id == null) {
                throw new Exception('O documento não foi gerado.');
            }

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            if ($document->document_status == Document_model::DOCUMENT_STATUS_DRAFT) {
                $this->DocumentService_model->downloadOriginalDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_UNSIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_SIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_PENDING
                || $document->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                $this->DocumentService_model->downloadDocument($document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
                throw new Exception('O documento foi cancelado.');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function abrirContrato($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            if ($inv->contract_id == null) {
                throw new Exception('O documento não foi gerado.');
            }

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            if ($document->document_status == Document_model::DOCUMENT_STATUS_DRAFT) {
                redirect('contracts/addSignatories/'.$document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_UNSIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_SIGNED
                || $document->document_status == Document_model::DOCUMENT_STATUS_PENDING
                || $document->document_status == Document_model::DOCUMENT_STATUS_COMPLETED) {
                redirect('contracts/monitorContractSignature/'.$document->id);
            } else if ($document->document_status == Document_model::DOCUMENT_STATUS_CANCELED) {
                throw new Exception('O documento foi cancelado.');
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    function to_share_contract($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

            $this->data['document'] = $document;

            $this->load->view($this->theme.'documents/to_share', $this->data);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    function open_contract_events($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['events'] = $this->DocumentRepository_model->getEvents($document->id);
            $this->data['contract'] = $document;

            $this->load->view($this->theme.'documents/events', $this->data);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    function to_share_validate_contract($saleID)
    {
        try {

            $inv = $this->site->getInvoiceByID($saleID);

            $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

            $this->load->model('service/DocumentService_model', 'DocumentService_model');
            $this->load->model('model/Document_model', 'Document_model');
            $this->load->model('model/Signatures_model', 'Signatures_model');

            $document = new Document_model($inv->contract_id);

            $this->data['document'] = $document;

            $this->load->view($this->theme.'documents/to_share_validate', $this->data);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }
}