<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Graphic extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->load->library('form_validation');

        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');

        $this->load->model('repository/WelcomeRepository_model', 'WelcomeRepository_model');


        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model('db_model');
        $this->load->model('settings_model');

        //$this->output->cache(5);
    }

    public function index()
    {
        if ($this->Settings->version == '2.3') {
            $this->session->set_flashdata('warning', 'Please complete your update by synchronizing your database.');
            redirect('sync');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

        $this->saldoJuno();

        $lmsdate = date('Y-m-d', strtotime('first day of last month')) . ' 00:00:00';
        $lmedate = date('Y-m-d', strtotime('last day of last month')) . ' 23:59:59';

        $this->data['relatorioVendasPorDespesas'] = $this->WelcomeRepository_model->getRelatorioVendasPorDespesas();
        $this->data['bs'] = $this->WelcomeRepository_model->getBestSeller();
        $this->data['lmbs'] = $this->WelcomeRepository_model->getBestSeller($lmsdate, $lmedate);
        $this->data['vVendedorMes'] = $this->WelcomeRepository_model->getVendasPorVendedor();
        $this->data['vVendedorMesAnterior'] = $this->WelcomeRepository_model->getVendasPorVendedor($lmsdate, $lmedate);
        $this->data['pcadMes'] = $this->WelcomeRepository_model->getVendasPorCategoria();
        $this->data['pcadMesPassado'] = $this->WelcomeRepository_model->getVendasPorCategoria($lmsdate, $lmedate);

        $this->data['salesFaturadas'] = $this->db_model->getLatestSales('faturada');
        $this->data['salesOrcamentos'] = $this->db_model->getLatestSales('orcamento');
        $this->data['salesListaEspera'] = $this->db_model->getLatestSales('lista_espera');

        $this->data['customers'] = $this->db_model->getLatestCustomers();
        $this->data['suppliers'] = $this->db_model->getLatestSuppliers();

        $this->data['totalRecebimentoMes'] = $this->WelcomeRepository_model->getTotalRecebimentoMes()->totalRecebimentoMes;
        $this->data['totalReceberMes'] = $this->WelcomeRepository_model->getTotalReceberMes()->totalReceberMes;
        $this->data['totalPagamentosMes'] = $this->WelcomeRepository_model->getTotalPagamentoMes()->totalPagamentoMes;
        $this->data['totalPagarMes'] = $this->WelcomeRepository_model->getTotalPagarMes()->totalPagarMes;

        $this->data['pMeioDivulgacaoMes'] = $this->WelcomeRepository_model->getVendasPorMeioDivulgacao();
        $this->data['pMeioDivulgacaoMesPassado'] = $this->WelcomeRepository_model->getVendasPorMeioDivulgacao($lmsdate, $lmedate);


        $this->data['pTipoCobrancaMes'] = $this->WelcomeRepository_model->getVendasPorTipoCobranca();
        $this->data['pTipoCobrancaMesPassado'] = $this->WelcomeRepository_model->getVendasPorTipoCobranca($lmsdate, $lmedate);


        //$this->data['transfers'] = $this->db_model->getLatestTransfers();
        //$this->data['stock'] = $this->db_model->getStockValue();
        //$this->data['vVendedorPorCategoria'] = $this->db_model->getVendasPorVendedorAgrupadoCategoria();
        //$this->data['contaReceberAno'] = $this->db_model->getContaReceberAno();
        //$this->data['contaPagarAno'] = $this->db_model->getContaPagarAno();
        //$this->data['aReceberEmAtraso'] = $this->db_model->getReceberEmAtraso()->valor;
        //$this->data['aPagarEmAtraso'] = $this->db_model->getPagamentoEmAtraso()->valor;

        $this->data['chatDataFluxoCaixaMes'] = $this->db_model->getChartDataFluxoCaixaMes();
        $this->data['chatDataFluxoCaixaMesQuitadoMes'] = $this->db_model->getChartDataFluxoCaixaMesQuitadoCredito();
        $this->data['chatDataFluxoCaixaDebitoMesQuitadoMes'] = $this->db_model->getChartDataFluxoCaixaMesQuitadoDebito();

        $this->data['aReceberHoje'] = $this->db_model->getReceberHoje()->valor;
        $this->data['aPagarHoje'] = $this->db_model->getPagarHoje()->valor;

        $this->data['aReceberMes'] = $this->db_model->getReceberMes()->valor;
        $this->data['aPagarMEs'] = $this->db_model->getPagarMes()->valor;

        $bc = array(array('link' => '#', 'page' => lang('graphic')));
        $meta = array('page_title' => lang('graphics'), 'bc' => $bc);

        $this->page_construct('graphic', $meta, $this->data);
    }

    function saldoJuno() {

        $junoSetting = $this->settings_model->getJunoSettings();

        if (!$junoSetting->active) return null;

        $consultaSaldo = $this->CobrancaService_model->consultaSaldo();

        if ($consultaSaldo != null) {
            $this->data['junoDisponivel'] = json_decode($consultaSaldo)->data->transferableBalance;
            $this->data['junoLiberar'] = json_decode($consultaSaldo)->data->withheldBalance;
            $this->data['junoTotal'] = json_decode($consultaSaldo)->data->balance;
        }

    }
    function promotions()
    {
        $this->load->view($this->theme . 'promotions', $this->data);
    }

    function image_upload()
    {
        if (DEMO) {
            $error = array('error' => $this->lang->line('disabled_in_demo'));
            $this->sma->send_json($error);
            exit;
        }
        $this->security->csrf_verify();
        if (isset($_FILES['file'])) {
            $this->load->library('upload');
            $config['upload_path'] = 'assets/uploads/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '500';
            $config['max_width'] = $this->Settings->iwidth;
            $config['max_height'] = $this->Settings->iheight;
            $config['encrypt_name'] = TRUE;
            $config['overwrite'] = FALSE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $error = array('error' => $error);
                $this->sma->send_json($error);
                exit;
            }
            $photo = $this->upload->file_name;
            $array = array(
                'filelink' => base_url() . 'assets/uploads/images/' . $photo
            );
            echo stripslashes(json_encode($array));
            exit;

        } else {
            $error = array('error' => 'No file selected to upload!');
            $this->sma->send_json($error);
            exit;
        }
    }

    function set_data($ud, $value)
    {
        $this->session->set_userdata($ud, $value);
        echo true;
    }

    function hideNotification($id = NULL)
    {
        $this->session->set_userdata('hidden' . $id, 1);
        echo true;
    }

    function language($lang = false)
    {
        if ($this->input->get('lang')) {
            $lang = $this->input->get('lang');
        }
        //$this->load->helper('cookie');
        $folder = 'sma/language/';
        $languagefiles = scandir($folder);
        if (in_array($lang, $languagefiles)) {
            $cookie = array(
                'name' => 'language',
                'value' => $lang,
                'expire' => '31536000',
                'prefix' => 'sma_',
                'secure' => false
            );
            $this->input->set_cookie($cookie);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function toggle_rtl()
    {
        $cookie = array(
            'name' => 'rtl_support',
            'value' => $this->Settings->user_rtl == 1 ? 0 : 1,
            'expire' => '31536000',
            'prefix' => 'sma_',
            'secure' => false
        );
        $this->input->set_cookie($cookie);
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function download($file)
    {
        $this->load->helper('download');
        force_download('./files/'.$file, NULL);
        exit();
    }

}
