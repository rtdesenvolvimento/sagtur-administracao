<?php defined('BASEPATH') OR exit('No direct script access allowed');

use phpDocumentor\Reflection\DocBlockFactory;
use PhpOffice\PhpWord\IOFactory;
use Dompdf\Dompdf;

class Contracts extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $plugsignSettings = $this->site->getPlugsignSettings();

        if (!$plugsignSettings->active || !$plugsignSettings->token){
            $this->session->set_flashdata('error', lang('plugsign_not_configured'));
            redirect('welcome');
        }

        $this->lang->load('documents', $this->Settings->user_language);

        //service
        $this->load->model('service/DocumentService_model', 'DocumentService_model');
        $this->load->model('service/ParseContract_model', 'ParseContract_model');

        //model
        $this->load->model('model/Document_model', 'Document_model');
        $this->load->model('model/Signatures_model', 'Signatures_model');
        $this->load->model('model/Folder_model', 'Folder_model');
        $this->load->model('model/DocumentCustomer_model', 'DocumentCustomer_model');

        //repository
        $this->load->model('repository/DocumentRepository_model', 'DocumentRepository_model');
        $this->load->model('repository/FolderRepository_model', 'FolderRepository_model');
        $this->load->model('repository/PlugsignRepository_model', 'PlugsignRepository_model');
        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');

        //dto
        $this->load->model('dto/DocumentFilterDTO_model', 'DocumentFilterDTO_model');
        $this->load->model('dto/FolderFilterDTO_model', 'FolderFilterDTO_model');
        $this->load->model('dto/ParseContractDTO_model', 'ParseContractDTO_model');

        $this->load->library('form_validation');

        $this->upload_path          = 'assets/uploads/documents/'.$this->session->userdata('cnpjempresa');
        $this->file_types           = 'pdf|doc|docx';
        $this->allowed_file_size    = '16384';//16MB tamanho maximo do arquivo
    }

    function index()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('documents')));
        $meta = array('page_title' => lang('documents'), 'bc' => $bc);

        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->page_construct('documents/grid', $meta, $this->data);
    }

    function list()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('documents')));
        $meta = array('page_title' => lang('documents'), 'bc' => $bc);

        $this->page_construct('documents/index', $meta, $this->data);
    }

    function grid_search()
    {

        $filter = new DocumentFilterDTO_model();
        $filter->statusDocument = $this->input->get('statusDocument');
        $filter->programacao_id = $this->input->get('programacaoID');
        $filter->folder_id = $this->input->get('folderID');
        $filter->biller_id = $this->input->get('billerID');
        $filter->dataVendaDe = $this->input->get('dataVendaDe');
        $filter->dataVendaAte = $this->input->get('dataVendaAte');
        $filter->strSignatory = $this->input->get('strSignatory');
        $filter->limit = $this->input->get('limit') == 0 ? 24 : $this->input->get('limit');

        $this->data['documents'] = $this->DocumentRepository_model->getAll($filter);

        $this->load->view($this->theme . 'documents/grid_search', $this->data);
    }

    function getDocuments() {

        $this->load->library('datatables');

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->datatables
            ->select("documents.id as id,
                    documents.name as doc,
                    documents.created_at as date,
                    documents.document_status as status,
                    documents.document_status as st")
            ->from('documents');

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function addDocument($folderID = 1)  {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $document = new Document_model();
            $document->name = $this->input->post('name');
            $document->folder_id = $this->input->post('folder_id');
            $document->note = $this->input->post('note');
            $document->file_name = $this->upload_document();

        } elseif ($this->input->post('addDocument')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        try {

            if ($this->form_validation->run() == true && ($documentID = $this->DocumentService_model->save($document))) {
                $this->session->set_flashdata('message', lang("document_successfully_added"));
                redirect('contracts/addSignatories/'.$documentID);
            } else {
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['folders'] = $this->FolderRepository_model->getAllFolders();
                $this->data['folderID'] = $folderID;
                $this->load->view($this->theme . 'documents/addDocument', $this->data);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function editDocument($document_id) {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        $document = new Document_model($document_id);

        if ($this->form_validation->run() == true) {
            $document->name = $this->input->post('name');
            $document->note = $this->input->post('note');
        }

        try {
            if ($this->form_validation->run() == true && $this->DocumentService_model->rename_document($document)) {
                $this->session->set_flashdata('message', lang("document_successfully_edited"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['document'] = $document;
                $this->load->view($this->theme . 'documents/editDocument', $this->data);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function to_share($documentID)
    {
        $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['document'] = new Document_model($documentID);

        $this->load->view($this->theme.'documents/to_share', $this->data);
    }

    function to_share_validate($documentID)
    {
        $this->data['error']    = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['document'] = new Document_model($documentID);

        $this->load->view($this->theme.'documents/to_share_validate', $this->data);
    }

    private function upload_document()
{
    if ($_FILES['file']['size'] > 0) {

        $this->load->library('upload');
        $config['upload_path'] = $this->upload_path . '/';
        $config['allowed_types'] = $this->file_types;
        $config['max_size'] = $this->allowed_file_size;
        $config['overwrite'] = FALSE;
        $config['encrypt_name'] = TRUE;
        $config['max_filename'] = 25;
        $this->upload->initialize($config);

        if (!file_exists($this->upload_path . '/')) {
            mkdir($this->upload_path , 0777, true);
        }

        if (!$this->upload->do_upload('file')) {
            $error = $this->upload->display_errors();
            $this->session->set_flashdata('error', $error);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        return $this->upload->file_name;
    }

    return false;
}

    public function addSignatories($document_id) {

        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('contracts'), 'page' => lang('contracts')),
            array('link' => '#', 'page' => lang('add_signatories'))
        );

        $meta = array('page_title' => lang('add_signatories'), 'bc' => $bc);

        if ($this->Settings->default_biller_contract) {
            $biller = $this->site->getCompanyByID($this->Settings->default_biller_contract);
        } else {
            $user   = $this->site->getUser($this->session->userdata('user_id'));
            $biller = $this->site->getCompanyByID($user->biller_id);
        }

        $this->data['biller'] = $biller;
        $this->data['document'] = new Document_model($document_id);

        $this->page_construct('documents/addSignatories', $meta, $this->data);
    }

    function addSignatory($document_id)
    {
        try {

            $this->form_validation->set_rules('signatory', lang("signatory"), 'required|min_length[3]');
            $this->form_validation->set_rules('shipping_type', lang("shipping_type"), 'required');
            $this->form_validation->set_rules('signer_field', lang("signer_field"), 'required');

            if ($this->form_validation->run() == true) {

                $type = $this->input->post('shipping_type');
                $note = $this->input->post('signatories_note');
                $nome = $this->input->post('signatory');

                $signature = new Signatures_model();
                $signature->name = $nome;
                $signature->note = $note;

                if ($type == 'telefone') {
                    $signature->phone = $this->input->post('signer_field');
                } else {
                    $signature->email = $this->input->post('signer_field');
                }
            }

            if ($this->form_validation->run() == true && $this->DocumentService_model->addSignatory($document_id, $signature)) {
                $this->session->set_flashdata('message', lang("signatory_added_successfully"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {

                $this->data['document'] = new Document_model($document_id);
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'documents/addSignatory', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function addCustomer($document_id)
    {
        try {

            $this->form_validation->set_rules('customer', lang("customer"), 'required');

            if ($this->form_validation->run() == true) {

                $customer = $this->site->getCompanyByID($this->input->post('customer'));
                $note = $this->input->post('signatories_note');
                $nome = $customer->name;

                $signature = new Signatures_model();

                if ($customer->cf5) {
                    $signature->phone = $customer->cf5;
                } else {
                    $signature->email = $customer->email;
                }

                $signature->customer = $customer->name;
                $signature->customer_id = $customer->id;
                $signature->name = $nome;
                $signature->note = $note;
            }

            if ($this->form_validation->run() == true && $this->DocumentService_model->addSignatory($document_id, $signature)) {
                $this->session->set_flashdata('message', lang("signatory_added_successfully"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {

                $this->data['document'] = new Document_model($document_id);
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->load->view($this->theme . 'documents/addCustomer', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function create_signatories($document_id)
    {
        $this->form_validation->set_rules('signature_type', lang("signature_type"), 'required');

        if ($this->form_validation->run() == true) {

            $document = new Document_model($document_id);

            $document->signature_type = $this->input->post('signature_type');
            $document->solicitar_selfie = $this->input->post('solicitar_selfie');
            $document->solicitar_documento = $this->input->post('solicitar_documento');
            $document->auto_destruir_solicitacao = $this->input->post('auto_destruir_solicitacao');
            $document->data_vencimento_contrato = $this->input->post('data_vencimento_contrato');
            $document->signatories_note = $this->input->post('signatories_note');
            $document->setSignatories(array());

            $i = isset($_POST['signatory']) ? sizeof($_POST['signatory']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $name = $_POST['signatory'][$r];

                if ($name) {

                    $signatures = new Signatures_model();

                    $type =  $_POST['shipping_type'][$r];
                    $customer_id = $_POST['customer_id'][$r];
                    $biller_id = $_POST['biller_id'][$r];

                    if ($customer_id) {
                        $signatures->customer_id = $customer_id;
                        $signatures->customer = $this->site->getCompanyByID($customer_id)->name;
                    }

                    if ($biller_id) {
                        $signatures->is_biller = 1;
                        $signatures->biller_id = $biller_id;
                        $signatures->biller = $this->site->getCompanyByID($biller_id)->name;
                    }

                    $signatures->name = $name;

                    if ($type == 'telefone') {
                        $signatures->phone = $_POST['signer_field'][$r];
                    } else {
                        $signatures->email = $_POST['signer_field'][$r];
                    }

                    $document->addSignature($signatures);
                }
            }
        }

        try {

            if ($this->form_validation->run() == true && $this->DocumentService_model->sendSignature($document)) {
                $this->session->set_flashdata('message', lang("subscription_request_sent_successfully"));

                redirect('contracts/monitorContractSignature/'.$document_id);

            } else {
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function monitorContractSignature($document_id)
    {
        $bc = array(
            array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('contracts'), 'page' => lang('contracts')),
            array('link' => '#', 'page' => lang('monitor_contract_signature'))
        );

        $meta = array('page_title' => lang('monitor_contract_signature'), 'bc' => $bc);

        $document = new Document_model($document_id);

        $this->data['document'] = $document;

        $this->data['biller'] = $this->site->getCompanyByID($document->biller_id);

        $this->page_construct('documents/monitorContractSignature', $meta, $this->data);
    }

    public function createDocument($document_id)
    {
        try {
            $this->DocumentService_model->createDocument($document_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function cancelDocument($document_id)
    {
        try {
            $this->DocumentService_model->cancel($document_id);

            $this->session->set_flashdata('message', lang("successfully_cancel_document"));

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect('contracts');

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function protectDocument($document_id)
    {
        try {

            $pass_user = 'root';
            $pass_owner = '123456';

            $this->DocumentService_model->protectDocument($document_id, $pass_user, $pass_owner);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function downloadDocument($document_id)
    {
        try {
            $this->DocumentService_model->downloadDocument($document_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function downloadOriginalDocument($document_id)
    {
        try {
            $this->DocumentService_model->downloadOriginalDocument($document_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function toSign($document_id)
    {
        try {
            $this->DocumentService_model->toSign($document_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            echo $exception->getMessage();
        }
    }

    public function sendDocumentTo($document_id)
    {
        try {

            $this->form_validation->set_rules('signatories_note', lang("signatories_note"), 'required|min_length[3]');

            if ($this->form_validation->run() == true) {
                $message = $this->input->post('signatories_note');
                $contact = $this->input->post('signer_field');
                $type = $this->input->post('shipping_type');
            }

            if ($this->form_validation->run() == true && $this->DocumentService_model->sendDocumentTo($document_id, $message, $contact, $type)) {
                $this->session->set_flashdata('message', lang("message_sent_successfully"));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['document'] = new Document_model($document_id);
                $this->load->view($this->theme . 'documents/send_document', $this->data);
            }

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function resendToSignatories($document_id)
    {
        try {

            $this->DocumentService_model->resendToSignatories($document_id);

            $this->session->set_flashdata('message', lang("successfully_forwarded_to_signatories"));

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function sendDocumentToWhatsApp($document_id)
    {
        try {

            $this->DocumentService_model->sendDocumentToWhatsApp($document_id);

            $this->session->set_flashdata('message', lang("successfully_forwarded_to_signatories"));

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function resendToSignatory($signatory_id, $document_id)
    {
        try {

            $this->DocumentService_model->resendToSignatories($signatory_id, $document_id);

            $this->session->set_flashdata('message', lang("successfully_forwarded_to_signatories"));

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }


    public function resendWhatsapp($signatario_id)
    {
        try {
            $this->DocumentService_model->resendWhatsapp($signatario_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function resendEmail($signatario_id)
    {
        try {
            $this->DocumentService_model->resendEmail($signatario_id);
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function cancelSignatory($signatures_id)
    {
        try {
            if ($this->DocumentService_model->cancelSignatory($signatures_id)) {
                $this->session->set_flashdata('message', lang("successfully_removed_to_signatory"));
            } else {
                $this->session->set_flashdata('error', lang("error_removed_to_signatory"));
            }

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function sign($document_id, $signatory_id)
    {
        try {

            if ($this->DocumentService_model->sign($document_id, $signatory_id) ){
                $this->session->set_flashdata('message', lang("successfully_signed_document"));
            }

            if ($this->input->is_ajax_request()) {
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function create_company_tecnospeed_plugsign_integration($warehouseID)
    {
        try {

            if ($this->DocumentService_model->create_company_tecnospeed_plugsign_integration($warehouseID)) {
                $this->session->set_flashdata('message', lang("company_included_with_success"));
            }

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function generateSaleContract($sale_id)
    {
        try {

            $itens = $this->site->getAllInvoiceItems($sale_id);
            $contract_id = $this->Settings->contract_id;

            foreach ($itens as $item) {
                $product = $this->site->getProductByID($item->product_id);
                if ($product->contract_id) {
                    $contract_id = $product->contract_id;
                    break;
                }
            }

            $file_name = $this->gerar_contrato($contract_id, $sale_id);
            $vendaEmEdicao = true;
            $forcarEmissaoContrato = true;

            if ($documentID = $this->DocumentService_model->generateSaleContract($sale_id, $file_name, $vendaEmEdicao, $forcarEmissaoContrato)) {

                $this->session->set_flashdata('message', lang("sale_contract_generated_successfully"));

                if ($this->Settings->enviar_contrato_signatarios_venda) {

                    if ($this->input->is_ajax_request()) {
                        echo lang('Contrato de Venda gerado com sucesso!');
                        die();
                    }

                    redirect('contracts/monitorContractSignature/'.$documentID);
                } else {

                    if ($this->input->is_ajax_request()) {
                        echo lang('Contrato de Venda gerado com sucesso!');
                        die();
                    }

                    redirect('contracts/addSignatories/'.$documentID);
                }
            } else {

                if ($this->input->is_ajax_request()) {
                    echo lang('Não foi possível gerar o contrato de venda!');
                    die();
                }

                redirect($_SERVER["HTTP_REFERER"]);
            }


        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());

            if ($this->input->is_ajax_request()) {
                echo lang($exception->getMessage());
                die();
            }

            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function contracts()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('contracts')));
        $meta = array('page_title' => lang('contracts'), 'bc' => $bc);

        $this->page_construct('contracts/index', $meta, $this->data);
    }

    function tags_contrato()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('tags_contrato')));
        $meta = array('page_title' => lang('tags_contrato'), 'bc' => $bc);

        $reflection = new ReflectionClass('TAGContrato_model');
        $docBlockFactory = DocBlockFactory::createInstance();
        $constantes = $reflection->getConstants();

        // Array para armazenar constantes e descrições
        $constantesComDescricao = [];

        foreach ($constantes as $nome => $valor) {
            $refConst = $reflection->getReflectionConstant($nome);
            $comentario = $refConst->getDocComment();
            $descricao = null;

            // Verifica se o DocBlock existe e, caso sim, extrai a descrição
            if ($comentario) {
                $docblock = $docBlockFactory->create($comentario);
                $descricao = $docblock->getSummary();
            }

            // Adiciona a constante e a descrição (ou null se não houver) ao array
            $constantesComDescricao[] = [
                'nome' => $nome,
                'valor' => '${'.$valor.'}',
                'descricao' => $descricao
            ];
        }

        // Envia para a view
        $this->data['constantes'] = $constantesComDescricao;

        $this->page_construct('contracts/tags_contrato', $meta, $this->data);
    }

    function getContracts()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("id, name, active")
            ->from("contracts")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" .
                site_url('contracts/editContract/$1') . "'class='tip' title='"
                . lang("edit_contract") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>"
                . lang("delete_contract") . "</b>' data-content=\"<p>"
                . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='"
                . site_url('contracts/deleteContract/$1') . "'>" . lang('i_m_sure')
                . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function addContract()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $contract = $this->input->post('contract', false);
            $clauses = $this->input->post('clauses', false);
            $type = $this->input->post('type');
            $usar_clausulas_site = $this->input->post('usar_clausulas_site');

            if ($type == 'text') {
                $usar_clausulas_site = false;
            }

            $data = array(
                'name' => $this->input->post('name'),
                'contract' => $contract,
                'type' => $type,
                'usar_clausulas_site' => $usar_clausulas_site,
                'clauses' => $clauses,
            );

            if ($_FILES['file']['size'] > 0) {
                $data['file_name'] = $this->upload_contract_template();
            }
        }

        if ($this->form_validation->run() == true && $this->ContractRepository_model->addContract($data)) {
            $this->session->set_flashdata('message', lang("contract_added_successfully"));
            redirect("contracts/contracts");
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_contract')));
            $meta = array('page_title' => lang('add_contract'), 'bc' => $bc);

            $this->page_construct('contracts/addContract', $meta, $this->data);
        }
    }

    function editContract($id)
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {

            $contract = $this->input->post('contract', false);
            $clauses = $this->input->post('clauses', false);
            $type = $this->input->post('type');
            $usar_clausulas_site = $this->input->post('usar_clausulas_site');

            if ($type == 'text') {
                $usar_clausulas_site = false;
            }

            $data = array(
                'name' => $this->input->post('name'),
                'contract' => $contract,
                'type' => $type,
                'usar_clausulas_site' => $usar_clausulas_site,
                'clauses' => $clauses,
            );

            if ($_FILES['file']['size'] > 0) {
                $data['file_name'] = $this->upload_contract_template();
            }
        }

        if ($this->form_validation->run() == true && $this->ContractRepository_model->editContract($id, $data)) {
            $this->session->set_flashdata('message', lang("contract_edited_successfully"));
            redirect("contracts/editContract/".$id);
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_contract')));
            $meta = array('page_title' => lang('edit_contract'), 'bc' => $bc);

            $this->data['contract'] = $this->ContractRepository_model->getById($id);

            $this->page_construct('contracts/editContract', $meta, $this->data);
        }
    }

    private function upload_contract_template()
    {
        if ($_FILES['file']['size'] > 0) {

            $this->load->library('upload');
            $config['upload_path'] = $this->upload_path . '/';
            $config['allowed_types'] = $this->file_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = FALSE;
            $config['encrypt_name'] = TRUE;
            $config['max_filename'] = 25;
            $this->upload->initialize($config);

            if (!file_exists($this->upload_path . '/')) {
                mkdir($this->upload_path , 0777, true);
            }

            if (!$this->upload->do_upload('file')) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            return $this->upload->file_name;
        }

        return false;
    }

    function deleteContract($id = NULL) {

        if ($this->ContractRepository_model->deleteContract($id)) {
            echo lang("contract_deleted_successfully");
        }
    }

    public function gerar_contrato($contractID, $saleID): ?string
    {

        $config = new ParseContractDTO_model();
        $config->type = ParseContractDTO_model::MODEL;
        $config->sale_id    = $saleID;

        if ($contractID == null) {
            $contractID = $this->Settings->contract_id;
        }

        $config->contractID = $contractID;

        $contractConfig = $this->ContractRepository_model->getById($contractID);

        $config->tipo = $contractConfig->type;

        if ($contractConfig->type == 'text') {
            return $this->gerar_contrato_pdf($config);
        } else if ($contractConfig->type == 'document') {
            return $this->gerar_contrato_word($config, $contractConfig);
        } else {
            return null;
        }

    }

    private function gerar_contrato_pdf($config): string
    {

        $biller = $this->site->getCompanyByID($this->Settings->default_biller);

        $this->data['converted_contract'] = $this->ParseContract_model->parse($config);

        $html = $this->load->view($this->theme . 'contracts/gerar_contrato', $this->data, true);

        $filename = md5(uniqid(mt_rand())) . ".pdf";

        return $this->sma->gerar_contrato_pdf($html, $filename, 'S', $this->data['biller']->invoice_footer, null, null, null, 'P', base_url() . 'assets/uploads/logos/'.$biller->logo);

    }

    private function gerar_contrato_word($config, $contractConfig): string
    {

        $this->load->helper('margedocx');

        $config->retorno = ParseContractDTO_model::TIPO_RETORNO_ARRAY;

        $tagsArray = $this->ParseContract_model->parse($config);

        $arquivo_entrada    = $this->upload_path.'/'.$contractConfig->file_name;

        $filename = md5(uniqid(mt_rand()));

        $filename_docx = $filename . ".docx";
        $filename_html = $filename . ".html";
        $filename_pdf  = $filename . ".pdf";

        $arquivo_saida_docx = $this->upload_path.'/'.$filename_docx;
        $arquivo_saida_html = $this->upload_path.'/'.$filename_html;

        marge_docx($arquivo_entrada, $arquivo_saida_docx, $tagsArray);

        $phpWord = IOFactory::load($arquivo_saida_docx);
        $phpWord->save($arquivo_saida_html, 'HTML');

        $html_gerado = file_get_contents($arquivo_saida_html);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html_gerado);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $output = $dompdf->output();

        if (!file_exists($this->upload_path . '/')) {
            mkdir($this->upload_path, 0777, true);
        }

        file_put_contents($this->upload_path . '/' . $filename_pdf, $output);

        unlink($arquivo_saida_html);

        return $filename_docx;//retorna o arquivo em docx
    }

    public function create_webhook()
    {
        try {
            $this->DocumentService_model->createWebhook();

            $this->session->set_flashdata('message', lang("webhook_created_successfully"));

            redirect($_SERVER["HTTP_REFERER"]);

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function events($document_id)
    {
        $this->data['events'] = $this->DocumentRepository_model->getEvents($document_id);
        $this->data['contract'] = new Document_model($document_id);

        $this->load->view($this->theme . 'documents/events', $this->data);
    }

}