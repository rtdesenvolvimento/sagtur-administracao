<?php defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';

use Aws\S3\S3Client;
use Aws\Exception\AwsException;

class MinioController extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
    }

    private function getConfig() {
        $config =
            [
                'version' => 'latest',
                'region' => 'us-west-2',
                'ssl_enabled' => false,
                'use_path_style_endpoint' => true,
                'endpoint' => 'http://sagtur-file-storage.jelastic.saveincloud.net/',
                'credentials' =>
                    [
                        'key' => 'OFPuRBy068',
                        'secret' => 'D3NnIkmHl5',
                    ],
            ];

        return $config;
    }
    public function listBuckets() {

        $client = new S3Client($this->getConfig());

        $buckets = $client->listBuckets();


        print_r($buckets);
    }

    public function createBucket() {
        $client = new S3Client($this->getConfig());

        $client->createBucket([
            'Bucket' => 'bucket-label'
        ]);
    }

    public function deleteBucket() {
        $client = new S3Client($this->getConfig());

        $client->deleteBucket([
            'Bucket' => 'bucket-label'
        ]);
    }

    public function listObjects() {
        $client = new S3Client($this->getConfig());

        $result = $client->listObjects(['Bucket' => 'sagtur']);
        foreach ($result['Contents'] as $object) {
            echo $object['Key'] . "<br/>";
        }
    }

    public function putObject() {
        $client = new S3Client($this->getConfig());

        $retorno = $client->putObject([
            'Bucket'     => 'sagtur',
            'Key'        => '92610a520b418cdac43d58a0dfa343672e3.png',
            'SourceFile' => 'C:\Users\andre\Downloads\0001.jpeg'
        ]);
        echo $retorno;
    }

    public function getObjectDonwload() {
        $client = new S3Client($this->getConfig());

        $client->getObject([
            'Bucket' => 'sagtur',
            'Key'    => '92610a520b418cdac43d58a0dfa672e3.png',
            'SaveAs' => 'C:\Users\andre\Downloads\2.png'
        ]);
    }

    public function getObjectURLContentType() {
        $client = new S3Client($this->getConfig());

        $retorno = $client->getObject([
            'Bucket' => 'sagtur',
            'Key'    => '92610a520b418cdac43d58a0dfa672e3.png',
        ]);

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $tipoArquivo = finfo_buffer($finfo, $retorno['Body']);
        finfo_close($finfo);
        header('Content-Type: ' . $tipoArquivo);

        echo $retorno['Body'];
    }

    public function getObjectURLContentURL() {
        $client = new S3Client($this->getConfig());

        $retorno = $client->getObjectUrl('sagtur','92610a520b418cdac43d58a0dfa672e3.png');


        echo $retorno;
    }

    public function getObjectURL() {
        $client = new S3Client($this->getConfig());

        $bucket = 'sagtur';
        $key = '92610a520b418cdac43d58a0dfa672e3.png';

        $cmd = $client->getCommand('GetObject', [
            'Bucket' => $bucket,
            'Key'    => $key,
        ]);

        $request = $client->createPresignedRequest($cmd, '+1 minutes');

        // Obtém a URL assinada
        $url = (string)$request->getUri();

        echo $url;
     }

     public function deleteObject() {
         $client = new S3Client($this->getConfig());

         $client->deleteObject([
             'Bucket' => 'bucket-label',
             'Key' => 'object-name'
         ]);
     }
}