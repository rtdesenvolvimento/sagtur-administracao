<?php defined('BASEPATH') or exit('No direct script access allowed');

define ('REJAX_APP_KEY', 'KEYliDtNmJiADHLvNEFQlYd2bw9y0DWqQGk');
define ('REJAX_APP_SECRET', 'SECRET3Ghe12ip3kPzX0P4g6NZU6WV1LRJcG9g');
define ('REJAX_API_URL', 'https://rejax.io:3001/api/server');

class Rejax extends MY_Controller
{

    public function post($url, $params = "", array $options = [])
    {
        $defaults = array(
            CURLOPT_POST => 1,
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_FRESH_CONNECT => 1,
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0
        );

        $ch = curl_init();
        curl_setopt_array($ch, ($options + $defaults));
        if (!$result = curl_exec($ch)) {
            throw new ErrorException(curl_error($ch));
        }
        curl_close($ch);
        return $result;
    }

    public function post_with_headers($url, $params = "", $headers = null)
    {
        $options = [];
        if ($headers) {
            $options = [CURLOPT_HTTPHEADER => $headers];
        }
        return $this->post($url, $params, $options);
    }

    public function send($text)
    {
        $decodedText = urldecode($text);

        $response = $this->post_with_headers(REJAX_API_URL, json_encode([
            'app_key' => REJAX_APP_KEY,
            'app_secret' => REJAX_APP_SECRET,
            'channel' => 'sagtur-messenger',
            'text' =>  $decodedText,
        ], JSON_UNESCAPED_UNICODE), [
            'Content-Type: application/json'
        ]);
        echo $response;
    }
}