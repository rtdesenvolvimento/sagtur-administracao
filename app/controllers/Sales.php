<?php defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpWord\IOFactory;
use Dompdf\Dompdf;

class Sales extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->load->model('sales_model');
        $this->load->model('products_model');
        $this->load->model('companies_model');
        $this->load->model('settings_model');
        $this->load->model('destino_model');
        $this->load->model('Roomlist_model');
        $this->load->model('Meiodivulgacao_model');

        $this->load->model('repository/TipoQuartoRepository_model', 'TipoQuartoRepository_model');
        $this->load->model('repository/VendaRepository_model', 'VendaRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');
        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/CupomDescontoRepository_model', 'CupomDescontoRepository_model');
        $this->load->model('repository/ValorFaixaRepository_model', 'ValorFaixaRepository_model');
        $this->load->model('repository/EventSaleRepository_model', 'EventSaleRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');
        $this->load->model('repository/BusRepository_model', 'BusRepository_model');
        $this->load->model('repository/MotivoCancelamentoRepository_model', 'MotivoCancelamentoRepository_model');
        $this->load->model('repository/RatingRepository_model', 'RatingRepository_model');

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        $this->load->model('model/Venda_model', 'Venda_model');
        $this->load->model('model/MotivoCancelamentoVenda_model', 'MotivoCancelamentoVenda_model');

        //$this->digital_upload_path = 'files/';
        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '16384';

        $this->data['logo'] = true;
    }

    public function index($programacaoId = null)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $flDataVendaDe = date('Y-01-01');
        $flDataVendaAte = date('Y-12-31');

        $ano = date('Y');
        $mes = date('m');

        $this->data['ano'] = $ano;
        $this->data['mes'] = $mes;

        //$this->data['flDataVendaDe'] = $flDataVendaDe;
        //$this->data['flDataVendaAte'] = $flDataVendaAte;

        $this->data['billers']  = $this->site->getAllCompanies('biller');
        $this->data['meiosDivulgacao']      = $this->Meiodivulgacao_model->getAll();

        //$this->data['programacoes'] = $this->sales_model->getProgramacaoAll();
        $this->data['productId'] = $programacaoId;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/index', $meta, $this->data);
    }

    public function canceled($warehouse_id = null)
    {
        //$this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['productId'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['productId'] = $this->session->userdata('warehouse_id');

            $this->data['warehouses'] =  $this->site->getAllWarehouses();;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $unit = 'Confirmado';
        $ano = date('Y');
        $mes = date('m');

        $this->data['ano'] = $ano;
        $this->data['mes'] = $mes;
        $this->data['programacoes'] = $this->sales_model->getProgramacaoAll($unit, $ano, $mes);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/canceled', $meta, $this->data);
    }

    public function archived_sales($warehouse_id = null)
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['productId'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['productId'] = $this->session->userdata('warehouse_id');

            $this->data['warehouses'] =  $this->site->getAllWarehouses();;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $unit = 'Confirmado';
        $ano = date('Y');
        $mes = date('m');

        $this->data['ano'] = $ano;
        $this->data['mes'] = $mes;
        $this->data['programacoes'] = $this->sales_model->getProgramacaoAll($unit, $ano, $mes);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales_archived')));
        $meta = array('page_title' => lang('sales_archived'), 'bc' => $bc);
        $this->page_construct('sales/archived', $meta, $this->data);
    }

    public function getSalesCanceleds($warehouse_id = null, $unit = NULL)
    {
        if ($warehouse_id == 'all') {
            $warehouse_id = NULL;
        }

        if ($warehouse_id == null) {
            $warehouse_id = $this->input->post('warehouse_id');
        }

        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $attrib = array('target' => 'newBlank');

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li class="divider"></li>
                    <li>' . $email_link . '</li>
                </ul>
            </div></div>';

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("sma_sales.id as id, 
                    sma_sales.date, 
                    sma_sales.reference_no,
                    sma_sales.vendedor,
                    customer,
                    sma_sales.sale_status, 
                    sma_sales.grand_total, 
                    sma_sales.paid, 
                    (sma_sales.grand_total-sma_sales.paid) as balance,
                    sma_sales.payment_status")
                ->from('sma_sales');

            $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
            $this->datatables->where('sale_items.programacaoId', $warehouse_id);
            $this->datatables->group_by('sales.id');
        } else {
            $this->datatables
                ->select("sales.id as id, 
                    date, 
                    reference_no, 
                    CONCAT(vendedor), 
                    customer,
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status")
                ->from('sales');
        }

        $this->datatables->where('pos !=', 1);
        $this->datatables->where("payment_status", "cancel");

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getSalesArchived($warehouse_id = null, $unit = NULL)
    {
        $eventos_venda = '<a href="'.base_url().'salesutil/events/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-archive"></i> Eventos da Venda</a>';
        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('salesutil/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $attrib = array('target' => 'newBlank');

        /*
        $delete_link = "<a href='#' class='po' title='<b>" . lang("cancelar_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/cancelar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('cancelar_sale') . "</a>";*/

        $delete_link = anchor('salesutil/motivo_cancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_sale'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        $unarchive_link = "<a href='#' class='po' title='<b>" . lang("unarchive_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/unarchive/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-archive'></i> "
            . lang('unarchive_sale') . "</a>";

        $compartilhar_venda_whatsapp = anchor(base_url().'appcompra/whatsapp/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_dados_venda_whatsapp'), $attrib);
        $compartilhar_voucher_whatsapp = anchor(base_url().'appcompra/whatsapp_pdf_atualizado/$1?token='.$this->session->userdata('cnpjempresa'), '<i class="fa fa-whatsapp"></i> ' . lang('compartilhar_voucher_whatsapp'), $attrib);
        $whatsapp_send_message_link = anchor('sales/whatsapp_message/$1', '<i class="fa fa-whatsapp"></i> ' . lang('whatsapp_send'), $attrib);

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $eventos_venda . '</li>
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li class="divider"></li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li class="divider"></li>
                    <li>' . $whatsapp_send_message_link . '</li>
                    <li>' . $compartilhar_venda_whatsapp . '</li>
                    <li>' . $compartilhar_voucher_whatsapp . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $unarchive_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("sma_sales.id as id, 
                    sma_sales.date, 
                    sma_sales.reference_no,
                    sma_sales.vendedor,
                    sma_sales.customer,
                    sma_sales.sale_status, 
                    sma_sales.grand_total, 
                    sma_sales.paid, 
                    (sma_sales.grand_total-sma_sales.paid) as balance,
                    sma_sales.payment_status")
                ->from('sma_sales');

            $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
            $this->datatables->where('sale_items.programacaoId', $warehouse_id);
            $this->datatables->group_by('sales.id');

            $this->datatables->where('pos !=', 1);
            $this->datatables->where("archived", 1);//TODO SOMENTE VENDAS ARQUIVADAS

        } else {
            $this->datatables
                ->select("sales.id as id, 
                    date, 
                    reference_no, 
                    CONCAT(vendedor), 
                    customer,
                    sale_status, 
                    grand_total, 
                    paid, 
                    (grand_total-paid) as balance, 
                    payment_status")
                ->from('sales');
        }

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        $this->datatables->where('pos !=', 1);
        $this->datatables->where("archived", 1);//TODO SOMENTE VENDAS ARQUIVADAS

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function getFilial() {
        echo 'filial= '.$this->session->userdata('warehouse_id');
    }

    public function return_sales($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $user->warehouse_id;
            $this->data['warehouse'] = $user->warehouse_id ? $this->site->getWarehouseByID($user->warehouse_id) : null;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('return_sales')));
        $meta = array('page_title' => lang('return_sales'), 'bc' => $bc);
        $this->page_construct('sales/return_sales', $meta, $this->data);
    }

    public function getReturns($warehouse_id = null)
    {
        $this->sma->checkPermissions('return_sales', true);

        if (!$this->Owner && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("date, return_sale_ref, reference_no, biller, customer, surcharge, grand_total, id")
                ->from('sales')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("date, return_sale_ref, reference_no, biller, customer, surcharge, grand_total, id")
                ->from('sales');
        }
        $this->datatables->where('sale_status', 'returned');

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('customer_id'));
        }

        echo $this->datatables->generate();
    }

    public function view($id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $return = $this->sales_model->getReturnBySID($id);

        $this->data['parcelasOrcamento'] = $this->sales_model->getParcelasOrcamentoVenda($id);
        $this->data['faturas'] = $this->financeiro_model->getParcelasFaturaByContaReceber($id);

        $this->data['return_sale'] = $return;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['paypal'] = $this->sales_model->getPaypalSettings();
        $this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->page_construct('sales/view', $meta, $this->data);
    }

    public function view_return($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getReturnByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllReturnItems($id);
        $this->data['sale'] = $this->sales_model->getInvoiceByID($inv->sale_id);
        $this->load->view($this->theme.'sales/view_return', $this->data);
    }

    public function montarHotel($id = null, $idVariante, $contadorHotel) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $hoteis = $this->sales_model->getProductOptions_Hotel($id, $idVariante);

        $this->data['inv']                  = $this->sales_model->getInvoiceByID($id);
        $this->data['itens_pedidos']        = $this->sales_model->getInvoiceByProduct($id);
        $this->data['itens_pedidos_group']  = $this->sales_model->getInvoiceByProductGroupBySale($id);
        $this->data['hotel']                = $hoteis[0];
        $this->data['idVariante']           = $idVariante;
        $this->data['product_id']           = $id;
        $this->data['contadorHotel']        = $contadorHotel;

       // print_r($hoteis[0]->fornecedor);
        //die();

        $meta = array('page_title' => lang('sales'), 'bc' => '');
        $this->page_construct('sales/montarHotel', $meta, $this->data);
    }

    public function montarPoltronas($produtoId=NULL, $tipoTransporteId=NULL, $programacaoId=NULL) {

        $variacaoFilter = null;

        if ($this->input->get('id')) $produtoId = $this->input->get('id');
        if ($this->input->get('tipoTransporteId')) $tipoTransporteId = $this->input->get('tipoTransporteId');
        if ($this->input->get('programacaoId')) $programacaoId = $this->input->get('programacaoId');

        if ($this->input->get('localEmbarque')) $localEmbarque = $this->input->get('localEmbarque');
        if ($this->input->get('sem_poltronas')) $semPoltrona = $this->input->get('sem_poltronas');
        if ($this->input->get('idVariante')) $variacaoFilter = $this->input->get('idVariante');

        $this->data['inv']              = $this->sales_model->getInvoiceByID($produtoId);
        $this->data['embarques']        = $this->site->getLocaisEmbarque();
        $this->data['itens_pedidos']    = $this->sales_model->getInvoiceByProductOrderByPoltrona($produtoId, $programacaoId, NULL, $localEmbarque);
        $this->data['tiposTransporte']  = $this->ProdutoRepository_model->getTransportes($produtoId);

        $this->data['product_id']       = $produtoId;
        $this->data['tipoTransporteId'] = $tipoTransporteId;
        $this->data['programacaoId']    = $programacaoId;
        $this->data['embarqueFilter']   = $localEmbarque;
        $this->data['sem_poltronas']    = $semPoltrona;
        $this->data['idVariante']       = $variacaoFilter;

        $meta = array('page_title' => lang('sales'), 'bc' => '');
        $this->page_construct('sales/montarPoltronas', $meta, $this->data);
    }

    public function verificaPoltrona($product_id,$poltrona,$variacao) {
        $iten = $this->sales_model->getSaleByItensByPoltrona($product_id, $poltrona, $variacao);
        echo json_encode($iten);
    }

    public function atribuir_local_embarque() {
        $localEmbarque = $this->input->get('localEmbarque');
        $itemId = $this->input->get('itemId');

        $this->sales_model->atualizarLocalEmbarque($itemId, $localEmbarque);
    }

    public function atribuir_tipo_transporte() {
        $tipoTransporte = $this->input->get('tipoTransporte');
        $itemId = $this->input->get('itemId');

        $this->sales_model->atualizarTipoTransporte($itemId, $tipoTransporte);
    }

    public function excluir_hotel($sale_id, $ordem) {
        $this->sales_model->atualizarHotel($sale_id, $ordem);
    }

    public function  salvar_hotel() {

        $sale_id            = $this->input->get('sale_id');
        $idVariante         = $this->input->get('idVariante');
        $product_id         = $this->input->get('product_id');
        $fornecedor_id      = $this->input->get('fornecedor_id');
        $tipo_quarto        = $this->input->get('tipo_quarto');
        $contadorHotel      = $this->input->get('contadorHotel');

        $customer_1_id 		= $this->input->get('customer_1');
        $customer_2_id 		= $this->input->get('customer_2');
        $customer_3_id 		= $this->input->get('customer_3');
        $customer_4_id 		= $this->input->get('customer_4');
        $customer_5_id 		= $this->input->get('customer_5');

        $customer_1			= null;
        $customer_2			= null;
        $customer_3			= null;
        $customer_4			= null;
        $customer_5			= null;

        if ($customer_1_id) {
            $customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
            $customer_1 			= $customer_details_1->name;
        }

        if ($customer_2_id) {
            $customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
            $customer_2 			= $customer_details_2->name;
        }

        if ($customer_3_id) {
            $customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
            $customer_3 			= $customer_details_3->name;
        }

        if ($customer_4_id) {
            $customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
            $customer_4 			= $customer_details_4->name;
        }

        if ($customer_5_id) {
            $customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
            $customer_5 			= $customer_details_5->name;
        }


        if ($contadorHotel > 0) {
            $numeroHotel = 'hotel'.$contadorHotel;
        } else {
            $numeroHotel = 'hotel';
        }
        $data = array(
            'customer_1_id' 		=> $customer_1_id,
            'customer_2_id' 		=> $customer_2_id,
            'customer_3_id' 		=> $customer_3_id,
            'customer_4_id' 		=> $customer_4_id,
            'customer_5_id' 		=> $customer_5_id,
            'customer_1' 			=> $customer_1,
            'customer_2' 			=> $customer_2,
            'customer_3' 			=> $customer_3,
            'customer_4' 			=> $customer_4,
            'customer_5' 			=> $customer_5,
            $numeroHotel 	        => $fornecedor_id,
            'tipo_quarto' 		    => $tipo_quarto,
        );
        $this->sales_model->atualizarVendaHotel($sale_id, $data);
        redirect('sales/montarHotel/'.$product_id.'/'.$idVariante.'/'.$contadorHotel);
    }

    public function meuticket($id = null, $customer = null ){
        echo $this->sales_model->buscarValcherEmFormaDeImagem($id, $customer);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
          //  $this->sma->view_rights($inv->created_by);
        }

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
        $this->data['parcelasOrcamento'] = $this->sales_model->getParcelasOrcamentoVenda($id);

        $return  = $this->sales_model->getReturnBySID($id);

        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['adicionais'] = $this->sales_model->getAllInvoiceItemsAdicionais($id);
        $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;
        //$this->data['paypal'] = $this->sales_model->getPaypalSettings();
        //$this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            if ($inv->sale_status == 'orcamento') {
                $name = lang("orcamento") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'cancel') {
                $name = lang("orcamento") .'_'.lang("cancel"). "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/cancelado.png'));
            } else {
                $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
                return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
            }
         } else {
            if ($inv->sale_status == 'orcamento') {
                $name = lang("orcamento") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'cancel') {
                $name = lang("sale") . '_'. lang("cancel") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/cancelado.png'));
            } else {
                $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa'). ".pdf";
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }
	
	function pdf_hotel($id = NULL , $programacaoId = NULL)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadasOrderByTipoTransporte($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens_pedidos']  = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'Relatório por Tipo de Hospedagem '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/pdf_hotel', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_geral_ferroviario($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasTodosPassageiros($id, $programacaoId, NULL, 'sales.reference_no');

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO GERAL DE ATIVIDADES '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_geral_ferroviario', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_geral_ferroviario_excel($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasTodosPassageiros($id, $programacaoId, NULL, 'sales.reference_no');

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $dataPorExtenso = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$dataPorExtenso;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle($dataPorExtenso);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('DATA'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('HORA'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('Assento'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('Passageiro'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('Nº Voucher.'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('E-mail'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('Contato'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('Atividade'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('Faixa'));

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->SetCellValue('J1', lang('Receber'));
        }

        $row = 2;
        foreach ($itens_pedidos as $item) {

            if (!$this->Customer &&
                !$this->Supplier &&
                !$this->Owner &&
                !$this->Admin) {

                if ($row->payment_status == 'due') {
                    continue;
                }
            }

            $totalAbertoPorDependente = '';

            $customer = $this->site->getCompanyByID($item->customerClient);

            $whatsApp = $customer->cf5;
            $phone = $customer->phone;
            $telefone_emergencia = $customer->telefone_emergencia;
            $email = $customer->email;

            $contato = '';

            if ($whatsApp) {
                $contato = $whatsApp;
            }
            if ($phone) {
                $contato = $contato.' '.$phone;
            }
            if ($telefone_emergencia) {
                $contato = $contato.' '.$telefone_emergencia;
            }

            if ($this->Settings->show_payment_report_shipment) {

                $totalPagarItem = $item->subtotal;
                $acrescimo      = $item->shipping;
                $desconto       = $item->order_discount;
                $totalVenda     = $item->grand_total - $acrescimo + $desconto;

                $totalRealPagoPorDependente = $totalPagarItem * (($item->paid*100/$totalVenda)/100);
                $totalAberto   =  $totalPagarItem - $totalRealPagoPorDependente;

                if ($totalAberto > 0) {
                    $totalAbertoPorDependente = $this->sma->formatMoney($totalAberto);

                    if ($acrescimo > 0) {
                        $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Acres: '.$this->sma->formatMoney($acrescimo).'</small>';
                    }

                    if ($desconto > 0) {
                        $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Desc:'.$this->sma->formatMoney($desconto).'</small>';
                    }

                    if ($acrescimo > 0 || $desconto > 0) {
                        $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Pagar:'.$this->sma->formatMoney($totalAberto + $acrescimo - $desconto).'</small>';
                    }

                } else {
                    $totalAbertoPorDependente = 'PAGO';
                }

            } else {
                if ($row->customer_id == $row->customerClient) {
                    $totalEmAberto = $item->grand_total - $item->paid;
                    if ($totalEmAberto > 0) {
                        $totalAbertoPorDependente = $this->sma->formatMoney($totalEmAberto);
                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                } else {
                    $totalAbertoPorDependente = 'PAGO';
                }
            }

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($programacao->dataSaida));
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $this->sma->hf($programacao->horaSaida));
            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $item->poltronaClient);
            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $item->reference_no);
            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $email);
            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $contato);
            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $item->product_name);
            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $item->faixaNome);

            if ($this->Settings->show_payment_report_shipment) {
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $totalAbertoPorDependente);
            }

            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(25);
        }

        // Definindo o estilo de fonte para as colunas
        $styleArray = [
            'font' => [
                'bold' => true,
                'size' => 15,
            ],
        ];

        if ($this->Settings->show_payment_report_shipment) {
            $this->excel->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        } else {
            $this->excel->getActiveSheet()->getStyle('A1:I1')->applyFromArray($styleArray);
        }

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO PASSAGEIROS POR ATIVIDADE '.$nomeViagem;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function relatorio_geral_passageiros_todos($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasTodosPassageiros($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO GERAL DE PASSAGEIROS '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_geral_passageiros_todos', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_adicional($produtoAdicionalId, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasTodosPassageiros($produtoAdicionalId, $programacaoId);

        if (!$produtoAdicionalId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $adicional = $this->site->getProductByID($produtoAdicionalId);

        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens']        = $itens_pedidos;
        $this->data['product']      = $adicional;
        $this->data['programacao']  = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeAdicional = strtoupper($adicional->name).' '.$data;

        $name = 'RELATÓRIO ADICIONAIS '.$nomeAdicional.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_adicional', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_geral_passageiros($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO GERAL DE PASSAGEIROS '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_geral_passageiros', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_seguradora_de_passageiros($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO SEGURO '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_seguradora_de_passageiros', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_autorizacao_imagem($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ASSINATURA '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_autorizacao_imagem', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_assinatura($id, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ASSINATURA '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_assinatura', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_assinatura_transporte($id, $programacaoId, $transporteID)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $transporteID);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;
        $this->data['tipoTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($transporteID);

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ASSINATURA POR TIPO DE TRANSPORTE '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_assinatura_transporte', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_enviado_empresa_onibus($id, $tipoTransporte, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens'] = $itens_pedidos;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;
        $this->data['tipoTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporte);

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ANTT '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_enviado_empresa_onibus', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');

    }

    function relatorio_enviado_empresa_onibus_excel($id, $tipoTransporte, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $dataPorExtenso = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem     = strtoupper($product->name).' '.$dataPorExtenso;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle($dataPorExtenso);
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('customer'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('vat_no'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('Documento'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('Nascimento'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('Embarque'));

        $row = 2;
        foreach ($itens_pedidos as $item) {
            $customer       = $this->site->getCompanyByID($item->customerClient);
            $localEmbarque  = $this->site->getLocalEmbarqueByID($item->localEmbarque);

            $rg             = $customer->cf1;
            $orgaoEmissor   = $customer->cf3;
            $tipoDocumento  = $customer->tipo_documento;
            $data_aniversario   = $customer->data_aniversario;

            if ($data_aniversario) {
                $data_aniversario = $this->sma->hrsd($data_aniversario);
            }

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->vat_no);
            $this->excel->getActiveSheet()->SetCellValue('C' . $row, strtoupper(lang($tipoDocumento)).': '.$rg.' - '.$orgaoEmissor);
            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_aniversario);
            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $localEmbarque->name);

            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(50);


        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO ANTT '.$nomeViagem;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function artesp_pdf($id, $tipoTransporte, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens']            = $itens_pedidos;
        $this->data['product']          = $product;
        $this->data['programacao']      = $programacao;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporte);

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ARTESP '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/artesp', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function artesp_pdf_rg($id, $tipoTransporte, $programacaoId)
    {
        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['itens']            = $itens_pedidos;
        $this->data['product']          = $product;
        $this->data['programacao']      = $programacao;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporte);

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO ARTESP  RG '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/artesprg', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    public function artesp_excel_rg($id, $tipoTransporte, $programacaoId)
    {

        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $dataPorExtenso = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem     = strtoupper($product->name).' '.$dataPorExtenso;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle($dataPorExtenso);
        $this->excel->getActiveSheet()->SetCellValue('A1', 'Nome do Passageiro');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'Número do Documento (RG)');

        $row = 2;
        foreach ($itens_pedidos as $item) {
            $customer   = $this->site->getCompanyByID($item->customerClient);

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->cf1.' '.$customer->cf3);
            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO ARTESP '.$nomeViagem;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    public function artesp_excel_cpf($id, $tipoTransporte, $programacaoId)
    {

        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporte);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $dataPorExtenso = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem     = strtoupper($product->name).' '.$dataPorExtenso;

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle($dataPorExtenso);
        $this->excel->getActiveSheet()->SetCellValue('A1', 'Nome do Passageiro');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'Número do Documento (CPF)');

        $row = 2;
        foreach ($itens_pedidos as $item) {
            $customer   = $this->site->getCompanyByID($item->customerClient);

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->vat_no);
            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(50);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'RELATÓRIO ARTESP '.$nomeViagem;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function relatorioEmbarqueAgrupadoPorLocalDeEmbarque($produtoId, $tipoTransporteId, $programacaoId)
    {

        $isActiveLocalEmbarque = false;

        $locaisDeEmbarque = $this->ProdutoRepository_model->getLocaisEmbarque($produtoId, $isActiveLocalEmbarque);

        if (!$produtoId  || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($produtoId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorioEmbarqueAgrupadoPorLocalEmbarque', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorioEmbarqueAgrupadoPorLocalDeEmbarque_com_cpf($produtoId, $tipoTransporteId, $programacaoId)
    {
        $locaisDeEmbarque = $this->ProdutoRepository_model->getLocaisEmbarque($produtoId);

        if (!$produtoId  || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($produtoId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorioEmbarqueAgrupadoPorLocalEmbarqueComCPF', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_embarque_assentos($produtoId, $tipoTransporteId, $programacaoId)
    {

        $isActiveLocalEmbarque = false;

        $locaisDeEmbarque = $this->ProdutoRepository_model->getLocaisEmbarque($produtoId, $isActiveLocalEmbarque);

        if (!$produtoId  || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($produtoId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_embarque_assentos', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_embarque_assentos_ordem_assentos($produtoId, $tipoTransporteId, $programacaoId)
    {

        $isActiveLocalEmbarque = false;

        $locaisDeEmbarque = $this->ProdutoRepository_model->getLocaisEmbarque($produtoId, $isActiveLocalEmbarque);

        if (!$produtoId  || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($produtoId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['tipoTransporte']   = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE ORDEM ASSENTOS '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_embarque_assentos_ordem_assento', $this->data, TRUE);

        $this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');
    }

    function relatorio_assentos($produtoId, $tipoTransporteId, $programacaoId)
    {
        $locaisDeEmbarque = $this->ProdutoRepository_model->getLocaisEmbarque($produtoId);

        if (!$produtoId  || !$tipoTransporteId || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($produtoId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $tipoTransporte = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporteId);

        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['tipoTransporte']   = $tipoTransporte;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE '.$nomeViagem.'.pdf';

        $this->data['assentos']         =  $this->BusRepository_model->getMapaByAutomovel($tipoTransporte->automovel_id);
        $this->data['bloqueados']       =  $this->BusRepository_model->getAssentosBloqueados($produtoId, $programacaoId, $tipoTransporte->id);

        //$html = $this->load->view($this->theme . 'products/relatorio_assentos', $this->data, TRUE);
        //$this->sma->generate_pdf($html, $name, 'I', '<p class="text-left;" style="border-top: 1px solid #0b0b0b;">'.$this->Settings->site_name.' - Emissão '.date('d/m/Y H:i').'</p>');

        $this->load->view($this->theme . 'products/relatorio_assentos', $this->data);
    }

    function relatorio_embarque($id, $tipoTransporte, $programacaoId)
    {
    	$itens_pedidos = $this->sales_model->getInvoiceByProduct($id, $programacaoId);

    	if (!$id || !$itens_pedidos || !$tipoTransporte | !$programacaoId) {
    		$this->session->set_flashdata('error', lang('viagem_item_not_found'));
    		redirect($_SERVER["HTTP_REFERER"]);
    	}

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);

        $this->data['tipoTransporte'] = $this->TipoTransporteRodoviarioRepository_model->getTipoTransporte($tipoTransporte);
    	$this->data['product_id'] = $id;
        $this->data['product'] = $product;
        $this->data['programacao'] = $programacao;

        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO DE EMBARQUE '.$nomeViagem.'.pdf';

        $html = $this->load->view($this->theme . 'products/relatorio_embarque', $this->data, TRUE);
    	$this->sma->generate_pdf($html, $name);
    }


    function relatorio_embarque_ordem_alfabetica($id = NULL, $idVariacao = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductOrderByCliente($id);

        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $name = "relatorio_embarque.pdf";

        $onibus = $this->sales_model->getProductOptions_Onibus($id, $idVariacao);

        $this->data['listOnibus'] = $onibus;
        $this->data['product_id'] = $id;
        $this->data['itens_pedidos'] = $itens_pedidos;
        $this->data['product'] = $this->site->getProductByID($id);

        $html = $this->load->view($this->theme . 'products/relatorio_embarque_ordem_alfabetica', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

	function pdf_aduana($id = NULL, $programacaoId)
    {

        $itens_pedidos = $this->sales_model->getInvoiceByProduct($id, $programacaoId);

        if (!$id || !$itens_pedidos || !$programacaoId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product = $this->site->getProductByID($id);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO INTERNACIONAL ADUANA '.$nomeViagem.'.pdf';

        $this->data['itens_pedidos'] = $itens_pedidos;
		$html = $this->load->view($this->theme . 'sales/aduana', $this->data, TRUE);


        //$this->sma->generate_pdf($html, $name. date('d/m/y'), TRUE, 'A4-L');
        $this->sma->generate_pdf($html, $name, $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');

    }

    function pdf_aduana_transporte($id = NULL, $programacaoId, $tipoTransporteId)
    {

        $itens_pedidos = $this->sales_model->getItensVendasFaturadas($id, $programacaoId, $tipoTransporteId);

        if (!$id || !$itens_pedidos || !$programacaoId || !$tipoTransporteId) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $product        = $this->site->getProductByID($id);
        $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $data           = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
        $nomeViagem     = strtoupper($product->name).' '.$data;

        $name = 'RELATÓRIO INTERNACIONAL ADUANA '.$nomeViagem.'.pdf';

        $this->data['itens_pedidos'] = $itens_pedidos;
        $html = $this->load->view($this->theme . 'sales/aduana', $this->data, TRUE);

        //$this->sma->generate_pdf($html, $name. date('d/m/y'), TRUE, 'A4-L');
        $this->sma->generate_pdf($html, $name, $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');

    }

    public function emitir_contrato($sales_id) {
        try {
            $this->load->model('service/ContratoService_model', 'ContratoService_model');

            $this->ContratoService_model->emitir($sales_id);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function combine_pdf($sales_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($sales_id as $id) {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $inv = $this->sales_model->getInvoiceByID($id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
            $this->data['user'] = $this->site->getUser($inv->created_by);
            $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
            $this->data['inv'] = $inv;
            $return = $this->sales_model->getReturnBySID($id);
            $this->data['return_sale'] = $return;
            $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
            $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;

            $html[] = array(
                'content' => $this->load->view($this->theme . 'salesutil/pdf', $this->data, true),
                'footer' => $this->data['biller']->invoice_footer,
            );
        }

        $name = lang("sales") . ".pdf";
        $this->sma->generate_pdf($html, $name);

    }

    public function whatsapp_message($id = null) {

        $inv = $this->sales_model->getInvoiceByID($id);
        $customer = $this->site->getCompanyByID($inv->customer_id);

        $phone = str_replace('(', '', str_replace(')', '', $customer->cf5));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        header('Location: https://api.whatsapp.com/send?phone=55' . trim($phone));
    }

    public function decode_html($str)
    {
        return strip_tags(html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
    }

    private function getEspacoHtmlWhatsApp()
    {
        return '%20';
    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {

            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->company,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);

            $biller = $this->site->getCompanyByID($inv->biller_id);
            $paypal = $this->sales_model->getPaypalSettings();
            $skrill = $this->sales_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';

            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=FC-BuyNow&rm=2&return=' . site_url('sales/view/' . $inv->id) . '&cancel_return=' . site_url('sales/view/' . $inv->id) . '&notify_url=' . site_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';

            }

            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                $btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . site_url('sales/view/' . $inv->id) . '&cancel_url=' . site_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . site_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }

            $btn_code .= '<div class="clearfix"></div></div>';

            $message = '<!DOCTYPE html>
                        <html lang="pt-br">
                            <head>
                                <meta charset="UTF-8">
                            </head>
                            <body>'.
                                $message . $btn_code.'
                            </body>
                        </html>';
            $attachment = $this->pdf($id, null, 'S');
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true) {

            if ($this->Settings->is_email_queue) {
                $this->sma->send_email_queue($this->db, 'sales', 'forwarding', $inv->id, $to, $subject, $message, null, null, $attachment, $cc, '');
            } else {
                $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc);
                delete_files($attachment);
            }

            $this->session->set_flashdata('message', lang("email_sent"));
            redirect("sales");
        } else {

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
                if ($inv->sale_status == 'orcamento') {
                    $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/orcamento.html');
                } else if ($inv->sale_status == 'lista_espera') {
                    $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/lista_espera.html');
                } else if ($inv->sale_status == 'cancel'){
                    $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
                } else {
                    $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
                }
            } else {
                if ($inv->sale_status == 'orcamento') {
                    $sale_temp = file_get_contents('./themes/default/views/email_templates/orcamento.html');
                } else if ($inv->sale_status == 'lista_espera') {
                    $sale_temp = file_get_contents('./themes/default/views/email_templates/lista_espera.html');
                } else if ($inv->sale_status == 'cancel'){
                    $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
                } else {
                    $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
                }
            }

            if ($inv->sale_status == 'orcamento') {
                $this->data['subject'] = array('name' => 'subject',
                    'id' => 'subject',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('subject', lang('orcamento') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
                );
            } else if ($inv->sale_status == 'lista_espera') {
                $this->data['subject'] = array('name' => 'subject',
                    'id' => 'subject',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('subject', lang('sale'). ' '. lang('lista_espera') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
                );
            } else if ($inv->sale_status == 'cancel')  {
                $this->data['subject'] = array('name' => 'subject',
                    'id' => 'subject',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('subject', lang('sale') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
                );
            } else {
                $this->data['subject'] = array('name' => 'subject',
                    'id' => 'subject',
                    'type' => 'text',
                    'value' => $this->form_validation->set_value('subject', lang('sale') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
                );
            }

            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $sale_temp),
            );

            $this->data['id'] = $id;
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/email', $this->data);
        }
    }

    public function send_email($id = null, $action = 'create') {

        $inv = $this->sales_model->getInvoiceByID($id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);

        $this->load->library('parser');

        $this->parser->set_delimiters('{', '}');

        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
        );

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            if ($inv->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/orcamento.html');
            } else if ($inv->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/lista_espera.html');
            } else if ($inv->sale_status == 'cancel') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale_cancel.html');
            } else {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            }
        } else {
            if ($inv->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/orcamento.html');
            } else if ($inv->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/lista_espera.html');
            } else if ($inv->sale_status == 'cancel')  {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale_cancel.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
            }
        }

        if ($inv->sale_status == 'orcamento') {
            $subject = lang('orcamento') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name;
        } else if ($inv->sale_status == 'lista_espera') {
            $subject = lang('sale'). ' '. lang('lista_espera') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name;
        } else if ($inv->sale_status == 'cancel') {
            $subject = lang('sale'). ' '. lang('cancel') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name;
        } else {
            $subject = lang('sale') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name;
        }

        if ($customer->email) {
            $to = $customer->email;
            $cc = $biller->email;
        } else {
            $to = $biller->email;
        }

        $attachment = $this->pdf($id, null, 'S');
        $message = $this->parser->parse_string($sale_temp, $parse_data);

        if ($this->Settings->is_email_queue) {
            $this->sma->send_email_queue($this->db, 'sales', $action, $inv->id, $to, $subject, $message, null, null, $attachment, $cc, '');
        } else {
            $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, '');
            delete_files($attachment);
        }

    }

    /* ------------------------------------------------------------------ */
    public function add($quote_id = null)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        //$this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');
        $this->form_validation->set_rules('tipoCobrancaId', lang("tipo_cobranca"), 'required');
        $this->form_validation->set_rules('condicaopagamentoId', lang("condicao_pagamento"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            if ($this->Owner || $this->Admin) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = date('Y-m-d H:i:s');

            $warehouse_id 			= $this->input->post('warehouse');
			$reference_no_variacao 	= $this->input->post('reference_no_variacao');
            $totalHoteis			= $this->input->post('totalHoteis');
            $supplier_id			= $this->input->post('supplier_id');
            $supplier_id2			= $this->input->post('supplier_id2');
            $supplier_id3			= $this->input->post('supplier_id3');
            $supplier_id4			= $this->input->post('supplier_id4');
            $supplier_id5			= $this->input->post('supplier_id5');
            $supplier_id6			= $this->input->post('supplier_id6');
            $supplier_id7			= $this->input->post('supplier_id7');

            if ($totalHoteis == 1) {
                $supplier_id2 = null;
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 2) {
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 3) {
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 4) {
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 5) {
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 6) {
                $supplier_id7 = null;
            }

            $customer_id 			= $this->input->post('customer');
			
            $customer_1_id 		= $this->input->post('form_slcustomer_1');
            $customer_2_id 		= $this->input->post('form_slcustomer_2');
            $customer_3_id 		= $this->input->post('form_slcustomer_3');
            $customer_4_id 		= $this->input->post('form_slcustomer_4');
            $customer_5_id 		= $this->input->post('form_slcustomer_5');
			
			$customer_1			= null;
			$customer_2			= null;
			$customer_3			= null;
			$customer_4			= null;
			$customer_5			= null;
			
			if ($customer_1_id) {
				$customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
				$customer_1 			= $customer_details_1->name;
			}
			
			if ($customer_2_id) {
				$customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
				$customer_2 			= $customer_details_2->name;
			}
			
			if ($customer_3_id) {
				$customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
				$customer_3 			= $customer_details_3->name;
			}
			
			if ($customer_4_id) {
				$customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
				$customer_4 			= $customer_details_4->name;
			}
			
			if ($customer_5_id) {
				$customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
				$customer_5 			= $customer_details_5->name;
			}

            $note_hotel 		= $this->input->post('form_slnote_hotel');
            $tipo_quarto 		= $this->input->post('form_sltipo_quarto');
            $biller_id          = $this->input->post('biller');

            if (!$biller_id) $biller_id = $this->session->userdata('biller_id');

            $total_items 		= $this->input->post('total_items');
            $sale_status 		= $this->input->post('sale_status');

            $payment_status 	= $this->input->post('payment_status');
            $payment_term 		= $this->input->post('payment_term');
            $due_date 			= $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping 			= $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details 	= $this->site->getCompanyByID($customer_id);
            $customer 			= $customer_details->name;
            $biller_details 	= $this->site->getCompanyByID($biller_id);
            $biller 			= $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note 				= $this->sma->clear_tags($this->input->post('note'));
            $staff_note 		= $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id 			= $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
			$local_saida 		= $this->input->post('local_saida');
            $condicao_pagamento = $this->input->post('condicao_pagamento');

            $total 				= 0;
            $product_tax 		= 0;
            $order_tax 			= 0;
            $product_discount 	= 0;
            $order_discount 	= 0;
            $percentage 		= '%';
            $i 					= isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
			
            for ($r = 0; $r < $i; $r++) {

                $item_id 			= $_POST['product_id'][$r];
                $customer_dependent = $this->site->getCompanyByID($_POST['customerClient'][$r]);

                if (!$reference_no_variacao) {
                    $onibus = $this->sales_model->buscar_Onibus_by_product($item_id);
                    foreach($onibus as $bus)  $reference_no_variacao = $bus->id;
                }

                $item_type 			= $_POST['product_type'][$r];
                $item_code 			= $_POST['product_code'][$r];
                $item_name 			= $_POST['product_name'][$r];

                $poltronaClient     = $_POST['poltronaClient'][$r];
                $customerClient 	= $customer_dependent->id;
                $customerClientName = $customer_dependent->name;

                $item_option 		= isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price	= $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price 		= $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity 		= $_POST['quantity'][$r];
                $item_serial 		= isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate		= isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount 		= isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {

                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;

                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $adicional = $_POST['adicional'][$r];

                    if ($adicional == 'true') $adicional = 1;
                    else $adicional = 0;

                    $item_id_code_row =  $_POST['item_id'][$r];

                    $k 	= isset($_POST['adicional_nomeServico_'.$item_id_code_row]) ? sizeof($_POST['adicional_nomeServico_'.$item_id_code_row]) : 0;
                    $servicosAdicionais = [];

                    for ($j = 0; $j < $k; $j++) {

                        $itemServicoAcional= $_POST['adicional_item_id_' . $item_id_code_row][$j];

                        if ($itemServicoAcional == $item_id_code_row) {
                            $servicosAdicionais[] = array(
                                'nome_produto' => $_POST['adicional_nomeServico_' . $item_id_code_row][$j],
                                'codigo_produto' => $_POST['adicional_servicoCode_' . $item_id_code_row][$j],
                                'categoria_produto' => $_POST['adicional_nmCategoria_' . $item_id_code_row][$j],
                                'produtoId' => $_POST['adicional_servicoId_' . $item_id_code_row][$j],
                                'valor' => $_POST['adicional_preco_' . $item_id_code_row][$j],
                                'quantidade' => 1,
                            );

                        }
                    }

                    $k 	= isset($_POST['origem_'.$item_id_code_row]) ? sizeof($_POST['origem_'.$item_id_code_row]) : 0;
                    $itinerario = [];

                    for ($j = 0; $j < $k; $j++) {

                        $itemItinerario = $_POST['itinerario_item_id_'.$item_id_code_row][$j];

                        if ($itemItinerario == $item_id_code_row) {

                            $itinerario[] = array(
                                'origem' => $_POST['origem_' . $item_id_code_row][$j],
                                'destino' => $_POST['destino_' . $item_id_code_row][$j],
                                'companhiaAerea' => $_POST['companhiaAerea_' . $item_id_code_row][$j],
                                'assentoVoo' => $_POST['assentoVoo_' . $item_id_code_row][$j],
                                'numeroVoo' => $_POST['numeroVoo_' . $item_id_code_row][$j],
                                'dataPartida' => $_POST['dataPartida_' . $item_id_code_row][$j],
                                'horaPartida' => $_POST['horaPartida_' . $item_id_code_row][$j],
                                'dataChegada' => $_POST['dataChegada_' . $item_id_code_row][$j],
                                'horaChegada' => $_POST['horaChegada_' . $item_id_code_row][$j],
                                'bagagem' => $_POST['bagagem_' . $item_id_code_row][$j],
                            );

                        }
                    }

                    $k 	= isset($_POST['tipoTransfer_'.$item_id_code_row]) ? sizeof($_POST['tipoTransfer_'.$item_id_code_row]) : 0;
                    $transfers = [];

                    for ($j = 0; $j < $k; $j++) {

                        $dataSaidaVooBus = $_POST['transfer_dataSaidaVooBus_'.$item_id_code_row][$j];
                        $horaSaidaBooBus = $_POST['transfer_horaSaidaBooBus_'.$item_id_code_row][$j];

                        $itemTransferencia = $_POST['transfer_item_id_'.$item_id_code_row][$j];

                        if ($itemTransferencia == $item_id_code_row) {

                            if ($dataSaidaVooBus == '') $dataSaidaVooBus = null;
                            if ($horaSaidaBooBus == '') $horaSaidaBooBus = null;

                            $transfers[] = array(
                                'tipoTransfer' => $_POST['tipoTransfer_' . $item_id_code_row][$j],
                                'origem' => $_POST['transfer_origem_' . $item_id_code_row][$j],
                                'destino' => $_POST['transfer_destino_' . $item_id_code_row][$j],
                                'data' => $_POST['transfer_data_' . $item_id_code_row][$j],
                                'hora' => $_POST['transfer_hora_' . $item_id_code_row][$j],
                                'dataEmissao' => $_POST['transfer_dataEmissao_' . $item_id_code_row][$j],

                                'dataSaidaVooBus' => $dataSaidaVooBus,
                                'horaSaidaBooBus' => $horaSaidaBooBus,
                                'referenciaVooBus' => $_POST['transfer_referenciaVooBus_' . $item_id_code_row][$j],

                                'referencia' => $_POST['transfer_referencia_' . $item_id_code_row][$j],
                                'fornecedor' => $_POST['transfer_fornecedor_' . $item_id_code_row][$j],
                            );
                        }
                    }

                    $dataApresentacao = $_POST['dataApresentacao'][$r];
                    $horaApresentacao = $_POST['horaApresentacao'][$r];

                    if ($dataApresentacao == '') $dataApresentacao = null;
                    if ($horaApresentacao == '') $horaApresentacao = null;

                    $products[] = array(

                        'itinerarios' => $itinerario,
                        'transfers' => $transfers,
                        'servicosAdicionais' => $servicosAdicionais,

                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'product_comissao' => $_POST['product_comissao'][$r],

                        'adicional'  =>  $adicional,

                        'faixaId'       => $_POST['faixaEtaria'][$r],
                        'faixaNome'     => $_POST['nomeFaixa'][$r],
                        'descontarVaga' => $_POST['descontarVaga'][$r],//TODO CONTROLA SE DESCONTA VAGAS.

                        'programacaoId'  => $_POST['programacaoId'][$r],
                        'servicoPai'  => $_POST['servicoPai'][$r],

                        'nmTipoTransporte'  => $_POST['nmTipoTransporte'][$r],
                        'tipoTransporte'  => $_POST['tipoTransporte'][$r],
                        'localEmbarque'  => $_POST['localEmbarque'][$r],
                        'tipoHospedagem'  => $_POST['tipoHospedagem'][$r],
                        'product_category' => $_POST['product_category'][$r],

                        'origem'  => $_POST['origem'][$r],
                        'destino'  => $_POST['destino'][$r],
                        'reserva'  => $_POST['reserva'][$r],
                        'nmFornecedor'  => $_POST['nmFornecedor'][$r],
                        'nmCategoria'  => $_POST['nmCategoria'][$r],
                        'tipoDestino'  => $_POST['tipoDestinoItem'][$r],

                        'dateCheckOut'  => $_POST['dateCheckOut'][$r],
                        'dateCheckin'  => $_POST['dateCheckin'][$r],
                        'dataEmissao'  => $_POST['dataEmissao'][$r],

                        'valorHospedagem'  => $_POST['valorHospedagem'][$r],
                        'valorPorFaixaEtaria'  => $_POST['valorPorFaixaEtaria'][$r],

                        'poltronaClient' => $poltronaClient,
                        'customerClient' => $customerClient,
                        'customerClientName' => $customerClientName,

                        'guia'     => $_POST['guia'][$r],
                        'motorista'     => $_POST['motorista'][$r],
                        'hora_embarque'     => $_POST['horaEmbarque'][$r],
                        'referencia'        => $_POST['referencia_item'][$r],
                        'note_item'         => $_POST['observacao_item'][$r],


                        'horaCheckin'  => $_POST['horaCheckin'][$r],
                        'horaCheckOut'  => $_POST['horaCheckOut'][$r],

                        'nmTipoHospedagem'  => $_POST['nmTipoHospedagem'][$r],
                        'categoriaAcomodacao'  => $_POST['categoriaAcomodacao'][$r],
                        'regimeAcomodacao'  => $_POST['regimeAcomodacao'][$r],

                        'receptivo'  => $_POST['receptivo'][$r],
                        'nmReceptivo'  => $_POST['nmReceptivo'][$r],

                        'nmEmpresaManual'  => $_POST['nmEmpresaManual'][$r],
                        'telefoneEmpresa'  => $_POST['telefoneEmpresa'][$r],
                        'enderecoEmpresa'  => $_POST['enderecoEmpresa'][$r],

                        'apresentacaoPassagem'  => $_POST['apresentacaoPassagem'][$r],
                        'dataApresentacao'  => $dataApresentacao,
                        'horaApresentacao'  => $horaApresentacao,

                        'tarifa' => $_POST['tarifa'][$r],
                        'taxaAdicionalFornecedor' => $_POST['taxaAdicionalFornecedor'][$r],
                        'descontoTarifa' => $_POST['descontoTarifa'][$r],
                        'acrescimoTarifa' => $_POST['acrescimoTarifa'][$r],
                        'adicionalBagagem' => $_POST['adicionalBagagem'][$r],
                        'adicionalReservaAssento' => $_POST['adicionalReservaAssento'][$r],
                        'valorPagoFornecedor' => $_POST['valorPagoFornecedor'][$r],
                        'formaPagamentoFornecedorCustomer' => $_POST['formaPagamentoFornecedorCustomer'][$r],
                        'bandeiraPagoFornecedor' => $_POST['bandeiraPagoFornecedor'][$r],
                        'numeroParcelasPagoFornecedor' => $_POST['numeroParcelasPagoFornecedor'][$r],
                        'descricaoServicos' => $_POST['descricaoServicos'][$r],
                        'observacaoServico' => $_POST['observacaoServico'][$r],

                        'fornecedorId' => $_POST['product_fornecedor'][$r],
                        'tipoCobrancaFornecedorId' => $_POST['product_tipo_cobranca_fornecedor'][$r],
                        'condicaoPagamentoFornecedorId' => $_POST['product_condicao_pagamento_fornecedor'][$r],

                        'desconto_fornecedor' => $_POST['desconto_fornecedor'][$r],
                        'comissao_rav_du' => $_POST['comissao_rav_du'][$r],
                        'incentivoFornecedor' => $_POST['incentivoFornecedor'][$r],
                        'outrasTaxasFornecedor' => $_POST['outrasTaxasFornecedor'][$r],
                        'taxaCartaoFornecedor' => $_POST['taxaCartaoFornecedor'][$r],

                        'valorRecebimentoValor' => $_POST['product_valor_recebimento_fornecedor'][$r],

                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $pr_tax,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $total += $item_net_price * $item_quantity;

					//$this->add_despesas_viagem($item_code, $warehouse_id,  $customer, $item_quantity, $poltronaClient,$customerClient, $customerClientName);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }

            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            if (!$this->input->post('amount-paid') && $this->input->post('amount-paid') <= 0) {
                $payment_status = 'due';
            }

			$total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);

            $user_id = $this->session->userdata('user_id');
            $user = $this->site->getUserByBiller($biller_id);

            if ($user) $user_id = $user->id;

            $data = array(
				'date' 					=> $date,
                'reference_no' 			=> $reference,
				'reference_no_variacao'	=> $reference_no_variacao,
                'note_hotel' 			=> $note_hotel,
                'tipo_quarto' 			=> $tipo_quarto,
                'totalHoteis'			=> $totalHoteis,
                'hotel'					=> $supplier_id,
                'hotel2'			    => $supplier_id2,
                'hotel3'				=> $supplier_id3,
                'hotel4'				=> $supplier_id4,
                'hotel5'				=> $supplier_id5,
                'hotel6'				=> $supplier_id6,
                'hotel7'				=> $supplier_id7,
                'customer_id' 			=> $customer_id,
				'customer_1_id' 		=> $customer_1_id,
				'customer_2_id' 		=> $customer_2_id,
				'customer_3_id' 		=> $customer_3_id,
				'customer_4_id' 		=> $customer_4_id,
				'customer_5_id' 		=> $customer_5_id,
                'customer' 				=> $customer,
				'customer_1' 			=> $customer_1,
				'customer_2' 			=> $customer_2,
				'customer_3' 			=> $customer_3,
				'customer_4' 			=> $customer_4,
				'customer_5' 			=> $customer_5,
                'biller_id' 			=> $biller_id,
                'biller' 				=> $biller,
				'local_saida' 			=> $local_saida,
                'condicao_pagamento'   	=> $condicao_pagamento,
                'warehouse_id'			=> $warehouse_id,
                'tipo_quarto_str'       => $this->input->post('tipo_quarto_str'),
                'note' 					=> $note,
                'staff_note' 			=> $staff_note,
                'total' 				=> $this->sma->formatDecimal($total),
                'product_discount' 		=> $this->sma->formatDecimal($product_discount),
                'order_discount_id' 	=> $order_discount_id,
                'order_discount' 		=> $order_discount,
                'total_discount' 		=> $total_discount,
                'product_tax' 			=> $this->sma->formatDecimal($product_tax),
                'order_tax_id' 			=> $order_tax_id,
                'order_tax' 			=> $order_tax,
                'total_tax' 			=> $total_tax,
                'shipping' 				=> $this->sma->formatDecimal($shipping),
                'grand_total' 			=> $grand_total,
                'total_items' 			=> $total_items,
                'sale_status' 			=> $sale_status,
                'payment_status' 		=> $payment_status,
                'payment_term' 			=> $payment_term,
                'due_date' 				=> $due_date,
                'vencimento'            => $this->input->post('vencimento'),
                'vendedor'              => $biller_details->name,
                'paid' 					=> 0,
                'created_by' 			=> $user_id,
                'condicaopagamentoId'   => $this->input->post('condicaopagamentoId'),
                'tipoCobrancaId'        => $this->input->post('tipoCobrancaId'),
                'previsao_pagamento'    => $this->sma->fsd(trim($this->input->post('previsao_pagamento'))),
                'valorVencimento'       => $this->input->post('valor'),
                'meioDivulgacao'        => $this->input->post('meio_divulgacao'),
            );

            if ($payment_status == 'partial' || $payment_status == 'paid') {

                if ($this->input->post('paid_by') == 'deposit') {
                    if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                        $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                if ($this->input->post('paid_by') == 'gift_card') {
                    $gc = $this->site->getGiftCardByNO($this->input->post('gift_card_no'));
                    $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                    $gc_balance = $gc->balance - $amount_paying;
                    $payment = array(
                        'date' 			=> $date,
                        'reference_no' 	=> $this->input->post('payment_reference_no'),
                        'amount' 		=> $this->sma->formatDecimal($amount_paying),
                        'paid_by' 		=> $this->input->post('paid_by'),
                        'cheque_no' 	=> $this->input->post('cheque_no'),
                        'cc_no' 		=> $this->input->post('gift_card_no'),
                        'cc_holder' 	=> $this->input->post('pcc_holder'),
                        'cc_month' 		=> $this->input->post('pcc_month'),
                        'cc_year' 		=> $this->input->post('pcc_year'),
                        'cc_type' 		=> $this->input->post('pcc_type'),
                        'created_by' 	=> $user_id,
                        'note' 			=> $this->input->post('payment_note'),
                        'type' 			=> 'received',
                        'gc_balance' 	=> $gc_balance,
                    );
                } else {

                    if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                        $payment = array(
                            'date' => $date,
                            'reference_no' => $this->input->post('payment_reference_no'),
                            'amount' => $this->sma->formatDecimal($this->input->post('amount-paid')),
                            'paid_by' => $this->input->post('paid_by'),
                            'cheque_no' => $this->input->post('cheque_no'),
                            'cc_no' => $this->input->post('pcc_no'),
                            'cc_holder' => $this->input->post('pcc_holder'),
                            'cc_month' => $this->input->post('pcc_month'),
                            'cc_year' => $this->input->post('pcc_year'),
                            'cc_type' => $this->input->post('pcc_type'),
                            'created_by' => $user_id,
                            'note' => $this->input->post('payment_note'),
                            'type' => 'received',
                        );
                    }
                }
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            //$this->sma->print_arrays($data, $products, $payment);
        }

        /*
         * LANCAMENTO DE CONTAS A RECEBER DO CLIENTE QUANDDO HÁ LANCAMENTO
         */
        $reference = $this->site->getReference('ex');
        $tipoCobranca = $this->input->post('tipoCobrancaId', true);
        $condicaoPagamento = $this->input->post('condicaopagamentoId', true);
        $valor = $this->input->post('valor');
        $note = $this->input->post('staff_note', true);
        $dataVencimento = date('Y-m-d');

        $valor_vencimento_parcelas = $this->input->post('valorVencimentoParcela');
        $data_vencimento_parcelas = $this->input->post('dtVencimentoParcela');
        $tipo_cobranca_parcelas = $this->input->post('tipocobrancaParcela');
        $desconto_parcelas = $this->input->post('descontoParcela');
        $movimentadores = $this->input->post('movimentadorParcela');
        $si_return = array();

        $conta_receber = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'receita'               => 13,//TODO RECEITA PADRAO VIRA DA CONFIGURACAO
            'reference'             => $reference,
            'created_by'            => $user_id,
            'pessoa'                => $customer_id,
            'tipocobranca'          => $tipoCobranca,
            'condicaopagamento'     => $condicaoPagamento,
            'dtvencimento'          => $dataVencimento,
            'dtcompetencia'         => $dataVencimento,
            'valor'                 => $valor,
            'warehouse'             => $warehouse_id,
            'note'                  =>  $note,
            'senderHash'            => $this->input->post('senderHash'),
        );

        if ($this->form_validation->run() == true ) {

            try {

                if ($this->Settings->gerar_contrato_faturamento_venda) {//gerar contrato

                    if ($item_id) {
                        $data['file_name_contract'] = $this->gerar_contrato($this->site->getProductByID($item_id)->contract_id, $data, $products);
                    } else {
                        $data['file_name_contract'] = $this->gerar_contrato($this->Settings->contract_id, $data, $products);
                    }

                }

                $sale_id = $this->sales_model->addSale(
                    $data,
                    $products,
                    $payment,
                    $si_return,
                    $conta_receber,
                    $valor_vencimento_parcelas,
                    $data_vencimento_parcelas,
                    $tipo_cobranca_parcelas,
                    $movimentadores,
                    $desconto_parcelas);

                $this->session->set_userdata('remove_slls', 1);

                if ($quote_id) $this->db->update('quotes', array('status' => 'completed'), array('id' => $quote_id));

                $this->session->set_flashdata('message', lang("sale_added"));

                if ($this->Settings->envio_email_venda_manual) {
                    $this->send_email($sale_id, 'create');
                }

                redirect("sales");

            } catch(Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } else {
            if ($quote_id) {

                $this->data['quote'] = $this->sales_model->getQuoteByID($quote_id);
                $items = $this->sales_model->getAllQuoteItems($quote_id);
                $c = rand(100000, 9999999);

                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                    }

                    $row->quantity = 0;
                    $pis = $this->sales_model->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);

                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }

                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    
					if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->sales_model->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }

                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
					
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->tax_rate) {
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'options' => $options);
                    } else {
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => false, 'options' => $options);
                    }
                    $c++;
                }
                $this->data['quote_items'] = json_encode($pr);
            }

            $this->data['error'] 		        = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['quote_id'] 	        = $quote_id;

            $this->data['billers'] 		        = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] 	        = $this->site->getAllWarehouses('Confirmado');
            $this->data['tax_rates'] 	        = $this->site->getAllTaxRates();
            $this->data['meiosDivulgacao']      = $this->Meiodivulgacao_model->getAllActive();

            $this->data['myPackages']           = $this->products_model->getAllProductsConfirmed();
            $this->data['tiposCobranca']        = $this->site->getAllTiposCobrancaReceita(true);
            $this->data['condicoesPagamento']   = $this->financeiro_model->getAllCondicoesPagamento();

            $this->data['destinos']             = $this->destino_model->getAll();
            $this->data['payment_ref'] 	        = $this->site->getReference('pay');
            $this->data['categories']           = $this->site->getAllCategories();

            $this->data['sessionCode']          = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro']      = $this->settings_model->getPagSeguroSettings();

            $this->data['staff']                = $this->ValorFaixaRepository_model->getStaff();

            $this->data['anoFilter'] = date('Y');
            $this->data['mesFilter'] = date('m');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale')));
            $meta = array('page_title' => lang('add_sale'), 'bc' => $bc);
            $this->page_construct('sales/add', $meta, $this->data);
        }
    }

    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

    public function getLocais() {

        $term = $this->input->get('term');
        $term = str_replace(' ', '%20', $term);
        $term = $this->tirarAcentos($term);

        $ch = curl_init();

         curl_setopt($ch, CURLOPT_URL, 'https://www.e-agencias.com.br/api/flights/autocomplete/cities_airports?query='.$term.'&agency=ag115106');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $content = trim(curl_exec($ch));
        curl_close($ch);

        $content = str_replace('name','label', $content);
        $content = str_replace('{"autocomplete":','', $content);
        $content = str_replace(']}', ']', $content);

        //$json = json_encode($content);
        //echo $content;

        echo $this->sma->send_json($content);
    }


    public function getHoteis() {

        $term = $this->input->get('term');
        $term = str_replace(' ', '%20', $term);
        $term = $this->tirarAcentos($term);

        $ch = curl_init();

        $header = [
            'agency-domain: ag115106',
            'Accept-Language: pt-BR',
            ];

        curl_setopt($ch, CURLOPT_URL, 'https://www.e-agencias.com.br/api/hotels/autocomplete?query='.$term.'&agency=ag115106&filtered=true');

        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        $content = trim(curl_exec($ch));
        curl_close($ch);

        $content = str_replace('description','label', $content);
       // $content = str_replace('{"autocomplete":','', $content);
       // $content = str_replace(']}', ']', $content);

        //$json = json_encode($content);
        //echo $content;

        echo $this->sma->send_json($content);
    }

    public function view_aereo() {
        $this->load->view($this->theme . 'sales/view_aereo', $this->data);
    }

    public function view_aereo_itinerario() {
        $this->load->view($this->theme . 'sales/view_aereo_itinerario', $this->data);
    }

    public function view_hospedagem() {
        $this->data['tiposHospedagem'] = $this->TipoQuartoRepository_model->getTiposQuarto();
        $this->load->view($this->theme . 'sales/view_hospedagem', $this->data);
    }

    public function view_transfer() {
        $this->load->view($this->theme . 'sales/view_transfer', $this->data);
    }

    public function addPacote() {

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('sales/add_pacote', $meta, $this->data);

    }

    public function addPacoteTerceiro() {

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('sales/add_pacote_terceiro', $meta, $this->data);

    }

    public function adicionarMeuPacote() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->load->view($this->theme . 'sales/add_meu_pacote', $this->data);
    }

    public function adicionarPasseio() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['currencies']   = $this->settings_model->getAllCurrencies();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['tiposCobranca']        = $this->financeiro_model->getAllTiposCobranca();
        $this->load->view($this->theme . 'sales/add_template_item', $this->data);
    }

    public function adicionarHospedagem() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['currencies']   = $this->settings_model->getAllCurrencies();

        $this->load->view($this->theme . 'sales/add_template_item', $this->data);
    }

    public function addTraslado() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
        $this->load->view($this->theme . 'sales/add_traslado', $this->data);
    }

    public function adicionarNovoCliente($nome, $sexo, $cpf, $rg, $celular, $email, $dataNascimento, $user, $telefone, $telefoneEmergencia, $alergiaMedicamento) {

        $dataNascimento = str_replace('/', '-', $dataNascimento);
        $dataNascimento = date('Y-m-d', strtotime($dataNascimento));

        $data = array(
            'group_id'              => 3,
            'group_name'            => 'customer',
            'customer_group_id'     => 1,
            'customer_group_name'   => 'PASSAGEIROS',
            'tipo_documento'        => 'rg',
            'company'               => strtoupper($nome),
            'name'                  => strtoupper($nome),
            'email'                 => $email,
            'data_aniversario'      => $dataNascimento,
            'sexo'                  => $sexo,
            'vat_no'                => $cpf,
            'phone'                 => $telefone,
            'cf1'                   => $rg,
            'cf5'                   => $celular,//whatsapp
            'telefone_emergencia'   => $telefoneEmergencia,
            'alergia_medicamento'   => $alergiaMedicamento
        );

        $customer = $this->companies_model->getVerificaCustomeByCPF($cpf);

        if (count($customer) > 0 && $this->sma->isCPFObrigaNovoCadastroEhValidaCPFCNJ($cpf)) {
            $cid =  $customer->id;
            $this->companies_model->updateCompanyWith($customer->id, $data, NULL);
        } else {
            $cid = $this->companies_model->addCompanyWith($data, NULL);
        }

        if ($dataNascimento != null) {
            $data_event = array(
                'title' 		=> $nome,
                'customers' 	=> $cid,
                'start'	 		=> $dataNascimento,
                'end' 			=> $dataNascimento,
                'description' 	=> 'De os parabens para '.$nome.' hoje e seu aniversario',
                'color' 		=>  '#1d7010',
                'user_id' 	    => $user->id
            );
            $this->companies_model->addEvent($data_event);
        }

        return $cid;
    }

    public function adicionarItemNaVenda($cid, $nomeCliente, $product, $warehouse, $criancaDeColo='NAO') {

        $colo = 0;

        if ($criancaDeColo == 'SIM') {
            $price = 0;
            $nomeCliente = $nomeCliente.' (colo)';
            $colo = 1;
        } else $price = $product->price;

        $subtotal = $price;
        $quantity = 1;
        $item_option = null;

        $products = array(
            'product_id' => $product->id,
            'product_code' => $product->code,
            'product_name' => $product->name,
            'product_type' => $product->type,
            'customerClient' => $cid,
            'customerClientName' =>  $nomeCliente,
            'option_id' => $item_option,
            'net_unit_price' => $price,
            'unit_price' => $price,
            'quantity' => $quantity,
            'warehouse_id' => $warehouse->id,
            'tax' => 0,
            'discount' => 0,
            'item_tax' => 0,
            'item_discount' => 0,
            'real_unit_price' => $price,
            'subtotal' => $subtotal,
            'colo' => $colo
        );

        $this->add_despesas_viagem($product->code, $warehouse->id,  $nomeCliente, $quantity, '', $cid, $nomeCliente);

        return $products;
    }

    public function criarUmaNovaVendaViaLinkDePagamento() {

        $nome = $this->input->get('nome');
        $sexo = $this->input->get('sexo');
        $cpf = $this->input->get('cpf');
        $rg = $this->input->get('rg');
        $celular = $this->input->get('celular');
        $telefone = $this->input->get('telefone');
        $telefone_emergencia = $this->input->get('telefone_emergencia');
        $alergia_medicamento = $this->input->get('alergia_medicamento');
        $email = $this->input->get('email');
        $dataNascimento = $this->input->get('dataNascimento');
        $viagem = $this->input->get('viagem');
        $vendedor = $this->input->get('vendedor');
        $observacoes =  $this->input->get('observacoes');

        $local_embarque = $this->input->get('local_embarque');
        $local_embarque_outros = $this->input->get('local_embarque_outros');

        $formas_pagamento = $this->input->get('formas_pagamento');
        $formas_pagamento_outros = $this->input->get('formas_pagamento_outros');

        $tipos_hospedagem = $this->input->get('tipos_hospedagem');
        $tipos_hospedagem_outros = $this->input->get('tipos_hospedagem_outros');

        $posicao_assento = $this->input->get('posicao_assento');

        $user = $this->site->getUserByBiller($vendedor);
        $product = $this->site->getProductByID($viagem);
        $warehouse = $this->site->getWarehouseByCode($product->code);
        $biller_details 	= $this->site->getCompanyByID($vendedor);

        $cid = $this->adicionarNovoCliente($nome, $sexo, $cpf, $rg, $celular, $email, $dataNascimento, $user, $telefone,$telefone_emergencia, $alergia_medicamento);
        $products = $this->adicionarItemNaVenda($cid, $nome , $product, $warehouse);
        $productsToo[] = $products;

        $i = isset($_GET['nomeDependente']) ? sizeof($_GET['nomeDependente']) : 0;
        $totalPassageiros = 0;

        for ($r = 0; $r < $i; $r++) {

            $nomeDependente = $_GET['nomeDependente'][$r];

            if ($nomeDependente != '') {

                $sexoDependente = $_GET['sexoDependente'][$r];
                $cpfDependente = $_GET['cpfDependente'][$r];
                $rgDependente = $_GET['rgDependente'][$r];
                $dataNascimentoDependente = $_GET['dataNascimentoDependente'][$r];
                $telefoneEmergenciaDependente = $_GET['telefoneEmergenciaDependente'][$r];
                $alergiaMedicamentoDependente = $_GET['alergiaMedicamentoDependente'][$r];
                $criancaDeColo =  $_GET['crianca_colo'][$r];

                $dependente = $this->adicionarNovoCliente($nomeDependente, $sexoDependente, $cpfDependente, $rgDependente, '', '', $dataNascimentoDependente, $user,'', $telefoneEmergenciaDependente, $alergiaMedicamentoDependente);
                $productsAdicional = $this->adicionarItemNaVenda($dependente, $nomeDependente, $product, $warehouse, $criancaDeColo);

                $productsToo[] = $productsAdicional;

                if ($criancaDeColo != 'SIM') $totalPassageiros = $totalPassageiros + 1;
            }
        }

        krsort($productsToo);

        if (strpos($local_embarque ,'OUTROS') !== false) $local_embarque = $local_embarque_outros;
        if (strpos($formas_pagamento ,'OUTROS') !== false) $formas_pagamento = $formas_pagamento_outros;
        if (strpos($tipos_hospedagem ,'OUTROS') !== false ) $tipos_hospedagem = $tipos_hospedagem_outros;

        $total = $product->price;
        $reference_no = $this->site->getReference('so');

        if ($i == 1) $i = 0;

        $venda = array(
            'date' 					=> date('Y-m-d H:i'),
            'reference_no' 			=>  $reference_no,
            'reference_no_variacao'	=> '',
            'note_hotel' 			=> '',
            'tipo_quarto_str'   	=> $tipos_hospedagem,
            'customer_id' 			=> $cid,
            'customer' 				=> $nome,
            'biller_id' 			=> $vendedor,
            'biller' 				=> $biller_details->name,
            'local_saida' 			=> $local_embarque,
            'condicao_pagamento'   	=> $formas_pagamento,
            'warehouse_id'			=> $warehouse->id,
            'note' 					=> $observacoes,
            'posicao_assento'       => $posicao_assento,
            'product_discount' 		=> 0,
            'order_discount_id' 	=> 0,
            'order_discount' 		=> 0,
            'total_discount' 		=> 0,
            'product_tax' 			=> 0,
            'order_tax_id' 			=> 0,
            'order_tax' 			=> 0,
            'total_tax' 			=> 0,
            'shipping' 				=> 0,
            'paid' 					=> 0,
            'grand_total' 			=> $total + ($total*$totalPassageiros),
            'total' 				=> $total + ($total*$totalPassageiros),
            'total_items' 			=> count($products),
            'sale_status' 			=> 'completed',
            'payment_status' 		=> 'due',
            'payment_term' 			=> '',
            'vencimento'            => date('Y-m-d'),
            'vendedor'              => $biller_details->name,
            'created_by' 			=> $user->id,
        );

        $sale_id = $this->sales_model->addSale($venda, $productsToo, array());

        if ($email != '') {

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            else $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');

            $customer = $this->site->getCompanyByID($cid);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->name,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
            );

            $message = $this->parser->parse_string($sale_temp, $parse_data);
            $subject = 'RESERVA ' . $this->Settings->site_name;
            $to = $email.','.$this->Settings->default_email;
            $attachment = $this->pdf($sale_id, null, 'S');

            if ($this->Settings->is_email_queue) {
                $this->sma->send_email_queue($this->db, 'sales', 'create', $sale_id, $to, $subject, $message, null, null, $attachment, '', '');
            } else {
                $this->sma->send_email($to, $subject, $message, null, null, $attachment, '', '');
                delete_files($attachment);
            }
        }

        redirect(base_url()."Linkcompra/link_compra_finalizacao/".$sale_id.'/'.$vendedor.'/'.$product->id);
    }

	/* -------------------------------------------------------------------------------------------------------------------------------- */
    public function add_despesas_viagem($item_code, $warehouse_id,  $customer=null, $item_quantity=1, $poltronaClient=null,$customerClient=null, $customerClientName=null) {

    	$product_details 	= $this->sales_model->getProductByCode($item_code);
    	$products_variacoes = $this->sales_model->getProductOptions($product_details->id,  $warehouse_id , true);
    	
    	foreach ($products_variacoes as $products_variacao) {
    		
    		unset($products);
    		unset($data);
    		
    		$nome_variacao 		= $products_variacao->name;
    		$fornecedor 		= $products_variacao->fornecedor;
    		$variacao 			= $this->sales_model->getVariantByName($nome_variacao);

            if(is_array($variacao)) {

                $lancamento_type = $variacao[0]->lancamento_type;

                if ($lancamento_type == 2) {

                    if ($products_variacao->price) $totalPreco = $products_variacao->price;
                    else $totalPreco = 0.00;

                    $reference = $this->site->getReference('po');
                    $date = date('Y-m-d H:i:s');

                    if ($fornecedor > 0) {
                        $supplier_id = $fornecedor;
                        $supplier_details = $this->site->getCompanyByID($fornecedor);
                        $supplier = $supplier_details->name . ' (' . $nome_variacao . ')';
                    } else {
                        $supplier_id = 622;//fixo para um fornecedor configurado como despesas de viagem
                        $supplier = "Despesas de Viagem";
                    }

                    $status = 'pending';
                    $total = 0;

                    $unit_cost = $totalPreco;
                    $unit_cost = $this->sma->formatDecimal($unit_cost);
                    $item_net_cost = $unit_cost;
                    $subtotal = ($item_net_cost * $item_quantity);

                    if ($customerClientName != '' &&
                        $customerClientName != 'undefined' &&
                        $customerClientName != '0') {
                        $customer = $customerClientName;
                    }

                    $products[] = array(
                        'product_code'      => $item_code,
                        'poltronaClient'    => $poltronaClient,
                        'customerClient'    => $customerClient,
                        'customerClientName'=> $customerClientName,
                        'product_id'        => $product_details->id,
                        'product_name'      => $customer,//nome do cliente como produto
                        'net_unit_cost'     => $item_net_cost,
                        'option_id'         => $products_variacao->id,
                        'unit_cost'         => $this->sma->formatDecimal($item_net_cost),
                        'quantity'          => $item_quantity,
                        'quantity_balance'  => $item_quantity,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'real_unit_cost'    => $totalPreco,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                    );

                    $total += ($item_net_cost * $item_quantity);

                    if (empty($products)) $this->form_validation->set_rules('product', lang("order_items"), 'required');
                    else krsort($products);

                    $grand_total = $this->sma->formatDecimal($total);

                    $data = array(
                        'reference_no'  => $reference,
                        'date'          => $date,
                        'supplier_id'   => $supplier_id,
                        'supplier'      => $supplier,
                        'warehouse_id'  => $warehouse_id,
                        'total'         => $this->sma->formatDecimal($total),
                        'grand_total'   => $grand_total,
                        'status'        => $status,
                        'created_by'    => $this->session->userdata('user_id'),
                    );

                    $purchases = $this->db->get_where('purchases',
                        array(
                            'supplier_id'   => $supplier_id,
                            'warehouse_id'  => $warehouse_id),
                        1);

                    if ($purchases->num_rows() > 0) {
                        $purchases = $purchases->row();
                        $this->sales_model->updatePurchase($purchases->id, $data, $products);
                    } else {
                        $this->sales_model->addPurchase($data, $products);
                    }
                }
            }
    	}				
 		$this->session->set_userdata('remove_pols', 1);      
    }

    public function  getProductOptions_Hotel($id) {
        $product_variants = $this->products_model->getProductOptions_Hotel($id);
        echo json_encode($product_variants);
    }

    public function getProdutctOption_Onibus($id) {
        $onibus = $this->sales_model->buscar_Onibus_by_product($id);
        echo json_encode($onibus);
    }

    public function getProdutctOption_OnibusByID($idVariacao) {
        $onibus = $this->sales_model->buscar_OnibusById($idVariacao);
        echo json_encode($onibus);
    }

    private function vendaNaoPertenceAoVendedorLogado($inv) {
        $user_id = $this->session->userdata('user_id');
        return $inv->created_by != $user_id;
    }

    /* ------------------------------------------------------------------------ */
    public function edit($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) $id = $this->input->get('id');

        $inv = $this->sales_model->getInvoiceByID($id);
        if ($inv->sale_status == 'returned' || $inv->return_id || $inv->return_sale_ref) {
            $this->session->set_flashdata('error', lang('sale_has_returned'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }

        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }

        $user_id = $this->session->userdata('user_id');

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {

            if ($this->vendaNaoPertenceAoVendedorLogado($inv)) {
                $this->session->set_flashdata('error', lang('this_sale_does_not_belong'));
                redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }
        }

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no');

            if ($this->Owner || $this->Admin) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = $inv->date;

            $reference_no_variacao 	= $this->input->post('reference_no_variacao');

            $totalHoteis        = $this->input->post('totalHoteis');
            $supplier_id		= $this->input->post('supplier_id');
            $supplier_id2		= $this->input->post('supplier_id2');
            $supplier_id3		= $this->input->post('supplier_id3');
            $supplier_id4		= $this->input->post('supplier_id4');
            $supplier_id5		= $this->input->post('supplier_id5');
            $supplier_id6		= $this->input->post('supplier_id6');
            $supplier_id7		= $this->input->post('supplier_id7');

            if ($totalHoteis == 1) {
                $supplier_id2 = null;
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 2) {
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 3) {
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 4) {
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 5) {
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 6) {
                $supplier_id7 = null;
            }

            $customer_1_id 		= $this->input->post('form_slcustomer_1');
            $customer_2_id 		= $this->input->post('form_slcustomer_2');
            $customer_3_id 		= $this->input->post('form_slcustomer_3');
            $customer_4_id 		= $this->input->post('form_slcustomer_4');
            $customer_5_id 		= $this->input->post('form_slcustomer_5');

            $customer_1			= null;
            $customer_2			= null;
            $customer_3			= null;
            $customer_4			= null;
            $customer_5			= null;

            if ($customer_1_id) {
                $customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
                $customer_1 			= $customer_details_1->name;
            }

            if ($customer_2_id) {
                $customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
                $customer_2 			= $customer_details_2->name;
            }

            if ($customer_3_id) {
                $customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
                $customer_3 			= $customer_details_3->name;
            }

            if ($customer_4_id) {
                $customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
                $customer_4 			= $customer_details_4->name;
            }

            if ($customer_5_id) {
                $customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
                $customer_5 			= $customer_details_5->name;
            }

            $note_hotel 		= $this->input->post('form_slnote_hotel');
            $tipo_quarto 		= $this->input->post('form_sltipo_quarto');
			
            $warehouse_id		= $this->input->post('warehouse') != null ? $this->input->post('warehouse') : 1;
            $customer_id 		= $this->input->post('customer');
			$local_saida 		= $this->input->post('local_saida');
            $condicao_pagamento = $this->input->post('condicao_pagamento');
            $biller_id 			= $this->input->post('biller');
            $total_items 		= $this->input->post('total_items');
            $sale_status 		= $this->input->post('sale_status');
            $payment_status 	= $this->input->post('payment_status');
            $payment_term 		= $this->input->post('payment_term');
            $due_date 			= $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping 			= $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details 	= $this->site->getCompanyByID($customer_id);
            $customer 			= $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details 	= $this->site->getCompanyByID($biller_id);
            $biller 			= $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note 				= $this->sma->clear_tags($this->input->post('note'));
            $staff_note 		= $this->sma->clear_tags($this->input->post('staff_note'));
			
            $total 				= 0;
            $product_tax 		= 0;
            $order_tax 			= 0;
            $product_discount 	= 0;
            $order_discount 	= 0;
            $percentage 		= '%';
			
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
			
            for ($r = 0; $r < $i; $r++) {
				
                $item_id 	        = $_POST['product_id'][$r];
                $itemSaleid         = $_POST['itemid'][$r];
                $customer_dependent = $this->site->getCompanyByID($_POST['customerClient'][$r]);

                if (!$reference_no_variacao) {
                    $onibus = $this->sales_model->buscar_Onibus_by_product($item_id);
                    foreach($onibus as $bus)  $reference_no_variacao = $bus->id;
                }

                $item_type 	        = $_POST['product_type'][$r];
                $item_code 	        = $_POST['product_code'][$r];
                $item_name 	        = $_POST['product_name'][$r];
                $poltronaClient     = $_POST['poltronaClient'][$r];

                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {

                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }
                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;
                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $adicional = $_POST['adicional'][$r];

                    if ($adicional == 'true') $adicional = 1;
                    else $adicional = 0;

                    $item_id_code_row =  $_POST['item_id'][$r];

                    $k 	= isset($_POST['adicional_nomeServico_'.$item_id_code_row]) ? sizeof($_POST['adicional_nomeServico_'.$item_id_code_row]) : 0;
                    $servicosAdicionais = [];

                    for ($j = 0; $j < $k; $j++) {

                        $itemServicoAcional= $_POST['adicional_item_id_' . $item_id_code_row][$j];

                        if ($itemServicoAcional == $item_id_code_row) {
                            $servicosAdicionais[] = array(
                                'nome_produto' => $_POST['adicional_nomeServico_' . $item_id_code_row][$j],
                                'codigo_produto' => $_POST['adicional_servicoCode_' . $item_id_code_row][$j],
                                'categoria_produto' => $_POST['adicional_nmCategoria_' . $item_id_code_row][$j],
                                'produtoId' => $_POST['adicional_servicoId_' . $item_id_code_row][$j],
                                'valor' => $_POST['adicional_preco_' . $item_id_code_row][$j],
                                'quantidade' => 1,
                            );
                        }
                    }

                    $k 	= isset($_POST['origem_'.$item_id_code_row]) ? sizeof($_POST['origem_'.$item_id_code_row]) : 0;
                    $itinerario = [];

                    for ($j = 0; $j < $k; $j++) {

                        $itemItinerario = $_POST['itinerario_item_id_'.$item_id_code_row][$j];

                        if ($itemItinerario == $item_id_code_row) {

                            $itinerario[] = array(
                                'origem' => $_POST['origem_' . $item_id_code_row][$j],
                                'destino' => $_POST['destino_' . $item_id_code_row][$j],
                                'companhiaAerea' => $_POST['companhiaAerea_' . $item_id_code_row][$j],
                                'assentoVoo' => $_POST['assentoVoo_' . $item_id_code_row][$j],
                                'numeroVoo' => $_POST['numeroVoo_' . $item_id_code_row][$j],
                                'dataPartida' => $_POST['dataPartida_' . $item_id_code_row][$j],
                                'horaPartida' => $_POST['horaPartida_' . $item_id_code_row][$j],
                                'dataChegada' => $_POST['dataChegada_' . $item_id_code_row][$j],
                                'horaChegada' => $_POST['horaChegada_' . $item_id_code_row][$j],
                                'bagagem' => $_POST['bagagem_' . $item_id_code_row][$j],
                            );
                        }
                    }

                    $k 	= isset($_POST['tipoTransfer_'.$item_id_code_row]) ? sizeof($_POST['tipoTransfer_'.$item_id_code_row]) : 0;
                    $transfers = [];

                    for ($j = 0; $j < $k; $j++) {

                        $dataSaidaVooBus = $_POST['transfer_dataSaidaVooBus_'.$item_id_code_row][$j];
                        $horaSaidaBooBus = $_POST['transfer_horaSaidaBooBus_'.$item_id_code_row][$j];

                        $itemTransferencia = $_POST['transfer_item_id_'.$item_id_code_row][$j];

                        if ($itemTransferencia == $item_id_code_row) {

                            if ($dataSaidaVooBus == '') $dataSaidaVooBus = null;
                            if ($horaSaidaBooBus == '') $horaSaidaBooBus = null;

                            $transfers[] = array(
                                'tipoTransfer' => $_POST['tipoTransfer_' . $item_id_code_row][$j],
                                'origem' => $_POST['transfer_origem_' . $item_id_code_row][$j],
                                'destino' => $_POST['transfer_destino_' . $item_id_code_row][$j],
                                'data' => $_POST['transfer_data_' . $item_id_code_row][$j],
                                'hora' => $_POST['transfer_hora_' . $item_id_code_row][$j],
                                'dataEmissao' => $_POST['transfer_dataEmissao_' . $item_id_code_row][$j],

                                'dataSaidaVooBus' => $dataSaidaVooBus,
                                'horaSaidaBooBus' => $horaSaidaBooBus,
                                'referenciaVooBus' => $_POST['transfer_referenciaVooBus_' . $item_id_code_row][$j],

                                'referencia' => $_POST['transfer_referencia_' . $item_id_code_row][$j],
                                'fornecedor' => $_POST['transfer_fornecedor_' . $item_id_code_row][$j],
                            );

                        }
                    }

                    $dataApresentacao = $_POST['dataApresentacao'][$r];
                    $horaApresentacao = $_POST['horaApresentacao'][$r];

                    if ($dataApresentacao == '') $dataApresentacao = null;
                    if ($horaApresentacao == '') $horaApresentacao = null;

                    $products[] = array(

                        'itinerarios' => $itinerario,
                        'transfers' => $transfers,
                        'servicosAdicionais' => $servicosAdicionais,

                        'product_id' => $item_id,
                        'itemId'    => $itemSaleid,

                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'product_comissao' => $_POST['product_comissao'][$r],

                        'adicional'  =>  $adicional,

                        'faixaId'       => $_POST['faixaEtaria'][$r],
                        'faixaNome'     => $_POST['nomeFaixa'][$r],
                        'descontarVaga' => $_POST['descontarVaga'][$r],//TODO CONTROLA SE DESCONTA VAGAS.

                        'programacaoId'  => $_POST['programacaoId'][$r],
                        'servicoPai'  => $_POST['servicoPai'][$r],

                        'nmTipoTransporte'  => $_POST['nmTipoTransporte'][$r],
                        'tipoTransporte'  => $_POST['tipoTransporte'][$r],
                        'localEmbarque'  => $_POST['localEmbarque'][$r],
                        'tipoHospedagem'  => $_POST['tipoHospedagem'][$r],
                        'product_category' => $_POST['product_category'][$r],

                        'origem'  => $_POST['origem'][$r],
                        'destino'  => $_POST['destino'][$r],
                        'reserva'  => $_POST['reserva'][$r],
                        'nmFornecedor'  => $_POST['nmFornecedor'][$r],
                        'nmCategoria'  => $_POST['nmCategoria'][$r],
                        'tipoDestino'  => $_POST['tipoDestinoItem'][$r],

                        'dateCheckOut'  => $_POST['dateCheckOut'][$r],
                        'dateCheckin'  => $_POST['dateCheckin'][$r],
                        'dataEmissao'  => $_POST['dataEmissao'][$r],

                        'guia'              => $_POST['guia'][$r],
                        'motorista'         => $_POST['motorista'][$r],
                        'hora_embarque'     => $_POST['horaEmbarque'][$r],
                        'referencia'        => $_POST['referencia_item'][$r],
                        'note_item'         => $_POST['observacao_item'][$r],

                        'valorHospedagem'  => $_POST['valorHospedagem'][$r],
                        'valorPorFaixaEtaria'  => $_POST['valorPorFaixaEtaria'][$r],

                        'poltronaClient'     => $poltronaClient,
                        'customerClient'     => $customer_dependent->id,
                        'customerClientName' => $customer_dependent->name,

                        'horaCheckin'  => $_POST['horaCheckin'][$r],
                        'horaCheckOut'  => $_POST['horaCheckOut'][$r],

                        'nmTipoHospedagem'  => $_POST['nmTipoHospedagem'][$r],
                        'categoriaAcomodacao'  => $_POST['categoriaAcomodacao'][$r],
                        'regimeAcomodacao'  => $_POST['regimeAcomodacao'][$r],

                        'receptivo'  => $_POST['receptivo'][$r],
                        'nmReceptivo'  => $_POST['nmReceptivo'][$r],

                        'nmEmpresaManual'  => $_POST['nmEmpresaManual'][$r],
                        'telefoneEmpresa'  => $_POST['telefoneEmpresa'][$r],
                        'enderecoEmpresa'  => $_POST['enderecoEmpresa'][$r],

                        'apresentacaoPassagem'  => $_POST['apresentacaoPassagem'][$r],
                        'dataApresentacao'  => $dataApresentacao,
                        'horaApresentacao'  => $horaApresentacao,

                        'tarifa' => $_POST['tarifa'][$r],
                        'taxaAdicionalFornecedor' => $_POST['taxaAdicionalFornecedor'][$r],
                        'descontoTarifa' => $_POST['descontoTarifa'][$r],
                        'acrescimoTarifa' => $_POST['acrescimoTarifa'][$r],
                        'adicionalBagagem' => $_POST['adicionalBagagem'][$r],
                        'adicionalReservaAssento' => $_POST['adicionalReservaAssento'][$r],
                        'valorPagoFornecedor' => $_POST['valorPagoFornecedor'][$r],
                        'formaPagamentoFornecedorCustomer' => $_POST['formaPagamentoFornecedorCustomer'][$r],
                        'bandeiraPagoFornecedor' => $_POST['bandeiraPagoFornecedor'][$r],
                        'numeroParcelasPagoFornecedor' => $_POST['numeroParcelasPagoFornecedor'][$r],
                        'descricaoServicos' => $_POST['descricaoServicos'][$r],
                        'observacaoServico' => $_POST['observacaoServico'][$r],

                        'fornecedorId' => $_POST['product_fornecedor'][$r],
                        'tipoCobrancaFornecedorId' => $_POST['product_tipo_cobranca_fornecedor'][$r],
                        'condicaoPagamentoFornecedorId' => $_POST['product_condicao_pagamento_fornecedor'][$r],

                        'desconto_fornecedor' => $_POST['desconto_fornecedor'][$r],
                        'comissao_rav_du' => $_POST['comissao_rav_du'][$r],
                        'incentivoFornecedor' => $_POST['incentivoFornecedor'][$r],
                        'outrasTaxasFornecedor' => $_POST['outrasTaxasFornecedor'][$r],
                        'taxaCartaoFornecedor' => $_POST['taxaCartaoFornecedor'][$r],

                        'valorRecebimentoValor' => $_POST['product_valor_recebimento_fornecedor'][$r],

                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $pr_tax,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $total += $item_net_price * $item_quantity;

                    //somente para a instalacao
                    //$this->add_despesas_viagem($item_code, $warehouse_id,  $customer, $item_quantity, $poltronaClient,$customerClient, $customerClientName);
                }
            }
            
			if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
			
            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');

                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }

            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);

            $user = $this->site->getUserByBiller($biller_id);

            if ($user) {
                $user_id = $user->id;
            }

            $cupom_id = $this->input->post('cupom_id');
            $desconto_cupom = $this->input->post('desconto_cupom');

            if ($order_discount_id == 0) {//todo se o desconto for removido - remove o cupom
                $cupom_id = null;
                $desconto_cupom = 0;
            }

            $data = array(
				'date'                  => $date,
                'reference_no' 		    => $reference,
                'reference_no_variacao'	=> $reference_no_variacao,
                'note_hotel' 			=> $note_hotel,
                'tipo_quarto' 			=> $tipo_quarto,
                'totalHoteis'			=> $totalHoteis,
                'hotel'					=> $supplier_id,
                'hotel2'			    => $supplier_id2,
                'hotel3'				=> $supplier_id3,
                'hotel4'				=> $supplier_id4,
                'hotel5'				=> $supplier_id5,
                'hotel6'				=> $supplier_id6,
                'hotel7'				=> $supplier_id7,
                'customer_id' 			=> $customer_id,
                'customer_1_id' 		=> $customer_1_id,
                'customer_2_id' 		=> $customer_2_id,
                'customer_3_id' 		=> $customer_3_id,
                'customer_4_id' 		=> $customer_4_id,
                'customer_5_id' 		=> $customer_5_id,
                'customer' 				=> $customer,
                'customer_1' 			=> $customer_1,
                'customer_2' 			=> $customer_2,
                'customer_3' 			=> $customer_3,
                'customer_4' 			=> $customer_4,
                'customer_5' 			=> $customer_5,
				'local_saida' 		    => $local_saida,
                'tipo_quarto_str'       => $this->input->post('tipo_quarto_str'),
                'condicao_pagamento'    => $condicao_pagamento,
                'warehouse_id' 		    => $warehouse_id,
                'note' 				    => $note,
                'staff_note' 		    => $staff_note,
                'total'				    => $this->sma->formatDecimal($total),
                'product_discount' 	    => $this->sma->formatDecimal($product_discount),
                'order_discount_id'     => $order_discount_id,
                'order_discount' 	    => $order_discount,
                'total_discount' 	    => $total_discount,
                'product_tax' 		    => $this->sma->formatDecimal($product_tax),
                'order_tax_id' 		    => $order_tax_id,
                'order_tax' 		    => $order_tax,
                'total_tax' 		    => $total_tax,
                'shipping' 			    => $this->sma->formatDecimal($shipping),
                'grand_total' 		    => $grand_total,
                'total_items' 		    => $total_items,
                'sale_status' 		    => $sale_status,
                'payment_status' 	    => $payment_status,
                'payment_term' 		    => $payment_term,
                'due_date' 			    => $due_date,
                'vencimento'            => $this->input->post('vencimento'),
                'condicaopagamentoId'   => $this->input->post('condicaopagamentoId'),
                'tipoCobrancaId'        => $this->input->post('tipoCobrancaId'),
                'previsao_pagamento'    => $this->sma->fsd(trim($this->input->post('previsao_pagamento'))),
                'valorVencimento'       => $this->input->post('valor'),

                'biller_id' 		    => $biller_id,
                'biller' 			    => $biller,
                'vendedor'              => $biller_details->name,
                'meioDivulgacao'        => $this->input->post('meio_divulgacao'),

                'cupom_id'              => $cupom_id,
                'desconto_cupom'        => $desconto_cupom,

                'created_by' 		    => $user_id == null ? $this->session->userdata('user_id') :  $user_id,//TODO POR ENQUANTO VAMOS ATUALIZAR O USUARIO DE CRIACOA POIS ELE E O FILTRO DE VENDEDOR.
                'updated_by' 		    => $this->session->userdata('user_id'),
                'updated_at' 		    => date('Y-m-d H:i:s'),
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true) {

            try {
               /*
                * LANCAMENTO DE CONTAS A RECEBER DO CLIENTE QUANDDO HÁ LANCAMENTO
                */
                $reference = $this->site->getReference('ex');
                $tipoCobranca = $this->input->post('tipoCobrancaId', true);
                $condicaoPagamento = $this->input->post('condicaopagamentoId', true);
                $valor = $this->input->post('valor');
                $note = $this->input->post('staff_note', true);
                $dataVencimento = date('Y-m-d');

                $valorVenciemntos = $this->input->post('valorVencimentoParcela');
                $vencimentos = $this->input->post('dtVencimentoParcela');
                $tiposCobranca = $this->input->post('tipocobrancaParcela');
                $descontos = $this->input->post('descontoParcela');
                $movimentadores = $this->input->post('movimentadorParcela');

                $conta_receber = array(
                    'status'                => Financeiro_model::STATUS_ABERTA,
                    'receita'               => 13,//TODO RECEITA PADRAO VIRA DA CONFIGURACAO
                    'reference'             => $reference,
                    'created_by'            => $user_id,
                    'pessoa'                => $customer_id,
                    'tipocobranca'          => $tipoCobranca,
                    'condicaopagamento'     => $condicaoPagamento,
                    'dtvencimento'          => $dataVencimento,
                    'dtcompetencia'         => $dataVencimento,
                    'valor'                 => $valor,
                    'warehouse'             => $warehouse_id,
                    'note'                  => $note,
                    'senderHash'            => $this->input->post('senderHash'),
                );

                if ($this->Settings->gerar_contrato_faturamento_venda) {//gerar contrato
                    if ($item_id) {
                        $data['file_name_contract'] = $this->gerar_contrato($this->site->getProductByID($item_id)->contract_id, $data, $products);
                    } else {
                        $data['file_name_contract'] = $this->gerar_contrato($this->Settings->contract_id, $data, $products);
                    }
                }

                $vendaModel = new Venda_model();
                $vendaModel->data = $data;
                $vendaModel->conta_receber = $conta_receber;
                $vendaModel->tiposCobranca = $tiposCobranca;
                $vendaModel->descontos = $descontos;
                $vendaModel->movimentadores = $movimentadores;
                $vendaModel->products = $products;
                $vendaModel->vencimentos = $vencimentos;
                $vendaModel->valorVencimentos = $valorVenciemntos;
                $vendaModel->apenasEditar = $this->input->post('apenasEditar', true);

                $this->sales_model->updateSale($id, $vendaModel);

                $this->session->set_userdata('remove_slls', 1);
                $this->session->set_flashdata('message', lang("sale_updated"));

                redirect("sales");

            } catch (Exception $exception) {
                $this->session->set_flashdata('error', $exception->getMessage());
                redirect($_SERVER["HTTP_REFERER"]);
            }

        } else {

            $this->data['error']    = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv']      = $this->sales_model->getInvoiceByID($id);

            if ($this->data['inv']->date <= date('Y-m-d', strtotime('-12 months'))) {
               // $this->session->set_flashdata('error', lang("sale_x_edited_older_than_3_months"));
               // redirect($_SERVER["HTTP_REFERER"]);
            }
			
            $inv_items = $this->sales_model->getAllInvoiceItems($id);

            $c = rand(100000, 9999999);

            foreach ($inv_items as $item) {

                $row        = $this->site->getProductByID($item->product_id);
                $customer   = $this->site->getCompanyByID($item->customerClient);

                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->details, $row->product_details, $row->cost, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                }

                $row->itemid    = $item->itemid;
                $row->id 		= $item->product_id;
                $row->code 		= $item->product_code;
                $row->name 		= $item->product_name;
                $row->type 		= $item->product_type;
                $row->qty 		= $item->quantity;
                $row->quantity += $item->quantity;

                $row->customerClient = $customer->id;
                $row->customerClientName = $customer->name;
                $row->poltronaClient      = $item->poltronaClient;

                //to
                $row->category_name = $item->nmCategoria;
                $row->categoriaId = $item->product_category;
                $row->comissao = $item->product_comissao;
                $row->fornecedor = $item->nmFornecedor;
                $row->tipoCobrancaFornecedorId = $item->tipoCobrancaFornecedorId;
                $row->condicaoPagamentoFornecedorId = $item->condicaoPagamentoFornecedorId;
                $row->vlTotalPagamentoFornecedorInput = $item->valorRecebimentoValor;

                $row->tipoTransporte = $item->tipoTransporte;
                $row->dataEmissao   = $item->dataEmissao;

                $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);

                if ($programacao) {//TODO APRESENTA A DATA HORA DA PROGRAMACAO

                     $row->horaCheckin = $item->horaSaida;
                     $row->horaCheckOut = $item->horaRetorno;

                     $row->dateCheckin = $item->dataSaida;
                     $row->dateCheckOut = $item->dataRetorno;

                } else {
                    if ($item->horaCheckin != '00:00:00') $row->horaCheckin = $item->horaCheckin;
                    if ($item->horaCheckOut != '00:00:00') $row->horaCheckOut = $item->horaCheckOut;

                    if ($item->dateCheckin != '0000-00-00') $row->dateCheckin = $item->dateCheckin;
                    if ($item->dateCheckOut != '0000-00-00') $row->dateCheckOut = $item->dateCheckOut;
                }

                $row->apresentacaoPassagem  = $item->apresentacaoPassagem;
                $row->dataApresentacao      = $item->dataApresentacao;
                $row->horaApresentacao      = $item->horaApresentacao;
                $row->horaApresentacao      = $item->horaApresentacao;

                $row->nmFornecedor          = $item->nmFornecedor;
                $row->fornecedor            = $item->fornecedorId;

                $row->faixaEtaria = $item->faixaId;
                $row->nmFaixaEtaria = $item->faixaNome;
                $row->descontarVaga = $item->descontarVaga;

                $row->nmFaixaEtaria = $item->faixaNome;

                $row->reserva = $item->reserva;
                $row->tipoDestino = $item->tipoDestino;

                $row->guia              = $item->guia;
                $row->motorista         = $item->motorista;
                $row->referencia_item   = $item->referencia;
                $row->observacao_item   = $item->note_item;
                $row->hora_embarque     = $item->hora_embarque;

                $row->programacaoId = $item->programacaoId;
                $row->servicoPai = $item->servicoPai;
                $row->origem = $item->origem;
                $row->destino = $item->destino;

                $row->valorHospedagem = $item->valorHospedagem;
                $row->valorPorFaixaEtaria = $item->valorPorFaixaEtaria;

                $row->categoriaAcomodacao = $item->categoriaAcomodacao;
                $row->regimeAcomodacao = $item->regimeAcomodacao;

                $row->receptivo = $item->receptivo;
                $row->nmReceptivo = $item->nmReceptivo;

                $row->nmEmpresaManual = $item->nmEmpresaManual;
                $row->telefoneEmpresa = $item->telefoneEmpresa;
                $row->enderecoEmpresa = $item->enderecoEmpresa;
                $row->enderecoHotel = $item->enderecoHotel;

                $row->apresentacaoPassagem = $item->apresentacaoPassagem;
                $row->dataApresentacao = $item->dataApresentacao;
                $row->horaApresentacao = $item->horaApresentacao;


                if ($item->adicional == '1') $row->adicional = true;
                else $row->adicional = false;

                //taxas
                $row->tarifa = $item->tarifa;

                $row->nmTipoTransporte    = $item->nmTipoTransporte;
                $row->tipoTransporte      = $item->tipoTransporteId;

                $row->nmTipoHospedagem    = $item->nmTipoHospedagem;
                $row->tipoHospedagem      = $item->tipoHospedagemId;

                $row->nmLocalEmbarque     = $item->localEmbarque;
                $row->localEmbarque       = $item->localEmbarqueId;

                $row->discount 	= $item->discount ? $item->discount : '0';
                $row->price 	= $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));


                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / ($item->quantity == 0 ? 1 : $item->quantity)) + $this->sma->formatDecimal($item->item_tax / ($item->quantity == 0 ? 1 : $item->quantity)) : $item->unit_price + ($item->item_discount / ($item->quantity == 0 ? 1 : $item->quantity));
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;

                $combo_items = false;

                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    $te = $combo_items;
                    foreach ($combo_items as $combo_item) {
                        $combo_item->quantity = $combo_item->qty * $item->quantity;
                    }
                }

                $ri = $this->Settings->item_addition ? $row->id : $c;

                if ($row->tax_rate) {
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'options' => []);
                } else {

                    $adicionais = $this->VendaRepository_model->getServicosAdicionaisItem($id, $item->id);
                    $itinerarios =  $this->VendaRepository_model->getItinerarios($id, $item->id);;
                    $transfers =  $this->VendaRepository_model->getTransfers($id, $item->id);;

                    $ar = false;
                    $it = false;
                    $tfr = false;
                    $txs = false;

                    $txs['taxas_cliente'] = array(
                        'tarifa' => $item->tarifa,
                        'taxaAdicionalFornecedor' => $item->taxaAdicionalFornecedor,
                        'descontoTarifa' => $item->descontoTarifa,
                        'acrescimoTarifa' => $item->acrescimoTarifa,
                        'adicionalBagagem' => $item->adicionalBagagem,
                        'adicionalReservaAssento' => $item->adicionalReservaAssento,
                        'valorPagoFornecedor' => $item->valorPagoFornecedor,
                        'formaPagamentoFornecedorCustomer' => $item->formaPagamentoFornecedorCustomer,
                        'bandeiraPagoFornecedor' => $item->bandeiraPagoFornecedor,
                        'numeroParcelasPagoFornecedor' => $item->numeroParcelasPagoFornecedor,
                        'descricaoServicos' => $item->descricaoServicos,
                        'observacaoServico' => $item->observacaoServico,
                    );

                    $txs['taxas_fornecedor'] = array(
                        'descontoAbsolutoSupplier' => $item->desconto_fornecedor,
                        'comissao_rav_du' => $item->comissao_rav_du,
                        'incentivoAbsolutoSupplier' => $item->incentivoFornecedor,
                        'outrasTaxasSupplier' => $item->outrasTaxasFornecedor,
                        'taxasCartaoSupplier' => $item->taxaCartaoFornecedor,
                    );

                    if ($transfers) {
                        foreach ($transfers as $transfer) {

                            if ($transfer->tipoTransfer == 'IN') {
                                $tfr[$c . '_in'] = array(
                                    'item' => $c,
                                    'mid' => $transfer->id,
                                    'tipoTransfer' => $transfer->tipoTransfer,
                                    'origem' => $transfer->origem,
                                    'destino' => $transfer->destino,
                                    'data' => $transfer->data,
                                    'hora' => $transfer->hora,
                                    'dataEmissao' => $transfer->dataEmissao,
                                    'dataSaidaVooBus' => $transfer->dataSaidaVooBus,
                                    'horaSaidaBooBus' => $transfer->horaSaidaBooBus,
                                    'referenciaVooBus' => $transfer->referenciaVooBus,
                                    'referencia' => $transfer->referencia,
                                    'fornecedor' => $transfer->fornecedor
                                );
                            }

                            if ($transfer->tipoTransfer = 'OUT') {
                                $tfr[$c . '_out'] = array(
                                    'item' => $c,
                                    'mid' => $transfer->id,
                                    'tipoTransfer' => $transfer->tipoTransfer,
                                    'origem' => $transfer->origem,
                                    'destino' => $transfer->destino,
                                    'data' => $transfer->data,
                                    'hora' => $transfer->hora,
                                    'dataEmissao' => $transfer->dataEmissao,
                                    'dataSaidaVooBus' => $transfer->dataSaidaVooBus,
                                    'horaSaidaBooBus' => $transfer->horaSaidaBooBus,
                                    'referenciaVooBus' => $transfer->referenciaVooBus,
                                    'referencia' => $transfer->referencia,
                                    'fornecedor' => $transfer->fornecedor
                                );
                            }
                        }
                    }

                    if ($adicionais) {
                        foreach ($adicionais as $adicional) {

                            $servicoAdicional = $this->sales_model->getItemByServicoAdicional($id, $adicional->produtoId, $customer->id);

                            $ar[$c. '_servico_adicional'] = array (
                                'item' => $servicoAdicional->id,
                                'mid' => $adicional->produtoId,
                                'item_pai' => $c,

                                'nmCategoria' => $adicional->categoria_produto,
                                'nomeServico' => $adicional->nome_produto,
                                'preco' => $adicional->valor,
                                'servicoCode' => $adicional->codigo_produto,
                                'servicoId' => $adicional->produtoId
                            );
                        }
                    }

                    if ($itinerarios) {
                        foreach ($itinerarios as $itinerario) {
                            $it[rand(100000, 9999999)] = array (
                                'item'=> $c,
                                'mid' => $itinerario->produtoId,

                                'assentoVoo' => $itinerario->assentoVoo,
                                'bagagem' => $itinerario->bagagem,
                                'companhiaAerea' => $itinerario->companhiaAerea,
                                'dataChegada' => $itinerario->dataChegada,
                                'dataPartida'=> $itinerario->dataPartida,
                                'destino' => $itinerario->destino,
                                'horaChegada' => $itinerario->horaChegada,
                                'horaPartida' => $itinerario->horaPartida,
                                'numeroVoo' => $itinerario->numeroVoo,
                                'origem' => $itinerario->origem,
                            );
                        }
                    }

                    $pr[$ri] = array(
                        'order' => time(),
                        'adicionais' => $ar,
                        'itinerarios' => $it,
                        'transfers' => $tfr,
                        'taxas' => $txs,
                        'id' => $c,
                        'item_id' => $row->id,
                        'label' => $row->name,
                        'row' => $row,
                        'combo_items' => $combo_items,
                        'tax_rate' => false,
                        'options' => []
                    );
                }
                $c++;
            }

            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;

            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['meiosDivulgacao']      = $this->Meiodivulgacao_model->getAllActive();

            $this->data['tiposCobranca']        = $this->site->getAllTiposCobrancaReceita(true);
            $this->data['condicoesPagamento']   = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['categories']           = $this->site->getAllCategories();

            $this->data['sessionCode']          = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro']      = $this->settings_model->getPagSeguroSettings();
            $this->data['staff']                = $this->ValorFaixaRepository_model->getStaff();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('edit_sale')));
            $meta = array('page_title' => lang('edit_sale'), 'bc' => $bc);
            $this->page_construct('sales/edit', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function return_sale($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->return_id) {
            $this->session->set_flashdata('error', lang("sale_already_returned"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('return_surcharge', lang("return_surcharge"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));

            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            $percentage = '%';
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                //$option_details = $this->sales_model->getProductOptionByID($item_option);
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity = (0-$_POST['quantity'][$r]);
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;

                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $products[] = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'warehouse_id' => $sale->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $pr_tax,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'sale_item_id' => $sale_item_id,
                    );

                    $si_return[] = array(
                        'id' => $sale_item_id,
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'option_id' => $item_option,
                        'quantity' => (0-$item_quantity),
                        'warehouse_id' => $sale->warehouse_id,
                        );

                    $total += $item_net_price * $item_quantity;
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            if ($this->input->post('discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $order_discount + $product_discount;

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax - $this->sma->formatDecimal($return_surcharge) - $order_discount);
            $data = array('date' => $date,
                'sale_id' => $id,
                'reference_no' => $sale->reference_no,
                'customer_id' => $sale->customer_id,
                'customer' => $sale->customer,
                'biller_id' => $sale->biller_id,
                'biller' => $sale->biller,
                'warehouse_id' => $sale->warehouse_id,
                'note' => $note,
                'total' => $this->sma->formatDecimal($total),
                'product_discount' => $this->sma->formatDecimal($product_discount),
                'order_discount_id' => $order_discount_id,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $this->sma->formatDecimal($product_tax),
                'order_tax_id' => $order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'return_sale_ref' => $reference,
                'sale_status' => 'returned',
                'payment_status' => $sale->payment_status == 'paid' ? 'due' : 'pending',
            );
            if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');
                $payment = array(
                    'date' => $date,
                    'reference_no' => $pay_ref,
                    'amount' => $this->sma->formatDecimal($this->input->post('amount-paid')),
                    'paid_by' => $this->input->post('paid_by'),
                    'cheque_no' => $this->input->post('cheque_no'),
                    'cc_no' => $this->input->post('pcc_no'),
                    'cc_holder' => $this->input->post('pcc_holder'),
                    'cc_month' => $this->input->post('pcc_month'),
                    'cc_year' => $this->input->post('pcc_year'),
                    'cc_type' => $this->input->post('pcc_type'),
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $data['payment_status'] = $grand_total == $this->input->post('amount-paid') ? 'paid' : 'partial';
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $si_return, $payment);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment, $si_return)) {
            $this->session->set_flashdata('message', lang("return_sale_added"));
            redirect("sales/return_sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $sale;
            if ($this->data['inv']->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang("sale_status_x_competed"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->data['inv']->date <= date('Y-m-d', strtotime('-3 months'))) {
                //$this->session->set_flashdata('error', lang("sale_x_edited_older_than_3_months"));
                //redirect($_SERVER["HTTP_REFERER"]);
            }
            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            $c = rand(100000, 9999999);

            foreach ($inv_items as $item) {

                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->details, $row->product_details, $row->cost, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                }
                $pis = $this->sales_model->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->sale_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->qty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id, true);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                if ($row->tax_rate) {
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => $tax_rate, 'options' => $options);
                } else {
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => false, 'options' => $options);
                }
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['payment_ref'] = '';
            $this->data['reference'] = ''; // $this->site->getReference('re');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('return_sale')));
            $meta = array('page_title' => lang('return_sale'), 'bc' => $bc);
            $this->page_construct('sales/return_sale', $meta, $this->data);
        }
    }

    public function archived($id = null) {

        if ($this->input->get('id')) $id = $this->input->get('id');

        try {
            if ($this->sales_model->archived($id)) {

                if ($this->input->is_ajax_request()) {
                    echo lang("Venda(s) Arquiva Com Sucesso!");
                    die();
                }
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) {
                echo  $exception->getMessage();
                die();
            }
        }
    }

    public function unarchive($id = null) {

        if ($this->input->get('id')) $id = $this->input->get('id');

        try {
            if ($this->sales_model->unarchive($id)) {

                if ($this->input->is_ajax_request()) {
                    echo lang("Venda(s) Desarquivada Com Sucesso!");
                    die();
                }
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) {
                echo  $exception->getMessage();
                die();
            }
        }
    }

    public function cancelar_action($id = null)
    {
        try {

            $inv = $this->sales_model->getInvoiceByID($id);

            $motivoCancelamento = new MotivoCancelamentoVenda_model();
            $motivoCancelamento->sale_id = $id;
            $motivoCancelamento->note = $this->input->post('note', true);
            $motivoCancelamento->motivo_cancelamento_id = $this->input->post('motivo_cancelamento_id', true);

            //credito de cliente
            $motivoCancelamento->gerar_credito = $this->input->post('gerar_credito') !== null;
            $motivoCancelamento->value = $this->input->post('value');
            $motivoCancelamento->expiry = $this->input->post('expiry');
            $motivoCancelamento->note_credito = $this->input->post('note_credito');
            $motivoCancelamento->card_no = $this->input->post('card_no', true);
            $motivoCancelamento->customer = $inv->customer;
            $motivoCancelamento->customer_id = $inv->customer_id;

            //reembolso
            $motivoCancelamento->gerar_reembolso = $this->input->post('gerar_reembolso') !== null;
            $motivoCancelamento->value_refund = $this->input->post('value_refund');
            $motivoCancelamento->expiry_refund = $this->input->post('expiry_refund');
            $motivoCancelamento->note_refund = $this->input->post('note_refund');
            $motivoCancelamento->card_no_refund = $this->input->post('card_no_refund', true);
            $motivoCancelamento->customer = $inv->customer;
            $motivoCancelamento->customer_id = $inv->customer_id;

            if ($this->sales_model->cancelar($motivoCancelamento)) {

                if ($this->Settings->send_email_canceling_sale) {
                    $this->send_email($id, 'canceling');
                }

                $this->session->set_flashdata('message', lang('Venda Cancelada Com Sucesso!'));

                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function cancelar($id = null) {

        //$this->sma->checkPermissions(null, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        try {

            $motivoCancelamento = new MotivoCancelamentoVenda_model();
            $motivoCancelamento->sale_id = $id;

            if ($this->sales_model->cancelar($motivoCancelamento)) {

                if ($this->Settings->send_email_canceling_sale) {
                    $this->send_email($id, 'canceling');
                }

                if ($this->input->is_ajax_request()) {
                    echo lang("Venda Cancelada Com Sucesso!");
                    die();
                }

                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) {
                echo  $exception->getMessage();
                die();
            }
        }
    }

    /* ------------------------------- */
    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) $id = $this->input->get('id');

        try {
            if ($this->sales_model->deleteSale($id)) {
                if ($this->input->is_ajax_request()) echo lang("sale_deleted");
                die();
            }

            redirect('welcome');
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            if ($this->input->is_ajax_request()) echo  $exception->getMessage();
            die();
        }
    }

    public function delete_return($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) $id = $this->input->get('id');

        if ($this->sales_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("return_sale_deleted");die();
            }
            $this->session->set_flashdata('message', lang('return_sale_deleted'));
            redirect('welcome');
        }
    }

    public function sale_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    try {
                        $this->sma->checkPermissions('delete');
                        foreach ($_POST['val'] as $id) {
                            $this->sales_model->deleteSale($id);
                        }
                        $this->session->set_flashdata('message', lang("sales_deleted"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect('sales');
                    }

                } elseif ($this->input->post('form_action') == 'archived') {

                    try {
                        $this->sma->checkPermissions('delete');
                        foreach ($_POST['val'] as $id) {
                            $this->sales_model->archived($id);
                        }

                        $this->session->set_flashdata('message', lang("sales_archived"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect('sales');
                    }
                } elseif ($this->input->post('form_action') == 'ratings'){
                    try {

                        foreach ($_POST['val'] as $id) {
                            $avaliar_dias = FALSE;
                            $ratingsArray = $this->sales_model->ratings($avaliar_dias, $id);

                            foreach ($ratingsArray as $rating) {
                                $this->send_email_avaliacao($rating);
                            }
                        }

                        $this->session->set_flashdata('message', lang("sales_ratings"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect('sales');
                    }
                } elseif ($this->input->post('form_action') == 'unarchive') {
                    try {
                        $this->sma->checkPermissions('delete');
                        foreach ($_POST['val'] as $id) {
                            $this->sales_model->unarchive($id);
                        }

                        $this->session->set_flashdata('message', lang("sales_unarchive"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect('sales');
                    }
                } elseif ($this->input->post('form_action') == 'generate_sales_commissions') {
                    try {

                        $this->generate_sales_commission_all($_POST['val']);

                        redirect($_SERVER["HTTP_REFERER"]);
                    } catch (Exception $exception) {
                        $this->session->set_flashdata('error', $exception->getMessage());
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);

                } elseif ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));

                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('Hora'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('Saida'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('Hora'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('Retorno'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('product'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('o_que_inclui'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('faixa'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('assento'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('R.G'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('Orgão Emissor'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('CPF'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('data_nascimento'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('Celular'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('Email'));
                    $this->excel->getActiveSheet()->SetCellValue('U1', lang('Cidade'));
                    $this->excel->getActiveSheet()->SetCellValue('V1', lang('Local_Embarque'));
                    $this->excel->getActiveSheet()->SetCellValue('W1', lang('Hospedagem'));
                    $this->excel->getActiveSheet()->SetCellValue('X1', lang('Tipo_Cobranca'));
                    $this->excel->getActiveSheet()->SetCellValue('Y1', lang('Condicoes'));
                    $this->excel->getActiveSheet()->SetCellValue('Z1', lang('Total Venda'));
                    $this->excel->getActiveSheet()->SetCellValue('AA1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('AB1', lang('payment_status'));
                    $this->excel->getActiveSheet()->SetCellValue('AC1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('AD1', lang('total_por_pessoa'));
                    $this->excel->getActiveSheet()->SetCellValue('AE1', lang('Comissao'));
                    $this->excel->getActiveSheet()->SetCellValue('AF1', lang('payments'));
                    $this->excel->getActiveSheet()->SetCellValue('AG1', lang('Data Primeiro Pagamento'));
                    $this->excel->getActiveSheet()->SetCellValue('AH1', lang('ultimo_pagamento'));
                    $this->excel->getActiveSheet()->SetCellValue('AI1', lang('meio_divulgacao'));

                    $row = 2;

                    $this->db->select('sales.*, condicao_pagamento.name as condicao_pagamento, tipo_cobranca.name as tipo_cobranca, meio_divulgacao.name as meio_divulgacao, companies.name as biller', false);
                    $this->db->join('companies', 'companies.id=sales.biller_id');
                    $this->db->join('condicao_pagamento', 'condicao_pagamento.id=sales.condicaoPagamentoId', 'left');
                    $this->db->join('tipo_cobranca', 'tipo_cobranca.id=sales.tipoCobrancaId', 'left');
                    $this->db->join('meio_divulgacao', 'meio_divulgacao.id=sales.meioDivulgacao', 'left');

                    $this->db->where_in('sales.id', $_POST['val']);
                    $q = $this->db->get('sales');

                    $dataSales = [];

                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowsl) {
                            $dataSales[] = $rowsl;
                        }
                    }

                    $itensAll = $this->sales_model->getAllInvoiceItemsByReport($_POST['val']);
                    $payments = $this->sales_model->getInvoicePaymentsByReports($_POST['val']);

                    foreach ($dataSales as $sale) {
                        //$sale = $this->sales_model->getInvoiceByID($id);
                        //$itens = $this->sales_model->getAllInvoiceItems($sale->id);
                        //$payments = $this->sales_model->getInvoicePayments($id);

                        foreach ($itensAll as $itemArray) {

                            if ($itemArray->sale_id != $sale->id) {
                                continue;
                            } else {
                                $item = $itemArray;
                            }

                            $comissao = $this->calcularComissaoParaExcel($item->product_id, $item->subtotal, $sale->biller_id);
                            $customer = $this->site->getCompanyByID($item->customerClient);

                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, date('d/m/Y', strtotime($sale->date)));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, date('H:i', strtotime($sale->date)));

                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->reference_no);

                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, date('d/m/Y', strtotime($item->dtSaida)));
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, date('H:i', strtotime($item->hrRetorno)));

                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->hrld($item->dtRetorno.' '.$item->hrRetorno));

                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $item->code);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $item->produtc_name);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->clear_tags($item->oqueInclui) );

                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $sale->biller);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $item->faixaNome);
                            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $item->poltronaClient);
                            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->name);
                            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf1);
                            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf3);
                            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->vat_no);

                            if ($customer->data_aniversario) {
                                $this->excel->getActiveSheet()->SetCellValue('Q' . $row, date('d/m/Y', strtotime($customer->data_aniversario)));
                            } else {
                                $this->excel->getActiveSheet()->SetCellValue('Q' . $row, '');
                            }

                            if ($sale->local_saida != null) {
                                $localEmbarque = $sale->local_saida;
                            } else {
                                $localEmbarque = $item->localEmbarque;
                            }

                            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->phone);
                            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $customer->cf5);
                            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $customer->email);
                            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $customer->city);

                            $this->excel->getActiveSheet()->SetCellValue('V' . $row, $localEmbarque);
                            $this->excel->getActiveSheet()->SetCellValue('W' . $row, $item->tipoHospedagem);

                            $this->excel->getActiveSheet()->SetCellValue('X' . $row, $sale->tipo_cobranca);
                            $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $sale->condicao_pagamento);

                            $this->excel->getActiveSheet()->SetCellValue('Z' . $row, $sale->grand_total);
                            $this->excel->getActiveSheet()->SetCellValue('AA' . $row, $sale->paid);
                            $this->excel->getActiveSheet()->SetCellValue('AB' . $row, lang($sale->payment_status));
                            $this->excel->getActiveSheet()->SetCellValue('AC' . $row, lang($sale->sale_status));
                            $this->excel->getActiveSheet()->SetCellValue('AD' . $row, $item->subtotal);
                            $this->excel->getActiveSheet()->SetCellValue('AE' . $row, $comissao);

                            if ($sale->grand_total > 0) {
                                $dataPrimeiroPagamento = '';
                                $dataUltimoPagamento = '';
                                $contador_pagamento = 0;

                                foreach ($payments as $payment) {

                                    if ($payment->sale_id != $sale->id) {
                                        continue;
                                    }

                                    if ($payment->status == 'ESTORNO') {
                                        continue;
                                    }

                                    if ($contador_pagamento == 0) {
                                        $dataPrimeiroPagamento = date('d/m/Y H:i', strtotime($payment->date));
                                        $contador_pagamento++;
                                    }

                                    $dataUltimoPagamento = date('d/m/Y H:i', strtotime($payment->date));
                                }

                                $this->excel->getActiveSheet()->SetCellValue('AF' . $row, $item->subtotal * (($sale->paid*100/$sale->grand_total)/100));
                                $this->excel->getActiveSheet()->SetCellValue('AG' . $row, $dataPrimeiroPagamento);
                                $this->excel->getActiveSheet()->SetCellValue('AH' . $row, $dataUltimoPagamento);


                            } else {
                                $this->excel->getActiveSheet()->SetCellValue('AF' . $row, 0);
                                $this->excel->getActiveSheet()->SetCellValue('AG' . $row, '');
                                $this->excel->getActiveSheet()->SetCellValue('AH' . $row, '');
                            }

                            $this->excel->getActiveSheet()->SetCellValue('AI' . $row, $sale->meio_divulgacao);

                            $row++;
                        }
                    }

                    foreach (range('A', $this->excel->getActiveSheet()->getHighestColumn()) as $columnID) {
                        $this->excel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                    }

                    $highestColumn = $this->excel->getActiveSheet()->getHighestColumn();
                    $this->excel->getActiveSheet()->getStyle('A1:' . $highestColumn . '1')->getFont()->setBold(true);

                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');

                    if ($this->input->post('form_action') == 'export_pdf') {

                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;

                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function send_email_avaliacao($rating_id = null) {

        $rating          = $this->site->getRatingID($rating_id);
        $customer        = $this->site->getCompanyByID($rating->customer_id);
        $product         = $this->site->getProductByID($rating->product_id);

        $this->load->library('parser');

        $parse_data = array(
            'url'               => $this->Settings->url_site_domain.'/rating/'.$rating->uuid,
            'product_name'      => $product->name,
            'contact_person'    => $customer->name,
            'company'           => $customer->company,
            'site_link'         => base_url(),
            'site_name'         => $this->Settings->site_name,
            'logo'              => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
        );

        $rating_temp = file_get_contents('./themes/default/views/email_templates/ratings.html');

        $subject = 'Obrigado pela sua presença - por favor, deixe uma avaliação 🌟 ' . $this->Settings->site_name;

        if ($customer->email) {
            $to = $customer->email;

            $message = $this->parser->parse_string($rating_temp, $parse_data);

            $body = <<<EOT
                $message
            EOT;

            $this->sma->send_email_api($to, $subject, $body);
        }
    }

    public function reportAllByProgramacaoId($progrmacaoId)
    {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('sales'));

        $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('Hora'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('Saida'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('Hora'));
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('Retorno'));
        $this->excel->getActiveSheet()->SetCellValue('G1', lang('code'));
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('product'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('o_que_inclui'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('biller'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('faixa'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('assento'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('customer'));
        $this->excel->getActiveSheet()->SetCellValue('N1', lang('R.G'));
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('Orgão Emissor'));
        $this->excel->getActiveSheet()->SetCellValue('P1', lang('CPF'));
        $this->excel->getActiveSheet()->SetCellValue('Q1', lang('data_nascimento'));
        $this->excel->getActiveSheet()->SetCellValue('R1', lang('phone'));
        $this->excel->getActiveSheet()->SetCellValue('S1', lang('Celular'));
        $this->excel->getActiveSheet()->SetCellValue('T1', lang('Email'));
        $this->excel->getActiveSheet()->SetCellValue('U1', lang('Cidade'));
        $this->excel->getActiveSheet()->SetCellValue('V1', lang('Local_Embarque'));
        $this->excel->getActiveSheet()->SetCellValue('W1', lang('Hospedagem'));
        $this->excel->getActiveSheet()->SetCellValue('X1', lang('Tipo_Cobranca'));
        $this->excel->getActiveSheet()->SetCellValue('Y1', lang('Condicoes'));
        $this->excel->getActiveSheet()->SetCellValue('Z1', lang('Total Venda'));
        $this->excel->getActiveSheet()->SetCellValue('AA1', lang('paid'));
        $this->excel->getActiveSheet()->SetCellValue('AB1', lang('payment_status'));
        $this->excel->getActiveSheet()->SetCellValue('AC1', lang('status'));
        $this->excel->getActiveSheet()->SetCellValue('AD1', lang('Total por pessoa'));
        $this->excel->getActiveSheet()->SetCellValue('AE1', lang('Comissao'));
        $this->excel->getActiveSheet()->SetCellValue('AF1', lang('payments'));
        $this->excel->getActiveSheet()->SetCellValue('AG1', lang('Data Primeiro Pagamento'));
        $this->excel->getActiveSheet()->SetCellValue('AH1', lang('ultimo_pagamento'));
        $this->excel->getActiveSheet()->SetCellValue('AI1', lang('meio_divulgacao'));

        $row = 2;

        $sales = $this->Sales_model->getAllSalesByProgramacao($progrmacaoId);

        foreach ($sales as $s) {
            $sale = $this->sales_model->getInvoiceByID($s->id);
            $itens = $this->sales_model->getAllInvoiceItems($s->id);
            $payments = $this->sales_model->getInvoicePayments($s->id);

            foreach ($itens as $item) {

                $comissao = $this->calcularComissaoParaExcel($item->product_id, $item->subtotal, $sale->biller_id);
                $customer = $this->site->getCompanyByID($item->customerClient);

                $this->excel->getActiveSheet()->SetCellValue('A' . $row, date('d/m/Y', strtotime($sale->date)));
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, date('H:i', strtotime($sale->date)));

                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->reference_no);

                $this->excel->getActiveSheet()->SetCellValue('D' . $row, date('d/m/Y', strtotime($item->dtSaida)));
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, date('H:i', strtotime($item->hrRetorno)));

                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->hrld($item->dtRetorno.' '.$item->hrRetorno));

                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $item->code);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $item->produtc_name);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->clear_tags($item->oqueInclui) );

                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $sale->biller_name);
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, $item->faixaNome);
                $this->excel->getActiveSheet()->SetCellValue('L' . $row, $item->poltronaClient);
                $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->name);
                $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf1);
                $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf3);
                $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->vat_no);

                if ($customer->data_aniversario) $this->excel->getActiveSheet()->SetCellValue('Q' . $row, date('d/m/Y', strtotime($customer->data_aniversario)));
                else $this->excel->getActiveSheet()->SetCellValue('Q' . $row, '');

                if ($sale->local_saida != null) $localEmbarque = $sale->local_saida;
                else $localEmbarque = $item->localEmbarque;

                $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->phone);
                $this->excel->getActiveSheet()->SetCellValue('S' . $row, $customer->cf5);
                $this->excel->getActiveSheet()->SetCellValue('T' . $row, $customer->email);
                $this->excel->getActiveSheet()->SetCellValue('U' . $row, $customer->city);

                $this->excel->getActiveSheet()->SetCellValue('V' . $row, $localEmbarque);
                $this->excel->getActiveSheet()->SetCellValue('W' . $row, $item->tipoHospedagem);

                $this->excel->getActiveSheet()->SetCellValue('X' . $row, $sale->tipoCobranca);
                $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $sale->condicaoPagamento);

                $this->excel->getActiveSheet()->SetCellValue('Z' . $row, $sale->grand_total);
                $this->excel->getActiveSheet()->SetCellValue('AA' . $row, $sale->paid);
                $this->excel->getActiveSheet()->SetCellValue('AB' . $row, lang($sale->payment_status));
                $this->excel->getActiveSheet()->SetCellValue('AC' . $row, lang($sale->sale_status));
                $this->excel->getActiveSheet()->SetCellValue('AD' . $row, $item->subtotal);
                $this->excel->getActiveSheet()->SetCellValue('AE' . $row, $comissao);

                if ($sale->grand_total > 0) {
                    $dataPrimeiroPagamento = '';
                    $dataUltimoPagamento = '';
                    $contador_pagamento = 0;

                    foreach ($payments as $payment) {
                        if ($payment->status == 'ESTORNO') continue;

                        if ($contador_pagamento == 0) {
                            $dataPrimeiroPagamento = date('d/m/Y H:i', strtotime($payment->date));
                            $contador_pagamento++;
                        }

                        $dataUltimoPagamento = date('d/m/Y H:i', strtotime($payment->date));
                    }

                    $this->excel->getActiveSheet()->SetCellValue('AF' . $row, $item->subtotal * (($sale->paid*100/$sale->grand_total)/100));
                    $this->excel->getActiveSheet()->SetCellValue('AG' . $row, $dataPrimeiroPagamento);
                    $this->excel->getActiveSheet()->SetCellValue('AH' . $row, $dataUltimoPagamento);


                } else {
                    $this->excel->getActiveSheet()->SetCellValue('AF' . $row, 0);
                    $this->excel->getActiveSheet()->SetCellValue('AG' . $row, '');
                    $this->excel->getActiveSheet()->SetCellValue('AH' . $row, '');
                }

                $this->excel->getActiveSheet()->SetCellValue('AI' . $row, $sale->meio_divulgacao_name);

                $row++;
            }
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(30);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(30);

        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'sales_' . date('Y_m_d_H_i_s');

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
        return $objWriter->save('php://output');
    }

    function clear_tags($str) {

        $str = str_replace('</p>','@quebra_linha@', $str);
        $str = str_replace('<br>','@quebra_linha@', $str);
        $str = str_replace('<li>','@quebra_linha@', $str);
        $str = str_replace('<strong>','@quebra_linha_strong@', $str);
        $str = str_replace('</strong>','@quebra_linha_strong@', $str);

        $str = $this->decode_html($str);

        $str = strip_tags($str);
        $str = trim($str);

        $str = str_replace('@quebra_linha_strong@','  ', $str);
        $str = str_replace('@quebra_linha@',' - ', $str);

        return $str;

    }

    function export_customer_all_actions($progrmacaoId)
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('customer'));
        $this->excel->getActiveSheet()->SetCellValue('A1', 'First Name');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'E-mail Address');
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('address'));
        $this->excel->getActiveSheet()->SetCellValue('E1', 'Home City');
        $this->excel->getActiveSheet()->SetCellValue('F1', lang('state'));
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Home Postal Code');
        $this->excel->getActiveSheet()->SetCellValue('H1', lang('country'));
        $this->excel->getActiveSheet()->SetCellValue('I1', lang('vat_no'));
        $this->excel->getActiveSheet()->SetCellValue('J1', lang('ccf1'));
        $this->excel->getActiveSheet()->SetCellValue('K1', lang('ccf2'));
        $this->excel->getActiveSheet()->SetCellValue('L1', lang('ccf3'));
        $this->excel->getActiveSheet()->SetCellValue('M1', lang('Web Page'));
        $this->excel->getActiveSheet()->SetCellValue('N1', 'Mobile Phone');
        $this->excel->getActiveSheet()->SetCellValue('O1', lang('ccf6'));
        $this->excel->getActiveSheet()->SetCellValue('P1', 'Anniversary');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'Home State');
        $this->excel->getActiveSheet()->SetCellValue('R1', 'Home City');
        $this->excel->getActiveSheet()->SetCellValue('S1', 'Home Street');
        $this->excel->getActiveSheet()->SetCellValue('T1', 'Home Postal Code');
        $this->excel->getActiveSheet()->SetCellValue('U1', 'Group Membership');
        //Company : Empresa
        //Job Title : Cargo/Função

        $row = 2;

        $customers = $this->Sales_model->getAllCustomerByProgramacao($progrmacaoId);
        $agenda = $this->AgendaViagemService_model->getProgramacaoById($progrmacaoId);
        $produto = $this->products_model->getProductByID($agenda->produto);
        $filename = 'GRUPO '.$produto->name.' SAÍDA '.$this->sma->hrsd($agenda->dataSaida) ;

        foreach ($customers as $customer) {

            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->name);
            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->email);
            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->phone);
            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->address);
            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->city);
            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->state);
            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->postal_code);
            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->country);
            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->vat_no);
            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->cf1);
            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf2);
            $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf3);
            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf4);
            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf5);
            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf6);
            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->data_aniversario);
            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $customer->state);
            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->city);
            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $customer->address. ' '.$customer->numero.' '.$customer->complemento.' '.$customer->bairro);
            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $customer->postal_code);
            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $customer->city);
            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $filename);

            $row++;
        }

        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');

        return $objWriter->save('php://output');
    }

    function calcularComissaoParaExcel($produtoId, $totalDaPassagem, $biller_id) {

        $produto                = $this->products_model->getProductByID($produtoId);
        $tipoComissaoProduto    = $produto->tipoComissao;
        $tipoCalculoComissao    = $produto->tipoCalculoComissao;
        $comissao               = 0;

        if ($tipoComissaoProduto == 'comissao_produto') {
            $percentualComissao = $produto->comissao;

            if ($tipoCalculoComissao == '1') {
                $comissao = $totalDaPassagem * ($percentualComissao / 100);
            } else {
                $comissao = $percentualComissao;
            }
        }

        if ($tipoComissaoProduto == 'comissao_vendedor') {
            $vendedor           = $this->companies_model->getCompanyByID($biller_id);
            $percentualComissao = $vendedor->comissao;
            $comissao = $totalDaPassagem * ($percentualComissao / 100);
        }

        if ($tipoComissaoProduto == 'comissao_categoria') {
            $cagegoria          = $this->sales_model->getCategoryByCId($produto->category_id);
            $percentualComissao = $cagegoria->comissao;
            $comissao           = $totalDaPassagem * ($percentualComissao / 100);
        }

        return $comissao;
    }

    function pdf_produtos($id = NULL, $view = NULL)
    {
        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->products_model->getSubCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load->view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'products/pdf', $this->data, TRUE);
            $this->sma->generate_pdf($html, $name);
        }
    }

    /* ------------------------------- */

    public function deliveries()
    {
        $this->sma->checkPermissions();

        $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('deliveries')));
        $meta = array('page_title' => lang('deliveries'), 'bc' => $bc);
        $this->page_construct('sales/deliveries', $meta, $this->data);

    }

    public function getDeliveries()
    {
        $this->sma->checkPermissions('deliveries');

        $detail_link = anchor('sales/view_delivery/$1', '<i class="fa fa-file-text-o"></i> ' . lang('delivery_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('sales/email_delivery/$1', '<i class="fa fa-envelope"></i> ' . lang('email_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit_delivery/$1', '<i class="fa fa-edit"></i> ' . lang('edit_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $pdf_link = anchor('sales/pdf_delivery/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_delivery") . "</b>' data-content=\"<p>"
        . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('sales/delete_delivery/$1') . "'>"
        . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
        . lang('delete_delivery') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
        . lang('actions') . ' <span class="caret"></span></button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>' . $detail_link . '</li>
        <li>' . $edit_link . '</li>
        <li>' . $pdf_link . '</li>
        <li>' . $delete_link . '</li>
    </ul>
</div></div>';

        $this->load->library('datatables');
        //GROUP_CONCAT(CONCAT('Name: ', sale_items.product_name, ' Qty: ', sale_items.quantity ) SEPARATOR '<br>')
        $this->datatables
            ->select("deliveries.id as id, date, do_reference_no, sale_reference_no, customer, address")
            ->from('deliveries')
            ->join('sale_items', 'sale_items.sale_id=deliveries.sale_id', 'left')
            ->group_by('deliveries.id');
        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf_delivery($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);

        $this->data['delivery'] = $deli;
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);

        $name = lang("delivery") . "_" . str_replace('/', '_', $deli->do_reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf_delivery', $this->data, true);
        if ($view) {
            $this->load->view($this->theme . 'sales/pdf_delivery', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function view_delivery($id = null)
    {
        $this->sma->checkPermissions('deliveries');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        if (!$sale) {
            $this->session->set_flashdata('error', lang('sale_not_found'));
            $this->sma->md();
        }
        $this->data['delivery'] = $deli;
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);
        $this->data['page_title'] = lang("delivery_order");

        $this->load->view($this->theme . 'sales/view_delivery', $this->data);
    }

    public function add_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        //$this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');

        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $dlDetails = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no') ? $this->input->post('do_reference_no') : $this->site->getReference('do'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );
        } elseif ($this->input->post('add_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addDelivery($dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_added"));
            redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $sale = $this->sales_model->getInvoiceByID($id);
            $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
            $this->data['inv'] = $sale;
            $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/add_delivery', $this->data);
        }
    }

    public function edit_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');
        //$this->form_validation->set_rules('note', lang("note"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $dlDetails = array(
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $dlDetails['date'] = $date;
            }
        } elseif ($this->input->post('edit_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateDelivery($id, $dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_updated"));
            redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['delivery'] = $this->sales_model->getDeliveryByID($id);
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/edit_delivery', $this->data);
        }
    }

    public function delete_delivery($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteDelivery($id)) {
            echo lang("delivery_deleted");
        }

    }

    public function delivery_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_delivery');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteDelivery($id);
                    }
                    $this->session->set_flashdata('message', lang("deliveries_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deliveries'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('do_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $delivery = $this->sales_model->getDeliveryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($delivery->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $delivery->do_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $delivery->sale_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $delivery->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $delivery->address);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

                    $filename = 'deliveries_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_delivery_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* -------------------------------------------------------------------------------- */

    public function payments($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['payments'] = $this->sales_model->getInvoicePayments($id);
        $this->load->view($this->theme . 'sales/payments', $this->data);
    }

    public function payment_note($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);

        if ($biller) {
            $this->data['biller'] = $biller;
        } else {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
        }

        $this->data['inv'] = $inv;
        $this->data['fatura'] = $this->financeiro_model->getFaturaById($payment->fatura);
        $this->data['parcela'] = $this->financeiro_model->getParcelaOneByFatura($payment->fatura);

        $this->data['payment'] = $payment;
        $this->data['customer'] = $this->site->getCompanyByID($payment->pessoa);
        $this->data['page_title'] = $this->lang->line("payment_pdf");

        $this->load->view($this->theme . 'sales/payment_note', $this->data);
    }

    public function sale_termo_aceite($event_id = null, $view = null)
    {
        $event      = $this->EventSaleRepository_model->getById($event_id);
        $inv        = $this->sales_model->getInvoiceByID($event->sale_id);
        $customer   = $this->site->getCompanyByID($inv->customer_id);

        $this->data['event']    = $event;
        $this->data['inv']      = $inv;
        $this->data['biller']   = $this->site->getCompanyByID($inv->biller_id);
        $this->data['customer'] = $customer;

        if ($view) {
            $this->load->view($this->theme . 'sales/termos_aceite_log', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'sales/termos_aceite_log', $this->data, true);
            $name = lang("termo_aceite").'_'. $customer->name . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

    public function payment_pdf($id = null, $view = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);

        if ($biller) {
            $this->data['biller'] = $biller;
        } else {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
        }

        $this->data['inv'] = $inv;
        $this->data['fatura'] = $this->financeiro_model->getFaturaById($payment->fatura);
        $this->data['parcela'] = $this->financeiro_model->getParcelaOneByFatura($payment->fatura);

        $this->data['payment'] = $payment;
        $this->data['customer'] = $this->site->getCompanyByID($payment->pessoa);
        $this->data['page_title'] = $this->lang->line("payment_note");

        if ($view) {
            $this->load->view($this->theme . 'sales/payment_pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'sales/payment_pdf', $this->data, true);

            $name = lang("payment") . "_" . str_replace('/', '_', $payment->reference_no) . ".pdf";
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }

    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            //if ($this->input->post('paid_by') == 'deposit') {
              //  $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
               // $customer_id = $sale->customer_id;
              //  if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                   // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                   // redirect($_SERVER["HTTP_REFERER"]);
              //  }
           // } else {
                $customer_id = null;
           // }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $sale = $this->site->getSaleByID($this->input->post('sale_id'));
            $user = $this->site->getUserByBiller($sale->biller_id);
            $user_id = $this->session->userdata('user_id');

            if ($user) $user_id = $user->id;

            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $user_id,
                'type' => 'received',
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addPayment($payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $sale = $this->sales_model->getInvoiceByID($id);
            $this->data['inv'] = $sale;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/add_payment', $this->data);
        }
    }

	 public function add_payment_mesclar($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');

        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            //if ($this->input->post('paid_by') == 'deposit') {
                //$sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
                //$customer_id = $sale->customer_id;
                //if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                   // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                   // redirect($_SERVER["HTTP_REFERER"]);
                //}
           // } else {
                $customer_id = null;
           // }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

		if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
		
		$saleb = $this->sales_model->getInvoiceByID($id);
		
		//$this->sma->print_arrays($sale);
		
		$sales = $this->sales_model->getAllSalesByCustomer(4);
 		$abrir = false;
		$totalVenda = 0;
		
		foreach($sales as $sl) {
		
 			if ($sl->payment_status != 'paid') {
 				$grand_total = $sl->grand_total;
				$paid = $sl->paid;
				$totalVenda = $totalVenda + ($grand_total - $paid);
			}
		}
		
		$amounr_paid = $this->input->post('amount-paid');
		
		foreach($sales as $sl) {
			
			$grand_total = $sl->grand_total;
			$paid = $sl->paid;
			
			if  ($amounr_paid > ($grand_total - $paid) ) {
				$pos_paid =  ($grand_total - $paid);
				$amounr_paid = $amounr_paid - $pos_paid;
			} else {
				$pos_paid = ($grand_total - $paid);
			}
			 		
			$pos_balance = $grand_total - $pos_paid;
			
			$payment = array(
                'date' => $date,
                'sale_id' => $sl->id,
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $pos_paid,
				'pos_paid' =>$pos_paid,
				'pos_balance' =>$pos_balance,
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'received',
             );
			 
			 //$this->sma->print_arrays($payment);
			
			if ($this->form_validation->run() == true && $this->sales_model->addPaymentMesclar($payment, $sl->payment_status, $customer_id)) {
				$this->session->set_flashdata('message', lang("payment_added"));
			} else {
				$abrir = true;
			}
		}
		
		if ($abrir) {
			$this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
			$sale = $this->sales_model->getInvoiceByID(null);
			$this->data['inv'] = $sale;
			$this->data['totalVenda'] = $totalVenda;
			$this->data['payment_ref'] = ''; //$this->site->getReference('pay');
			$this->data['modal_js'] = $this->site->modal_js();
			$this->load->view($this->theme . 'sales/add_payment_mesclar', $this->data);
		} else {
			redirect($_SERVER["HTTP_REFERER"]);
		}
    }
	
    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $payment = $this->sales_model->getPaymentByID($id);
        if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') {
            $this->session->set_flashdata('error', lang('x_edit_payment'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
           // if ($this->input->post('paid_by') == 'deposit') {
             //   $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
             //   $customer_id = $sale->customer_id;
              //  $amount = $this->input->post('amount-paid')-$payment->amount;
              //  if ( ! $this->site->check_customer_deposit($customer_id, $amount)) {
                //    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                //    redirect($_SERVER["HTTP_REFERER"]);
               // }
           // } else {
                $customer_id = null;
            //}
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updatePayment($id, $payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_payment', $this->data);
        }
    }

    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deletePayment($id)) {
            //echo lang("payment_deleted");
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    public function getProgramacao() {
        $this->sma->send_json($this->sales_model->getProgramacao('', null));
    }

    public function suggestions()
    {
        $term           = $this->input->get('term', true);
        $warehouse_id   = $this->input->get('warehouse_id', true);
        $customer_id    = $this->input->get('customer_id', true);
        $ano            = $this->input->get('ano', true);
        $mes            = $this->input->get('mes', true);
        $data_passeio   = $this->input->get('data_passeio', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed   = $this->sma->analyze_term($term);
        $sr         = $analyzed['term'];
        $option_id  = $analyzed['option_id'];

        $limit = 200;
        $productId = null;
        $group_by = NULL;

        $customer       = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows           = $this->sales_model->getProgramacoesVenda($sr, $warehouse_id, $limit, $productId, $group_by, $ano, $mes, $data_passeio);

        if ($rows) {
            foreach ($rows as $row) {

                $label =  $row->name.' - '.$this->sma->hrsd($row->dataSaida). ' '.$this->sma->hf($row->horaSaida);

                if ($row->programacaoId != null) {
                    $agenda = $this->AgendaViagemService_model->getProgramacaoById($row->programacaoId);

                    $totalQuartosDisponivel = 0;

                    foreach ($agenda->getQuartos() as $quarto) {
                        $totalQuartosDisponivel += $quarto->qtd_quartos_disponivel;
                    }

                    if ($agenda->getControleEstoqueHospedagem()) {
                        $label .= ' ('.$agenda->getTotalDisponvel().' Vaga(s) disponíveis | Quarto(s) Disponíveis: '.$totalQuartosDisponivel.')';
                    } else {
                        if ($agenda->getTotalDisponvel() > 1) {
                            $label .=  '  ('.$agenda->getTotalDisponvel() .' Vagas disponível)';
                        } else {
                            $label .= ' (' . $agenda->getTotalDisponvel() . ' Vaga disponível)';
                        }
                    }

                    $row->quantity = $agenda->getTotalDisponvel();
                }

                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';

                $row->destino = '';
                $row->origem = '';
                $row->customerClient = $customer->id;
                $row->customerClientName = $customer->name;

                $row->faixaEtaria = null;
                $row->nmFaixaEtaria = '';

                $row->dateCheckin = $row->dataSaida;
                $row->dateCheckOut = $row->dataRetorno;

                $row->horaCheckin = $row->horaSaida;
                $row->horaCheckOut = $row->horaRetorno;

                $row->dataEmissao = date('Y-m-d');
                $row->digitado = '0';
                $row->comissao = 0;

                $opt = json_decode('{}');
                $opt->price = 0;
                $row->option = $option_id;

                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }

                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $row->real_unit_price = $row->price;
                $combo_items = false;

                $pr[] = array(
                    'id' => str_replace(".", "", microtime(true)),
                    'item_id' => $row->id,
                    'label' => $label,
                    'category' => $row->category_id,
                    'row' => $row,
                    'combo_items' => $combo_items,
                    'tax_rate' => false,
                    'options' => [],
                    'dataSaida' => $this->sma->hrsd($row->dataSaida)
                );
            }

            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function imprimir_recibo_pagamento($pagamentoId) {
        echo $this->sales_model->imprimir_recibo_pagamento($pagamentoId);
    }

    /* -------------------------------------------------------------------------------------- */
    public function sale_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', $this->lang->line("upload_file"), 'xss_clean');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {

            $quantity = "quantity";
            $product = "product";
            $unit_cost = "unit_cost";
            $tax_rate = "tax_rate";
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            $percentage = '%';

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');

                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("sales/sale_by_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'net_unit_price', 'quantity', 'variant', 'item_tax_rate', 'discount', 'serial');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {

                    if (isset($csv_pr['code']) && isset($csv_pr['net_unit_price']) && isset($csv_pr['quantity'])) {

                        if ($product_details = $this->sales_model->getProductByCode($csv_pr['code'])) {

                            if ($csv_pr['variant']) {
                                $item_option = $this->sales_model->getProductVariantByName($csv_pr['variant'], $product_details->id);
                                if (!$item_option) {
                                    $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $product_details->name . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $item_option = json_decode('{}');
                                $item_option->id = null;
                            }

                            $item_id = $product_details->id;
                            $item_type = $product_details->type;
                            $item_code = $product_details->code;
                            $item_name = $product_details->name;
                            $item_net_price = $this->sma->formatDecimal($csv_pr['net_unit_price']);
                            $item_quantity = $csv_pr['quantity'];
                            $item_tax_rate = $csv_pr['item_tax_rate'];
                            $item_discount = $csv_pr['discount'];
                            $item_serial = $csv_pr['serial'];

                            if (isset($item_code) && isset($item_net_price) && isset($item_quantity)) {
                                $product_details = $this->sales_model->getProductByCode($item_code);

                                if (isset($item_discount)) {
                                    $discount = $item_discount;
                                    $dpos = strpos($discount, $percentage);
                                    if ($dpos !== false) {
                                        $pds = explode("%", $discount);
                                        $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($item_net_price)) * (Float) ($pds[0])) / 100);
                                    } else {
                                        $pr_discount = $this->sma->formatDecimal($discount);
                                    }
                                } else {
                                    $pr_discount = 0;
                                }
                                $item_net_price = $this->sma->formatDecimal($item_net_price - $pr_discount);
                                $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                                $product_discount += $pr_item_discount;

                                if (isset($item_tax_rate) && $item_tax_rate != 0) {

                                    if ($tax_details = $this->sales_model->getTaxRateByName($item_tax_rate)) {
                                        $pr_tax = $tax_details->id;
                                        if ($tax_details->type == 1) {

                                            $item_tax = $this->sma->formatDecimal((($item_net_price) * $tax_details->rate) / 100, 4);
                                            $tax = $tax_details->rate . "%";

                                        } elseif ($tax_details->type == 2) {
                                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                                            $tax = $tax_details->rate;
                                        }
                                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                                    } else {
                                        $this->session->set_flashdata('error', lang("tax_not_found") . " ( " . $item_tax_rate . " ). " . lang("line_no") . " " . $rw);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }

                                } elseif ($product_details->tax_rate) {

                                    $pr_tax = $product_details->tax_rate;
                                    $tax_details = $this->site->getTaxRateByID($pr_tax);
                                    if ($tax_details->type == 1) {

                                        $item_tax = $this->sma->formatDecimal((($item_net_price) * $tax_details->rate) / 100, 4);
                                        $tax = $tax_details->rate . "%";

                                    } elseif ($tax_details->type == 2) {

                                        $item_tax = $this->sma->formatDecimal($tax_details->rate);
                                        $tax = $tax_details->rate;

                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

                                } else {
                                    $item_tax = 0;
                                    $pr_tax = 0;
                                    $pr_item_tax = 0;
                                    $tax = "";
                                }
                                $product_tax += $pr_item_tax;

                                $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);
                                $products[] = array(
                                    'product_id' => $item_id,
                                    'product_code' => $item_code,
                                    'product_name' => $item_name,
                                    'product_type' => $item_type,
                                    'option_id' => $item_option->id,
                                    'net_unit_price' => $item_net_price,
                                    'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                                    'quantity' => $item_quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $pr_tax,
                                    'tax' => $tax,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'subtotal' => $this->sma->formatDecimal($subtotal),
                                    'serial_no' => $item_serial,
                                    'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                                    'real_unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax + $pr_discount),
                                );

                                $total += $item_net_price * $item_quantity;
                            }

                        } else {
                            $this->session->set_flashdata('error', $this->lang->line("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . $this->lang->line("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $rw++;
                    }

                }
            }

            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $this->sma->formatDecimal($total),
                'product_discount' => $this->sma->formatDecimal($product_discount),
                'order_discount_id' => $order_discount_id,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $this->sma->formatDecimal($product_tax),
                'order_tax_id' => $order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($payment_status == 'paid') {

                $payment = array(
                    'date' => $date,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $grand_total,
                    'paid_by' => 'cash',
                    'cheque_no' => '',
                    'cc_no' => '',
                    'cc_holder' => '',
                    'cc_month' => '',
                    'cc_year' => '',
                    'cc_type' => '',
                    'created_by' => $this->session->userdata('user_id'),
                    'note' => lang('auto_added_for_sale_by_csv') . ' (' . lang('sale_reference_no') . ' ' . $reference . ')',
                    'type' => 'received',
                );

            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', $this->lang->line("sale_added"));
            redirect("sales");
        } else {

            $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['slnumber'] = $this->site->getReference('so');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale_by_csv')));
            $meta = array('page_title' => lang('add_sale_by_csv'), 'bc' => $bc);
            $this->page_construct('sales/sale_by_csv', $meta, $this->data);
        }
    }

    public function generate_sales_commission($sale_id)
    {
        try {

            $this->load->model('service/CommissionsService_model', 'CommissionsService_model');

            if ($this->CommissionsService_model->save($sale_id)) {

                if ($this->input->is_ajax_request()) {
                    echo lang('Comissão da Venda Gerada Com Sucesso!');
                    die();
                }

                $this->session->set_flashdata('message', 'Comissão da Venda Gerada Com Sucesso!');

                redirect($_SERVER["HTTP_REFERER"]);
            }
        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function generate_sales_commission_all($sales_id)
    {
        try {

            $this->load->model('service/CommissionsService_model', 'CommissionsService_model');

            foreach ($sales_id as $id) {
                $this->CommissionsService_model->save($id);
            }

            $this->session->set_flashdata('message', 'Comissões da Venda Geradas Com Sucesso!');

        } catch (Exception $exception) {
            $this->session->set_flashdata('error', $exception->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function gerar_contrato($contractID = null, $venda = array(), $products = array()): ?string
    {
        $this->load->model('service/ParseContract_model', 'ParseContract_model');
        $this->load->model('dto/ParseContractDTO_model', 'ParseContractDTO_model');
        $this->load->model('repository/ContractRepository_model', 'ContractRepository_model');

        $config = new ParseContractDTO_model();
        $config->type = ParseContractDTO_model::ARRAY;
        $config->venda = $venda;
        $config->products = $products;

        if ($contractID == null) {
            $contractID = $this->Settings->contract_id;
        }

        $config->contractID = $contractID;

        $contractConfig = $this->ContractRepository_model->getById($contractID);

        $config->tipo = $contractConfig->type;

        if ($contractConfig->type == 'text') {
            return $this->gerar_contrato_pdf($config);
        } else if ($contractConfig->type == 'document') {
            return $this->gerar_contrato_word($config, $contractConfig);
        } else {
            return null;
        }

    }

    private function gerar_contrato_pdf($config): string
    {

        $biller = $this->site->getCompanyByID($this->Settings->default_biller);

        $this->data['converted_contract'] = $this->ParseContract_model->parse($config);

        $html = $this->load->view($this->theme . 'contracts/gerar_contrato', $this->data, true);

        $filename = md5(uniqid(mt_rand())) . ".pdf";

        return $this->sma->gerar_contrato_pdf($html, $filename, 'S', $this->data['biller']->invoice_footer, null, null, null, 'P', base_url() . 'assets/uploads/logos/'.$biller->logo);

    }

    private function gerar_contrato_word($config, $contractConfig): string
    {
        $this->load->helper('margedocx');

        $config->retorno = ParseContractDTO_model::TIPO_RETORNO_ARRAY;

        $tagsArray = $this->ParseContract_model->parse($config);

        $this->upload_path = 'assets/uploads/documents/'.$this->session->userdata('cnpjempresa');

        $arquivo_entrada    = $this->upload_path.'/'.$contractConfig->file_name;

        $filename = md5(uniqid(mt_rand()));

        $filename_docx = $filename . ".docx";
        $filename_html = $filename . ".html";
        $filename_pdf  = $filename . ".pdf";

        $arquivo_saida_docx = $this->upload_path.'/'.$filename_docx;
        $arquivo_saida_html = $this->upload_path.'/'.$filename_html;

        marge_docx($arquivo_entrada, $arquivo_saida_docx, $tagsArray);

        $phpWord = IOFactory::load($arquivo_saida_docx);
        $phpWord->save($arquivo_saida_html, 'HTML');

        $html_gerado = file_get_contents($arquivo_saida_html);

        $dompdf = new Dompdf();
        $dompdf->loadHtml($html_gerado);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $output = $dompdf->output();

        if (!file_exists($this->upload_path . '/')) {
            mkdir($this->upload_path, 0777, true);
        }

        file_put_contents($this->upload_path . '/' . $filename_pdf, $output);

        unlink($arquivo_saida_html);

        return $filename_docx;
    }

}
