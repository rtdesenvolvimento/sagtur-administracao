<?php defined('BASEPATH') or exit('No direct script access allowed');

class Availability extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function generate_availability() {

        $programacao = $this->getProgramacao();

        $start_time = $programacao->dataSaida.' '.$programacao->horaSaida;
        $end_time   = $programacao->dataRetorno.' '.$programacao->horaRetorno;

        $datetimes = $this->generate_datetime($start_time, $end_time);

        $this->print_datatime($datetimes);

        foreach ($datetimes as $datetime) {
            $date = explode(' ', $datetime)[0];
            $time = explode(' ', $datetime)[1];

            $item = array(
                'produto' => $programacao->produto,
                'agenda_viagem_id' => $programacao->id,
                'data' => $date,
                'hora' => $time,
            );
            $this->db->insert('agenda_disponibilidade', $item);
        }

    }

    public function print_datatime($datetimes) {
        // Imprimir as datas e horas geradas
        foreach ($datetimes as $datetime) {
            $date = explode(' ', $datetime)[0];
            $time = explode(' ', $datetime)[1];
            echo "Data: " . $date . ", Hora: " . $time . "<br>";
        }
    }


    function generate_datetime($start_time, $end_time) {
        $datetimes = array();
        $current_time = strtotime($start_time);
        $end_time = strtotime($end_time);

        while ($current_time <= $end_time) {
            $datetimes[] = date('Y-m-d H:i:s', $current_time);
            $current_time = strtotime('+1 hour', $current_time);
        }

        return $datetimes;
    }


    public function getProgramacao()
    {
        $q = $this->db->get_where('agenda_viagem', array('id' => 219), 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

}