<?php defined('BASEPATH') OR exit('No direct script access allowed');

class  Cartaocredito extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->init();

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');

        $this->load->model('service/CartaocreditoService_model', 'CartaocreditoService_model');
        $this->load->model('service/SaleEventService_model', 'SaleEventService_model');

        $this->load->model('sales_model');
        $this->load->model('financeiro_model');
        $this->load->model('products_model');
    }

    public function index() {show_404();}

    public function view($code) {

        $cobrancaFatura = $this->getCobrancaFaturaByCode($code);

        $fatura = $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);
        $venda = $this->sales_model->getInvoiceByID($fatura->sale_id);

        if (!empty($venda)) {
            $this->data['venda'] =  $venda;
            $this->data['vendedor'] =  $this->site->getCompanyByID($venda->biller_id);
        }

        $this->data['fatura'] =  $fatura;
        $this->data['cobrancaFatura'] = $cobrancaFatura;
        $this->data['cliente'] = $this->site->getCompanyByID($fatura->pessoa);

        if ($this->faturaAberta($cobrancaFatura)) {
            if ($cobrancaFatura->integracao == 'valepay') {
                $this->data['configValepay'] = $this->settings_model->getValepaySettings();
                $this->load->view($this->theme . 'cartao_credito/pagamento_valepay', $this->data);
            } else if ($cobrancaFatura->integracao == 'mercadopago') {
                $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();
                $this->load->view($this->theme . 'cartao_credito/pagamento', $this->data);
            }
        } else if ($this->faturaPendente($cobrancaFatura)) {
            $this->load->view($this->theme . 'cartao_credito/pendente', $this->data);
        } else if ($this->faturaQuitada($cobrancaFatura) ) {

            if ($venda && $this->isEmailAindaNaoEnviado($venda->id)) $this->enviar_email($venda);

            $this->data['payments'] =  $this->sales_model->getPaymentsInvoice($fatura->id);;

            $this->load->view($this->theme . 'cartao_credito/confirmacao', $this->data);
        } else if ($this->faturaCancelada($cobrancaFatura)) {
            redirect(base_url() . "cartaocredito/error/" . $code. '?token=' . $this->session->userdata('cnpjempresa'));
        } else {
            show_404();
        }
    }

    function error($code) {

        $cobrancaFatura = $this->getCobrancaFaturaByCode($code);

        $fatura = $this->Financeiro_model->getFaturaById($cobrancaFatura->fatura);
        $venda = $this->sales_model->getInvoiceByID($fatura->sale_id);

        if (!empty($venda)) {
            $this->data['venda'] =  $venda;
            $this->data['vendedor'] =  $this->site->getCompanyByID($venda->biller_id);
        }

        $this->data['fatura'] =  $fatura;
        $this->data['cobrancaFatura'] = $cobrancaFatura;
        $this->data['cliente'] = $this->site->getCompanyByID($fatura->pessoa);

        $this->load->view($this->theme . 'cartao_credito/error', $this->data);
    }


    function processar_pagamento($code) {
        try {

            $cobrancaFaturaAtualizada = $this->CartaocreditoService_model->processar_pagamento($code);

            redirect(base_url() . "cartaocredito/pagamento/" . $cobrancaFaturaAtualizada->code . '?token=' . $this->session->userdata('cnpjempresa'));
        } catch(Exception $exception) {
            redirect(base_url() . "cartaocredito/pagamento/" . $code . '?token=' . $this->session->userdata('cnpjempresa').'&error='.$exception->getMessage());
        }
    }

    function pagamento($code) {

        $cobrancaFatura = $this->getCobrancaFaturaByCode($code);//TODO CONSULTA SE JA FOI NOTIFICADO O PAGAMENTO VIA INTEGRACAO

        if ($this->fatura_aberta($cobrancaFatura)) {

            sleep(5);//TODO ESPERA 5 SEGUNDO PARA UMA NOVA PESQUISA DE BAIXA

            $cobrancaFatura = $this->getCobrancaFaturaByCode($code);// DEPOIS DE 5 SEGUNDOS FAZ NOVA VERIFICACAO DE PAGMENTO ANTES DE FORCAR A BAIXA VIA CONSUTLA

            if ($cobrancaFatura->integracao == 'pagseguro') {
                $this->atualizaPagamento($cobrancaFatura->integracao, $cobrancaFatura->fatura);
                $cobrancaFaturaAtualizada = $cobrancaFatura;
            } else if ($cobrancaFatura->integracao == 'valepay') {
                $cobrancaFaturaAtualizada = $this->atualizaPagamento($cobrancaFatura->integracao, $cobrancaFatura->code);
            } else if ($cobrancaFatura->integracao == 'mercadopago') {
                $cobrancaFaturaAtualizada = $this->atualizaPagamento($cobrancaFatura->integracao, $cobrancaFatura->code);
            } else {
                $cobrancaFaturaAtualizada = $this->atualizaPagamento($cobrancaFatura->integracao, $cobrancaFatura->code);
            }
        }

        $this->view($cobrancaFaturaAtualizada->code);
    }

    private function fatura_aberta($cobrancaFatura)
    {
        return $cobrancaFatura->status !== 'QUITADA' && $cobrancaFatura->status !== 'CANCELADA';
    }

    function atualizaPagamento($integracao, $code) {
        return $this->CartaocreditoService_model->consultaPagamento($integracao, $code);
    }

    function enviar_email($venda)
    {
        $this->load->library('parser');

        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $customer = $this->site->getCompanyByID($venda->customer_id);

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
        } else {
            $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
        }

        $parse_data = array(
            'reference_number' => $venda->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->name,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo2 . '" alt="' . $this->Settings->site_name . '"/>',
        );

        $message = $this->parser->parse_string($sale_temp, $parse_data, true);

        $subject = 'RESERVA ' . $this->Settings->site_name . ' CLIENTE ' . $customer->name;
        $to = $customer->email;
        $cc = $vendedor->email;

        $attachment = $this->pdf($venda->id, null, 'S');

        if ($this->Settings->is_email_queue) {
            $this->sma->send_email_queue($this->db, 'sales', 'create', $venda->id, $to, $subject, $message, null, null, $attachment, $cc, '');
        } else {
            $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, '');
            $this->SaleEventService_model->send_mail($venda->id);
            delete_files($attachment);
        }
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->site->getInvoiceByID($id);

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($id);
        $return = $this->sales_model->getReturnBySID($id);

        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->site->getPaymentsForSale($id);
        $this->data['biller']   = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user']     = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento'] = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['inv'] = $inv;
        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->site->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->site->getAllReturnItems($return->id) : null;

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";

        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {

            if ($inv->sale_status == 'orcamento') {
                $showWatermarkImage = base_url('assets/images/orcamento2.png');
                /*
                $this->sma->generate_pdf_showWatermarkImage(
                    $html,
                    $name,
                    false,
                    $this->data['biller']->invoice_footer,
                    $margin_bottom = null,
                    $header = null,
                    $margin_top = null,
                    $orientation = 'P',
                    $showWatermarkImage);*/
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            } else {
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    private function getCobrancaFaturaByCode($code) {
        $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaCode($code);

        if (empty($cobrancaFatura)) {
            $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($code);
            if (empty($cobrancaFatura)) show_404();
        }

        return $cobrancaFatura;
    }

    private function faturaAberta($cobrancaFatura) {
        return $cobrancaFatura->status == 'ABERTA';
    }

    private function faturaCancelada($cobrancaFatura) {
        return $cobrancaFatura->status == 'CANCELADA';
    }

    private function faturaQuitada($cobrancaFatura) {
        return $cobrancaFatura->status == 'QUITADA';
    }

    private function faturaPendente($cobrancaFatura) {
        return $cobrancaFatura->status == 'PENDENTE';
    }

    private function isEmailEnviado($sale_id) {
        return $this->SaleEventService_model->isEmailEnviado($sale_id);
    }

    private function isEmailAindaNaoEnviado($sale_id) {
        return !$this->isEmailEnviado($sale_id);
    }

    private function init() {

        if ($this->input->get('token') != ''){
            $this->session->set_userdata(array('cnpjempresa' => $this->input->get('token')));
        } else {
            if ($this->session->userdata('cnpjempresa') == '') {
                $this->session->set_userdata(array('cnpjempresa' => $this->uri->segment(2)));
            }
        }

        $this->Settings = $this->site->get_setting();
        $this->data['Settings'] = $this->Settings;

        if ($this->Settings->status == 0) {
            redirect('login');
        }

        $sd = $this->site->getDateFormat($this->Settings->dateformat);

        $dateFormats = array(
            'js_sdate' => $sd->js,
            'php_sdate' => $sd->php,
            'mysq_sdate' => $sd->sql,
            'js_ldate' => $sd->js . ' hh:ii',
            'php_ldate' => $sd->php . ' H:i',
            'mysql_ldate' => $sd->sql . ' %H:%i'
        );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m']= $this->m;
        $this->data['v'] = $this->v;
    }
}