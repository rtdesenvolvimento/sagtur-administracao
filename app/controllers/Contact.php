<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contact extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');

        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');

        //repository
        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');

        if ($this->Settings->web_page_caching) {
            $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
        }
    }

    public function index() {
        $this->contato($this->Settings->default_biller);
    }

    function contato($vendedorId = null) {
        try {

            if ($vendedorId == null) $vendedorId = $this->Settings->default_biller;

            $vendedor = $this->site->getCompanyByID($vendedorId);
            $user = $this->site->getUserByBillerActive($vendedorId);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $mes = $this->input->get('mes') == null ? ($this->Settings->filtrar_itens_loja == true ? date('m') : null) : ( $this->input->get('mes') != 'all' ? $this->input->get('mes') : null);
            $produto = $this->input->get('destino') != 'all' ? $this->input->get('destino') : null;
            $category =  $this->input->get('category');
            $embarque =  $this->input->get('embarque');

            $filter->produto = $produto;
            $filter->mes = $mes;
            $filter->category = $category;
            $filter->embarque   = $embarque;

            $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

            $this->data['programacoes'] =  $programacoes;

            if ($filter->produto) {
                $filter = new ProgramacaoFilter_DTO_model();
                $filter->mes = $mes;
                $this->data['programacoesFilter'] = $this->AgendaViagemService_model->getAllProgramacao($filter);
            } else {
                $this->data['programacoesFilter'] = $programacoes;
            }

            $this->data['vendedor']         = $vendedor;
            $this->data['destinoFilter']    = $produto;
            $this->data['mesFilter']        = $mes;
            $this->data['categoryFilter']   = $category;
            $this->data['embarqueFilter']   = $embarque;
            $this->data['testimonials']     = $this->site->getAllTestimonial();
            $this->data['embarques']        = $this->site->getLocaisEmbarque();
            $this->data['menusPai']         = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']        = $this->MenuRepository_model->isPhotosGallery();
            $this->data['categories']       = $this->site->getAllCategories();

            $this->load->view($this->theme . 'website/contato', $this->data);

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar contato', $exception->getMessage());
        }
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }
}?>