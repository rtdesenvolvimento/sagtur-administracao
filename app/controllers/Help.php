<?php defined('BASEPATH') or exit('No direct script access allowed');

class Help extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }
    }

    function index() {

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('help')));
        $meta = array('page_title' => lang('help'), 'bc' => $bc);
        $this->page_construct('help/index2', $meta, $this->data);
    }


}?>