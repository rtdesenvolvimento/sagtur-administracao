<?php defined('BASEPATH') or exit('No direct script access allowed');

class Itinerario extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('sales', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->model('sales_model');

        $this->load->model('service/ItinerarioService_model', 'ItinerarioService_model');
        $this->load->model('model/Itinerario_model', 'Itinerario_model');
        $this->load->model('model/Itinerarioitem_model', 'Itinerarioitem_model');

        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '16384';

        $this->data['logo'] = true;
    }

    public function index()
    {

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['programacoes'] = $this->sales_model->getProgramacaoAll();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('itinerario/index', $meta, $this->data);
    }

    public function getSalesReceptivo()
    {

        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('sales/pdf/$1', '<i class="fa fa-download"></i> ' . lang('download_voucher'));
        $historico_parcela = '<a href="'.base_url().'financeiro/historicoByVenda/$1" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i> Ver Parcelas / Adicionar Pagamento</a>';

        $delete_link = "<a href='#' class='po' title='<b>" . lang("cancelar_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger vo-delete' href='" . site_url('sales/cancelar/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class='fa fa-trash-o'></i> "
            . lang('cancelar_sale') . "</a>";

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li>' . $contrato_link . '</li>
                    <li>' . $pdf_link . '</li>
                    <li>' . $historico_parcela . '</li>
                    <li>' . $email_link . '</li>
                    <li class="divider"></li>
                    <li>' . $delete_link . '</li>
                </ul>
            </div></div>';

        $payment_status = $this->input->post('situacao');
        $billerFilter = $this->input->post('billerFilter');
        $this->load->library('datatables');

        $this->datatables
            ->select("sale_items.id as id, 
                    sale_items.dataEmbarque as date, 
                    sale_items.horaEmbarque  as hora, 
                    local_embarque.name as local_embarque,
                    sale_items.product_name,
                    sma_sales.total_items qtd_pax,
                    veiculo.name veiculo,
                    meio_divulgacao.name as meio_divulgacao,
                    sale_items.product_name as categoria,
                    companies.idioma as idioma,
                    sale_items.customerClientName as customer,
                    g.name as guia,
                    m.name as motorista")
            ->from('sma_sales');

        $this->datatables->join('sale_items', 'sale_items.sale_id = sma_sales.id');
        $this->datatables->join('companies', 'sale_items.customerClient = companies.id', 'left');
        $this->datatables->join('companies as g', 'sale_items.guia = g.id', 'left');
        $this->datatables->join('companies as m', 'sale_items.motorista = m.id', 'left');
        $this->datatables->join('local_embarque', 'sale_items.localEmbarque = local_embarque.id', 'left');
        $this->datatables->join('veiculo', 'sale_items.veiculo = veiculo.id', 'left');
        $this->datatables->join('meio_divulgacao', 'sma_sales.meioDivulgacao = meio_divulgacao.id', 'left');

        $this->datatables->group_by('sales.id');

        if ($payment_status) {
            $this->datatables->where('payment_status', $payment_status);
        }

        if ($billerFilter) {
            $this->datatables->where('biller_id', $billerFilter);
        }

        $this->datatables->where('pos !=', 1);

        if ($this->Settings->exibirCancelamentosListaVenda == '1')  {
            $this->datatables->where("payment_status in ('due','partial','paid', 'cancel') ");
        } else {
            $this->datatables->where("payment_status in ('due','partial','paid') ");
        }

        if (!$this->Customer &&
            !$this->Supplier &&
            !$this->Owner &&
            !$this->Admin &&
            !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function itinerario_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'export_montar') {

                    $veiculo = $_POST['veiculo'];
                    $motorista = $_POST['motorista'];
                    $guia = $_POST['guia'];

                    $itinerario = new Itinerario_model();

                    $itinerario->guia = $guia;
                    $itinerario->motorista = $motorista;
                    $itinerario->veiculo = $veiculo;

                    foreach ($_POST['val'] as $id) {
                        $item = new Itinerarioitem_model();
                        $item->item = $id;
                        $itinerario->adicionarItem($item);
                    }

                    $this->ItinerarioService_model->gerar($itinerario);

                    $this->session->set_flashdata('sucesso', lang("itinerario_salvo_com_sucesso"));
                    redirect($_SERVER["HTTP_REFERER"]);


                } elseif ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('Saida'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('Retorno'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('product'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('faixa'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('R.G'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('Orgão Emissor'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('CPF'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('data_nascimento'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('Celular'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('Email'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('Cidade'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('Local_Embarque'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('Hospedagem'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('Tipo_Cobranca'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('Condicoes'));
                    $this->excel->getActiveSheet()->SetCellValue('U1', lang('Total Venda'));
                    $this->excel->getActiveSheet()->SetCellValue('V1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('W1', lang('payment_status'));
                    $this->excel->getActiveSheet()->SetCellValue('X1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('Y1', lang('Total por pessoa'));
                    $this->excel->getActiveSheet()->SetCellValue('Z1', lang('Comissao'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->sales_model->getInvoiceByID($id);
                        $itens = $this->sales_model->getAllInvoiceItems($id);

                        foreach ($itens as $item) {

                            $comissao = $this->calcularComissaoParaExcel($item->product_id, $item->subtotal, $sale->biller_id);

                            $customer = $this->site->getCompanyByID($item->customerClient);

                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);

                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->hrld($item->dtSaida. ' '.$item->hrSaida));
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $this->sma->hrld($item->dtRetorno.' '.$item->hrRetorno));

                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $item->produtc_name);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sale->biller_name);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->tipoFaixaEtaria);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->name);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->cf1);
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->cf3);
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->vat_no);

                            if ($customer->data_aniversario) $this->excel->getActiveSheet()->SetCellValue('L' . $row, date('d/m/Y', strtotime($customer->data_aniversario)));
                            else $this->excel->getActiveSheet()->SetCellValue('L' . $row, '');

                            if ($sale->local_saida != null) $localEmbarque = $sale->local_saida;
                            else $localEmbarque = $item->localEmbarque;

                            $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->phone);
                            $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf5);
                            $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->email);
                            $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->city);

                            $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $localEmbarque);
                            $this->excel->getActiveSheet()->SetCellValue('R' . $row, $item->tipoHospedagem);

                            $this->excel->getActiveSheet()->SetCellValue('S' . $row, $sale->tipoCobranca);
                            $this->excel->getActiveSheet()->SetCellValue('T' . $row, $sale->condicaoPagamento);

                            $this->excel->getActiveSheet()->SetCellValue('U' . $row, $sale->grand_total);
                            $this->excel->getActiveSheet()->SetCellValue('V' . $row, $sale->paid);
                            $this->excel->getActiveSheet()->SetCellValue('W' . $row, lang($sale->payment_status));
                            $this->excel->getActiveSheet()->SetCellValue('X' . $row, lang($sale->sale_status));
                            $this->excel->getActiveSheet()->SetCellValue('Y' . $row, $item->subtotal);
                            $this->excel->getActiveSheet()->SetCellValue('Z' . $row, $comissao);

                            $row++;
                        }
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');

                    if ($this->input->post('form_action') == 'export_pdf') {

                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;

                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }
}