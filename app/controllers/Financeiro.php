<?php defined('BASEPATH') or exit('No direct script access allowed');

class Financeiro extends MY_Controller
{
    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('reports', $this->Settings->user_language);
        $this->lang->load('financeiro', $this->Settings->user_language);

        $this->load->library('form_validation');

        $this->load->model('financeiro_model');
        $this->load->model('reports_model');
        $this->load->model('companies_model');
        $this->load->model('settings_model');
        $this->load->model('sales_model');

        $this->load->model('model/ContaReceber_model', 'ContaReceber_model');
        $this->load->model('model/ContaPagar_model', 'ContaPagar_model');

        $this->load->model('dto/parcelaDTO_model');

        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/CobrancaService_model', 'CobrancaService_model');
        $this->load->model('service/ReembolsoService_model', 'ReembolsoService_model');

        $this->load->model('repository/GiftRepository_model', 'GiftRepository_model');

        $this->digital_upload_path  = 'assets/uploads/';
        $this->upload_path          = 'assets/uploads/';
        $this->thumbs_path          = 'assets/uploads/thumbs/';
        $this->image_types          = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types   = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size    = '16384';
    }

    function index(){}

    function extrato_financeiro()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['planoReceitas'] = $this->financeiro_model->getAllReceitasSuperiores();
        $this->data['planoDespesas'] = $this->financeiro_model->getAllDespesasSuperiores();
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['ano'] = date('Y');
        $this->data['mes'] = date('m');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/extrato_financeiro', $meta, $this->data);
    }

    function getExtrato() {
        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_fatura_link = anchor('faturas/editarFatura/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //<li class="divider"></li>
        //<li>'.$edit_fatura_link.'</li>

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                CONCAT({$this->db->dbprefix('products')}.name, ' ' , DATE_FORMAT({$this->db->dbprefix('agenda_viagem')}.dataSaida, '%d/%m/%Y' )) as product_name,
                {$this->db->dbprefix('fatura')}.strReceitas as receita,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.valorpago, 
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code,
                {$this->db->dbprefix('fatura')}.tipooperacao")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('agenda_viagem', 'agenda_viagem.id = fatura.programacaoId', 'left')
            ->join('products', 'products.id = agenda_viagem.produto', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca');

        if ($this->input->post('start_date')) $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$this->input->post('start_date')}' ");
        if ($this->input->post('end_date')) $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$this->input->post('end_date')}' ");
        if ($this->input->post('filterTipoCobranca')) $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        if ($this->input->post('filterReceita')) $this->datatables->where("{$this->db->dbprefix('fatura')}.receita", $this->input->post('filterReceita') );
        if ($this->input->post('filterDespesa')) $this->datatables->where("{$this->db->dbprefix('fatura')}.despesa", $this->input->post('filterDespesa') );
        if ($this->input->post('filterProgramacao')) $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        if ($this->input->post('filterStatus')) {
            if ($this->input->post('filterStatus') == 'VENCIDA') {
                $this->datatables->where("DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) < 0");
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL')");
            } else {
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status", $this->input->post('filterStatus') );
            }
        } else {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL', 'QUITADA')");
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function receitasDURAV()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $tipoCobranca = $this->input->post('filterTipoCobranca');
        $receita = $this->input->post('filterReceita');
        $warehouse = $this->input->post('filterFilial');
        $condicaoPagamento = $this->input->post('filterCondicaoPagamento');
        $cliente = $this->input->post('filterCliente');
        $status = $this->input->post('filterStatus');
        $dataPagamentoDe = $this->input->post('data_pagamento_de');
        $dataPagamentoAte = $this->input->post('data_pagamento_ate');
        $numeroDocumento = $this->input->post('filter_numero_documento');
        $movimentado = $this->input->post('filterMovimentador');
        $programacao = $this->input->post('filterProgramacao');
        $categoria = $this->input->post('filterCategoria');

        $parcelaDTO_model = new ParcelaDTO_model();
        $parcelaDTO_model->setDataVencimentoDe($start_date);
        $parcelaDTO_model->setDataVencimentoAte($end_date);
        $parcelaDTO_model->setTipoCobranca($tipoCobranca);
        $parcelaDTO_model->setFilial($warehouse);
        $parcelaDTO_model->setCondicaoPagamento($condicaoPagamento);
        $parcelaDTO_model->setCliente($cliente);
        $parcelaDTO_model->setStatus($status);
        $parcelaDTO_model->setDataPagamentoDe($dataPagamentoDe);
        $parcelaDTO_model->setDataPagamentoAte($dataPagamentoAte);
        $parcelaDTO_model->setNumeroDocumento($numeroDocumento);
        $parcelaDTO_model->setMovimentador($movimentado);
        $parcelaDTO_model->setProgramacao($programacao);
        $parcelaDTO_model->setCategoria($categoria);

        $parcelaDTO_model->setReceita(16);//TODO CATEGORIA FIXA VIA CONFIGURACAO

        $this->data['planoReceitas'] = $this->financeiro_model->getAllReceitasSuperiores();
        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['user_id'] = 1;
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['receita'] = $receita;
        $this->data['warehouse'] = $warehouse;

        $this->data['programacoes'] =  $this->sales_model->getProgramacaoAll('');
        $this->data['categories'] = $this->site->getAllCategories();

        $this->data['receitas'] = $this->financeiro_model->buscarFaturasDeReceitas($parcelaDTO_model);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/receitasDURAV', $meta, $this->data);
    }

    function comissao()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $tipoCobranca = $this->input->post('filterTipoCobranca');
        $despesa = $this->input->post('filterDespesa');
        $warehouse = $this->input->post('filterFilial');
        $condicaoPagamento = $this->input->post('filterCondicaoPagamento');
        $cliente = $this->input->post('filterCliente');
        $status = $this->input->post('filterStatus');
        $dataPagamentoDe = $this->input->post('data_pagamento_de');
        $dataPagamentoAte = $this->input->post('data_pagamento_ate');
        $numeroDocumento = $this->input->post('filter_numero_documento');
        $movimentado = $this->input->post('filterMovimentador');
        $programacao = $this->input->post('filterProgramacao');
        $categoria = $this->input->post('filterCategoria');
        $vendedor = $this->input->post('filterVendedor');

        $parcelaDTO_model = new ParcelaDTO_model();
        $parcelaDTO_model->setDataVencimentoDe($start_date);
        $parcelaDTO_model->setDataVencimentoAte($end_date);
        $parcelaDTO_model->setTipoCobranca($tipoCobranca);
        $parcelaDTO_model->setFilial($warehouse);
        $parcelaDTO_model->setCondicaoPagamento($condicaoPagamento);
        $parcelaDTO_model->setCliente($cliente);
        $parcelaDTO_model->setStatus($status);
        $parcelaDTO_model->setDataPagamentoDe($dataPagamentoDe);
        $parcelaDTO_model->setDataPagamentoAte($dataPagamentoAte);
        $parcelaDTO_model->setNumeroDocumento($numeroDocumento);
        $parcelaDTO_model->setMovimentador($movimentado);
        $parcelaDTO_model->setProgramacao($programacao);
        $parcelaDTO_model->setCategoria($categoria);
        $parcelaDTO_model->setVendedor($vendedor);

        $this->data['planoDespesas'] = $this->financeiro_model->getAllDespesasSuperiores();
        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['user_id'] = 1;
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['despesa'] = $despesa;
        $this->data['warehouse'] = $warehouse;

        if ($this->Settings->restrict_user && !$this->Owner && !$this->Admin) {
            $parcelaDTO_model->setCliente($this->session->userdata('biller_id'));
        } else {
            $parcelaDTO_model->setDespesa(3);//TODO FIXO COMISSAO VIA CONFIGURACAO
        }

        $this->data['programacoes'] =  $this->sales_model->getProgramacaoAll('');
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['despesas'] = $this->financeiro_model->buscarFaturasDeDespesas($parcelaDTO_model);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('relatorio_comissao')));

        $meta = array('page_title' => lang('relatorio_comissao'), 'bc' => $bc);

        $this->page_construct('financeiro/comissao', $meta, $this->data);
    }

    function pagamentoFornecedor()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $tipoCobranca = $this->input->post('filterTipoCobranca');
        $despesa = $this->input->post('filterDespesa');
        $warehouse = $this->input->post('filterFilial');
        $condicaoPagamento = $this->input->post('filterCondicaoPagamento');
        $cliente = $this->input->post('filterCliente');
        $status = $this->input->post('filterStatus');
        $dataPagamentoDe = $this->input->post('data_pagamento_de');
        $dataPagamentoAte = $this->input->post('data_pagamento_ate');
        $numeroDocumento = $this->input->post('filter_numero_documento');
        $movimentado = $this->input->post('filterMovimentador');
        $programacao = $this->input->post('filterProgramacao');
        $categoria = $this->input->post('filterCategoria');
        $vendedor = $this->input->post('filterVendedor');

        $parcelaDTO_model = new ParcelaDTO_model();
        $parcelaDTO_model->setDataVencimentoDe($start_date);
        $parcelaDTO_model->setDataVencimentoAte($end_date);
        $parcelaDTO_model->setTipoCobranca($tipoCobranca);
        $parcelaDTO_model->setFilial($warehouse);
        $parcelaDTO_model->setCondicaoPagamento($condicaoPagamento);
        $parcelaDTO_model->setCliente($cliente);
        $parcelaDTO_model->setStatus($status);
        $parcelaDTO_model->setDataPagamentoDe($dataPagamentoDe);
        $parcelaDTO_model->setDataPagamentoAte($dataPagamentoAte);
        $parcelaDTO_model->setNumeroDocumento($numeroDocumento);
        $parcelaDTO_model->setMovimentador($movimentado);
        $parcelaDTO_model->setProgramacao($programacao);
        $parcelaDTO_model->setCategoria($categoria);
        $parcelaDTO_model->setVendedor($vendedor);

        $parcelaDTO_model->setDespesa(7);//TODO FIXO COMISSAO VIA CONFIGURACAO

        $this->data['planoDespesas'] = $this->financeiro_model->getAllDespesasSuperiores();
        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['user_id'] = 1;
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['despesa'] = $despesa;
        $this->data['warehouse'] = $warehouse;

        $this->data['programacoes'] =  $this->sales_model->getProgramacaoAll('');
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['despesas'] = $this->financeiro_model->buscarFaturasDeDespesas($parcelaDTO_model);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('relatorio_pagamento_fornecedor')));

        $meta = array('page_title' => lang('relatorio_pagamento_fornecedor'), 'bc' => $bc);

        $this->page_construct('financeiro/pagamentoFornecedor', $meta, $this->data);
    }

    function pagamentoReceptivo()
    {
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $tipoCobranca = $this->input->post('filterTipoCobranca');
        $despesa = $this->input->post('filterDespesa');
        $warehouse = $this->input->post('filterFilial');
        $condicaoPagamento = $this->input->post('filterCondicaoPagamento');
        $cliente = $this->input->post('filterCliente');
        $status = $this->input->post('filterStatus');
        $dataPagamentoDe = $this->input->post('data_pagamento_de');
        $dataPagamentoAte = $this->input->post('data_pagamento_ate');
        $numeroDocumento = $this->input->post('filter_numero_documento');
        $movimentado = $this->input->post('filterMovimentador');
        $programacao = $this->input->post('filterProgramacao');
        $categoria = $this->input->post('filterCategoria');
        $vendedor = $this->input->post('filterVendedor');

        $parcelaDTO_model = new ParcelaDTO_model();
        $parcelaDTO_model->setDataVencimentoDe($start_date);
        $parcelaDTO_model->setDataVencimentoAte($end_date);
        $parcelaDTO_model->setTipoCobranca($tipoCobranca);
        $parcelaDTO_model->setFilial($warehouse);
        $parcelaDTO_model->setCondicaoPagamento($condicaoPagamento);
        $parcelaDTO_model->setCliente($cliente);
        $parcelaDTO_model->setStatus($status);
        $parcelaDTO_model->setDataPagamentoDe($dataPagamentoDe);
        $parcelaDTO_model->setDataPagamentoAte($dataPagamentoAte);
        $parcelaDTO_model->setNumeroDocumento($numeroDocumento);
        $parcelaDTO_model->setMovimentador($movimentado);
        $parcelaDTO_model->setProgramacao($programacao);
        $parcelaDTO_model->setCategoria($categoria);
        $parcelaDTO_model->setVendedor($vendedor);

        $parcelaDTO_model->setDespesa(8);//TODO FIXO COMISSAO VIA CONFIGURACAO

        $this->data['planoDespesas'] = $this->financeiro_model->getAllDespesasSuperiores();
        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['user_id'] = 1;
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['despesa'] = $despesa;
        $this->data['warehouse'] = $warehouse;

        $this->data['programacoes'] =  $this->sales_model->getProgramacaoAll('');
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['despesas'] = $this->financeiro_model->buscarFaturasDeDespesas($parcelaDTO_model);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('pagamento_receptivo')));

        $meta = array('page_title' => lang('relatorio_pagamento_fornecedor'), 'bc' => $bc);

        $this->page_construct('financeiro/pagamentoFornecedor', $meta, $this->data);
    }


    function saldo()
    {

        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');

        if ($start_date == NULL) $start_date = date('Y-m-d');
        if ($end_date == NULL) $end_date = date('Y-m-d');

        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;

        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);

        $this->page_construct('financeiro/saldo', $meta, $this->data);
    }

    function dre()
    {
        $user_id = 1;

        $this->data['sales'] = $this->reports_model->getSalesTotals($user_id);
        $this->data['total_sales'] = $this->reports_model->getCustomerSales($user_id);
        $this->data['total_quotes'] = $this->reports_model->getCustomerQuotes($user_id);
        $this->data['total_returns'] = $this->reports_model->getCustomerReturns($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['lancamentos'] = $this->financeiro_model->buscarFinanceiroContaReceber();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/dre', $meta, $this->data);
    }

    function extrato($mesAtual = NULL, $anoAtual = NULL, $movimentador = NULL)
    {

        if ($anoAtual == null) $anoAtual = date('Y');
        if ($mesAtual == null) $mesAtual = date('m');

        $this->data['mesAtual'] = $mesAtual;
        $this->data['anoAtual'] = $anoAtual;
        $this->data['movimentador'] = $movimentador;

        $this->data['movimentadores'] = $this->settings_model->getAllContas();
        $this->data['extratos'] = $this->financeiro_model->buscarExtrato($mesAtual, $anoAtual, $movimentador);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/extrato', $meta, $this->data);
    }

    function fluxocaixaano()
    {
        $user_id = 1;

        $this->data['sales'] = $this->reports_model->getSalesTotals($user_id);
        $this->data['total_sales'] = $this->reports_model->getCustomerSales($user_id);
        $this->data['total_quotes'] = $this->reports_model->getCustomerQuotes($user_id);
        $this->data['total_returns'] = $this->reports_model->getCustomerReturns($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $dataInicioMovimentoSaldoPeriodo = date('Y-m-d');
        $dataFinalMovimentaSaldoPeriodo = date('Y-m-d');

        $this->data['saldosEmConta'] = $this->financeiro_model->getMovimentadorSaldoPeriodo(1, $dataInicioMovimentoSaldoPeriodo, $dataFinalMovimentaSaldoPeriodo);
        //$this->data['lancamentos'] = $this->financeiro_model->buscarFaturasDeReceitas();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/fluxocaixaano', $meta, $this->data);
    }

    function fluxocaixadiario()
    {

        $user_id = 1;

        /*
        $this->data['sales'] = $this->reports_model->getSalesTotals($user_id);
        $this->data['total_sales'] = $this->reports_model->getCustomerSales($user_id);
        $this->data['total_quotes'] = $this->reports_model->getCustomerQuotes($user_id);
        $this->data['total_returns'] = $this->reports_model->getCustomerReturns($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $dataInicioMovimentoSaldoPeriodo = date('Y-m-d');
        $dataFinalMovimentaSaldoPeriodo = date('Y-m-d');

        $this->data['saldosEmConta'] = $this->financeiro_model->getMovimentadorSaldoPeriodo($dataInicioMovimentoSaldoPeriodo, $dataFinalMovimentaSaldoPeriodo);
        $this->data['lancamentos'] = $this->financeiro_model->buscarFaturasDeReceitas();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        */
        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('gerenciamento_financeiro')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('financeiro/fluxocaixadiario', $meta, $this->data);
    }

    public function adicionarContaReceber()
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('valor', lang("valor"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('receita', lang("receita"), 'required');
        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->site->getReference('ex');
            $vencimentos = $this->input->post('dtVencimentoParcela');
            $tiposCobranca = $this->input->post('tipocobrancaParcela');
            $valorVencimento = $this->input->post('valorVencimentoParcela');
            $movimentadores = $this->input->post('movimentadorParcela');
            $descontoParcela = $this->input->post('descontoParcela');
            $senderHash = $this->input->post('senderHash', true);

            $data = array(
                'status' => ContaReceber_model::STATUS_ABARTA,
                'reference' => $reference,
                'created_by' => $this->session->userdata('user_id'),
                'pessoa' => $this->input->post('pessoa', true),
                'tipocobranca' => $this->input->post('tipocobranca', true),
                'receita' => $this->input->post('receita', true),
                'condicaopagamento' => $this->input->post('condicaopagamento', true),
                'dtvencimento' => $this->input->post('dtvencimento', true),
                'dtcompetencia' => $this->input->post('dtvencimento', true),
                'programacaoId' => $this->input->post('programacaoId', true),
                'valor' => $this->input->post('valor'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
                'palavra_chave' => $this->input->post('palavra_chave', true),
                'numero_documento' => $this->input->post('numero_documento', true)
            );

            $data['attachment'] = $this->uploadArquivo();
            //$this->sma->print_arrays($data);

            $contaReceber = new ContaReceber_model();
            $contaReceber->data = $data;
            $contaReceber->valores = $valorVencimento;
            $contaReceber->vencimentos = $vencimentos;
            $contaReceber->cobrancas = $tiposCobranca;
            $contaReceber->movimentadores = $movimentadores;
            $contaReceber->descontos = $descontoParcela;
            $contaReceber->senderHash = $senderHash;

        } elseif ($this->input->post('adicionarContaReceber')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run()) {
            try {
                $this->financeiro_model->adicionarReceita($contaReceber);
                $this->session->set_flashdata('message', lang("financeiro_adicionado"));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['receitas'] = $this->financeiro_model->getAllReceitasSuperiores();
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobrancaDebito();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();
            $this->data['programacoes'] = $this->sales_model->getProgramacao('','');

            $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();
            $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();

            $this->load->view($this->theme . 'financeiro/adicionarContaReceber', $this->data);
        }
    }

    private function uploadArquivo()
    {
        if ($_FILES['userfile']['size'] > 0) {

            $this->load->library('upload');

            $config['upload_path'] = $this->upload_path;
            $config['allowed_types'] = $this->digital_file_types;
            $config['max_size'] = $this->allowed_file_size;
            $config['overwrite'] = false;
            $config['encrypt_name'] = true;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload()) {
                $error = $this->upload->display_errors();
                $this->session->set_flashdata('error', $error);
                redirect($_SERVER["HTTP_REFERER"]);
            }

            return $this->upload->file_name;
        }

        return '';
    }


    public function reembolso($faturaId)
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('valorpago', lang("valorpago"), 'required');
        $this->form_validation->set_rules('valorReembolso', lang("valor_reembolso"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->site->getReference('ex');
            $vencimentos = $this->input->post('dtVencimentoParcela');
            $tiposCobranca = $this->input->post('tipocobrancaParcela');
            $valorVencimento = $this->input->post('valorVencimentoParcela');
            $movimentadores = $this->input->post('movimentadorParcela');
            $descontoParcela = $this->input->post('descontoParcela');

            $data = array(
                'reference'             => $reference,
                'status'                => Financeiro_model::STATUS_ABERTA,
                'pessoa'                => $this->input->post('pessoa', true),
                'tipocobranca'          => $this->input->post('tipocobranca', true),
                'condicaopagamento'     => $this->input->post('condicaopagamento', true),
                'dtvencimento'          => $this->input->post('dtvencimento', true),
                'dtcompetencia'         => $this->input->post('dtvencimento', true),
                'valor'                 => $this->input->post('valorReembolso', true),
                'desconto'              => 0,
                'acrescimo'             => 0,
                'warehouse'             => 1,//TODO PASSAR A FILIAL PARA A FATURA
                'created_by'            => $this->session->userdata('user_id'),
            );

            $contaReceber = new ContaReceber_model();
            $contaReceber->data = $data;
            $contaReceber->valores = $valorVencimento;
            $contaReceber->vencimentos = $vencimentos;
            $contaReceber->cobrancas = $tiposCobranca;
            $contaReceber->movimentadores = $movimentadores;
            $contaReceber->descontos = $descontoParcela;

        } elseif ($this->input->post('editarFatura')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        } if ($this->form_validation->run()) {
            try {
                $this->FinanceiroService_model->reembolso($contaReceber, $this->input->post('faturaId', true), $this->input->post('note', true));

                $this->session->set_flashdata('message', lang('reembolso_salvo_com_sucesso'));
            } catch (Exception $erro) {
                $this->session->set_flashdata('error', $erro->getMessage());
            }
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] =  (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($this->data['error'] != '') {
                $this->session->set_flashdata('error', (validation_errors() ? validation_errors() : $this->session->flashdata('error')));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $this->data['modal_js'] = $this->site->modal_js();
            $fatura = $this->financeiro_model->getFaturaById($faturaId);

            $this->data['fatura'] = $fatura;
            $this->data['valorReembolso'] = $this->ReembolsoService_model->getValorReembolso($fatura);
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

            $this->load->view($this->theme . 'financeiro/reembolso', $this->data);
        }
    }

    public function negociarContaReceber($pessoaId)
    {

        $this->load->helper('security');

        $this->form_validation->set_rules('valor', lang("valor"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('receita', lang("receita"), 'required');
        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            $vencimentos = $this->input->post('dtVencimentoParcela');
            $tiposCobranca = $this->input->post('tipocobrancaParcela');
            $valorVencimento = $this->input->post('valorVencimentoParcela');
            $descontoParcela = $this->input->post('descontoParcela');
            $parcelasReparcelar = $this->input->post('parcelas[]');

            $data = array(
                'status' => Financeiro_model::STATUS_ABERTA,
                'created_by' => $this->session->userdata('user_id'),
                'pessoa' => $this->input->post('pessoa', true),
                'tipocobranca' => $this->input->post('tipocobranca', true),
                'receita' => $this->input->post('receita', true),
                'condicaopagamento' => $this->input->post('condicaopagamento', true),
                'dtvencimento' => $this->input->post('dtvencimento', true),
                'dtcompetencia' => $this->input->post('dtvencimento', true),
                'valor' => $this->input->post('valor'),
                'desconto' => $this->input->post('desconto'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
                'palavra_chave' => $this->input->post('palavra_chave', true),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            //$this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->financeiro_model->adicionarReceita($data, $valorVencimento, $vencimentos, $tiposCobranca, $descontoParcela, $parcelasReparcelar)) {
            $this->session->set_flashdata('message', lang("financeiro_renegociado_com_sucesso"));
            redirect('financeiro');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['receitas'] = $this->financeiro_model->getAllReceitasSuperiores();
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

            $this->data['pessoa'] = $customer = $this->site->getCompanyByID($pessoaId);
            $this->data['parcelas'] = $this->financeiro_model->buscarParcelasRenegociacaoDividasByPessoa($pessoaId);

            $this->load->view($this->theme . 'financeiro/negociarContaReceber', $this->data);
        }
    }

    public function negociarContaReceberContrato($faturaId, $pessoaId)
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('valor', lang("valor"), 'required');
        $this->form_validation->set_rules('pessoa', lang("pessoa"), 'required');
        $this->form_validation->set_rules('tipocobranca', lang("tipocobranca"), 'required');
        $this->form_validation->set_rules('receita', lang("receita"), 'required');
        $this->form_validation->set_rules('dtvencimento', lang("dtvencimento"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            $vencimentos = $this->input->post('dtVencimentoParcela');
            $tiposCobranca = $this->input->post('tipocobrancaParcela');
            $valorVencimento = $this->input->post('valorVencimentoParcela');
            $parcelasReparcelar = $this->input->post('parcelas[]');
            $descontoParcela = $this->input->post('descontoParcela');

            $data = array(
                'status' => Financeiro_model::STATUS_ABERTA,
                'created_by' => $this->session->userdata('user_id'),
                'pessoa' => $this->input->post('pessoa', true),
                'tipocobranca' => $this->input->post('tipocobranca', true),
                'receita' => $this->input->post('receita', true),
                'condicaopagamento' => $this->input->post('condicaopagamento', true),
                'dtvencimento' => $this->input->post('dtvencimento', true),
                'dtcompetencia' => $this->input->post('dtvencimento', true),
                'valor' => $this->input->post('valor'),
                'desconto' => $this->input->post('desconto'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
                'palavra_chave' => $this->input->post('palavra_chave', true),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            //$this->sma->print_arrays($data);
        }

        if ($this->form_validation->run() == true && $this->financeiro_model->adicionarReceita($data, $valorVencimento, $vencimentos, $tiposCobranca, $descontoParcela, $parcelasReparcelar)) {
            $this->session->set_flashdata('message', lang("financeiro_adicionado"));
            redirect('financeiro');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['receitas'] = $this->financeiro_model->getAllReceitasSuperiores();
            $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

            $this->data['faturaId'] = $faturaId;
            $this->data['pessoa'] = $customer = $this->site->getCompanyByID($pessoaId);
            $this->data['parcelas'] = $this->financeiro_model->buscarParcelasRenegociacaoDividasByFatura($faturaId);

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/negociarContaReceberContrato', $this->data);
        }
    }

    public function historicoByVenda($vendaId) {
        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);

        foreach ($faturas as $faturaObj) {
            $fatura = $faturaObj;
        }

        $this->historico($fatura->id);
    }

    public function historico($faturaId)
    {
        $this->load->helper('security');

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['receitas'] = $this->financeiro_model->getAllReceitasSuperiores();
        $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobranca();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

        $this->data['fatura'] = $this->financeiro_model->getFaturaById($faturaId);
        $this->data['parcelas'] = $this->financeiro_model->buscarHistoricoParcelasByFatura($faturaId);

        $this->load->view($this->theme . 'financeiro/historicoParcelas', $this->data);
    }

    public function estornar($pagamentoId)
    {
        try {
            $this->FinanceiroService_model->estornar($pagamentoId, $this->input->post('note', true));

            $this->session->set_flashdata('message', lang("pagamento_estornado_com_sucesso"));
        } catch (Exception $erro) {
            $this->session->set_flashdata('error', $erro->getMessage());
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function cancelar($faturaId)
    {
        try {
            $this->FinanceiroService_model->cancelar($faturaId, $this->input->post('note', true));
            $this->session->set_flashdata('message', lang("conta_cancelada_com_sucesso"));
        } catch (Exception $erro) {
            $this->session->set_flashdata('error', $erro->getMessage());
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function cancelarCobranca($code)
    {
        echo $this->CobrancaService_model->cancelarCobranca($code);
    }

    public function editarPagamento($id = null)
    {
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        $payment = $this->sales_model->getPaymentByID($id);

        if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') {
            $this->session->set_flashdata('error', lang('x_edit_payment'));
            $this->sma->md();
        }

        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            // if ($this->input->post('paid_by') == 'deposit') {
            //   $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            //   $customer_id = $sale->customer_id;
            //  $amount = $this->input->post('amount-paid')-$payment->amount;
            //  if ( ! $this->site->check_customer_deposit($customer_id, $amount)) {
            //    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            //    redirect($_SERVER["HTTP_REFERER"]);
            // }
            // } else {
            $customer_id = null;
            //}
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updatePayment($id, $payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;
            $this->data['movimentadores'] = $this->settings_model->getAllContas();
            $this->data['formaspagamento'] = $this->settings_model->getAllFormasPagamento();
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'financeiro/editarPagamento', $this->data);
        }
    }

    public function motivoCancelamento($id = null)
    {
        $this->data['id'] = $id;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'financeiro/motivoCancelamento', $this->data);
    }

    public function motivoEstorno($id = null)
    {
        $this->data['id'] = $id;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load->view($this->theme . 'financeiro/motivoEstorno', $this->data);
    }


    public function adicionarTransferencia()
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('valorTranferencia', lang("valorTranferencia"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('contaOrigem', lang("conta_origem"), 'required');
        $this->form_validation->set_rules('contaDestino', lang("conta_destino"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('date')) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = date('Y-m-d H:i:s');

            $reference = $this->site->getReference('ex');
            $contaOrigem = $this->input->post('contaOrigem', true);
            $contaDestino = $this->input->post('contaDestino', true);

            $data = array(
                'reference' => $reference,
                'status' => Financeiro_model::STATUS_ABERTA,
                'created_by' => $this->session->userdata('user_id'),
                'pessoa' => $this->input->post('pessoa', true),
                'dtvencimento' => $date,
                'dtcompetencia' => $date,
                'valor' => $this->input->post('valorTranferencia'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $data['attachment'] = $photo;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data);

        } elseif ($this->input->post('adicionarTransferencia')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->financeiro_model->adicionarTransferencia($data, $contaOrigem, $contaDestino)) {
            $this->session->set_flashdata('message', lang("transferencia_adicionada_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));;
            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/adicionarTransferencia', $this->data);
        }
    }

    public function adicionarSaque()
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('valorSaque', lang("valorSaque"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('contaOrigem', lang("conta_origem"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('date')) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = date('Y-m-d H:i:s');

            $contaOrigem = $this->input->post('contaOrigem', true);
            $reference_no = $this->input->post('reference_no');

            $data = array(
                'status' => Financeiro_model::STATUS_ABERTA,
                'dtvencimento' => $date,
                'dtcompetencia' => $date,
                'despesa' => $this->input->post('despesa'),
                'valor' => $this->input->post('valorSaque'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $data['attachment'] = $photo;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data);

        } elseif ($this->input->post('adicionarSaque')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->financeiro_model->adicionarSaque($data, $contaOrigem, $reference_no, Sales_model::TIPO_DE_PAGAMENTO_SAQUE)) {
            $this->session->set_flashdata('message', lang("saque_adicionado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));;
            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();
            $this->data['despesas'] = $this->financeiro_model->getAllDespesasSuperiores();
            $this->data['payment_ref'] = $this->site->getReference('pay');

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/adicionarSaque', $this->data);
        }
    }

    public function adicionarDeposito()
    {
        $this->load->helper('security');

        $this->form_validation->set_rules('valorDeposito', lang("valorDeposito"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('contaOrigem', lang("conta_origem"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('date')) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = date('Y-m-d H:i:s');

            $reference = $this->site->getReference('ex');
            $contaOrigem = $this->input->post('contaOrigem', true);

            $data = array(
                'status' => Financeiro_model::STATUS_ABERTA,
                'dtvencimento' => $date,
                'dtcompetencia' => $date,
                'valor' => $this->input->post('valorDeposito'),
                'warehouse' => $this->input->post('warehouse', true),
                'note' => $this->input->post('note', true),
            );

            if ($_FILES['userfile']['size'] > 0) {

                $this->load->library('upload');

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;

                $data['attachment'] = $photo;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data);

        } elseif ($this->input->post('adicionarDeposito')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->financeiro_model->adicionarDeposito($data, $contaOrigem, Sales_model::TIPO_DE_PAGAMENTO_DEPOSITO)) {
            $this->session->set_flashdata('message', lang("saque_adicionado_com_sucesso"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));;
            if ($this->data['error'] != '') redirect($_SERVER["HTTP_REFERER"]);

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['movimentadores'] = $this->financeiro_model->getAllContas();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/adicionarDeposito', $this->data);
        }
    }

    public function pagamentos($faturaId = null)
    {
        $this->data['payments'] = $this->financeiro_model->getPagamentosByFatura($faturaId);

        $this->load->view($this->theme . 'financeiro/pagamentos', $this->data);
    }

    public function consultarPagamento()
    {
        try {
            echo $this->FinanceiroService_model->baixarPagamentoIntegracao(date('Y-m-d'));
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    public function consultaSaldo()
    {
        echo $this->FinanceiroService_model->consultaSaldo();
    }

    function contas()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('financeiro'),
                'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('contas_link')));
        $meta = array('page_title' => lang('contas_link'), 'bc' => $bc);
        $this->page_construct('financeiro/contas', $meta, $this->data);
    }

    function getContas()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, name, status")
            ->from("pos_register")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('financeiro/editarConta/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_conta") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_conta") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('financeiro/deletarConta/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function adicionarConta()
    {
        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('status', lang("status"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'user_id'       => $this->session->userdata('user_id'),
                'name'          => $this->input->post('name'),
                'status'        => $this->input->post('status'),
                'date'          => date('Y-m-d H:i:s'),
                'cash_in_hand'  => 0,
                'caixa'         => 0,
            );
        } elseif ($this->input->post('adicionarConta')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/contas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarConta($data)) {
            $this->session->set_flashdata('message', lang("conta_adicionada_com_successo"));
            redirect("financeiro/contas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/adicionarConta', $this->data);
        }
    }

    function editarConta($id = NULL)
    {
        $conta = $this->settings_model->getContaById($id);

        $this->form_validation->set_rules('name', lang("name"), 'required|min_length[3]');
        $this->form_validation->set_rules('status', lang("status"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name'          => $this->input->post('name'),
                'status'        => $this->input->post('status'),
            );
        } elseif ($this->input->post('editarConta')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/contas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateConta($id, $data)) {
            $this->session->set_flashdata('message', lang("conta_atualizada_com_sucesso"));
            redirect("financeiro/contas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $conta;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/editarConta', $this->data);
        }
    }

    function deletarConta($id = NULL)
    {
        if ($this->settings_model->verificaIntegridadeConta($id)) {
            $this->session->set_flashdata('error', lang("conta_sendo_utilizada"));
            redirect("financeiro/contas", 'refresh');
        }

        if ($this->settings_model->deletarConta($id)) {
            echo lang("conta_deletada_com_sucesso");
        }
    }

    function plano_despesas()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('system_settings'),
                'page' => lang('system_settings')),
            array('link' => '#', 'page' => lang('despesas')));
        $meta = array('page_title' => lang('categories'), 'bc' => $bc);
        $this->page_construct('financeiro/plano_despesas', $meta, $this->data);
    }

    function adicionarPlanoDespesa()
    {
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'tipo' => $this->input->post('tipo'),
                'despesasuperior' => $this->input->post('despesaSuperior'),
            );
        } elseif ($this->input->post('adicionarPlanoDespesa')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/plano_despesas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarDespesa($data)) {
            $this->session->set_flashdata('message', lang("despesa_adicionada_com_successo"));
            redirect("financeiro/plano_despesas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['despesasSuperior'] = $this->settings_model->getAllDespesasSuperior();
            $this->data['dre'] = $this->settings_model->getAllDREReceitas();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/adicionarPlanoDespesa', $this->data);
        }
    }

    function editarPlanoDespesa($id = NULL)
    {
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $category = $this->settings_model->getDespesaById($id);

        if ($this->input->post('code') != $category->code) {
            $this->form_validation->set_rules('code', lang("category_code"), 'is_unique[expense_categories.code]');
        }

        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'tipo' => $this->input->post('tipo'),
                'despesasuperior' => $this->input->post('despesaSuperior'),
                'dre' => $this->input->post('dre'),
            );
        } elseif ($this->input->post('editarDespesa')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/plano_despesas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateDespesa($id, $data)) {
            $this->session->set_flashdata('message', lang("despesa_atualizada_com_sucesso"));
            redirect("financeiro/plano_despesas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $category;
            $this->data['despesasSuperior'] = $this->settings_model->getAllDespesasSuperior();
            $this->data['dre'] = $this->settings_model->getAllDREReceitas();

            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/editarPlanoDespesa', $this->data);
        }
    }

    function getPlanoDespesas()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select("id, code, name")
            ->from("despesa")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('financeiro/editarPlanoDespesa/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_despesa") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_despesa") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('financeiro/deletarPlanoDespesa/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function deletarPlanoDespesa($id = NULL)
    {
        if ($this->settings_model->verificaIntegridadeDespesa($id)) {
            $this->session->set_flashdata('error', lang("despesa_sendo_utilizada"));
            redirect("financeiro/plano_despesas", 'refresh');
        }

        if ($this->settings_model->deletarDespesa($id)) {
            echo lang("despesa_deletada_com_sucesso");
        }
    }

    function plano_receitas()
    {
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(),
            'page' => lang('home')),
            array('link' => site_url('financeiro'),
                'page' => lang('financeiro')),
            array('link' => '#', 'page' => lang('plano_receitas')));
        $meta = array('page_title' => lang('plano_receitas'), 'bc' => $bc);
        $this->page_construct('financeiro/plano_receitas', $meta, $this->data);
    }

    function adicionarPlanoContasReceita()
    {
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $this->form_validation->set_rules('name', lang("name"), 'required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'code' => $this->input->post('code'),
                'tipo' => $this->input->post('tipo'),
                'receitasuperior' => $this->input->post('receitasuperior'),
                'dre' => $this->input->post('dre'),
            );
        } elseif ($this->input->post('adicionarReceita')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/plano_receitas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->adicionarReceita($data)) {
            $this->session->set_flashdata('message', lang("receita_adicionada_com_successo"));
            redirect("financeiro/plano_receitas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['receitasSuperior'] = $this->settings_model->getAllReceitasSuperiores();
            $this->data['dre'] = $this->settings_model->getAllDREReceitas();
            $this->load->view($this->theme . 'financeiro/adicionarPlanoContasReceita', $this->data);
        }
    }

    function editarPlanoContasReceita($id = NULL)
    {
        $this->form_validation->set_rules('code', lang("category_code"), 'trim|required');
        $category = $this->settings_model->getReceitaById($id);

        if ($this->input->post('code') != $category->code) {
            $this->form_validation->set_rules('code', lang("category_code"), 'is_unique[expense_categories.code]');
        }

        $this->form_validation->set_rules('name', lang("category_name"), 'required|min_length[3]');

        if ($this->form_validation->run() == true) {
            $data = array(
                'code' => $this->input->post('code'),
                'name' => $this->input->post('name'),
                'tipo' => $this->input->post('tipo'),
                'receitasuperior' => $this->input->post('receitasuperior'),
                'dre' => $this->input->post('dre'),
            );
        } elseif ($this->input->post('editarReceita')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("financeiro/plano_receitas");
        }

        if ($this->form_validation->run() == true && $this->settings_model->updateReceita($id, $data)) {
            $this->session->set_flashdata('message', lang("receita_atualizada_com_sucesso"));
            redirect("financeiro/plano_receitas");
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['category'] = $category;
            $this->data['receitasSuperior'] = $this->settings_model->getAllReceitasSuperiores();
            $this->data['dre'] = $this->settings_model->getAllDREReceitas();
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'financeiro/editarPlanoContasReceita', $this->data);
        }
    }

    function getReceitasPlanoContas()
    {
        $this->load->library('datatables');

        $this->datatables
            ->select("id, code, name")
            ->from("receita")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('financeiro/editarPlanoContasReceita/$1') . "' data-toggle='modal' data-target='#myModal' class='tip' title='" . lang("editar_receita") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("deletar_receita") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('financeiro/deletarPlanoContasReceita/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    function deletarPlanoContasReceita($id = NULL)
    {
        if ($this->settings_model->verificaIntegridadeReceita($id)) {
            $this->session->set_flashdata('error', lang("receita_sendo_utilizada"));
            redirect("financeiro/plano_receitas", 'refresh');
        }

        if ($this->settings_model->deletarReceita($id)) {
            echo lang("receita_deletada_com_sucesso");
        }
    }

    public function getParcelas()
    {
        $condicaoPagamentoId = $this->input->get('condicaoPagamentoId');
        $dtvencimento = $this->input->get('dtvencimento');
        $valorVencimento = $this->input->get('valorVencimento');
        $tipoCobrancaId = $this->input->get('tipoCobrancaId');
        $desconto = $this->input->get('desconto');
        $blockUltimaParcela = $this->input->get('blockUltimaParcela');


        $tiposCobranca      = $this->site->getAllTiposCobranca();
        $condicaoPagamento  = $this->site->getCondicaoPagamentoById($condicaoPagamentoId);
        $tipoCobranca       = $this->site->getTipoCobrancaById($tipoCobrancaId);
        $movimentadores     = $this->site->getAllContas();

        $totalParcelas      = $condicaoPagamento->parcelas;

        if ($desconto != '' && $desconto < $valorVencimento) {
            $valorVencimento = $valorVencimento - $desconto;
        } else {
            $desconto = 0;
        }

        $valorVencimento = number_format($valorVencimento / $totalParcelas, 2, '.', '');

        $html = '';

        if ($dtvencimento == null) {
            $dtvencimento = date('Y-m-d');
        } else {
            $dtvencimento = date('Y-m-d', strtotime($dtvencimento));
        }

        $preservaDia = date('d', strtotime($dtvencimento));

        $tipo       = $tipoCobranca->tipo;
        $integracoa = $tipoCobranca->integracao;
        $conta      = $tipoCobranca->conta;
        $readOnly   = '';

        if ($tipo == 'carne' ||
            $tipo == 'carne_boleto' ||
            $tipo == 'carne_cartao' ||
            $tipo == 'carne_boleto_cartao' &&
            $integracoa != '') {
            $readOnly = 'readonly';
        }

        $readOnlyVencimento = '';

        for ($i = 0; $i < $totalParcelas; $i++) {

            if ($blockUltimaParcela == 'true' && ($i == $totalParcelas - 1) || $totalParcelas == 1) $readOnlyVencimento = 'readonly';

            $html .= '<tr>
                    <td>' . ($i + 1) . '/' . $totalParcelas . '</td>
                    <td><input type="text" ' . $readOnly . ' ' . $readOnlyVencimento . ' name="valorVencimentoParcela[]" onkeyup="atualizarValorVencimentoAoEditarParcela(this, ' . $i . ');" required="required" class="form-control mask_money" vencimento="' . $valorVencimento . '" value="' . $valorVencimento . '"/></td>
                    <td><input type="text" ' . $readOnly . ' name="descontoParcela[]" class="form-control" readonly value="' . $desconto / $totalParcelas . '" /></td>
                    <td><input type="date" ' . $readOnly . ' name="dtVencimentoParcela[]" required="required" class="form-control" value="' . $dtvencimento . '" /></td>
                    <td>
                        <select class="form-control" ' . $readOnly . ' name="tipocobrancaParcela[]" required="required">';

            foreach ($tiposCobranca as $tipoCobranca) {
                if ($tipoCobrancaId == $tipoCobranca->id) {
                    $html .= '      <option selected="selected" value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $tipoCobranca->id . '">' . $tipoCobranca->name . '</option>';
                }
            }

            $html .= ' </select> 
                      </td>';

            $html .= '<td>
                        <select class="form-control" ' . $readOnly . ' name="movimentadorParcela[]" required="required">';

            foreach ($movimentadores as $movimentador) {
                if ($movimentador->id == $conta) {
                    $html .= '      <option selected="selected" value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                } else if ($readOnly == '') {
                    $html .= '      <option value="' . $movimentador->id . '">' . $movimentador->name . '</option>';
                }
            }

            $html .= '    </select>
                    </td>
                </tr>';

            $anoAtual = date('Y', strtotime($dtvencimento));
            $mesAtual = date('m', strtotime($dtvencimento));
            $diaAtual = date('d', strtotime($dtvencimento));

            if ($mesAtual == 1 && $diaAtual > 28) {
                if (date('L', strtotime($anoAtual . '-01-01'))) {
                    $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-02-29'));
                } else {
                    $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-02-28'));
                }
            } else if ($mesAtual == 2) {//todo gera marco
                $dtvencimento = date('Y-m-d', strtotime($anoAtual . '-03-' . $preservaDia));
            } else {
                $dtvencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtvencimento)));
            }

        }
        echo $html;
    }
}
