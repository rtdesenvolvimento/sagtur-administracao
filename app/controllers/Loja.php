<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loja extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('sales', $this->Settings->user_language);

        //dto
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');
        $this->load->model('dto/AnalyticalDTO_model', 'AnalyticalDTO_model');

        //service
        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/AnalyticalService_model', 'AnalyticalService_model');

        //repository
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');
        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/ContactRepository_model', 'ContactRepository_model');
    }

    public function index() {
        $this->open($this->Settings->default_biller);
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

    public function open($vendedorId) {
        try {

            $vendedor   = $this->site->getCompanyByID($vendedorId);
            $user       = $this->site->getUserByBillerActive($vendedorId);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor desativado', 'Entre em contatato com agência para solicitar um link válido.');
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $mes = $this->input->get('mes') == null ? ($this->Settings->filtrar_itens_loja == true ? date('m') : null) : ( $this->input->get('mes') != 'all' ? $this->input->get('mes') : null);
            $produto = $this->input->get('destino') != 'all' ? $this->input->get('destino') : null;
            $category =  $this->input->get('category');
            $embarque =  $this->input->get('embarque');

            $filter->produto    = $produto;
            $filter->mes        = $mes;
            $filter->category   = $category;
            $filter->embarque   = $embarque;
            $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

            if ($this->Settings->pagination_type == 'by_date' ) {
                $filter->limit_by_data = true;
            } elseif ($this->Settings->pagination_type == 'by_category' ) {
                $filter->order_by_custom = 'categories.id, agenda_viagem.dataSaida, agenda_viagem.horaSaida';
                $filter->limit_by_category = true;
            }

            $programacoes   = $this->AgendaViagemService_model->getAllProgramacao($filter);
            $destaques      = $this->AgendaViagemService_model->getAllProgramacaoDestaque($filter);

            if ($filter->produto) {
                $filter = new ProgramacaoFilter_DTO_model();
                $filter->mes = $mes;
                $this->data['programacoesFilter'] = $this->AgendaViagemService_model->getAllProgramacao($filter);
            } else {
                $this->data['programacoesFilter'] = $programacoes;
            }

            $this->data['programacoes']     =  $programacoes;
            $this->data['destaques']        =  $destaques;
            $this->data['vendedor']         = $vendedor;
            $this->data['destinoFilter']    = $produto;
            $this->data['mesFilter']        = $mes;
            $this->data['categoryFilter']   = $category;
            $this->data['embarqueFilter']   = $embarque;
            $this->data['testimonials']     = $this->site->getAllTestimonial();
            $this->data['embarques']        = $this->site->getLocaisEmbarque();
            $this->data['menusPai']         = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']        = $this->MenuRepository_model->isPhotosGallery();
            $this->data['categories']       = $this->site->getAllCategories();
            $this->data['galleries']        = $this->site->getPhotosGallery();

            if ($this->Settings->theme_site == 'theme02') {
                $this->load->view($this->theme . 'website/index', $this->data);
            } else {
                $this->load->model('products_model');
                $this->load->view($this->theme . 'appcompra/loja', $this->data);
            }

            $this->record_store_access($produto);//TODO LOG DE ACESSO

            if ($this->Settings->web_page_caching) {
                $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function categories($vendedorId, $groupID)
    {
        try {

            $vendedor   = $this->site->getCompanyByID($vendedorId);
            $user       = $this->site->getUserByBillerActive($vendedorId);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor desativado', 'Entre em contatato com agência para solicitar um link válido.');
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $filter->category   = $groupID;
            $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

            //custom
            if ($this->Settings->pagination_type == 'by_category' ) {
                $filter->order_by_custom = 'categories.id, agenda_viagem.dataSaida, agenda_viagem.horaSaida';
            }

            $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

            $this->data['programacoesFilter'] = $programacoes;
            $this->data['programacoes']     = $programacoes;
            $this->data['vendedor']         = $vendedor;
            $this->data['menusPai']         = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']        = $this->MenuRepository_model->isPhotosGallery();
            $this->data['categories']       = $this->site->getAllCategories();
            $this->data['galleries']        = $this->site->getPhotosGallery();

            if ($this->Settings->theme_site == 'theme02') {
                $this->load->view($this->theme . 'website/categories', $this->data);

            } else {
                $this->load->model('products_model');
                $this->load->view($this->theme . 'appcompra/loja', $this->data);
            }

            if ($this->Settings->web_page_caching) {
                $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function categories_data($vendedorId, $mes, $ano)
    {
        try {

            $vendedor   = $this->site->getCompanyByID($vendedorId);
            $user       = $this->site->getUserByBillerActive($vendedorId);

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor desativado', 'Entre em contatato com agência para solicitar um link válido.');
            }

            $filter = new ProgramacaoFilter_DTO_model();

            $filter->ano        = $ano;
            $filter->mes        = $mes;
            $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

            //custom
            if ($this->Settings->pagination_type == 'by_category' ) {
                $filter->order_by_custom = 'categories.id, agenda_viagem.dataSaida, agenda_viagem.horaSaida';
            }

            $programacoes = $this->AgendaViagemService_model->getAllProgramacao($filter);

            $this->data['programacoesFilter'] = $programacoes;
            $this->data['programacoes']     = $programacoes;
            $this->data['vendedor']         = $vendedor;
            $this->data['menusPai']         = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']        = $this->MenuRepository_model->isPhotosGallery();
            $this->data['categories']       = $this->site->getAllCategories();

            if ($this->Settings->theme_site == 'theme02') {
                $this->load->view($this->theme . 'website/categories', $this->data);

            } else {
                $this->load->model('products_model');
                $this->load->view($this->theme . 'appcompra/loja', $this->data);
            }

            if ($this->Settings->web_page_caching) {
                $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function load_packages()
    {

        $filter = new ProgramacaoFilter_DTO_model();
        $mes = $this->input->get('mes') == null ? ($this->Settings->filtrar_itens_loja == true ? date('m') : null) : ( $this->input->get('mes') != 'all' ? $this->input->get('mes') : null);
        $produto = $this->input->get('destino') != 'all' ? $this->input->get('destino') : null;
        $category =  $this->input->get('category');
        $embarque =  $this->input->get('embarque');

        $filter->produto    = $produto;
        $filter->mes        = $mes;
        $filter->category   = $category;
        $filter->embarque   = $embarque;

        //custom
        $filter->order_by_custom = 'categories.id DESC, agenda_viagem.dataSaida, agenda_viagem.horaSaida';
        $filter->limit = $this->input->get('limit') == 0 ? 4 : $this->input->get('limit');
        $filter->offset = $this->input->get('offset') == 0 ? 0 : $this->input->get('offset');

        $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

        $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);;

        $this->load->view($this->theme . 'website/load_packages', $this->data);
    }

    private function record_store_access($produto = null) {

        $analytical = new AnalyticalDTO_model();

        if ($produto != null) {
            $analytical->product_id = $produto;
            $analytical->type= AnalyticalDTO_model::PAGE_VIEW_PRODUCT;
        } else {
            $analytical->type= AnalyticalDTO_model::PAGE_VIEW;
        }

        //$this->AnalyticalService_model->registry_access($analytical);//todo vamos desabilitar e deixra apenas para produtos
    }

    public function enviar_email() {

        $name = $this->input->get('name');
        $tel = $this->input->get('tel');
        $email = $this->input->get('email');
        $subject = $this->input->get('subject');
        $message = $this->input->get('message');
        $vendedorId = $this->input->get('vendedor');

        $vendedor = $this->site->getCompanyByID($vendedorId);

        $data_contact = array(
            'name' => $name,
            'telefone' => $tel,
            'email' => $email,
            'subject' => $subject,
            'message' => $message,
            'biller_id' => $vendedorId,
        );

        $this->ContactRepository_model->addContact($data_contact);//todo email site

        $this->sma->send_email($vendedor->email, 'Mensagem Site de '.$name.' '.$subject, $message.'<br/>Tel: '.$tel, null, null, null, $email, '');
    }

    public function enviar_email_pacote() {

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');

        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->get('programacaoId'));
        $product = $this->site->getProductByID($programacao->produto);

        $product_name = str_replace("'", "", $product->name).' Com saída dia '.date("d/m/Y", strtotime($programacao->dataSaida)).' às '.$programacao->horaSaida;

        $name       = $this->input->get('name');
        $tel        = $this->input->get('tel');
        $email      = $this->input->get('email') != null ? $this->input->get('email')  : '';
        $subject    = 'Tenho Interesse: '.$name.' | '.$product_name;
        $message    = $this->input->get('message');
        $vendedorId = $this->input->get('vendedor');
        $vendedor   = $this->site->getCompanyByID($vendedorId);

        $message = '<div style="background: #dee2e6;padding: 25px 15px 15px 15px;">TENHO INTERESSE<br/><br/>Nome:'.$name.'<br/>Pacote ou Serviço:'.$product_name.'<br/>Tel: '.$tel.'<br/>E-mail:'.$email.'<br/>Obs:'.$message.'</div>';

        $this->createLead();

        $this->sma->send_email($vendedor->email, $subject, $message , null, null, null, $email, '');
    }

    public function createLead($lista_espera = false)
    {
        try {

            //service
            $this->load->model('service/CaptacaoService_model', 'CaptacaoService_model');

            //model
            $this->load->model('model/Captacao_model', 'Captacao_model');

            //repository
            $this->load->model('repository/CaptacaoRespository_model', 'CaptacaoRespository_model');
            $this->load->model('repository/DepartmentRepository_model', 'DepartmentRepository_model');
            $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');

            $name           = $this->input->get('name');
            $phoneNumber    = $this->input->get('tel');
            $email          = $this->input->get('email') != null ? $this->input->get('email')  : '';
            $message        = $this->input->get('message') != null ? $this->input->get('message')  : '';
            $biller_id      = $this->input->get('vendedor');
            $programacao_id = $this->input->get('programacaoId');
            $programacao    = $this->AgendaViagemRespository_model->getProgramacaoById($programacao_id);

            $captacao   = new Captacao_model();

            $biller     = $this->site->getCompanyByID($biller_id);
            $user       = $this->site->getUserByBillerActive($biller_id);

            $captacao->reference_no = $this->site->getReference('cp');
            //TODO CRIAR O CONTATO CATPACAO NA LISTA

            //detalhes
            $captacao->date             = date('Y-m-d H:i:s');
            $captacao->programacao_id   = $programacao_id;
            $captacao->product_id       = $programacao->produto;

            if ($biller) {
                $captacao->biller       = $biller->name;
                $captacao->biller_id    = $biller_id;
            }

            if ($user) {
                $created_by = $user->id;
            } else {
                $user       = $this->site->getUserByBillerActive($this->Settings->default_biller);
                $created_by = $user->id;
            }

            //configuraocao
            $captacao->plantao_id           = $this->Settings->plantao_default_id;
            $captacao->meio_divulgacao_id   = $this->Settings->meio_default_divulgacao_id;
            $captacao->forma_atendimento_id = $this->Settings->forma_default_atendimento_id;
            $captacao->department_id        = $this->Settings->department_default_id;//setor

            if ($lista_espera) {
                $captacao->interesse_id     = $this->Settings->interesse_lista_espera_default_id;//lista de espera
            } else {
                $captacao->interesse_id     = $this->Settings->interesse_tenho_interesse_default_id;//tenho interesse
            }

            //data
            $captacao->name             = $name;
            $captacao->cell_phone       = $phoneNumber;
            $captacao->email            = $email;
            $captacao->note             = $message;
            $captacao->url              = $_SERVER['HTTP_REFERER'];

            //log
            $captacao->created_at       = date('Y-m-d H:i:s');
            $captacao->created_by       = $created_by;

            $this->CaptacaoService_model->save($captacao);

        } catch (Exception $exception) {
            echo $this->sma->send_json(array('success' => false, 'error' => $exception->getMessage()));
        }

    }

}