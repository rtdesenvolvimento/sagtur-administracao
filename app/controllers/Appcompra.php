<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Appcompra extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->Settings->status == 0) {
            $this->loggedIn = false;
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $sd = $this->site->getDateFormat($this->Settings->dateformat);

        $dateFormats = array(
            'js_sdate' => $sd->js,
            'php_sdate' => $sd->php,
            'mysq_sdate' => $sd->sql,
            'js_ldate' => $sd->js . ' hh:ii',
            'php_ldate' => $sd->php . ' H:i',
            'mysql_ldate' => $sd->sql . ' %H:%i'
        );

        $this->dateFormats = $dateFormats;
        $this->data['dateFormats'] = $dateFormats;

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m'] = $this->m;
        $this->data['v'] = $this->v;

        $this->lang->load('sales', $this->Settings->user_language);

        $this->load->model('repository/AgendaViagemRespository_model', 'AgendaViagemRespository_model');
        $this->load->model('repository/ProdutoRepository_model', 'ProdutoRepository_model');
        $this->load->model('repository/CobrancaFaturaRepository_model', 'CobrancaFaturaRepository_model');
        $this->load->model('repository/FinanceiroRepository_model', 'FinanceiroRepository');
        $this->load->model('repository/CupomDescontoRepository_model', 'CupomDescontoRepository_model');
        $this->load->model('repository/ProductDetailsRepository_model', 'ProductDetailsRepository_model');
        $this->load->model('repository/MenuRepository_model', 'MenuRepository_model');
        $this->load->model('repository/TipoTransporteRodoviarioRepository_model', 'TipoTransporteRodoviarioRepository_model');

        $this->load->model('service/AgendaViagemService_model', 'AgendaViagemService_model');
        $this->load->model('service/AppCompraService_model', 'AppCompraService_model');
        $this->load->model('service/VendaService_model', 'VendaService_model');
        $this->load->model('service/FinanceiroService_model', 'FinanceiroService_model');
        $this->load->model('service/AnalyticalService_model', 'AnalyticalService_model');
        $this->load->model('service/BusService_model', 'BusService_model');

        $this->load->model('service/log/LogAppcomprapost_model', 'LogAppcomprapost_model');

        $this->load->model('model/Cliente_model', 'Cliente_model');
        $this->load->model('model/ProdutosAdicionais_model', 'Adicionais');
        $this->load->model('model/AppCompra_model', 'AppCompra_model');
        $this->load->model('model/Ingresso_model', 'Ingresso_model');

        $this->load->model('dto/CupmDescontoViewDTO_model', 'CupmDescontoViewDTO_model');
        $this->load->model('dto/ProgramacaoFilter_DTO_model', 'ProgramacaoFilter_DTO_model');
        $this->load->model('dto/AnalyticalDTO_model', 'AnalyticalDTO_model');

        $this->load->model('settings_model');
        $this->load->model('sales_model');
        $this->load->model('financeiro_model');
        $this->load->model('products_model');
        $this->load->model('Meiodivulgacao_model');

    }

    public function index()
    {
        show_404();
    }

    public function open($id, $vendedorId)
    {
        try {

            $programacao = $this->AgendaViagemService_model->getProgramacaoById($id);
            $vendedor = $this->site->getCompanyByID($vendedorId);
            $user = $this->site->getUserByBillerActive($vendedorId);

            if (!$programacao) {
                $this->show_page_error('Produto não encontrado', 'Produto não encontrado');
            }

            if (!$programacao->active) {
                $this->show_page_error('A Data para Este Produto está inativo no momento', 'Produto fora da venda.');
            }

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $produto =  $this->site->getProductByID($programacao->getProduto());

            if (!$produto->enviar_site) {
                //show_error('Este produto não está mais dipónivel para compra', 404, 'Produto fora da venda.');//todo futurametne vamos criar uma flag para deixar o link disponivel apenas privado
            }

            if (!$produto->active) {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            if ($produto->unit == 'Arquivado') {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $this->show_page_error('As vendas para esse produto já foi encerrado', 'Produto fora da venda.');
            }

            if ($produto->isTaxasComissao) {
                $this->data['tiposCobranca'] = $this->financeiro_model->getAllFormasPagamentoProdutoExibirLinkCompra($produto->id);
            } else {
                $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobrancaExibirLinkCompra();
            }

            $transporteDisponivel = $this->BusService_model->getPlantaDisponivelMarcacao($programacao->getProduto(), $id);

            $this->data['configuracaoGeral'] = $this->site->get_setting();
            $this->data['programacao'] = $programacao;
            $this->data['vendedor'] = $vendedor;
            $this->data['produto'] = $produto;
            $this->data['categoria'] = $this->products_model->getCategoryById($produto->category_id);
            $this->data['meiosDivulgacao'] = $this->Meiodivulgacao_model->getAllActive();
            $this->data['embarques'] = $this->ProdutoRepository_model->getLocaisEmbarque($programacao->getProduto());
            $this->data['tiposHospedagem'] = $this->ProdutoRepository_model->getTipoHospedagem($programacao->getProduto());
            $this->data['opcionais'] = $this->products_model->getProdutosAdicionais($programacao->getProduto());
            $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();
            $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();
            $this->data['faixaEtariaValores'] = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto());
            $this->data['menusPai']         = $this->MenuRepository_model->getAllMenusSuperiorEhURL();
            $this->data['isGallery']        = $this->MenuRepository_model->isPhotosGallery();
            $this->data['transporteDisponivel'] = $transporteDisponivel;

            if ($transporteDisponivel) {
                if ($transporteDisponivel->automovel_id) {
                    $cobrancaExtraConfig = $this->TipoTransporteRodoviarioRepository_model->getTransportesRodoviarioProduto($produto->id, $transporteDisponivel->id);
                    $rotulos = $this->BusRepository_model->getlRotuloCobrancaExtra($cobrancaExtraConfig->configuracao_assento_extra_id);
                    $this->data['rotulos'] =  $rotulos;
                }
            }


            $filter = new ProgramacaoFilter_DTO_model();
            $filter->produto = $produto->id;
            $filter->enviar_site = false;
            //$filter->group_by_date = true;

            $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);

            if ($this->Settings->receptive) {

                if ($this->Settings->atividades_recomendadas) {
                    $this->data['destaques'] = $this->getAtividadesRelacionadas($produto->category_id);
                }

                $this->load->view($this->theme . 'website/cart/shopping_cart_dates1', $this->data);
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO

                if ($this->Settings->web_page_caching) {
                    $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
                }

            } else if ($this->Settings->use_product_landing_page) {

                if ($this->Settings->atividades_recomendadas) {
                    $this->data['destaques'] = $this->getAtividadesRelacionadas($produto->category_id);
                }

                $this->load->view($this->theme . 'website/cart/shopping_cart_dates1', $this->data);//TODO ABRE A LANGING PAGE
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO

                if ($this->Settings->web_page_caching) {
                    $this->output->cache($this->Settings->cache_minutes);//TODO CACHE
                }

            } else {
                if ($programacao->getTotalDisponvel() > 0 || $produto->permitirListaEmpera) {
                    $this->load->view($this->theme . 'appcompra/index', $this->data);
                    $this->save_shopping_cart_access($produto->id, $id);//TODO LOG DE ACESSO

                } else {
                    $this->load->view($this->theme . 'appcompra/esgotado', $this->data);
                    $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO
                }
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function carrinho_compra($id, $vendedorId) {
        try {

            $programacao = $this->AgendaViagemService_model->getProgramacaoById($id);
            $vendedor = $this->site->getCompanyByID($vendedorId);
            $user = $this->site->getUserByBillerActive($vendedorId);

            if (!$programacao) {
                $this->show_page_error('Produto não encontrado', 'Produto não encontrado');
            }

            if (!$programacao->active) {
                $this->show_page_error('A Data para Este Produto está inativo no momento', 'Produto fora da venda.');
            }

            if (!$vendedor || !$user) {
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            if ($vendedor->is_biller == 0 ){
                $this->show_page_error('Vendedor não encontrado', 'Vendedor não encontrado. Entre em contato com agência e solicite o link correto do vendedor.');
            }

            $produto =  $this->site->getProductByID($programacao->getProduto());

            if (!$produto->enviar_site) {
                //show_error('Este produto não está mais dipónivel para compra', 404, 'Produto fora da venda.');//todo futurametne vamos criar uma flag para deixar o link disponivel apenas privado
            }

            if (!$produto->active) {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            if ($produto->unit == 'Arquivado') {
                $this->show_page_error('Este produto está inativo no momento', 'Produto fora da venda.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $this->show_page_error('As vendas para esse produto já foi encerrado', 'Produto fora da venda.');
            }

            if ($produto->isTaxasComissao) {
                $this->data['tiposCobranca'] = $this->financeiro_model->getAllFormasPagamentoProdutoExibirLinkCompra($produto->id);
            } else {
                $this->data['tiposCobranca'] = $this->financeiro_model->getAllTiposCobrancaExibirLinkCompra();
            }

            $transporteDisponivel = $this->BusService_model->getPlantaDisponivelMarcacao($programacao->getProduto(), $id);

            $this->data['configuracaoGeral'] = $this->site->get_setting();
            $this->data['programacao'] = $programacao;
            $this->data['vendedor'] = $vendedor;
            $this->data['produto'] = $produto;
            $this->data['meiosDivulgacao'] = $this->Meiodivulgacao_model->getAllActive();
            $this->data['embarques'] = $this->ProdutoRepository_model->getLocaisEmbarque($programacao->getProduto());
            $this->data['tiposHospedagem'] = $this->ProdutoRepository_model->getTipoHospedagem($programacao->getProduto());
            $this->data['opcionais'] = $this->products_model->getProdutosAdicionais($programacao->getProduto());
            $this->data['sessionCode'] = $this->sma->getTokenPagSeguro();
            $this->data['configPagSeguro'] = $this->settings_model->getPagSeguroSettings();
            $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();
            $this->data['faixaEtariaValores'] = $this->ProdutoRepository_model->getValoresPorFaixaEtaria($programacao->getProduto());
            $this->data['transporteDisponivel'] =  $transporteDisponivel;

            if ($transporteDisponivel) {
                if ($transporteDisponivel->automovel_id) {
                    $cobrancaExtraConfig = $this->TipoTransporteRodoviarioRepository_model->getTransportesRodoviarioProduto($produto->id, $transporteDisponivel->id);
                    $rotulos = $this->BusRepository_model->getlRotuloCobrancaExtra($cobrancaExtraConfig->configuracao_assento_extra_id);
                    $this->data['rotulos'] =  $rotulos;
                }
            }

            $filter = new ProgramacaoFilter_DTO_model();
            $filter->produto = $produto->id;
            $filter->group_by_date = true;
            $filter->enviar_site = false;

            $this->data['programacoes'] =  $this->AgendaViagemService_model->getAllProgramacao($filter);

            if ($programacao->getTotalDisponvel() > 0 || $produto->permitirListaEmpera) {
                $this->load->view($this->theme . 'appcompra/index', $this->data);
                $this->save_shopping_cart_access($produto->id, $id);//TODO LOG DE ACESSO
            } else {
                $this->load->view($this->theme . 'appcompra/esgotado', $this->data);
                $this->record_product_access($produto->id, $id);//TODO LOG DE ACESSO
            }

        } catch (Exception $exception) {
            $this->show_page_error('Erro ao carregar página', $exception->getMessage());
        }
    }

    public function product_lookup($description, $vendedorId) {

        $uid = $this->searh_pacote_by_date($description);

        $this->open($uid, $vendedorId);
    }

    public function shopping_cart($description, $vendedorId) {

        $uid = $this->searh_pacote_by_date($description);

        $this->carrinho_compra($uid, $vendedorId);
    }

    private function searh_pacote_by_date($uri)
    {

        if (strpos($uri, 'uid') !== false) {
            preg_match('/uid(\d+)/', $uri, $matches);
            $uid = $matches[1];
        } elseif (strpos($uri, 'data') !== false) {

            preg_match('/data_(\d{2})-(\d{2})-(\d{4})h(\d{2}_\d{2})p(\d+)/', $uri, $matches);
            $data = $matches[3] . '-' . $matches[2] . '-' . $matches[1];
            $hora = str_replace('_', ':', $matches[4]);
            $produtoID = $matches[5];

            $programacao = $this->AgendaViagemRespository_model->getProgramacaoByData($produtoID, $data, $hora);

            if (empty($programacao)) {
                $programacao = $this->AgendaViagemRespository_model->getProximaProgramcaoByData($produtoID);
            }

            if (empty($programacao)) {
                $this->show_page_error('Ops! Problema para abrir esse produto. Verifique com Agência!', 'Não encontramos a data vinculada a esse produto.');
            }

            $dias = $this->sma->diferencaEntreDuasDatasEmDias(date('Y-m-d'), $programacao->dataSaida);

            if ($dias < 0) {
                $programacao = $this->AgendaViagemRespository_model->getProximaProgramcaoByData($produtoID);
            }

            $uid =  $programacao->id;

        } else {
            $uid = $uri;
        }

        return $uid;
    }

    public function katatur($id, $vendedor) {
        $this->session->set_userdata(array('cnpjempresa' => 'katatur'));

        $this->open($id, $vendedor);
    }

    private function record_product_access($product_id = null, $programacao_id = null) {
        $analytical = new AnalyticalDTO_model();
        $analytical->type= AnalyticalDTO_model::PAGE_VIEW_PRODUCT;
        $analytical->product_id = $product_id;
        $analytical->programacao_id = $programacao_id;

        $this->AnalyticalService_model->registry_access($analytical);
    }

    private function save_shopping_cart_access($product_id = null, $programacao_id = null) {
        $analytical = new AnalyticalDTO_model();
        $analytical->type= AnalyticalDTO_model::PRODUCT_ADDED_CART;
        $analytical->product_id = $product_id;
        $analytical->programacao_id = $programacao_id;

        $this->AnalyticalService_model->registry_access($analytical);
    }

    /*
    public function search_schedulexx() {
        $product_id = $this->input->get('product_id', false);
        $date_shedule = $this->input->get('date_shedule', false);

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->produto = $product_id;
        $filter->date_shedule = $date_shedule;
        $filter->enviar_site = false;

        $this->sma->send_json($this->AgendaViagemService_model->getAllProgramacao($filter));
    }
    */

    private function coletarPagador($product)
    {
        return $product->isApenasColetarPagador == 1 || $product->isApenasColetarPagador == 2;
    }

    private function colegarIngresso($product)
    {
        return $product->isApenasColetarPagador == 1;
    }

    public function add()
    {
        $clientePrincipal   = new Cliente_model();
        $appModel           = new AppCompra_model();

        //registrar log do post
        $this->log();

        /*
        if ($this->session->userdata('cnpjempresa') == 'deboraexcursoes') {
            $this->rd_station();
        }
        */

        $tipoCobrancaVerifica   = $this->financeiro_model->getTipoCobrancaById($this->input->post('tipoCobranca'));
        $programacao            = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));
        $product                = $this->site->getProductByID($programacao->produto);

        if ($tipoCobrancaVerifica->integracao == 'valepay') {
            $cardName = $this->input->post('name_card_valepay', true);
            $cpfTitular = $this->input->post('cpf_titular_valepay');
            $celularTitular = $this->input->post('tel_titular_valepay');
            $cepTitular = $this->input->post('cep_titular_valepay');
            $enderecoTitular = $this->input->post('endereco_titular_valepay');
            $numeroEnderecoTitular = $this->input->post('numero_endereco_titular_valepay');
            $complementoEnderecoTitular = $this->input->post('complemento_endereco_titular');
            $bairroTitular = $this->input->post('bairro_titular_valepay');
            $cidadeTitular = $this->input->post('cidade_titular_valepay');
            $estadoTitular = $this->input->post('estado_titular_valepay');
            $parcelas = $this->input->post('parcelamento_valepay', true);

        } else {
            $cardName = $this->input->post('nameCartaoPagSeguro', true);
            $cpfTitular = $this->input->post('cpfTitular');
            $celularTitular = $this->input->post('telTitular');
            $cepTitular = $this->input->post('cepTitular');
            $enderecoTitular = $this->input->post('enderecoTitular');
            $numeroEnderecoTitular = $this->input->post('numeroEnderecoTitular');
            $complementoEnderecoTitular = $this->input->post('complementoEnderecoTitular');
            $bairroTitular = $this->input->post('bairroTitular');
            $cidadeTitular = $this->input->post('cidadeTitular');
            $estadoTitular = $this->input->post('estadoTitular');

            if ($this->input->post('installments', true) > 0) {
                $parcelas = $this->input->post('installments', true);
            } else {
                $parcelas = $this->input->post('parcelas', true);
            }
        }

        $i = isset($_POST['nomeDependente']) ? sizeof($_POST['nomeDependente']) : 0;
        $sequencialResponsavelCompra    = 1;
        $contadorClientes               = 1;

        if ($this->coletarPagador($product)) {

            $i = isset($_POST['nomeDependentePagador']) ? sizeof($_POST['nomeDependentePagador']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $clientePrincipal->setName($_POST['nomeDependentePagador'][$r]);
                $clientePrincipal->setCompany($_POST['company_name'][$r]);
                $clientePrincipal->setNomeResponsavel($_POST['nome_responsavel'][$r]);
                $clientePrincipal->setEmail($_POST['emailDependentePagador'][$r]);
                $clientePrincipal->setSexo($_POST['sexoDependentePagador'][$r]);
                $clientePrincipal->setVatNo($_POST['cpfDependentePagador'][$r]);
                $clientePrincipal->setCf5($_POST['celularDependentePagador'][$r]);
                $clientePrincipal->setTelefoneEmergencia($_POST['telefone_emergenciaDependentePagador'][$r]);
                $clientePrincipal->setAlergiaMedicamento($_POST['alergia_medicamentoDependentePagador'][$r]);

                /*
                $clientePrincipal->setFaixaId(($_POST['tipoFaixaEtariaId'][$r]));
                $clientePrincipal->setFaixaName(($_POST['tipoFaixaEtariaNome'][$r]));
                $clientePrincipal->setFaixaEtariaValor(($_POST['tipoFaixaEtariaValor'][$r]));
                $clientePrincipal->setDescontarVaga(($_POST['descontarVaga'][$r]));
                */

                $clientePrincipal->setTipoDocumento(($_POST['tipo_documentoDependentePagador'][$r]));
                $clientePrincipal->setCf1($_POST['rgDependentePagador'][$r]);
                $clientePrincipal->setCf3(($_POST['orgaoEmissorDependentePagador'][$r]));

                $clientePrincipal->setSocialName(($_POST['social_namePagador'][$r]));
                $clientePrincipal->setProfession(($_POST['profissaoDependentePagador'][$r]));
                $clientePrincipal->setDoencaInformar(($_POST['doenca_informarPagador'][$r]));
                $clientePrincipal->setUltimaNota(($_POST['observacao_item'][$r]));

                $clientePrincipal->setUltimoAssento('');
                $clientePrincipal->setUltimoValorExtraAssento(0);

                $dia = $_POST['diaDependentePagador'][$r];
                $mes = $_POST['mesDependentePagador'][$r];
                $ano = $_POST['anoDependentePagador'][$r];

                $clientePrincipal->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                $cep = $this->input->post('cep');
                $endereco = $this->input->post('endereco');
                $numeroEndereco = $this->input->post('numeroEndereco');
                $complementoEndereco = $this->input->post('complementoEndereco');
                $bairro = $this->input->post('bairro');
                $cidade = $this->input->post('cidade');
                $estado = $this->input->post('estado');

                $clientePrincipal->setPostalCode($cep);
                $clientePrincipal->setAddress($endereco);
                $clientePrincipal->setNumero($numeroEndereco);
                $clientePrincipal->setComplemento($complementoEndereco);
                $clientePrincipal->setBairro($bairro);
                $clientePrincipal->setCity($cidade);
                $clientePrincipal->setState($estado);

                $appModel->cliente = $clientePrincipal;

            }
        } else {
            for ($r = 0; $r < $i; $r++) {

                $responsavelPelaCompra = $_POST['responsavelPelaCompra'][$r];

                if ($responsavelPelaCompra == 'SIM') {//TODO VERIFICA O RESPONSAVEL PELA COMPRA ESTA COM ERRO AQUI

                    $sequencialResponsavelCompra  = $contadorClientes;

                    $clientePrincipal->setName($_POST['nomeDependente'][$r]);
                    $clientePrincipal->setCompany($_POST['company_name'][$r]);
                    $clientePrincipal->setNomeResponsavel($_POST['nome_responsavel'][$r]);
                    $clientePrincipal->setEmail($_POST['emailDependente'][$r]);
                    $clientePrincipal->setSexo($_POST['sexoDependente'][$r]);
                    $clientePrincipal->setVatNo($_POST['cpfDependente'][$r]);
                    $clientePrincipal->setCf5($_POST['celularDependente'][$r]);
                    $clientePrincipal->setTelefoneEmergencia($_POST['telefone_emergenciaDependente'][$r]);
                    $clientePrincipal->setAlergiaMedicamento($_POST['alergia_medicamentoDependente'][$r]);

                    $clientePrincipal->setFaixaId(($_POST['tipoFaixaEtariaId'][$r]));
                    $clientePrincipal->setFaixaName(($_POST['tipoFaixaEtariaNome'][$r]));
                    $clientePrincipal->setFaixaEtariaValor(($_POST['tipoFaixaEtariaValor'][$r]));
                    $clientePrincipal->setDescontarVaga(($_POST['descontarVaga'][$r]));

                    $clientePrincipal->setTipoDocumento(($_POST['tipo_documentoDependente'][$r]));
                    $clientePrincipal->setCf1($_POST['rgDependente'][$r]);
                    $clientePrincipal->setCf3(($_POST['orgaoEmissorDependente'][$r]));

                    $clientePrincipal->setSocialName(($_POST['social_name'][$r]));
                    $clientePrincipal->setProfession(($_POST['profissaoDependente'][$r]));
                    $clientePrincipal->setDoencaInformar(($_POST['doenca_informar'][$r]));
                    $clientePrincipal->setUltimoAssento(($_POST['assento'][$r]));
                    $clientePrincipal->setUltimoValorExtraAssento(($_POST['valor_extra_assento'][$r]));
                    $clientePrincipal->setUltimaNota(($_POST['observacao_item'][$r]));

                    $dia = $_POST['diaDependente'][$r];
                    $mes = $_POST['mesDependente'][$r];
                    $ano = $_POST['anoDependente'][$r];

                    $clientePrincipal->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                    $cep = $this->input->post('cep');
                    $endereco = $this->input->post('endereco');
                    $numeroEndereco = $this->input->post('numeroEndereco');
                    $complementoEndereco = $this->input->post('complementoEndereco');
                    $bairro = $this->input->post('bairro');
                    $cidade = $this->input->post('cidade');
                    $estado = $this->input->post('estado');

                    $clientePrincipal->setPostalCode($cep);
                    $clientePrincipal->setAddress($endereco);
                    $clientePrincipal->setNumero($numeroEndereco);
                    $clientePrincipal->setComplemento($complementoEndereco);
                    $clientePrincipal->setBairro($bairro);
                    $clientePrincipal->setCity($cidade);
                    $clientePrincipal->setState($estado);

                    $appModel->cliente = $clientePrincipal;
                }

                $contadorClientes++;
            }
        }

        if ($this->colegarIngresso($product)) {

            foreach ($_POST as $chave => $qtdIngressos) {
                if (strpos($chave, "Clientes_") === 0) {

                    $partes         = explode("_", $chave);
                    $faixaId        = $partes[1];
                    $preco          = $partes[2];
                    $descontarVaga  = $partes[3];
                    $hospedagem     = $partes[4];

                    $ingresso = new Ingresso_model();
                    $ingresso->faixaId          = $faixaId;
                    $ingresso->quantidade       = $qtdIngressos;
                    $ingresso->valor            = $preco;
                    $ingresso->descontarVaga    = $descontarVaga;

                    $appModel->addIngressos($ingresso);
                }
            }
        }

        $appModel->localEmbarque = $this->input->post('localEmbarque');
        $appModel->tipoHospedagem = $this->input->post('tipoHospedagem');
        $appModel->tipoTransporte = $this->input->post('tipoTransporteId');

        $appModel->tipoCobranca = $this->input->post('tipoCobranca');
        $appModel->condicaoPagamento = $this->input->post('condicaoPagamento');
        $appModel->observacaoAdicional = $this->input->post('observacao');

        $appModel->total = $this->input->post('subTotal');
        $appModel->totalComTaxas = $this->input->post('subTotalTaxas');

        //sinal
        $appModel->valorSinal = (double) $this->input->post('valorSinal');
        $appModel->tipoCobrancaSinal = $this->input->post('tipoCobrancaSinal');

        $appModel->valorHospedagemAdulto = $this->input->post('valorHospedagemAdulto');
        $appModel->valorHospedagemCrianca = $this->input->post('valorHospedagemCrianca');
        $appModel->valorHospedagemBebevalorHospedagemBebe = $this->input->post('valorHospedagemBebe');

        $appModel->valorPacoteAdulto = $this->input->post('valorPacoteAdulto');
        $appModel->valorPacoteCrianca = $this->input->post('valorPacoteCrianca');
        $appModel->valorPacoteBebe = $this->input->post('valorPacoteBebe');

        $appModel->vendedorId = $this->input->post('vendedor');

        $appModel->programacao = $programacao;
        $appModel->produto =  $product;

        $appModel->file_pdf = $this->input->post('file_pdf');

        $appModel->vencimentosParcela = $this->input->post('dtVencimentos');
        $appModel->valorVencimentosParcelas = $this->input->post('valorVencimentos');
        $appModel->descontos = $this->input->post('descontos');
        $appModel->tiposCobranca = $this->input->post('tiposCobranca');
        $appModel->movimentadores = $this->input->post('movimentadores');

        $appModel->totalPassageiros = $this->input->post('totalPassageiros');

        $appModel->cupom_id = $this->input->post("cupom_desconto_id");
        $appModel->desconto_cupom = $this->input->post("valor_cupom_desconto");

        $appModel->listaEspera = (int) $this->input->post("listaEmpera");

        $senderHash = htmlspecialchars($this->input->post('senderHash', true));
        $creditCardToken = htmlspecialchars($this->input->post('creditCardToken', true));
        $deviceId = htmlspecialchars($this->input->post('deviceId', true));

        $appModel->meioDivulgacao = $this->input->post('meioDivulgacao');

        $appModel->totalParcelaPagSeguro = $this->input->post('totalParcelaPagSeguro');

        $appModel->cpfTitularCartao = $cpfTitular;
        $appModel->celularTitularCartao = $celularTitular;
        $appModel->cepTitularCartao = $cepTitular;
        $appModel->enderecoTitularCartao = $enderecoTitular;
        $appModel->numeroEnderecoTitularCartao = $numeroEnderecoTitular;
        $appModel->complementoEnderecoTitularCartao = $complementoEnderecoTitular;
        $appModel->bairroTitularCartao = $bairroTitular;
        $appModel->cidadeTitularCartao = $cidadeTitular;
        $appModel->estadoTitularCartao = $estadoTitular;

        $appModel->parcelas             = $parcelas;
        $appModel->cardName             = $cardName;
        $appModel->cardNumber           = $this->input->post('card_number_valepay', true);
        $appModel->cardExpiryMonth      = $this->input->post('card_expiry_mm_valepay', true);
        $appModel->cardExpiryYear       = $this->input->post('card_expiry_yyyy_valepay', true);
        $appModel->cvc                  = $this->input->post('cvc_valepay', true);

        //token cartao de credito
        $appModel->senderHash       = $senderHash;
        $appModel->creditCardToken  = $creditCardToken;
        $appModel->deviceId         = $deviceId;

        $i = isset($_POST['nomeDependente']) ? sizeof($_POST['nomeDependente']) : 0;
        $contadorClientes = 1;

        for ($r = 0; $r < $i; $r++) {
            $dependente = new Cliente_model();
            $nomeDependente = $_POST['nomeDependente'][$r];

            if ($nomeDependente != '' && $contadorClientes != $sequencialResponsavelCompra) {

                $dependente->setName($nomeDependente);
                $dependente->setSexo($_POST['sexoDependente'][$r]);
                $dependente->setVatNo($_POST['cpfDependente'][$r]);
                $dependente->setCf5($_POST['celularDependente'][$r]);
                $dependente->setTelefoneEmergencia($_POST['telefone_emergenciaDependente'][$r]);
                $dependente->setAlergiaMedicamento($_POST['alergia_medicamentoDependente'][$r]);
                $dependente->setEmail($_POST['emailDependente'][$r]);

                $dependente->setFaixaId(($_POST['tipoFaixaEtariaId'][$r]));
                $dependente->setFaixaName(($_POST['tipoFaixaEtariaNome'][$r]));
                $dependente->setFaixaEtariaValor(($_POST['tipoFaixaEtariaValor'][$r]));
                $dependente->setDescontarVaga(($_POST['descontarVaga'][$r]));

                $dependente->setTipoDocumento(($_POST['tipo_documentoDependente'][$r]));
                $dependente->setCf1($_POST['rgDependente'][$r]);
                $dependente->setCf3(($_POST['orgaoEmissorDependente'][$r]));
                $dependente->setSocialName(($_POST['social_name'][$r]));
                $dependente->setProfession(($_POST['profissaoDependente'][$r]));
                $dependente->setDoencaInformar(($_POST['doenca_informar'][$r]));
                $dependente->setUltimoAssento(($_POST['assento'][$r]));
                $dependente->setUltimoValorExtraAssento(($_POST['valor_extra_assento'][$r]));
                $dependente->setUltimaNota(($_POST['observacao_item'][$r]));

                $dia = $_POST['diaDependente'][$r];
                $mes = $_POST['mesDependente'][$r];
                $ano = $_POST['anoDependente'][$r];

                $dependente->setDataAniversario($ano . '-' . $mes . '-' . $dia);

                $appModel->adicionarDependente($dependente);
            }

            $contadorClientes++;
        }

        $j = isset($_POST['qtdAdicionais']) ? sizeof($_POST['qtdAdicionais']) : 0;
        for ($r = 0; $r < $j; $r++) {

            $servicoId       = $_POST['servicosAdicionais'][$r];
            $valorAdicional  = $_POST['valorServicoAdicional'][$r];
            $qtd             = $_POST['qtdAdicionais'][$r];
            $faixaIdAdiconal = $_POST['faixaIdAdiconal'][$r];

            $adicional = new ProdutosAdicionais_model();
            $adicional->produto = $servicoId;
            $adicional->quantidade = $qtd;
            $adicional->valor = $valorAdicional;
            $adicional->faixaId = $faixaIdAdiconal;
            $adicional->tipo = ProdutosAdicionais_model::ADULTO;

            $appModel->addAdicionais($adicional);
        }

        try {

            $vendaId        = $this->AppCompraService_model->salvar($appModel);
            $venda          = $this->sales_model->getInvoiceByID($vendaId);
            $tipoCobranca   = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

            if ($this->pagamentoComPix($tipoCobranca)) {

                if ($clientePrincipal->getEmail() != '') {
                    $this->enviar_email($venda, $clientePrincipal->getEmail());
                }

                $this->confirmacaoVendaPix($vendaId);

            } else if ($this->pagamentoEmCartaoCreditoMercadoPagoTransparente($tipoCobranca) || $this->pagamentoComCartaoCreditoPagSeguroTransparente($tipoCobranca) || $this->pagamentoEmCartaoCreditoValepayTransparente($tipoCobranca)) {
                $this->confirmacaoVendaCartaoCredito($vendaId);
            } else if ($this->pagamentoComLinkPagamentoPagSeguro($tipoCobranca)){

                if ($clientePrincipal->getEmail() != '') $this->enviar_email($venda, $clientePrincipal->getEmail());

                $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

                foreach ($faturas as $fat) {
                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                    redirect($cobranca->checkoutUrl);
                }

            } else {

                if ($clientePrincipal->getEmail() != '') {
                    $this->enviar_email($venda, $clientePrincipal->getEmail());
                }

                if ($venda->sale_status == 'orcamento') {
                    redirect(base_url() . "appcompra/orcamento/" . $vendaId . '/' . $programacao->id . '/' . $this->input->post('vendedor') . '?token=' . $this->session->userdata('cnpjempresa'));
                } else if ($venda->sale_status == 'lista_espera') {
                    redirect(base_url() . "appcompra/lista_espera/" . $vendaId . '/' . $programacao->id . '/' . $this->input->post('vendedor') . '?token=' . $this->session->userdata('cnpjempresa'));
                } else {
                    redirect(base_url() . "appcompra/confirmacaoDeReserva/" . $vendaId . '/' . $programacao->id . '/' . $this->input->post('vendedor') . '?token=' . $this->session->userdata('cnpjempresa'));
                }
            }

        } catch (Exception $exception) {

            $vendedor = $this->site->getCompanyByID($this->input->post('vendedor'));
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($this->input->post('programacaoId'));

            $this->data['erroProcessaPagamento'] = $exception->getMessage();
            $this->data['produto'] = $this->site->getProductByID($programacao->produto);

            $this->data['vendedor'] = $vendedor;
            $this->data['programacao'] = $programacao;
            $this->data['configuracaoGeral'] = $this->site->get_setting();

            $this->load->view($this->theme . 'appcompra/erro_pagamento_cartao', $this->data);
        }
    }

    private function rd_station()
    {
        require_once('vendor/autoload.php');

        // Captura todos os campos do formulário
        $formData = $_POST;

        $formData['conversion_identifier'] = 'Reserva de Pacotes';
        //$formData['email'] = 'andre@resultatec.com.br';
        //$formData['name'] = 'Jose Velho';

        // Constrói o payload com todos os dados do formulário
        $payload = [
            'event_type' => 'CONVERSION',
            'event_family' => 'CDP',
            'payload' => $formData
        ];

        // Inicializa o cliente Guzzle
        $client = new \GuzzleHttp\Client();

        // Faz a requisição para a API
        $response = $client->request('POST', 'https://api.rd.services/platform/conversions', [
            'query' => ['api_key' => 'WoVpkWqfXCGRiYTQPPNphCArpoYrXniuSznU'],
            'body' => json_encode($payload), // Converte o payload para JSON
            'headers' => [
                'Content-Type' => 'application/json',
                'accept' => 'application/json',
            ],
        ]);

        // Exibe a resposta da API
        $data = $response->getBody();

        echo $data;
    }

    private function log() {
        $this->LogAppcomprapost_model->info(print_r($_POST, true));
    }

    function confirmacaoVendaPix($vendaId)
    {

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
        $contador = 0;

        foreach ($faturas as $fatura) {
            if ($contador == 0) $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);
            $contador++;
        }

        if (empty($cobrancaFatura) && empty($faturas)) show_404();

        if (stristr($cobrancaFatura->code, 'pay_') ) {
            redirect(base_url() . "pix/pagamento/" . $cobrancaFatura->code . '?token=' . $this->session->userdata('cnpjempresa'));
        } else if ($cobrancaFatura->integracao == 'cobrefacil') {
            redirect($cobrancaFatura->link);
        } else {
            redirect(base_url() . "pix/pagamento/" . $cobrancaFatura->qr_code . '?token=' . $this->session->userdata('cnpjempresa'));
        }
    }

    function confirmacaoVendaCartaoCredito($vendaId)
    {
        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);

        foreach ($faturas as $fatura) {
            $cobrancaFatura = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);
        }

        if (empty($cobrancaFatura) && empty($faturas)) show_404();

        redirect(base_url() . "cartaocredito/pagamento/" . $cobrancaFatura->code . '?token=' . $this->session->userdata('cnpjempresa'));

    }

    function pagamentoEmCartaoCreditoMercadoPagoTransparente($tipoCobranca)
    {
        return $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' && $tipoCobranca->integracao == 'mercadopago';
    }

    function pagamentoEmCartaoCreditoValepayTransparente($tipoCobranca)
    {
        return $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' && $tipoCobranca->integracao == 'valepay';
    }

    function pagamentoComCartaoCreditoPagSeguroTransparente($tipoCobranca) {
        return $tipoCobranca->tipo == 'carne_cartao_transparent' && $tipoCobranca->integracao == 'pagseguro';
    }

    function pagamentoComPix($tipoCobranca)
    {
        return $tipoCobranca->tipo == 'pix';
    }

    function pagamentoComLinkPagamentoPagSeguro($tipoCobranca) {
        return $tipoCobranca->tipo == 'carne_cartao' && $tipoCobranca->integracao == 'pagseguro';
    }

    function enviar_email($venda, $email)
    {
        /*
        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $configuracaoGeral = $this->site->get_setting();

        if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
            if ($venda->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/orcamento.html');
            } else if ($venda->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/lista_espera.html');
            } else {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            }
        } else {
            if ($venda->sale_status == 'orcamento') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/orcamento.html');
            } else if ($venda->sale_status == 'lista_espera') {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/lista_espera.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
            }
        }

        $customer = $this->site->getCompanyByID($venda->customer_id);
        $this->load->library('parser');

        $parse_data = array(
            'reference_number' => $venda->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->name,
            'site_link' => base_url(),
            'site_name' => $configuracaoGeral->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $configuracaoGeral->logo2 . '" alt="' . $configuracaoGeral->site_name . '"/>',
        );

        $message = $this->parser->parse_string($sale_temp, $parse_data);

        if ($venda->sale_status == 'orcamento') {
            $subject = 'ORÇAMENTO  ' . $configuracaoGeral->site_name . ' CLIENTE ' . $customer->name;
        } else if($venda->sale_status == 'lista_espera') {
            $subject = 'LISTA DE ESPERA ' . $configuracaoGeral->site_name . ' CLIENTE ' . $customer->name;
        } else {
            $subject = 'RESERVA ' . $configuracaoGeral->site_name . ' CLIENTE ' . $customer->name;
        }

        $to = $email;
        $cc = $vendedor->email;

        $attachment = $this->pdf($venda->id, null, 'S');

        if ($configuracaoGeral->is_email_queue) {
            $this->sma->send_email_queue($this->db, 'sales', 'create', $venda->id, $to, $subject, $message, null, null, $attachment, $cc, '');
        } else {
            $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, '');
            delete_files($attachment);
        }
        */

    }

    function orcamento($vendaId, $programacaoId, $vendedorId)
    {

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;
        $this->data['vendedor'] = $vendedor;
        $this->data['faturas'] = $faturas;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['configuracaoGeral'] = $this->site->get_setting();
        $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();

        $this->load->view($this->theme . 'appcompra/orcamento', $this->data);
    }

    function lista_espera($vendaId, $programacaoId, $vendedorId)
    {

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;
        $this->data['vendedor'] = $vendedor;
        $this->data['faturas'] = $faturas;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['configuracaoGeral'] = $this->site->get_setting();
        $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();

        $this->load->view($this->theme . 'appcompra/lista_espera', $this->data);
    }

    function confirmacaoDeReserva($vendaId, $programacaoId, $vendedorId)
    {

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;
        $this->data['vendedor'] = $vendedor;
        $this->data['faturas'] = $faturas;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['configuracaoGeral'] = $this->site->get_setting();
        $this->data['configMercadoPago'] = $this->settings_model->getMercadoPagoSettings();

        $this->load->view($this->theme . 'appcompra/confirmacao', $this->data);
    }

    function view($vendaId, $programacaoId, $vendedorId)
    {

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($programacaoId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;
        $this->data['vendedor'] = $vendedor;
        $this->data['faturas'] = $faturas;
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['configuracaoGeral'] = $this->site->get_setting();

        $this->load->view($this->theme . 'appcompra/view', $this->data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $inv = $this->site->getInvoiceByID($id);

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";

        $faturas = $this->site->getParcelasFaturaByContaReceber($id);
        $return  = $this->site->getReturnBySID($id);

        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->site->getPaymentsForSale($id);
        $this->data['biller']   = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user']     = $this->site->getUser($inv->created_by);
        $this->data['warehouse']= $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['parcelasOrcamento'] = $this->site->getParcelasOrcamentoVenda($id);
        $this->data['return_sale'] = $return;
        $this->data['faturas'] = $faturas;
        $this->data['rows'] = $this->site->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->site->getAllReturnItems($return->id) : null;
        $this->data['inv'] = $inv;

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . '_' . $this->session->userdata('cnpjempresa') . ".pdf";
        $html = $this->load->view($this->theme . 'salesutil/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'salesutil/pdf', $this->data);
        } elseif ($save_bufffer) {
            if ($inv->sale_status == 'orcamento') {
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else if ($inv->sale_status == 'lista_espera') {
                return $this->sma->generate_pdf_showWatermarkImage($html, $name, $save_bufffer, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/lista_espera.png'));
            } else {
                return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
            }
        } else {
            if ($inv->sale_status == 'orcamento') {
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/orcamento2.png'));
            } else  if ($inv->sale_status == 'lista_espera') {
                $this->sma->generate_pdf_showWatermarkImage($html, $name, false, $this->data['biller']->invoice_footer, null, null, null, 'P', base_url('assets/images/lista_espera.png'));
            } else {
                $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
            }
        }
    }

    public function confirmacao($programacaoId, $vendedor, $vendaId)
    {
        $programacao = $this->AgendaViagemService_model->getProgramacaoById($programacaoId);
        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);

        $this->data['configuracaoGeral'] = $this->site->get_setting();
        $this->data['programacao'] = $programacao;
        $this->data['venda'] = $venda;

        $this->data['produto'] = $this->site->getProductByID($programacao->getProduto());
        $this->data['tipoCobranca'] = $tipoCobranca;
        $this->data['vendedor'] = $vendedor;

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
        $cobranca = NULL;

        foreach ($faturas as $fat) {
            $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
        }

        $this->data['cobranca'] = $cobranca;
        $this->data['fatura'] = $fat;

        if ($tipoCobranca->tipo == 'carne_cartao_transparent') {//TODO CARTAO CHECKIN TRANSPARTENTE
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
            $cobranca = NULL;

            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
            }

            if ($fat->status == 'CANCELADA') {
                $this->load->view($this->theme . 'appcompra/finalizacao', $this->data);
            } else if ($fat->status == 'ABERTA') {
                $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
            } else if ($fat->status == 'PAGO') {
                $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
            }

        } else {
            $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fat, $cobranca);
        }
    }

    function whatsapp_pdf_atualizado($vendaId) {

        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);
        $eols = array("\n", "\r", "\r\n");

        foreach ($faturas as $fatura) {
            $cobranca = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);
        }

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $cliente = $this->site->getCompanyByID($venda->customer_id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);
        $condicaoPagamento = $this->financeiro_model->getCondicaoPagamentoById($venda->condicaopagamentoId);
        $configuracao = $this->site->get_setting();

        $phone = $cliente->cf5;

        $phone = str_replace('(', '', str_replace(')', '', $phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $inv_items = $this->sales_model->getAllInvoiceItems($venda->id);

        foreach ($inv_items as $item) {
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);
            $produto = $this->site->getProductByID($programacao->produto);
            $localEmbarque = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($item->product_id, $item->localEmbarqueId);
        }

        $text = '';

        $text .= '%0A';

        if ($venda->sale_status == 'orcamento') {
            $text .= '%0AOlá, segue os dados do seu orçanento.';
        } else if ($venda->sale_status == 'orcamento') {
            $text .= '%0AOlá, segue os dados da sua lista de espera.';
        } else {
            $text .= '%0AOlá, segue os dados da sua reserva.';
        }

        $text .= '%0AVendedor: *' . $vendedor->name . '*';
        $text .= '%0A';

        if ($venda->total_items > 0) {
            $text .= '%0A*✔ PASSAGEIRO(S)*%0A';
            foreach ($inv_items as $item) {
                $dependente = $this->site->getCompanyByID($item->customerClient);
                if (!$item->adicional) {
                    $text .= '%0A *Nome:* ' . $dependente->name . ' *[' . $item->faixaNome . ']*';
                    if ($dependente->cf5) $text .= '%0A *Celular:*  ' . $dependente->phone . ' ' . $dependente->cf5;
                    if ($dependente->email) $text .= '%0A *E-mail:* ' . $dependente->email;
                    $text .= '%0A';
                }
            }
        }

        $text .= '%0A';
        $text .= '%0A*✔ DADOS DA VIAGEM*%0A';
        $text .= '%0A *'.$produto->name.'*';

        if ($this->Settings->ocultar_horario_saida){
            $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida);
        } else {
            $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida) . ' ' . $programacao->horaSaida;
        }

        $text .= '%0A *Data Retorno:* ' . $this->sma->hrsd($programacao->dataRetorno) . ' ' . $programacao->horaRetorno;

        if ($localEmbarque->name) {
            $text .= '%0A *Local Embarque:* ' . $localEmbarque->name;
        }

        if ($localEmbarque->note) {
            $text .= '%0A *Nota:* '.$this->decode_html($localEmbarque->note);
        }

        if ($localEmbarque->dataEmbarque != null && $localEmbarque->dataEmbarque != '0000-00-00'){
            $text .= '%0A *Data Embarque:* '.$this->sma->hrsd($localEmbarque->dataEmbarque);
        }

        if ($localEmbarque->horaEmbarque != null && $localEmbarque->horaEmbarque != '00:00:00')  {
            $text .= '%0A *Hora Embarque:* '.$this->sma->hf($localEmbarque->horaEmbarque);
        }

        $text .= '%0A';

        $text .= '%0A*✔ DADOS DA RESERVA*%0A';
        $text .= '%0A *Código:* ' . $venda->reference_no;
        $text .= '%0A *Data de Emissão:* ' . $this->sma->hrld($venda->date);
        $text .= '%0A *Valor:* ' . $this->sma->formatMoney($venda->total);
        $text .= '%0A *Situação da venda:* *' . lang($fatura->status).'*';
        $text .= '%0A *Forma de Pagamento:* ' . trim($tipoCobranca->name);

        if ($tipoCobranca->note) {

            $note_cobranca = $tipoCobranca->note;
            $note_cobranca = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('°', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('&', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $note_cobranca);

            $text .= '%0A *OBS:* ' . $this->decode_html($note_cobranca);
        }

        if ($tipoCobranca->tipo == 'dinheiro') {
            $text .= '%0A Condições de Pagamento: ' . $condicaoPagamento->name;
            $text .= '%0A Se optou por deposito/transferência assim que realizar o pagamento nos envia o comprovante para confirmar a reserva.😉';
        }

        if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') {//boletos
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0ACondições de Pagamento: ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver boleto ' . $contador . 'º parcela clique aqui ' . $cobranca->link;
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'pix') {//pix
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver o Link de Pagamento' . $contador . 'º parcela clique aqui%0A' . urlencode($cobranca->link);
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'carne') {//carne
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0ACondições de Pagamento: ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {

                if ($contador == 1) {
                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                    $text .= '%0A Ver carnê clique aqui ' . $cobranca->link;
                }
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'cartao' || $tipoCobranca->tipo == 'carne_cartao') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A Link para pagamento do cartão de crédito ' . $cobranca->checkoutUrl;
        }

        if ($tipoCobranca->tipo == 'carne_cartao_transparent') {
            if ($fatura->status == 'ABERTA') {
                $text .= '%0A Situação do pagamento: Pagamento em análise juntamente com a PagSeguro. Você poderá receber um contato da equipe PagSeguro para confirmar sua compra.';
            } else if ($fatura->status == 'PAGO') {
                $text .= '%0A Situação do pagamento: Seu pagamento foi confirmado com sucesso!';
            }
        }

        if ($tipoCobranca->tipo == 'link_pagamento' || $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0ALink para pagamento ' . $cobranca->checkoutUrl;
        }

        $text .= '%0A';
        $text .= '%0AClique no link abaixo para baixar o voucher atualizado da sua compra:%0A' . base_url() . 'appcompra/pdf/' . $venda->id . '?token=' . $this->session->userdata('cnpjempresa');

        if ($this->Settings->informacoesImportantesVoucher) {
            $info = $this->Settings->informacoesImportantesVoucher;

            $info = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('°', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('&', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $info);

            $text .= '%0A';
            $text .= '%0A*✔ INFORMAÇÕES IMPORTANTES*%0A';
            $text .= '%0A';
            $text .= $this->decode_html($info);
            $text .= '%0A';
        }

        $text .= '%0A';
        $text .= '%0AAtenciosamente,';
        $text .= '%0A*'.$configuracao->site_name.'*';
        $text .= '%0A%0A' . date('d/m/Y H:i:s');
        $text .= '%0A*[Mensagem Automática]*';

        $text = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('°', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('&', $this->getEspacoHtmlWhatsApp(), $text);

        $text = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $text);

        //$phone = '4898235746';
        //echo $text;
        //die();

        header('Content-Type: text/html; charset=utf-8');

        header('Location: https://api.whatsapp.com/send?phone=55' . trim($phone) . '&text=' . $text);
        exit;
    }

    function abrirWhatsAppConfirmacao($vendedor, $venda, $fatura, $cobranca, $phone=null)
    {

        $configuracao = $this->site->get_setting();
        $eols = array("\n", "\r", "\r\n");

        if ($phone == null) {
            $phone = $vendedor->phone;
        }

        $cliente = $this->site->getCompanyByID($venda->customer_id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);
        $condicaoPagamento = $this->financeiro_model->getCondicaoPagamentoById($venda->condicaopagamentoId);

        $phone = str_replace('(', '', str_replace(')', '', $phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $inv_items = $this->sales_model->getAllInvoiceItems($venda->id);

        foreach ($inv_items as $item) {
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);
            $produto = $this->site->getProductByID($programacao->produto);
            $localEmbarque = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($item->product_id, $item->localEmbarqueId);
        }

        $text = '';

        $text .= 'Olá me chamo *' . trim($cliente->name) . '*';

        if ($venda->sale_status == 'orcamento') {
            $text .= '%0AAcabo de fazer um orçamento pelo link.';
        } else if ($venda->sale_status == 'lista_espera') {
            $text .= '%0AAcabo de entrar para a lista de espera pelo link de reservas.';
        } else {
            $text .= '%0AAcabo de reservar meu pacote pelo link de reservas.';
        }

        $text .= '%0A';
        $text .= '%0AVendedor:  *' . $vendedor->name . '*';
        $text .= '%0A';

        if ($venda->total_items > 0) {
            $text .= '%0A*✔ PASSAGEIRO(S)*%0A';
            foreach ($inv_items as $item) {
                $dependente = $this->site->getCompanyByID($item->customerClient);

                if (!$item->adicional) {

                    $text .= '%0A *Nome:* ' . $dependente->name . ' *[' . trim($item->faixaNome) . ']*';
                    if ($dependente->cf5) $text .= '%0A *Celular:*  ' . $dependente->phone . ' ' . $dependente->cf5;
                    if ($dependente->email) $text .= '%0A *E-mail:* ' . $dependente->email;
                    if ($dependente->vat_no) $text .= '%0A *CPF:* ' . $dependente->vat_no;
                    if ($dependente->cf1) $text .= '%0A *Documento:* ' . strtoupper($dependente->tipo_documento) . ' ' . $dependente->cf1;

                    if ($dependente->data_aniversario) {

                        list($ano, $mes, $dia) = explode('-', $dependente->data_aniversario);

                        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                        $text .= '%0A *Nascimento:* ' . $this->sma->hrsd($dependente->data_aniversario) . ' ' . $idade . ' anos';
                    }
                    $text .= '%0A';

                } else {
                    $text .= '%0A *Adicional:* ' . $item->product_name.'%0A';
                }
            }
        }

        $text .= '%0A';

        $text .= '%0A*✔ DADOS DA VIAGEM*%0A';
        $text .= '%0A *' . $produto->name. '*';

        if ($produto->category_id != 14) {
            if ($this->Settings->ocultar_horario_saida){
                $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida);
            } else {
                $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida) . ' ' . $this->sma->hf($programacao->horaSaida);
            }
            $text .= '%0A *Data Retorno:* ' . $this->sma->hrsd($programacao->dataRetorno) . ' ' . $this->sma->hf($programacao->horaRetorno);
        }

        if ($localEmbarque->name) {
            $text .= '%0A *Local Embarque:* ' . $localEmbarque->name;
        }

        if ($localEmbarque->note) {
            $text .= '%0A *Nota:* '.$this->decode_html($localEmbarque->note);
        }

        if ($localEmbarque->dataEmbarque != null && $localEmbarque->dataEmbarque != '0000-00-00'){
            $text .= '%0A *Data Embarque:* '.$this->sma->hrsd($localEmbarque->dataEmbarque);
        }

        if ($localEmbarque->horaEmbarque != null && $localEmbarque->horaEmbarque != '00:00:00')  {
            $text .= '%0A *Hora Embarque:* '.$this->sma->hf($localEmbarque->horaEmbarque);
        }

        $text .= '%0A';

        $text .= '%0A*✔ DADOS DA RESERVA*%0A';
        $text .= '%0A *Código da Venda:* ' . $venda->reference_no;
        $text .= '%0A *Data de Emissão:* ' . $this->sma->hrld($venda->date);
        $text .= '%0A *Valor da Venda:* ' . $this->sma->formatMoney($venda->total);
        $text .= '%0A *Acres:* ' . $this->sma->formatMoney($venda->shipping);
        $text .= '%0A *Desc:* ' . $this->sma->formatMoney($venda->order_discount);
        $text .= '%0A *Subtotal:* ' . $this->sma->formatMoney($venda->grand_total);
        $text .= '%0A *Total de Passageiros:* ' . $venda->total_items . ' pessoa(s)';
        $text .= '%0A *Primeiro Vencimento:* ' . $this->sma->hrsd($venda->previsao_pagamento);
        $text .= '%0A *Situação do pagamento:* *' . lang($fatura->status).'*';
        $text .= '%0A *Forma de Pagamento:* ' . trim($tipoCobranca->name);

        if ($tipoCobranca->note) {
            $note_cobranca = $tipoCobranca->note;
            $note_cobranca = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('°', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('&', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $note_cobranca);

            $text .= '%0A *OBS:* ' . $this->decode_html($note_cobranca);
        }

        if ($tipoCobranca->tipo == 'dinheiro') {
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $text .= '%0ASe optou por deposito/transferência assim que realizar o pagamento nos envia o comprovante para confirmar a reserva.';
        }

        if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') {//boletos
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver boleto ' . $contador . 'º parcela clique aqui%0A' . urlencode($cobranca->link);
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'pix') {//pix
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver o Link de Pagamento' . $contador . 'º parcela clique aqui%0A' . urlencode($cobranca->link);
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'carne') {//carne
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                if ($contador == 1) {
                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                    $text .= '%0A Ver carnê clique aqui ' . $cobranca->link;
                }
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'cartao' || $tipoCobranca->tipo == 'carne_cartao') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A Link para pagamento do cartão de crédito%0A' . $cobranca->checkoutUrl;
        }

        if ($tipoCobranca->tipo == 'carne_cartao_transparent') {
            if ($fatura->status == 'ABERTA') {
                $text .= '%0A Situação do pagamento: Pagamento em análise juntamente com a PagSeguro. Você poderá receber um contato da equipe PagSeguro para confirmar sua compra.';
            } else if ($fatura->status == 'PAGO') {
                $text .= '%0A Situação do pagamento: Seu pagamento foi confirmado com sucesso!';
            }
        }

        if ($tipoCobranca->tipo == 'link_pagamento' || $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A Link para pagamento%0A' . $cobranca->checkoutUrl;
        }

        $text .= '%0A';
        $text .= '%0AClique no link abaixo para baixar o voucher atualizado da sua compra:%0A' . base_url() . 'appcompra/pdf/' . $venda->id . '?token=' . $this->session->userdata('cnpjempresa');

        $text .= '%0A';
        $text .= '%0AEnviamos para seu e-mail o Voucher. Se não recebeu verifique na sua caixa de spam.';

        if ($this->Settings->informacoesImportantesVoucher) {
            $info = $this->Settings->informacoesImportantesVoucher;

            $info = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('°', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('&', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $info);

            $text .= '%0A';
            $text .= '%0A*✔ INFORMAÇÕES IMPORTANTES*%0A';
            //$text .= '%0A';
            $text .= $this->decode_html($info);
            $text .= '%0A';
        }

        if ($configuracao->frase_site != '') {
            $text .= '%0A*' . $configuracao->frase_site . '*';
        }

        $text .= '%0A%0A' . date('d/m/Y H:i:s');
        $text .= '%0A*[Mensagem Automática]*';

        $text = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('°', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('&', $this->getEspacoHtmlWhatsApp(), $text);

        $text = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $text);

        //$phone = '4898235746';
        //echo $text;
        //die();

        header('Content-Type: text/html; charset=utf-8');

        header('Location: https://api.whatsapp.com/send?phone=55' . trim($phone) . '&text=' . $text);
        exit;
    }

    public function decode_html($str)
    {
        $str = str_replace('<strong>', '*', $str);
        $str = str_replace('</strong>', '*', $str);

        $str = str_replace('<p>', $this->quebraLinha(), $str);
        $str = str_replace('<br>', $this->quebraLinha(), $str);
        //$str = str_replace('-', $this->quebraLinha().'- ', $str);

        return strip_tags(html_entity_decode($str, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
    }

    function whatsapp($vendaId) {
        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);

        foreach ($faturas as $fatura) {
            $cobranca = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);
        }

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $cliente = $this->site->getCompanyByID($venda->customer_id);

        $this->abrirWhatsAppConfirmacao($vendedor, $venda, $fatura, $cobranca, $cliente->cf5);
    }



    function whatsapp1($vendaId) {
        $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($vendaId);

        foreach ($faturas as $fatura) {
            $cobranca = $this->CobrancaFaturaRepository_model->getCobrancaFaturaByFatura($fatura->id);
        }

        $venda = $this->sales_model->getInvoiceByID($vendaId);
        $vendedor = $this->site->getCompanyByID($venda->biller_id);
        $cliente = $this->site->getCompanyByID($venda->customer_id);

        $this->abrirWhatsAppConfirmacao1($vendedor, $venda, $fatura, $cobranca, $cliente->cf5);
    }

    function abrirWhatsAppConfirmacao1($vendedor, $venda, $fatura, $cobranca, $phone=null)
    {

        $configuracao = $this->site->get_setting();
        $eols = array("\n", "\r", "\r\n");

        if ($phone == null) {
            $phone = $vendedor->phone;
        }

        $cliente = $this->site->getCompanyByID($venda->customer_id);
        $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($venda->tipoCobrancaId);
        $condicaoPagamento = $this->financeiro_model->getCondicaoPagamentoById($venda->condicaopagamentoId);

        $phone = str_replace('(', '', str_replace(')', '', $phone));
        $phone = str_replace('-', '', $phone);
        $phone = str_replace(' ', '', $phone);

        $inv_items = $this->sales_model->getAllInvoiceItems($venda->id);

        foreach ($inv_items as $item) {
            $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($item->programacaoId);
            $produto = $this->site->getProductByID($programacao->produto);
            $localEmbarque = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($item->product_id, $item->localEmbarqueId);
        }

        $text = '';

        $text .= 'Olá me chamo *' . trim($cliente->name) . '*';

        if ($venda->sale_status == 'orcamento') {
            $text .= '%0AAcabo de fazer um orçamento pelo link.';
        } else if ($venda->sale_status == 'lista_espera') {
            $text .= '%0AAcabo de entrar para a lista de espera pelo link de reservas.';
        } else {
            $text .= '%0AAcabo de reservar meu pacote pelo link de reservas.';
        }

        $text .= '%0A';
        $text .= '%0AVendedor:  *' . $vendedor->name . '*';
        $text .= '%0A';

        if ($venda->total_items > 0) {
            $text .= '%0A*✔ PASSAGEIRO(S)*%0A';
            foreach ($inv_items as $item) {
                $dependente = $this->site->getCompanyByID($item->customerClient);

                if (!$item->adicional) {

                    $text .= '%0A *Nome:* ' . $dependente->name . ' *[' . trim($item->faixaNome) . ']*';
                    if ($dependente->cf5) $text .= '%0A *Celular:*  ' . $dependente->phone . ' ' . $dependente->cf5;
                    if ($dependente->email) $text .= '%0A *E-mail:* ' . $dependente->email;
                    if ($dependente->vat_no) $text .= '%0A *CPF:* ' . $dependente->vat_no;
                    if ($dependente->cf1) $text .= '%0A *Documento:* ' . strtoupper($dependente->tipo_documento) . ' ' . $dependente->cf1;

                    if ($dependente->data_aniversario) {

                        list($ano, $mes, $dia) = explode('-', $dependente->data_aniversario);

                        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                        $text .= '%0A *Nascimento:* ' . $this->sma->hrsd($dependente->data_aniversario) . ' ' . $idade . ' anos';
                    }
                    $text .= '%0A';

                } else {
                    $text .= '%0A *Adicional:* ' . $item->product_name.'%0A';
                }
            }
        }

        $text .= '%0A';

        $text .= '%0A*✔ DADOS DA VIAGEM*%0A';
        $text .= '%0A *' . $produto->name. '*';

        if ($produto->category_id != 14) {
            if ($this->Settings->ocultar_horario_saida){
                $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida) ;
            } else {
                $text .= '%0A *Data Saída:* ' . $this->sma->hrsd($programacao->dataSaida) . ' ' . $this->sma->hf($programacao->horaSaida);
            }
            $text .= '%0A *Data Retorno:* ' . $this->sma->hrsd($programacao->dataRetorno) . ' ' . $this->sma->hf($programacao->horaRetorno);
        }

        if ($localEmbarque->name) {
            $text .= '%0A *Local Embarque:* ' . $localEmbarque->name;
        }

        if ($localEmbarque->note) {
            $text .= '%0A *Nota:* '.$this->decode_html($localEmbarque->note);
        }

        if ($localEmbarque->dataEmbarque != null && $localEmbarque->dataEmbarque != '0000-00-00'){
            $text .= '%0A *Data Embarque:* '.$this->sma->hrsd($localEmbarque->dataEmbarque);
        }

        if ($localEmbarque->horaEmbarque != null && $localEmbarque->horaEmbarque != '00:00:00')  {
            $text .= '%0A *Hora Embarque:* '.$this->sma->hf($localEmbarque->horaEmbarque);
        }

        $text .= '%0A';

        $text .= '%0A*✔ DADOS DA RESERVA*%0A';
        $text .= '%0A *Código da Venda:* ' . $venda->reference_no;
        $text .= '%0A *Data de Emissão:* ' . $this->sma->hrld($venda->date);
        $text .= '%0A *Valor da Venda:* ' . $this->sma->formatMoney($venda->total);
        $text .= '%0A *Acres:* ' . $this->sma->formatMoney($venda->shipping);
        $text .= '%0A *Desc:* ' . $this->sma->formatMoney($venda->order_discount);
        $text .= '%0A *Subtotal:* ' . $this->sma->formatMoney($venda->grand_total);
        $text .= '%0A *Total de Passageiros:* ' . $venda->total_items . ' pessoa(s)';
        $text .= '%0A *Primeiro Vencimento:* ' . $this->sma->hrsd($venda->previsao_pagamento);
        $text .= '%0A *Situação do pagamento:* *' . lang($fatura->status).'*';
        $text .= '%0A *Forma de Pagamento:* ' . trim($tipoCobranca->name);

        if ($tipoCobranca->note) {
            $note_cobranca = $tipoCobranca->note;
            $note_cobranca = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('°', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace('&', $this->getEspacoHtmlWhatsApp(), $note_cobranca);
            $note_cobranca = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $note_cobranca);

            $text .= '%0A *OBS:* ' . $this->decode_html($note_cobranca);
        }

        if ($tipoCobranca->tipo == 'dinheiro') {
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $text .= '%0ASe optou por deposito/transferência assim que realizar o pagamento nos envia o comprovante para confirmar a reserva.';
        }

        if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') {//boletos
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver boleto ' . $contador . 'º parcela clique aqui%0A' . urlencode($cobranca->link);
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'pix') {//pix
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                $text .= '%0A Ver o Link de Pagamento' . $contador . 'º parcela clique aqui%0A' . urlencode($cobranca->link);
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'carne') {//carne
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A *Número de Parcelas:* ' . $condicaoPagamento->name;
            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($venda->id);

            $contador = 1;
            foreach ($faturas as $fat) {
                if ($contador == 1) {
                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
                    $text .= '%0A Ver carnê clique aqui ' . $cobranca->link;
                }
                $contador++;
            }
        }

        if ($tipoCobranca->tipo == 'cartao' || $tipoCobranca->tipo == 'carne_cartao') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A Link para pagamento do cartão de crédito%0A' . $cobranca->checkoutUrl;
        }

        if ($tipoCobranca->tipo == 'carne_cartao_transparent') {
            if ($fatura->status == 'ABERTA') {
                $text .= '%0A Situação do pagamento: Pagamento em análise juntamente com a PagSeguro. Você poderá receber um contato da equipe PagSeguro para confirmar sua compra.';
            } else if ($fatura->status == 'PAGO') {
                $text .= '%0A Situação do pagamento: Seu pagamento foi confirmado com sucesso!';
            }
        }

        if ($tipoCobranca->tipo == 'link_pagamento' || $tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago') {
            $text .= '%0A';
            $text .= '%0A*✔ DADOS DO PAGAMENTO*%0A';
            $text .= '%0A Link para pagamento%0A' . $cobranca->checkoutUrl;
        }

        $text .= '%0A';
        $text .= '%0AClique no link abaixo para baixar o voucher atualizado da sua compra:%0A' . base_url() . 'appcompra/pdf/' . $venda->id . '?token=' . $this->session->userdata('cnpjempresa');

        $text .= '%0A';
        $text .= '%0AEnviamos para seu e-mail o Voucher. Se não recebeu verifique na sua caixa de spam.';

        if ($this->Settings->informacoesImportantesVoucher) {
            $info = $this->Settings->informacoesImportantesVoucher;

            $info = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('°', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace('&', $this->getEspacoHtmlWhatsApp(), $info);
            $info = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $info);

            $text .= '%0A';
            $text .= '%0A*✔ INFORMAÇÕES IMPORTANTES*%0A';
            //$text .= '%0A';
            $text .= $this->decode_html($info);
            $text .= '%0A';
        }

        if ($configuracao->frase_site != '') {
            $text .= '%0A*' . $configuracao->frase_site . '*';
        }

        $text .= '%0A%0A' . date('d/m/Y H:i:s');
        $text .= '%0A*[Mensagem Automática]*';

        $text = str_replace(' ', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('°', $this->getEspacoHtmlWhatsApp(), $text);
        $text = str_replace('&', $this->getEspacoHtmlWhatsApp(), $text);

        $text = str_replace($eols, $this->getEspacoHtmlWhatsApp(), $text);

        //$phone = '4898235746';
        //echo $text;
        //die();

        header('Content-Type: text/html; charset=utf-8');

        echo $text;

        //header('Location: https://api.whatsapp.com/send?phone=55' . trim($phone) . '&text=' . $text);
        exit;
    }


    private function getEspacoHtmlWhatsApp()
    {
        return '%20';
    }

    private function quebraLinha() {
        return '%0A';
    }

    public function condicoes()
    {
        $produtoID              = $this->input->get('produtoID');
        $istaxasComissaoAtivo   = $this->input->get('istaxasComissaoAtivo');
        $tipoCobrancaId         = $this->input->get('tipoCobrancaId');
        $tipo                   = $this->input->get('tipo');
        $dataSaida              = $this->input->get('data_Saida');

        if ($istaxasComissaoAtivo) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, FALSE , $produtoID);
        } else {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId);
        }

        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento   = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento                = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        $dataMaximaParaRealizarPagamento    = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida);

        $dtVencimento   = null;
        $maximoParcelas = 0;

        for ($i = 0; $i < 25; $i++) {
            $dtVencimento = $this->getDataProximoVencimentoCondicoes($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo);
            if ($dtVencimento != $dataMaximaParaRealizarPagamento && strtotime($dtVencimento) < strtotime($dataMaximaParaRealizarPagamento)) {
                $maximoParcelas++;
            } else {
                if (date('Y-m', strtotime($dtVencimento)) == date('Y-m', strtotime($dataMaximaParaRealizarPagamento))) {
                    $maximoParcelas++;
                }
            }
        }

        if ($maximoParcelas == 0)  $maximoParcelas = 1;

        if ($tipo == 'carne_cartao' || $tipo == 'cartao' || $i == 0) {
            if ($istaxasComissaoAtivo) {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, NULL, $produtoID);
            } else {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId);
            }
        } else {
            if ($istaxasComissaoAtivo) {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcelas, $produtoID);
            } else {
                $condicoesPagamento = $this->settings_model->getAllCondicoesPagamentoByTipoCobranca($tipoCobrancaId, $maximoParcelas);
            }
        }

        $this->sma->send_json($condicoesPagamento);
    }

    public function getParcelamento()
    {

        $produtoID              = $this->input->get('produtoID');
        $condicaoPagamentoId    = $this->input->get('condicaoPagamentoId');
        $tipoCobrancaId         = $this->input->get('tipoCobrancaId');
        $subTotal               = $this->input->get('subTotal');
        $tipo                   = $this->input->get('tipo');
        $conta                  = $this->input->get('conta');
        $dataSaida              = $this->input->get('data_Saida');
        $istaxasComissaoAtivo   = $this->input->get('istaxasComissaoAtivo');

        $condicaoPagamento      = $this->financeiro_model->getCondicaoPagamentoById($condicaoPagamentoId);

        $totalParcelas = $condicaoPagamento != null ? $condicaoPagamento->parcelas : 1;

        if ($istaxasComissaoAtivo) {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId, $produtoID);
        } else {
            $taxas = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobrancaId, $condicaoPagamentoId);
        }

        $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
        $diasAvancaPrimeiroVencimento   = $taxas->diasAvancaPrimeiroVencimento;

        $dtMaximaDePagamento = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        $dataMaximaParaRealizarPagamento = $this->getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida);

        $memorizaPrimeiroVencimento = null;
        $dtVencimento = null;
        $html = '';

        for ($i = 0; $i < $totalParcelas; $i++) {

            $readonly = '';

            $dtVencimento = $this->getDataProximoVencimento($dtVencimento, $i, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo);

            if ($i == 0) {
                $memorizaPrimeiroVencimento = $dtVencimento;
                $readonly = 'readonly="readonly"';
            }

            if ($tipo == 'carne') {
                $readonly = 'readonly="readonly"';
            }

            $html .= '
               <div class="row">
                    <div class="col-1" style="line-height: 35px;">' . ($i + 1) . 'X</div>
                    <div class="col-7"><input type="date" original="' . $dtVencimento . '" diasAntesDaViagem="' . $diasMaximoPagamentoAntesViagem . '" dataMaximaPagamento="' . $dataMaximaParaRealizarPagamento . '" class="form-control" value="' . $dtVencimento . '" name="dtVencimentos[]" ' . $readonly . ' required="required"></div>
                    <div class="col-4" style="line-height: 35px;">
                        <input type="hidden" value="' . $subTotal / $totalParcelas . '" name="valorVencimentos[]">
                        <input type="hidden" value="' . $tipoCobrancaId . '" name="tiposCobranca[]">
                        <input type="hidden" value="' . $conta . '" name="movimentadores[]">
                        <input type="hidden" value="0" name="descontos[]">
                        ' . $this->sma->formatMoney($subTotal / $totalParcelas) . '
                    </div>
              </div>';
        }

        $html = $this->getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem, $dataSaida, $totalParcelas);

        echo $html;
    }

    public function getDataProximoVencimentoCondicoes($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo)
    {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+" . $diasAvancaPrimeiroVencimento . " day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento


            if ($tipo != 'carne') {
                //se nao for carne os proximos vencimentos volta para o dia da compra
                $dtProximoVencimento = date('Y-m-d', strtotime("-" . $diasAvancaPrimeiroVencimento . " day", strtotime($dtProximoVencimento)));//tira obriacao do dia minimo
            }

        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }
        return $dtProximoVencimento;
    }

    public function getDataProximoVencimento($dtVencimento, $contador, $diasAvancaPrimeiroVencimento, $dtMaximaDePagamento, $tipo)
    {

        if ($dtVencimento == null && $contador == 0) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+" . $diasAvancaPrimeiroVencimento . " day", strtotime(date('Y-m-d'))));
        } else if ($contador == 1) {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));//todo avanca vencimento

            if ($tipo != 'carne') {
                //se nao for carne os proximos vencimentos volta para o dia da compra
                $dtProximoVencimento = date('Y-m-d', strtotime("-" . $diasAvancaPrimeiroVencimento . " day", strtotime($dtProximoVencimento)));//tira obriacao do dia minimo
            }

        } else {
            $dtProximoVencimento = date('Y-m-d', strtotime("+1 month", strtotime($dtVencimento)));
        }

        //Se a data maxima para pagar ja passou entao atribuir a proxima data
        if (strtotime($dtMaximaDePagamento) < strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = $dtMaximaDePagamento;
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtProximoVencimento)) {
            $dtProximoVencimento = date('Y-m-d');
        }

        return $dtProximoVencimento;
    }

    private function getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $dataSaida)
    {

        if ($diasMaximoPagamentoAntesViagem > 0) {
            $dtMaximaDePagamento = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));
        } else {
            $dtMaximaDePagamento = $dataSaida;//data da saida da viagem
        }

        //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
        if (strtotime(date('Y-m-d')) > strtotime($dtMaximaDePagamento)) {
            $dtMaximaDePagamento = date('Y-m-d');
        }

        return $dtMaximaDePagamento;
    }

    private function getTextoDiasMaximoPagamentoAntesDaViagem($html, $memorizaPrimeiroVencimento, $diasMaximoPagamentoAntesViagem, $dataSaida, $totalParcelas)
    {

        $html .= '<div class="row" style="margin-top: 10px;">';
        $html .= '<div class="col-12" style="color: #007bff;">';

        if ($diasMaximoPagamentoAntesViagem > 0) {

            $dtMaximaDePagamento = date('Y-m-d', strtotime("-" . $diasMaximoPagamentoAntesViagem . " day", strtotime($dataSaida)));

            if (strtotime(date('Y-m-d')) < strtotime($dtMaximaDePagamento)) {

                if ($totalParcelas > 1) {
                    $html .= '<b>*Leia-me:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                    $html .= '<b>*Leia-me: Apenas</b> após o pagamento da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';

                    $html .= '<br/><br/><b>*Leia-me: </b>No caso de parcelamento o pagamento total deverá ser <b>quitado até ' . $diasMaximoPagamentoAntesViagem . ' dias antes da viagem</b>. Data máxima para pagamento <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>';
                } else {
                    $html .= '<b>*Leia-me: Apenas</b> após o pagamento da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
                }
            } else {
                $html .= '<br/><br/><b>*Leia-me: </b>A viagem saíra ainda esta semana e a <b>quitação total do débito deverá ser feita ainda hoje para confirmação da compra.</b> Data máxima para pagamento <b>' . date('d/m/Y', strtotime($dtMaximaDePagamento)) . '</b>';
            }

        } else {
            if ($totalParcelas > 1) {
                $html .= '<b>*Leia-me:</b> Ajuste os vencimentos das parcelas de acordo com a necessidade.<br/><br/>';

                $html .= '<b>Apenas após o pagamento</b> da 1º parcela ou sinal com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
            } else {
                $html .= '<b>*Leia-me: Apenas</b> após o pagamento da parcela com vencimento para <b>' . date('d/m/Y', strtotime($memorizaPrimeiroVencimento)) . '</b> sua reserva <b>será  confirmada.</b>';
            }
        }

        $html .= '</div>';
        $html .= '</div>';
        return $html;
    }

    public function vagas()
    {
        $programacaoId               = $this->input->get('programacaoId');
        $tipo_hospedagem             = $this->input->get('tipo_hospedagem');
        $controle_estoque_hospedagem = $this->input->get('controle_estoque_hospedagem') == '1';

        echo $this->AgendaViagemService_model->vagas($programacaoId, $controle_estoque_hospedagem, $tipo_hospedagem);
    }

    function getVerificaCustomeByCPF(){

        $cpf                    =  $this->input->get('cpf');
        $permiteDuplicidade     = $this->input->get('permite_duplicidade');
        $programacaoID          = $this->input->get('programacaoId');
        $encontrouDuplicidade   = FALSE;

        if (!$permiteDuplicidade) {
            $encontrouDuplicidade = $this->sales_model->verificaClienteVenda($cpf, $programacaoID);
        }

        $pessoa = $this->site->getVerificaCPF($cpf);

        $this->sma->send_json(array('pessoa' => $pessoa, 'verifica_duplicidade' => $encontrouDuplicidade));
    }

    public function validar_cupom()
    {
        $cupom = $this->input->get('cupom', false);
        $programacao_id = $this->input->get('programacao_id');

        $cupom = urldecode($cupom);

        $this->sma->send_json($this->CupomDescontoRepository_model->validar($programacao_id, $cupom));
    }

    public function simulation_installments_valepay() {

        $valePaySettings = $this->settings_model->getValepaySettings();

        if ($valePaySettings->active) {

            $valepay              = new Valepay();
            $valepay->sandbox     = $valePaySettings->sandbox;
            $valepay->key_public  = $valePaySettings->public_key;
            $valepay->key_private = $valePaySettings->private_key;

            $valor_total_taxas    = $this->input->get('valor_total_taxas');
            $tipo_cobranca        = $this->input->get('tipo_cobranca');
            $produtoID            = $this->input->get('produtoID');
            $istaxasComissaoAtivo = $this->input->get('istaxasComissaoAtivo');

            if ($istaxasComissaoAtivo) {
                $configurcaoTaxa      = $this->settings_model->getTaxaConfiguracaoSettings($tipo_cobranca, NULL, $produtoID);
            } else {
                $configurcaoTaxa      = $this->settings_model->getTaxaConfiguracaoSettings($tipo_cobranca);
            }

            $numeroMaxParcelas              = $configurcaoTaxa->numero_max_parcelas;
            $numero_max_parcelas_sem_juros  = $configurcaoTaxa->numero_max_parcelas_sem_juros;

            $emmiter = array(
                'amount'            => $valor_total_taxas,
                'markup_type'       => '1',//1 - Multiplo 2-Divisor
                'markup'            => '0',//Porcentagem do markup
                'calculate_markup'  => FALSE,
                'entry_money'       => 0,//valor da entrada
            );

            $rav = array(
                'amount'            => $valor_total_taxas,
                'entry_money'       => 0,//valor da entrada
            );

            $simulacoes = $valepay->simulation_installments_by_customer($emmiter);

            if ($this->validate_transaction_simulation_installments_valepay($simulacoes)) {

                if ($numero_max_parcelas_sem_juros > 0) {
                    $simulacoes_rav = $valepay->simulation_rav($rav);

                    $contador = 0;
                    foreach ($simulacoes_rav->data as $simulacao_rav) {
                        if ($simulacao_rav->parcela <= $numero_max_parcelas_sem_juros) {
                            $simulacoes->data[$contador] =  $simulacoes_rav->data[$contador];
                            $simulacoes->data[$contador]->sem_juros = true;
                        }
                        $contador++;
                    }
                }

                $contador = 0;
                foreach ($simulacoes->data as $simulacao) {
                    $parcela = $simulacao->parcela;
                    if ($parcela > $numeroMaxParcelas) {
                        unset($simulacoes->data[$contador]);
                    }

                    $contador++;
                }

                $this->sma->send_json($simulacoes);
            } else {
                $this->sma->send_json([]);
            }

        } else {
            $this->sma->send_json([]);
        }
    }

    public function validate_transaction_simulation_installments_valepay($transaction) {

        $response = new stdClass();

        $error = false;
        $status = false;

        if ($transaction->status == 'error') {
            if(!empty($transaction->message)) $error = $transaction->message;
        } else if ($transaction->status == 'success' && !empty($transaction->data)) {
            $status = true;
        }

        $response->status = $status;
        $response->error = $error;

        return $response;
    }

    public function show_page_error($title = 'Página Não Encontrada', $error = '') {

        $this->data['title']      = $title;
        $this->data['error']      = $error;
        $this->data['urlLink']    = $this->Settings->url_site_domain;

        echo $this->load->view($this->theme . 'website/error/page_error', $this->data, true);

        exit(4); // EXIT_UNKNOWN_FILE
    }

    private function getAtividadesRelacionadas($category_id)
    {

        $filter = new ProgramacaoFilter_DTO_model();
        $filter->category = $category_id;
        $filter->enviar_site = false;
        $filter->limit = 8;
        $filter->order_by_custom = 'RAND()';
        $filter->group_data_website = $this->Settings->receptive;//TODO Configurado para receptivo

        return $this->AgendaViagemService_model->getAllProgramacao($filter);

    }
}