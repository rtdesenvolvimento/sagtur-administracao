<?php defined('BASEPATH') or exit('No direct script access allowed');

class Sales extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->lang->load('sales', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->model('sales_model');
        $this->load->model('products_model');
        $this->load->model('companies_model');
        $this->load->model('settings_model');
        $this->load->model('integracaositetrendytravel_model');
        $this->load->model('boletofacil_model');
        $this->load->model('destino_model');

        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '16384';

        $this->data['logo'] = true;
    }

    public function index($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        if ($warehouse_id &&
            $warehouse_id  != 'all' &&
            ($this->session->userdata('warehouse_id') != $warehouse_id)) {

            if (is_numeric ( $warehouse_id )) {
                $this->session->set_userdata('warehouse_id', $warehouse_id);
                $warehouse_id = $this->session->userdata('warehouse_id');
            } else {
                $this->session->set_userdata('warehouse_id', null);
                $warehouse_id = $this->session->userdata('warehouse_id');
            }
        }

        if ($this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] =  $this->site->getAllWarehouses();;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/index', $meta, $this->data);
    }

    public function salesFilter($warehouse_id = null, $unit = NULL)
    {

        if ($warehouse_id &&
            $warehouse_id  != 'all' &&
            ($this->session->userdata('warehouse_id') != $warehouse_id)) {

            if (is_numeric ( $warehouse_id )) {
                $this->session->set_userdata('warehouse_id', $warehouse_id);
                $warehouse_id = $this->session->userdata('warehouse_id');
            } else {
                $this->session->set_userdata('warehouse_id', null);
                $warehouse_id = $this->session->userdata('warehouse_id');
            }
        }
        if ($this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses($unit);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] =  $this->site->getAllWarehouses();;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        if ($unit)$this->data['unit'] = $unit;
        else $this->data['unit'] = null;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('sales')));
        $meta = array('page_title' => lang('sales'), 'bc' => $bc);
        $this->page_construct('sales/index', $meta, $this->data);
    }

    public function getSales($warehouse_id = null, $unit = NULL)
    {
        if ($warehouse_id == 'all') {
            $warehouse_id = NULL;
        }

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        if ($warehouse_id &&
            $warehouse_id  != 'all' &&
            ($this->session->userdata('warehouse_id') != $warehouse_id)) {

            if (is_numeric ( $warehouse_id )) {
                $this->session->set_userdata('warehouse_id', $warehouse_id);
                $warehouse_id = $this->session->userdata('warehouse_id');
            } else {
                $this->session->set_userdata('warehouse_id', null);
                $warehouse_id = $this->session->userdata('warehouse_id');
            }
        }

        if ($this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }

        $contrato_link = anchor('sales/emitir_contrato/$1', '<i class="fa fa-book"></i> ' . lang('baixar_contrato'));
        $detail_link = anchor('sales/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $payments_link = anchor('sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('sales/add_payment/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), 'data-toggle="modal" data-target="#myModal"');
        //$add_delivery_link = anchor('sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('sales/email/$1', '<i class="fa fa-envelope"></i> ' . lang('email_sale'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('sales/pdf/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        //$return_link = anchor('sales/return_sale/$1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_sale") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('sales/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i></i> "
            . lang('delete_sale') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
        <ul class="dropdown-menu pull-right" role="menu">
            <li>' . $detail_link . '</li>
            <li>' . $contrato_link . '</li>
            <li>' . $payments_link . '</li>
            <li>' . $add_payment_link . '</li>
            <li>' . $edit_link . '</li>
            <li>' . $pdf_link . '</li>
            <li>' . $email_link . '</li>
            <li>' . $delete_link . '</li>
        </ul>
    </div></div>';
        //$action = '<div class="text-center">' . $detail_link . ' ' . $edit_link . ' ' . $email_link . ' ' . $delete_link . '</div>';
//            <li>' . $add_delivery_link . '</li>

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("id, date, reference_no, CONCAT( vendedor )  , customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status")
                ->from('sales')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("id, date, reference_no, CONCAT(  vendedor ) , customer, sale_status, grand_total, paid, (grand_total-paid) as balance, payment_status")
                ->from('sales');
        }

        $this->datatables->where('pos !=', 1);
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function getFilial() {
        echo 'filial= '.$this->session->userdata('warehouse_id');
    }
    public function return_sales($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $user->warehouse_id;
            $this->data['warehouse'] = $user->warehouse_id ? $this->site->getWarehouseByID($user->warehouse_id) : null;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('return_sales')));
        $meta = array('page_title' => lang('return_sales'), 'bc' => $bc);
        $this->page_construct('sales/return_sales', $meta, $this->data);
    }

    public function getReturns($warehouse_id = null)
    {
        $this->sma->checkPermissions('return_sales', true);

        if (!$this->Owner && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("date, return_sale_ref, reference_no, biller, customer, surcharge, grand_total, id")
                ->from('sales')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("date, return_sale_ref, reference_no, biller, customer, surcharge, grand_total, id")
                ->from('sales');
        }
        $this->datatables->where('sale_status', 'returned');

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('customer_id'));
        }

        echo $this->datatables->generate();
    }

    public function modal_view($id = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : NULL;

        $this->load->view($this->theme . 'sales/modal_view', $this->data);
    }

    public function view($id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $return = $this->sales_model->getReturnBySID($id);
        $this->data['return_sale'] = $return;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['paypal'] = $this->sales_model->getPaypalSettings();
        $this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->page_construct('sales/view', $meta, $this->data);
    }

    public function view_return($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getReturnByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllReturnItems($id);
        $this->data['sale'] = $this->sales_model->getInvoiceByID($inv->sale_id);
        $this->load->view($this->theme.'sales/view_return', $this->data);
    }

    public function montarHotel($id = null, $idVariante, $contadorHotel) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $hoteis = $this->sales_model->getProductOptions_Hotel($id, $idVariante);

        $this->data['inv']                  = $this->sales_model->getInvoiceByID($id);
        $this->data['itens_pedidos']        = $this->sales_model->getInvoiceByProduct($id);
        $this->data['itens_pedidos_group']  = $this->sales_model->getInvoiceByProductGroupBySale($id);
        $this->data['hotel']                = $hoteis[0];
        $this->data['idVariante']           = $idVariante;
        $this->data['product_id']           = $id;
        $this->data['contadorHotel']        = $contadorHotel;

        // print_r($hoteis[0]->fornecedor);
        //die();

        $meta = array('page_title' => lang('sales'), 'bc' => '');
        $this->page_construct('sales/montarHotel', $meta, $this->data);
    }

    public function montarPoltronas($id = null, $idVariante) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $onibus = $this->sales_model->getProductOptions_Onibus($id, $idVariante);

        $this->data['inv']              = $this->sales_model->getInvoiceByID($id);
        $this->data['itens_pedidos']    = $this->sales_model->getInvoiceByProductOrderByPoltrona($id);
        $this->data['onibus']           = $onibus[0];
        $this->data['idVariante']       = $idVariante;
        $this->data['product_id']       = $id;

        $meta = array('page_title' => lang('sales'), 'bc' => '');
        $this->page_construct('sales/montarPoltronas', $meta, $this->data);
    }

    public function verificaPoltrona($product_id,$poltrona,$variacao) {
        $iten = $this->sales_model->getSaleByItensByPoltrona($product_id, $poltrona, $variacao);
        echo json_encode($iten);
    }

    public function atribuir_poltrona($id_item, $idVariante, $poltrona, $sale_id) {
        $this->sales_model->atualizarPoltrona($poltrona, $idVariante, $id_item, $sale_id);
    }

    public function excluir_hotel($sale_id, $ordem) {
        $this->sales_model->atualizarHotel($sale_id, $ordem);
    }

    public function  salvar_hotel() {

        $sale_id            = $this->input->get('sale_id');
        $idVariante         = $this->input->get('idVariante');
        $product_id         = $this->input->get('product_id');
        $fornecedor_id      = $this->input->get('fornecedor_id');
        $tipo_quarto        = $this->input->get('tipo_quarto');
        $contadorHotel      = $this->input->get('contadorHotel');

        $customer_1_id 		= $this->input->get('customer_1');
        $customer_2_id 		= $this->input->get('customer_2');
        $customer_3_id 		= $this->input->get('customer_3');
        $customer_4_id 		= $this->input->get('customer_4');
        $customer_5_id 		= $this->input->get('customer_5');

        $customer_1			= null;
        $customer_2			= null;
        $customer_3			= null;
        $customer_4			= null;
        $customer_5			= null;

        if ($customer_1_id) {
            $customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
            $customer_1 			= $customer_details_1->name;
        }

        if ($customer_2_id) {
            $customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
            $customer_2 			= $customer_details_2->name;
        }

        if ($customer_3_id) {
            $customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
            $customer_3 			= $customer_details_3->name;
        }

        if ($customer_4_id) {
            $customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
            $customer_4 			= $customer_details_4->name;
        }

        if ($customer_5_id) {
            $customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
            $customer_5 			= $customer_details_5->name;
        }


        if ($contadorHotel > 0) {
            $numeroHotel = 'hotel'.$contadorHotel;
        } else {
            $numeroHotel = 'hotel';
        }
        $data = array(
            'customer_1_id' 		=> $customer_1_id,
            'customer_2_id' 		=> $customer_2_id,
            'customer_3_id' 		=> $customer_3_id,
            'customer_4_id' 		=> $customer_4_id,
            'customer_5_id' 		=> $customer_5_id,
            'customer_1' 			=> $customer_1,
            'customer_2' 			=> $customer_2,
            'customer_3' 			=> $customer_3,
            'customer_4' 			=> $customer_4,
            'customer_5' 			=> $customer_5,
            $numeroHotel 	        => $fornecedor_id,
            'tipo_quarto' 		    => $tipo_quarto,

        );

        $this->sales_model->atualizarVendaHotel($sale_id, $data);

        redirect('sales/montarHotel/'.$product_id.'/'.$idVariante.'/'.$contadorHotel);

    }

    public function meuticket($id = null, $customer = null ){
        echo $this->sales_model->buscarValcherEmFormaDeImagem($id, $customer);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        if ($this->input->get('id')) $id = $this->input->get('id');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);

        if (!$this->session->userdata('view_right')) {
            //  $this->sma->view_rights($inv->created_by);
        }

        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $return = $this->sales_model->getReturnBySID($id);
        $this->data['return_sale'] = $return;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;
        //$this->data['paypal'] = $this->sales_model->getPaypalSettings();
        //$this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf', $this->data, true);

        if ($view) {
            $this->load->view($this->theme . 'sales/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

    function pdf_hotel($id = NULL , $idVariante = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductGroupBySale($id);
        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $hoteis = $this->sales_model->getProductOptions_Hotel($id, $idVariante);

        $name = "lista_hospedagem.pdf";
        $this->data['itens_pedidos']    = $itens_pedidos;
        $this->data['listHoteis']       = $hoteis;

        $html = $this->load->view($this->theme . 'products/pdf_hotel', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

    function relatorioEmbarqueAgrupadoPorLocalDeEmbarque($produtoId = NULL, $variacaoId = NULL)
    {
        $locaisDeEmbarque = $this->sales_model->buscarLocaisDeEmbarquePorViagem($produtoId);

        if (!$produtoId || !$locaisDeEmbarque) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $onibus = $this->sales_model->getProductOptions_Onibus($produtoId, $variacaoId);

        $name = "relatorio_enviado_empresa_onibus.pdf";
        $this->data['locaisDeEmbarque'] = $locaisDeEmbarque;
        $this->data['listOnibus'] = $onibus;
        $this->data['product'] = $this->site->getProductByID($produtoId);

        $html = $this->load->view($this->theme . 'products/relatorioEmbarqueAgrupadoPorLocalEmbarque', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

    function relatorio_enviado_empresa_onibus($id = NULL, $idVariacao = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductGroupByLocalEmbarque($id);
        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $onibus = $this->sales_model->getProductOptions_Onibus($id, $idVariacao);

        $name = "relatorio_enviado_empresa_onibus.pdf";
        $this->data['itens_pedidos_groupBy'] = $itens_pedidos;
        $this->data['listOnibus'] = $onibus;
        $this->data['product'] = $this->site->getProductByID($id);

        $html = $this->load->view($this->theme . 'products/relatorio_enviado_empresa_onibus', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }


    function relatorio_geral_passageiros($id = NULL )
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductOrderByCliente($id);
        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $name = "relatorio_geral_passageiros.pdf";
        $this->data['itens_pedidos_groupBy'] = $itens_pedidos;
        $this->data['product'] = $this->site->getProductByID($id);

        $html = $this->load->view($this->theme . 'products/relatorio_geral_passageiros', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

    function relatorio_seguradora_de_passageiros($id = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductOrderByCliente($id);

        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $name = "relatorio_seguradora_de_passageiros.pdf";
        $this->data['itens_pedidos_groupBy'] = $itens_pedidos;
        $this->data['product'] = $this->site->getProductByID($id);

        $html = $this->load->view($this->theme . 'products/relatorio_seguradora_de_passageiros', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

    function relatorio_embarque($id = NULL, $idVariacao = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProduct($id);
        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }


        $onibus = $this->sales_model->getProductOptions_Onibus($id, $idVariacao);

        $this->data['listOnibus'] = $onibus;
        $this->data['product_id'] = $id;
        $this->data['product'] = $this->site->getProductByID($id);

        $name = "relatorio_embarque.pdf";

        $html = $this->load->view($this->theme . 'products/relatorio_embarque', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }


    function relatorio_embarque_ordem_alfabetica($id = NULL, $idVariacao = NULL)
    {
        $itens_pedidos = $this->sales_model->getInvoiceByProductOrderByCliente($id);

        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $name = "relatorio_embarque.pdf";

        $onibus = $this->sales_model->getProductOptions_Onibus($id, $idVariacao);

        $this->data['listOnibus'] = $onibus;
        $this->data['product_id'] = $id;
        $this->data['itens_pedidos'] = $itens_pedidos;
        $this->data['product'] = $this->site->getProductByID($id);

        $html = $this->load->view($this->theme . 'products/relatorio_embarque_ordem_alfabetica', $this->data, TRUE);
        $this->sma->generate_pdf($html, $name);
    }

    function pdf_aduana($id = NULL)
    {

        ini_set('max_execution_time', 300); //300 seconds = 5 minutes
        ini_set('memory_limit', '-1');

        $itens_pedidos = $this->sales_model->getInvoiceByProduct($id);
        if (!$id || !$itens_pedidos) {
            $this->session->set_flashdata('error', lang('viagem_item_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $name = "relatorio_internacional_aduana.pdf";

        $this->data['itens_pedidos'] = $itens_pedidos;
        $html = $this->load->view($this->theme . 'sales/aduana', $this->data, TRUE);

        //$this->sma->generate_pdf($html, $name. date('d/m/y'), TRUE, 'A4-L');
        $this->sma->generate_pdf($html, $name, $output_type = null, $footer = null, $margin_bottom = null, $header = null, $margin_top = null, 'A4-L');

    }

    public function emitir_contrato($sales_id) {

        $this->load->helper('margedocx');

        setlocale( LC_ALL, 'pt_BR', 'pt_BR.iso-8859-1', 'pt_BR.utf-8', 'portuguese' );
        date_default_timezone_set('Europe/Lisbon');
        $mes =  utf8_encode(gmstrftime('%B'));

        $inv_items      = $this->sales_model->getAllInvoiceItems($sales_id);
        $sale           = $this->sales_model->getInvoiceByID($sales_id);
        $pagamentos     = $this->sales_model->getInvoicePayments($sales_id);
        $objCustomer    = $this->site->getCompanyByID($sale->customer_id);

        $nomeHotel = '';
        if ($sale->hotel) {
            $hotel          = $this->site->getCompanyByID($sale->hotel);
            $nomeHotel      = $hotel->company;
        }

        $arquivo_entrada = 'files/contrato/contrato'.count($inv_items).'passageiro.docx';
        $arquivo_saida = 'filies/contrato/emissao/saida.docx';

        $contadorPassagem = 1;

        $passageiro1 = array();
        $passageiro2 = array();
        $passageiro3 = array();
        $passageiro4 = array();
        $passageiro5 = array();
        $passageiro6 = array();
        $passageiro7 = array();
        $passageiro8 = array();
        $passageiro9 = array();
        $passageiro10 = array();
        $passageiro11 = array();
        $passageiro12 = array();
        $passageiro13 = array();
        $passageiro14 = array();
        $passageiro15 = array();

        $allDepedentes = '';

        foreach ($inv_items as $item) {

            $product = $this->site->getProductByID($item->product_id);

            if ($sale->customer_id != $item->customerClient) {
                $cliente = $this->site->getCompanyByID($item->customerClient);
                $name = $cliente->name;
                $rg = $cliente->cf1;
                $orgaoEmissorRG = $cliente->cf3;
                $data_aniversario = $cliente->data_aniversario;
                $telefone = $cliente->phone.' '.$cliente->cf5;
                $cpf = $cliente->vat_no;

                $allDepedentes .= 'DEPENDENTE: '.$name.'</w:t><w:br/><w:t>';
                $allDepedentes .= 'RG: '.$rg.'/'.$orgaoEmissorRG.'</w:t><w:br/><w:t>';
                $allDepedentes .= 'CPF: '.$cpf.'</w:t><w:br/><w:t>';
                if ($data_aniversario) $allDepedentes .= 'DATA DE NASCIMENTO: '.date(('d/m/Y'), strtotime($data_aniversario)).'</w:t><w:br/><w:t>';
                $allDepedentes .= 'TELEFONE: '.$telefone.'</w:t><w:br/><w:t></w:t><w:br/><w:t>';
            }

            if ($contadorPassagem  == 1) $passageiro1 = $this->imprimirPassageiroContratoEmissao($item, 1);
            if ($contadorPassagem  == 2) $passageiro2 = $this->imprimirPassageiroContratoEmissao($item, 2);
            if ($contadorPassagem  == 3) $passageiro3 = $this->imprimirPassageiroContratoEmissao($item, 3);
            if ($contadorPassagem  == 4) $passageiro4 = $this->imprimirPassageiroContratoEmissao($item, 4);
            if ($contadorPassagem  == 5) $passageiro5 = $this->imprimirPassageiroContratoEmissao($item, 5);
            if ($contadorPassagem  == 6) $passageiro6 = $this->imprimirPassageiroContratoEmissao($item, 6);
            if ($contadorPassagem  == 7) $passageiro7 = $this->imprimirPassageiroContratoEmissao($item, 7);
            if ($contadorPassagem  == 8) $passageiro8 = $this->imprimirPassageiroContratoEmissao($item, 8);
            if ($contadorPassagem  == 9) $passageiro9 = $this->imprimirPassageiroContratoEmissao($item, 9);
            if ($contadorPassagem  == 10) $passageiro10 =$this->imprimirPassageiroContratoEmissao($item, 10);
            if ($contadorPassagem  == 11) $passageiro11 =$this->imprimirPassageiroContratoEmissao($item, 11);
            if ($contadorPassagem  == 12) $passageiro12 =$this->imprimirPassageiroContratoEmissao($item, 12);
            if ($contadorPassagem  == 13) $passageiro13 =$this->imprimirPassageiroContratoEmissao($item, 13);
            if ($contadorPassagem  == 14) $passageiro14 =$this->imprimirPassageiroContratoEmissao($item, 14);
            if ($contadorPassagem  == 15) $passageiro15 =$this->imprimirPassageiroContratoEmissao($item, 15);

            $contadorPassagem = $contadorPassagem + 1;
        }

        $name               = $objCustomer->name;
        $cpf                = $objCustomer->vat_no;
        $endereco           = $objCustomer->address;

        $sexo               = $objCustomer->sexo;
        $cumprimento        = '';

        if ($sexo == 'FEMININO')  $cumprimento = 'Srª';
        else $cumprimento = 'Srº';

        $bairro             = '';
        $cidade             = $objCustomer->city;
        $email              = $objCustomer->email;
        $data_aniversario   = $objCustomer->data_aniversario;
        $rg                 = $objCustomer->cf1;
        $cep                = $objCustomer->postal_code;
        $estado             = $objCustomer->state;
        $telefone           = $objCustomer->phone;
        $celular            = $objCustomer->cf5;
        $apenasTelefone     = $objCustomer->phone;
        $telefone_emergencia= $objCustomer->telefone_emergencia;
        $orgaoEmissorRG     = $objCustomer->cf3;
        $tipoDocumento      = $objCustomer->tipo_documento;
        $country            = $objCustomer->country;

        $allDepedentes .= 'RESPONSÁVEL: '.$name.'</w:t><w:br/><w:t>';
        $allDepedentes .= 'RG: '.$rg.'/'.$orgaoEmissorRG.'</w:t><w:br/><w:t>';
        $allDepedentes .= 'CPF: '.$cpf.'</w:t><w:br/><w:t>';

        if ($data_aniversario) $allDepedentes .= 'DATA DE NASCIMENTO: '.date(('d/m/Y'), strtotime($data_aniversario)).'</w:t><w:br/><w:t>';
        if ($telefone) $allDepedentes .= 'TELEFONE: '.$telefone.'</w:t><w:br/><w:t>';
        if ($celular) $allDepedentes .= 'CELULAR: '.$celular.'</w:t><w:br/><w:t>';

        if ($objCustomer->cf5 != '') $telefone = $telefone.' - '.$objCustomer->cf5;

        $data_chegada       = $product->data_chegada;
        $hora_chegada       = $product->hora_chegada;
        $data_saida         = $product->data_saida;
        $hora_saida         = $product->hora_saida;
        $data_retorno       = $product->data_retorno;
        $hora_retorno       = $product->hora_retorno;
        $preco_pacote       = number_format($product->price, 2, ',', ' ');

        $local_retorno      = $product->local_retorno;
        $locais_embarque    = $product->origem;
        $locais_embarque    = strip_tags($locais_embarque);

        $detalhesDaViagem   = $product->product_details;
        $itinerario         = $product->itinerario;
        $oQueInclui         = $product->oqueInclui;
        $details            = $product->details;

        $oqueInclui         = $this->formatarOQueInclui($detalhesDaViagem);
        $oqueInclui         .= $this->formatarOQueInclui($itinerario);
        $oqueInclui         .= $this->formatarOQueInclui($oQueInclui);
        $oqueInclui         .= $this->formatarOQueInclui($details);

        $tempo_viagem       = $product->tempo_viagem;
        $destino            = strip_tags($product->destino);

        $local_saida        = $sale->local_saida;
        $tipo_quarto        = $sale->tipo_quarto;
        $subtotal           = $sale->grand_total;
        $subtotalReais      = $sale->grand_total;

        $nomeViagem         = $product->name;

        setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
        date_default_timezone_set('America/Sao_Paulo');
        $data_saida_extenso =  utf8_encode(strftime('%d de %B de %Y',strtotime($data_saida)));

        if ($data_chegada) $data_chegada = date(('d/m/Y'), strtotime($data_chegada));

        if ($data_saida)  $data_saida = date(('d/m/Y'), strtotime($data_saida));
        if ($data_retorno) $data_retorno = date(('d/m/Y'), strtotime($data_retorno));

        $enderecoArray = explode(",", $endereco);

        if (count($enderecoArray) > 0) {
            $endereco = $enderecoArray[0];
            if (count($enderecoArray > 1)) $bairro = $enderecoArray[1];
        }

        $condicoes_pagamento = $sale->condicao_pagamento;

        $dataPagamento = '';
        $formaPagamento = '';
        $valorQueFoiPago = '';

        if (count($pagamentos) > 0) {
            foreach ($pagamentos as $pagamento) {
                $dataPagamento = $this->sma->hrld($pagamento->date);
                $formaPagamento = lang($pagamento->paid_by);
                $valorQueFoiPago = number_format($pagamento->amount, 2, '.', '');
            }
        }

        if($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

        if ($subtotal) {
            $numerExtenso = number_format($subtotal, 2, '.', '');
            $subtotalReais = number_format($subtotal, 2, ',', ' ');
            $subtotal = 'R$ '.number_format($subtotal, 2, ',', ' ').' ('.$this->extenso($numerExtenso).') ';
        }

        $isSingle               = '';
        $isDuploSolteiro        = '';
        $isDuplocasal           = '';
        $isTripoSolteiro1Casal  = '';
        $isTripoSolteiro        = '';
        $isQuaduplo             = '';
        $str_tipo_quarto        = '';

        if ($tipo_quarto == 1) {
            $str_tipo_quarto	= 'DUPLO CASAL';
            $isSingle               = '';
            $isDuploSolteiro        = '';
            $isDuplocasal           = 'X';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';

        } else if ($tipo_quarto == 2) {
            $str_tipo_quarto	= 'DUPLO CASAL + 1 ADULTO';
            $isSingle               = '';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = 'X';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';
        } else if ($tipo_quarto == 3) {
            $str_tipo_quarto	= 'DUPLO CASAL + 2 ADULTO';
            $isSingle               = '';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = 'X';
        } else if ($tipo_quarto == 4) {
            $str_tipo_quarto	= 'DUPLO CASAL + 3 ADULTO';
            $isSingle               = '';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';
        } else if ($tipo_quarto == 5) {
            $str_tipo_quarto	= 'SOLTEIRO';
            $isSingle               = 'X';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';
        } else if ($tipo_quarto == 6) {
            $str_tipo_quarto	= 'DUPLO SOLTEIRO';
            $isSingle               = '';
            $isDuploSolteiro        = 'X';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';
        } else if ($tipo_quarto == 7) {
            $str_tipo_quarto	= 'TRIPLO SOLTEIRO';
            $isSingle               = '';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = 'X';
            $isQuaduplo             = '';
        } else if ($tipo_quarto == 8) {
            $str_tipo_quarto	= 'SINGLE';
            $isSingle               = 'X';
            $isDuploSolteiro        = '';
            $isDuplocasal           = '';
            $isTripoSolteiro1Casal  = '';
            $isTripoSolteiro        = '';
            $isQuaduplo             = '';
        }

        $strTipoDocuemnto = '';
        if ($tipoDocumento == 'rg') $strTipoDocuemnto = 'Identidade (RG)';
        else $strTipoDocuemnto = 'Passaporte';

        $total_passagem = count($inv_items);
        if ($total_passagem > 1)  $total_passagem_exteno =  $total_passagem.' passagens';
        else $total_passagem_exteno =  $total_passagem.' passagem';

        $mesclas = array(
            "dia"                   => date('d'),
            "mes"                   => $mes,
            "mes_numero"            => date('m'),
            "ano"                   => date('Y'),

            "cumprimento"           => $cumprimento,
            "nome_passageiro"       => $name,
            "cpf_passageiro"        => $cpf,
            "endereco_passageiro"   => $endereco,
            "bairro_passageiro"     => $bairro,
            "cidade_passageiro"     => $cidade,
            "email_passageiro"      => $email,
            "data_nascimento"       => $data_aniversario,
            "rg_passageiro"         => $rg,
            "cep_passageiro"        => $cep,
            "estado_passageiro"     => $estado,
            "telefone_passageiro"   => $telefone,
            "telefone_emergencia"   => $telefone_emergencia,
            "telefone"              =>$apenasTelefone,
            "celular"               => $celular,
            "orgao_emissor_rg"      => $orgaoEmissorRG,
            "local_embarque"        => $local_saida,
            "viagem"                => $nomeViagem,
            "oqueinclui"            => $oqueInclui,
            "pais"                  => $country,
            "naturalidade_passageiro" => $objCustomer->cf4,
            "reference_no"          => $sale->reference_no,
            "data_hoje_extenso"     =>  utf8_encode(strftime('%d de %B de %Y',strtotime(date('Y-m-d')))),
            "preco_pacote"          => $preco_pacote,
            "data_saida_extenso"    => $data_saida_extenso,
            "data_saida"            => $data_saida,
            "data_retorno"          => $data_retorno,
            "data_chegada"          => $data_chegada,
            "hora_saida"            => $hora_saida,
            "hora_retorno"          => $hora_retorno,
            "local_retorno"         => $local_retorno,
            "destino"               => $destino,
            "tempo_viagem"          => $tempo_viagem,
            "locais_embarque"       => $locais_embarque,
            "hora_chegada"          => $hora_chegada,
            "observacao_venda"      => $sale->note,

            "total_passageiros"     => count($inv_items),

            "hotel_hospedagem"      => $nomeHotel,
            "valor_passagem"        => $subtotal,
            "valor_passagem_reais"  => $subtotalReais,

            "valorQueFoiPago"       => $valorQueFoiPago,
            "data_pagamento"        => $dataPagamento,
            "forma_pagamento"       => $formaPagamento,
            "condicoes_pagamento"   => $condicoes_pagamento,

            "tipo_quarto"           => $str_tipo_quarto,
            "total_passagem"        => $total_passagem_exteno,

            "dependentes"           => $allDepedentes,

            "isSingle"              => $isSingle,
            "isDuploSolteiro"       => $isDuploSolteiro,
            "isDuplocasal"          => $isDuplocasal,
            "isTripoSolteiro1Casal" => $isTripoSolteiro1Casal,
            "isTripoSolteiro"       => $isTripoSolteiro,
            "isQuaduplo"            => $isQuaduplo,
        );

        $arrayTool = array_merge ( $mesclas,  $passageiro1, $passageiro2 , $passageiro3,
            $passageiro4, $passageiro5, $passageiro6, $passageiro7, $passageiro8, $passageiro9,
            $passageiro10, $passageiro11, $passageiro12, $passageiro13, $passageiro14, $passageiro15);

        marge_docx($arquivo_entrada, $arquivo_saida, $arrayTool);

        header("Pragma: public");
        header("Content-type: application/save");
        header("X-Download-Options: noopen ");
        header("X-Content-Type-Options: nosniff");
        header("Content-Disposition: attachment; filename=".$name.".docx");
        header("Expires: 0");
        header("Pragma: no-cache");

        readfile('/home/resultaweb/pipaturismo.resultaweb.com.br/filies/contrato/emissao/saida.docx');
    }

    function formatarOQueInclui($oqueInclui) {

        $oqueInclui = str_replace('</p>','@quebra_linha@', $oqueInclui);
        $oqueInclui = str_replace('<br>','@quebra_linha@', $oqueInclui);
        $oqueInclui = strip_tags($oqueInclui);
        $oqueInclui = str_replace('@quebra_linha@','</w:t><w:br/><w:br/><w:t>', $oqueInclui);

        return $oqueInclui.'</w:t><w:br/><w:br/><w:t>';
    }

    function imprimirPassageiroContratoEmissao($item, $index)  {

        $objCustomer       = $this->site->getCompanyByID($item->customerClient);
        $subtotal          = $item->subtotal;
        $name              = $objCustomer->name;
        $rg                = $objCustomer->cf1;
        $orgaoEmissorRG    = $objCustomer->cf3;
        $data_aniversario  = $objCustomer->data_aniversario;
        $telefone          = $objCustomer->phone;
        $vat_no            = $objCustomer->vat_no;

        $subtotal = number_format($subtotal, 2, ',', ' ');

        if($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

        $lNome = 'nome_passageiro'.$index;
        $lRg = 'rg_passageiro'.$index;
        $lOrgaoemissao = 'orgao_emissor_rg'.$index;
        $lDataNascimento = 'data_nascimento'.$index;
        $lTelefone = 'telefone_passageiro'.$index;
        $lpreco = 'preco'.$index;
        $lvat_no= 'cpf_passageiro'.$index;

        $passageiro = array(
            $lNome              => $name,
            $lRg                => $rg,
            $lOrgaoemissao      => $orgaoEmissorRG,
            $lDataNascimento    => $data_aniversario,
            $lTelefone          => $telefone,
            $lpreco             => $subtotal,
            $lvat_no            => $vat_no
        );
        return $passageiro;
    }

    function extenso($valor = 0, $maiusculas = false) {
        if(!$maiusculas){
            $singular = ["centavo", "real", "mil", "milhões", "bilhições", "trilhões", "quatrilhões"];
            $plural = ["centavos", "reais", "mil", "milhões", "bilhições", "trilhões", "quatrilhões"];
            $u = ["", "um", "dois", "três", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
        }else{
            $singular = ["CENTAVO", "REAL", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
            $plural = ["CENTAVOS", "REAIS", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
            $u = ["", "UM", "DOIS", "TRES", "QUANTRO", "CINCO", "SEIS",  "SETE", "OITO", "NOVE"];
        }

        $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
        $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
        $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];

        $z = 0;
        $rt = "";

        $valor = number_format($valor, 2, ".", ".");
        $inteiro = explode(".", $valor);
        for($i=0;$i<count($inteiro);$i++)
            for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
                $inteiro[$i] = "0".$inteiro[$i];

        $fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
        for ($i=0;$i<count($inteiro);$i++) {
            $valor = $inteiro[$i];
            $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
            $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
            $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

            $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd &&
                    $ru) ? " e " : "").$ru;
            $t = count($inteiro)-1-$i;
            $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
            if ($valor == "000")$z++; elseif ($z > 0) $z--;
            if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
            if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
        }

        if(!$maiusculas){
            $return = $rt ? $rt : "zero";
        } else {
            if ($rt) $rt = ereg_replace(" E "," e ",ucwords($rt));
            $return = ($rt) ? ($rt) : "Zero" ;
        }

        if(!$maiusculas){
            return utf8_encode(ereg_replace(" E "," e ",ucwords($return)));
        }else{
            return utf8_encode(strtoupper($return));
        }
    }



    public function combine_pdf($sales_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($sales_id as $id) {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $inv = $this->sales_model->getInvoiceByID($id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
            $this->data['user'] = $this->site->getUser($inv->created_by);
            $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
            $this->data['inv'] = $inv;
            $return = $this->sales_model->getReturnBySID($id);
            $this->data['return_sale'] = $return;
            $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
            $this->data['return_items'] = $return ? $this->sales_model->getAllReturnItems($return->id) : null;

            $html[] = array(
                'content' => $this->load->view($this->theme . 'sales/pdf', $this->data, true),
                'footer' => $this->data['biller']->invoice_footer,
            );
        }


        $name = lang("sales") . ".pdf";
        $this->sma->generate_pdf($html, $name);

    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        $this->form_validation->set_rules('note', lang("message"), 'trim');

        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $inv->reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->company,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>',
            );
            $msg = $this->input->post('note');
            $message = $this->parser->parse_string($msg, $parse_data);

            $biller = $this->site->getCompanyByID($inv->biller_id);
            $paypal = $this->sales_model->getPaypalSettings();
            $skrill = $this->sales_model->getSkrillSettings();
            $btn_code = '<div id="payment_buttons" class="text-center margin010">';
            if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                } else {
                    $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                }
                $btn_code .= '<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&business=' . $paypal->account_email . '&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&image_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $paypal_fee) . '&no_shipping=1&no_note=1&currency_code=' . $this->default_currency->code . '&bn=FC-BuyNow&rm=2&return=' . site_url('sales/view/' . $inv->id) . '&cancel_return=' . site_url('sales/view/' . $inv->id) . '&notify_url=' . site_url('payments/paypalipn') . '&custom=' . $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee . '"><img src="' . base_url('assets/images/btn-paypal.png') . '" alt="Pay by PayPal"></a> ';

            }
            if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                if (trim(strtolower($customer->country)) == $biller->country) {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                } else {
                    $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                }
                $btn_code .= ' <a href="https://www.moneybookers.com/app/payment.pl?method=get&pay_to_email=' . $skrill->account_email . '&language=EN&merchant_fields=item_name,item_number&item_name=' . $inv->reference_no . '&item_number=' . $inv->id . '&logo_url=' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '&amount=' . (($inv->grand_total - $inv->paid) + $skrill_fee) . '&return_url=' . site_url('sales/view/' . $inv->id) . '&cancel_url=' . site_url('sales/view/' . $inv->id) . '&detail1_description=' . $inv->reference_no . '&detail1_text=Payment for the sale invoice ' . $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatMoney($inv->grand_total + $skrill_fee) . '&currency=' . $this->default_currency->code . '&status_url=' . site_url('payments/skrillipn') . '"><img src="' . base_url('assets/images/btn-skrill.png') . '" alt="Pay by Skrill"></a>';
            }

            $btn_code .= '<div class="clearfix"></div>
    </div>';
            $message = $message . $btn_code;

            $attachment = $this->pdf($id, null, 'S');
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sma->send_email($to, $subject, $message, null, null, $attachment, $cc, $bcc)) {
            delete_files($attachment);
            $this->session->set_flashdata('message', lang("email_sent"));
            redirect("sales");
        } else {

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) {
                $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            } else {
                $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');
            }

            $this->data['subject'] = array('name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('invoice') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['note'] = array('name' => 'note',
                'id' => 'note',
                'type' => 'text',
                'value' => $this->form_validation->set_value('note', $sale_temp),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);

            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/email', $this->data);
        }
    }

    /* ------------------------------------------------------------------ */

    public function add($quote_id = null)
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');
        $this->form_validation->set_rules('tipoCobrancaId', lang("tipo_cobranca"), 'required');
        $this->form_validation->set_rules('condicaopagamentoId', lang("condicao_pagamento"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');

            if ($this->Owner || $this->Admin) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = date('Y-m-d H:i:s');

            $warehouse_id 			= $this->input->post('warehouse');
            $reference_no_variacao 	= $this->input->post('reference_no_variacao');
            $totalHoteis			= $this->input->post('totalHoteis');
            $supplier_id			= $this->input->post('supplier_id');
            $supplier_id2			= $this->input->post('supplier_id2');
            $supplier_id3			= $this->input->post('supplier_id3');
            $supplier_id4			= $this->input->post('supplier_id4');
            $supplier_id5			= $this->input->post('supplier_id5');
            $supplier_id6			= $this->input->post('supplier_id6');
            $supplier_id7			= $this->input->post('supplier_id7');

            if ($totalHoteis == 1) {
                $supplier_id2 = null;
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 2) {
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 3) {
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 4) {
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 5) {
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 6) {
                $supplier_id7 = null;
            }

            $customer_id 			= $this->input->post('customer');

            $customer_1_id 		= $this->input->post('form_slcustomer_1');
            $customer_2_id 		= $this->input->post('form_slcustomer_2');
            $customer_3_id 		= $this->input->post('form_slcustomer_3');
            $customer_4_id 		= $this->input->post('form_slcustomer_4');
            $customer_5_id 		= $this->input->post('form_slcustomer_5');

            $customer_1			= null;
            $customer_2			= null;
            $customer_3			= null;
            $customer_4			= null;
            $customer_5			= null;

            if ($customer_1_id) {
                $customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
                $customer_1 			= $customer_details_1->name;
            }

            if ($customer_2_id) {
                $customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
                $customer_2 			= $customer_details_2->name;
            }

            if ($customer_3_id) {
                $customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
                $customer_3 			= $customer_details_3->name;
            }

            if ($customer_4_id) {
                $customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
                $customer_4 			= $customer_details_4->name;
            }

            if ($customer_5_id) {
                $customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
                $customer_5 			= $customer_details_5->name;
            }

            $note_hotel 		= $this->input->post('form_slnote_hotel');
            $tipo_quarto 		= $this->input->post('form_sltipo_quarto');
            $biller_id          = $this->input->post('biller');

            if (!$biller_id) $biller_id = $this->session->userdata('biller_id');

            $total_items 		= $this->input->post('total_items');
            $sale_status 		= $this->input->post('sale_status');

            $payment_status 	= $this->input->post('payment_status');
            $payment_term 		= $this->input->post('payment_term');
            $due_date 			= $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping 			= $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details 	= $this->site->getCompanyByID($customer_id);
            $customer 			= $customer_details->name;
            $biller_details 	= $this->site->getCompanyByID($biller_id);
            $biller 			= $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note 				= $this->sma->clear_tags($this->input->post('note'));
            $staff_note 		= $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id 			= $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $local_saida 		= $this->input->post('local_saida');
            $condicao_pagamento = $this->input->post('condicao_pagamento');

            $total 				= 0;
            $product_tax 		= 0;
            $order_tax 			= 0;
            $product_discount 	= 0;
            $order_discount 	= 0;
            $percentage 		= '%';
            $i 					= isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $item_id 			= $_POST['product_id'][$r];

                if (!$reference_no_variacao) {
                    $onibus = $this->sales_model->buscar_Onibus_by_product($item_id);
                    foreach($onibus as $bus)  $reference_no_variacao = $bus->id;
                }

                $item_type 			= $_POST['product_type'][$r];
                $item_code 			= $_POST['product_code'][$r];
                $item_name 			= $_POST['product_name'][$r];
                $item_comissao  	= $_POST['product_comissao'][$r];

                $poltronaClient     = $_POST['poltronaClient'][$r];
                $customerClient 	= $_POST['customerClient'][$r];
                $customerClientName = 'Diego Hoffmann';

                $item_option 		= isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price	= $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price 		= $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity 		= $_POST['quantity'][$r];
                $item_serial 		= isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate		= isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount 		= isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;

                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $products[] = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'product_comissao' => $item_comissao,
                        'poltronaClient' => $poltronaClient,
                        'customerClient' => $customerClient,
                        'customerClientName' => $customerClientName,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $pr_tax,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,

                        'fornecedorId' => $_POST['product_fornecedor'][$r],
                        'tipoCobrancaFornecedorId' => $_POST['product_tipo_cobranca_fornecedor'][$r],
                        'condicaoPagamentoFornecedorId' => $_POST['product_condicao_pagamento_fornecedor'][$r],
                        'valorRecebimentoValor' => $_POST['product_valor_recebimento_fornecedor'][$r],
                    );

                    $total += $item_net_price * $item_quantity;

                    //$this->add_despesas_viagem($item_code, $warehouse_id,  $customer, $item_quantity, $poltronaClient,$customerClient, $customerClientName);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            if (!$this->input->post('amount-paid') && $this->input->post('amount-paid') <= 0) {
                $payment_status = 'due';
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);

            $user = $this->site->getUserByBiller($biller_id);
            $user_id = $this->session->userdata('user_id');

            if ($user) $user_id = $user->id;

            $data = array(
                'date' 					=> $date,
                'reference_no' 			=> $reference,
                'reference_no_variacao'	=> $reference_no_variacao,
                'note_hotel' 			=> $note_hotel,
                'tipo_quarto' 			=> $tipo_quarto,
                'totalHoteis'			=> $totalHoteis,
                'hotel'					=> $supplier_id,
                'hotel2'			    => $supplier_id2,
                'hotel3'				=> $supplier_id3,
                'hotel4'				=> $supplier_id4,
                'hotel5'				=> $supplier_id5,
                'hotel6'				=> $supplier_id6,
                'hotel7'				=> $supplier_id7,
                'customer_id' 			=> $customer_id,
                'customer_1_id' 		=> $customer_1_id,
                'customer_2_id' 		=> $customer_2_id,
                'customer_3_id' 		=> $customer_3_id,
                'customer_4_id' 		=> $customer_4_id,
                'customer_5_id' 		=> $customer_5_id,
                'customer' 				=> $customer,
                'customer_1' 			=> $customer_1,
                'customer_2' 			=> $customer_2,
                'customer_3' 			=> $customer_3,
                'customer_4' 			=> $customer_4,
                'customer_5' 			=> $customer_5,
                'biller_id' 			=> $biller_id,
                'biller' 				=> $biller,
                'local_saida' 			=> $local_saida,
                'condicao_pagamento'   	=> $condicao_pagamento,
                'warehouse_id'			=> $warehouse_id,
                'tipo_quarto_str'       => $this->input->post('tipo_quarto_str'),
                'note' 					=> $note,
                'staff_note' 			=> $staff_note,
                'total' 				=> $this->sma->formatDecimal($total),
                'product_discount' 		=> $this->sma->formatDecimal($product_discount),
                'order_discount_id' 	=> $order_discount_id,
                'order_discount' 		=> $order_discount,
                'total_discount' 		=> $total_discount,
                'product_tax' 			=> $this->sma->formatDecimal($product_tax),
                'order_tax_id' 			=> $order_tax_id,
                'order_tax' 			=> $order_tax,
                'total_tax' 			=> $total_tax,
                'shipping' 				=> $this->sma->formatDecimal($shipping),
                'grand_total' 			=> $grand_total,
                'total_items' 			=> $total_items,
                'sale_status' 			=> $sale_status,
                'payment_status' 		=> $payment_status,
                'payment_term' 			=> $payment_term,
                'due_date' 				=> $due_date,
                'vencimento'            => $this->input->post('vencimento'),
                'vendedor'              => $biller_details->name,
                'paid' 					=> 0,
                'created_by' 			=> $user_id,
            );

            if ($payment_status == 'partial' || $payment_status == 'paid') {
                if ($this->input->post('paid_by') == 'deposit') {
                    if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                        $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                if ($this->input->post('paid_by') == 'gift_card') {
                    $gc = $this->site->getGiftCardByNO($this->input->post('gift_card_no'));
                    $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                    $gc_balance = $gc->balance - $amount_paying;
                    $payment = array(
                        'date' 			=> $date,
                        'reference_no' 	=> $this->input->post('payment_reference_no'),
                        'amount' 		=> $this->sma->formatDecimal($amount_paying),
                        'paid_by' 		=> $this->input->post('paid_by'),
                        'cheque_no' 	=> $this->input->post('cheque_no'),
                        'cc_no' 		=> $this->input->post('gift_card_no'),
                        'cc_holder' 	=> $this->input->post('pcc_holder'),
                        'cc_month' 		=> $this->input->post('pcc_month'),
                        'cc_year' 		=> $this->input->post('pcc_year'),
                        'cc_type' 		=> $this->input->post('pcc_type'),
                        'created_by' 	=> $user_id,
                        'note' 			=> $this->input->post('payment_note'),
                        'type' 			=> 'received',
                        'gc_balance' 	=> $gc_balance,
                    );
                } else {

                    if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                        $payment = array(
                            'date' => $date,
                            'reference_no' => $this->input->post('payment_reference_no'),
                            'amount' => $this->sma->formatDecimal($this->input->post('amount-paid')),
                            'paid_by' => $this->input->post('paid_by'),
                            'cheque_no' => $this->input->post('cheque_no'),
                            'cc_no' => $this->input->post('pcc_no'),
                            'cc_holder' => $this->input->post('pcc_holder'),
                            'cc_month' => $this->input->post('pcc_month'),
                            'cc_year' => $this->input->post('pcc_year'),
                            'cc_type' => $this->input->post('pcc_type'),
                            'created_by' => $user_id,
                            'note' => $this->input->post('payment_note'),
                            'type' => 'received',
                        );
                    }
                }
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products, $payment);
        }



        /*
         * LANCAMENTO DE CONTAS A RECEBER DO CLIENTE QUANDDO HÁ LANCAMENTO
         */
        $reference = $this->site->getReference('ex');
        $tipoCobranca = $this->input->post('tipoCobrancaId', true);
        $condicaoPagamento = $this->input->post('condicaopagamentoId', true);
        $valor = $this->input->post('valor');
        $note = $this->input->post('staff_note', true);
        $dataVencimento = date('Y-m-d');

        $valor_vencimento_parcelas = $this->input->post('valorVencimentoParcela');
        $data_vencimento_parcelas = $this->input->post('dtVencimentoParcela');
        $tipo_cobranca_parcelas = $this->input->post('tipocobrancaParcela');
        $desconto_parcelas = $this->input->post('descontoParcela');
        $si_return = array();

        $conta_receber = array(
            'status'                => Financeiro_model::STATUS_ABERTA,
            'receita'               => 13,//TODO RECEITA PADRAO VIRA DA CONFIGURACAO
            'reference'             => $reference,
            'created_by'            => $user_id,
            'pessoa'                => $customer_id,
            'tipocobranca'          => $tipoCobranca,
            'condicaopagamento'     => $condicaoPagamento,
            'dtvencimento'          => $dataVencimento,
            'dtcompetencia'         => $dataVencimento,
            'valor'                 => $valor,
            'warehouse'             => $warehouse_id,
            'note'                  =>  $note,
        );

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment, $si_return , $conta_receber, $valor_vencimento_parcelas, $data_vencimento_parcelas, $tipo_cobranca_parcelas, $desconto_parcelas )) {
            $this->session->set_userdata('remove_slls', 1);

            if ($quote_id) $this->db->update('quotes', array('status' => 'completed'), array('id' => $quote_id));

            $this->session->set_flashdata('message', lang("sale_added"));
            redirect("sales");
        } else {
            if ($quote_id) {

                $this->data['quote'] = $this->sales_model->getQuoteByID($quote_id);
                $items = $this->sales_model->getAllQuoteItems($quote_id);
                $c = rand(100000, 9999999);

                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                    }

                    $row->quantity = 0;
                    $pis = $this->sales_model->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);

                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);

                    if ($options) {
                        $option_quantity = 0;
                        foreach ($options as $option) {
                            $pis = $this->sales_model->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                            if ($pis) {
                                foreach ($pis as $pi) {
                                    $option_quantity += $pi->quantity_balance;
                                }
                            }
                            if ($option->quantity > $option_quantity) {
                                $option->quantity = $option_quantity;
                            }
                        }
                    }
                    $combo_items = false;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }

                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->tax_rate) {
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'options' => $options);
                    } else {
                        $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => false, 'options' => $options);
                    }
                    $c++;
                }
                $this->data['quote_items'] = json_encode($pr);
            }

            $this->data['error'] 		        = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['quote_id'] 	        = $quote_id;

            $this->data['billers'] 		        = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] 	        = $this->site->getAllWarehouses('Confirmado');
            $this->data['tax_rates'] 	        = $this->site->getAllTaxRates();
            $this->data['myPackages']           = $this->products_model->getAllProductsConfirmed();
            $this->data['tiposCobranca']        = $this->financeiro_model->getAllTiposCobranca();
            $this->data['condicoesPagamento']   = $this->financeiro_model->getAllCondicoesPagamento();
            $this->data['destinos']             = $this->destino_model->getAll();
            $this->data['slnumber'] 	        = $this->site->getReference('so');
            $this->data['payment_ref'] 	        = $this->site->getReference('pay');
            $this->data['categories']           = $this->site->getAllCategories();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale')));
            $meta = array('page_title' => lang('add_sale'), 'bc' => $bc);
            $this->page_construct('sales/add', $meta, $this->data);
        }
    }

    public function addPacote() {

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('sales/add_pacote', $meta, $this->data);

    }

    public function addPacoteTerceiro() {

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();

        $this->data['products']     = $this->products_model->getAllProductsConfirmed();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('products')));
        $meta = array('page_title' => lang('products'), 'bc' => $bc);
        $this->page_construct('sales/add_pacote_terceiro', $meta, $this->data);

    }

    public function adicionarMeuPacote() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->load->view($this->theme . 'sales/add_meu_pacote', $this->data);
    }

    public function adicionarPasseio() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['currencies']   = $this->settings_model->getAllCurrencies();
        $this->data['condicoesPagamento'] = $this->financeiro_model->getAllCondicoesPagamento();
        $this->data['tiposCobranca']        = $this->financeiro_model->getAllTiposCobranca();
        $this->load->view($this->theme . 'sales/add_template_item', $this->data);
    }

    public function adicionarHospedagem() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['currencies']   = $this->settings_model->getAllCurrencies();

        $this->load->view($this->theme . 'sales/add_template_item', $this->data);
    }

    public function addTraslado() {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['modal_js'] = $this->site->modal_js();
        $this->data['customer_groups'] = $this->companies_model->getAllCustomerGroups();
        $this->load->view($this->theme . 'sales/add_traslado', $this->data);
    }

    public function adicionarNovoCliente($nome, $sexo, $cpf, $rg, $celular, $email, $dataNascimento, $user, $telefone, $telefoneEmergencia, $alergiaMedicamento) {

        $dataNascimento = date("Y-m-d", strtotime($dataNascimento));

        $data = array(
            'group_id'              => 3,
            'group_name'            => 'customer',
            'customer_group_id'     => 1,
            'customer_group_name'   => 'PASSAGEIROS',
            'tipo_documento'        => 'rg',
            'company'               => strtoupper($nome),
            'name'                  => strtoupper($nome),
            'email'                 => $email,
            'data_aniversario'      => $dataNascimento,
            'sexo'                  => $sexo,
            'vat_no'                => $cpf,
            'phone'                 => $telefone,
            'cf1'                   => $rg,
            'cf5'                   => $celular,//whatsapp
            'telefone_emergencia'   => $telefoneEmergencia,
            'alergia_medicamento'   => $alergiaMedicamento
        );

        $nomeArquivo = '/home/resultaweb/familiamk.resultaweb.com.br/cadastros/'.strtoupper($nome).'-'.$cpf.'.txt';
        $arquivo = fopen($nomeArquivo,'w');

        if ($arquivo == true) {
            $texto = "name=".strtoupper($nome).' email='.$email.' data_aniversario='.$dataNascimento.' cpf='.$cpf.' phone='.$telefone.' rg='.$rg.' celular='.$celular.' telefone emergência='.$telefoneEmergencia;
            fwrite($arquivo, $texto);
            fclose($arquivo);
        }

        $customer = $this->companies_model->getVerificaCustomeByCPF($cpf);

        if (count($customer) > 0 && $cpf) {
            $cid =  $customer->id;
            $this->companies_model->updateCompanyWith($customer->id, $data, NULL);
        } else {
            $cid = $this->companies_model->addCompanyWith($data, NULL);
        }

        if ($dataNascimento != null) {
            $data_event = array(
                'title' 		=> $nome,
                'customers' 	=> $cid,
                'start'	 		=> $dataNascimento,
                'end' 			=> $dataNascimento,
                'description' 	=> 'De os parabens para '.$nome.' hoje e seu aniversario',
                'color' 		=>  '#1d7010',
                'user_id' 	    => $user->id
            );
            $this->companies_model->addEvent($data_event);
        }

        return $cid;
    }

    public function adicionarItemNaVenda($cid, $nomeCliente, $product, $warehouse, $criancaDeColo='NAO') {

        $colo = 0;

        if ($criancaDeColo == 'SIM') {
            $price = 0;
            $nomeCliente = $nomeCliente.' (colo)';
            $colo = 1;
        } else $price = $product->price;

        $subtotal = $price;
        $quantity = 1;
        $item_option = null;

        $products = array(
            'product_id' => $product->id,
            'product_code' => $product->code,
            'product_name' => $product->name,
            'product_type' => $product->type,
            'customerClient' => $cid,
            'customerClientName' =>  $nomeCliente,
            'option_id' => $item_option,
            'net_unit_price' => $price,
            'unit_price' => $price,
            'quantity' => $quantity,
            'warehouse_id' => $warehouse->id,
            'tax' => 0,
            'discount' => 0,
            'item_tax' => 0,
            'item_discount' => 0,
            'real_unit_price' => $price,
            'subtotal' => $subtotal,
            'colo' => $colo
        );

        $this->add_despesas_viagem($product->code, $warehouse->id,  $nomeCliente, $quantity, '', $cid, $nomeCliente);

        return $products;
    }

    public function criarUmaNovaVendaViaLinkDePagamento() {

        $nome = $this->input->get('nome');
        $sexo = $this->input->get('sexo');
        $cpf = $this->input->get('cpf');
        $rg = $this->input->get('rg');
        $celular = $this->input->get('celular');
        $telefone = $this->input->get('telefone');
        $telefone_emergencia = $this->input->get('telefone_emergencia');
        $alergia_medicamento = $this->input->get('alergia_medicamento');
        $email = $this->input->get('email');
        $dataNascimento = $this->input->get('dataNascimento');
        $viagem = $this->input->get('viagem');
        $vendedor = $this->input->get('vendedor');
        $observacoes =  $this->input->get('observacoes');

        $local_embarque = $this->input->get('local_embarque');
        $local_embarque_outros = $this->input->get('local_embarque_outros');

        $formas_pagamento = $this->input->get('formas_pagamento');
        $formas_pagamento_outros = $this->input->get('formas_pagamento_outros');

        $tipos_hospedagem = $this->input->get('tipos_hospedagem');
        $tipos_hospedagem_outros = $this->input->get('tipos_hospedagem_outros');

        $posicao_assento = $this->input->get('posicao_assento');

        $user = $this->site->getUserByBiller($vendedor);
        $product = $this->site->getProductByID($viagem);
        $warehouse = $this->site->getWarehouseByCode($product->code);
        $biller_details 	= $this->site->getCompanyByID($vendedor);

        $cid = $this->adicionarNovoCliente($nome, $sexo, $cpf, $rg, $celular, $email, $dataNascimento, $user, $telefone,$telefone_emergencia, $alergia_medicamento);
        $products = $this->adicionarItemNaVenda($cid, $nome , $product, $warehouse);
        $productsToo[] = $products;

        $i = isset($_GET['nomeDependente']) ? sizeof($_GET['nomeDependente']) : 0;
        $totalPassageiros = 0;

        for ($r = 0; $r < $i; $r++) {

            $nomeDependente = $_GET['nomeDependente'][$r];

            if ($nomeDependente != '') {

                $sexoDependente = $_GET['sexoDependente'][$r];
                $cpfDependente = $_GET['cpfDependente'][$r];
                $rgDependente = $_GET['rgDependente'][$r];
                $dataNascimentoDependente = $_GET['dataNascimentoDependente'][$r];
                $telefoneEmergenciaDependente = $_GET['telefoneEmergenciaDependente'][$r];
                $alergiaMedicamentoDependente = $_GET['alergiaMedicamentoDependente'][$r];
                $criancaDeColo =  $_GET['crianca_colo'][$r];

                $dependente = $this->adicionarNovoCliente($nomeDependente, $sexoDependente, $cpfDependente, $rgDependente, '', '', $dataNascimentoDependente, $user,'', $telefoneEmergenciaDependente, $alergiaMedicamentoDependente);
                $productsAdicional = $this->adicionarItemNaVenda($dependente, $nomeDependente, $product, $warehouse, $criancaDeColo);

                $productsToo[] = $productsAdicional;

                if ($criancaDeColo != 'SIM') $totalPassageiros = $totalPassageiros + 1;
            }
        }

        krsort($productsToo);

        if (strpos($local_embarque ,'OUTROS') !== false) $local_embarque = $local_embarque_outros;
        if (strpos($formas_pagamento ,'OUTROS') !== false) $formas_pagamento = $formas_pagamento_outros;
        if (strpos($tipos_hospedagem ,'OUTROS') !== false ) $tipos_hospedagem = $tipos_hospedagem_outros;

        $total = $product->price;
        $reference_no = $this->site->getReference('so');

        if ($i == 1) $i = 0;

        $venda = array(
            'date' 					=> date('Y-m-d h:i'),
            'reference_no' 			=>  $reference_no,
            'reference_no_variacao'	=> '',
            'note_hotel' 			=> '',
            'tipo_quarto_str'   	=> $tipos_hospedagem,
            'customer_id' 			=> $cid,
            'customer' 				=> $nome,
            'biller_id' 			=> $vendedor,
            'biller' 				=> $biller_details->name,
            'local_saida' 			=> $local_embarque,
            'condicao_pagamento'   	=> $formas_pagamento,
            'warehouse_id'			=> $warehouse->id,
            'note' 					=> $observacoes,
            'posicao_assento'       => $posicao_assento,
            'product_discount' 		=> 0,
            'order_discount_id' 	=> 0,
            'order_discount' 		=> 0,
            'total_discount' 		=> 0,
            'product_tax' 			=> 0,
            'order_tax_id' 			=> 0,
            'order_tax' 			=> 0,
            'total_tax' 			=> 0,
            'shipping' 				=> 0,
            'paid' 					=> 0,
            'grand_total' 			=> $total + ($total*$totalPassageiros),
            'total' 				=> $total + ($total*$totalPassageiros),
            'total_items' 			=> count($products),
            'sale_status' 			=> 'completed',
            'payment_status' 		=> 'due',
            'payment_term' 			=> '',
            'vencimento'            => date('Y-m-d'),
            'vendedor'              => $biller_details->name,
            'created_by' 			=> $user->id,
        );

        $sale_id = $this->sales_model->addSale($venda, $productsToo, array());

        if ($email != '') {

            if (file_exists('./themes/' . $this->theme . '/views/email_templates/sale.html')) $sale_temp = file_get_contents('themes/' . $this->theme . '/views/email_templates/sale.html');
            else $sale_temp = file_get_contents('./themes/default/views/email_templates/sale.html');

            $customer = $this->site->getCompanyByID($cid);
            $this->load->library('parser');
            $parse_data = array(
                'reference_number' => $reference_no,
                'contact_person' => $customer->name,
                'company' => $customer->name,
                'site_link' => base_url(),
                'site_name' => $this->Settings->site_name,
                'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>',
            );
            $message = $this->parser->parse_string($sale_temp, $parse_data);

            $subject = 'RESERVA DE PASSAGEM ' . $this->Settings->site_name;
            $to = $email.','.$this->Settings->default_email;
            $attachment = $this->pdf($sale_id, null, 'S');

            $this->sma->send_email($to, $subject, $message, null, null, $attachment, '', '');
        }

        redirect(base_url()."Linkcompra/link_compra_finalizacao/".$sale_id.'/'.$vendedor.'/'.$product->id);
    }

    /* -------------------------------------------------------------------------------------------------------------------------------- */
    public function add_despesas_viagem($item_code, $warehouse_id,  $customer=null, $item_quantity=1, $poltronaClient=null,$customerClient=null, $customerClientName=null) {

        $product_details 	= $this->sales_model->getProductByCode($item_code);
        $products_variacoes = $this->sales_model->getProductOptions($product_details->id,  $warehouse_id , true);

        foreach ($products_variacoes as $products_variacao) {

            unset($products);
            unset($data);

            $nome_variacao 		= $products_variacao->name;
            $fornecedor 		= $products_variacao->fornecedor;
            $variacao 			= $this->sales_model->getVariantByName($nome_variacao);

            if(is_array($variacao)) {

                $lancamento_type = $variacao[0]->lancamento_type;

                if ($lancamento_type == 2) {

                    if ($products_variacao->price) $totalPreco = $products_variacao->price;
                    else $totalPreco = 0.00;

                    $reference = $this->site->getReference('po');
                    $date = date('Y-m-d H:i:s');

                    if ($fornecedor > 0) {
                        $supplier_id = $fornecedor;
                        $supplier_details = $this->site->getCompanyByID($fornecedor);
                        $supplier = $supplier_details->name . ' (' . $nome_variacao . ')';
                    } else {
                        $supplier_id = 622;//fixo para um fornecedor configurado como despesas de viagem
                        $supplier = "Despesas de Viagem";
                    }

                    $status = 'pending';
                    $total = 0;

                    $unit_cost = $totalPreco;
                    $unit_cost = $this->sma->formatDecimal($unit_cost);
                    $item_net_cost = $unit_cost;
                    $subtotal = ($item_net_cost * $item_quantity);

                    if ($customerClientName != '' &&
                        $customerClientName != 'undefined' &&
                        $customerClientName != '0') {
                        $customer = $customerClientName;
                    }

                    $products[] = array(
                        'product_code'      => $item_code,
                        'poltronaClient'    => $poltronaClient,
                        'customerClient'    => $customerClient,
                        'customerClientName'=> $customerClientName,
                        'product_id'        => $product_details->id,
                        'product_name'      => $customer,//nome do cliente como produto
                        'net_unit_cost'     => $item_net_cost,
                        'option_id'         => $products_variacao->id,
                        'unit_cost'         => $this->sma->formatDecimal($item_net_cost),
                        'quantity'          => $item_quantity,
                        'quantity_balance'  => $item_quantity,
                        'warehouse_id'      => $warehouse_id,
                        'subtotal'          => $this->sma->formatDecimal($subtotal),
                        'real_unit_cost'    => $totalPreco,
                        'date'              => date('Y-m-d', strtotime($date)),
                        'status'            => $status,
                    );

                    $total += ($item_net_cost * $item_quantity);

                    if (empty($products)) $this->form_validation->set_rules('product', lang("order_items"), 'required');
                    else krsort($products);

                    $grand_total = $this->sma->formatDecimal($total);

                    $data = array(
                        'reference_no'  => $reference,
                        'date'          => $date,
                        'supplier_id'   => $supplier_id,
                        'supplier'      => $supplier,
                        'warehouse_id'  => $warehouse_id,
                        'total'         => $this->sma->formatDecimal($total),
                        'grand_total'   => $grand_total,
                        'status'        => $status,
                        'created_by'    => $this->session->userdata('user_id'),
                    );

                    $purchases = $this->db->get_where('purchases',
                        array(
                            'supplier_id'   => $supplier_id,
                            'warehouse_id'  => $warehouse_id),
                        1);

                    if ($purchases->num_rows() > 0) {
                        $purchases = $purchases->row();
                        $this->sales_model->updatePurchase($purchases->id, $data, $products);
                    } else {
                        $this->sales_model->addPurchase($data, $products);
                    }
                }
            }
        }
        $this->session->set_userdata('remove_pols', 1);
    }

    public function  getProductOptions_Hotel($id) {
        $product_variants = $this->products_model->getProductOptions_Hotel($id);
        echo json_encode($product_variants);
    }

    public function getProdutctOption_Onibus($id) {
        $onibus = $this->sales_model->buscar_Onibus_by_product($id);
        echo json_encode($onibus);

    }

    public function getProdutctOption_OnibusByID($idVariacao) {
        $onibus = $this->sales_model->buscar_OnibusById($idVariacao);
        echo json_encode($onibus);
    }

    /* ------------------------------------------------------------------------ */
    public function edit($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) $id = $this->input->get('id');

        $inv = $this->sales_model->getInvoiceByID($id);
        if ($inv->sale_status == 'returned' || $inv->return_id || $inv->return_sale_ref) {
            $this->session->set_flashdata('error', lang('sale_has_returned'));
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');
        //$this->form_validation->set_rules('note', lang("note"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no');

            if ($this->Owner || $this->Admin) $date = $this->sma->fld(trim($this->input->post('date')));
            else $date = $inv->date;

            $reference_no_variacao 	= $this->input->post('reference_no_variacao');

            $totalHoteis        = $this->input->post('totalHoteis');
            $supplier_id		= $this->input->post('supplier_id');
            $supplier_id2		= $this->input->post('supplier_id2');
            $supplier_id3		= $this->input->post('supplier_id3');
            $supplier_id4		= $this->input->post('supplier_id4');
            $supplier_id5		= $this->input->post('supplier_id5');
            $supplier_id6		= $this->input->post('supplier_id6');
            $supplier_id7		= $this->input->post('supplier_id7');

            if ($totalHoteis == 1) {
                $supplier_id2 = null;
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 2) {
                $supplier_id3 = null;
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 3) {
                $supplier_id4 = null;
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 4) {
                $supplier_id5 = null;
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 5) {
                $supplier_id6 = null;
                $supplier_id7 = null;
            }

            if ($totalHoteis == 6) {
                $supplier_id7 = null;
            }

            $customer_1_id 		= $this->input->post('form_slcustomer_1');
            $customer_2_id 		= $this->input->post('form_slcustomer_2');
            $customer_3_id 		= $this->input->post('form_slcustomer_3');
            $customer_4_id 		= $this->input->post('form_slcustomer_4');
            $customer_5_id 		= $this->input->post('form_slcustomer_5');

            $customer_1			= null;
            $customer_2			= null;
            $customer_3			= null;
            $customer_4			= null;
            $customer_5			= null;

            if ($customer_1_id) {
                $customer_details_1 	= $this->site->getCompanyByID($customer_1_id);
                $customer_1 			= $customer_details_1->name;
            }

            if ($customer_2_id) {
                $customer_details_2 	= $this->site->getCompanyByID($customer_2_id);
                $customer_2 			= $customer_details_2->name;
            }

            if ($customer_3_id) {
                $customer_details_3 	= $this->site->getCompanyByID($customer_3_id);
                $customer_3 			= $customer_details_3->name;
            }

            if ($customer_4_id) {
                $customer_details_4 	= $this->site->getCompanyByID($customer_4_id);
                $customer_4 			= $customer_details_4->name;
            }

            if ($customer_5_id) {
                $customer_details_5 	= $this->site->getCompanyByID($customer_5_id);
                $customer_5 			= $customer_details_5->name;
            }

            $note_hotel 		= $this->input->post('form_slnote_hotel');
            $tipo_quarto 		= $this->input->post('form_sltipo_quarto');

            $warehouse_id		= $this->input->post('warehouse');
            $customer_id 		= $this->input->post('customer');
            $local_saida 		= $this->input->post('local_saida');
            $condicao_pagamento = $this->input->post('condicao_pagamento');
            $biller_id 			= $this->input->post('biller');
            $total_items 		= $this->input->post('total_items');
            $sale_status 		= $this->input->post('sale_status');
            $payment_status 	= $this->input->post('payment_status');
            $payment_term 		= $this->input->post('payment_term');
            $due_date 			= $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping 			= $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details 	= $this->site->getCompanyByID($customer_id);
            $customer 			= $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details 	= $this->site->getCompanyByID($biller_id);
            $biller 			= $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note 				= $this->sma->clear_tags($this->input->post('note'));
            $staff_note 		= $this->sma->clear_tags($this->input->post('staff_note'));

            $total 				= 0;
            $product_tax 		= 0;
            $order_tax 			= 0;
            $product_discount 	= 0;
            $order_discount 	= 0;
            $percentage 		= '%';

            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $item_id 	        = $_POST['product_id'][$r];

                if (!$reference_no_variacao) {
                    $onibus = $this->sales_model->buscar_Onibus_by_product($item_id);
                    foreach($onibus as $bus)  $reference_no_variacao = $bus->id;
                }

                $item_type 	        = $_POST['product_type'][$r];
                $item_code 	        = $_POST['product_code'][$r];
                $item_name 	        = $_POST['product_name'][$r];
                $poltronaClient     = $_POST['poltronaClient'][$r];
                $customerClient 	= $_POST['customerClient'][$r];
                $customerClientName = $_POST['customerClientName'][$r];

                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }
                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;
                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $products[] = array(
                        'product_id' 		=> $item_id,
                        'product_code' 		=> $item_code,
                        'product_name' 		=> $item_name,
                        'product_type' 		=> $item_type,
                        'option_id' 		=> $item_option,
                        'net_unit_price' 	=> $item_net_price,
                        'unit_price' 		=> $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' 			=> $item_quantity,
                        'warehouse_id' 		=> $warehouse_id,
                        'item_tax' 			=> $pr_item_tax,
                        'tax_rate_id' 		=> $pr_tax,
                        'tax' 				=> $tax,
                        'discount' 			=> $item_discount,
                        'item_discount' 	=> $pr_item_discount,
                        'subtotal' 			=> $this->sma->formatDecimal($subtotal),
                        'serial_no' 		=> $item_serial,
                        'real_unit_price' 	=> $real_unit_price,
                        'poltronaClient'    => $poltronaClient,
                        'customerClient'    => $customerClient,
                        'customerClientName' => $customerClientName,
                    );

                    $total += $item_net_price * $item_quantity;

                    //somente para a instalacao
                    //$this->add_despesas_viagem($item_code, $warehouse_id,  $customer, $item_quantity, $poltronaClient,$customerClient, $customerClientName);
                }
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);

            $user = $this->site->getUserByBiller($biller_id);
            $user_id = $this->session->userdata('user_id');

            if ($user) $user_id = $user->id;

            $data = array(
                'date'                  => $date,
                'reference_no' 		    => $reference,
                'reference_no_variacao'	=> $reference_no_variacao,
                'note_hotel' 			=> $note_hotel,
                'tipo_quarto' 			=> $tipo_quarto,
                'totalHoteis'			=> $totalHoteis,
                'hotel'					=> $supplier_id,
                'hotel2'			    => $supplier_id2,
                'hotel3'				=> $supplier_id3,
                'hotel4'				=> $supplier_id4,
                'hotel5'				=> $supplier_id5,
                'hotel6'				=> $supplier_id6,
                'hotel7'				=> $supplier_id7,
                'customer_id' 			=> $customer_id,
                'customer_1_id' 		=> $customer_1_id,
                'customer_2_id' 		=> $customer_2_id,
                'customer_3_id' 		=> $customer_3_id,
                'customer_4_id' 		=> $customer_4_id,
                'customer_5_id' 		=> $customer_5_id,
                'customer' 				=> $customer,
                'customer_1' 			=> $customer_1,
                'customer_2' 			=> $customer_2,
                'customer_3' 			=> $customer_3,
                'customer_4' 			=> $customer_4,
                'customer_5' 			=> $customer_5,
                'local_saida' 		    => $local_saida,
                'tipo_quarto_str'       => $this->input->post('tipo_quarto_str'),
                'condicao_pagamento'    => $condicao_pagamento,
                'biller_id' 		    => $biller_id,
                'biller' 			    => $biller,
                'warehouse_id' 		    => $warehouse_id,
                'note' 				    => $note,
                'staff_note' 		    => $staff_note,
                'total'				    => $this->sma->formatDecimal($total),
                'product_discount' 	    => $this->sma->formatDecimal($product_discount),
                'order_discount_id'     => $order_discount_id,
                'order_discount' 	    => $order_discount,
                'total_discount' 	    => $total_discount,
                'product_tax' 		    => $this->sma->formatDecimal($product_tax),
                'order_tax_id' 		    => $order_tax_id,
                'order_tax' 		    => $order_tax,
                'total_tax' 		    => $total_tax,
                'shipping' 			    => $this->sma->formatDecimal($shipping),
                'grand_total' 		    => $grand_total,
                'total_items' 		    => $total_items,
                'sale_status' 		    => $sale_status,
                'payment_status' 	    => $payment_status,
                'payment_term' 		    => $payment_term,
                'due_date' 			    => $due_date,
                'vencimento'            => $this->input->post('vencimento'),
                'vendedor'              => $biller_details->name,
                'updated_by' 		    => $user_id,
                'created_by' 			=> $user_id,
                'updated_at' 		    => date('Y-m-d H:i:s'),
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateSale($id, $data, $products)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_updated"));
            redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['inv'] = $this->sales_model->getInvoiceByID($id);

            if ($this->data['inv']->date <= date('Y-m-d', strtotime('-3 months'))) {
                //$this->session->set_flashdata('error', lang("sale_x_edited_older_than_3_months"));
                //redirect($_SERVER["HTTP_REFERER"]);
            }

            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->details, $row->product_details, $row->cost, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                }
                $pis = $this->sales_model->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id 		= $item->product_id;
                $row->code 		= $item->product_code;
                $row->name 		= $item->product_name;
                $row->type 		= $item->product_type;
                $row->qty 		= $item->quantity;
                $row->quantity += $item->quantity;

                $row->customerClient      = $item->customerClient;
                $row->customerClientName  = $item->customerClientName;
                $row->poltronaClient      = $item->poltronaClient;

                $row->discount 	= $item->discount ? $item->discount : '0';
                $row->price 	= $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);

                if ($options) {
                    $option_quantity = 0;
                    foreach ($options as $option) {
                        $pis = $this->sales_model->getPurchasedItems($row->id, $item->warehouse_id, $item->option_id);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $option_quantity += $pi->quantity_balance;
                            }
                        }
                        $option_quantity += $item->quantity;
                        if ($option->quantity > $option_quantity) {
                            $option->quantity = $option_quantity;
                        }
                    }
                }

                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    $te = $combo_items;
                    foreach ($combo_items as $combo_item) {
                        $combo_item->quantity = $combo_item->qty * $item->quantity;
                    }
                }
                $ri = $this->Settings->item_addition ? $row->id : $c;
                if ($row->tax_rate) {
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'options' => $options);
                } else {
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => false, 'options' => $options);
                }
                $c++;
            }

            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('edit_sale')));
            $meta = array('page_title' => lang('edit_sale'), 'bc' => $bc);
            $this->page_construct('sales/edit', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function return_sale($id = null)
    {
        $this->sma->checkPermissions('return_sales');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->return_id) {
            $this->session->set_flashdata('error', lang("sale_already_returned"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('return_surcharge', lang("return_surcharge"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));

            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            $percentage = '%';
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' ? $_POST['product_option'][$r] : null;
                //$option_details = $this->sales_model->getProductOptionByID($item_option);
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_quantity = (0-$_POST['quantity'][$r]);
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    $unit_price = $real_unit_price;
                    $pr_discount = 0;

                    if (isset($item_discount)) {
                        $discount = $item_discount;
                        $dpos = strpos($discount, $percentage);
                        if ($dpos !== false) {
                            $pds = explode("%", $discount);
                            $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($unit_price)) * (Float) ($pds[0])) / 100);
                        } else {
                            $pr_discount = $this->sma->formatDecimal($discount);
                        }
                    }

                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_tax = 0;
                    $pr_item_tax = 0;
                    $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {
                        $pr_tax = $item_tax_rate;
                        $tax_details = $this->site->getTaxRateByID($pr_tax);
                        if ($tax_details->type == 1 && $tax_details->rate != 0) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                        } elseif ($tax_details->type == 2) {

                            if ($product_details && $product_details->tax_method == 1) {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / 100, 4);
                                $tax = $tax_details->rate . "%";
                            } else {
                                $item_tax = $this->sma->formatDecimal((($unit_price) * $tax_details->rate) / (100 + $tax_details->rate), 4);
                                $tax = $tax_details->rate . "%";
                                $item_net_price = $unit_price - $item_tax;
                            }

                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                            $tax = $tax_details->rate;

                        }
                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);

                    $products[] = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'warehouse_id' => $sale->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $pr_tax,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'sale_item_id' => $sale_item_id,
                    );

                    $si_return[] = array(
                        'id' => $sale_item_id,
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'option_id' => $item_option,
                        'quantity' => (0-$item_quantity),
                        'warehouse_id' => $sale->warehouse_id,
                    );

                    $total += $item_net_price * $item_quantity;
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            if ($this->input->post('discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $order_discount + $product_discount;

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax - $this->sma->formatDecimal($return_surcharge) - $order_discount);
            $data = array('date' => $date,
                'sale_id' => $id,
                'reference_no' => $sale->reference_no,
                'customer_id' => $sale->customer_id,
                'customer' => $sale->customer,
                'biller_id' => $sale->biller_id,
                'biller' => $sale->biller,
                'warehouse_id' => $sale->warehouse_id,
                'note' => $note,
                'total' => $this->sma->formatDecimal($total),
                'product_discount' => $this->sma->formatDecimal($product_discount),
                'order_discount_id' => $order_discount_id,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $this->sma->formatDecimal($product_tax),
                'order_tax_id' => $order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('user_id'),
                'return_sale_ref' => $reference,
                'sale_status' => 'returned',
                'payment_status' => $sale->payment_status == 'paid' ? 'due' : 'pending',
            );
            if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
                $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');
                $payment = array(
                    'date' => $date,
                    'reference_no' => $pay_ref,
                    'amount' => $this->sma->formatDecimal($this->input->post('amount-paid')),
                    'paid_by' => $this->input->post('paid_by'),
                    'cheque_no' => $this->input->post('cheque_no'),
                    'cc_no' => $this->input->post('pcc_no'),
                    'cc_holder' => $this->input->post('pcc_holder'),
                    'cc_month' => $this->input->post('pcc_month'),
                    'cc_year' => $this->input->post('pcc_year'),
                    'cc_type' => $this->input->post('pcc_type'),
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $data['payment_status'] = $grand_total == $this->input->post('amount-paid') ? 'paid' : 'partial';
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $si_return, $payment);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment, $si_return)) {
            $this->session->set_flashdata('message', lang("return_sale_added"));
            redirect("sales/return_sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $sale;
            if ($this->data['inv']->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang("sale_status_x_competed"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($this->data['inv']->date <= date('Y-m-d', strtotime('-3 months'))) {
                //$this->session->set_flashdata('error', lang("sale_x_edited_older_than_3_months"));
                //redirect($_SERVER["HTTP_REFERER"]);
            }
            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->details, $row->product_details, $row->cost, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price);
                }
                $pis = $this->sales_model->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->sale_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->qty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->oqty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id, true);
                $ri = $this->Settings->item_addition ? $row->id : $c;
                if ($row->tax_rate) {
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => $tax_rate, 'options' => $options);
                } else {
                    $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'tax_rate' => false, 'options' => $options);
                }
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['payment_ref'] = '';
            $this->data['reference'] = ''; // $this->site->getReference('re');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('return_sale')));
            $meta = array('page_title' => lang('return_sale'), 'bc' => $bc);
            $this->page_construct('sales/return_sale', $meta, $this->data);
        }
    }

    /* ------------------------------- */

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteSale($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("sale_deleted");die();
            }
            $this->session->set_flashdata('message', lang('sale_deleted'));
            redirect('welcome');
        }
    }

    public function delete_return($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                echo lang("return_sale_deleted");die();
            }
            $this->session->set_flashdata('message', lang('return_sale_deleted'));
            redirect('welcome');
        }
    }

    public function sale_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteSale($id);
                    }
                    $this->session->set_flashdata('message', lang("sales_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);

                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);

                } elseif ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('payment_status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->sales_model->getInvoiceByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sale->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sale->paid);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $sale->payment_status);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function pdf_produtos($id = NULL, $view = NULL)
    {
        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . site_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->products_model->getSubCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load->view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load->view($this->theme . 'products/pdf', $this->data, TRUE);
            $this->sma->generate_pdf($html, $name);
        }
    }

    /* ------------------------------- */

    public function deliveries()
    {
        $this->sma->checkPermissions();

        $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('deliveries')));
        $meta = array('page_title' => lang('deliveries'), 'bc' => $bc);
        $this->page_construct('sales/deliveries', $meta, $this->data);

    }

    public function getDeliveries()
    {
        $this->sma->checkPermissions('deliveries');

        $detail_link = anchor('sales/view_delivery/$1', '<i class="fa fa-file-text-o"></i> ' . lang('delivery_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('sales/email_delivery/$1', '<i class="fa fa-envelope"></i> ' . lang('email_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('sales/edit_delivery/$1', '<i class="fa fa-edit"></i> ' . lang('edit_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $pdf_link = anchor('sales/pdf_delivery/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_delivery") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('sales/delete_delivery/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_delivery') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
    <ul class="dropdown-menu pull-right" role="menu">
        <li>' . $detail_link . '</li>
        <li>' . $edit_link . '</li>
        <li>' . $pdf_link . '</li>
        <li>' . $delete_link . '</li>
    </ul>
</div></div>';

        $this->load->library('datatables');
        //GROUP_CONCAT(CONCAT('Name: ', sale_items.product_name, ' Qty: ', sale_items.quantity ) SEPARATOR '<br>')
        $this->datatables
            ->select("deliveries.id as id, date, do_reference_no, sale_reference_no, customer, address")
            ->from('deliveries')
            ->join('sale_items', 'sale_items.sale_id=deliveries.sale_id', 'left')
            ->group_by('deliveries.id');
        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf_delivery($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);

        $this->data['delivery'] = $deli;
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);

        $name = lang("delivery") . "_" . str_replace('/', '_', $deli->do_reference_no) . ".pdf";
        $html = $this->load->view($this->theme . 'sales/pdf_delivery', $this->data, true);
        if ($view) {
            $this->load->view($this->theme . 'sales/pdf_delivery', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function view_delivery($id = null)
    {
        $this->sma->checkPermissions('deliveries');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        if (!$sale) {
            $this->session->set_flashdata('error', lang('sale_not_found'));
            $this->sma->md();
        }
        $this->data['delivery'] = $deli;
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);
        $this->data['page_title'] = lang("delivery_order");

        $this->load->view($this->theme . 'sales/view_delivery', $this->data);
    }

    public function add_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        //$this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');

        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $dlDetails = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no') ? $this->input->post('do_reference_no') : $this->site->getReference('do'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );
        } elseif ($this->input->post('add_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addDelivery($dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_added"));
            redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $sale = $this->sales_model->getInvoiceByID($id);
            $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
            $this->data['inv'] = $sale;
            $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/add_delivery', $this->data);
        }
    }

    public function edit_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');
        //$this->form_validation->set_rules('note', lang("note"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            $dlDetails = array(
                'sale_id' => $this->input->post('sale_id'),
                'do_reference_no' => $this->input->post('do_reference_no'),
                'sale_reference_no' => $this->input->post('sale_reference_no'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $dlDetails['date'] = $date;
            }
        } elseif ($this->input->post('edit_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateDelivery($id, $dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_updated"));
            redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['delivery'] = $this->sales_model->getDeliveryByID($id);
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/edit_delivery', $this->data);
        }
    }

    public function delete_delivery($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteDelivery($id)) {
            echo lang("delivery_deleted");
        }

    }

    public function delivery_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_delivery');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteDelivery($id);
                    }
                    $this->session->set_flashdata('message', lang("deliveries_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deliveries'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('do_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $delivery = $this->sales_model->getDeliveryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($delivery->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $delivery->do_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $delivery->sale_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $delivery->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $delivery->address);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

                    $filename = 'deliveries_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_delivery_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* -------------------------------------------------------------------------------- */

    public function payments($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['payments'] = $this->sales_model->getInvoicePayments($id);
        $this->load->view($this->theme . 'sales/payments', $this->data);
    }

    public function payment_note($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['inv'] = $inv;

        $this->data['fatura'] = $this->financeiro_model->getFaturaById($payment->fatura);
        $this->data['payment'] = $payment;
        $this->data['customer'] = $this->site->getCompanyByID($payment->pessoa);
        $this->data['page_title'] = $this->lang->line("payment_note");

        $this->load->view($this->theme . 'sales/payment_note', $this->data);
    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');

        if ($this->input->get('id')) $id = $this->input->get('id');

        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            //if ($this->input->post('paid_by') == 'deposit') {
            //  $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            // $customer_id = $sale->customer_id;
            //  if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
            // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            // redirect($_SERVER["HTTP_REFERER"]);
            //  }
            // } else {
            $customer_id = null;
            // }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $sale = $this->site->getSaleByID($this->input->post('sale_id'));
            $user = $this->site->getUserByBiller($sale->biller_id);
            $user_id = $this->session->userdata('user_id');

            if ($user) $user_id = $user->id;

            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $user_id,
                'type' => 'received',
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addPayment($payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $sale = $this->sales_model->getInvoiceByID($id);
            $this->data['inv'] = $sale;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();

            $this->load->view($this->theme . 'sales/add_payment', $this->data);
        }
    }

    public function add_payment_mesclar($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');

        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            //if ($this->input->post('paid_by') == 'deposit') {
            //$sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            //$customer_id = $sale->customer_id;
            //if ( ! $this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
            // $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            // redirect($_SERVER["HTTP_REFERER"]);
            //}
            // } else {
            $customer_id = null;
            // }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $saleb = $this->sales_model->getInvoiceByID($id);

        //$this->sma->print_arrays($sale);

        $sales = $this->sales_model->getAllSalesByCustomer(4);
        $abrir = false;
        $totalVenda = 0;

        foreach($sales as $sl) {

            if ($sl->payment_status != 'paid') {
                $grand_total = $sl->grand_total;
                $paid = $sl->paid;
                $totalVenda = $totalVenda + ($grand_total - $paid);
            }
        }

        $amounr_paid = $this->input->post('amount-paid');

        foreach($sales as $sl) {

            $grand_total = $sl->grand_total;
            $paid = $sl->paid;

            if  ($amounr_paid > ($grand_total - $paid) ) {
                $pos_paid =  ($grand_total - $paid);
                $amounr_paid = $amounr_paid - $pos_paid;
            } else {
                $pos_paid = ($grand_total - $paid);
            }

            $pos_balance = $grand_total - $pos_paid;

            $payment = array(
                'date' => $date,
                'sale_id' => $sl->id,
                'reference_no' => $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('pay'),
                'amount' => $pos_paid,
                'pos_paid' =>$pos_paid,
                'pos_balance' =>$pos_balance,
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'received',
            );

            //$this->sma->print_arrays($payment);

            if ($this->form_validation->run() == true && $this->sales_model->addPaymentMesclar($payment, $sl->payment_status, $customer_id)) {
                $this->session->set_flashdata('message', lang("payment_added"));
            } else {
                $abrir = true;
            }
        }

        if ($abrir) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $sale = $this->sales_model->getInvoiceByID(null);
            $this->data['inv'] = $sale;
            $this->data['totalVenda'] = $totalVenda;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/add_payment_mesclar', $this->data);
        } else {
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }


    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $payment = $this->sales_model->getPaymentByID($id);
        if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') {
            $this->session->set_flashdata('error', lang('x_edit_payment'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            // if ($this->input->post('paid_by') == 'deposit') {
            //   $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
            //   $customer_id = $sale->customer_id;
            //  $amount = $this->input->post('amount-paid')-$payment->amount;
            //  if ( ! $this->site->check_customer_deposit($customer_id, $amount)) {
            //    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
            //    redirect($_SERVER["HTTP_REFERER"]);
            // }
            // } else {
            $customer_id = null;
            //}
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updatePayment($id, $payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_payment', $this->data);
        }
    }

    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deletePayment($id)) {
            //echo lang("payment_deleted");
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    public function suggestions()
    {
        $term           = $this->input->get('term', true);
        $warehouse_id   = $this->input->get('warehouse_id', true);
        $customer_id    = $this->input->get('customer_id', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . site_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed   = $this->sma->analyze_term($term);
        $sr         = $analyzed['term'];
        $option_id  = $analyzed['option_id'];

        $customer       = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows           = $this->sales_model->getProductNames($sr, $warehouse_id);

        if ($rows) {
            foreach ($rows as $row) {
                $option = false;
                $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);

                if ($options) {
                    $opt = $option_id ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                }
                $row->option = $option_id;
                $pis = $this->sales_model->getPurchasedItems($row->id, $warehouse_id, $row->option);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                if ($options) {
                    $option_quantity = 0;
                    foreach ($options as $option) {
                        $pis = $this->sales_model->getPurchasedItems($row->id, $warehouse_id, $row->option);
                        if ($pis) {
                            foreach ($pis as $pi) {
                                $option_quantity += $pi->quantity_balance;
                            }
                        }
                        if ($option->quantity > $option_quantity) {
                            // $option->quantity = $option_quantity;
                        }
                    }
                }
                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }
                if ($opt->price != 0) {
                    $row->price = $opt->price + (($opt->price * $customer_group->percent) / 100);
                } else {
                    $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                }
                $row->real_unit_price = $row->price;
                $combo_items = false;
                if ($row->tax_rate) {
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                    }
                    $pr[] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'category' => $row->category_id, 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'options' => $options);
                } else {
                    $pr[] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'category' => $row->category_id, 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => false, 'options' => $options);
                }
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    /* ------------------------------------ Gift Cards ---------------------------------- */

    public function gift_cards()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('gift_cards')));
        $meta = array('page_title' => lang('gift_cards'), 'bc' => $bc);
        $this->page_construct('sales/gift_cards', $meta, $this->data);
    }

    public function getGiftCards()
    {

        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('gift_cards') . ".id as id, card_no, value, balance, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name) as created_by, customer, expiry", false)
            ->join('users', 'users.id=gift_cards.created_by', 'left')
            ->from("gift_cards")
            ->add_column("Actions", "<div class=\"text-center\"><a href='" . site_url('sales/view_gift_card/$1') . "' class='tip' title='" . lang("view_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-eye\"></i></a> <a href='" . site_url('sales/edit_gift_card/$1') . "' class='tip' title='" . lang("edit_gift_card") . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_gift_card") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . site_url('sales/delete_gift_card/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function view_gift_card($id = null)
    {
        $this->data['page_title'] = lang('gift_card');
        $gift_card = $this->site->getGiftCardByID($id);
        $this->data['gift_card'] = $this->site->getGiftCardByID($id);
        $this->data['customer'] = $this->site->getCompanyByID($gift_card->customer_id);
        $this->load->view($this->theme . 'sales/view_gift_card', $this->data);
    }

    public function validate_gift_card($no)
    {
        //$this->sma->checkPermissions();
        if ($gc = $this->site->getGiftCardByNO($no)) {
            if ($gc->expiry) {
                if ($gc->expiry >= date('Y-m-d')) {
                    $this->sma->send_json($gc);
                } else {
                    $this->sma->send_json(false);
                }
            } else {
                $this->sma->send_json($gc);
            }
        } else {
            $this->sma->send_json(false);
        }
    }

    public function add_gift_card()
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|is_unique[gift_cards.card_no]|required');
        $this->form_validation->set_rules('value', lang("value"), 'required');

        if ($this->form_validation->run() == true) {
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => $this->input->post('value'),
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
                'created_by' => $this->session->userdata('user_id'),
            );
            $sa_data = array();
            $ca_data = array();
            if ($this->input->post('staff_points')) {
                $sa_points = $this->input->post('sa_points');
                $user = $this->site->getUser($this->input->post('user'));
                if ($user->award_points < $sa_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    redirect("sales/gift_cards");
                }
                $sa_data = array('user' => $user->id, 'points' => ($user->award_points - $sa_points));
            } elseif ($customer_details && $this->input->post('use_points')) {
                $ca_points = $this->input->post('ca_points');
                if ($customer_details->award_points < $ca_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    redirect("sales/gift_cards");
                }
                $ca_data = array('customer' => $this->input->post('customer'), 'points' => ($customer_details->award_points - $ca_points));
            }
            // $this->sma->print_arrays($data, $ca_data, $sa_data);
        } elseif ($this->input->post('add_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->addGiftCard($data, $ca_data, $sa_data)) {
            $this->session->set_flashdata('message', lang("gift_card_added"));
            redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->sales_model->getStaff();
            $this->data['page_title'] = lang("new_gift_card");
            $this->load->view($this->theme . 'sales/add_gift_card', $this->data);
        }
    }

    public function edit_gift_card($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        $gc_details = $this->site->getGiftCardByID($id);
        if ($this->input->post('card_no') != $gc_details->card_no) {
            $this->form_validation->set_rules('card_no', lang("card_no"), 'is_unique[gift_cards.card_no]');
        }
        $this->form_validation->set_rules('value', lang("value"), 'required');
        //$this->form_validation->set_rules('customer', lang("customer"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $gift_card = $this->site->getGiftCardByID($id);
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array('card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => ($this->input->post('value') - $gift_card->value) + $gift_card->balance,
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
            );
        } elseif ($this->input->post('edit_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateGiftCard($id, $data)) {
            $this->session->set_flashdata('message', lang("gift_card_updated"));
            redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['gift_card'] = $this->site->getGiftCardByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load->view($this->theme . 'sales/edit_gift_card', $this->data);
        }
    }

    public function sell_gift_card()
    {
        $this->sma->checkPermissions('gift_cards', true);
        $error = null;
        $gcData = $this->input->get('gcdata');
        if (empty($gcData[0])) {
            $error = lang("value") . " " . lang("is_required");
        }
        if (empty($gcData[1])) {
            $error = lang("card_no") . " " . lang("is_required");
        }

        $customer_details = (!empty($gcData[2])) ? $this->site->getCompanyByID($gcData[2]) : null;
        $customer = $customer_details ? $customer_details->company : null;
        $data = array('card_no' => $gcData[0],
            'value' => $gcData[1],
            'customer_id' => (!empty($gcData[2])) ? $gcData[2] : null,
            'customer' => $customer,
            'balance' => $gcData[1],
            'expiry' => (!empty($gcData[3])) ? $this->sma->fsd($gcData[3]) : null,
            'created_by' => $this->session->userdata('user_id'),
        );

        if (!$error) {
            if ($this->sales_model->addGiftCard($data)) {
                $this->sma->send_json(array('result' => 'success', 'message' => lang("gift_card_added")));
            }
        } else {
            $this->sma->send_json(array('result' => 'failed', 'message' => $error));
        }

    }

    public function delete_gift_card($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->sales_model->deleteGiftCard($id)) {
            echo lang("gift_card_deleted");
        }
    }

    public function gift_card_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete_gift_card');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteGiftCard($id);
                    }
                    $this->session->set_flashdata('message', lang("gift_cards_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel' || $this->input->post('form_action') == 'export_pdf') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('gift_cards'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('card_no'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('value'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->site->getGiftCardByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->card_no);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->value);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->customer);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'gift_cards_' . date('Y_m_d_H_i_s');
                    if ($this->input->post('form_action') == 'export_pdf') {
                        $styleArray = array('borders' => array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN)));
                        $this->excel->getDefaultStyle()->applyFromArray($styleArray);
                        $this->excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
                        require_once APPPATH . "third_party" . DIRECTORY_SEPARATOR . "MPDF" . DIRECTORY_SEPARATOR . "mpdf.php";
                        $rendererName = PHPExcel_Settings::PDF_RENDERER_MPDF;
                        $rendererLibrary = 'MPDF';
                        $rendererLibraryPath = APPPATH . 'third_party' . DIRECTORY_SEPARATOR . $rendererLibrary;
                        if (!PHPExcel_Settings::setPdfRenderer($rendererName, $rendererLibraryPath)) {
                            die('Please set the $rendererName: ' . $rendererName . ' and $rendererLibraryPath: ' . $rendererLibraryPath . ' values' .
                                PHP_EOL . ' as appropriate for your directory structure');
                        }

                        header('Content-Type: application/pdf');
                        header('Content-Disposition: attachment;filename="' . $filename . '.pdf"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'PDF');
                        return $objWriter->save('php://output');
                    }
                    if ($this->input->post('form_action') == 'export_excel') {
                        header('Content-Type: application/vnd.ms-excel');
                        header('Content-Disposition: attachment;filename="' . $filename . '.xls"');
                        header('Cache-Control: max-age=0');

                        $objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
                        return $objWriter->save('php://output');
                    }

                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_gift_card_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function get_award_points($id = null)
    {
        $this->sma->checkPermissions('index');

        $row = $this->site->getUser($id);
        $this->sma->send_json(array('sa_points' => $row->award_points));
    }

    public function imprimir_recibo_pagamento($pagamentoId) {
        echo $this->sales_model->imprimir_recibo_pagamento($pagamentoId);
    }

    /* -------------------------------------------------------------------------------------- */
    public function sale_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', $this->lang->line("upload_file"), 'xss_clean');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {
            $quantity = "quantity";
            $product = "product";
            $unit_cost = "unit_cost";
            $tax_rate = "tax_rate";
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $order_tax = 0;
            $product_discount = 0;
            $order_discount = 0;
            $percentage = '%';

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');

                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;

                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("sales/sale_by_csv");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'net_unit_price', 'quantity', 'variant', 'item_tax_rate', 'discount', 'serial');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {

                    if (isset($csv_pr['code']) && isset($csv_pr['net_unit_price']) && isset($csv_pr['quantity'])) {

                        if ($product_details = $this->sales_model->getProductByCode($csv_pr['code'])) {

                            if ($csv_pr['variant']) {
                                $item_option = $this->sales_model->getProductVariantByName($csv_pr['variant'], $product_details->id);
                                if (!$item_option) {
                                    $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $product_details->name . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $item_option = json_decode('{}');
                                $item_option->id = null;
                            }

                            $item_id = $product_details->id;
                            $item_type = $product_details->type;
                            $item_code = $product_details->code;
                            $item_name = $product_details->name;
                            $item_net_price = $this->sma->formatDecimal($csv_pr['net_unit_price']);
                            $item_quantity = $csv_pr['quantity'];
                            $item_tax_rate = $csv_pr['item_tax_rate'];
                            $item_discount = $csv_pr['discount'];
                            $item_serial = $csv_pr['serial'];

                            if (isset($item_code) && isset($item_net_price) && isset($item_quantity)) {
                                $product_details = $this->sales_model->getProductByCode($item_code);

                                if (isset($item_discount)) {
                                    $discount = $item_discount;
                                    $dpos = strpos($discount, $percentage);
                                    if ($dpos !== false) {
                                        $pds = explode("%", $discount);
                                        $pr_discount = $this->sma->formatDecimal((($this->sma->formatDecimal($item_net_price)) * (Float) ($pds[0])) / 100);
                                    } else {
                                        $pr_discount = $this->sma->formatDecimal($discount);
                                    }
                                } else {
                                    $pr_discount = 0;
                                }
                                $item_net_price = $this->sma->formatDecimal($item_net_price - $pr_discount);
                                $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_quantity);
                                $product_discount += $pr_item_discount;

                                if (isset($item_tax_rate) && $item_tax_rate != 0) {

                                    if ($tax_details = $this->sales_model->getTaxRateByName($item_tax_rate)) {
                                        $pr_tax = $tax_details->id;
                                        if ($tax_details->type == 1) {

                                            $item_tax = $this->sma->formatDecimal((($item_net_price) * $tax_details->rate) / 100, 4);
                                            $tax = $tax_details->rate . "%";

                                        } elseif ($tax_details->type == 2) {
                                            $item_tax = $this->sma->formatDecimal($tax_details->rate);
                                            $tax = $tax_details->rate;
                                        }
                                        $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                                    } else {
                                        $this->session->set_flashdata('error', lang("tax_not_found") . " ( " . $item_tax_rate . " ). " . lang("line_no") . " " . $rw);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }

                                } elseif ($product_details->tax_rate) {

                                    $pr_tax = $product_details->tax_rate;
                                    $tax_details = $this->site->getTaxRateByID($pr_tax);
                                    if ($tax_details->type == 1) {

                                        $item_tax = $this->sma->formatDecimal((($item_net_price) * $tax_details->rate) / 100, 4);
                                        $tax = $tax_details->rate . "%";

                                    } elseif ($tax_details->type == 2) {

                                        $item_tax = $this->sma->formatDecimal($tax_details->rate);
                                        $tax = $tax_details->rate;

                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);

                                } else {
                                    $item_tax = 0;
                                    $pr_tax = 0;
                                    $pr_item_tax = 0;
                                    $tax = "";
                                }
                                $product_tax += $pr_item_tax;

                                $subtotal = (($item_net_price * $item_quantity) + $pr_item_tax);
                                $products[] = array(
                                    'product_id' => $item_id,
                                    'product_code' => $item_code,
                                    'product_name' => $item_name,
                                    'product_type' => $item_type,
                                    'option_id' => $item_option->id,
                                    'net_unit_price' => $item_net_price,
                                    'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                                    'quantity' => $item_quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $pr_tax,
                                    'tax' => $tax,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'subtotal' => $this->sma->formatDecimal($subtotal),
                                    'serial_no' => $item_serial,
                                    'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                                    'real_unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax + $pr_discount),
                                );

                                $total += $item_net_price * $item_quantity;
                            }

                        } else {
                            $this->session->set_flashdata('error', $this->lang->line("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . $this->lang->line("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $rw++;
                    }

                }
            }

            if ($this->input->post('order_discount')) {
                $order_discount_id = $this->input->post('order_discount');
                $opos = strpos($order_discount_id, $percentage);
                if ($opos !== false) {
                    $ods = explode("%", $order_discount_id);
                    $order_discount = $this->sma->formatDecimal((($total + $product_tax) * (Float) ($ods[0])) / 100);
                } else {
                    $order_discount = $this->sma->formatDecimal($order_discount_id);
                }
            } else {
                $order_discount_id = null;
            }
            $total_discount = $this->sma->formatDecimal($order_discount + $product_discount);

            if ($this->Settings->tax2) {
                $order_tax_id = $this->input->post('order_tax');
                if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                    if ($order_tax_details->type == 2) {
                        $order_tax = $this->sma->formatDecimal($order_tax_details->rate);
                    }
                    if ($order_tax_details->type == 1) {
                        $order_tax = $this->sma->formatDecimal((($total + $product_tax - $order_discount) * $order_tax_details->rate) / 100);
                    }
                }
            } else {
                $order_tax_id = null;
            }

            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal($this->sma->formatDecimal($total) + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $this->sma->formatDecimal($total),
                'product_discount' => $this->sma->formatDecimal($product_discount),
                'order_discount_id' => $order_discount_id,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $this->sma->formatDecimal($product_tax),
                'order_tax_id' => $order_tax_id,
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
            );

            if ($payment_status == 'paid') {

                $payment = array(
                    'date' => $date,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $grand_total,
                    'paid_by' => 'cash',
                    'cheque_no' => '',
                    'cc_no' => '',
                    'cc_holder' => '',
                    'cc_month' => '',
                    'cc_year' => '',
                    'cc_type' => '',
                    'created_by' => $this->session->userdata('user_id'),
                    'note' => lang('auto_added_for_sale_by_csv') . ' (' . lang('sale_reference_no') . ' ' . $reference . ')',
                    'type' => 'received',
                );

            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            //$this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', $this->lang->line("sale_added"));
            redirect("sales");
        } else {

            $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['slnumber'] = $this->site->getReference('so');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => site_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale_by_csv')));
            $meta = array('page_title' => lang('add_sale_by_csv'), 'bc' => $bc);
            $this->page_construct('sales/sale_by_csv', $meta, $this->data);

        }
    }

    public function teste() {
        $boletoFacil = $this->boletofacil_model;
        $boletoFacil->token = "F6C94B6AA911575AB604F5F4758A10AB2AA6154B78D64944027447A549E80BB5";
        $boletoFacil->sandbox  = true;

        $boletoFacil->createCharge("D. Neuci", "06624498943", "Pagamento de boleto de compra da viagem para Gramando", "7.00", date('d/m/Y'));
        $boletoFacil->installments = 1;
        $boletoFacil->payerEmail = "andrej.velho@gmail.com";
        echo $boletoFacil->issueCharge();
    }

    public function corrigir() {

        $itens = $this->getAllItens();

        foreach ($itens as $item) {

            $sale_id = $item->sale_id;
            $customerClient = $item->customerClient;
            $customerClientName = $item->customerClientName;
            $warehouse_id = $item->warehouse_id;

            //verificar se a venda ja existe neste caso ela ja foi importada
            $sale = $this->sales_model->getInvoiceByID($sale_id);

            //if ($sale->id != null) continue;

            $newItens = $this->sales_model->getAllInvoiceItems($sale_id);

            $subTotal = 0;
            $discount = 0;
            $quantidade = 0;
            $ZERO = 0;

            foreach ($newItens as $newItem) {
                $subTotal += $subTotal + $newItem->subtotal;
                $discount += $discount + $newItem->discount;
                $quantidade += $quantidade + $newItem->quantity;
            }

            $valorPago = 0;
            $pagamentos = $this->sales_model->getInvoicePayments($sale_id);
            $created_by = null;
            $dataPagamento = null;

            foreach ($pagamentos as $pagamento) {
                $valorPago = $valorPago + $pagamento->amount;
                $created_by = $pagamento->created_by;
                $dataPagamento = $pagamento->date;
            }

            if ($valorPago > 0) {
                if ($valorPago >= $subTotal) $payment_status = 'paid';
                else $payment_status = 'partial';
            } else {
                $payment_status =  'due';
            }

            $despesa = $this->getPurchasesItem($sale_id);
            $data = $despesa->date;

            if ($data == null) $dataVenda = $dataPagamento;
            else $dataVenda = date('Y-m-d H:i:s', strtotime($data));

            $ano = date('Y', strtotime($dataVenda));
            $mes = date('m', strtotime($dataVenda));

            $ref = 'PASS/'.$ano.'/'.$mes.'/'.$sale_id;

            $purchase = $this->getPurchaseByID($despesa->purchase_id);
            $supplier_id = $purchase->supplier_id;

            $biller_details  = $this->site->getCompanyByID($supplier_id);
            $user = $this->site->getUserByBiller($supplier_id);
            $usuarioCriacao = $user->id;

            if ($usuarioCriacao==null) {
                $usuarioCriacao = $created_by;
                if ($biller_details->id == null) {
                    $user = $this->site->getUser($usuarioCriacao);
                    $biller_details  = $this->site->getCompanyByID($user->biller_id);
                }
            }

            $customer  = $this->site->getCompanyByID($customerClient);

            $data = array(
                'id' =>  $sale_id,
                'date' =>  $dataVenda,
                'reference_no' =>  $ref,
                'customer_id' => $customerClient,
                'customer' =>  $customer->name,
                'biller_id' =>  $biller_details->id,
                'biller' =>  $biller_details->name,
                'warehouse_id' =>  $warehouse_id,
                'note' =>  '',//? do pdf
                'staff_note' =>  '',
                'total' =>  $subTotal,
                'product_discount' =>  $discount,
                'order_discount_id' =>  NULL,
                'total_discount' =>  $discount,
                'order_discount' =>  $ZERO,
                'product_tax' =>  $ZERO,
                'order_tax_id' =>  NULL,
                'order_tax' =>  $ZERO,
                'total_tax' =>  $ZERO,
                'shipping' =>  $ZERO,
                'grand_total' =>  $subTotal,
                'sale_status' =>  'completed',
                'payment_status' =>  $payment_status,
                'payment_term' =>  NULL,
                'due_date' =>  NULL,
                'created_by' =>  $usuarioCriacao,
                'updated_at' =>  date('Y-m-d H:i:s'),
                'total_items' => $quantidade,
                'pos' =>  '0',
                'paid' =>  $valorPago,
                'return_id' =>  NULL,
                'surcharge' =>  $ZERO,
                'attachment' =>  NULL,
                'return_sale_ref' =>  NULL,
                'sale_id' =>  '',
                'return_sale_total' =>  $ZERO,
                'local_saida' =>  '',//? do pdf
                'condicao_pagamento' =>  '',//? do pdf
                'vencimento' =>  date('Y-m-d', strtotime($data)),//? do pdf
                'order_site_id' =>  NULL,
                'vendedor' =>  $biller_details->name,
                'posicao_assento' =>  'Do meio para frente',
                'tipo_quarto_str' =>  'DAY USE / BATE E VOLTA',
            );

            //inserir-venda
            $this->db->insert('sales', $data);
        }
    }

    public function getPurchasesItem($sale_id)
    {
        $q = $this->db->get_where('purchase_items', array('source_of_sale' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllItens()
    {
        //ordenar asc
        $this->db->order_by('sale_items.sale_id asc');
        $q = $this->db->get_where('sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchaseByID($id)
    {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function corracao() {
        $path = "E:\\IP\\correcao\\";
        $diretorio = dir($path);
        $contador = 1;
        echo "Lista de Arquivos do diretório '<strong>".$path."</strong>':<br />";
        while($arquivo = $diretorio -> read()){
            echo $contador." - Arquivo do diretório '<strong>".$path.$arquivo."</strong>'";
            $this->correcao2($path.$arquivo);

            $contador++;
        }
        $diretorio -> close();
    }

    public function correcao2($arquivo) {

        $dados = file($arquivo);
        $linha = '';

        foreach($dados as $leitor){
            $linha .= $leitor;
        }

        $nomeViagem = explode('@NOME_VIAGEM@', $linha)[1];
        $cliente = explode('@CLIENTE@', $linha)[1];
        $formaPagamento = explode('@PAG@', $linha)[1];
        $ref = explode('@REF@', $linha)[1];
        $embarque = explode('@EMBARQUE@', $linha)[1];
        $nota = explode('@NOTA@', $linha)[1];

        $item = $this->getItem(trim($nomeViagem), $cliente);

        if ($item->id != null) {
            $data = array(
                'reference_no' => $ref,
                'local_saida' => $embarque,
                'condicao_pagamento' => $formaPagamento,
                'note' => $nota,
            );

            $this->db->update('sales', $data, array('id' => $item->sale_id));

            echo ' - atualizou a venda '.$ref.'<br/>';
        } else {
            ' - NAO encontrou a venda <br/>';
        }
    }

    public function getItem($nomeViagem, $nomeCliente)
    {
        $this->db->join('sma_companies', 'sma_companies.id=sale_items.customerClient');
        $this->db->where('product_name', $this->tirarAcentos($nomeViagem));
        $this->db->where('name', $this->tirarAcentos($nomeCliente));
        $this->db->limit(1);
        $q = $this->db->get('sale_items');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

}
