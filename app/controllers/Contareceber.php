<?php defined('BASEPATH') or exit('No direct script access allowed');

class Contareceber extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            redirect('login');
        }

        $this->lang->load('financeiro', $this->Settings->user_language);
        $this->lang->load('reports', $this->Settings->user_language);

        $this->digital_upload_path = 'assets/uploads/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '16384';
    }

    public function index()
    {
        $start_date  = $this->session->userdata('fl_fin_start_date') != null ? $this->session->userdata('fl_fin_start_date') : $this->input->post('start_date');
        $end_date    = $this->session->userdata('fl_fin_end_date') != null ? $this->session->userdata('fl_fin_end_date') :  $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) {
            $start_date = date('Y-m-d', $data_incio);
        }

        if (!$end_date) {
            $end_date = date('Y-m-d', $data_fim);
        }

        $this->data['filterTipoCobranca']   = $this->session->userdata('fl_fin_tipo_cobranca');
        $this->data['filterStatus']         = $this->session->userdata('fl_fin_status');
        $this->data['filterProgramacao']    = $this->session->userdata('fl_fin_programacao');

        $this->data['date_payment_from']    = $this->session->userdata('fl_fin_pgamento_de');
        $this->data['date_payment_until']   = $this->session->userdata('fl_fin_pgamento_ate');
        $this->data['customer_filter']      = $this->session->userdata('fl_fin_customer');
        $this->data['biller_filter']        = $this->session->userdata('fl_fin_biller');

        $this->data['start_date']   = $start_date;
        $this->data['end_date']     = $end_date;

        $this->data['ano'] = date('Y');
        $this->data['mes'] = date('m');

        $this->data['tiposCobranca']        = $this->site->getAllTiposCobranca();
        $this->data['planoReceitas']        = $this->site->getAllReceitasSuperiores();
        $this->data['billers']              = $this->site->getAllCompanies('biller');

        $bc = array(array('link' => base_url(), 'page' => lang('home')),
            array('link' => site_url('financeiro'), 'page' => lang('financeiro')), array('link' => '#', 'page' => lang('contas_receber')));
        $meta = array('page_title' => lang('contas_receber'), 'bc' => $bc);

        $this->page_construct('conta_receber/index_admin', $meta, $this->data);
        //$this->page_construct('conta_receber/index', $meta, $this->data);
    }

    function getReceitas() {
        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_fatura_link = anchor('faturas/editarFatura/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //<li class="divider"></li>
        //<li>'.$edit_fatura_link.'</li>

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$edit_fatura_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                CONCAT({$this->db->dbprefix('products')}.name, ' ' , DATE_FORMAT({$this->db->dbprefix('agenda_viagem')}.dataSaida, '%d/%m/%Y' )) as product_name,
                {$this->db->dbprefix('fatura')}.strReceitas as receita,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.taxas,
                {$this->db->dbprefix('fatura')}.valorpago,
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('agenda_viagem', 'agenda_viagem.id = fatura.programacaoId', 'left')
            ->join('products', 'products.id = agenda_viagem.produto', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'CREDITO');

        if ($this->input->post('filterBiller')) {
            $this->datatables->join('sales', 'sales.id = fatura.sale_id', 'left');

            $this->datatables->where("{$this->db->dbprefix('sales')}.biller_id", $this->input->post('filterBiller'));
        }

        $start_date = $this->input->post('start_date');
        $end_date   = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$start_date}' ");
        $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$end_date}' ");

        if ($this->input->post('filterTipoCobranca')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        }

        if ($this->input->post('filterReceita')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.receita", $this->input->post('filterReceita') );
        }

        if ($this->input->post('filterProgramacao')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        }

        if ($this->input->post('filterDataPagamentoDe')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento >= '{$this->input->post('filterDataPagamentoDe')}' ");
        }

        if ($this->input->post('filterDataPagamentoAte')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento <= '{$this->input->post('filterDataPagamentoAte')}' ");
        }

        if ($this->input->post('filterCustomer')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.pessoa", $this->input->post('filterCustomer'));
        }

        $this->session->set_userdata('fl_fin_start_date', $this->input->post('start_date'));
        $this->session->set_userdata('fl_fin_end_date', $this->input->post('end_date'));
        $this->session->set_userdata('fl_fin_tipo_cobranca', $this->input->post('filterTipoCobranca'));
        $this->session->set_userdata('fl_fin_receita', $this->input->post('filterReceita'));
        $this->session->set_userdata('fl_fin_programacao', $this->input->post('filterProgramacao'));
        $this->session->set_userdata('fl_fin_pgamento_de', $this->input->post('filterDataPagamentoDe'));
        $this->session->set_userdata('fl_fin_pgamento_ate', $this->input->post('filterDataPagamentoAte'));
        $this->session->set_userdata('fl_fin_customer', $this->input->post('filterCustomer'));
        $this->session->set_userdata('fl_fin_status', $this->input->post('filterStatus'));
        $this->session->set_userdata('fl_fin_biller', $this->input->post('filterBiller'));

        if ($this->input->post('filterStatus')) {
            if ($this->input->post('filterStatus') == 'VENCIDA') {
                $this->datatables->where("DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) < 0");
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL')");
            } else {
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status", $this->input->post('filterStatus') );
            }
        } else {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL', 'QUITADA')");
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    function getReceitasAdmin() {
        $this->load->library('datatables');

        $historico_link = anchor('financeiro/historico/$1', '<i class="fa fa-eye"></i> ' . lang('historico_faturas'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $pagamento_link = anchor('financeiro/pagamentos/$1', '<i class="fa fa-money"></i> ' . lang('ver_pagamentos'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $edit_fatura_link = anchor('faturas/editarFatura/$1', '<i class="fa fa-edit"></i> ' . lang('edit_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_pagamento_link = anchor('faturas/adicionarPagamento/$1', '<i class="fa fa-money"></i> ' . lang('add_pagamento'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');
        $add_cancelar_link = anchor('financeiro/motivoCancelamento/$1', '<i class="fa fa-trash-o"></i> ' . lang('cancelar_fatura'), 'data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"');

        //<li class="divider"></li>
        //<li>'.$edit_fatura_link.'</li>

        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>'.$historico_link.'</li>
                    <li class="divider"></li>
                    <li>'.$edit_fatura_link.'</li>
                    <li class="divider"></li>
                    <li>'.$pagamento_link.'</li>
                    <li>'.$add_pagamento_link.'</li>
                    <li class="divider"></li>
                    <li>'.$add_cancelar_link.'</li>                    
                </ul>
            </div></div>';

        $this->datatables
            ->select("{$this->db->dbprefix('fatura')}.id as id, 
                {$this->db->dbprefix('fatura')}.dtVencimento,
                {$this->db->dbprefix('companies')}.name, 
                CONCAT({$this->db->dbprefix('products')}.name, ' ' , DATE_FORMAT({$this->db->dbprefix('agenda_viagem')}.dataSaida, '%d/%m/%Y' )) as product_name,
                {$this->db->dbprefix('fatura')}.strReceitas as receita,
                {$this->db->dbprefix('tipo_cobranca')}.name as tipoCobranca, 
                {$this->db->dbprefix('fatura')}.valorfatura, 
                {$this->db->dbprefix('fatura')}.taxas,
                {$this->db->dbprefix('fatura')}.valorpago,
                {$this->db->dbprefix('fatura')}.valorpagar, 
                {$this->db->dbprefix('fatura')}.status,
                concat('(',{$this->db->dbprefix('fatura')}.numero_parcela,')') as parcelas,
                date_format({$this->db->dbprefix('fatura')}.dtultimopagamento, '%d/%m/%Y') as dtultimopagamento, 
                DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) AS quantidade_dias, 
                {$this->db->dbprefix('fatura')}.sale_id,
                {$this->db->dbprefix('fatura_cobranca')}.link,
                {$this->db->dbprefix('fatura_cobranca')}.tipo,
                {$this->db->dbprefix('fatura_cobranca')}.checkoutUrl,
                {$this->db->dbprefix('fatura_cobranca')}.code,
                {$this->db->dbprefix('nf_agendamentos')}.status as status_nf
                ")
            ->from('fatura')
            ->join('fatura_cobranca', 'fatura_cobranca.fatura = fatura.id and (fatura_cobranca.status = "ABERTA" OR fatura_cobranca.status = "QUITADA") ', 'left')
            ->join('companies', 'companies.id = fatura.pessoa')
            ->join('agenda_viagem', 'agenda_viagem.id = fatura.programacaoId', 'left')
            ->join('products', 'products.id = agenda_viagem.produto', 'left')
            ->join('tipo_cobranca', 'tipo_cobranca.id = fatura.tipoCobranca')
            ->join('nf_agendamentos', 'nf_agendamentos.fatura_id = fatura.id', 'left')
            ->where("{$this->db->dbprefix('fatura')}.tipooperacao", 'CREDITO')
            ->order_by("{$this->db->dbprefix('fatura')}.dtVencimento", 'asc');

        if ($this->input->post('filterBiller')) {
            $this->datatables->join('sales', 'sales.id = fatura.sale_id', 'left');

            $this->datatables->where("{$this->db->dbprefix('sales')}.biller_id", $this->input->post('filterBiller'));
        }

        $start_date = $this->input->post('start_date');
        $end_date   = $this->input->post('end_date');

        $data_incio = mktime(0, 0, 0, date('m'), 1, date('Y'));
        $data_fim = mktime(23, 59, 59, date('m'), date("t"), date('Y'));

        if (!$start_date) $start_date = date('Y-m-d', $data_incio);
        if (!$end_date) $end_date = date('Y-m-d', $data_fim);

        $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento >= '{$start_date}' ");
        $this->datatables->where("{$this->db->dbprefix('fatura')}.dtVencimento <= '{$end_date}' ");

        if ($this->input->post('filterTipoCobranca')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.tipoCobranca", $this->input->post('filterTipoCobranca') );
        }

        if ($this->input->post('filterReceita')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.receita", $this->input->post('filterReceita') );
        }

        if ($this->input->post('filterProgramacao')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.programacaoId", $this->input->post('filterProgramacao') );
        }

        if ($this->input->post('filterDataPagamentoDe')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento >= '{$this->input->post('filterDataPagamentoDe')}' ");
        }

        if ($this->input->post('filterDataPagamentoAte')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.dtultimopagamento <= '{$this->input->post('filterDataPagamentoAte')}' ");
        }

        if ($this->input->post('filterCustomer')) {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.pessoa", $this->input->post('filterCustomer'));
        }

        $this->session->set_userdata('fl_fin_start_date', $this->input->post('start_date'));
        $this->session->set_userdata('fl_fin_end_date', $this->input->post('end_date'));
        $this->session->set_userdata('fl_fin_tipo_cobranca', $this->input->post('filterTipoCobranca'));
        $this->session->set_userdata('fl_fin_receita', $this->input->post('filterReceita'));
        $this->session->set_userdata('fl_fin_programacao', $this->input->post('filterProgramacao'));
        $this->session->set_userdata('fl_fin_pgamento_de', $this->input->post('filterDataPagamentoDe'));
        $this->session->set_userdata('fl_fin_pgamento_ate', $this->input->post('filterDataPagamentoAte'));
        $this->session->set_userdata('fl_fin_customer', $this->input->post('filterCustomer'));
        $this->session->set_userdata('fl_fin_status', $this->input->post('filterStatus'));
        $this->session->set_userdata('fl_fin_biller', $this->input->post('filterBiller'));

        if ($this->input->post('filterStatus')) {
            if ($this->input->post('filterStatus') == 'VENCIDA') {
                $this->datatables->where("DATEDIFF ({$this->db->dbprefix('fatura')}.dtVencimento, NOW() ) < 0");
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL')");
            } else {
                $this->datatables->where("{$this->db->dbprefix('fatura')}.status", $this->input->post('filterStatus') );
            }
        } else {
            $this->datatables->where("{$this->db->dbprefix('fatura')}.status in ('ABERTA', 'PARCIAL', 'QUITADA')");
        }

        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

}
