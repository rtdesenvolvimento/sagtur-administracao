<?php

require "vendor/autoload.php";
use DocxMerge\DocxMerge;

function marge_docx($arquivo_entrada, $arquivo_saida, $array_mesclar) {

    $dm = new DocxMerge();

    $dm->setValues(
        $arquivo_entrada,
        $arquivo_saida,
        $array_mesclar
    );
}
?>