<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'errors/error_404';
$route['translate_uri_dashes'] = FALSE;

$route['users'] = 'auth/users';
$route['users/create_user'] = 'auth/create_user';
$route['users/profile/(:num)'] = 'auth/profile/$1';
$route['login'] = 'auth/login';
$route['login/(:any)'] = 'auth/login/$1';
$route['logout'] = 'auth/logout';
$route['logout/(:any)'] = 'auth/logout/$1';
$route['register'] = 'auth/register';
$route['forgot_password'] = 'auth/forgot_password';
$route['sales/(:num)'] = 'sales/index/$1';
$route['products/(:num)'] = 'products/index/$1';
$route['purchases/(:num)'] = 'purchases/index/$1';
$route['quotes/(:num)'] = 'quotes/index/$1';

//bots
//$route['robots\.txt']           = 'FileRobotsController/getRobots';
//$route['sitemap_(:any).xml']    = 'FileSitemapsController/index/$1';

$empresa    = explode('/', $_SERVER['REQUEST_URI'])[1];
$host       = str_replace('www.', '', $_SERVER['HTTP_HOST']);

switch ($host) {
    case 'sagtur':                  $route = local_host($route); break;
    case 'contratacao.sagtur.com.br': $route = contratacao($route); break;

    default:
        $route[$empresa.'/(:num)']                          = "errors/error_new_page_cart";
        $route[$empresa.'/loja/(:num)']                     = "errors/error_new_page_cart";
        $route[$empresa.'/estoque/(:num)']                  = "errors/error_new_page_cart";
        $route[$empresa.'/carrinho/(:num)/(:num)']          = "errors/error_new_page_cart";
        $route[$empresa.'/carrinho/(:any)/(:num)']          = "errors/error_new_page_cart";
        $route[$empresa.'/pacote/(:any)/(:num)']            = "errors/error_new_page_cart";
        $route[$empresa.'/page/(:any)/(:num)']              = "errors/error_new_page_cart";

        $route['appcompra/(:any)/(:num)/(:num)']            = "errors/error_new_page_cart";
        $route['loja/(:any)/(:num)']                        = "errors/error_new_page_cart";

        $route['default_controller']                        = 'welcome';
        break;
}

function contratacao($route) {

    $route['(:num)']                        = "loja/open/$1";
    $route['loja/(:num)']                   = "loja/open/$1";

    $route['sobre/(:num)']                  = "about/sobre/$1";
    $route['sobre']                         = "about/index";

    $route['contato/(:num)']                = "contact/contato/$1";
    $route['contato']                       = "contact/index";

    $route['promocao/(:num)']               = "promotion/promocao/$1";
    $route['promocao']                      = "promotion/index";

    $route['galeria/(:num)']                = "gallery/galeria/$1";
    $route['galeria']                       = "gallery/index";

    $route['estoque/(:num)']                = "estoque/open/$1";
    $route['estoque/']                      = "estoque/index";

    $route['carrinho/(:any)/(:num)']        = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']          = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']        = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)'] = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)'] = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']            = "page/page/$1/$2";
    $route['page/(:any)']                   = "page/page/$1";

    $route['appcompra/(:num)/(:num)']       = "appview/open/$1/$2";
    $route['admin']                         = 'auth/login';

    $route['rating/(:any)']                 = "ratings/rating/$1";
    $route['rate/(:num)']                   = "ratings/rate/$1";

    $route['simulation']                    = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']        = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']          = "simulationcreditcard/pagseguro";

    $route['myapp']                         = "myapp/index";

    $route['default_controller'] 		    = 'loja/index';

    $route['login']                         =  'errors/error_404';

    return $route;
}

function local_host($route) {

    $route['(:num)']                            = "loja/open/$1";
    $route['loja/(:num)']                       = "loja/open/$1";

    $route['sobre/(:num)']                      = "about/sobre/$1";
    $route['sobre']                             = "about/index";

    $route['contato/(:num)']                    = "contact/contato/$1";
    $route['contato']                           = "contact/index";

    $route['promocao/(:num)']                   = "promotion/promocao/$1";
    $route['promocao']                          = "promotion/index";

    $route['galeria/(:num)']                    = "gallery/galeria/$1";
    $route['galeria']                           = "gallery/index";

    $route['estoque/(:num)']                    = "estoque/open/$1";
    $route['estoque/']                          = "estoque/index";

    $route['carrinho/(:any)/(:num)']            = "appview/product_lookup/$1/$2";
    $route['pacote/(:any)/(:num)']              = "appview/product_lookup/$1/$2";
    $route['carrinho/(:num)/(:num)']            = "appview/open/$1/$2";
    $route['carrinho_compra/(:num)/(:num)']     = "appview/carrinho_compra/$1/$2";
    $route['carrinho_compra/(:any)/(:num)']     = "appview/shopping_cart/$1/$2";
    $route['page/(:any)/(:num)']                = "page/page/$1/$2";
    $route['page/(:any)']                       = "page/page/$1";

    $route['rating/(:any)']                     = "ratings/rating/$1";
    $route['rate/(:num)']                       = "ratings/rate/$1";

    $route['simulation']                        = "simulationcreditcard/simulation";
    $route['simulation/mercadopago']            = "simulationcreditcard/mercadopago";
    $route['simulation/pagseguro']              = "simulationcreditcard/pagseguro";

    $route['myapp']                             = "myapp/index";

    $route['admin']                             = 'auth/login';

    return $route;
}