var ControllerSaleHospedagem = function () {

    this.onLoad = onLoad;

    function onLoad() {
        onClick();
    }

    function onClick() {
        $('#adicionar-formulario-hospedagem').click(function (event) {

            if (!controllerSales.isPreencheuFilial() ) return;

            adiconarFormularioHospedagem();
        });
    }

    function adiconarFormularioHospedagem() {

        $('#pacotes-com-hospedagem').load(site.base_url + "sales/view_hospedagem", function(event) {

            $('#slTable').hide();

            $('#barra-remover').show();
            $('#sticker').hide();

            $('#descricaoCadastroManual').html('Adicionar Hospedagem');

            $('#iconCadastroManual').removeClass('fa-plus');
            $('#iconCadastroManual').removeClass('fa-briefcase');

            $('#iconCadastroManual').addClass('fa-plane');
            $('#iconCadastroManual').addClass('fa-bed');

            $('#slhotel').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url+"suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            }).on('change', function (e) {
                let endereco = e.added.address+' '+e.added.city+'/'+e.added.state+' CEP:'+e.added.postal_code+' - '+e.added.country;

                $('#enderecoHotel').val(endereco);
                $('#hotelManual').val(e.added.text);
                $('#telefoneHotel').val(e.added.phone);
            });

            $('#slSupplierHospedagem').val(OPERADORA).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url+"suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $(".buscar_hoteis").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: 'get',
                        url: site.base_url+"sales/getHoteis",
                        dataType: "json",
                        data: {
                            term : $("#hotelManual").val(),
                        },
                        success: function (data) {

                            if (data === '') return false;

                            data = JSON.parse(data);
                            response(data);
                        }
                    });
                },
                minLength: 3,
                autoFocus: false,
                delay: 200,
                placeholder: 'Digite uma origem/aeroporto',
                response: function (event, ui) {},
                select: function (event, ui) {}
            });

            buscarFormularioTaxasClienteFornecedor();

            $('#adicionar-hospedagem').click(function (event) {
                salvarHospedagem();
            });
        });
    }

    function buscarFormularioTaxasClienteFornecedor() {

        $('#load-new-hospedagem').load(site.base_url+"sales/adicionarPasseio", function() {

            setCampoObrigatorio('slhotel');
            setCampoObrigatorio('slSupplierHospedagem');

            setCampoObrigatorio('slEmissaoHospedagem');
            setCampoObrigatorio('slDateCheckinHospedagem');
            setCampoObrigatorio('slHoraCheckinHospedagem');
            setCampoObrigatorio('slDateCheckoutHospedagem');
            setCampoObrigatorio('slHoraCheckoutHospedagem');

            setCampoObrigatorio('telefoneHotel');
            setCampoObrigatorio('enderecoHotel');
            setCampoObrigatorio('hotelManual');
            setCampoObrigatorio('tipoHospedagem');
            setCampoObrigatorio('slCategoriaAcomodacaoHospedagem');
            setCampoObrigatorio('slAcomodacaoRegimeCategoria');
            setCampoObrigatorio('slAcomodacaoRegimeCategoria');

            controllerSalesPacote.onLoadCarregarConfiguracaoItem();

            $('#hotelManual').focus();

            $('#slEmissaoHospedagem').val(controllerSalesPacote.dataAtualFormatada());
        });
    }

    function salvarHospedagem() {

        if (!controllerSales.isPreencheuFilial() ) return;

        if (!isCamposObrigatoriosPreenchidosAntesSalvar()){
            bootbox.alert(lang.select_above);

            $('#hotelManual').focus();

            return false;
        }

        let mid = (new Date).getTime();

        let mcode = mid;
        let mqty = 1;
        let mtax = 0;

        let nomePassageiro = $('#search-customer').select2('data').text;
        let clienteId = $('#search-customer').val();

        let categoria = 'Hospedagem';//TODO
        let categoriaId = 3;//TODO

        let fornecedor = $('#slSupplierHospedagem').val();
        let nmFornecedor = $('#slSupplierHospedagem').select2('data').text;

        let hotel = '';
        let nmHotel = '';

        if ($('#slhotel').select2('data') !== null) {
            nmHotel = $('#slhotel').select2('data').text;
            hotel = $('#slhotel').val();
        }

        let nmHotelManual = $('#hotelManual').val();
        let telefoneHotel = $('#telefoneHotel').val();
        let enderecoHotel = $('#enderecoHotel').val();

        let dataEmissao = $('#slEmissaoHospedagem').val();
        let dateCheckin = $('#slDateCheckinHospedagem').val();
        let dateCheckOut = $('#slDateCheckoutHospedagem').val();

        let horaCheckin = $('#slHoraCheckinHospedagem').val();
        let horaCheckOut = $('#slHoraCheckoutHospedagem').val();

        let nmTipoHospedagem = $('#tipoHospedagem').select2('data').text;
        let tipoHospedagem = $('#tipoHospedagem').val();
        let categoriaAcomodacao = $('#slCategoriaAcomodacaoHospedagem').select2('data').text;
        let regimeAcomodacao = $('#slAcomodacaoRegimeCategoria').select2('data').text;
        let nomePacote = '';

        if (nmHotelManual !== '') {
            nomePacote = categoria+' '+nmHotelManual+'<br/><small> '+nmTipoHospedagem+' ('+categoriaAcomodacao+' - '+regimeAcomodacao+')</small>';
        } else {
            nomePacote = categoria+' '+ nmHotel + '<br/><small>'+nmTipoHospedagem+' ('+categoriaAcomodacao+' - '+regimeAcomodacao+')</small>';
        }

        let reserva = $('#slReservaHospedagem').val();

        let unit_price = getNumber($('#vlLiquidoCustomer').find('div').html());
        let mdiscount = getNumber($('#slTaxasAdicionaisDescontoCustomer').val());
        let comissao = getNumber($('#slComissaoConsultorAbsolutoSupplier').val());

        let tipoCobrancaFornecedorId = $('#tipoCobrancaFornecedorId').val();
        let condicaoPagamentoFornecedorId = $('#condicaoPagamentoFornecedorId').val();
        let vlTotalPagamentoFornecedorInput = $('#vlTotalPagamentoFornecedorInput').val();

        let mtax_rate = {};

        slitems[mid] = {
            "id": mid,
            "item_id": mid,
            "label": nomePacote,
            "row": {
                "id": mid,
                "code": mcode,
                "name": nomePacote,
                "quantity": mqty,
                "price": unit_price,
                "unit_price": unit_price,
                "real_unit_price": unit_price,
                "tax_rate": mtax,
                "tax_method": 0,
                "qty": mqty,
                "type": "manual",
                "discount": String(mdiscount),
                "serial": "",
                "option": "",

                "customerClient" :clienteId,
                "customerClientName" : nomePassageiro,

                "fornecedor" : fornecedor,
                'nmFornecedor' : nmFornecedor,

                "receptivo" : hotel,
                'nmReceptivo' : nmHotel,

                "nmEmpresaManual" : nmHotelManual,
                "telefoneEmpresa" : telefoneHotel,
                "enderecoEmpresa" : enderecoHotel,

                "dataEmissao" : dataEmissao,

                "dateCheckin" : dateCheckin,
                "dateCheckOut" : dateCheckOut,
                "horaCheckin" : horaCheckin,
                "horaCheckOut" : horaCheckOut,

                "reserva" : reserva,

                "nmTipoHospedagem" : nmTipoHospedagem,
                "tipoHospedagem" : tipoHospedagem,
                "categoriaAcomodacao" : categoriaAcomodacao,
                "regimeAcomodacao" : regimeAcomodacao,

                "tipoCobrancaFornecedorId": tipoCobrancaFornecedorId,
                "condicaoPagamentoFornecedorId": condicaoPagamentoFornecedorId,
                "vlTotalPagamentoFornecedorInput": vlTotalPagamentoFornecedorInput,

                "comissao" : comissao,

                "category_name" : categoria,
                "categoria_id": categoriaId,

                "digitado" : '1'
            },
            "tax_rate": mtax_rate,
            "options": false
        };

        controllerSalesPacote.preencherTaxasCliente(slitems[mid]);
        controllerSalesPacote.preencherDadosCliente(slitems[mid]);

        localStorage.setItem('slitems', JSON.stringify(slitems));

        loadItems();

        controllerSalesPacote.limparDadosFormularioPacotes();
        //audio_success.play();
    }

    function isCamposObrigatoriosPreenchidosAntesSalvar() {

        var customer =  $('#search-customer').val();
        var supplierHospedagem =  $('#slSupplierHospedagem').val();
        var tipoHospedagem =  $('#tipoHospedagem').val();
        var categoriaAcomodacaoHospedagem =  $('#slCategoriaAcomodacaoHospedagem').val();
        var acomodacaoRegimeCategoria =  $('#slAcomodacaoRegimeCategoria').val();

        return $('form[data-toggle="validator"]').data('bootstrapValidator').validate().isValid() &&
            customer !== '' &&
            tipoHospedagem !== '' &&
            categoriaAcomodacaoHospedagem !== '' &&
            acomodacaoRegimeCategoria !== '' &&
            supplierHospedagem !== '';
    }

    function setCampoObrigatorio(atributo) {
        $('#'+atributo).attr("required", true);
    }

}