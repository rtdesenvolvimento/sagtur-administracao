var PRODUTO = null;

var controllerSales = null;
var controllerSalesPacote = null;
var controllerSaleAereo = null;
var controllerSaleHospedagem = null;
var controllerSaleTransfer = null;
var controllerSalesCalculoTarifas = null;

var mask = {
    money: function() {
        var el = this,exec = function(v) {
            v = v.replace(/\D/g,"");
            v = new String(Number(v));
            var len = v.length;
            if (1 == len)
                v = v.replace(/(\d)/,"0.0$1");
            else if (2 == len)
                v = v.replace(/(\d)/,"0.$1");
            else if (len > 2) {
                v = v.replace(/(\d{2})$/,'.$1');
            }
            return v;
        };

        setTimeout(function(){
            el.value = exec(el.value);
        },1);
    }
}

$(document).ready(function (e) {

    controllerSales = new ControllerSales();
    controllerSalesPacote = new ControllerSalesPacote();
    controllerSalesCalculoTarifas = new ControllerSalesCalculoTarifas();
    controllerSaleAereo = new ControllerSaleAereo();
    controllerSaleHospedagem = new ControllerSaleHospedagem();
    controllerSaleTransfer = new ControllerSaleTransfer();

    controllerSales.onLoad();
    controllerSalesPacote.onLoad();
    controllerSaleAereo.onLoad();
    controllerSaleHospedagem.onLoad();
    controllerSaleTransfer.onLoad();

    $('body a, body button').attr('tabindex', -1);

    $(function(){
        $('.mask_money').bind('keypress',mask.money);
        $('.mask_money').click(function(){$(this).select();});
    });

    $(function(){
        $('.mask_integer').bind('keypress',mask_integer.money);
        $('.mask_integer').click(function(){$(this).select();});
    });

    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }

    $('#reference_no_variacao_onibus').change(function (e) {
        var referencia = $(this).val();
        $('#reference_no_variacao').val(referencia);
    });

    var $customer = $('#slcustomer');

    $customer.change(function (e) {
        localStorage.setItem('slcustomer', $(this).val());

        $('#add_item').autocomplete("search");
    });

    if (slcustomer = localStorage.getItem('slcustomer')) {

        $customer.val(slcustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "saleitem/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    } else {
        nsCustomer();
    }

    //onibus
    nsOnibus();

    nsCustomersClient();

    $('#addNovoDependente').click(function (event) {
        return adicionarNovoDependente();
    });

    // Order level shipping and discount localStorage
    if (sldiscount = localStorage.getItem('sldiscount')) {
        $('#sldiscount').val(sldiscount);
    }

    $('#sltax2').change(function (e) {
        localStorage.setItem('sltax2', $(this).val());
        $('#sltax2').val($(this).val());
    });

    if (sltax2 = localStorage.getItem('sltax2')) {
        $('#sltax2').select2("val", sltax2);
    }

    $('#slsale_status').change(function (e) {
        localStorage.setItem('slsale_status', $(this).val());
    });

    if (slsale_status = localStorage.getItem('slsale_status')) {
        $('#slsale_status').select2("val", slsale_status);
    }

    $('#slpayment_status').change(function (e) {
        var ps = $(this).val();
        localStorage.setItem('slpayment_status', ps);
        if (ps == 'partial' || ps == 'paid') {
            if (ps == 'paid') {
                $('#amount_1').val(formatDecimal(parseFloat(((total + invoice_tax) - order_discount) + shipping)));
            }
            $('#payments').slideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
        }
    });

    if (slpayment_status = localStorage.getItem('slpayment_status')) {
        $('#slpayment_status').select2("val", slpayment_status);
        var ps = slpayment_status;
        if (ps == 'partial' || ps == 'paid') {
            $('#payments').slideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
        }
    }

    $(document).on('change', '.paid_by', function () {
        var p_val = $(this).val();
        localStorage.setItem('paid_by', p_val);
        $('#rpaidby').val(p_val);

        if (p_val == 'cash' || p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }
    });

    if (paid_by = localStorage.getItem('paid_by')) {
        var p_val = paid_by;
        $('.paid_by').select2("val", paid_by);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' || p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }
    }

    if (gift_card_no = localStorage.getItem('gift_card_no')) {
        $('#gift_card_no').val(gift_card_no);
    }

    $('#gift_card_no').change(function (e) {
        localStorage.setItem('gift_card_no', $(this).val());
    });

    if (amount_1 = localStorage.getItem('amount_1')) {
        $('#amount_1').val(amount_1);
    }

    $('#amount_1').change(function (e) {
        localStorage.setItem('amount_1', $(this).val());
    });

    if (paid_by_1 = localStorage.getItem('paid_by_1')) {
        $('#paid_by_1').val(paid_by_1);
    }

    $('#paid_by_1').change(function (e) {
        localStorage.setItem('paid_by_1', $(this).val());
    });

    if (valor = localStorage.getItem('valor')) {
        $('#valor').val(valor);
    }

    $('#valor').change(function (e) {
        localStorage.setItem('valor', $(this).val());
    });

    if (tipoCobrancaId = localStorage.getItem('tipoCobrancaId')) {
        $('#tipoCobrancaId').val(tipoCobrancaId).change();
    }

    $('#tipoCobrancaId').change(function (e) {
        localStorage.setItem('tipoCobrancaId', $(this).val());
    });

    if (condicaopagamentoId = localStorage.getItem('condicaopagamentoId')) {
        $('#condicaopagamentoId').val(tipoCobrancaId).change();
    }

    $('#condicaopagamentoId').change(function (e) {
        localStorage.setItem('condicaopagamentoId', $(this).val());
    });

    if (previsao_pagamento = localStorage.getItem('previsao_pagamento')) {
        $('#previsao_pagamento').val(previsao_pagamento);
    }

    $('#previsao_pagamento').change(function (e) {
        localStorage.setItem('previsao_pagamento', $(this).val());
    });

    if (pcc_holder_1 = localStorage.getItem('pcc_holder_1')) {
        $('#pcc_holder_1').val(pcc_holder_1);
    }

    $('#pcc_holder_1').change(function (e) {
        localStorage.setItem('pcc_holder_1', $(this).val());
    });

    if (pcc_type_1 = localStorage.getItem('pcc_type_1')) {
        $('#pcc_type_1').select2("val", pcc_type_1);
    }

    $('#pcc_type_1').change(function (e) {
        localStorage.setItem('pcc_type_1', $(this).val());
    });

    if (pcc_month_1 = localStorage.getItem('pcc_month_1')) {
        $('#pcc_month_1').val(pcc_month_1);
    }

    $('#pcc_month_1').change(function (e) {
        localStorage.setItem('pcc_month_1', $(this).val());
    });

    if (pcc_year_1 = localStorage.getItem('pcc_year_1')) {
        $('#pcc_year_1').val(pcc_year_1);
    }

    $('#pcc_year_1').change(function (e) {
        localStorage.setItem('pcc_year_1', $(this).val());
    });

    if (pcc_no_1 = localStorage.getItem('pcc_no_1')) {
        $('#pcc_no_1').val(pcc_no_1);
    }

    $('#pcc_no_1').change(function (e) {
        var pcc_no = $(this).val();
        localStorage.setItem('pcc_no_1', pcc_no);
        var CardType = null;
        var ccn1 = pcc_no.charAt(0);
        if (ccn1 == 4)
            CardType = 'Visa';
        else if (ccn1 == 5)
            CardType = 'MasterCard';
        else if (ccn1 == 3)
            CardType = 'Amex';
        else if (ccn1 == 6)
            CardType = 'Discover';
        else
            CardType = 'Visa';

        $('#pcc_type_1').select2("val", CardType);
    });

    if (cheque_no_1 = localStorage.getItem('cheque_no_1')) {
        $('#cheque_no_1').val(cheque_no_1);
    }
    $('#cheque_no_1').change(function (e) {
        localStorage.setItem('cheque_no_1', $(this).val());
    });

    if (payment_note_1 = localStorage.getItem('payment_note_1')) {
        $('#payment_note_1').redactor('set', payment_note_1);
    }

    $('#payment_note_1').redactor('destroy');

    $('#payment_note_1').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('payment_note_1', v);
        }
    });

    var old_payment_term;
    $('#slpayment_term').focus(function () {
        old_payment_term = $(this).val();
    }).change(function (e) {
        var new_payment_term = $(this).val() ? parseFloat($(this).val()) : 0;
        if (!is_numeric($(this).val())) {
            $(this).val(old_payment_term);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            localStorage.setItem('slpayment_term', new_payment_term);
            $('#slpayment_term').val(new_payment_term);
        }
    });

    if (slpayment_term = localStorage.getItem('slpayment_term')) {
        $('#slpayment_term').val(slpayment_term);
    }

    var old_shipping;
    $('#slshipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        if (!is_numeric($(this).val())) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            shipping = $(this).val() ? parseFloat($(this).val()) : '0';
        }

        localStorage.setItem('slshipping', shipping);
        var gtotal = ((total + invoice_tax) - order_discount) + shipping;

        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));

        $('#valor').val(gtotal.toFixed(2));

        controllerSales.getParcelamento();
    });

    if (slshipping = localStorage.getItem('slshipping')) {
        shipping = parseFloat(slshipping);
        $('#slshipping').val(shipping);
    } else {
        shipping = 0;
    }

    $('#add_sale, #edit_sale, #enviar_faturar, #enviar_orcamento, #enviar_orcamento_edit').attr('disabled', true);

    $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        slitems[item_id].row.serial = $(this).val();
        localStorage.setItem('slitems', JSON.stringify(slitems));
    });

    // If there is any item in localStorage
    if (localStorage.getItem('slitems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('slitems')) {
                    localStorage.removeItem('slitems');
                }
                if (localStorage.getItem('sldiscount')) {
                    localStorage.removeItem('sldiscount');
                }
                if (localStorage.getItem('sltax2')) {
                    localStorage.removeItem('sltax2');
                }
                if (localStorage.getItem('slshipping')) {
                    localStorage.removeItem('slshipping');
                }
                if (localStorage.getItem('slref')) {
                    localStorage.removeItem('slref');
                }
                if (localStorage.getItem('slwarehouse')) {
                    localStorage.removeItem('slwarehouse');
                }
                if (localStorage.getItem('slnote')) {
                    localStorage.removeItem('slnote');
                }
                if (localStorage.getItem('slinnote')) {
                    localStorage.removeItem('slinnote');
                }
                if (localStorage.getItem('slcustomer')) {
                    localStorage.removeItem('slcustomer');
                }
                if (localStorage.getItem('slcurrency')) {
                    localStorage.removeItem('slcurrency');
                }
                if (localStorage.getItem('sldate')) {
                    localStorage.removeItem('sldate');
                }
                if (localStorage.getItem('slstatus')) {
                    localStorage.removeItem('slstatus');
                }
                if (localStorage.getItem('slbiller')) {
                    localStorage.removeItem('slbiller');
                }
                if (localStorage.getItem('sdivulgacao')) {
                    localStorage.removeItem('sdivulgacao');
                }
                if (localStorage.getItem('gift_card_no')) {
                    localStorage.removeItem('gift_card_no');
                }
                if (localStorage.getItem('previsao_pagamento')) {
                    localStorage.removeItem('previsao_pagamento');
                }
                if (localStorage.getItem('valor')) {
                    localStorage.removeItem('valor');
                }
                if (localStorage.getItem('tipoCobrancaId')) {
                    localStorage.removeItem('tipoCobrancaId');
                }
                if (localStorage.getItem('condicaopagamentoId')) {
                    localStorage.removeItem('condicaopagamentoId');
                }

                $('#modal-loading').show();
                location.reload();
            }
        });
    });

    // save and load the fields in and/or from localStorage
    $('#slref').change(function (e) {
        localStorage.setItem('slref', $(this).val());
    });
    if (slref = localStorage.getItem('slref')) {
        $('#slref').val(slref);
    }

    $('#slwarehouse').change(function (e) {
        localStorage.setItem('slwarehouse', $(this).val());
        var filial = $("#slwarehouse option:selected").html();
        $('#add_item').val(filial);
    });

    if (slwarehouse = localStorage.getItem('slwarehouse')) {
        $('#slwarehouse').select2("val", slwarehouse);
    }

    $('#slnote').redactor('destroy');
    $('#slnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('slnote', v);
        }
    });

    if (slnote = localStorage.getItem('slnote')) {
        $('#slnote').redactor('set', slnote);
    }

    $('#slinnote').redactor('destroy');
    $('#slinnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('slinnote', v);
        }
    });

    if (slinnote = localStorage.getItem('slinnote')) {
        $('#slinnote').redactor('set', slinnote);
    }

    // prevent default action usln enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

    // Order tax calculation
    if (site.settings.tax2 != 0) {
        $('#sltax2').change(function () {
            localStorage.setItem('sltax2', $(this).val());
            loadItems();
            return;
        });
    }

    // Order discount calculation
    var old_sldiscount;
    $('#sldiscount').focus(function () {
        old_sldiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('sldiscount');
            localStorage.setItem('sldiscount', new_discount);
            loadItems();
            return;
        } else {
            $(this).val(old_sldiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }
    });

    /* ----------------------
     * Delete Row Method
     * ---------------------- */
    $(document).on('click', '.sldel', function () {

        var item_em_edicao = $('#row_id').val();

        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');

        delete slitems[item_id];

        row.remove();

        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();

        if (item_em_edicao === 'row_' + item_id) {
            $('#prModal').modal('hide');
        }
    });

    /* ----------------------
    * Delete Row Method
    * ---------------------- */
    $(document).on('click', '.sldeladdicional', function () {

        var row = $(this).closest('tr');
        var item_id = $(this).attr('id');
        var item_pai = $(this).attr('data-item');

        $.each(slitems, function () {
            var item = this;
            var itemid = item.row.itemid;

            if (itemid === item_id) {
                delete slitems[item_pai].adicionais[item_pai+'_servico_adicional'];
                delete slitems[item.id];
                row.remove();
            }

        });

        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();

    });



    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */

    $(document).on('click', '.editi', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('data-item-id');

        $('#' + row_id).click();
    });

    $(document).on('click', '.edit', function () {

        var row     = $(this).closest('tr');
        var row_id  = row.attr('id');

        item_id     = row.attr('data-item-id');
        item        = slitems[item_id];

        var qty = row.children().children('.rquantity').val(),
            product_option = row.children().children('.roption').val(),
            unit_price = formatDecimal(row.children().children('.realuprice').val()),
            mealuprice = formatDecimal(row.children().children('.mealuprice').val()),
            discount = row.children().children('.rdiscount').val();

        var rpoltrona = row.children().children('.rpoltrona').val();
        var rclient = row.children().children('.rclient').val();
        var net_price = unit_price;

        $('#prModalLabel').text(item.label);

        if (site.settings.tax1) {
            $('#ptax').select2('val', item.row.tax_rate);
            $('#old_tax').val(item.row.tax_rate);
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }

            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if (this.id == pr_tax) {
                        if (this.type == 1) {

                            if (slitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }

                        } else if (this.type == 2) {
                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;
                        }
                    }
                });
            }
        }

        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }

        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';

        if (item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\"  disabled name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if (o == 1) {
                    if (product_option == '') {
                        product_variant = this.id;
                    } else {
                        product_variant = product_option;
                    }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            product_variant = 0;
        }

        $('#poltronaClient').val(rpoltrona);
        $("#customerClient" ).unbind( "change" );

        var nmPagador = $.trim($('#slcustomer').select2('data').text.split('|')[0]);

        $('#nmpagador').html(nmPagador);

        if (!ADD_NEW_PASS) {
            $('#customerClient').val(rclient).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "saleitem/getCustomer/" + rclient,
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "saleitem/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            }).on('change', function (e) {
                if (e.added === undefined) return false;
            });
        } else {
            //$('#customerClient').select2('val', null).select2('open');
            $('#customerClient').select2('val', null);
            ADD_NEW_PASS = false;
        }

        var reference_no_variacao = $('#reference_no_variacao').val();

        if (reference_no_variacao !== '' && reference_no_variacao !== '0') {
            $('#reference_no_variacao_onibus').val(reference_no_variacao).select2({
                minimumInputLength: 0,
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "sales/getProdutctOption_OnibusByID/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "sales/getProdutctOption_Onibus/"+item.row.id,
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data != null) {
                            return {results: data};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        } else {
            $('#reference_no_variacao_onibus').select2({
                minimumInputLength: 0,
                ajax: {
                    url: site.base_url + "sales/getProdutctOption_Onibus/" + item.row.id,
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data != null) {
                            return {results: data};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        }

        $('#tipo_hospedagem').empty();

        var faixaEtaria     = row.children().children('.faixaEtaria').val();
        var localEmbarque   = row.children().children('.localEmbarque').val();
        var tipoTransporte  = row.children().children('.tipoTransporte').val();
        var tipoHospedagem  = row.children().children('.tipoHospedagem').val();

        $('#poptions-div').html(opt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pprice').val(unit_price);
        $('#memoriapreco').val(mealuprice);
        $('#poption').select2('val', item.row.option);
        $('#old_price').val(unit_price);
        $('#row_id').val(row_id);
        $('#item_id').val(item_id);
        $('#productId').val(item.row.id);
        $('#programacaoId').val(getRow(item.row.programacaoId));

        if (item.row.hora_embarque == null) {
            $('#hora_embarque').val(item.row.horaSaida);//todo pega o horario do pacote
        } else {
            $('#hora_embarque').val(item.row.hora_embarque);
        }


        $('#referencia_item').val(item.row.referencia_item);
        $('#observacao_item').redactor('set', getRow(item.row.observacao_item));

        $('#tipoHospedagemId').val(tipoHospedagem !== null && tipoHospedagem !== 'undefined' ? tipoHospedagem : '');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price) + parseFloat(pr_tax_val)));
        $('#pdiscount').val(discount);
        $('#net_price').text(formatMoney(net_price - item_discount));
        $('#pro_tax').text(formatMoney(pr_tax_val));
        $('#pserial').val(row.children().children('.rserial').val());

        $('#div_tipo_transporte').hide();
        $('#div_tipo_hospedagem').hide();
        $('#div_servicos_adicionais').hide();
        $('#div_local_embarque').hide();
        $('#add_passageiro').hide();

        buscarFaixaEtariaBateVolta(item.row.id, faixaEtaria);
        buscarFaixaEtariaHospedagem(item.row.id, faixaEtaria);
        buscarLocalEmbarque(item.row.id, localEmbarque);
        buscarTipoTransporte(item.row.id, tipoTransporte);

        calcularValorProduto();

        loadItems();

        $('#prModal').appendTo("body").modal({
            backdrop: 'static',
            keyboard: false
        }).modal('show');
    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if ($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('keyup', '#pprice, #ptax, #pdiscount', function () {

        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_price = parseFloat($('#pprice').val());
        var item = slitems[item_id];
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';

        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }

        unit_price -= item_discount;

        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;

        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if (this.id == pr_tax) {
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#net_price').text(formatMoney(unit_price));
        $('#pro_tax').text(formatMoney(pr_tax_val));
    });

    /* -----------------------
     * Edit Row Method
     ----------------------- */
    $(document).on('click', '#editItem', function () {

        var row = $('#' + $('#row_id').val());

        if (editarItem(row)) {

            loadItems();

            operacaoSucesso('DADOS DO PASSAGEIRO SALVO COM SUCESSO!', 8000);

            var modal   = $('#prModal'); // Substitua pelo seletor do seu modal
            var divAlvo = $('#slTableDependents'); // Substitua pelo seletor da sua div alvo

            $('#add_passageiro').show();

            modal.animate({
                scrollTop: divAlvo.offset().top - modal.offset().top + modal.scrollTop()
            }, 1000);

        }
        return true;
    });

    $(document).on('click', '#okbtnaddpass', function () {
        $('#add_passageiro').click();
    });

    $(document).on('click', '#closeItem', function () {

        $('#prModal').modal('hide');

        return true;
    });

    //localStorage.clear();//TODO CLEAR

    /* -----------------------
     * Product option change
     ----------------------- */
    $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = slitems[item_id];
        if (item.options !== false) {
            $.each(item.options, function () {
                if (this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(this.price).trigger('change');
                }
            });
        }
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
    $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            slitems = {};

            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                //$('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

    /* ------------------------------
     * Show manual bus modal
     ------------------------------- */
    $(document).on('click', '#addBusManually', function (e) {

        if (count == 1) {
            slitems = {};

            if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                //$('#slcustomer').select2("readonly", true);
                $('#slwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }

        slitems = JSON.parse(localStorage.getItem('slitems'));

        if (slitems == null){
            bootbox.alert(lang.select_above);
            item = null;
            return false;
        }

        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal2').appendTo("body").modal('show');
        return false;
    });

    $(document).on('click', '#addItemManually', function (e) {

        var mid = (new Date).getTime(),
            mcode = $('#mcode').val(),
            mname = $('#mname').val(),
            mtax = parseInt($('#mtax').val()),
            mqty = parseFloat($('#mquantity').val()),
            mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
            unit_price = parseFloat($('#mprice').val()),
            mtax_rate = {};

        if (mcode && mname && mqty && unit_price) {

            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            slitems[mid] = {
                "id": mid,
                "item_id": mid,
                "label": mname + ' (' + mcode + ')',
                "row": {
                    "id": mid,
                    "code": mcode,
                    "name": mname,
                    "quantity": mqty,
                    "price": unit_price,
                    "unit_price": unit_price,
                    "real_unit_price": unit_price,
                    "tax_rate": mtax,
                    "tax_method": 0,
                    "qty": mqty,
                    "type": "manual",
                    "discount": mdiscount,
                    "serial": "",
                    "option": ""
                },
                "tax_rate": mtax_rate,
                "options": false
            };
            localStorage.setItem('slitems', JSON.stringify(slitems));
            loadItems();
        }

        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if (this.id == pr_tax) {
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;
                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
    --------------------------- */
    var old_row_qty;
    $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
            item_id = row.attr('data-item-id');
        slitems[item_id].row.qty = new_qty;
        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();
    });

    /* --------------------------
     * Edit Row Price Method
     -------------------------- */
    var old_price;
    $(document).on("focus", '.rprice', function () {
        old_price = $(this).val();
    }).on("change", '.rprice', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_price);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_price = parseFloat($(this).val()),
            item_id = row.attr('data-item-id');
        slitems[item_id].row.price = new_price;
        localStorage.setItem('slitems', JSON.stringify(slitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
        //$('#slcustomer').select2('readonly', false);
        //$('#slwarehouse').select2('readonly', false);
        return false;
    });

    $('#faixa_etaria').change(function (event) {

        var produtoId = $('#productId').val();
        var tipoHospedagem = $('#tipoHospedagemId').val();
        var faixaId = $(this).val();

        if (faixaId === '') return false;

        buscarTipoHospedagem(produtoId, tipoHospedagem, faixaId);
        buscarServicosAdicionais(produtoId, faixaId);

        calcularValorProduto();
    });
});
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for customer if no localStorage value
function nsCustomersClient() {

    $('#customerClient').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "saleitem/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                }
            }
        }
    });
}

function nsCustomer() {
    $('#slcustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "saleitem/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                }
            }
        }
    });
}

function nsOnibus() {
    $('#reference_no_variacao_onibus').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "sales/getProdutctOption_Onibus",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                }
            }
        }
    });
}

function setar_poltrona(reference_no, variacao) {
    $('#slref').val(reference_no);
    $('#reference_no_variacao').val(variacao);
    $('#close_bus').click();
}

function setar_poltrona_client(reference_no, variacao) {
    $('#reference_no_variacao').val(variacao);
    $('#poltronaClient').val(reference_no);
    $('#close_bus').click();
}

function carregar_tabelas_dependentes(customer, item_id, subtotal, adicional, item_name, supdate) {

    if (supdate === 'I') {
        return false;
    }

    var newRow = '';

    newRow = $("<tr data-item-id='" + item_id + "' id='row_" + item_id + "' >");

    if (adicional) {
        newRow.append("<td colspan='2'> &nbsp;&nbsp;&nbsp;<img src=" + site.base_url + "assets/images/plus.gif>" + item_name + "</td>");
    } else {
        newRow.append("<td>" + customer + "</td>");
        newRow.append("<td>" + item_name + "</td>");
    }

    newRow.append("<td style='text-align: right;'>" + subtotal + "</td>");

    if (adicional) {
        newRow.append("<td></td>");
    } else {
        newRow.append("<td style='text-align: center;'><i class='fa fa-edit tip pointer editi'  id='" + item_id + "' data-item='" + item_id + "' title='Edit'></i></td>");
    }

    newRow.append("<td style='text-align: center;'><i class='fa fa-times tip pointer sldel' id='" + item_id + "' title='Remove' style='cursor:pointer;'></i></td>");

    $("#slTableDependents tbody").append(newRow);

}

function loadItems() {

    if (localStorage.getItem('slitems')) {

        total = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;

        $("#slTable tbody").empty();
        $("#slTableDependents tbody").empty();

        slitems = JSON.parse(localStorage.getItem('slitems'));

        sortedItems = _.sortBy(slitems, function (o) {
            return [parseInt(o.order)];
        });

        sortedItems = _.sortBy(sortedItems, function (o) {
            return [parseInt(o.row.customerClient)];
        });

        $('#add_sale, #edit_sale, #enviar_faturar, #enviar_orcamento, #enviar_orcamento_edit').attr('disabled', false);

        $.each(sortedItems, function () {

            var item = this;

            var supdate = item.supdate;

            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

            PRODUTO =  item.item_id;

            slitems[item_id] = item;

            var totalPorItem = contadorTotalItens(item);

            item.order = item.order ? item.order : new Date().getTime();

            var product_id = item.row.id,
                item_type = item.row.type,
                combo_items = item.combo_items,
                item_price = item.row.price,
                item_qty = item.row.qty,
                item_aqty = item.row.quantity,
                item_tax_method = item.row.tax_method,
                item_ds = item.row.discount,
                item_discount = 0,
                item_option = item.row.option,
                item_code = item.row.code,
                item_serial = item.row.serial,
                item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;"),
                item_comissao = getRow(item.row.comissao),
                item_nome_categoria = item.row.category_name,
                item_categoria_id = item.row.categoriaId,
                item_fornecedor = getRow(item.row.fornecedor),
                item_tipo_cobranca_fornecedor = getRow(item.row.tipoCobrancaFornecedorId),
                item_condicao_pagamento_fornecedor = getRow(item.row.condicaoPagamentoFornecedorId),
                item_valor_recebimento_fornecedor = getRow(item.row.vlTotalPagamentoFornecedorInput);

            var nmTipoTransporte = isRow(item.row.nmTipoTransporte) ? '<br/><i class="fa fa-bus"></i> '+ item.row.nmTipoTransporte : '';
            var nmLocalEmbarque = isRow(item.row.nmLocalEmbarque) ? '<br/><i class="fa fa-map-marker"></i> '+item.row.nmLocalEmbarque  : '';
            var nmTipoHospedagem = isRow(item.row.nmTipoHospedagem) ? '<br/><i class="fa fa-bed"></i> '+item.row.nmTipoHospedagem  : '';
            var nmFaixaEtaria = isRow(item.row.nmFaixaEtaria) ? ' ['+item.row.nmFaixaEtaria+']<br/>'  : '<br/>';
            var nmOrigem = isRow(item.row.origem) ? '<br/><i class="fa fa-hand-o-right"></i> '+item.row.origem  : '';
            var nmDestino = isRow(item.row.destino) ? '<br/><i class="fa fa-hand-o-left"></i> '+item.row.destino  : '';
            var nmFornecedor = isRow(item.row.nmFornecedor) ? '<br/><i class="fa fa-building"></i> '+item.row.nmFornecedor  : '';

            var tipoTransporte = item.row.tipoTransporte;
            var localEmbarque = item.row.localEmbarque;
            var tipoHospedagem = item.row.tipoHospedagem;
            var faixaEtaria = item.row.faixaEtaria;
            var adicional = getRow(item.row.adicional);

            var data = '';
            var ida = '';
            var volta = '';
            var reserva = '';
            var tipoDestino = '';
            var horaCheckin = '';
            var horaCheckOut = '';
            var poltronaClient = '';
            var customerClient = '';

            if (isRow(item.row.dataEmissao)) data = fsd(getRow(item.row.dataEmissao));
            if (isRow(item.row.horaCheckin)) horaCheckin = '  '+getRow(item.row.horaCheckin);
            if (isRow(item.row.horaCheckOut)) horaCheckOut = '  '+getRow(item.row.horaCheckOut);
            if (isRow(item.row.dateCheckin)) ida = '<br/><i class="fa fa-calendar" aria-hidden="true"></i> Saída '+fsd(getRow(item.row.dateCheckin))+horaCheckin;
            if (isRow(item.row.dateCheckOut)) volta = '<br/><i class="fa fa-calendar" aria-hidden="true"></i> Retorno '+fsd(getRow(item.row.dateCheckOut))+horaCheckOut;
            if (isRow(item.row.reserva)) reserva = '<br/><i class="fa fa-ticket" aria-hidden="true"></i> '+getRow(item.row.reserva);
            if (isRow(item.row.tipoDestino)) tipoDestino = '<br/><i class="fa fa-random" aria-hidden="true"></i> Destino '+getRow(item.row.tipoDestino.toLowerCase());
            if (getRow(item.row.poltronaClient) !== '') poltronaClient   = '<br/><i class="fa fa-map-pin" aria-hidden="true"></i> Poltrona '+item.row.poltronaClient;
            if (getRow(item.row.customerClientName)) customerClient   = item.row.customerClientName;

            var descricao =
                '<b style="font-size: 1.5rem;">'+customerClient+'</b>'+
                nmFaixaEtaria +
                '<b style="font-weight: bold;color: #428bca;">'+item_name +'</b>' +
                tipoDestino +
                nmFornecedor +
                reserva +
                nmOrigem +
                nmDestino +
                ida +
                volta +
                nmTipoTransporte +
                nmLocalEmbarque +
                poltronaClient +
                nmTipoHospedagem;

            var unit_price = item.row.real_unit_price;

            var ds = item_ds ? item_ds : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((unit_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = formatDecimal(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }

            product_discount += parseFloat(item_discount * item_qty);

            unit_price = formatDecimal(unit_price - item_discount);
            var pr_tax = item.tax_rate;
            var pr_tax_val = 0, pr_tax_rate = 0;

            if (site.settings.tax1 == 1) {
                if (pr_tax !== false) {
                    if (pr_tax.type == 1) {

                        if (item_tax_method == '0') {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(pr_tax.rate)) / (100 + parseFloat(pr_tax.rate)), 4);
                            pr_tax_rate = formatDecimal(pr_tax.rate) + '%';
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(pr_tax.rate)) / 100), 4);
                            pr_tax_rate = formatDecimal(pr_tax.rate) + '%';
                        }

                    } else if (pr_tax.type == 2) {

                        pr_tax_val = parseFloat(pr_tax.rate);
                        pr_tax_rate = pr_tax.rate;

                    }
                    product_tax += pr_tax_val * item_qty;
                }
            }
            item_price = item_tax_method == 0 ? formatDecimal(unit_price - pr_tax_val) : formatDecimal(unit_price);
            unit_price = formatDecimal(unit_price + item_discount);
            var sel_opt = '';
            $.each(item.options, function () {
                if (this.id == item_option) {
                    sel_opt = this.name;
                }
            });

            var row_no = item_id;//new Date().getTime();
            item.row.row_no = row_no;

            var itemid = item.row.itemid;

            var newTr = '';

            if (adicional) {
                newTr = $('<tr id="row_' + row_no + '" itemid="'+itemid+'" style="background: #fdf59a80;"  class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            } else {
                newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            }

            var tr_html = '';

            if (item_nome_categoria !== undefined) {
                if (adicional) {
                    tr_html += '<td style="text-align: left;display: none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-hand-o-up" aria-hidden="true"></i> '+item_nome_categoria+'</td>';
                } else {
                    tr_html += '<td style="text-align: left;display: none;">'+item_nome_categoria+'</td>';
                }
            } else {
                if (adicional) {
                    tr_html += '<td style="text-align: left;display: none;"><i class="fa fa-plus" aria-hidden="true"></i> ???? </td>';
                } else {
                    tr_html += '<td style="text-align: left;display: none;"> ???? </td>';
                }
            }

            tr_html += '<td>' +

                '<input name="item_id[]" type="hidden" class="item_id" value="' + item_id  + '">' +
                '<input name="itemid[]" type="hidden" class="itemid" value="' + itemid  + '">' +

                '<input name="programacaoId[]" type="hidden" class="programacaoId" value="' + getRow(item.row.programacaoId)  + '">' +
                '<input name="servicoPai[]" type="hidden" class="servicoPai" value="' + getRow(item.row.servicoPai)  + '">' +

                '<input name="origem[]" type="hidden" class="origem" value="' + getRow(item.row.origem)  + '">' +
                '<input name="destino[]" type="hidden" class="destino" value="' + getRow(item.row.destino)  + '">' +
                '<input name="nmFornecedor[]" type="hidden" class="nmFornecedor" value="' + getRow(item.row.nmFornecedor)  + '">' +
                '<input name="nmCategoria[]" type="hidden" class="nmCategoria" value="' + item_nome_categoria + '">' +

                '<input name="dateCheckin[]" type="hidden" class="dateCheckin" value="' + getRow(item.row.dateCheckin)  + '">' +
                '<input name="dateCheckOut[]" type="hidden" class="dateCheckOut" value="' + getRow(item.row.dateCheckOut)  + '">' +
                '<input name="dataEmissao[]" type="hidden" class="dataEmissaoItem" value="' + getRow(item.row.dataEmissao)  + '">' +

                '<input name="reserva[]" type="hidden" class="reserva" value="' + getRow(item.row.reserva)  + '">' +
                '<input name="tipoDestinoItem[]" type="hidden" class="tipoDestinoItem" value="' + getRow(item.row.tipoDestino)  + '">' +
                '<input name="valorHospedagem[]" type="hidden" class="valorHospedagem" value="' + getNumber(item.row.valorHospedagem) + '">' +
                '<input name="valorPorFaixaEtaria[]" type="hidden" class="valorPorFaixaEtaria" value="' + getNumber(item.row.valorPorFaixaEtaria) + '">' +

                '<input name="adicional[]" type="hidden" class="adicional" value="' + adicional + '">' +
                '<input name="tipoTransporte[]" type="hidden" class="tipoTransporte" value="' + getRow(tipoTransporte) + '">' +
                '<input name="localEmbarque[]" type="hidden" class="localEmbarque" value="' + getRow(localEmbarque) + '">' +
                '<input name="nmTipoTransporte[]" type="hidden" class="nmTipoTransporte" value="' + getRow(item.row.nmTipoTransporte) + '">' +
                '<input name="tipoHospedagem[]" type="hidden" class="tipoHospedagem" value="' + getRow(tipoHospedagem) + '">' +

                '<input name="faixaEtaria[]" type="hidden" class="faixaEtaria" value="' + getRow(faixaEtaria) + '">' +
                '<input name="descontarVaga[]" type="hidden" class="descontarVaga" value="' + getRow(item.row.descontarVaga) + '">' +
                '<input name="nomeFaixa[]" type="hidden" class="nomeFaixa" value="' + getRow(item.row.nmFaixaEtaria) + '">' +

                '<input name="guia[]"               type="hidden" class="guia"              value="' + getRow(item.row.guia) + '">' +
                '<input name="motorista[]"          type="hidden" class="motorista"         value="' + getRow(item.row.motorista) + '">' +
                '<input name="horaEmbarque[]"       type="hidden" class="horaEmbarque"      value="' + getRow(item.row.hora_embarque) + '">' +
                '<input name="referencia_item[]"    type="hidden" class="referencia_item"   value="' + getRow(item.row.referencia_item) + '">' +
                '<input name="observacao_item[]"    type="hidden" class="observacao_item"   value="' + getRow(item.row.observacao_item) + '">' +

                '<input name="horaCheckin[]" type="hidden" class="horaCheckin" value="' + getRow(item.row.horaCheckin) + '">' +
                '<input name="horaCheckOut[]" type="hidden" class="horaCheckOut" value="' + getRow(item.row.horaCheckOut) + '">' +

                '<input name="nmTipoHospedagem[]" type="hidden" class="nmTipoHospedagem" value="' + getRow(item.row.nmTipoHospedagem) + '">' +
                '<input name="categoriaAcomodacao[]" type="hidden" class="categoriaAcomodacao" value="' + getRow(item.row.categoriaAcomodacao) + '">' +
                '<input name="regimeAcomodacao[]" type="hidden" class="regimeAcomodacao" value="' + getRow(item.row.regimeAcomodacao) + '">' +

                '<input name="receptivo[]" type="hidden" class="receptivo" value="' + getRow(item.row.receptivo) + '">' +
                '<input name="nmReceptivo[]" type="hidden" class="nmReceptivo" value="' + getRow(item.row.nmReceptivo) + '">' +

                '<input name="nmEmpresaManual[]" type="hidden" class="nmEmpresaManual" value="' + getRow(item.row.nmEmpresaManual) + '">' +
                '<input name="telefoneEmpresa[]" type="hidden" class="telefoneEmpresa" value="' + getRow(item.row.telefoneEmpresa) + '">' +
                '<input name="enderecoEmpresa[]" type="hidden" class="enderecoEmpresa" value="' + getRow(item.row.enderecoEmpresa) + '">' +

                '<input name="apresentacaoPassagem[]" type="hidden" class="apresentacaoPassagem" value="' + getRow(item.row.apresentacaoPassagem) + '">' +
                '<input name="dataApresentacao[]" type="hidden" class="dataApresentacao" value="' + getRow(item.row.dataApresentacao) + '">' +
                '<input name="horaApresentacao[]" type="hidden" class="horaApresentacao" value="' + getRow(item.row.horaApresentacao) + '">' +

                '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">' +
                '<input name="product_category[]" type="hidden" class="rid" value="' + item_categoria_id + '">' +

                '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">' +
                '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">' +
                '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">' +

                '<input name="poltronaClient[]" type="hidden" class="rpoltrona" value="' + getRow(item.row.poltronaClient) + '">' +
                '<input name="customerClient[]" type="hidden" class="rclient" value="' + getRow(item.row.customerClient) + '">' +
                '<input name="customerClientName[]" type="hidden" class="rclientnome" value="' + getRow(item.row.customerClientName) + '">' +

                '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">';

            //taxas do cliente
            if (item.taxas !== undefined) {
                tr_html += '' +
                    '<input name="tarifa[]" type="hidden" class="tarifa" value="' + getNumber(item.taxas.taxas_cliente.tarifa) + '">' +
                    '<input name="taxaAdicionalFornecedor[]" type="hidden" class="taxaAdicionalFornecedor" value="' + getNumber(item.taxas.taxas_cliente.taxaAdicionalFornecedor) + '">' +
                    '<input name="descontoTarifa[]" type="hidden" class="descontoTarifa" value="' + getNumber(item.taxas.taxas_cliente.descontoTarifa) + '">' +
                    '<input name="acrescimoTarifa[]" type="hidden" class="acrescimoTarifa" value="' + getNumber(item.taxas.taxas_cliente.acrescimoTarifa) + '">' +

                    '<input name="adicionalBagagem[]" type="hidden" class="adicionalBagagem" value="' + getNumber(item.taxas.taxas_cliente.adicionalBagagem) + '">' +
                    '<input name="adicionalReservaAssento[]" type="hidden" class="adicionalReservaAssento" value="' + getNumber(item.taxas.taxas_cliente.adicionalReservaAssento) + '">' +

                    '<input name="valorPagoFornecedor[]" type="hidden" class="valorPagoFornecedor" value="' + getNumber(item.taxas.taxas_cliente.valorPagoFornecedor) + '">' +
                    '<input name="formaPagamentoFornecedorCustomer[]" type="hidden" class="formaPagamentoFornecedorCustomer" value="' + getRow(item.taxas.taxas_cliente.formaPagamentoFornecedorCustomer) + '">' +
                    '<input name="bandeiraPagoFornecedor[]" type="hidden" class="bandeiraPagoFornecedor" value="' + getRow(item.taxas.taxas_cliente.bandeiraPagoFornecedor) + '">' +
                    '<input name="numeroParcelasPagoFornecedor[]" type="hidden" class="numeroParcelasPagoFornecedor" value="' + getNumber(item.taxas.taxas_cliente.numeroParcelasPagoFornecedor) + '">' +
                    '<input name="descricaoServicos[]" type="hidden" class="descricaoServicos" value="' + getRow(item.taxas.taxas_cliente.descricaoServicos) + '">' +
                    '<input name="observacaoServico[]" type="hidden" class="descricaoServicos" value="' + getRow(item.taxas.taxas_cliente.observacaoServico) + '">';

                tr_html += '' +
                    '<input name="desconto_fornecedor[]" type="hidden" class="roption" value="' + getRow(item.taxas.taxas_fornecedor.descontoAbsolutoSupplier) + '">' +
                    '<input name="comissao_rav_du[]" type="hidden" class="roption" value="' + getRow(item.taxas.taxas_fornecedor.comissaoAbsolutoSupplier) + '">' +
                    '<input name="incentivoFornecedor[]" type="hidden" class="roption" value="' + getRow(item.taxas.taxas_fornecedor.incentivoAbsolutoSupplier) + '">' +
                    '<input name="outrasTaxasFornecedor[]" type="hidden" class="roption" value="' + getRow(item.taxas.taxas_fornecedor.outrasTaxasSupplier) + '">' +
                    '<input name="taxaCartaoFornecedor[]" type="hidden" class="roption" value="' + getRow(item.taxas.taxas_fornecedor.taxasCartaoSupplier) + '">';
            }

            tr_html += '<input name="product_comissao[]" type="hidden" class="roption" value="' + item_comissao + '">' +
                    '<input name="product_fornecedor[]" type="hidden" class="roption" value="' + item_fornecedor + '">' +
                    '<input name="product_tipo_cobranca_fornecedor[]" type="hidden" class="roption" value="' + item_tipo_cobranca_fornecedor + '">' +
                    '<input name="product_condicao_pagamento_fornecedor[]" type="hidden" class="roption" value="' + item_condicao_pagamento_fornecedor + '">' +
                    '<input name="product_valor_recebimento_fornecedor[]" type="hidden" class="roption" value="' + item_valor_recebimento_fornecedor + '">';

            //taxas do fornecedor
            if (adicional){
                tr_html +='<span style="margin-left: 20px;"><img src="'+site.base_url+'assets/images/plus.gif"></span>' +
                    '<span style="font-weight: bold;margin-left: 5px;"> <small> '+item_name+'</small></span>';
            } else if(getRow(item.row.digitado) === '1') {
                tr_html +='<span id="name_' + row_no + '">'+descricao+'</span>';
            } else {
               // tr_html +='<span class="sname" id="name_' + row_no + '">' + descricao + '</span>';
                tr_html +='<span id="name_' + row_no + '">'+descricao+'</span>';
            }

            tr_html +='</td>';

            if (adicional) {
                tr_html += '<td style="text-align: center;"></td>';
            } else {
                tr_html += '<td style="text-align: center;">'+data+'</td>';
            }

            if (site.settings.product_serial == 1)  {
                tr_html += '<td class="text-right"><input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="' + item_serial + '"></td>';
            }

            tr_html += '' +
                '<td class="text-right">' +
                '   <input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">' +
                '   <input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">' +
                '   <input class="mealuprice" name="mealuprice[]" type="hidden" value="' + item.row.price + '">' +
                '   <input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '">' +
                '   <span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price) + '</span>' +
                '</td>';

            tr_html += '<td><input class="form-control text-center rquantity" style="height: 25px;" tabindex="' + ((site.settings.set_focus == 1) ? an : (an + 1)) + '" name="quantity[]" type="text" value="' + formatDecimal(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onFocus="this.select();"></td>';

            if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
                tr_html += '<td class="text-right"><input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '"><span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(0 - (item_discount * item_qty)) + '</span></td>';
            }

            if (site.settings.tax1 == 1) {
                tr_html += '' +
                    '<td class="text-right">' +
                    '   <input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax.id + '">' +
                    '   <span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (parseFloat(pr_tax_rate) != 0 ? '(' + pr_tax_rate + ')' : '') + ' ' + formatMoney(pr_tax_val * item_qty) + '</span>' +
                    '</td>';
            }

            var sub_total = formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)));

            tr_html += '<td class="text-right"><span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + sub_total + '</span></td>';


            carregar_tabelas_dependentes(customerClient, item_id, sub_total, adicional, item_name, supdate);//aqui chama a tabela de passageiros do modal de adicao

            if (adicional) {
                tr_html += '<td class="text-center"></td>';
            } else {
                tr_html += '<td class="text-center"><i  class="pull-right fa fa-edit tip pointer edit" style="" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i></td>';
            }

            tr_html += '<td class="text-center"><i class="fa fa-times tip pointer sldel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';

            var newTrCategoria = $('<tr id="cat_row_' + row_no + '" class="row_' + item_id + '  row_'+row_no+'" data-item-id="' + item_id + '"></tr>');

            var tr_htmlCategoria = '';

            if (item_nome_categoria === 'Passagem Aérea') {
                tr_htmlCategoria = '<td colspan="9" style="font-weight: bold;text-align: left;border: 1px solid #ffffff;border-bottom: 1px solid #ddd;background: #eeeeee;"><i class="fa fa-plane"></i> ' + item_nome_categoria + '</td>';
            } else if (item_nome_categoria === 'Hospedagem') {
                tr_htmlCategoria = '<td colspan="9" style="font-weight: bold;text-align: left;border: 1px solid #ffffff;border-bottom: 1px solid #ddd;background: #eeeeee;"><i class="fa fa-bed"></i> ' + item_nome_categoria + '</td>';
            } else if (item_nome_categoria === 'Transfer'){
                tr_htmlCategoria = '<td colspan="9" style="font-weight: bold;text-align: left;border: 1px solid #ffffff;border-bottom: 1px solid #ddd;background: #eeeeee;"><i class="fa fa-exchange"></i> ' + item_nome_categoria + '</td>';
            } else  {
                if (adicional) {
                    tr_htmlCategoria = '<td colspan="9" style="display: none;font-weight: bold;text-align: left;border: 1px solid #ffffff;border-bottom: 1px solid #ddd;background: #eeeeee;"><i class="fa fa-map-signs"></i> ' + item_nome_categoria + '</td>';
                } else {
                    tr_htmlCategoria = '<td colspan="9" style="font-weight: bold;text-align: left;border: 1px solid #ffffff;border-bottom: 1px solid #ddd;background: #eeeeee;"><i class="fa fa-map-signs"></i> ' + item_nome_categoria + '</td>';
                }
            }

            newTrCategoria.html(tr_htmlCategoria);
            newTrCategoria.appendTo("#slTable");

            newTr.html(tr_html);

            newTr.appendTo("#slTable");

            loadItinerario(item);
            loadTransfers(item);
            loadServicosAdicionais(item);

            if (!adicional) {
                newTrCategoria = $('<tr></tr>');
                tr_htmlCategoria = '<td colspan="9" style="border: 1px solid #ffffff;"><br/></td>';
                newTrCategoria.html(tr_htmlCategoria);
            }

            total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)));
            count += parseFloat(item_qty);
            an++;

            if (item_type == 'standard' && totalPorItem > item_aqty) {
                $('#row_' + row_no).addClass('danger');
                if (site.settings.overselling != 1) {
                    $('#add_sale, #edit_sale, #enviar_faturar, #enviar_orcamento, #enviar_orcamento_edit').attr('disabled', true);
                }
            } else if (item_type == 'combo') {
                if (combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                    if (site.settings.overselling != 1) {
                        $('#add_sale, #edit_sale, #enviar_faturar, #enviar_orcamento, #enviar_orcamento_edit').attr('disabled', true);
                    }
                } else {
                    $.each(combo_items, function () {
                        if (parseFloat(this.quantity) < (parseFloat(this.qty) * item_qty) && this.type == 'standard') {
                            $('#row_' + row_no).addClass('danger');
                            if (site.settings.overselling != 1) {
                                $('#add_sale, #edit_sale, #enviar_faturar, #enviar_orcamento, #enviar_orcamento_edit').attr('disabled', true);
                            }
                        }
                    });
                }
            }
        });

        $('#total_meus_pacotes').html(parseFloat(count) - 1);

        var col = 3;

        if (site.settings.product_serial == 1) {
            col++;
        }

        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="' + col + '">Total</th><th class="text-center">' + formatNumber(parseFloat(count) - 1) + '</th>';

        if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
            tfoot += '<th class="text-right">' + formatMoney(product_discount) + '</th>';
        }

        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right">' + formatMoney(product_tax) + '</th>';
        }

        tfoot += '<th class="text-right">' + formatMoney(total) + '</th><th class="text-center"><i style="opacity:0.5; filter:alpha(opacity=50);"></i></th><th></th></tr>';
        $('#slTable tfoot').html(tfoot);

        // Order level discount calculations
        if (sldiscount = localStorage.getItem('sldiscount')) {
            var ds = sldiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    order_discount = parseFloat(((total) * parseFloat(pds[0])) / 100);
                } else {
                    order_discount = parseFloat(ds);
                }
            } else {
                order_discount = parseFloat(ds);
            }
            //total_discount += parseFloat(order_discount);
        }

        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (sltax2 = localStorage.getItem('sltax2')) {
                $.each(tax_rates, function () {
                    if (this.id == sltax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            invoice_tax = formatDecimal(((total - order_discount) * this.rate) / 100);
                        }
                    }
                });
            }
        }

        total_discount = parseFloat(order_discount + product_discount);

        // Totals calculations after item addition
        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + shipping);

        $('#total').text(formatMoney(total));
        $('#net_price_passengers').text(formatMoney(total));

        $('#titems').text((an - 1) + ' (' + formatNumber(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        //$('#tds').text('('+formatMoney(product_discount)+'+'+formatMoney(order_discount)+')'+formatMoney(total_discount));
        $('#tds').text(formatMoney(order_discount));

        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax));
        }

        $('#tship').text(formatMoney(shipping));
        $('#gtotal').text(formatMoney(gtotal));

        $('#valor').val(gtotal.toFixed(2));

        controllerSales.getParcelamento();

        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }

        if (count > 0) {
            //$('#slcustomer').select2("readonly", true);
            $('#slwarehouse').select2("readonly", true);
        }
        set_page_focus();
    }
}

function loadTransfers(item) {

    if (item.transfers === undefined) return;

    var itinerario = $('<tr></tr>');

    var tr_html = '<td colspan="10" style="padding: 0px;">';

    tr_html += '<table style="width: 100%;"><tbody>';

    sortedItems =  _.sortBy(item.transfers, function (o) {
        return [parseInt(o.order)];
    }) ;

    $(sortedItems).each(function( index, transfer ) {

        var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

        tr_html += '<tr>';

        tr_html += '<td style="border-bottom: 1px solid #ddd;">';

        tr_html += '<input name="transfer_item_id_'+item_id+'[]" type="hidden" value="' + item_id + '">';
        tr_html += '<input name="tipoTransfer_'+item_id+'[]" type="hidden" value="' + getRow(transfer.tipoTransfer) + '">';
        tr_html += '<input name="transfer_origem_'+item_id+'[]" type="hidden" value="' + getRow(transfer.origem)  + '">';
        tr_html += '<input name="transfer_destino_'+item_id+'[]" type="hidden" value="' + getRow(transfer.destino)  + '">';
        tr_html += '<input name="transfer_data_'+item_id+'[]" type="hidden" value="' + getRow(transfer.data)  + '">';
        tr_html += '<input name="transfer_hora_'+item_id+'[]" type="hidden" value="' + getRow(transfer.hora)  + '">';
        tr_html += '<input name="transfer_dataSaidaVooBus_'+item_id+'[]" type="hidden" value="' + getRow(transfer.dataSaidaVooBus)  + '">';
        tr_html += '<input name="transfer_horaSaidaBooBus_'+item_id+'[]" type="hidden" value="' + getRow(transfer.horaSaidaBooBus)  + '">';
        tr_html += '<input name="transfer_referenciaVooBus_'+item_id+'[]" type="hidden" value="' + getRow(transfer.referenciaVooBus)  + '">';
        tr_html += '<input name="transfer_referencia_'+item_id+'[]" type="hidden" value="' + getRow(transfer.referencia)  + '">';
        tr_html += '<input name="transfer_fornecedor_'+item_id+'[]" type="hidden" value="' + getRow(transfer.fornecedor)  + '">';
        tr_html += '<input name="transfer_dataEmissao_'+item_id+'[]" type="hidden" value="' + getRow(transfer.dataEmissao)  + '">';

        tr_html += '<span style="font-weight: bold;"></span><br/>&nbsp;'+transfer.tipoTransfer+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Origem </span><br/>'+transfer.origem+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Destino </span><br/>'+transfer.destino+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Data </span><br/>'+fsd(transfer.data)+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Hora </span><br/>'+transfer.hora+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Ref:. </span><br/>'+transfer.referencia+'</td>';

        tr_html += '</tr>';
    });

    tr_html += '</tbody></table>';
    tr_html += '</tr></td>';

    itinerario.html(tr_html);
    itinerario.appendTo("#slTable");
}

function loadItinerario(item) {

    if (item.itinerarios === undefined) return;

    var itinerario = $('<tr></tr>');

    var tr_html = '<td colspan="10" style="padding: 0px;">';

    tr_html += '<table style="width: 100%;"><tbody>';

    sortedItems =  _.sortBy(item.itinerarios, function (o) {
        return [parseInt(o.order)];
    }) ;

    $(sortedItems).each(function( index, itinerario ) {

        var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

        tr_html += '<tr>';

        tr_html += '<td style="border-bottom: 1px solid #ddd;">';

        tr_html += '<input name="itinerario_item_id_'+item_id+'[]" type="hidden" value="' + item_id + '">';
        tr_html += '<input name="origem_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.origem) + '">';
        tr_html += '<input name="destino_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.destino)  + '">';
        tr_html += '<input name="companhiaAerea_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.companhiaAerea)  + '">';
        tr_html += '<input name="assentoVoo_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.assentoVoo)  + '">';
        tr_html += '<input name="numeroVoo_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.numeroVoo)  + '">';
        tr_html += '<input name="dataPartida_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.dataPartida)  + '">';
        tr_html += '<input name="horaPartida_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.horaPartida)  + '">';
        tr_html += '<input name="dataChegada_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.dataChegada)  + '">';
        tr_html += '<input name="horaChegada_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.horaChegada)  + '">';
        tr_html += '<input name="horaChegada_'+item_id+'[]" type="hidden" value="' + getRow(itinerario.bagagem)  + '">';

        tr_html += '<span style="font-weight: bold;">&nbsp;De</span><br/>&nbsp;'+itinerario.origem+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Para </span><br/>'+itinerario.destino+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Cia </span><br/>'+itinerario.companhiaAerea+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Vôo </span><br/>'+itinerario.numeroVoo+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Assento </span><br/>'+itinerario.assentoVoo+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Partida </span><br/>'+fsd(itinerario.dataPartida)+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Embarque </span><br/>'+itinerario.horaPartida+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Chegada </span><br/>'+fsd(itinerario.dataChegada)+'</td>';
        tr_html += '<td style="border-bottom: 1px solid #ddd;"><span style="font-weight: bold;">Desembarque </span><br/>'+itinerario.horaChegada+'</td>';

        tr_html += '</tr>';

    });

    tr_html += '</tbody></table>';
    tr_html += '</tr></td>';

    itinerario.html(tr_html);
    itinerario.appendTo("#slTable");
}

function loadServicosAdicionais(item) {

    if (item.adicionais === undefined) return;

    var adicionais = $('<tr></tr>');

    var tr_html = '<td colspan="10" style="padding: 10px;">';

    tr_html += '<table style="width: 100%;"><tbody>';

    sortedItems =  _.sortBy(item.adicionais, function (o) {
        return [parseInt(o.order)];
    }) ;

    $(sortedItems).each(function( index, adicional ) {
        tr_html += '<tr data-item="'+adicional.item+'">';

        tr_html += '<td style="padding: 4px;">';

        tr_html += '<input name="adicional_item_id_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.item) + '">';
        tr_html += '<input name="adicional_nomeServico_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.nomeServico) + '">';
        tr_html += '<input name="adicional_preco_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.preco) + '">';
        tr_html += '<input name="adicional_nmCategoria_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.nmCategoria) + '">';
        tr_html += '<input name="adicional_servicoId_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.servicoId) + '">';
        tr_html += '<input name="adicional_servicoCode_'+adicional.item+'[]" type="hidden" value="' + getRow(adicional.servicoCode) + '">';

        tr_html += '<span style="font-weight: bold;">&nbsp;&nbsp;&nbsp;<img src="'+site.base_url+'assets/images/plus.gif">&nbsp;'+getRow(adicional.nmCategoria)+' '+getRow(adicional.nomeServico)+'<small> (R$'+getRow(adicional.preco)+')</small></span></td>';

        tr_html += '<td style="text-align: center;border: 1px solid #ddd;">' +
            '   <i class="fa fa-times tip pointer sldeladdicional" style="cursor:pointer;" id="'+adicional.item+'" data-item="'+adicional.item_pai+'" title="Edit"></i>' +
            '<td/>';


        tr_html += '</tr>';

    });

    tr_html += '</tbody></table>';
    tr_html += '</tr></td>';

    //adicionais.html(tr_html);
    //adicionais.appendTo("#slTable");
}


/* -----------------------------
 * Add Sale Order Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
function add_invoice_item(item) {

    if (count == 1) {
        slitems = {};

        if ($('#slwarehouse').val() && $('#slcustomer').val()) {
            //$('#slcustomer').select2("readonly", true);
            $('#slwarehouse').select2("readonly", true);
         } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }

    if (item == null) return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

    if (slitems[item_id]) {
        slitems[item_id].row.qty = parseFloat(slitems[item_id].row.qty) + 1;
    } else {
        slitems[item_id] = item;
    }

    slitems[item_id].order = new Date().getTime();
    localStorage.setItem('slitems', JSON.stringify(slitems));

    loadItems();
    return true;
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}

function adicionarNovoDependente() {
    var newHtMLTemplateDependents = '<tr>\n' +
        '\t<td><input type="text" class="form-control" name="dependenteNome" value=""></td>\n' +
        '\t<td>\n' +
        '\t\t<select class="form-control" name="dependenteTipoDocumento[]">\n' +
        '\t\t\t<option value="RG">RG</option>\n' +
        '\t\t\t<option value="CERTIDAO">CERTIDAO</option>\n' +
        '\t\t\t<option value="PASSAPORTE">PASSAPORTE</option>\n' +
        '\t\t\t<option value="OUTROS">OUTROS</option>\n' +
        '\t\t</select>\n' +
        '\t</td>\n' +
        '\t<td><input type="text" class="form-control" name="dependenteRG" value=""></td>\n' +
        '\t<td><input type="date" class="form-control" name="dependenteNascimento" value=""></td>\n' +
        '\t<td>\n' +
        '\t\t<select class="form-control" onblur="atualizarTiposDepemdentes();" name="dependenteTipo[]">\n' +
        '\t\t\t<option value="ADT">ADT</option>\n' +
        '\t\t\t<option value="CHD">CHD</option>\n' +
        '\t\t\t<option value="FREE">FREE</option>\n' +
        '\t\t</select>\n' +
        '\t</td>\n' +
        '\t<td>\n' +
        '\t\t<i class="fa fa-times tip pointer" title="Remove" style="cursor:pointer;"></i>\n' +
        '\t</td>\n' +
        '</tr>';
    $('#slTableDependentes tbody').append(newHtMLTemplateDependents);

    atualizarTiposDepemdentes();

    return false;
}

function calcularValorProduto() {

    var preco = $('#faixa_etaria').find('option:selected').attr('preco');
    var descontarVaga = $('#faixa_etaria').find('option:selected').attr('descontarVaga');
    var valorHospedagem = getNumber($('#tipo_hospedagem').find('option:selected').attr('preco'));

    if (preco !== undefined) {
        preco = getNumber(preco);
        $('#pprice').attr('disabled', true);
    } else {
        preco = getNumber($('#memoriapreco').val());
        $('#pprice').attr('disabled', false);
    }

    $('#valorHospedagem').val(valorHospedagem);
    $('#valorPorFaixaEtaria').val(preco);

    $('#pprice').val(valorHospedagem + preco);

    if (descontarVaga === '1') {
        $('#sdescontarVaga').html('SIM');
        $('#sdescontarVaga').css('color', 'green');
        $('#sdescontarVaga').css('font-weight', '600');


        $('#descontarVaga').attr('checked', true);
    } else {
        $('#sdescontarVaga').html('NÃO');
        $('#sdescontarVaga').css('color', 'red');
        $('#sdescontarVaga').css('font-weight', '600');

        $('#descontarVaga').attr('checked', false);
    }

    $('input[type="checkbox"],[type="radio"]').iCheck('update');

    $('#pprice').change();
    $('#pdiscount').keyup();
}

function atualizarTiposDepemdentes() {

    var totalAdultos = 0;
    var totalCriancas = 0;
    var totalFree = 0;

    $("select[name='dependenteTipo[]']").each(function(){
        if ($(this).val() === 'ADT') totalAdultos++;
        if ($(this).val() === 'CHD') totalCriancas++;
        if ($(this).val() === 'FREE') totalFree++;
    });

    $('#qtdAdultos').val(totalAdultos);
    $('#qtdCriancas').val(totalCriancas);
    $('#qtdFree').val(totalFree);
}

function buscarFaixaEtariaHospedagem(productId, faixaEtaria) {

    $.ajax({
        url: site.base_url + "saleitem/getFaixasTipoHospedagemProduto/",
        dataType: 'json',
        type: 'get',
        async: true,
        data: {
            productId : productId
        },
    }).done(function (data) {

        if (data.length > 0) {

            console.log('montando tipo faixa valor por: hospedagem');

            $('#faixa_etaria').empty();

            criarOpcaoSelecioneUmaOpcao('faixa_etaria');

            $(data).each(function (index, option) {
                var newOption;

                if (index === 0) {
                    newOption = new Option(option.name, option.faixaId, false, true);
                } else {
                    newOption = new Option(option.name, option.faixaId, false, false);
                }

                newOption.setAttribute("preco", 0);
                newOption.setAttribute('descontarVaga', option.descontarVaga);
                newOption.setAttribute('nomeFaixa', option.name);

                $('#faixa_etaria').append(newOption);
            });

            preenche_staff();

            if (faixaEtaria !== '') {
                $('#faixa_etaria').val(faixaEtaria).trigger('change');
            } else {
                $('#faixa_etaria').val('').trigger('change');
            }
        }

    });
}

function buscarFaixaEtariaBateVolta(productId, faixaEtaria) {

    $.ajax({
        url: site.base_url + "saleitem/getValoresPorFaixaEtaria/",
        dataType: 'json',
        type: 'get',
        async: true,
        data: {
            productId : productId,
            programacaoId: $('#programacaoId').val(),
        },
    }).done(function (data) {

        if (data.length > 0) {

            console.log('montando tipo faixa valor por: bate volta');

            $('#faixa_etaria').empty();
            criarOpcaoSelecioneUmaOpcao('faixa_etaria');

            $(data).each(function (index, option) {
                var newOption;

                if (index === 0) {
                    newOption = new Option(option.text, option.faixaId, false, true);
                } else {
                    newOption = new Option(option.text, option.faixaId, false, false);
                }

                newOption.setAttribute("preco", option.valor);
                newOption.setAttribute('descontarVaga', option.descontarVaga);
                newOption.setAttribute('nomeFaixa', option.name);

                $('#faixa_etaria').append(newOption);
            });

            preenche_staff();

            if (faixaEtaria !== '') {
                $('#faixa_etaria').val(faixaEtaria).trigger('change');
            } else {
                $('#faixa_etaria').val('').trigger('change');
            }
        }
    });
}

function preenche_staff() {
    $(staff).each(function (index, st) {
        var newOption;

        if (index === 0) {
            newOption = new Option(st.name, st.id, false, true);
        } else {
            newOption = new Option(st.name, st.id, false, false);
        }

        newOption.setAttribute("preco", 0);
        newOption.setAttribute('descontarVaga', st.descontarVaga);
        newOption.setAttribute('nomeFaixa', st.name);

        $('#faixa_etaria').append(newOption);
    });
}

function buscarLocalEmbarque(productId, localEmbarque) {

    $('#local_embarque').empty();
    $.ajax({
        url: site.base_url + "saleitem/getLocaisEmbarque",
        dataType: 'json',
        type: 'get',
        async: true,
        data: {
            productId: productId
        }
    }).done(function (locaisEmbarques) {

        $('#div_local_embarque').hide();

        $(locaisEmbarques).each(function (index, option) {
            var selected = false;

            if (localEmbarque === option.id) {
                selected = true;
            }

            var newOption = new Option(option.text, option.id, false, selected);

            $('#local_embarque').append(newOption).trigger('change');

            $('#div_local_embarque').show();
        });
    });
}

function buscarTipoTransporte(productId, tipoTransporte) {
    $('#tipo_transporte').empty();

    $.ajax({
        url: site.base_url + "saleitem/getTransportes",
        dataType: 'json',
        type: 'get',
        async: true,
        data: {
            productId: productId
        }
    }).done(function (data) {

        $('#div_tipo_transporte').hide();

        $( data ).each(function( index , option ) {
            var selected = false;

            if (tipoTransporte === option.id) {
                selected = true;
            }

            var newOption = new Option(option.text, option.id, false, selected);
            $('#tipo_transporte').append(newOption).trigger('change');

            $('#div_tipo_transporte').show();
        });
    });
}

function buscarServicosAdicionais(productId, faixaId) {

    $('#tbServicosAdicionais tbody').empty();

    $.ajax({
        url: site.base_url + "saleitem/getServicosAdicionais",
        dataType: 'json',
        type: 'get',
        //async: false,
        data: {
            productId : productId,
            faixaId: faixaId
        }
    }).done(function (data) {

        var tableNew = '<tr>';
        $('#div_servicos_adicionais').hide();

        $( data ).each(function( index , servico ) {

            $('#div_servicos_adicionais').show();

            tableNew += '<tr>' +
                '<td><input type="hidden" value="'+servico.id+'" name="servicoAdicionalId[]" />' +
                '   <input type="checkbox" name="servicoAdicional[]"></td>' +
                '<td><input type="hidden" value="'+servico.category_id+'" name="categoriaId[]" />' +
                '    <input type="hidden"  value="'+servico.code+'" name="codigoProdutoAdicional[]" />' +
                '    <input  type="hidden" value="'+servico.category+'" name="nmCategoriaServicoAdicional[]">'+servico.category+'</td>' +
                '<td><span name="nmServicoAdicional[]">'+servico.text+'</span></td>' +
                '<td style="text-align: right;"><span>'+formatMoney(servico.price)+'</span><span name="priceServicoAdicional[]" style="display: none;">'+servico.price+'</span></td></tr>';

        });

        $('#tbServicosAdicionais tbody').show().append(tableNew);

        $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });
    });
}

function criarOpcaoSelecioneUmaOpcao(tag) {
    var newSelectOption = new Option('Selecione uma opção', '', false, false);

    $('#'+tag).append(newSelectOption).trigger('change');
}


function criarOpcaoCombox(tag, tipo) {
    var newSelectOption = new Option(tipo, tipo, false, false);

    $('#'+tag).append(newSelectOption);
}

function getRow(str) {
    return str !== '' && str  !== undefined && str  !== null ? str : '';
}

function isRow(str) {
    return str !== '' && str !== undefined && str !== null;
}

function contadorTotalItens(item) {

    var sortedItemsQtd = (site.settings.item_addition == 1) ? _.sortBy(slitems, function (o) {
        return [parseInt(o.order)];
    }) : slitems;

    var totalPorItem = 0;
    $.each(sortedItemsQtd, function (index, obj) {
        if (obj.row.adicional === false) {
            if (item.item_id === obj.item_id) {
                totalPorItem = totalPorItem + obj.row.qty;
            }
        }
    });

    return totalPorItem;
}


function editarItem(row) {

    var item_id         = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = {};
    var faixaEtaria     = $('#faixa_etaria').val();
    var customerClient  = $('#customerClient').val();

    if (customerClient === '') {
        alert('Passageiro é obrigatório para ser adicionado na venda.');
        $('#customerClient').select2('open');

        return false;
    }

    if (faixaEtaria === '') {
        alert('Faixa Etária é obrigatório para incluir o passageiro.')
        return false;
    }

    if ($('#tipo_hospedagem').siblings('.select2-container').is(':visible')) {
        if ($('#tipo_hospedagem option').length > 0) {
            if ($('#tipo_hospedagem').val() === '') {
                alert('Selecione um tipo de hospedagem para compor o pacote!')
                return false;
            }
        }
    }

    if (new_pr_tax) {
        $.each(tax_rates, function () {
            if (this.id == new_pr_tax) {
                new_pr_tax_rate = this;
            }
        });
    } else {
        new_pr_tax_rate = false;
    }

    var price = parseFloat($('#pprice').val());
    var valorHospedagem = getNumber($('#valorHospedagem').val());
    var valorPorFaixaEtaria = getNumber($('#valorPorFaixaEtaria').val());

    if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
        if (!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
            bootbox.alert(lang.unexpected_value);
            return false;
        }
    }

    if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return;
    }

    var customerClientName  = $('#customerClient').select2('data').text;
    var poltronaClient      = $('#poltronaClient').val();

    var localEmbarque   = $('#local_embarque').val();
    var tipoHospedagem  = $('#tipo_hospedagem').val();
    var tipoTransporte  = $('#tipo_transporte').val();

    var nmLocalEmbarque = '';
    var nmTipoHospedagem = '';
    var nmTipoTransporte = '';

    var hora_embarque = $('#hora_embarque').val();
    var referencia_item = $('#referencia_item').val();
    var observacao_item = $('#observacao_item').val();

    if (localEmbarque !== '' && localEmbarque !== null) {
        nmLocalEmbarque = $('#local_embarque').select2('data').text;
    }

    if (tipoHospedagem !== '' && tipoHospedagem !== null) {
        nmTipoHospedagem = $('#tipo_hospedagem').select2('data').text;
    }

    if (tipoTransporte !== '' && tipoTransporte != null) {
        nmTipoTransporte =  $('#tipo_transporte').select2('data').text;
    }

    var nmFaixaEtaria = $('#faixa_etaria').find('option:selected').attr('nomefaixa');
    var descontarVaga = $('#descontarVaga').is(":checked");

    slitems[item_id].row.qty = parseFloat($('#pquantity').val()),

        slitems[item_id].row.poltronaClient = poltronaClient,
        slitems[item_id].row.customerClient = customerClient,
        slitems[item_id].row.customerClientName = customerClientName,

        slitems[item_id].row.hora_embarque = hora_embarque,
        slitems[item_id].row.referencia_item = referencia_item,
        slitems[item_id].row.observacao_item = observacao_item,

        slitems[item_id].row.tipoTransporte = tipoTransporte,
        slitems[item_id].row.nmTipoTransporte = nmTipoTransporte,

        slitems[item_id].row.localEmbarque = localEmbarque,
        slitems[item_id].row.nmLocalEmbarque = nmLocalEmbarque,

        slitems[item_id].row.tipoHospedagem = tipoHospedagem,
        slitems[item_id].row.nmTipoHospedagem = nmTipoHospedagem,

        slitems[item_id].row.faixaEtaria = faixaEtaria,
        slitems[item_id].row.nmFaixaEtaria = nmFaixaEtaria,

        slitems[item_id].row.real_unit_price = price,
        slitems[item_id].row.valorHospedagem = valorHospedagem;
    slitems[item_id].row.valorPorFaixaEtaria = valorPorFaixaEtaria;
    slitems[item_id].row.tax_rate = new_pr_tax,
        slitems[item_id].tax_rate = new_pr_tax_rate;

    if (descontarVaga) {
        slitems[item_id].row.descontarVaga = '1';
    } else {
        slitems[item_id].row.descontarVaga = '0';
    }

    slitems[item_id].row.discount = $('#pdiscount').val() ? $('#pdiscount').val() : '';
    slitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '';
    slitems[item_id].row.serial = $('#pserial').val();
    slitems[item_id].supdate = 'U';

    localStorage.setItem('slitems', JSON.stringify(slitems));

    adicionarServicosAdicionais(slitems[item_id].order, slitems[item_id].item_id, slitems[item_id].row.programacaoId);

    $('#updateItem').val('1');

    return true;
}

function adicionarServicosAdicionais(order, servicoPai, programacaoId) {

    $("input[name='servicoAdicional[]']").each(function(index){
        if ($("[name='servicoAdicional[]']")[index].checked) {

            var faixaId     = $('#faixa_etaria').val();
            var nmFaixaEtaria = $('#faixa_etaria').find('option:selected').attr('nomefaixa');

            var mqty = 1;
            var mtax = 0;
            var discount = 0;

            var nomePassageiro = $('#customerClient').select2('data').text;
            var clienteId = $('#customerClient').val();

            var nomePacote = $("[name='nmServicoAdicional[]']")[index].innerHTML;
            var unit_price = getNumber($("[name='priceServicoAdicional[]']")[index].innerHTML);
            var nmCategoria = $("[name='nmCategoriaServicoAdicional[]']")[index].value
            var mcode = $("[name='codigoProdutoAdicional[]']")[index].value;
            var produt_id = $("[name='servicoAdicionalId[]']")[index].value;

            if (nomePacote === '') return;

            var mtax_rate = {};

            $.each(tax_rates, function () {
                if (this.id === mtax) mtax_rate = this;
            });

            var mid =  new Date().getTime();

            slitems[mid] = {
                "id": mid,
                "item_id": mid,
                "label": nomePacote,
                "row": {
                    "id": produt_id,
                    "servicoPai": servicoPai,
                    "programacaoId" : programacaoId,
                    "code": mcode,
                    "name": nomePacote,
                    "quantity": mqty,
                    "price": unit_price,
                    "unit_price": unit_price,
                    "real_unit_price": unit_price,
                    "tax_rate": mtax,
                    "qty": mqty,
                    "discount": String(discount),

                    "faixaEtaria": faixaId,
                    "nmFaixaEtaria": nmFaixaEtaria,

                    "customerClient": clienteId,
                    "customerClientName": nomePassageiro,

                    'category_name' : nmCategoria,
                    'categoria_id' : '',

                    "tax_method": 0,
                    "type": "standard",

                    "serial": "",
                    "option": "",
                    'adicional':  true,
                },
                "tax_rate": mtax_rate,
                "options": false,
                "order" : mid,
            };
            localStorage.setItem('slitems', JSON.stringify(slitems));
        }
    });

    $("[name='servicoAdicional[]']").iCheck('uncheck');
}
