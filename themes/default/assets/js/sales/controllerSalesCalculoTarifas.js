var ControllerSalesCalculoTarifas = function () {

    this.onLoad = onLoad;
    this.calculateCustomerValues = calculateCustomerValues;

    function onLoad() {
        onkeyPress();
    }

    function onkeyPress() {

        $('#slTarifaCurrencieCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slTaxaFornecedorCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slTaxaBagagemCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slReservaAssentoCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slTaxasAdicionaisCartaoCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slTaxasAdicionaisDescontoCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slVlPagamentoFornecedorCustomer').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slDescontoAbsolutoSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slComissaoAbsolutoSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slIncentivoAbsolutoSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slOutrasTaxasSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slTaxasCartaoSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slComissaoConsultorAbsolutoSupplier').keyup(function (event) {
            calculateCustomerValues();
        });

        $('#slComissaoConsultorPercentualSupplier').keyup(function (event){
           calcularValorComissaoPorPercentual();
        });
    }

    function calculateCustomerValues() {

        let slTarifaCurrencieCustomer = formatDecimal($('#slTarifaCurrencieCustomer').val());
        let slTaxaFornecedorCustomer = formatDecimal($('#slTaxaFornecedorCustomer').val());

        let slTaxaBagagemCustomer = formatDecimal($('#slTaxaBagagemCustomer').val());
        let slReservaAssentoCustomer = formatDecimal($('#slReservaAssentoCustomer').val());

        let slTaxasAdicionaisCartaoCustomer = formatDecimal($('#slTaxasAdicionaisCartaoCustomer').val());
        let slTaxasAdicionaisDescontoCustomer = formatDecimal($('#slTaxasAdicionaisDescontoCustomer').val());

        let slVlPagamentoFornecedorCustomer = formatDecimal($('#slVlPagamentoFornecedorCustomer').val());

        let tarifaTaxa = slTarifaCurrencieCustomer+slTaxaFornecedorCustomer;
        let totalBruto = tarifaTaxa + slTaxaBagagemCustomer + slReservaAssentoCustomer + slTaxasAdicionaisCartaoCustomer - slTaxasAdicionaisDescontoCustomer;

        let totalLiquido = totalBruto - slVlPagamentoFornecedorCustomer;

        //set
        $('#slTarifaRealCustomer').val(slTarifaCurrencieCustomer);

        //set format
        $('#tarifaTaxaCustomer').html(format(tarifaTaxa));
        $('#vlTotalBruto').html(format(totalBruto));
        $('#vlLiquidoCustomer').html(format(totalLiquido));

        calculateSupplierValues();
    }

    function calculateSupplierValues() {

        let slTarifaCurrencieCustomer = formatDecimal($('#slTarifaCurrencieCustomer').val());
        let slTaxaFornecedorCustomer = formatDecimal($('#slTaxaFornecedorCustomer').val());
        let slDescontoAbsolutoSupplier = formatDecimal($('#slDescontoAbsolutoSupplier').val());
        let slComissaoAbsolutoSupplier = formatDecimal($('#slComissaoAbsolutoSupplier').val());
        let slIncentivoAbsolutoSupplier = formatDecimal($('#slIncentivoAbsolutoSupplier').val());
        let slOutrasTaxasSupplier =  formatDecimal($('#slOutrasTaxasSupplier').val());
        let slTaxasCartaoSupplier = formatDecimal($('#slTaxasCartaoSupplier').val());
        let slVlPagamentoFornecedorCustomer = formatDecimal($('#slVlPagamentoFornecedorCustomer').val());
        let slComissaoConsultorAbsolutoSupplier = formatDecimal($('#slComissaoConsultorAbsolutoSupplier').val());

        let tarifaTaxa = slTarifaCurrencieCustomer + slTaxaFornecedorCustomer;
        let totalDesconto = slDescontoAbsolutoSupplier + slComissaoAbsolutoSupplier + slIncentivoAbsolutoSupplier;
        let totalPagamentoFornecedor = tarifaTaxa - slVlPagamentoFornecedorCustomer - totalDesconto + slOutrasTaxasSupplier + slTaxasCartaoSupplier;

        //set format
        $('#vlTarifaTaxaSupplier').html(format(tarifaTaxa));
        $('#vlTotalPagamentoFornecedor').html(format(totalPagamentoFornecedor));

        $('#vlTotalCustomer').html(format(totalDesconto - slComissaoConsultorAbsolutoSupplier));
        $('#receitaAgencia').val(totalDesconto);

        //set
        $('#vlTotalPagamentoFornecedorInput').val(totalPagamentoFornecedor);
        $('#slVlPagamentoFornecedorRealCustomer').val(slVlPagamentoFornecedorCustomer);
        $('#slTaxaSupplier').val(slTaxaFornecedorCustomer);
        $('#slTarifaCurrencieSupplier').val(slTarifaCurrencieCustomer);
        $('#slTarifaRealSupplier').val(slTarifaCurrencieCustomer);
        $('#slPagamentoFornecedorSupplier').val(slVlPagamentoFornecedorCustomer);
    }

    function format(valor) {
        return currencyFormat(valor, "text-center");
    }

    function calcularValorComissaoPorPercentual() {

        let receitaAgencia = formatDecimal($('#receitaAgencia').val());
        let comissaoDigitada = formatDecimal($('#slComissaoConsultorPercentualSupplier').val());
        let comissao = 0;

        if (comissaoDigitada > 0 && receitaAgencia >0)  comissao = receitaAgencia*(comissaoDigitada/100);

        $('#slComissaoConsultorAbsolutoSupplier').val(comissao.toFixed(2));
        calculateCustomerValues();
    }
}