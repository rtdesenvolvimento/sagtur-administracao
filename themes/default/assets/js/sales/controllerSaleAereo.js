var ControllerSaleAereo = function () {

    this.onLoad  = onLoad;

    function onLoad() {
        onClick();
    }

    function onClick() {

        $('#adicionar-formulario-aereo').click(function (event) {

            if (!controllerSales.isPreencheuFilial() ) return;

            adicionarFormularioAereo();
        });
    }

    function adicionarFormularioAereo() {

        $('#pacotes-com-aereo').load(site.base_url + "sales/view_aereo", function(event) {

            $('#slTable').hide();

            buscarFormularioTaxasClienteFornecedor();

            $('#barra-remover').show();
            $('#sticker').hide();

            $('#descricaoCadastroManual').html('Adicionar Passagem Aérea');

            $('#iconCadastroManual').removeClass('fa-plus');
            $('#iconCadastroManual').removeClass('fa-briefcase');
            $('#iconCadastroManual').addClass('fa-plane');

            autocompleteOrigemDestino();

            $('#supplierPassagemAerea').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url+"suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if(data.results != null) {
                            return { results: data.results };
                        } else {
                            return { results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('.addItinerario').click(function (event){

                $.ajax({
                    url: site.base_url + "sales/view_aereo_itinerario",
                    dataType: 'html',
                    type: 'get',
                }).done(function (html) {
                    $('#pacotes-com-aereo-itinerario').append(html);

                    $('.removeItinerario').click(function (event) {
                        $(this).parent().parent().remove()
                    });

                    autocompleteOrigemDestino();

                    controllerSalesPacote.onLoadCarregarConfiguracaoItem();

                });
            });

            $('#adicionar-pacote-aereo').click(function (event){
                salvarAereo();
            });

        });
    }

    function buscarFormularioTaxasClienteFornecedor() {

        $('#load-new-pacote-aereo').load(site.base_url+"sales/adicionarPasseio", function() {

            $('#divDadosAereo').show();

            setCampoObrigatorio('lsCompanhiaPassagemAerea');
            setCampoObrigatorio('slEmissaoPassagemAerea');
            setCampoObrigatorio('supplierPassagemAerea');
            setCampoObrigatorio('numeroBilete');

            setCampoObrigatorioPorClasse('obrigatorio');

            controllerSalesPacote.onLoadCarregarConfiguracaoItem();

            $('#slEmissaoPassagemAerea').val(controllerSalesPacote.dataAtualFormatada());

            $('#lsCompanhiaPassagemAerea').focus();

        });
    }

    function setCampoObrigatorio(atributo) {
        $('#'+atributo).attr("required", true);
    }

    function setCampoObrigatorioPorClasse(atributo) {
        $('.'+atributo).attr("required", true);
    }

    function autocompleteOrigemDestino() {

        $(".autocomplete_origem_destino").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: site.base_url+"sales/getLocais",
                    dataType: "json",
                    data: {
                        term : request.term,
                    },
                    success: function (data) {
                        if (data !== '') response(JSON.parse(data));
                        else response([]);
                    }
                });
            },
            minLength: 3,
            autoFocus: false,
            delay: 200,
            placeholder: 'Digite uma origem/aeroporto',
            response: function (event, ui) {},
            select: function (event, ui) {}
        });
    }

    function salvarAereo() {

        if (!controllerSales.isPreencheuFilial() ) return;

        if (!isCamposObrigatoriosPreenchidosAntesSalvar()){
            bootbox.alert(lang.select_above);

            $('#hotelManual').focus();

            return false;
        }

        let mid = (new Date).getTime();

        let mcode = mid;
        let mqty = 1;
        let mtax = 0;

        let nomePassageiro = $('#search-customer').select2('data').text;
        let clienteId = $('#search-customer').val();

        let categoria = 'Passagem Aérea';//TODO
        let categoriaId = 7;//TODO

        let fornecedor = $('#supplierPassagemAerea').val();
        let nmFornecedor = $('#supplierPassagemAerea').select2('data').text;

        let apresentacaoPassagem = $('#apresentacaoPassagem').val();
        let dataApresentacao = $('#dataApresentacao').val();
        let horaApresentacao = $('#horaApresentacao').val();
        let numeroBilhete = $('#numeroBilhete').val();

        let tipoDestino = $('#slTipoDestino').val();
        let dataEmissao = $('#slEmissaoPassagemAerea').val();

        let nomePacote = 'Passagem Aérea Bilhete '+numeroBilhete;

        let reserva = $('#slLocalizadorPassagemAerea').val();

        let unit_price = getNumber($('#vlLiquidoCustomer').find('div').html());
        let mdiscount = getNumber($('#slTaxasAdicionaisDescontoCustomer').val());
        let comissao = getNumber($('#slComissaoConsultorAbsolutoSupplier').val());

        let tipoCobrancaFornecedorId = $('#tipoCobrancaFornecedorId').val();
        let condicaoPagamentoFornecedorId = $('#condicaoPagamentoFornecedorId').val();
        let vlTotalPagamentoFornecedorInput = $('#vlTotalPagamentoFornecedorInput').val();

        let mtax_rate = {};

        slitems[mid] = {
            "id": mid,
            "item_id": mid,
            "label": nomePacote,
            "row": {
                "id": mid,
                "code": mcode,
                "name": nomePacote,
                "quantity": mqty,
                "price": unit_price,
                "unit_price": unit_price,
                "real_unit_price": unit_price,
                "tax_rate": mtax,
                "tax_method": 0,
                "qty": mqty,
                "type": "manual",
                "discount": String(mdiscount),
                "serial": "",
                "option": "",

                "customerClient" :clienteId,
                "customerClientName" : nomePassageiro,

                "fornecedor" : fornecedor,
                'nmFornecedor' : nmFornecedor,

                "dataEmissao" : dataEmissao,

                "reserva" : reserva,

                "tipoDestino" : tipoDestino,
                "apresentacaoPassagem" : apresentacaoPassagem,
                "dataApresentacao" : dataApresentacao,
                "horaApresentacao" : horaApresentacao,
                "numeroBilhete" : numeroBilhete,

                "tipoCobrancaFornecedorId": tipoCobrancaFornecedorId,
                "condicaoPagamentoFornecedorId": condicaoPagamentoFornecedorId,
                "vlTotalPagamentoFornecedorInput": vlTotalPagamentoFornecedorInput,

                "comissao" : comissao,

                "category_name" : categoria,
                "categoria_id": categoriaId,

                "digitado" : '1'
            },
            "tax_rate": mtax_rate,
            "options": false
        };

        adicionarItinerario(slitems[mid]);

        controllerSalesPacote.preencherTaxasCliente(slitems[mid]);
        controllerSalesPacote.preencherDadosCliente(slitems[mid]);

        localStorage.setItem('slitems', JSON.stringify(slitems));

        loadItems();

        controllerSalesPacote.limparDadosFormularioPacotes();
        //audio_success.play();
    }

    function adicionarItinerario(slitems) {

        $($('input[name="slOrigemPassagemAerea[]"]') ).each(function(index) {

            let mid = (new Date).getTime();

            let origemPacote = $('input[name="slOrigemPassagemAerea[]"]')[index].value;
            let destinoPacote = $('input[name="destinoPassagemAerea[]"]')[index].value;

            let dataPartida = $('input[name="dataSaidaPassagemAerea[]"]')[index].value;
            let horaPartida = $('input[name="horaSaidaPassagemAerea[]"]')[index].value;

            let dataChegada = $('input[name="dataChegadaPassagemAerea[]"]')[index].value;
            let horaChegada = $('input[name="horaChegadaPassagemAerea[]"]')[index].value;

            let numeroVoo = $('input[name="vooPassagemAerea[]"]')[index].value;
            let assentoVoo = $('input[name="assento[]"]')[index].value;
            let bagagem = $('input[name="bagagem[]"]')[index].value;
            let companhiaAerea =$('input[name="companhiaPassagemAerea[]"]')[index].value;

            debugger;

            if (slitems.itinerarios === undefined) slitems.itinerarios = {}

            slitems.itinerarios[mid] = {
                "mid" : mid,
                "item" : slitems.id,

                "companhiaAerea": companhiaAerea,
                "origem": origemPacote,
                "destino": destinoPacote,
                "dataPartida": dataPartida,
                "horaPartida": horaPartida,

                "dataChegada": dataChegada,
                "horaChegada": horaChegada,

                "numeroVoo": numeroVoo,
                "assentoVoo": assentoVoo,
                "bagagem": bagagem,
            }
        });
    }

    function isCamposObrigatoriosPreenchidosAntesSalvar() {

        var customer =  $('#search-customer').val();
        var supplierHospedagem =  $('#supplierPassagemAerea').val();

        return $('form[data-toggle="validator"]').data('bootstrapValidator').validate().isValid() &&
            customer !== '' &&
            supplierHospedagem !== '';
    }
}