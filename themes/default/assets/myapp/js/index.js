function getObjeto(atributo) {
    return $('#'+atributo);
}
function getString(atributo) {
    var str = $("#"+atributo).val();
    if (str === '_') str = '';
    if (str === undefined) str = '';
    return str.trim();
}

function getInteger(atributo) {
    return parseInt(getString(atributo));
}

function getImagem(atributo) {
    var image = document.getElementById(atributo);
    return image.src;
}

function setString(atributo, valor) {
    $('#'+atributo).val(valor);
}

function setCheckBox(atributo, valor) {
    if (valor === '1') $("#"+atributo).prop("checked", true).checkboxradio("refresh");
    if (valor === '0') $(""+atributo).prop("checked", false).checkboxradio("refresh");
}

function addAtributte(campo, atributo, valor) {
    $('#'+campo).attr(atributo, valor);
}

function setImagem(atributo, imageData) {
    var image = document.getElementById(atributo);
    image.src = imageData;
}

function setCombo(atributo, valor) {
    if (isNaN(valor)) {
        $('#' + atributo).val(valor).selectmenu('refresh');
    } else {
        $('#' + atributo).val(parseInt(valor)).selectmenu('refresh');
    }
}

function getBoolean(atributo) {
    var valor = $("#"+atributo).val();
    if (valor  === 'off')
        return 1;
    else return 0;
}

function setBoolean(atributo, valor) {
    if (valor === 1) {
        $('#'+atributo).val('off');
    } else {
        $('#'+atributo).val('on');
    }
    $("#"+atributo).trigger('change');
}

function preencherComboAtributos(atributo, json) {
    $("#"+atributo).empty();
    $('#'+atributo).append($('<option>', {
        value: '',
        text : 'Selecione uma opção'
    }));
    jQuery.each(json, function(i, item) {
        $('#'+atributo).append($('<option>', {
            value: item.id,
            text : item.descricao
        }));
    });
}

function replaceAll(str, de, para) {
    var pos = str.indexOf(de);
    while (pos > -1){
        str = str.replace(de, para);
        pos = str.indexOf(de);
    }
    return str.trim();
}

function abrirFotoNavegador() {
    alert(window.location);
    var ref = cordova.InAppBrowser.open('http://apache.org', '_blank', 'location=yes');
}

function parseDouble(strValor) {

    if (strValor === 'undefined') return 0;
    if (strValor === undefined) return 0;
    if (strValor === '') return 0;
    if (strValor === null) return 0;

    if (!isNaN(strValor)) return parseFloat(strValor);

    var str = replaceAll(strValor, ',','.');
    return parseFloat(str);
}

function addOptionToSelect(select, value, text) {
    $('#'+select).append($('<option>', {
        value: value,
        text: text
    }));
}

function showPageLoading(mensagem) {
    $.mobile.loading( "show", {
        text: mensagem,
        textVisible: mensagem,
        theme: $.mobile.loader.prototype.options.theme,
        textonly: false,
        html: ''
    });
}

function hidePageLoading() {
    $('#hide-page-loading-msg').click();
}