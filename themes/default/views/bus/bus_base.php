<?php if(!empty($assentos)) {?>
    <?php foreach ($assentos as $assento) {
        $habilitado = '';
        if ($assento->habilitado) {
            $habilitado = 'enabled';
        } else {
            $habilitado = 'disabled';
        }
        ?>
        <li andar="<?=$assento->andar;?>"
            data-toggle="modal"
            data-target="#axisMapModal"
            data-backdrop="static"
            data-keyboard="false"
            data-note-assento=""
            class="seat andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> seat-<?=$habilitado?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
            data-seat="<?=$assento->assento;?>"
            data-andar="<?=$assento->andar;?>"
            data-status="<?=$assento->habilitado;?>"
            data-x="<?=$assento->x;?>"
            data-y="<?=$assento->y;?>"
            data-z="<?=$assento->z;?>"
            data-position-order="<?=$assento->ordem;?>"
            data-position-order_name="<?=$assento->name;?>">
            <span class="seatNumberLabel"><?=$assento->name;?></span>
        </li>
    <?php }?>
<?php } ?>

<?php if(!empty($assentos2)) {?>
    <?php foreach ($assentos2 as $assento) {
        $habilitado = '';
        if ($assento->habilitado) {
            $habilitado = 'enabled';
        } else {
            $habilitado = 'disabled';
        }
        ?>
        <li andar="<?=$assento->andar;?>"
            data-toggle="modal"
            data-target="#axisMapModal"
            data-backdrop="static"
            data-keyboard="false"
            data-note-assento=""
            class="seat andar-<?=$assento->andar;?> seat-<?=$assento->ordem;?> seat-<?=$habilitado?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
            data-seat="<?=$assento->assento;?>"
            data-andar="<?=$assento->andar;?>"
            data-status="<?=$assento->habilitado;?>"
            data-x="<?=$assento->x;?>"
            data-y="<?=$assento->y;?>"
            data-z="<?=$assento->z;?>"
            data-position-order="<?=$assento->ordem;?>"
            data-position-order_name="<?=$assento->name;?>">
            <span class="seatNumberLabel"><?=$assento->name;?></span>
        </li>
    <?php }?>
<?php } ?>
