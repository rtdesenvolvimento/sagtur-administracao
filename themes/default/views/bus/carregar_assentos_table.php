<?php if (!empty($assentos)) {?>
    <tr><td colspan="3" style="background: #dbdee0;"><span style="font-size: 20px;font-weight: 500;">1º Andar</span></td></tr>
    <?php foreach ($assentos as $assento) {?>
        <?php if($assento->habilitado) {?>
            <tr>
                <td style="text-align: center;font-weight: 700;">
                    <input type="hidden" value="<?=$assento->name?>" name="assento[]"/>
                    <input type="hidden" value="1" name="andar[]"/>
                    <?=$assento->name?>
                </td>
                <td>
                    <input name="valor_extra[]" type="text" id="assento_<?=$assento->id?>" assento_id="<?=$assento->id?>" assento="<?=$assento->name?>" required="required" value="0.00" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                </td>
                <td>
                    <select id="cor<?=$assento->id;?>" assento_id="<?=$assento->id?>" assento="<?=$assento->name?>"  name="cor[]" style="width: 100%;">
                        <option value="">Selecione uma cor</option>
                        <?php foreach ($cores as $cor) {?>
                            <option cor="<?=trim($cor->cor);?>" style="background: <?=$cor->cor;?>;color: #ffffff;" value="<?=$cor->id;?>"><?=$cor->name;?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
<?php } ?>
<?php if (!empty($assentos2)) {?>
    <tr><td colspan="3" style="background: #dbdee0;"><span style="font-size: 20px;font-weight: 500;">2º Andar</span></td></tr>
    <?php foreach ($assentos2 as $assento) {?>
        <?php if($assento->habilitado) {?>
            <tr>
                <td style="text-align: center;font-weight: 700;">
                    <input type="hidden" value="<?=$assento->name?>" name="assento[]"/>
                    <input type="hidden" value="2" name="andar[]"/>
                    <?=$assento->name?>
                </td>
                <td>
                    <input name="valor_extra[]" id="assento_<?=$assento->id?>" type="text" assento_id="<?=$assento->id?>" assento="<?=$assento->name?>"  required="required" value="0.00" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                </td>
                <td>
                    <select id="cor<?=$assento->id;?>" assento_id="<?=$assento->id?>" assento="<?=$assento->assento?>"  name="cor[]" style="width: 100%;">
                        <option value="">Selecione uma cor</option>
                        <?php foreach ($cores as $cor) {?>
                            <option cor="<?=trim($cor->cor);?>" style="background: <?=$cor->cor;?>;color: #ffffff;" value="<?=$cor->id;?>"><?=$cor->name;?></option>
                        <?php } ?>
                    </select>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
<?php } ?>
