
<!--A Design by W3layouts
   Author: W3layout
   Author URL: http://w3layouts.com
   License: Creative Commons Attribution 3.0 Unported
   License URL: http://creativecommons.org/licenses/by/3.0/
   -->
<!DOCTYPE html>

<html lang="pt-br">

<head>
    <title>SAGTur</title>
    <!-- Meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content=""/>

    <script>
        addEventListener("load", function () { setTimeout(hideURLbar, 0); }, false); function hideURLbar() { window.scrollTo(0, 1); }
    </script>
    <!-- Meta tags -->

    <!-- font-awesome icons -->
    <link href="<?= $assets ?>login/css/font-awesome.min.css" rel="stylesheet">
    <!-- //font-awesome icons -->

    <!--stylesheets-->
    <link href="<?= $assets ?>login/css/style.css" rel='stylesheet' type='text/css' media="all">
 
</head>
<body>
    <noscript>
        <div class="global-site-notice noscript">
            <div class="notice-inner">
                <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                    your browser to utilize the functionality of this website.</p>
            </div>
        </div>
    </noscript>
    <h1 class="error">Gadget Sign Up Form</h1>
	<!---728x90--->
    <div class="w3layouts-two-grids">
	<!---728x90--->
        <div class="mid-class">
            <div class="img-right-side">
                <h3>Manage Your Gadgets Account</h3>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget Lorem ipsum dolor sit
                    amet, consectetuer adipiscing elit. Aenean commodo ligula ege</p>
                <img src="https://sagtur.com.br/wp-content/uploads/2020/03/81178-5-dicas-de-marketing-para-contadores.png" class="img-fluid" alt="">
            </div>
            <div class="txt-left-side">
                <h2> ACESSE SEU SISTEMA </h2>
                <?php if ($Settings->mmode) { ?>
                    <div class="alert alert-warning">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?= lang('site_is_offline') ?>
                    </div>
                <?php }
                if ($error) { ?>
                    <div class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <ul class="list-group"><?= $error; ?></ul>
                    </div>
                <?php }
                if ($message) { ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <ul class="list-group"><?= $message; ?></ul>
                    </div>
                <?php } ?>
                <?php echo form_open("auth/login", 'class="login" data-toggle="validator"'); ?>
                    <div class="form-left-to-w3l">
                        <span class="fa fa-envelope-o" aria-hidden="true"></span>
                        <input type="email" name="identity" placeholder="<?= lang('username') ?>"
                               required="required">
                        <div class="clear"></div>
                    </div>
                    <div class="form-left-to-w3l ">
                        <span class="fa fa-lock" aria-hidden="true"></span>
                        <input type="password" name="password" placeholder="<?= lang('pw') ?>"
                               required="required">
                        <div class="clear"></div>
                    </div>
                    <div class="main-two-w3ls">
                        <div class="left-side-forget">
                            <input type="checkbox" class="checked">
                            <span class="remenber-me">Remember me </span>
                        </div>
                        <div class="right-side-forget">
                            <a href="#" class="for">Forgot password...?</a>
                        </div>
                    </div>
                    <div class="btnn">
                        <button type="submit"><?= lang('login') ?> &nbsp;
                            <i class="fa fa-sign-in"></i> </button>
                    </div>
                <?php echo form_close(); ?>
                <div class="clear"></div>
            </div>
        </div>
    </div>
	<!---728x90--->
    <footer class="copyrigh-wthree">
        <p>
            © <?php echo date('Y')?> All Rights Reserved | Design by
            <a href="http://www.resultatec.com.br" target="_blank">Resultatec Sistemas</a>
        </p>
    </footer>
</body>

</html>