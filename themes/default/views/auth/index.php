<style type="text/css" media="screen">
    #UsrTable td:nth-child(6) {display: none;}
    #UsrTable td:nth-child(4) {text-align: center;}
    #UsrTable td:nth-child(5) {text-align: center;}
    #UsrTable td:nth-child(9) {display: none;}
</style>

<script>

    function group_name(x) {

        if (x === 'owner') return 'Administrador';
        if (x === 'colaborador') return  'Vendedor';
        if (x === 'admin') return  'Administrador';

        return x;
    }

    $(document).ready(function () {
        'use strict';
        var oTable = $('#UsrTable').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('auth/getUsers') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, null, null, null, null, null,  {"mRender": group_name}, {"mRender": user_status},  null, {"bSortable": false}]
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('first_name');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('last_name');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('email_address');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
        ], "footer");
    });
</script>
<style>.table td:nth-child(6) {
        text-align: right;
        width: 10%;
    }

    .table td:nth-child(8) {
        text-align: center;
    }</style>
<?php if ($Owner) {
    echo form_open('auth/user_actions', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('users'); ?></h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip"
                                                                                  data-placement="left"
                                                                                  title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?= site_url('auth/create_user'); ?>"><i
                                    class="fa fa-plus-circle"></i> <?= lang("add_user"); ?></a></li>
                        <li><a href="#" id="excel" data-action="export_excel"><i
                                    class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li><a href="#" id="pdf" data-action="export_pdf"><i
                                    class="fa fa-file-pdf-o"></i> <?= lang('export_to_pdf') ?></a></li>
                        <li class="divider"></li>
                        <li style="display: none;"><a href="#" class="bpo" title="<?= $this->lang->line("delete_users") ?>"
                               data-content="<?= lang('r_u_sure') ?><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                               data-html="true" data-placement="left"><i
                                    class="fa fa-trash-o"></i> <?= lang('delete_users') ?></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">

        <div class="row">
            <div class="col-lg-12">

                <table cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th class="col-xs-2" style="text-align: left;"><?php echo lang('plan'); ?></th>
                        <th class="col-xs-2"><?php echo lang('valor'); ?></th>
                        <th class="col-xs-2"><?php echo lang('qtd_users'); ?></th>
                        <th class="col-xs-2"><?php echo lang('users_active'); ?></th>
                        <th class="col-xs-2"><?php echo lang('users_available'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><?=$plan_name;?></td>
                        <td style="text-align: center;"><?=$this->sma->formatMoney($plan_price);?></td>
                        <?php if ($total_users > 100){?>
                            <td style="text-align: center;">Ilimitados</td>
                        <?php } else { ?>
                            <td style="text-align: center;"><?=$total_users;?></td>
                        <?php } ?>
                        <td style="text-align: center;"><?=$total_users_used;?></td>
                        <?php if ($total_users > 100){?>
                            <td style="text-align: center;">Ilimitados</td>
                        <?php } else { ?>
                            <td style="text-align: center;"><?=$total_users_available;?></td>
                        <?php } ?>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="UsrTable" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                            </th>
                            <th class="col-xs-2" style="text-align: left;"><?php echo lang('first_name'); ?></th>
                            <th class="col-xs-2" style="text-align: left;"><?php echo lang('last_name'); ?></th>
                            <th class="col-xs-2"><?php echo lang('Acesso'); ?></th>
                            <th class="col-xs-2" style="text-align: center;"><?php echo lang('company'); ?></th>
                            <th class="col-xs-1" style="display: none;"><?php echo lang('award_points'); ?></th>
                            <th class="col-xs-1"style="text-align: center;"><?php echo lang('group'); ?></th>
                            <th style="width:100px;"><?php echo lang('status'); ?></th>
                            <td style="display: none;"></td>
                            <th style="width:80px;"><?php echo lang('actions'); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="width:100px;"></th>
                            <th style="width:85px;"><?= lang("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>

    <script language="javascript">
        $(document).ready(function () {
            $('#set_admin').click(function () {
                $('#usr-form-btn').trigger('click');
            });

        });
    </script>

<?php } ?>