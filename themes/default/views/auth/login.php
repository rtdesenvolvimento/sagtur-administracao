<?php

$uri = $this->input->get('empresa');
$cnpj = $this->input->get('CNPJ');

if ($uri == null) {
    $uri = str_replace('http://', '', base_url());
    $uri = str_replace('https://', '', $uri);
    $uri = str_replace('www', '', $uri);
    $uri = str_replace('resultaweb.com.br', '', $uri);
    $uri = str_replace('resultatec.com.br', '', $uri);
    $uri = str_replace('sagtur.com.br', '', $uri);
    $uri = str_replace('/sagtur/', '', $uri);
    $uri = str_replace('./', '', $uri);
    $uri = str_replace('sistema', '', $uri);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $title ?></title>
    <script type="text/javascript">if (parent.frames.length !== 0) {
            top.location = '<?=site_url('pos')?>';
        }</script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="follow" />

    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/helpers/login.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/respond.min.js"></script>
    <![endif]-->

    <style>
        body {
            min-width: 350px;
        }
        .bblue {
            background: #fff !important;
        }
        .login-page .page-back {
            display: flex;
            align-items: center;
            flex-direction: column;
            justify-content: center;
            background-size: cover !important;
            background-position: center !important;
            background-image: url("<?= $assets ?>login/images/<?php echo $num = rand(1, 33);?>.jpg") !important;
        }
        .contents {
            margin: 16px;
            border-radius: 6px;
            padding: 32px 16px;
            background: rgba(0, 0, 0, 0.1);
            border: 1px solid rgba(0, 0, 0, 0.2);
        }
        .login-content, .login-page .login-form-links {
            margin-top: 20px;
            border-radius: 6px;
        }
    </style>
</head>

<body class="login-page">
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div class="page-back">
    <div class="contents">
        <div class="text-center">
        </div>
        <div id="login">
            <div class=" container">
                <div class="login-form-div">
                    <div class="login-content">
                        <?php if ($Settings->mmode) { ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?= lang('site_is_offline') ?>
                            </div>
                        <?php }
                        if ($error) { ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $error; ?></ul>
                            </div>
                        <?php }
                        if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $message; ?></ul>
                            </div>
                        <?php } ?>
                        <?php if ($warning){?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $warning; ?></ul>
                            </div>
                        <?php }?>
                        <?php echo form_open("auth/login", 'class="login" data-toggle="validator"'); ?>
                        <div class="div-title">
                            <h3 class="text-primary">
                                <?php echo '<img src="' . base_url('assets/images/login_logo.png') . '" alt="' . $Settings->site_name . '" style="margin-bottom:30px;margin-top:30px;" />'; ?>
                                <?= lang('login_to_your_account') ?>
                            </h3>
                        </div>
                        <div class="textbox-wrap form-group" <?php if ($uri == 'demonstracao') echo 'style="display: none;"';?> >
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                <?php if ($uri == 'demonstracao') {?>
                                    <input type="number" required="required"
                                           readonly
                                           value="38055980000100"
                                           autocomplete="on"
                                           class="form-control buscar_agencias_cadastradas"
                                           id="cnpjempresa"
                                           name="cnpjempresa"
                                           placeholder="Digite o CNPJ (somente números)"/>
                                <?php }else { ?>
                                    <input type="number" required="required" value="38055980000100"
                                           autocomplete="on"
                                           class="form-control buscar_agencias_cadastradas"
                                           id="cnpjempresa"
                                           name="cnpjempresa"
                                           placeholder="Digite o CNPJ (somente números)"/>
                                <?php }?>
                            </div>
                        </div>
                        <div class="textbox-wrap form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php if ($uri == 'demonstracao') {?>
                                    <input type="text" required="required" value="andre@resultatec.com.br" class="form-control" name="identity"
                                           placeholder="<?= lang('username') ?>"/>
                                <?php } else {?>
                                    <input type="text" required="required" class="form-control" name="identity"
                                           placeholder="<?= lang('username') ?>"/>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="textbox-wrap form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                                <?php if ($uri == 'demonstracao') {?>
                                    <input type="password" required="required" value="" class="form-control" id="password" name="password"
                                           placeholder="<?= lang('pw') ?>"/>
                                <?php } else { ?>
                                    <input type="password" required="required" class="form-control " id="password" name="password"
                                           placeholder="<?= lang('pw') ?>"/>
                                <?php } ?>
                                <span class="input-group-addon" id="senha_view" style="cursor: pointer;"><i class="fa fa-eye"></i></span>
                            </div>
                        </div>
                        <?php if ($Settings->captcha) { ?>
                            <div class="textbox-wrap form-group">
                                <div class="row">
                                    <div class="col-sm-6 div-captcha-left">
                                        <span class="captcha-image"><?php echo $image; ?></span>
                                    </div>
                                    <div class="col-sm-6 div-captcha-right">
                                        <div class="input-group">
                                            <span class="input-group-addon"><a href="<?= base_url(); ?>auth/reload_captcha"
                                                                               class="reload-captcha"><i
                                                        class="fa fa-refresh"></i></a></span>
                                            <?php echo form_input($captcha); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } /* echo $recaptcha_html; */ ?>

                        <div class="form-action clearfix">
                            <div class="checkbox pull-left">
                                <div class="custom-checkbox">
                                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
                                </div>
                                <span class="checkbox-text pull-left"><label
                                        for="remember"><?= lang('remember_me') ?></label></span>
                            </div>
                            <button type="submit" class="btn btn-success pull-right"><?= lang('login') ?> &nbsp; <i
                                    class="fa fa-sign-in"></i></button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                    <div class="login-form-links link2">
                        <h4 class="text-danger" style="color: #0b0b0b"><?= lang('forgot_your_password') ?></h4>
                        <span><?= lang('dont_worry') ?></span>
                        <a href="#forgot_password" class="text-danger forgot_password_link"><?= lang('click_here') ?></a>
                        <span><?= lang('to_rest') ?></span>
                    </div>
                    <?php if ($Settings->allow_reg) { ?>
                        <div class="login-form-links link1">
                            <h4 class="text-info"><?= lang('dont_have_account') ?></h4>
                            <span><?= lang('no_worry') ?></span>
                            <a href="#register" class="text-info register_link"><?= lang('click_here') ?></a>
                            <span><?= lang('to_register') ?></span>
                        </div>
                    <?php } ?>
                </div>

            </div>
        </div>
        <div id="forgot_password">
            <div class=" container">
                <div class="login-form-div">
                    <div class="login-content">
                        <?php if ($error) { ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $error; ?></ul>
                            </div>
                        <?php }
                        if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <ul class="list-group"><?= $message; ?></ul>
                            </div>
                        <?php } ?>
                        <div class="div-title">
                            <h3 class="text-primary"><?= lang('forgot_password') ?></h3>
                        </div>
                        <?php echo form_open("auth/forgot_password", 'class="login" data-toggle="validator"'); ?>
                        <div class="textbox-wrap form-group">
                            <div class="input-group">
                                <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                                <input type="email" name="forgot_email" class="form-control "
                                       placeholder="<?= lang('email_address') ?>" required="required"/>
                            </div>
                        </div>
                        <div class="textbox-wrap form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-road"></i></span>
                                <input type="number" required="required" value=""
                                       autocomplete="on"
                                       class="form-control"
                                       id="forgot_cnpjempresa"
                                       name="forgot_cnpjempresa"
                                       placeholder="Digite o CNPJ (somente números)"/>
                            </div>
                        </div>
                        <div class="form-action clearfix">
                            <a class="btn btn-success pull-left login_link" href="#login"><i
                                    class="fa fa-chevron-left"></i> <?= lang('back') ?>  </a>
                            <button type="submit" class="btn btn-primary pull-right"><?= lang('submit') ?> &nbsp;&nbsp; <i
                                    class="fa fa-envelope"></i></button>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($Settings->allow_reg) { ?>
            <div id="register">
                <div class=" container">

                    <div class="registration-form-div">
                        <form>
                            <div class="div-title reg-header">
                                <h3 class="text-primary"><?= lang('register_account_heading') ?></h3>
                            </div>
                            <div class="clearfix">
                                <div class="col-sm-6 registration-left-div">
                                    <div class="reg-content">
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control "
                                                       placeholder="<?= lang('first_name') ?>" required="required"/>
                                            </div>
                                        </div>
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control "
                                                       placeholder="<?= lang('last_name') ?>" required="required"/>
                                            </div>
                                        </div>
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                                                <input type="email" class="form-control "
                                                       placeholder="<?= lang('email_address') ?>" required="required"/>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                <div class="col-sm-6 registration-right-div">
                                    <div class="reg-content">
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                                <input type="text" class="form-control "
                                                       placeholder="<?= lang('username') ?>" required="required"/>
                                            </div>
                                        </div>
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                                <input type="password" class="form-control " placeholder="<?= lang('pw') ?>"
                                                       required="required"/>
                                            </div>
                                        </div>
                                        <div class="textbox-wrap form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                                <input type="password" class="form-control "
                                                       placeholder="<?= lang('confirm_password') ?>" required="required"/>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="registration-form-action clearfix">
                                <a href="#login" class="btn btn-success pull-left login_link">
                                    <i class="fa fa-chevron-left"></i> <?= lang('back') ?>
                                </a>
                                <button type="submit" class="btn btn-primary pull-right"><?= lang('register_now') ?> <i
                                        class="fa fa-user"></i></button>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script src="<?= $assets ?>js/jquery.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js"></script>
<script src="<?= $assets ?>js/bootstrap.min.js"></script>
<script src="<?= $assets ?>js/jquery.cookie.js"></script>
<script src="<?= $assets ?>js/login.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        viewSenha();

        var hash = window.location.hash;

        if (hash && hash != '') {
            $("#login").hide();
            $(hash).show();
        }

        /*
        $(".buscar_agencias_cadastradas").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url:  "<?php echo base_url();?>auth/getAgencias",
                    dataType: "json",
                    data: {
                        term : $(".buscar_agencias_cadastradas").val(),
                    },
                    success: function (data) {
                        if (data === '') return false;
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            placeholder: '',
            response: function (event, ui) {},
            select: function (event, ui) {}
        });
        */
    });
    function viewSenha(){
        var tipo = document.getElementById('password')

        document.getElementById('senha_view').addEventListener('click', () => {
            if(tipo.value) {
                tipo.type == 'password' ? tipo.type = 'text' : tipo.type = 'password';
            }
        })
    }

</script>
</body>
</html>
