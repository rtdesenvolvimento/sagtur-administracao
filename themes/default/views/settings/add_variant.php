<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_variant'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/add_variant", $attrib); ?>
	        <div class="modal-body">
	            <p><?= lang('enter_info'); ?></p>
	
	            <div class="form-group">
	                <label for="name"><?php echo $this->lang->line("name"); ?></label>
	                <div  class="controls"> <?php echo form_input('name', '', 'class="form-control" id="name" required="required"'); ?> </div>
	            </div>
	            
	            <div class="form-group">
                 	<?= lang("lancamento_type", "lancamento_type") ?>
                    <?php
                    $opts = array(
                        '1' => lang('lancamento_grupo') ,
                        '2' => lang('lancamento_individual'),
                    );
                    echo form_dropdown('lancamento_type', $opts, '' , 'class="form-control" id="lancamento_type" required="required"');
                     ?>
                 </div>
	            <div class="form-group">
	                <?= lang("usar_fornecedor", "usar_fornecedor") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                     echo form_dropdown('usar_fornecedor', $opts, '' , 'class="form-control" id="usar_fornecedor"');
                     ?>
	            </div>
		        <div class="form-group">
	                <label class="control-label"
	                       for="customer_group"><?php echo $this->lang->line("default_suppliers_group"); ?></label>
	
	                <div class="controls"> <?php
	                	$cgs[] = '';
	                    foreach ($customer_groups as $customer_group) {
	                        $cgs[$customer_group->id] = $customer_group->name;
	                    }
	                    echo form_dropdown('customer_group', $cgs, $this->Settings->customer_group, 'class="form-control tip select" id="customer_group" style="width:100%;" ');
	                    ?>
	                </div>
            	</div>
            
	            <div class="form-group">
  	                <?= lang("usar_localizacao", "usar_localizacao") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                     echo form_dropdown('usar_localizacao', $opts, '' , 'class="form-control" id="usar_localizacao"');
                     ?>
	            </div>
	            
	            <div class="form-group" style="display: none;">
  	                <?= lang("is_transporte", "is_transporte") ?>
                    <?php
                    $opts = array('1' => lang('sim'), '2' => lang('no') );
                     echo form_dropdown('is_transporte', $opts, '' , 'class="form-control" id="is_transporte"');
                     ?>
	            </div>
	            
	            <div class="form-group" style="display: none;">
  	                <?= lang("transport_type", "transport_type") ?>
                    <?php
                    $opts = array('1' => lang('1_andar'), '2' => lang('2_andar') );
                    echo form_dropdown('transport_type', $opts, '', 'class="form-control" id="transport_type"');
                     ?>
                </div>

                <div class="form-group" style="display: none;">
                    <?= lang("planta", "planta") ?>
                    <div class="controls"> <?php
                        $pl[] = '';
                        foreach ($plantas as $planta) {
                            $pl[$planta->id] = $planta->name;
                        }
                        echo form_dropdown('planta', $pl, '', 'class="form-control tip select" id="planta" style="width:100%;" ');
                        ?>
                    </div>
                </div>
	            
	        </div>
	        <div class="modal-footer">
	            <?php echo form_submit('add_variant', lang('add_variant'), 'class="btn btn-primary"'); ?>
	        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>