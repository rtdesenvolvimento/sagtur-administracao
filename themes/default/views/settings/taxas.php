<div class="row" style="margin-bottom: 15px;">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa fa-th"></i><span class="break"></span>
                    <?= lang('info_configuracao_taxas_forma_pagamento') ?></h2>
            </div>
            <div class="box-content">
                <?php foreach ($tiposCobranca as $tipoCobranca) {?>
                <div class="col-lg-3 col-md-2 col-xs-6" style="margin-top: 15px;">
                    <a class="bblue white quick-button small" data-toggle="modal" data-target="#myModal" href="<?= site_url('system_settings/taxa_cobranca').'/'.$tipoCobranca->id ?>">
                        <i class="fa fa-edit"></i>
                        <p><?php echo $tipoCobranca->name;?> </p>
                    </a>
                </div>
                <?php }?>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>