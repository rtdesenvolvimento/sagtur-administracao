<script>
    $(document).ready(function () {
        $('#asaas_form').bootstrapValidator({
            message: 'Please enter/select a value',
            submitButtons: 'input[type="submit"]'
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('asaas_settings'); ?></h2>
        <div class="box-icon">
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('role' => 'form', 'id="asaas_form"');
                echo form_open("system_settings/asaas", $attrib);
                ?>
                <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("activate", "active"); ?>
                            <?php
                            $yn = array('1' => lang("sim"), '0' => lang("nao"));
                            echo form_dropdown('active', $yn, $asaas->active, 'class="form-control tip" required="required" id="active"'); ?>
                        </div>
                        <div class="form-group">
                            <?= lang("account_token_asaas", "account_token_asaas"); ?>
                            <?php echo form_input('account_token_asaas', $asaas->token, 'class="form-control tip" id="account_token_asaas"'); ?>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <label>Como pegar os dados para integração com Asaas</label>
                        <iframe width="383" height="384" src="https://www.youtube.com/embed/6b8vf5bDd0Q" title="Como pegar o Token de Integração Asaas para incluir no Sistema SAGTur" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>