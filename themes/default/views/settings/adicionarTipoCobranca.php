<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_tipo_cobranca'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarTipoCobranca", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <input type="hidden" name="tipoExibir" value="receita"/>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'Ativo' => lang('ativo'),
                    'Inativo' => lang('inativo')
                );
                echo form_dropdown('status', $opts, (isset($_POST['status']) ? $_POST['status'] :  ''), 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('nome_tipo_cobranca', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_status" style="display: none;">
                <?= lang("faturar_automatico", "faturar_automatico") ?>
                <?php
                $opts = array(
                    'sim' => lang('yes'),
                    'nao' => lang('no')
                );
                echo form_dropdown('faturar_automatico', $opts, (isset($_POST['faturar_automatico']) ? $_POST['faturar_automatico'] :  ''), 'class="form-control" id="faturar_automatico" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('conta_destino', 'conta'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbContas[''] = lang('select').' '.lang('conta_destino');
                    foreach ($movimentadores as $movimentador) {
                        $cbContas[$movimentador->id] = $movimentador->name;
                    } ?>
                    <?= form_dropdown('conta', $cbContas, set_value('conta'), 'class="form-control" required="required" id="conta"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('forma_pagamento', 'formapagamento'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-money"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbFormaPagamento[''] = lang('select').' '.lang('forma_pagamento');
                    foreach ($formaspagamento as $formapagamento) {
                        $cbFormaPagamento[$formapagamento->id] = $formapagamento->name;
                    } ?>
                    <?= form_dropdown('formapagamento', $cbFormaPagamento, set_value('formapagamento'), 'class="form-control" required="required" id="formapagamento"'); ?>
                </div>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("faturarVenda", "faturarVenda") ?>
                <?php
                $opts = array(
                    '1' => lang('faturar_venda'),
                    '0' => lang('orcamento_venda')
                );
                echo form_dropdown('faturarVenda', $opts, (isset($_POST['faturarVenda']) ? $_POST['faturarVenda'] :  ''), 'class="form-control" id="faturarVenda" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("integracao", "integracao") ?>
                <?php
                $opts = array(
                    'nenhuma' => lang('nenhuma'),
                    'juno' => lang('juno'),
                    'pagseguro' => lang('pagseguro'),
                    'mercadopago' => lang('mercadopago'),
                );
                echo form_dropdown('integracao', $opts, (isset($_POST['status']) ? $_POST['status'] :  ''), 'class="form-control" id="integracao" required="required"');
                ?>
            </div>
            <div class="form-group" id="div_tipo_emissao_integracao" style="display: none;">
                <?= lang("tipo", "tipo") ?>
                <?php
                $opts = array(
                    'nenhuma' => lang('select'),
                );
                echo form_dropdown('tipo', $opts, (isset($_POST['tipo']) ? $_POST['tipo'] :  ''), 'class="form-control" id="tipo"  required="required"');
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarTipoCobranca', lang('adicionar_tipo_cobranca'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $('#integracao').select2().on("change", function(e) {
            const tipoIntegracao =  $("#integracao  option:selected").val();
            if (tipoIntegracao !== 'nenhuma') {
                $('#div_tipo_emissao_integracao').show();
            } else {
                $('#div_tipo_emissao_integracao').hide();
            }

            if (tipoIntegracao === 'juno') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne', text: '<?php echo lang('carne');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao', text: '<?php echo lang('link_pagamento');?>'}));
            }

            if (tipoIntegracao === 'pagseguro') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao', text: '<?php echo lang('link_pagamento');?>'}));
            }

            if (tipoIntegracao === 'mercadopago') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao_transparent_mercado_pago', text: '<?php echo lang('carne_cartao_transparent_mercado_pago');?>'}));
                $('#tipo').append($('<option>', {value: 'link_pagamento', text: '<?php echo lang('link_pagamento');?>'}));
                $('#tipo').append($('<option>', {value: 'pix', text: '<?php echo lang('pix');?>'}));
            }

        });
    });
</script>
