<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_valor_faixa'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("system_settings/editarValorFaixa/" . $valorFaixa->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    '1' => lang('ativo'),
                    '0' => lang('inativo')
                );
                echo form_dropdown('active', $opts, (isset($_POST['active']) ? $_POST['active'] :  ''), 'class="form-control" id="active" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $valorFaixa->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('descontarVagas', '1', $valorFaixa->descontarVagas, ''); ?>
                <?= lang('descontarVagas', 'descontarVagas'); ?>
            </div>
            <?php echo form_hidden('id', $valorFaixa->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarValorFaixa', lang('editar_valor_faixa'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

