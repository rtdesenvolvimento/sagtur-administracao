<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_conta'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarConta", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'open' => lang('ativo'),
                    'close' => lang('inativo')
                );
                echo form_dropdown('status', $opts, (isset($_POST['status']) ? $_POST['status'] :  ''), 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarConta', lang('adicionar_conta'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>