<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_receita'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarReceita", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang("tipo", "tipo") ?>
                <?php
                $opts = array(
                    'grupo' => lang('grupo'),
                    'subgrupo' => lang('subgrupo')
                );
                echo form_dropdown('tipo', $opts, (isset($_POST['tipo']) ? $_POST['tipo'] :  '') , ' class="form-control" id="tipo" required="required"');
                ?>
            </div>
            <div class="form-group" id="divDRE">
                <?= lang('dre', 'dre'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-cogs"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbDRE[''] = lang('select').' '.lang('dre');
                    foreach ($dre as $dr) {
                        $cbDRE[$dr->id] =  $dr->name;
                    } ?>
                    <?= form_dropdown('dre', $cbDRE, set_value('dre'), 'class="form-control tip" id="dre"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('receita_pai', 'receita'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbReceitasSuperiores[''] = lang('selecione_despesa_pai');
                    foreach ($receitasSuperior as $receitaSuperior) {
                        $cbReceitasSuperiores[$receitaSuperior->id] = $receitaSuperior->name;
                    } ?>
                    <?= form_dropdown('receitasuperior', $cbReceitasSuperiores, set_value('receitasuperior'), 'class="form-control tip" id="receitasuperior"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('category_code', 'code'); ?>
                <?= form_input('code', '', 'class="form-control" id="code" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('category_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarReceita', lang('adicionar_receita'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script language="javascript">
    $(document).ready(function () {
        $("#s2id_tipo").blur(() => {
            if ($(this).value() === 'grupo') $('#divDRE').show();
            else $('#divDRE').hide();
        });
    });
</script>

