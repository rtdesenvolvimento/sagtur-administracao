<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-clock-o"></i><?= lang('configurar_horario_comercial'); ?></h2>
        <div class="box-icon"></div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php echo form_open("system_settings/horarios", array('role' => 'form','')); ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("duracao", "duracao"); ?>
                            </label>
                            <div class="col-sm-8">
                                <?php
                                $optsDuracao = array(
                                    '5'  => '5 min',
                                    '10' => '10 min',
                                    '12' => '12 min',
                                    '15' => '15 min',
                                    '20' => '20 min',
                                    '30' => '30 min',
                                    '40' => '40 min',
                                    '45' => '45 min',
                                    '60' => '1 h',
                                    '90' => '1 h 30 min',
                                    '120'=> '2 h',
                                    '180'=> '3 h',
                                    '240'=> '4 h',
                                    '360'=> '6 h',
                                );
                                echo form_dropdown('duracao', $optsDuracao, $horario->duracao , 'class="form-control" id="duracao" required="required"');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label"><?= lang("intervalo", "intervalo"); ?></label>
                            <div class="col-sm-8">
                                <?php
                                $optsIntervalo = array(
                                    '0'  => '0 min',
                                    '5'  => '5 min',
                                    '10' => '10 min',
                                    '12' => '12 min',
                                    '15' => '15 min',
                                    '20' => '20 min',
                                    '30' => '30 min',
                                    '45' => '45 min',
                                    '60' => '1 h',
                                    '90' => '1 h 30 min',
                                    '120'=> '2 h',
                                    '180'=> '3 h',
                                    '240'=> '4 h',
                                    '360'=> '6 h',
                                );
                                echo form_dropdown('intervalo', $optsIntervalo, $horario->intervalo , 'class="form-control" id="intervalo" required="required"');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("segunda", "segunda"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('segunda_manha_de', $this->site->getSelectHorarios(), (isset($_POST['segunda_manha_de']) ? $_POST['segunda_manha_de'] : ($horario ? $horario->segunda_manha_de : '08:00:00')) , 'class="form-control" id="segunda_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('segunda_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['segunda_manha_ate']) ? $_POST['segunda_manha_ate'] : ($horario ? $horario->segunda_manha_ate : '12:00:00')) , 'class="form-control" id="segunda_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('segunda_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['segunda_tarde_de']) ? $_POST['segunda_tarde_de'] : ($horario ? $horario->segunda_tarde_de : '14:00:00')) , 'class="form-control" id="segunda_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('segunda_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['segunda_tarde_ate']) ? $_POST['segunda_tarde_ate'] : ($horario ? $horario->segunda_tarde_ate : '18:00:00')), 'class="form-control" id="segunda_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("terca", "terca"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('terca_manha_de', $this->site->getSelectHorarios(), (isset($_POST['terca_manha_de']) ? $_POST['terca_manha_de'] : ($horario ? $horario->terca_manha_de : '08:00:00')) , 'class="form-control" id="terca_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('terca_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['terca_manha_ate']) ? $_POST['terca_manha_ate'] : ($horario ? $horario->terca_manha_ate : '12:00:00')) , 'class="form-control" id="terca_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('terca_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['terca_tarde_de']) ? $_POST['terca_tarde_de'] : ($horario ? $horario->terca_tarde_de : '14:00:00')) , 'class="form-control" id="terca_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('terca_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['terca_tarde_ate']) ? $_POST['terca_tarde_ate'] : ($horario ? $horario->terca_tarde_ate : '18:00:00')), 'class="form-control" id="terca_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("quarta", "quarta"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('quarta_manha_de', $this->site->getSelectHorarios(), (isset($_POST['quarta_manha_de']) ? $_POST['quarta_manha_de'] : ($horario ? $horario->quarta_manha_de : '08:00:00')) , 'class="form-control" id="quarta_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('quarta_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['quarta_manha_ate']) ? $_POST['quarta_manha_ate'] : ($horario ? $horario->quarta_manha_ate : '12:00:00')) , 'class="form-control" id="quarta_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('quarta_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['quarta_tarde_de']) ? $_POST['quarta_tarde_de'] : ($horario ? $horario->quarta_tarde_de : '14:00:00')) , 'class="form-control" id="quarta_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('quarta_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['quarta_tarde_ate']) ? $_POST['quarta_tarde_ate'] : ($horario ? $horario->quarta_tarde_ate : '18:00:00')), 'class="form-control" id="quarta_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("quinta", "quinta"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('quinta_manha_de', $this->site->getSelectHorarios(), (isset($_POST['quinta_manha_de']) ? $_POST['quinta_manha_de'] : ($horario ? $horario->quinta_manha_de : '08:00:00')) , 'class="form-control" id="quinta_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('quinta_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['quinta_manha_ate']) ? $_POST['quinta_manha_ate'] : ($horario ? $horario->quinta_manha_ate : '12:00:00')) , 'class="form-control" id="quinta_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('quinta_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['quinta_tarde_de']) ? $_POST['quinta_tarde_de'] : ($horario ? $horario->quinta_tarde_de : '14:00:00')) , 'class="form-control" id="quinta_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('quinta_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['quinta_tarde_ate']) ? $_POST['quinta_tarde_ate'] : ($horario ? $horario->quinta_tarde_ate : '18:00:00')), 'class="form-control" id="quinta_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("sexta", "sexta"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('sexta_manha_de', $this->site->getSelectHorarios(), (isset($_POST['sexta_manha_de']) ? $_POST['sexta_manha_de'] : ($horario ? $horario->sexta_manha_de : '08:00:00')) , 'class="form-control" id="sexta_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('sexta_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['sexta_manha_ate']) ? $_POST['sexta_manha_ate'] : ($horario ? $horario->sexta_manha_ate : '12:00:00')) , 'class="form-control" id="sexta_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('sexta_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['sexta_tarde_de']) ? $_POST['sexta_tarde_de'] : ($horario ? $horario->sexta_tarde_de : '14:00:00')) , 'class="form-control" id="sexta_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('sexta_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['sexta_tarde_ate']) ? $_POST['sexta_tarde_ate'] : ($horario ? $horario->sexta_tarde_ate : '18:00:00')), 'class="form-control" id="sexta_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("sabado", "sabado"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('sabado_manha_de', $this->site->getSelectHorarios(), (isset($_POST['sabado_manha_de']) ? $_POST['sabado_manha_de'] : ($horario ? $horario->sabado_manha_de : '08:00:00')) , 'class="form-control" id="sabado_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('sabado_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['sabado_manha_ate']) ? $_POST['sabado_manha_ate'] : ($horario ? $horario->sabado_manha_ate : '12:00:00')) , 'class="form-control" id="sabado_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('sabado_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['sabado_tarde_de']) ? $_POST['sabado_tarde_de'] : ($horario ? $horario->sabado_tarde_de : '14:00:00')) , 'class="form-control" id="sabado_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('sabado_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['sabado_tarde_ate']) ? $_POST['sabado_tarde_ate'] : ($horario ? $horario->sabado_tarde_ate : '18:00:00')), 'class="form-control" id="sabado_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">
                                <?= lang("domingo", "domingo"); ?>
                            </label>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        manhã
                                    </div>
                                    <?php
                                    echo form_dropdown('domingo_manha_de', $this->site->getSelectHorarios(), (isset($_POST['domingo_manha_de']) ? $_POST['domingo_manha_de'] : ($horario ? $horario->domingo_manha_de : '08:00:00')) , 'class="form-control" id="domingo_manha_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('domingo_manha_ate', $this->site->getSelectHorarios(), (isset($_POST['domingo_manha_ate']) ? $_POST['domingo_manha_ate'] : ($horario ? $horario->domingo_manha_ate : '12:00:00')) , 'class="form-control" id="domingo_manha_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        tarde
                                    </div>
                                    <?php
                                    echo form_dropdown('domingo_tarde_de', $this->site->getSelectHorarios(),(isset($_POST['domingo_tarde_de']) ? $_POST['domingo_tarde_de'] : ($horario ? $horario->domingo_tarde_de : '14:00:00')) , 'class="form-control" id="domingo_tarde_de" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        até
                                    </div>
                                    <?php
                                    echo form_dropdown('domingo_tarde_ate', $this->site->getSelectHorarios(), (isset($_POST['domingo_tarde_ate']) ? $_POST['domingo_tarde_ate'] : ($horario ? $horario->domingo_tarde_ate : '18:00:00')), 'class="form-control" id="domingo_tarde_ate" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both; height: 10px;"></div>
                <div class="form-group">
                    <?php echo form_submit('atualizarHorarioComercial', lang("atualizar_horario_comercial"), 'class="btn btn-primary"'); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>