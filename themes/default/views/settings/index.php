<?php
$wm = array('0' => lang('no'), '1' => lang('yes'));
$ps = array('0' => lang("disable"), '1' => lang("enable"));
?>
<script>
    $(document).ready(function () {
        <?php if(isset($message)) { echo 'localStorage.clear();'; } ?>
        var timezones = <?php echo json_encode(DateTimeZone::listIdentifiers(DateTimeZone::ALL)); ?>;
        $('#timezone').autocomplete({
            source: timezones
        });
        if ($('#protocol').val() == 'smtp') {
            $('#smtp_config').slideDown();
        } else if ($('#protocol').val() == 'sendmail') {
            $('#sendmail_config').slideDown();
        }
        $('#protocol').change(function () {
            if ($(this).val() == 'smtp') {
                $('#sendmail_config').slideUp();
                $('#smtp_config').slideDown();
            } else if ($(this).val() == 'sendmail') {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideDown();
            } else {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideUp();
            }
        });
        $('#overselling').change(function () {
            if ($(this).val() == 1) {
                if ($('#accounting_method').select2("val") != 2) {
                    bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                    $('#accounting_method').select2("val", '2');
                }
            }
        });
        $('#accounting_method').change(function () {
            var oam = <?=$Settings->accounting_method?>, nam = $(this).val();
            if (oam != nam) {
                bootbox.alert('<?=lang('accounting_method_change_alert')?>');
            }
        });
        $('#accounting_method').change(function () {
            if ($(this).val() != 2) {
                if ($('#overselling').select2("val") == 1) {
                    bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                    $('#overselling').select2("val", 0);
                }
            }
        });
        $('#item_addition').change(function () {
            if ($(this).val() == 1) {
                bootbox.alert('<?=lang('product_variants_feature_x')?>');
            }
        });
        var sac = $('#sac').val()
        if(sac == 1) {
            $('.nsac').slideUp();
        } else {
            $('.nsac').slideDown();
        }
        $('#sac').change(function () {
            if ($(this).val() == 1) {
                $('.nsac').slideUp();
            } else {
                $('.nsac').slideDown();
            }
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('system_settings'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('update_info'); ?></p>
                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("system_settings", $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border"><?= lang('site_config') ?></legend>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label" for="plano_sagtur"><?= lang("plano_sagtur"); ?></label>
                                    <div class="controls"> <?php
                                        foreach ($planos as $plano) {
                                            $pl[$plano->id] = $plano->name;
                                        }
                                        echo form_dropdown('plano_sagtur', $pl, $Settings->plano_sagtur, 'class="form-control tip" readonly disabled id="plano_sagtur" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <?= lang("numero_ususarios", "numero_ususarios"); ?>
                                    <?php echo form_input('numero_ususarios', $Settings->numero_ususarios, 'class="form-control tip" readonly id="numero_ususarios"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <?= lang("dia_vencimento_plano", "dia_vencimento_plano"); ?>
                                    <?php echo form_input('dia_vencimento_plano', $Settings->dia_vencimento_plano, 'class="form-control tip" readonly id="dia_vencimento_plano"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("site_name", "site_name"); ?>
                                    <?php echo form_input('site_name', $Settings->site_name, 'class="form-control tip" id="site_name"  required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("language", "language"); ?>
                                    <?php
                                    $lang = array(
										'english'      => 'English',
                                        'portugues'    => 'Portugues',
                                     ); 
                                    echo form_dropdown('language', $lang, $Settings->language, 'class="form-control tip" id="language" required="required" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="currency"><?= lang("default_currency"); ?></label>

                                    <div class="controls"> <?php
                                        foreach ($currencies as $currency) {
                                            $cu[$currency->code] = $currency->name;
                                        }
                                        echo form_dropdown('currency', $cu, $Settings->default_currency, 'class="form-control tip" id="currency" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="theme"><?= lang("theme"); ?></label>

                                    <div class="controls">
                                        <?php
                                        $themes = array(
                                            'default' => 'Default'
                                        );
                                        echo form_dropdown('theme', $themes, $Settings->theme, 'id="theme" class="form-control tip" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"
                                           for="rows_per_page"><?= lang("rows_per_page"); ?></label>

                                    <?php echo form_input('rows_per_page', $Settings->rows_per_page, 'class="form-control tip" id="rows_per_page" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="dateformat"><?= lang("dateformat"); ?></label>

                                    <div class="controls">
                                        <?php
                                        foreach ($date_formats as $date_format) {
                                            $dt[$date_format->id] = $date_format->js;
                                        }
                                        echo form_dropdown('dateformat', $dt, $Settings->dateformat, 'id="dateformat" class="form-control tip" style="width:100%;" required="required"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="timezone"><?= lang("timezone"); ?></label>
                                    <?php
                                    $timezone_identifiers = DateTimeZone::listIdentifiers();
                                    foreach ($timezone_identifiers as $tzi) {
                                        $tz[$tzi] = $tzi;
                                    }
                                    ?>
                                    <?php echo form_dropdown('timezone', $tz, TIMEZONE, 'class="form-control tip" id="timezone" required="required"'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label"
                                           for="warehouse"><?= lang("default_warehouse"); ?></label>

                                    <div class="controls"> <?php
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name . ' (' . $warehouse->code . ')';
                                        }
                                        echo form_dropdown('warehouse', $wh, $Settings->default_warehouse, 'class="form-control tip" id="warehouse" required="required" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("default_biller", "biller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('receptive', 1, $Settings->receptive, 'id="receptive"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('is_receptive'); ?></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('avaliar', 1, $Settings->avaliar, 'id="avaliar"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('is_avaliar'); ?></label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('usar_captacao', 1, $Settings->usar_captacao, 'id="usar_captacao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('is_usar_captacao'); ?></label>
                                </div>
                            </div>
                    </fieldset>
                    <fieldset class="scheduler-border" style="display: none;">
                        <legend class="scheduler-border"><?= lang('products') ?></legend>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="image_size"><?= lang("image_size"); ?> (Width :
                                    Height) *</label>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php echo form_input('iwidth', $Settings->iwidth, 'class="form-control tip" id="iwidth" placeholder="image width" required="required"'); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <?php echo form_input('iheight', $Settings->iheight, 'class="form-control tip" id="iheight" placeholder="image height" required="required"'); ?></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="thumbnail_size"><?= lang("thumbnail_size"); ?>
                                    (Width : Height) *</label>

                                <div class="row">
                                    <div class="col-xs-6">
                                        <?php echo form_input('twidth', $Settings->twidth, 'class="form-control tip" id="twidth" placeholder="thumbnail width" required="required"'); ?>
                                    </div>
                                    <div class="col-xs-6">
                                        <?php echo form_input('theight', $Settings->theight, 'class="form-control tip" id="theight" placeholder="thumbnail height" required="required"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('watermark', 'watermark'); ?>
                                <?php
                                    echo form_dropdown('watermark', $wm, (isset($_POST['watermark']) ? $_POST['watermark'] : $Settings->watermark), 'class="tip form-control" required="required" id="watermark" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('sales') ?></legend>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label"
                                       for="reference_format"><?= lang("reference_format"); ?></label>
                                <div class="controls">
                                    <?php
                                    $ref = array(1 => lang('prefix_year_no'), 2 => lang('prefix_month_year_no'), 3 => lang('sequence_number'), 4 => lang('random_number'));
                                    echo form_dropdown('reference_format', $ref, $Settings->reference_format, 'class="form-control tip" required="required" id="reference_format" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('exibirCancelamentosListaVenda', 'exibirCancelamentosListaVenda'); ?>
                                <?php
                                echo form_dropdown('exibirCancelamentosListaVenda', $wm, (isset($_POST['exibirCancelamentosListaVenda']) ? $_POST['exibirCancelamentosListaVenda'] : $Settings->exibirCancelamentosListaVenda), 'class="tip form-control" required="required" id="exibirCancelamentosListaVenda" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3" style="display: none;">
                            <div class="form-group">
                                <?= lang('usarOrcamentoLoja', 'usarOrcamentoLoja'); ?>
                                <?php
                                echo form_dropdown('usarOrcamentoLoja', $wm, (isset($_POST['usarOrcamentoLoja']) ? $_POST['usarOrcamentoLoja'] : $Settings->usarOrcamentoLoja), 'class="tip form-control" required="required" id="usarOrcamentoLoja" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('exibirDadosFaturaVoucher', 'exibirDadosFaturaVoucher'); ?>
                                <?php
                                echo form_dropdown('exibirDadosFaturaVoucher', $wm, (isset($_POST['exibirDadosFaturaVoucher']) ? $_POST['exibirDadosFaturaVoucher'] : $Settings->exibirDadosFaturaVoucher), 'class="tip form-control" required="required" id="exibirDadosFaturaVoucher" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('ocultarProdutosSemEstoqueLoja', 'ocultarProdutosSemEstoqueLoja'); ?>
                                <?php
                                echo form_dropdown('ocultarProdutosSemEstoqueLoja', $wm, (isset($_POST['ocultarProdutosSemEstoqueLoja']) ? $_POST['ocultarProdutosSemEstoqueLoja'] : $Settings->ocultarProdutosSemEstoqueLoja), 'class="tip form-control" required="required" id="ocultarProdutosSemEstoqueLoja" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang('habilitar_cupom_desconto', 'habilitar_cupom_desconto'); ?>
                                <?php
                                echo form_dropdown('habilitar_cupom_desconto', $wm, (isset($_POST['habilitar_cupom_desconto']) ? $_POST['habilitar_cupom_desconto'] : $Settings->habilitar_cupom_desconto), 'class="tip form-control" required="required" id="habilitar_cupom_desconto" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('envio_email_venda_manual', 'envio_email_venda_manual'); ?>
                                <?php
                                echo form_dropdown('envio_email_venda_manual', $wm, (isset($_POST['envio_email_venda_manual']) ? $_POST['envio_email_venda_manual'] : $Settings->envio_email_venda_manual), 'class="tip form-control" required="required" id="envio_email_venda_manual" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('send_email_canceling_sale', 'send_email_canceling_sale'); ?>
                                <?php
                                echo form_dropdown('send_email_canceling_sale', $wm, (isset($_POST['send_email_canceling_sale']) ? $_POST['send_email_canceling_sale'] : $Settings->send_email_canceling_sale), 'class="tip form-control" required="required" id="send_email_canceling_sale" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("product_contrato", "contrato") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('contrato', $Settings->contrato, 'class="form-control" id="contrato"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("product_informacoes_importantes_voucher", "informacoesImportantesVoucher") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('informacoesImportantesVoucher', $Settings->informacoesImportantesVoucher, 'class="form-control" id="informacoesImportantesVoucher"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('prefix') ?></legend>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label" for="sales_prefix"><?= lang("sales_prefix"); ?></label>

                                <?php echo form_input('sales_prefix', $Settings->sales_prefix, 'class="form-control tip" id="sales_prefix"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label"
                                       for="return_prefix"><?= lang("return_prefix"); ?></label>

                                <?php echo form_input('return_prefix', $Settings->return_prefix, 'class="form-control tip" id="return_prefix"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label"
                                       for="payment_prefix"><?= lang("payment_prefix"); ?></label>

                                <?php echo form_input('payment_prefix', $Settings->payment_prefix, 'class="form-control tip" id="payment_prefix"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="control-label"
                                       for="purchase_prefix"><?= lang("purchase_prefix"); ?></label>

                                <?php echo form_input('purchase_prefix', $Settings->purchase_prefix, 'class="form-control tip" id="purchase_prefix"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?= lang("assinatura", "assinatura") ?>
                                <input id="assinatura" type="file" data-browse-label="<?= lang('browse'); ?>" name="assinatura" data-show-upload="false"
                                       data-show-preview="false" accept="image/*" class="form-control file">
                            </div>
                        </div>
                        <?php if ($Settings->assinatura){?>
                            <div class="col-md-12">
                                <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->assinatura ?>"
                                     alt="Logo marca <?= $Settings->site_name ?>" class="img-responsive img-thumbnail"/>
                            </div>
                        <?php } ?>
                    </fieldset>

                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border"><?= lang('config_loja') ?></legend>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("facebook", "facebook"); ?>
                                <?php echo form_input('facebook', $Settings->facebook, 'class="form-control tip" id="facebook"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("instagram", "instagram"); ?>
                                <?php echo form_input('instagram', $Settings->instagram, 'class="form-control tip" id="instagram"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("youtube", "youtube"); ?>
                                <?php echo form_input('youtube', $Settings->youtube, 'class="form-control tip" id="youtube"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("frase_site", "frase_site"); ?>
                                <?php echo form_input('frase_site', $Settings->frase_site, 'class="form-control tip" id="frase_site"'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("pixelFacebook", "pixelFacebook"); ?>
                                <?php echo form_input('pixelFacebook', $Settings->pixelFacebook, 'class="form-control tip" id="youtube"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("googleAnalytics", "googleAnalytics"); ?>
                                <?php echo form_input('googleAnalytics', $Settings->googleAnalytics, 'class="form-control tip" id="youtube"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('filtrar_itens_loja', 'filtrar_itens_loja'); ?>
                                <?php
                                echo form_dropdown('filtrar_itens_loja', $wm, (isset($_POST['filtrar_itens_loja']) ? $_POST['filtrar_itens_loja'] : $Settings->filtrar_itens_loja), 'class="tip form-control" required="required" id="filtrar_itens_loja" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("theme_color", "theme_color"); ?>
                                <?php echo form_input('theme_color', $Settings->theme_color, 'class="form-control tip" id="theme_color"', 'color'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("theme_color_secondary", "theme_color_secondary"); ?>
                                <?php echo form_input('theme_color_secondary', $Settings->theme_color_secondary, 'class="form-control tip" id="theme_color_secondary"', 'color'); ?>
                            </div>
                        </div>

                        <div class="col-md-4" style="display: none;">
                            <div class="form-group">
                                <?= lang("theme_site", "theme_site"); ?>
                                <?php
                                $lang = array(
                                    'theme01'      => lang('theme01'),
                                    'theme02'       => lang('theme02'),
                                );
                                echo form_dropdown('theme_site', $lang, $Settings->theme_site, 'class="form-control tip" id="theme_site" required="required" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang('view_logomarca_site', 'view_logomarca_site'); ?>
                                <?php
                                echo form_dropdown('view_logomarca_site', $wm, (isset($_POST['view_logomarca_site']) ? $_POST['view_logomarca_site'] : $Settings->view_logomarca_site), 'class="tip form-control" required="required" id="view_logomarca_site" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("site_width", "site_width"); ?>
                                <?php echo form_input('site_width', $Settings->site_width, 'class="form-control tip" id="site_width"'); ?>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("site_height", "site_height"); ?>
                                <?php echo form_input('site_height', $Settings->site_height, 'class="form-control tip" id="site_height"'); ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("quantity_thumbs", "quantity_thumbs"); ?>
                                <?php echo form_input('quantity_thumbs', $Settings->quantity_thumbs, 'class="form-control tip" id="quantity_thumbs"'); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("horario_atendimento", "horario_atendimento"); ?>
                                <?php echo form_input('horario_atendimento', $Settings->horario_atendimento, 'class="form-control tip" id="horario_atendimento"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('usar_email_area_cliente', 1, $Settings->usar_email_area_cliente, 'id="usar_email_area_cliente"'); ?>
                                <label for="attributes" class="padding05"><?= lang('usar_email_area_cliente'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('usar_dtnascimento_area_cliente', 1, $Settings->usar_dtnascimento_area_cliente, 'id="usar_dtnascimento_area_cliente"'); ?>
                                <label for="attributes" class="padding05"><?= lang('usar_dtnascimento_area_cliente'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('use_product_landing_page', 1, $Settings->use_product_landing_page, 'id="use_product_landing_page"'); ?>
                                <label for="attributes" class="padding05"><?= lang('use_product_landing_page'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('show_payment_report_shipment', 1, $Settings->show_payment_report_shipment, 'id="show_payment_report_shipment"'); ?>
                                <label for="attributes" class="padding05"><?= lang('show_payment_report_shipment'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('dependent_shipping_report', 1, $Settings->dependent_shipping_report, 'id="dependent_shipping_report"'); ?>
                                <label for="attributes" class="padding05"><?= lang('dependent_shipping_report'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading open" slider="div_slider1"><i class="fa-fw fa fa-user"></i> <?= lang("slider1", "slider1") ?></div>
                                <div class="panel-body" style="display: none;" id="div_slider1">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= lang("logo_shop", "logo_shop") ?>
                                            <input id="logo_shop" type="file" data-browse-label="<?= lang('browse'); ?>" name="logo_shop" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->logo_shop ?>"
                                             alt="Logo marca <?= $Settings->site_name ?>" class="img-responsive img-thumbnail"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading open" slider="div_slider2"><i class="fa-fw fa fa-user"></i> <?= lang("slider2", "slider2") ?></div>
                                <div class="panel-body" id="div_slider2" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("slider1_slogan", "slider1_slogan"); ?>
                                            <?php echo form_input('slider1_slogan', $Settings->slider1_slogan, 'class="form-control tip" id="slider1_slogan"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= lang("slider1", "slider1") ?>
                                            <input id="slider1" type="file" data-browse-label="<?= lang('browse'); ?>" name="slider1" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                    </div>
                                    <?php if ($Settings->slider1) {?>
                                        <div class="col-md-12 gallery-image">
                                            <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->slider1 ?>"
                                                 alt="Logo marca <?= $Settings->slider1 ?>" class="img-responsive img-thumbnail"/>
                                            <a href="#" class="delimgslider" data-item-id="slider1"><i class="fa fa-times" style="font-size: 20px;"></i></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading open" slider="div_slider3"><i class="fa-fw fa fa-user"></i> <?= lang("slider3", "slider3") ?></div>
                                <div class="panel-body" id="div_slider3" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("slider2_slogan", "slider2_slogan"); ?>
                                            <?php echo form_input('slider2_slogan', $Settings->slider2_slogan, 'class="form-control tip" id="slider2_slogan"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= lang("slider3", "slider3") ?>
                                            <input id="slider2" type="file" data-browse-label="<?= lang('browse'); ?>" name="slider2" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                    </div>
                                    <?php if ($Settings->slider2 ) {?>
                                        <div class="col-md-12 gallery-image">
                                            <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->slider2 ?>"
                                                 alt="Logo marca <?= $Settings->slider2 ?>" class="img-responsive img-thumbnail"/>
                                            <a href="#" class="delimgslider" data-item-id="slider2"><i class="fa fa-times" style="font-size: 20px;"></i></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading open" slider="div_slider4"><i class="fa-fw fa fa-user"></i> <?= lang("slider4", "slider4") ?></div>
                                <div class="panel-body" id="div_slider4" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("slider3_slogan", "slider3_slogan"); ?>
                                            <?php echo form_input('slider3_slogan', $Settings->slider3_slogan, 'class="form-control tip" id="slider3_slogan"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= lang("slider4", "slider4") ?>
                                            <input id="slider3" type="file" data-browse-label="<?= lang('browse'); ?>" name="slider3" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                    </div>

                                    <?php if ($Settings->slider3) { ?>
                                        <div class="col-md-12 gallery-image">
                                            <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->slider3 ?>"
                                                 alt="Logo marca <?= $Settings->slider3 ?>" class="img-responsive img-thumbnail"/>
                                            <a href="#" class="delimgslider" data-item-id="slider3"><i class="fa fa-times" style="font-size: 20px;"></i></a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading open" slider="about_photo"><i class="fa-fw fa fa-user"></i> <?= lang("about_photo", "about_photo") ?></div>
                                <div class="panel-body" id="about_photo" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= lang("about_photo", "about_photo") ?>
                                            <input id="about_photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="about_photo" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <img id="pr-image" src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->about_photo ?>"
                                             alt="Logo marca <?= $Settings->about_photo ?>" class="img-responsive img-thumbnail"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("about", "about") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('about', $Settings->about, 'class="form-control" id="about"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("termos_aceite", "termos_aceite") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('termos_aceite', $Settings->termos_aceite, 'class="form-control" id="termos_aceite"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px;">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("instrucoes_planta_onibus", "instrucoes_planta_onibus") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('instrucoes_planta_onibus', $Settings->instrucoes_planta_onibus, 'class="form-control" id="instrucoes_planta_onibus"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px;">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("instrucoes_tipo_cobranca", "instrucoes_tipo_cobranca") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('instrucoes_tipo_cobranca', $Settings->instrucoes_tipo_cobranca, 'class="form-control" id="instrucoes_tipo_cobranca"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px;">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("instrucoes_faixa_valores", "instrucoes_faixa_valores") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('instrucoes_faixa_valores', $Settings->instrucoes_faixa_valores, 'class="form-control" id="instrucoes_faixa_valores"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("instrucoes_sinal_pagamento", "instrucoes_sinal_pagamento") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('instrucoes_sinal_pagamento', $Settings->instrucoes_sinal_pagamento, 'class="form-control" id="instrucoes_sinal_pagamento"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 15px;">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-info"></i> <?= lang("instrucoes_local_embarque", "instrucoes_local_embarque") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <div class="form-group all">
                                            <?= form_textarea('instrucoes_local_embarque', $Settings->instrucoes_local_embarque, 'class="form-control" id="instrucoes_local_embarque"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <fieldset class="scheduler-border" style="display: none;">
                        <legend class="scheduler-border"><?= lang('money_number_format') ?></legend>
                        <div class="col-md-4">
                            <!-- display_symbol form dropdown -->
                            <div class="form-group">
                                <?= lang('display_currency_symbol', 'display_symbol'); ?>
                                <?php $opts = array(0 => lang('disable'), 1 => lang('before'), 2 => lang('after')); ?>
                                <?= form_dropdown('display_symbol', $opts, $Settings->display_symbol, 'class="form-control" id="display_symbol" style="width:100%;" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="decimals"><?= lang("decimals"); ?></label>

                                <div class="controls"> <?php
                                    $decimals = array(0 => lang('disable'), 1 => '1', 2 => '2', 3 => '3', 4 => '4');
                                    echo form_dropdown('decimals', $decimals, $Settings->decimals, 'class="form-control tip" id="decimals"  style="width:100%;" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="control-label" for="qty_decimals"><?= lang("qty_decimals"); ?></label>

                                <div class="controls"> <?php
                                    $qty_decimals = array(0 => lang('disable'), 1 => '1', 2 => '2', 3 => '3', 4 => '4');
                                    echo form_dropdown('qty_decimals', $qty_decimals, $Settings->qty_decimals, 'class="form-control tip" id="qty_decimals"  style="width:100%;" required="required"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="nsac">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="decimals_sep"><?= lang("decimals_sep"); ?></label>

                                    <div class="controls"> <?php
                                        $dec_point = array('.' => lang('dot'), ',' => lang('comma'));
                                        echo form_dropdown('decimals_sep', $dec_point, $Settings->decimals_sep, 'class="form-control tip" id="decimals_sep"  style="width:100%;" required="required"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label class="control-label" for="thousands_sep"><?= lang("thousands_sep"); ?></label>
                                    <div class="controls"> <?php
                                        $thousands_sep = array('.' => lang('dot'), ',' => lang('comma'), '0' => lang('space'));
                                        echo form_dropdown('thousands_sep', $thousands_sep, $Settings->thousands_sep, 'class="form-control tip" id="thousands_sep"  style="width:100%;" required="required"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- symbol form input -->
                            <div class="form-group">
                                <?= lang('currency_symbol', 'symbol'); ?>
                                <?= form_input('symbol', $Settings->symbol, 'class="form-control" id="symbol" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </fieldset>

                    <fieldset class="scheduler-border" style="display: none;">
                        <legend class="scheduler-border"><?= lang('award_points') ?></legend>
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-5 col-xs-6">
                                        <?= lang('each_in_sale'); ?><br>
                                        <?= form_input('each_sale', $this->sma->formatDecimal($Settings->each_sale), 'class="form-control"'); ?>
                                    </div>
                                    <div class="col-sm-1 col-xs-1 text-center"><i class="fa fa-arrow-right"></i>
                                    </div>
                                    <div class="col-sm-5 col-xs-5">
                                        <?= lang('award_points'); ?><br>
                                        <?= form_input('sa_point', $Settings->sa_point, 'class="form-control"'); ?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <div style="clear: both; height: 10px;"></div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="controls">
                        <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
</div>

<script>

    $(document).ready(function () {
       $('.open').click(function(){
           open($(this).attr('slider'));
       });

        $(document).on('click', '.delimgslider', function(e) {
            e.preventDefault();
            var ele = $(this), id = $(this).attr('data-item-id');
            bootbox.confirm(lang.r_u_sure, function(result) {
                if(result == true) {
                    $.get(site.base_url+'system_settings/delete_image_slider/'+id, function(data) {
                        if (data.error === 0) {
                            addAlert(data.msg, 'success');
                            ele.parent('.gallery-image').remove();
                        }
                    });
                }
            });
            return false;
        });
    });

    function open(id) {
        if ($( "#"+id).is( ":visible" )) {
            $("#"+id).hide();
        } else {
            $("#"+id).show();
        }
    }
</script>
