<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('configuracoes_taxas_tipo_cobranca').' '.$objTipoCobranca->name; ?></h4>
        </div>
        <?php echo form_open_multipart("system_settings/taxa_cobranca/".$objTipoCobranca->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>
            <div class="box">
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group all" style="display: none;">
                                <?= lang("name", "name") ?>
                                <div class="controls" id="subcat_data"> <?php
                                    echo form_input('name', $taxa->name, 'class="form-control" id="name"');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("diasAvancaPrimeiroVencimento", "diasAvancaPrimeiroVencimento") ?>
                                <div class="controls">
                                    <?php echo form_input('diasAvancaPrimeiroVencimento', $taxa_cobranca->diasAvancaPrimeiroVencimento, 'class="form-control mask_integer"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("diasMaximoPagamentoAntesViagem", "diasMaximoPagamentoAntesViagem") ?>
                                <div class="controls">
                                    <?php echo form_input('diasMaximoPagamentoAntesViagem', $taxa_cobranca->diasMaximoPagamentoAntesViagem, 'class="form-control mask_integer"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("taxaIntermediacao", "taxaIntermediacao") ?>
                                <div class="controls">
                                    <?php echo form_input('taxaIntermediacao', $taxa_cobranca->taxaIntermediacao, 'class="form-control mask_money"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("taxaFixaIntermediacao", "taxaFixaIntermediacao") ?>
                                <div class="controls">
                                    <?php echo form_input('taxaFixaIntermediacao', $taxa_cobranca->taxaFixaIntermediacao, 'class="form-control mask_money"'); ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($objTipoCobranca->integracao == 'valepay') {?>
                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang("numero_max_parcelas", "numero_max_parcelas") ?>
                                    <div class="controls">
                                        <?php
                                        $parcelas = array(
                                            '1' => '1X',
                                            '2' => '2X',
                                            '3' => '3X',
                                            '4' => '4X',
                                            '5' => '5X',
                                            '6' => '6X',
                                            '7' => '7X',
                                            '8' => '8X',
                                            '9' => '9X',
                                            '10' => '10X',
                                            '11' => '11X',
                                            '12' => '12X',
                                        );
                                        echo form_dropdown('numero_max_parcelas', $parcelas, $taxa_cobranca->numero_max_parcelas, 'class="form-control" id="taxaFixaIntermediacao" ');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang("numero_max_parcelas_sem_juros", "numero_max_parcelas_sem_juros") ?>
                                    <div class="controls">
                                        <?php
                                        $parcelas_sem_juros = array(
                                            '0' => lang('cobrar_juros_todas_parcelas'),
                                            '1' => '1X',
                                            '2' => '2X',
                                            '3' => '3X',
                                            '4' => '4X',
                                            '5' => '5X',
                                            '6' => '6X',
                                            '7' => '7X',
                                            '8' => '8X',
                                            '9' => '9X',
                                            '10' => '10X',
                                            '11' => '11X',
                                            '12' => '12X',
                                        );
                                        echo form_dropdown('numero_max_parcelas_sem_juros', $parcelas_sem_juros, $taxa_cobranca->numero_max_parcelas_sem_juros, 'class="form-control" id="numero_max_parcelas_sem_juros" ');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-12">
                            <div class="form-group all">
                                <div class="controls">
                                    <?php echo form_checkbox('is_sinal', 1 , $taxa_cobranca->is_sinal, 'id="is_sinal" class="form-control"'); ?>
                                    <?= lang("is_sinal", "is_sinal") ?>
                                </div>
                            </div>
                        </div>

                        <div id="div_sinal" style="<?php if (!$taxa_cobranca->is_sinal) echo 'display: none;';?>">
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="panel panel-warning">
                                    <div class="panel-heading"><h4><?= lang('Sinal') ?></h4></div>
                                    <div class="panel-body" style="padding: 5px;">
                                        <div class="col-md-6">
                                            <div class="form-group all">
                                                <?= lang("sinal_primeiro_vencimento", "sinal") ?>
                                                <div class="controls">
                                                    <?php echo form_input('sinal', ($taxa_cobranca->sinal != null ? $taxa_cobranca->sinal : 0.00), 'class="form-control mask_money"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group all">
                                                <?= lang("tipo_sinal", "tipo_sinal") ?>
                                                <?php
                                                $opts = array(
                                                    'percentual' => lang('em_percentual'),
                                                    'absoluto' => lang('absoluto'),
                                                );
                                                echo form_dropdown('tipo_sinal', $opts, $taxa_cobranca->tipo_sinal, 'class="form-control"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group all">
                                                <?= lang("acrescimo_desconto_sinal", "acrescimo_desconto_sinal") ?>
                                                <?php
                                                $opts = array(
                                                    'acrescimo' => lang('acrescimo'),
                                                    'desconto' => lang('desconto')
                                                );
                                                echo form_dropdown('acrescimo_desconto_sinal', $opts, $taxa_cobranca->acrescimo_desconto_sinal, 'class="form-control" required="required"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group all">
                                                <?= lang("valor_acres_desc_sinal", "valor_acres_desc_sinal") ?>
                                                <?= form_input('valor_acres_desc_sinal', ($taxa_cobranca->valor_acres_desc_sinal != null ? $taxa_cobranca->valor_acres_desc_sinal : 0.00), 'class="form-control tip mask_money" required="required"') ?>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group all">
                                                <?= lang("tipo_acres_desc_sinal", "tipo_acres_desc_sinal") ?>
                                                <?php
                                                $opts = array(
                                                    'percentual' => lang('em_percentual'),
                                                    'absoluto' => lang('absoluto'),
                                                );
                                                echo form_dropdown('tipo_acres_desc_sinal', $opts, $taxa_cobranca->tipo_acres_desc_sinal, 'class="form-control" required="required"');
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group all">
                                <div class="controls">
                                    <?php
                                    $exibirNaSemanaDaViagem = FALSE;
                                    if ($taxa_cobranca->exibirNaSemanaDaViagem == 1) $exibirNaSemanaDaViagem= TRUE;

                                    echo form_checkbox('exibirNaSemanaDaViagem', 1 , $exibirNaSemanaDaViagem, 'class="form-control"'); ?>
                                    <?= lang("exibirNaSemanaDaViagem", "exibirNaSemanaDaViagem") ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="<?php if ($objTipoCobranca->tipo != 'carne_cartao_transparent') echo 'display: none';?>">
                            <div class="form-group all" style="display: none;">
                                <div class="controls">
                                    <?php
                                    $usarTaxasPagSeguro = FALSE;
                                    if ($taxa_cobranca->usarTaxasPagSeguro == 1) $usarTaxasPagSeguro= TRUE;

                                    echo form_checkbox('usarTaxasPagSeguro', 1 , $usarTaxasPagSeguro, 'class="form-control"'); ?>
                                    <?= lang("usarTaxasPagSeguro", "usarTaxasPagSeguro") ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <table id="attrTableHospedagemTaxas" class="table table-bordered table-condensed table-striped table-hover" style="margin-bottom: 20px;cursor: pointer;">
                                <thead>
                                <tr class="active">
                                    <th class="col-md-1" style="text-align: center;"><?= 'Parcelas' ?></th>
                                    <th class="col-md-1" style="text-align: center;width: 1%;"><?= lang('Ativar') ?></th>
                                    <th class="col-md-2" style="text-align: left;"><?= lang('tipo') ?></th>
                                    <th class="col-md-2" style="text-align: right;">Taxa por parcela</th>
                                    <th class="col-md-2" style="text-align: right;"><?= lang('Em (%)/(R$)') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($tiposCobranca as $tipoCobranca) {
                                    if ($tipoCobranca->id != $objTipoCobranca->id) continue; ?>
                                    <?php foreach ($condicoesPagamento as $condicaoPagamento){

                                        $verificar = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobranca->id, $condicaoPagamento ->id);

                                        $ativo = FALSE;
                                        $acrescimoDescontoTipo = '';
                                        $tipo = '';
                                        $valor = '0.00';
                                        $diasAvancaPrimeiroVencimento = 0;
                                        $diasMaximoPagamentoAntesViagem = 0;
                                        $sinal = 0;
                                        $taxaIntermediacao = 0;
                                        $taxaFixaIntermediacao = 0;

                                        if (!empty($verificar) ) {

                                            if ($verificar->ativo == 1) $ativo = TRUE;

                                            $acrescimoDescontoTipo = $verificar->acrescimoDescontoTipo;
                                            $tipo = $verificar->tipo;
                                            $valor = $verificar->valor;
                                            $diasAvancaPrimeiroVencimento = $verificar->diasAvancaPrimeiroVencimento != null ? $verificar->diasAvancaPrimeiroVencimento  : 0;
                                            $diasMaximoPagamentoAntesViagem = $verificar->diasMaximoPagamentoAntesViagem != null ? $verificar->diasMaximoPagamentoAntesViagem : 0;
                                            $sinal = $verificar->sinal != null ? $verificar->sinal : 0;
                                            $taxaIntermediacao = $verificar->taxaIntermediacao != null ? $verificar->taxaIntermediacao : 0;
                                            $taxaFixaIntermediacao = $verificar->taxaFixaIntermediacao != null ? $verificar->taxaFixaIntermediacao : 0;

                                        } ?>
                                        <tr>
                                            <td style="text-align: center;"><input type="hidden" name="formaPagamento[]" value="<?php echo $condicaoPagamento->id;?>" /><?php echo $condicaoPagamento->name;?></td>
                                            <td style="text-align: center;" colspan="1">
                                                <?php echo form_checkbox('ativoCondicaoPagamento[]', $tipoCobranca->id.'_'.$condicaoPagamento->id, $ativo, ''); ?>
                                                <input type="hidden" name="tipoCobranca[]" value="<?php echo $tipoCobranca->id;?>" />
                                            </td>
                                            <td>
                                                <?php
                                                $opts = array(
                                                    'acrescimo' => lang('acrescimo'),
                                                    'desconto' => lang('desconto')
                                                );
                                                echo form_dropdown('acrescimoDescontoCondicaoPagamento[]', $opts, $acrescimoDescontoTipo, 'class="form-control" required="required"');
                                                ?>
                                            </td>
                                            <td>
                                                <?= form_input('precoCondicaoPagamento[]', $valor, 'class="form-control tip mask_money" required="required"') ?>
                                            </td>
                                            <td>
                                                <?php
                                                $opts = array(
                                                    'percentual' => lang('em_percentual'),
                                                    'absoluto' => lang('absoluto'),
                                                );
                                                echo form_dropdown('tipoCobrancaCondicaoPagamento[]', $opts, $tipo, 'class="form-control" required="required"');
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }?>
                                <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="text-align: left;">
                    <button type="submit" class="btn btn-primary" id="updateAttr"><?= lang('save') ?></button>
                </div>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">


    $(document).ready(function () {
        $('#is_sinal').on('ifChecked', function (e) {
           $('#div_sinal').show();
        });

        $('#is_sinal').on('ifUnchecked', function (e) {
            $('#div_sinal').hide();
        });
    });

    $(function(){

        $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        });

        $('.mask_money').bind('keypress',mask.money);
        $('.mask_money').click(function(){$(this).select();});
    });

    $(function(){
        $('.mask_integer').bind('keypress',mask_integer.money);
        $('.mask_integer').click(function(){$(this).select();});
    });
</script>
