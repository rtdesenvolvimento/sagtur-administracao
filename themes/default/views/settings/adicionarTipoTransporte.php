<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_tipo_transporte'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarTipoTransporte", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang('tipos_transporte_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('totalPoltronas', 'totalPoltronas'); ?>
                <?= form_input('totalPoltronas', '', 'class="form-control mask_integer" id="totalPoltronas" required="required"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarTipoTransporte', lang('adicionar_tipo_transporte'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    var mask_integer = {
        money : function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'$1');
                }
                return v;
            };
            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(function(){
        $('.mask_integer').bind('keypress',mask_integer.money);
        $('.mask_integer').click(function(){$(this).select();});
    });
</script>
