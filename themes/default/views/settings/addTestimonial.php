<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_testimonial'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("site_settings/addTestimonial", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] :  '') , 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('profession', 'profession'); ?>
                <?= form_input('profession', (isset($_POST['profession']) ? $_POST['profession'] :  '') , 'class="form-control" id="profession" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("testimonial", "testimonial"); ?>
                <?php echo form_textarea('testimonial', (isset($_POST['testimonial']) ? $_POST['testimonial'] : ""), 'class="form-control" id="testimonial" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
            <div class="form-group">
                <?= lang("photo", "photo") ?>
                <input id="photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="photo" data-show-upload="false"
                       data-show-preview="false" accept="image/*" class="form-control file">
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_testimonial', lang('add_testimonial'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>