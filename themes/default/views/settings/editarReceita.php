<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_receita'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("system_settings/editarReceita/" . $category->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group">
                <?= lang("tipo", "tipo") ?>
                <?php
                $opts = array(
                    'grupo' => lang('grupo'),
                    'subgrupo' => lang('subgrupo')
                );
                echo form_dropdown('tipo', $opts, $category->tipo, ' class="form-control" id="tipo" required="required"');
                ?>
            </div>
            <div class="form-group" id="divDRE">
                <?= lang('dre', 'dre'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-cogs"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbDRE[''] = lang('select').' '.lang('dre');
                    foreach ($dre as $dr) {
                        $cbDRE[$dr->id] =  $dr->name;
                    } ?>
                    <?= form_dropdown('dre', $cbDRE, $category->dre, 'class="form-control tip" id="dre"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('receita_pai', 'receita'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbReceitasSuperiores[''] = lang('selecione_despesa_pai');
                    foreach ($receitasSuperior as $receitaSuperior) {
                        $cbReceitasSuperiores[$receitaSuperior->id] = $receitaSuperior->name;
                    } ?>
                    <?= form_dropdown('receitasuperior', $cbReceitasSuperiores, $category->receitasuperior , 'class="form-control tip" id="receitasuperior"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('category_code', 'code'); ?>
                <?= form_input('code', $category->code, 'class="form-control" id="code" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('category_name', 'name'); ?>
                <?= form_input('name', $category->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <?php echo form_hidden('id', $category->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarReceita', lang('editar_receita'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>