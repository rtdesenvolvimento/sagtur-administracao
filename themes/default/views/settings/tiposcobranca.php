<style type="text/css" media="screen">
    #TiposCobrancaTable td:nth-child(1) {display: none;}
    #TiposCobrancaTable td:nth-child(2) {}
    #TiposCobrancaTable td:nth-child(3) {}
    #TiposCobrancaTable td:nth-child(4) {width: 10%;text-align: center;}
</style>

<script>
    $(document).ready(function () {
        $('#TiposCobrancaTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('system_settings/getTiposCobranca') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, null, null, null, {"bSortable": false}]
        });
    });
</script>
<?= form_open('system_settings/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('tiposcobranca'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('system_settings/adicionarTipoCobranca'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('adicionar_tipo_cobranca'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/mercadopago') ?>" class="toggle_up">
                        <i class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_mercadopago'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/pagseguro') ?>" class="toggle_up">
                        <i class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configurar_pagseguro'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/juno') ?>" class="toggle_up">
                        <i class="icon fa fa-usd"></i><span class="padding-right-10"><?= lang('configurar_juno'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/taxas') ?>" class="toggle_up">
                        <i class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configar_taxas'); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?= lang('informaceos_tipo_cobranca'); ?></p>
                <div class="table-responsive">
                    <table id="TiposCobrancaTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("nome_tipo_cobranca"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("tipo_cobranca_conta"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {});
</script>

