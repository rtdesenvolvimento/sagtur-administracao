<style>
    .del-img-gallery { position: absolute; top: 0; right: 9px; }
</style>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_gallery'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("site_settings/editGallery/" . $gallery->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $gallery->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group all">
                <?= lang("product_gallery_images", "images") ?>
                <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                       data-show-preview="false" class="form-control file" accept="image/*">
            </div>

            <div id="multiimages" class="padding10">
                <?php if (!empty($images)) {
                    foreach ($images as $ph) {
                        echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                        echo '<a href="#" class="del-img-gallery" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                        echo '</div>';
                    }
                }
                ?>
                <div class="clearfix"></div>
            </div>
            <?php echo form_hidden('id', $gallery->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editar_team', lang('editar_team'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $(document).on('click', '.del-img-gallery', function(e) {
            e.preventDefault();
            var ele = $(this), id = $(this).attr('data-item-id');
            bootbox.confirm(lang.r_u_sure, function(result) {
                if(result == true) {
                    $.get(site.base_url+'site_settings/delete_image_gallery/'+id, function(data) {
                        if (data.error === 0) {
                            addAlert(data.msg, 'success');
                            ele.parent('.gallery-image').remove();
                        }
                    });
                }
            });
            return false;
        });
    });

</script>
