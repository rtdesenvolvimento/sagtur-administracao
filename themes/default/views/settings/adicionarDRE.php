<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_dre'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("system_settings/adicionarDRE", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'Ativo' => lang('ativo'),
                    'Inativo' => lang('inativo')
                );
                echo form_dropdown('status', $opts, (isset($_POST['status']) ? $_POST['status'] :  '') , 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("ordem_exibir_dre", "ordem"); ?>
                <?php echo form_input('ordem', '', 'class="form-control input-tip mask_integer" id="ordem"'); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("dre_tipo", "tipo") ?>
                <?php
                $opts = array(
                    '(+) Receitas' => lang('dre_receitas'),
                    '(-) Despesas' => lang('dre_despesas'),
                    '(+/-) Ambas' => lang('ambas'),
                    '(=) Totalizador' => lang('totalizador'),
                );
                echo form_dropdown('tipo', $opts, (isset($_POST['tipo']) ? $_POST['tipo'] :  '') , 'class="form-control" id="tipo" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('dre_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarDRE', lang('adicionar_dre'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>