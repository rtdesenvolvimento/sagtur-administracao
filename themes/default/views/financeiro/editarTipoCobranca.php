<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_tipo_cobranca'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiroutil/editarTipoCobranca/" . $category->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <input type="hidden" name="tipoExibir" value="receita"/>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    'Ativo' => lang('ativo'),
                    'Inativo' => lang('inativo')
                );
                echo form_dropdown('status', $opts, $category->status , 'class="form-control" id="status" required="required"');
                ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("tipoExibir", "tipoExibir") ?>
                <?php
                $opts = array(
                    'receita' => lang('receita'),
                    'despesa' => lang('despesa')
                );
                echo form_dropdown('tipoExibir', $opts, $category->tipoExibir, 'class="form-control" id="tipoExibir"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('nome_tipo_cobranca', 'name'); ?>
                <?= form_input('name', $category->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_status" style="display: none;">
                <?= lang("faturar_automatico", "faturar_automatico") ?>
                <?php
                $opts = array(
                    'sim' => lang('yes'),
                    'nao' => lang('no')
                );
                echo form_dropdown('faturar_automatico', $opts,  $category->faturar_automatico , 'class="form-control" id="faturar_automatico" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('conta_destino', 'conta'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbContas[''] = lang('select').' '.lang('conta_destino');
                    foreach ($movimentadores as $movimentador) {
                        $cbContas[$movimentador->id] = $movimentador->name;
                    } ?>
                    <?= form_dropdown('conta', $cbContas,  $category->conta , 'class="form-control" required="required" id="conta"'); ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang('forma_pagamento', 'formapagamento'); ?>
                <div class="input-group">
                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                        <i class="fa fa-money"  style="font-size: 1.2em;"></i>
                    </div>
                    <?php
                    $cbFormaPagamento[''] = lang('select').' '.lang('forma_pagamento');
                    foreach ($formaspagamento as $formapagamento) {
                        $cbFormaPagamento[$formapagamento->id] = $formapagamento->name;
                    } ?>
                    <?= form_dropdown('formapagamento', $cbFormaPagamento, $category->formapagamento, 'class="form-control" required="required" id="formapagamento"'); ?>
                </div>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("faturarVenda", "faturarVenda") ?>
                <?php
                $opts = array(
                    '1' => lang('faturar_venda'),
                    '0' => lang('orcamento_venda')
                );
                echo form_dropdown('faturarVenda', $opts, $category->faturarVenda, 'class="form-control" id="faturarVenda" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("automatic_cancellation_sale", "automatic_cancellation_sale") ?>
                <?php
                $opts = array(
                    '1' => lang('yes'),
                    '0' => lang('no')
                );
                echo form_dropdown('automatic_cancellation_sale', $opts, $category->automatic_cancellation_sale , 'class="form-control" id="automatic_cancellation_sale" required="required"');
                ?>
            </div>
            <div class="form-group" id="div_numero_dias_cancelamento" <?php if($category->automatic_cancellation_sale == '0') echo 'style="display: none;"';?> >
                <?= lang("numero_dias_cancelamento", "numero_dias_cancelamento") ?>
                <?= form_input('numero_dias_cancelamento', $category->numero_dias_cancelamento, 'class="form-control tip mask_integer" id="numero_dias_cancelamento" '); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("integracao", "integracao") ?>
                <?php
                $opts = array(
                    'nenhuma' => lang('nao_integrado_pagamento'),
                    'juno' => lang('juno'),
                    'pagseguro' => lang('pagseguro'),
                    'mercadopago' => lang('mercadopago'),
                    'cobrefacil' => lang('cobrefacil'),
                    'asaas' => lang('asaas'),
                    'valepay' => lang('valepay'),
                );
                echo form_dropdown('integracao', $opts, $category->integracao, 'class="form-control" id="integracao" required="required"');
                ?>
            </div>
            <div class="form-group" id="div_tipo_emissao_integracao" <?php if ($category->integracao == "nenhuma") echo  'style="display: none;'?>">
                <?= lang("tipo_integracao_geracao", "tipo") ?>
                <?php
                $opts = array(
                    'nenhuma' => lang('select'),
                    'boleto' => lang('boleto'),
                    'carne' => lang('carne'),
                    'carne_cartao' => lang('cartao'),
                    'carne_cartao_transparent' => lang('carne_cartao_transparent'),
                    'carne_cartao_transparent_mercado_pago' => lang('carne_cartao_transparent_mercado_pago'),
                    'link_pagamento' => lang('link_pagamento'),
                    'pix' => lang('pix'),
                    'loterioca' => lang('loterioca'),
                    'boleto_pix' => lang('boleto_pix'),
                    'cartao_credito_link' => lang('cartao_credito_link'),
                    'mensalidade' => lang('mensalidade'),
                );
                echo form_dropdown('tipo', $opts, $category->tipo, 'class="form-control" id="tipo" required="required"');
                ?>
            </div>
            <div class="form-group all">
                <div class="controls">
                    <?php echo form_checkbox('configurar_pix', 1 , $category->configurar_pix, 'id="configurar_pix" class="form-control"'); ?>
                    <?= lang("configurar_pix", "configurar_pix") ?>
                </div>
            </div>
            <div class="form-group" id="div_pix"  style="<?php if (!$category->configurar_pix) echo 'display: none';?>">
                <div class="panel panel-warning" style="padding: 15px;">
                    <div class="panel-heading"><h4><?= lang('pix') ?></h4></div>
                    <div class="form-group">
                        <?= lang('chave_pix', 'chave_pix'); ?>
                        <?= form_input('chave_pix', $category->chave_pix, 'class="form-control" id="chave_pix"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang('titular', 'titular'); ?>
                        <?= form_input('titular', $category->titular, 'class="form-control" id="titular"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang('telefone', 'telefone'); ?>
                        <?= form_input('telefone', $category->telefone, 'class="form-control" id="telefone"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang('email', 'email'); ?>
                        <?= form_input('email', $category->email, 'class="form-control" id="email"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', $category->note, 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
            <?php echo form_hidden('id', $category->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarTipoCobranca', lang('editar_tipo_cobranca'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        $('#configurar_pix').on('ifChecked', function (e) {
            $('#div_pix').show();
        });

        $('#configurar_pix').on('ifUnchecked', function (e) {
            $('#div_pix').hide();
        });

        $('#automatic_cancellation_sale').change(function (event){
            if ($(this).val() === '1') {
                $('#div_numero_dias_cancelamento').show();
            } else {
                $('#div_numero_dias_cancelamento').hide();
            }
        });

        $('#integracao').select2().on("change", function(e) {

            const tipoIntegracao =  $("#integracao  option:selected").val();

            if (tipoIntegracao !== 'nenhuma') {
                $('#div_tipo_emissao_integracao').show();
            } else {
                $('#div_tipo_emissao_integracao').hide();
            }

            if (tipoIntegracao === 'juno') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne', text: '<?php echo lang('carne');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao', text: '<?php echo lang('link_pagamento');?>'}));
            }

            if (tipoIntegracao === 'pagseguro') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao_transparent', text: '<?php echo lang('carne_cartao_transparent');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao', text: '<?php echo lang('link_pagamento');?>'}));
            }

            if (tipoIntegracao === 'mercadopago') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'carne_cartao_transparent_mercado_pago', text: '<?php echo lang('carne_cartao_transparent_mercado_pago');?>'}));
                $('#tipo').append($('<option>', {value: 'link_pagamento', text: '<?php echo lang('link_pagamento');?>'}));
                $('#tipo').append($('<option>', {value: 'pix', text: '<?php echo lang('pix');?>'}));
            }

            if (tipoIntegracao === 'cobrefacil') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto_pix', text: '<?php echo lang('boleto_pix');?>'}));
                $('#tipo').append($('<option>', {value: 'link_pagamento', text: '<?php echo lang('link_pagamento');?>'}));
                $('#tipo').append($('<option>', {value: 'pix', text: '<?php echo lang('pix');?>'}));
                $('#tipo').append($('<option>', {value: 'carne', text: '<?php echo lang('carne');?>'}));
                $('#tipo').append($('<option>', {value: 'cartao_credito_link', text: '<?php echo lang('cartao_credito_link');?>'}));
                $('#tipo').append($('<option>', {value: 'mensalidade', text: '<?php echo lang('mensalidade');?>'}));
            }

            if (tipoIntegracao === 'asaas') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'boleto', text: '<?php echo lang('boleto');?>'}));
                $('#tipo').append($('<option>', {value: 'pix', text: '<?php echo lang('pix');?>'}));
                $('#tipo').append($('<option>', {value: 'carne', text: '<?php echo lang('carne');?>'}));
                $('#tipo').append($('<option>', {value: 'link_pagamento', text: '<?php echo lang('link_pagamento');?>'}));
            }

            if (tipoIntegracao === 'valepay') {
                $('#tipo').empty();
                $('#tipo').append($('<option>', {value: 'nenhuma', text: '<?php echo lang('select');?>'}));
                $('#tipo').append($('<option>', {value: 'link_pagamento', text: '<?php echo lang('link_pagamento');?>'}));
                $('#tipo').append($('<option>', {value: 'cartao_credito_transparent_valepay', text: '<?php echo lang('cartao_credito_transparent_valepay');?>'}));
                $('#tipo').append($('<option>', {value: 'pix', text: '<?php echo lang('pix');?>'}));
            }
        });

        $('#integracao').change();
        $('#tipo').select2().val('<?php echo $category->tipo;?>').change();
    });
</script>