<style type="text/css" media="screen">
    #SLData td:nth-child(1) {width: 1%;text-align: center;}
    #SLData td:nth-child(2) {text-align: center;width: 2%;}
    #SLData td:nth-child(3) {text-align: left;width: 150%;}
    #SLData td:nth-child(4) {text-align: left;display: none;}
    #SLData td:nth-child(5) {text-align: left;display: none;}
    #SLData td:nth-child(6) {text-align: left;display: none;}
    #SLData td:nth-child(7) {text-align: right;width: 5%;}
    #SLData td:nth-child(8) {text-align: right;width: 5%;}
    #SLData td:nth-child(9) {text-align: right;width: 5%;}
    #SLData td:nth-child(10) {text-align: right;width: 5%;}
    #SLData td:nth-child(11) {text-align: right;width: 5%;}
    #SLData td:nth-child(12) {text-align: center;display: none;}
    #SLData td:nth-child(13) {text-align: center;display: none;}
    #SLData td:nth-child(14) {text-align: center;display: none;}
    #SLData td:nth-child(15) {text-align: center;display: none;}
    #SLData td:nth-child(16) {text-align: center;display: none;}
    #SLData td:nth-child(17) {text-align: center;display: none;}
    #SLData td:nth-child(18) {text-align: center;display: none;}
    #SLData td:nth-child(19) {text-align: center;display: none;}
    #SLData td:nth-child(20) {text-align: center;}

</style>

<script>

    var struture = {
        'id' : 0,
        'dtVencimento' : 1,
        'name': 2,
        'product_name': 3,
        'receita' : 4,
        'tipoCobranca' : 5,
        'valorfatura' : 6,
        'taxas' : 7,
        'valorpago' : 8,
        'valorpagar' : 9,
        'status' : 10,
        'parcelas' : 11,
        'dtultimopagamento' : 12,
        'quantidade_dias' : 13,
        'sale_id' : 14,
        'link,' : 15,
        'tipo' : 16,
        'checkoutUrl' : 17,
        'code' : 18,
    };

    function attachDescriptionToName(x, alignment, aaData) {

        x = '<span style="font-weight: bold;font-size: 12px;line-height: 1.71;color: #428bca">'+x+'</span>';

        if ( aaData[struture.tipo] !== null ) {

            //if ( aaData[struture.tipo] === 'boleto') x = '<a href="'+aaData[15]+'" target="_blank" title="Ver Boleto"> '+x+'</a>';
            //else  x = '<a href="'+aaData[17]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[struture.tipo] === 'boleto')  {
            x += '<br/><small><i class="fa fa-barcode"></i> '+aaData[struture.tipoCobranca]+'</small>';
        } else {
            x += '<br/><small><i class="fa fa-money"></i> '+aaData[struture.tipoCobranca]+'</small>';
        }

        return x;
    }

    function currencyFormatValuePay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'">'+formatMoney(x)+'<br/><small>'+aaData[struture.parcelas]+'</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }

    }

    function currencyFormatTaxas(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            if (x > 0) return '<div class="'+alignment+'" style="color: red;font-weight: bold;">'+formatMoney(x)+'<br/>&nbsp;</small></div>';
            else return '<div class="'+alignment+'">'+formatMoney(x)+'<br/>&nbsp;</small></div>';
        } else {
            return '<div class="'+alignment+'">'+formatMoney(0)+'<br/>&nbsp;</small></div>';
        }
    }

    function currencyFormatValuePaid(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let dtUltimoPagamento = aaData[struture.dtultimopagamento] != null ? aaData[struture.dtultimopagamento] : '&nbsp;';

        if (x != null) {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + dtUltimoPagamento + '</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValueToPay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let diferencaEmDias = aaData[struture.quantidade_dias] != null ? aaData[struture.quantidade_dias] : '&nbsp;';
        let status = aaData[struture.status];

        if (diferencaEmDias < 0 && status !== 'QUITADA') {
            if (diferencaEmDias === -1) return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + diferencaEmDias + ' dia</small></div>';
            else return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + diferencaEmDias + ' dias</small></div>';
        } else {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>&nbsp;</small></div>';
        }
    }

    function row_status_financeiro(x,  alignment, aaData) {

        let diasAtraso = aaData[struture.quantidade_dias];

        if(x == null) {
            return '';
        } else if(x == 'pending') {
            return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
        } else if(x == 'completed' || x == 'paid' || x == 'sent' || x == 'received' || x == 'QUITADA') {
            return '<div class="text-center"><span class="label label-success">'+lang[x]+'</span></div>';
        } else if(x == 'partial' || x == 'transferring' || x == 'ordered' || x == 'PARCIAL') {
            return '<div class="text-center"><span class="label label-info">'+lang[x]+'</span></div>';
        } else if(x == 'due' || x == 'returned' || x == 'ABERTA') {
            if (diasAtraso < 0) {
                return '<div class="text-center"><span class="label label-danger">Vencida</span></div>';
            } else {
                return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
            }
        } else if (x == 'cancel' || x == 'CANCELADA') {
            return '<div class="text-center"><span class="label label-default">'+lang[x]+'</span></div>';
        } else if (x== 'reembolso'){
            return '<div class="text-center"><span class="label label-default">'+lang[x]+'</span></div>';
        } else if (x == 'orcamento') {
            return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
        } else if (x == 'lista_espera') {
            return '<div class="text-center"><span class="label label-info">'+lang[x]+'</span></div>';
        } else if (x == 'faturada'){
            return '<div class="text-center"><span class="label label-success">'+lang[x]+'</span></div>';
        }
    }

    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterTipoCobranca').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterReceita').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').keydown(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#programacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('financeiro/getReceitas')?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filter_numero_documento", "value":  $('#filter_numero_documento').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterReceita", "value":  $('#filterReceita').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                let vendaId = aData[struture.sale_id];

                if (vendaId !== null) {
                    nRow.id = aData[struture.sale_id];
                    nRow.className = "invoice_link";
                }

                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormatValuePay},
                {"mRender": currencyFormatTaxas},
                {"mRender": currencyFormatValuePaid},
                {"mRender": currencyFormatValueToPay},
                {"mRender": row_status_financeiro},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                const totalizadorCredito = aaData
                    .reduce( (totalizadores, faturaAtual) => {
                        return createTotalizador(totalizadores, faturaAtual);
                    }, {total:0, pago: 0, taxas: 0, pagar: 0} );

                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(totalizadorCredito.total));
                nCells[6].innerHTML = currencyFormat(parseFloat(totalizadorCredito.taxas));
                nCells[7].innerHTML = currencyFormat(parseFloat(totalizadorCredito.pago));
                nCells[8].innerHTML = currencyFormat(parseFloat(totalizadorCredito.pagar));
            }
        }) ;
    });
    
    function createTotalizador(totalizadores, faturaAtual) {

        let taxa = 0;

        if (!isNaN(parseFloat(faturaAtual[struture.taxas]))) {
             taxa = parseFloat(faturaAtual[struture.taxas]);
        }

        if (isNaN(totalizadores.taxas)) {
            totalizadores.taxas = 0;
        }

        totalizadores.total = totalizadores.total + parseFloat(faturaAtual[struture.valorfatura]);
        totalizadores.pago = totalizadores.pago + parseFloat(faturaAtual[struture.valorpago]);
        totalizadores.pagar = totalizadores.pagar + parseFloat(faturaAtual[struture.valorpagar]);
        totalizadores.taxas = totalizadores.taxas + taxa;
        
        return totalizadores;
    }

</script>
<?php if ($Owner || $GP['bulk_actions']) {
    echo form_open('financeiro/financeiro_actions', 'id="action-form"');
} ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa fa-sign-in"></i><?=lang('contas_receber');?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('financeiro/adicionarContaReceber')?>" data-toggle="modal" id="novaContaReceber" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-arrow-circle-o-up"></i> <?=lang('adicionar_conta_receber')?>
                            </a>
                        </li>
                        <li style="display: none;">
                            <a href="#" class="bpo" title="<?= $this->lang->line("gerar_conta_receber_lote") ?>"
                               data-content="<?= lang('r_u_sure') ?><button type='button' class='btn btn-danger' id='delete' data-action='gerar_conta_receber_lote'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left">
                                <i class="fa fa-money"></i> <?= lang('gerar_conta_receber_lote') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" class="bpo" title="<?= $this->lang->line("Cancelar Parcelas Selecionadas") ?>"
                               data-content="<?= lang('r_u_sure') ?><button type='button' class='btn btn-danger' id='delete' data-action='cancelar_fatura_em_lote'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left">
                                <i class="fa fa-trash-o"></i> <?= lang('Cancelar Parcelas Selecionadas') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;"  class="divfilters">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_de", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_ate", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="filterStatus"><?=lang('payment_status');?></label>
                                    <?php
                                    $cbSituacao[""] = lang('select') ;
                                    $cbSituacao["ABERTA"] = lang('ABERTA') ;
                                    $cbSituacao["QUITADA"] = lang('QUITADA') ;
                                    $cbSituacao["VENCIDA"] = lang('VENCIDA') ;
                                    echo form_dropdown('filterStatus', $cbSituacao, (isset($_POST['filterStatus']) ? $_POST['filterStatus'] : $filterStatus), 'class="form-control" id="filterStatus" data-placeholder="' . $this->lang->line("select")  . '"');
                                    ?>

                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                    <?php
                                    $tcs[""] = lang('select') ;
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : $filterTipoCobranca), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select")  . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2" style="display: none;">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_receita"); ?></label>
                                    <select class="form-control tip" name="filterReceita" id="filterReceita">
                                        <option value=""><?php echo lang('select');?></option>
                                        <?php
                                        foreach ($planoReceitas as $objReceita) {?>
                                            <optgroup label="<?php echo $objReceita->name;?>">
                                                <?php
                                                $receitasFilhas = $this->financeiro_model->getReceitaByReceitaSuperiorId($objReceita->id);
                                                foreach ($receitasFilhas as $item) {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display: none;">
                                <div class="form-group">
                                    <?= lang("numero_documento", "filter_numero_documento") ?>
                                    <?= form_input('filter_numero_documento', (isset($_POST['filter_numero_documento']) ? $_POST['filter_numero_documento'] : ''), 'class="form-control" id="filter_numero_documento"'); ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;font-weight: bold;color: #79796A;">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("Vencimeto"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("Cobrança"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                            <th><?php echo $this->lang->line("total"); ?></th>
                            <th><?php echo $this->lang->line("taxas"); ?></th>
                            <th><?php echo $this->lang->line("pago"); ?></th>
                            <th><?php echo $this->lang->line("pagar"); ?></th>
                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                            <th style="display: none;">Parc.</th>
                            <th style="display: none;">Ult.</th>
                            <th style="display: none;">Atras.</th>
                            <th style="display: none;">Venda.</th>
                            <th style="display: none;">Link.</th>
                            <th style="display: none;">Tipo.</th>
                            <th style="display: none;">Cartao.</th>
                            <th style="display: none;">Cod.</th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>
<script type="text/javascript" src="<?=$assets?>pos/js/plugins.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        shortcut.add('F4', function () {
            $('#novaContaReceber').click();
        }, {'type': 'keydown', 'propagate': false, 'target': document});

    });
</script>