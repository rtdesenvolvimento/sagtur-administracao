<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_conta_pagar'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/adicionarContaPagar", $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-2">
                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="form-group">
                                <?= lang("warehouse", "slwarehouse"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-building"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh,  $Settings->default_warehouse, 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $warehouse_input = array(
                                'type' => 'hidden',
                                'name' => 'warehouse',
                                'id' => 'slwarehouse',
                                'value' => $this->session->userdata('warehouse_id'),
                            );
                            echo form_input($warehouse_input);
                            ?>
                        <?php } ?>
                    </div>
                    <div class="col-md-10">
                        <div class="form-group">
                            <?= lang("cliente_conta", "pessoa"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-user"  style="font-size: 1.2em;"></i>
                                </div>
                                <?php
                                echo form_input('pessoa', (isset($_POST['pessoa']) ? $_POST['pessoa'] : ""), 'id="pessoa" data-placeholder="' . lang("select") . ' ' . lang("cliente_conta") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang('products', 'products'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-map"  style="font-size: 1.2em;"></i>
                                </div>
                                <?php
                                $cbProgramacao[''] = 'Selecione uma opção';
                                foreach ($programacoes as $programacao) {
                                    $label =  $this->sma->hrsd($programacao->dataSaida). ' - '.$programacao->name;
                                    $cbProgramacao[$programacao->programacaoId] = $label;
                                }
                                echo form_dropdown('programacaoId', $cbProgramacao, (isset($_POST['programacaoId']) ? $_POST['programacaoId'] : ""), 'id="programacaoId" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("products") . '" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('despesa', 'despesa'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                                </div>
                                <select class="form-control tip" name="despesa" id="despesa" required="required">
                                    <option value=""><?php echo lang('select').' '.lang('despesa');?></option>
                                    <?php
                                    foreach ($despesas as $despesa) {?>
                                        <optgroup label="<?php echo $despesa->name;?>">
                                            <?php
                                            $despesasFilhas = $this->financeiro_model->getReceitaByDespesaSuperiorId($despesa->id);
                                            foreach ($despesasFilhas as $item) {?>
                                                <?php if ($this->Settings->receitaPadrao == $item->id ) {?>
                                                    <option  selected="selected" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php } else {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php } ?>
                                            <?php }?>
                                        </optgroup>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("data_vencimento", "dtvencimento"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                </div>
                                <input type="date" name="dtvencimento" value="<?php echo date('Y-m-d');?>" class="form-control tip" id="dtvencimento" required="required" data-original-title="Data do primeiro vencimento" title="Data do primeiro vencimento">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('tipo_cobranca_financeiro', 'tipo_cobranca'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-barcode"  style="font-size: 1.2em;"></i>
                                </div>
                                <?php
                                $cbTipoDeCobranca[''] = '';
                                foreach ($tiposCobranca as $tipoCobranca) {
                                    $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                                }
                                echo form_dropdown('tipocobranca', $cbTipoDeCobranca, $this->Settings->tipoCobrancaBoleto, 'id="tipocobranca" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca_financeiro") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("valor_vencimento", "valor"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                </div>
                                <input type="text" name="valor" id="valor" value="" style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('condicao_pagamento', 'condicao_pagamento'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <i class="fa fa-tags"  style="font-size: 1.2em;"></i>
                                </div>
                                <?php
                                $cbCondicoesPagamento[''] = lang('select').' '.lang('condicao_pagamento');
                                foreach ($condicoesPagamento as $condicaoPagamento) {
                                    $cbCondicoesPagamento[$condicaoPagamento->id] = $condicaoPagamento->name;
                                } ?>
                                <?= form_dropdown('condicaopagamento', $cbCondicoesPagamento, $this->Settings->condicaoPagamentoAVista, 'class="form-control tip" id="condicaopagamento" required="required"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group all">
                            <?= lang("numero_documento", "numero_documento") ?>
                            <?= form_input('numero_documento', (isset($_POST['numero_documento']) ? $_POST['numero_documento'] : ''), 'class="form-control" id="numero_documento"'); ?>
                        </div>
                    </div>
                    <div class="col-md-2" style="display: none;">
                        <div class="form-group">
                            <?= lang("pagar_agora", "recebido"); ?>
                            <?php
                            $opts = array(
                                'no' => lang('no'),
                                'sim' => lang('yes'),
                            );
                            echo form_dropdown('recebido', $opts, (isset($_POST['recebido']) ? $_POST['recebido'] :  '') , ' class="form-control" id="recebido" required="required"');
                            ?>
                        </div>
                    </div>
                    <div id="div-pagar-agora" style="display: none;">
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("valor_entrada", "valorentrada"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input name="valorentrada" type="text" id="valorentrada" required="required" value="" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("data_pagamento_entrada", "dtParamentoEntrada"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="date" name="dtParamentoEntrada" value="<?php echo date('Y-m-d');?>" required="required" class="form-control tip" id="dtParamentoEntrada" data-original-title="Data do pagamento da entrada" title="Data do pagamento da entrada">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang('conta_destino', 'conta_destino'); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <?php
                                    $cbContas[''] = lang('select').' '.lang('conta_destino');
                                    foreach ($movimentadores as $movimentador) {
                                        $cbContas[$movimentador->id] = $movimentador->name;
                                    } ?>
                                    <?= form_dropdown('contadestino', $cbContas, set_value('contadestino'), 'class="form-control" id="contadestino"'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><h4><?= lang('parcelas') ?></h4></div>
                            <div class="panel-body" style="padding: 5px;">
                                 <div class="controls table-controls">
                                    <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th style="text-align: left;">#</th>
                                            <th class="col-md-3" style="text-align: left;">Valor</th>
                                            <th class="col-md-1" style="text-align: left;">Desc.</th>
                                            <th class="col-md-2" style="text-align: left;">Vencimento</th>
                                            <th class="col-md-3" style="text-align: left;">Cobrança</th>
                                            <th class="col-md-3" style="text-align: left;">Conta</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("attachment", "attachment") ?>
                            <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                                   class="form-control file">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("note", "note"); ?>
                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" required="required" id="note"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('adicionarContaPagar', lang('adicionar_conta_pagar'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">

    var mask = {
        money: function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"0.0$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"0.$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'.$1');
                }
                return v;
            };

            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(document).ready(function () {

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;

        $('#recebido').select2().on("change", function(e) {
            let contapadao =  $("#recebido  option:selected").val();
            if (contapadao === 'sim') {
                $('#div-pagar-agora').show();
                $('#valorentrada').attr('required','required');
            } else {
                $('#div-pagar-agora').hide();
                $('#valorentrada').removeAttr('required');
            }
        });

        $("#tipocobranca").select2().on("change", function(e) {
            let contapadao =  $("#tipocobranca  option:selected").attr('conta');
            if (contapadao !== '') {
                $('#contadestino').val(contapadao).change();
                $('#contadestino').attr('disabled', true);
            } else {
                $('#contadestino').val('').change();
                $('#contadestino').attr('disabled', false);
            }
            getParcelamento();
        });

        $('#valor').blur(function (event) {
            $('#valorentrada').val($(this).val());
            getParcelamento();
        });

        $('#dtvencimento').blur(function (event) {
            getParcelamento();
        });

        $("#condicaopagamento").select2().on("change", function(e) {
            getParcelamento();
        });

        $('#valorentrada').blur(function (event) {
            let valor = $('#valor').val();
            let valorEntrada = $(this).val();

            if (valor !== '') valor = parseFloat(valor);
            else valor = 0;

            if (valorEntrada !== '') valorEntrada = parseFloat(valorEntrada);
            else valorEntrada = 0;

            if (valorEntrada > valor) {
                alert('Valor da entrada não pode ser maior que o valor do vencimento.');
                $(this).val(valor);
            }
        })

        $('#pessoa').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestionsAll",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });

    function getParcelamento() {

        var condicaoPagamento = $('#condicaopagamento').val();
        var tipoCobranca = $("#tipocobranca  option:selected").val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: site.base_url + "financeiro/getParcelas",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    dtvencimento: $('#dtvencimento').val(),
                    valorVencimento: $('#valor').val(),
                    tipoCobrancaId: tipoCobranca,
                },
                dataType: 'html',
                success: function (html) {
                    $('#slTableParcelas tbody').html(html);

                    $(function(){
                        $('.mask_money').bind('keypress',mask.money);
                        $('.mask_money').click(function(){$(this).select();});
                    });
                }
            });
        } else {
            $('#slTableParcelas tbody').html('');
        }
    }

    function atualizarValorVencimentoAoEditarParcela(tag, index) {
        let totalVencimento = 0;

        $("input[name='valorVencimentoParcela[]']").each(function(){
            var valorVencimento = $(this).val();
            if (valorVencimento !== '')  totalVencimento = totalVencimento + parseFloat(valorVencimento);
        });

        $('#valor').val(totalVencimento.toFixed(2));
        $("#valorentrada").val(totalVencimento.toFixed(2));

    }
</script>
