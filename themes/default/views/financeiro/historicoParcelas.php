<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel">
                <?= lang("info_historico_fatura"); ?><br/>
                <i class="fa fa-user" style="font-size: 1.2em;"></i> <?php echo $fatura->nomeCliente;?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/negociarContaReceber/".$fatura->id, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="controls table-controls">
                        <table id="slTableParcelasRenegociacao" class="table items table-striped table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th style="text-align: center;"><?= lang('actions') ?></th>
                                <th style="text-align: left;" ><?= lang("categoria"); ?></th>
                                <th style="text-align: center;"><?= lang('vencimento') ?></th>
                                <th style="text-align: center;"><?= lang('tipo_cobranca') ?></th>
                                <th style="text-align: center;"><?= lang('valor_vencimento') ?></th>
                                <th style="text-align: center;"><?= lang('pago_financeiro') ?></th>
                                <th style="text-align: center;"><?= lang('multa') ?></th>
                                <th style="text-align: center;"><?= lang('juros') ?></th>
                                <th style="text-align: center;"><?= lang('valor_pagar') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $totalValorVencimento = 0;
                            $totalValorPago = 0;
                            $totalMulta = 0;
                            $totalJuros = 0;
                            $totalValorPagar = 0;

                            foreach ($parcelas as $parcela){

                                $fatura = $this->financeiro_model->getParcelaFaturaByParcela($parcela->id);

                                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fatura->id);

                                $isExibirOperacoesDaFatura = $this->financeiro_model->isExibirPagamentoConta($fatura->status);

                                $jurosMulta = 0;//$this->sma->getValorVencimentoComMultaJuros($parcela->valorpagar, $parcela->dtvencimento);TODO COMENTADO ATE MELHORIAS NO FINANCEIRO
                                $multa = 0;//$this->sma->getValorMulta($parcela->valorpagar, $parcela->dtvencimento);
                                $juros = 0;//$this->sma->getJurosMora($parcela->valorpagar, $parcela->dtvencimento);

                                $totalValorVencimento += $parcela->valor;
                                $totalValorPago += $parcela->valorpago;
                                $totalMulta += $multa;
                                $totalJuros += $juros;
                                $totalValorPagar += $jurosMulta;

                                ?>
                                <tr>
                                    <td class="acoes">
                                        <div class="text-center">
                                            <div class="btn-group text-left">
                                                <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu pull-right" role="menu">
                                                    <?php if ($parcela->status == Financeiro_model::STATUS_REPARCELADA) {?>
                                                        <li>
                                                            <a href="<?php echo base_url();?>financeiro/historico/<?php echo $parcela->contaOriginal;?>" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false">
                                                                <i class="fa fa-history" style="font-size: 1.2em;"></i> <?php echo lang('ver_fatura_gerada');?>
                                                            </a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li><a href="<?php echo base_url();?>financeiro/pagamentos/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Ver Pagamentos</a></li>
                                                        <?php if ($isExibirOperacoesDaFatura) {?>
                                                            <li><a href="<?php echo base_url();?>faturas/adicionarPagamento/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal4" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Adicionar Pagamento</a></li>
                                                        <?php }?>
                                                    <?php } ?>
                                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                                        <li class="divider"></li>
                                                        <li>
                                                            <a href="<?php echo base_url();?>faturas/editarFatura/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"><span class="fa fa-edit"></span> Editar Parcela</a>
                                                        </li>
                                                    <?php } ?>
                                                    <?php if (count($cobranca) > 0 && $isExibirOperacoesDaFatura) {?>
                                                        <li class="divider"></li>
                                                        <?php if ($cobranca->link || $cobranca->installmentLink){

                                                            $text = str_replace(' ', '%20', $fatura->note);
                                                            $text = str_replace('°', '%20', $text);
                                                            $text = str_replace('&', '%20', $text);

                                                            $mesagemWhatsAppBoleto = 'Olá tudo bem?';
                                                            $mesagemWhatsAppBoleto .= '%0A';
                                                            $mesagemWhatsAppBoleto .= '%0APassando para lembrar que o sua fatura: '.html_entity_decode($text, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
                                                            $mesagemWhatsAppBoleto .= '%0Avence dia:';
                                                            $mesagemWhatsAppBoleto .= '%0A*'.$this->sma->hrsd($fatura->dtvencimento).'*';
                                                            $mesagemWhatsAppBoleto .= '%0Ano valor de ';
                                                            $mesagemWhatsAppBoleto .= '%0A*'.$this->sma->formatMoney($fatura->valorpagar).'*';
                                                            $mesagemWhatsAppBoleto .= '%0A';
                                                            $mesagemWhatsAppBoleto .= '%0A👇🏻 Abaixo segue link para pagamento.';
                                                            $mesagemWhatsAppBoleto .= '%0A%0A'.($cobranca->link == null ? $cobranca->installmentLink : $cobranca->link);
                                                            $mesagemWhatsAppBoleto .= '%0A';
                                                            $mesagemWhatsAppBoleto .= '%0A✔️ Caso já tenha efetuado o pagamento desconsidere esta mensagem.';
                                                            $mesagemWhatsAppBoleto .= '%0A';
                                                            $mesagemWhatsAppBoleto .= '%0AAtenciosamente,';
                                                            $mesagemWhatsAppBoleto .= '%0A*'.$this->Settings->site_name.'*';
                                                            $mesagemWhatsAppBoleto .= '%0A*[Mensagem Automática]*';

                                                            if ($fatura->whatsapp) $cobrancaMensagem = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text='.$mesagemWhatsAppBoleto;
                                                            else  $cobrancaMensagem = 'https://api.whatsapp.com/send?text='.$mesagemWhatsAppBoleto;

                                                            ?>
                                                            <li><a href="<?php echo ($cobranca->link == null ? $cobranca->installmentLink : $cobranca->link)?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Imprimir Boleto</a></li>
                                                            <li><a href="<?php echo $cobrancaMensagem?>" target="<?php echo $fatura->id;?>"><i class="fa fa-whatsapp"></i> Enviar Cobrança pelo Whastapp</a></li>
                                                        <?php } ?>

                                                        <?php
                                                        if ($fatura->whatsapp) $wz = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text=*Segue%20o%20Link de pagamento*:%0A'.$cobranca->checkoutUrl;
                                                        else  $wz = 'https://api.whatsapp.com/send?text=*Segue%20o%20Link de pagamento*:%0A'.$cobranca->checkoutUrl;
                                                        ?>
                                                        <?php if ($cobranca->checkoutUrl) {

                                                            $text = str_replace(' ', '%20', $fatura->note);
                                                            $text = str_replace('°', '%20', $text);
                                                            $text = str_replace('&', '%20', $text);

                                                            $mesagemWhatsLinkPagamento = 'Olá tudo bem?';
                                                            $mesagemWhatsLinkPagamento .= '%0A';
                                                            $mesagemWhatsLinkPagamento .= '%0APassando para lembrar que o sua fatura: '.html_entity_decode($text, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
                                                            $mesagemWhatsLinkPagamento .= '%0Avence dia:';
                                                            $mesagemWhatsLinkPagamento .= '%0A*'.$this->sma->hrsd($fatura->dtvencimento).'*';
                                                            $mesagemWhatsLinkPagamento .= '%0Ano valor de';
                                                            $mesagemWhatsLinkPagamento .= '%0A*'.$this->sma->formatMoney($fatura->valorpagar).'*';
                                                            $mesagemWhatsLinkPagamento .= '%0A';
                                                            $mesagemWhatsLinkPagamento .= '%0A👇🏻 Abaixo segue link para pagamento.';
                                                            $mesagemWhatsLinkPagamento .= '%0A%0A'.$cobranca->checkoutUrl;
                                                            $mesagemWhatsLinkPagamento .= '%0A';
                                                            $mesagemWhatsLinkPagamento .= '%0A✔️ Caso já tenha efetuado o pagamento desconsidere esta mensagem.';
                                                            $mesagemWhatsLinkPagamento .= '%0A';
                                                            $mesagemWhatsLinkPagamento .= '%0AAtenciosamente,';
                                                            $mesagemWhatsLinkPagamento .= '%0A*'.$this->Settings->site_name.'*';
                                                            $mesagemWhatsLinkPagamento .= '%0A*[Mensagem Automática]*';

                                                            $wz = 'https://api.whatsapp.com/send?text=*Segue%20o%20Boleto*:%0A'.$cobranca->checkoutUrl;

                                                            if ($fatura->whatsapp) $cobrancaMensagem = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text='.$mesagemWhatsLinkPagamento;
                                                            else  $cobrancaMensagem = 'https://api.whatsapp.com/send?text='.$mesagemWhatsLinkPagamento;

                                                            ?>
                                                            <li><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-link"></i> Visualizar Link de Pagamento</a></li>
                                                            <li><a href="<?php echo $cobrancaMensagem?>" target="<?php echo $fatura->id;?>"><i class="fa fa-whatsapp"></i> Enviar Cobrança pelo Whastapp</a></li>
                                                        <?php } ?>
                                                    <?php } else {

                                                        $text = str_replace(' ', '%20', $fatura->note);
                                                        $text = str_replace('°', '%20', $text);
                                                        $text = str_replace('&', '%20', $text);

                                                        $mesagemWhatsAppCobranca = 'Olá tudo bem?';
														$mesagemWhatsAppCobranca .= '%0A';
														$mesagemWhatsAppCobranca .= '%0APassando para lembrar que o sua fatura: '.html_entity_decode($text, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8');
														$mesagemWhatsAppCobranca .= '%0Avence dia:';
														$mesagemWhatsAppCobranca .= '%0A*'.$this->sma->hrsd($fatura->dtvencimento).'*';
														$mesagemWhatsAppCobranca .= '%0Ano valor de';
														$mesagemWhatsAppCobranca .= '%0A*'.$this->sma->formatMoney($fatura->valorpagar).'*';
														$mesagemWhatsAppCobranca .= '%0A';
														$mesagemWhatsAppCobranca .= '%0A✔️ Caso já tenha efetuado o pagamento desconsidere esta mensagem.';
														$mesagemWhatsAppCobranca .= '%0A';
														$mesagemWhatsAppCobranca .= '%0AAtenciosamente,';
														$mesagemWhatsAppCobranca .= '%0A*'.$this->Settings->site_name.'*';
                                                        $mesagemWhatsAppCobranca .= '%0A*[Mensagem Automática]*';

                                                        if ($fatura->whatsapp) $cobrancaMensagem = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text='.$mesagemWhatsAppCobranca;
                                                        else  $cobrancaMensagem = 'https://api.whatsapp.com/send?text='.$mesagemWhatsAppCobranca; ?>
                                                        <?php if ($isExibirOperacoesDaFatura) {?>
                                                            <li><a href="<?php echo $cobrancaMensagem?>" target="<?php echo $fatura->id;?>"><i class="fa fa-whatsapp"></i> Enviar Cobrança pelo Whastapp</a></li>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    <?php if (count($cobranca) > 0 && $cobranca->integracao == 'pagseguro') {?>
                                                        <li><a href="<?php echo base_url();?>infrastructure/PagSeguroController/buscarDadosTransacaoByReference/<?php echo $fatura->id;?>" data-toggle="modal"
                                                               data-target="#myModal3" data-backdrop="static" data-keyboard="false">
                                                                <i class="fa fa-money"></i> Ver Dados da Fatura no PagSeguro</a>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </td>
                                    <td style="text-align: left;">
                                        <small><b style="font-size: 11px;"><i class="fa fa-newspaper-o"></i> <?php echo $fatura->reference?></b></small><br/>
                                        <?php echo $parcela->receita;?><br/>
                                        <?php if ($parcela->note) echo $parcela->note.'<br/>';?>
                                        <?php echo $this->sma->billing_status_design($parcela->status, $parcela->dtvencimento)?>
                                    </td>
                                    <td style="text-align: center;"><?php echo  $this->sma->hrsd($parcela->dtvencimento);?></td>
                                    <td style="text-align: center;">
                                        <?php echo $parcela->tipocobranca; ?>
                                        <?php if (count($cobranca) > 0 && $parcela->tipocobrancaTipo == 'boleto') {?>
                                            <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                        <?php } ?>
                                    </td>
                                    <td style="text-align: right;">
                                        <?php echo $this->sma->formatMoney($parcela->valor);?>
                                        <br/><small><?php echo '('.$parcela->numeroparcela.' / '.$parcela->totalParcelas.')'; ?></small>
                                    </td>
                                    <td style="text-align: right;"><?php echo $this->sma->formatMoney($parcela->valorpago);?><br/><small>&nbsp;</small></td>
                                    <td style="text-align: right;"><?php echo $this->sma->formatMoney($multa);?><br/><small>&nbsp;</small></td>
                                    <td style="text-align: right;"><?php echo $this->sma->formatMoney($juros);?><br/><small>&nbsp;</small></td>
                                    <td style="text-align: right;">
                                        <?php echo $this->sma->formatMoney($parcela->valorpagar);?>
                                        <?php if ($parcela->status == Financeiro_model::STATUS_ABERTA || $parcela->status == Financeiro_model::STATUS_FATURADA || $parcela->status == Financeiro_model::STATUS_PARCIAL) {?>
                                            <?php echo $this->sma->mostrarDiasVencimento($parcela->status, $parcela->dtvencimento);?>
                                        <?php } else {?>
                                            <br/><small>&nbsp;</small></td>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td class="right"><?php echo $this->sma->formatMoney($totalValorVencimento);?></td>
                                <td class="right"><?php echo $this->sma->formatMoney($totalValorPago);?></td>
                                <td class="right"><?php echo $this->sma->formatMoney($totalMulta);?></td>
                                <td class="right"><?php echo $this->sma->formatMoney($totalJuros);?></td>
                                <td class="right"><?php echo $this->sma->formatMoney($totalValorPagar);?></td>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>