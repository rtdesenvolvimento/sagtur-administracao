<style type="text/css" media="screen">
    #TiposCobrancaTable td:nth-child(1) {display: none;}

    #TiposCobrancaTable td:nth-child(2) {}
    #TiposCobrancaTable td:nth-child(3) {}
    #TiposCobrancaTable td:nth-child(4) {width: 15%;}

    #TiposCobrancaTable td:nth-child(5) {width: 5%;text-align: center;}
    #TiposCobrancaTable td:nth-child(6) {width: 5%;text-align: center;}
</style>

<script>
    $(document).ready(function () {

        function row_status(x) {

            if(x == null) {
                return '';
            } else if(x === 'Ativo') {
                return '<div class="text-center"><span class="label label-success">Ativo</span></div>';
            } else if(x === 'Inativo') {
                return '<div class="text-center"><span class="label label-danger">Inativo</span></div>';
            }
        }

        function row_type(x) {

            if(x == null) {
                return '';
            } else if(x === 'receita') {
                return '<div class="text-center"><span class="label label-success" style="background-color: #0088cc;">Receita</span></div>';
            } else if(x === 'despesa') {
                return '<div class="text-center"><span class="label label-danger">Despesa</span></div>';
            }
        }

        $('#type').change(function(event){
            $('#TiposCobrancaTable').DataTable().fnClearTable()
        });


        $('#TiposCobrancaTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('financeiroutil/getTiposCobranca') ?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filter_type", "value":  $('#type').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"bSortable": false, "mRender": checkbox}, null, null, null,  {"mRender": row_status}, {"mRender": row_type}, {"bSortable": false}]
        });
    });
</script>
<?= form_open('financeiro/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('tiposcobranca'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('financeiroutil/adicionarTipoCobranca'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('adicionar_tipo_cobranca'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/mercadopago') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_mercadopago'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/pagseguro') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_pagseguro'); ?></span>
                    </a>
                </li>
                <li class="dropdown" style="display: none;">
                    <a href="<?= site_url('system_settings/juno') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_juno'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/asaas') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_asaas'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('system_settings/valepay') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_valepay'); ?></span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?= site_url('financeiroutil/taxas') ?>" class="toggle_up">
                        <i class="icon fa fa-money"></i><span class="padding-right-10"><?= lang('configar_taxas'); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-2">
                <?= lang("type", "type") ?>
                <?php
                $opts = array(
                    '' => lang('select') ,
                    'receita' => lang('receita'),
                    'despesa' => lang('despesa'),
                );
                echo form_dropdown('type', $opts,  'receita', 'class="form-control" id="type"');
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="TiposCobrancaTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("nome_tipo_cobranca"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("tipo_cobranca_conta"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("forma_pagamento"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th><?= $this->lang->line("type"); ?></th>
                            <th style="width:50px;text-align: center;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("nome_tipo_cobranca"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("tipo_cobranca_conta"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("forma_pagamento"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("type"); ?></th>
                            <th style="width:50px;text-align: center;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {});
</script>

