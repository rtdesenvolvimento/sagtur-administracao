<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo $this->lang->line('view_payments'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table id="CompTable" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-hover table-striped">
                    <thead>
                    <tr>
                        <th><?= $this->lang->line("actions"); ?></th>
                        <th><?= $this->lang->line("date"); ?></th>
                        <th><?= $this->lang->line("reference_no"); ?></th>
                        <th><?= $this->lang->line("tipo_cobranca"); ?></th>
                        <th><?= $this->lang->line("movimentador"); ?></th>
                        <th><?= $this->lang->line("amount"); ?></th>
                        <th><?= $this->lang->line("paid_by"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($payments)) {
                        foreach ($payments as $payment) { ?>
                            <tr class="row<?= $payment->id ?>" style="<?php if ($payment->status == Pagamento_model::STATUS_ESTORNO) echo 'text-decoration: line-through;'; ?>">
                                <td class="acoes">
                                    <div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right" role="menu">

                                                <?php if ($payment->status != Pagamento_model::STATUS_ESTORNO){?>
                                                    <li><a href="<?= site_url('sales/payment_note/' . $payment->id) ?>" target="<?php echo $payment->id;?>"><i class="fa fa-file-text-o"></i> Emitir Recibo de pagamento</a></li>
                                                <?php }?>

                                                <?php if ($payment->attachment) {?>
                                                    <li><a href="<?= base_url('assets/uploads/' . $payment->attachment); ?>" target="_blank"><i class="fa fa-chain"></i> Ver anexo de pagamento</a></li>
                                                <?php } ?>
                                                <?php if ($payment->paid_by != 'gift_card') { ?>
                                                    <li><a href="<?= site_url('financeiro/editarPagamento/' . $payment->id) ?>"
                                                           data-toggle="modal" data-target="#myModal4"><i class="fa fa-comments"></i> Ver Observações de pagamento</a></li>

                                                    <?php if ($payment->status != Pagamento_model::STATUS_ESTORNO){?>
                                                        <li class="divider"></li>
                                                        <li><a href="<?= site_url('financeiro/motivoEstorno/' . $payment->id) ?>"
                                                               data-toggle="modal" data-target="#myModal4"><i class="fa fa-recycle"></i> Estornar Pagamento</a></li>
                                                    <?php }?>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                                <td class="center"><?= $this->sma->hrld($payment->date); ?><?php if ($payment->status == Pagamento_model::STATUS_ESTORNO) echo '<br/><small>'.$payment->motivo_estorno.'</small>'?></td>
                                <td class="center"><?= $payment->reference_no; ?></td>
                                <td class="center"><?= $payment->tipoCobranca; ?></td>
                                <td class="center"><?= $payment->conta; ?></td>
                                <td class="right"><?= $this->sma->formatMoney($payment->amount). ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_blank"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                <td class="center"><?= lang($payment->paid_by); ?></td>
                            </tr>
                        <?php }
                    } else {
                        echo "<tr><td colspan='7'>" . lang('no_data_available') . "</td></tr>";
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('click', '.po-delete', function () {
            var id = $(this).attr('id');
            $(this).closest('tr').remove();
        });
    });
</script>
