<style>
    #SLData td:nth-child(1) {
        display: none;
    }

    #SLData td:nth-child(2) {
       width: 90%;
    }
    #SLData td:nth-child(3) {
        text-align: center;
        width: 3%;
    }
</style>
<script>

    function verificar(x, alignment, aaData) {
        if (x === '1') {
            return 'SIM';
        } if (x === '2') {
            return 'BLOQUEADO';
        } else {//0
            return 'NÃO';
        }
    }

    $(document).ready(function () {

        var cTable = $('#SLData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('financeiro/getContasReceberLote') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                //nRow.className = "customer_details_link";
                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                {"mRender": verificar},
            ]
        });

        setInterval(function() {
            $('#SLData').DataTable().fnClearTable();
            console.log('consulta');

            processar();

        }, 8000);
    })

    function processar() {
        $.ajax({
            type: "get",
            async: true,
            url: site.base_url+"financeiro/gerarLoteUmPorUm/",
            dataType: "json",
            success: function (data) {
              //  if (data == null ) alert("Concluido!!!");
                console.log('atualizado')
            }
        });
    }
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa fa-sign-in"></i><?=lang('contas_receber_lote');?>
        </h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;font-weight: bold;color: #79796A;">
                        <thead>
                        <tr class="primary">
                            <th style="display: none"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= lang("name"); ?></th>
                            <th style="text-align: center;"><?= lang("processado"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="2" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>