<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#saldo-em-conta" class="tab-grey" style="text-align: center;"><i class="fa fa-university" style="font-size: 20px;"></i><br/>  <?= lang('saldo_das_contas') ?></a></li>
</ul>
<div class="tab-content">
    <div id="saldo-em-conta" class="tab-pane fade in">

        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-spinner nb"></i> <?= lang('info_saldo_das_contas_no_periodo') ?></h2>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf5" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls5" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <style>
                th {
                    background: #f2f2f2;
                    color: #000;
                    font-weight: bold;
                    font-size: 12px;
                }

            </style>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">

                        <div id="form">
                            <?php
                            $attrib = array('data-toggle' => 'validator', 'id' => 'form-saldo', 'role' => 'form');
                            echo form_open("financeiro/saldo", $attrib); ?>
                            <div class="row well">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?= lang("start_date", "start_date"); ?>
                                        <div class="input-group">
                                            <div id="div-retroceder-dada-inicio" class="input-group-addon no-print" style="padding: 2px 15px;cursor: pointer;">
                                                <i class="fa fa-chevron-left" style="font-size: 1.2em;"></i>
                                            </div>
                                            <input type="date" name="start_date" value="<?php echo $start_date;?>" class="form-control" id="start_date" required="required">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?= lang("end_date", "end_date"); ?>
                                        <div class="input-group">
                                            <input type="date" name="end_date" value="<?php echo $end_date;?>" class="form-control" id="end_date"" required="required">
                                            <div id="div-avancar-dada-final" class="input-group-addon no-print" style="padding: 2px 15px;cursor: pointer;">
                                                <i class="fa fa-chevron-right"  style="font-size: 1.2em;"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls">
                                    <?php echo form_submit('submit_sale_report', $this->lang->line("submit"), 'id="btnSubmit" class="btn btn-primary"'); ?> </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>

                        <div class="table-responsive">
                            <table class="table-for-extrato table-hover" style="cursor: pointer;">
                                <thead>
                                <tr class="tr-border-left-color-gree">
                                    <th class="col-md-4" style="padding: 15px;"><?= lang("contas"); ?></th>
                                    <th class="col-md-2 right"><?= lang("contas_saldo_dia_anterior"); ?></th>
                                    <th class="col-md-2 right"><?= lang("contas_entradas"); ?></th>
                                    <th class="col-md-2 right"><?= lang("contas_saidas"); ?></th>
                                    <th class="col-md-2 right"><?= lang("contas_saldo"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $totalSaldoDiaAnterior = 0;
                                $totalEntrada = 0;
                                $totalSaida = 0 ;
                                $totalSaldo = 0;

                                foreach ($movimentadores as $movimentador) {

                                    $saldosEmConta = $this->financeiro_model->getMovimentadorSaldoPeriodo($movimentador->id, $start_date, $end_date);

                                    if (count($saldosEmConta) > 0) {
                                        foreach ($saldosEmConta as $saldoEmConta) {

                                            $saldoDoDiaAnterior = $this->financeiro_model->getMovimentadoSaldoAnterior($end_date, $saldoEmConta->movimentadorId);

                                            $saldo = $saldoEmConta->saldo + $saldoDoDiaAnterior;

                                            $totalSaldoDiaAnterior += $saldoDoDiaAnterior;
                                            $totalEntrada += $saldoEmConta->entradas;
                                            $totalSaida += $saldoEmConta->saidas;
                                            $totalSaldo += $saldo;
                                            ?>
                                            <tr class="border-top-dotted tr-border-left-color-gree" onclick="abrirExtratoDaConta(<?php echo $saldoEmConta->movimentadorId;?>)">
                                                <td style="font-weight: bold;padding: 10px;"><i class="fa-fw fa fa-bank nb" style="font-size: 20px;"></i> <?php echo strtoupper($saldoEmConta->movimentador); ?></td>
                                                <?php if ($saldoDoDiaAnterior > 0 ) {?>
                                                    <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> C</td>
                                                <?php } else { ?>
                                                    <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> D</td>
                                                <?php } ?>
                                                <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney($saldoEmConta->entradas); ?> C</td>
                                                <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney($saldoEmConta->saidas); ?> D</td>
                                                <?php if ($saldo > 0){?>
                                                    <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney($saldo); ?> C</td>
                                                <?php } else {?>
                                                    <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney($saldo); ?> D</td>
                                                <?php } ?>
                                            </tr>
                                        <?php }  ?>
                                    <?php } else {

                                        $saldoDoDiaAnterior = $this->financeiro_model->getMovimentadoSaldoAnterior($end_date, $movimentador->id);

                                        $totalSaldoDiaAnterior += $saldoDoDiaAnterior;
                                        $totalSaldo +=  $saldoDoDiaAnterior;

                                        ?>
                                        <tr class="border-top-dotted tr-border-left-color-gree" onclick="abrirExtratoDaConta(<?php echo $movimentador->id;?>)">
                                            <td style="font-weight: bold;padding: 10px;"><i class="fa-fw fa fa-bank nb" style="font-size: 20px;"></i> <?php echo strtoupper($movimentador->name); ?></td>
                                            <?php if ($saldoDoDiaAnterior > 0 ) {?>
                                                <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> C</td>
                                            <?php } else { ?>
                                                <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> D</td>
                                            <?php } ?>
                                            <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney(0); ?> C</td>
                                            <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney(0); ?> D</td>

                                            <?php if ($saldoDoDiaAnterior > 0) {?>
                                                <td class="right" style="color: blue;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> C</td>
                                            <?php } else { ?>
                                                <td class="right" style="color: red;padding: 10px;"><?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?> D</td>
                                            <?php } ?>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="border-buttom-dotted" style="border-left: solid 2px #f2f2f2 !important;">
                                    <td class="right title-for-totalizador-extato"><b>TOTAL</b></td>

                                    <?php if ($totalSaldoDiaAnterior > 0) {?>
                                        <td class="right title-for-totalizador-extato" style="color: blue;"><?php echo $this->sma->formatMoney($totalSaldoDiaAnterior);?> C</td>
                                    <?php } else { ?>
                                        <td class="right title-for-totalizador-extato" style="color: red;"><?php echo $this->sma->formatMoney($totalSaldoDiaAnterior);?> D</td>
                                    <?php } ?>
                                    <td class="right title-for-totalizador-extato" style="color: blue;"><?php echo $this->sma->formatMoney($totalEntrada);?> C</td>
                                    <td class="right title-for-totalizador-extato" style="color: red;"><?php echo $this->sma->formatMoney($totalSaida);?> D</td>

                                    <?php if ($totalSaldo > 0){?>
                                        <td class="right title-for-totalizador-extato" style="color: blue;"><?php echo $this->sma->formatMoney($totalSaldo);?> C</td>
                                    <?php } else { ?>
                                        <td class="right title-for-totalizador-extato" style="color: red;"><?php echo $this->sma->formatMoney($totalSaldo);?> D</td>
                                    <?php } ?>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/pdf/?v=1'.$v)?>";
            return false;
        });

        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/0/xls/?v=1'.$v)?>";
            return false;
        });

        $('#div-retroceder-dada-inicio').click(function (event) {
            retrocederData($('#start_date'));
            retrocederData($('#end_date'));

            $('#btnSubmit').click();
        });

        $('#div-avancar-dada-final').click(function (event) {
            avancarData($('#end_date'));
            avancarData($('#start_date'));

            $('#btnSubmit').click();
        });

        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.sales-table'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
        $('#pdf1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/pdf/?v=1'.$p)?>";
            return false;
        });
        $('#xls1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/0/xls/?v=1'.$p)?>";
            return false;
        });
        $('#image1').click(function (event) {
            event.preventDefault();
            html2canvas($('.payments-table'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
    });

    function retrocederData(dataRetroceder) {

        let dtInicio = new Date(dataRetroceder.val());

        //dtInicio.setTime(dtInicio.getTime() - (24*60*60*1000));
        var newDate = dtInicio.toLocaleDateString().split('/').reverse().join('-');

        dataRetroceder.val(newDate);
    }

    function avancarData(dataAvancar) {

        let dtInicio = new Date(dataAvancar.val());

        dtInicio.setTime(dtInicio.getTime() + (24*60*60*1000)*2);
        var newDate = dtInicio.toLocaleDateString().split('/').reverse().join('-');

        dataAvancar.val(newDate);
    }

    function abrirExtratoDaConta(movimentador) {
        window.location.href = "<?=site_url('financeiro/extrato/'.date('m') )?>/<?php echo date('Y');?>/"+movimentador;
    }
</script>
