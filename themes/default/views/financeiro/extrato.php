<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#financeiro" class="tab-grey" style="text-align: center;font-size: 13px;font-weight: bold;">
            <i class="fa fa-bank" style="font-size: 20px;margin-bottom: 5px;"></i><br/>
            <?php
                $cbContas[''] =  lang('todas_as_contas');
                foreach ($movimentadores as $conta) {
                    $cbContas[$conta->id] = $conta->name;
                }
            ?>
            <?= form_dropdown('movimentador', $cbContas, $movimentador, 'class="form-control" required="required" '.$readonly.' id="movimentador"'); ?>
        </a>
    </li>
</ul>
<div class="tab-content">
    <div id="financeiro" class="tab-pane fade in">

        <?php
        $v = "&customer=" . $user_id;
        if ($this->input->post('submit_sale_report')) {
            if ($this->input->post('biller')) {
                $v .= "&biller=" . $this->input->post('biller');
            }
            if ($this->input->post('warehouse')) {
                $v .= "&warehouse=" . $this->input->post('warehouse');
            }
            if ($this->input->post('user')) {
                $v .= "&user=" . $this->input->post('user');
            }
            if ($this->input->post('serial')) {
                $v .= "&serial=" . $this->input->post('serial');
            }
            if ($this->input->post('start_date')) {
                $v .= "&start_date=" . $this->input->post('start_date');
            }
            if ($this->input->post('end_date')) {
                $v .= "&end_date=" . $this->input->post('end_date');
            }
        }
        ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#form').hide();
                $('.toggle_down').click(function () {
                    $("#form").slideDown();
                    return false;
                });
                $('.toggle_up').click(function () {
                    $("#form").slideUp();
                    return false;
                });
            });
        </script>

        <style>
            #calendario {
                height: 55px;
                background-color: #FFF;
                border-bottom: 1px solid #cccccc
            }

            #calendario #content_select_ano {
                float: left;
                margin: 30px 0 0 45px;
                position: relative
            }

            #calendario #content_select_ano #ano_selected {
                font-size: 30px;
                font-family: "robotobold", Impact, Haettenschweiler, "Franklin Gothic Bold", "Arial Black", sans-serif;
                color: #428bca;
                font-size: 30px;
                text-decoration: none;
                width: 102px;
                display: block;
                text-align: left;
                background: url('../img/all/bg_images.png') no-repeat 75px -309px
            }

            #calendario #content_select_ano #ano_selected.listaAtiva {
                background-position: 75px -375px
            }

            #calendario #content_select_ano ul {
                list-style: none;
                margin: 0;
                padding: 0;
                background-color: #FFF;
                height: 200px;
                width: 100px;
                overflow: auto;
                border: 1px solid #CCC;
                position: absolute;
                top: 41px;
                left: 0;
                display: none
            }

            #calendario #content_select_ano ul li {
                border-bottom: 1px solid #C4C4C4
            }

            #calendario #content_select_ano ul li a {
                padding: 10px 20px;
                display: block;
                text-decoration: none;
                color: #727272
            }

            #calendario #content_select_ano ul li a:hover {
                text-decoration: none;
                color: #000;
                background-color: #D9D9D9
            }

            #calendario #btns_acoes {
                margin: 39px 30px 18px 0;
                float: right
            }

            #calendario #radio_meses {
                width: 100%;
                clear: both
            }

            #calendario #radio_meses a {
                color: #2c3e50;
                text-decoration: none;
                font-size: 17px;
                padding: 3px 0;
                margin: 0 18px
            }

            #calendario #radio_meses a:hover {
                border-bottom: 4px solid #428bca
            }

            #calendario #radio_meses a.mesAtual {
                border-bottom: 4px solid #428bca
            }

            #bodyPrincipal form.form_cadastro {
                text-align: left
            }
        </style>
        <div class="box sales-table">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-tasks nb"></i> <?= lang('info_extrato_bancario') ?></h2>
            </div>
            <div class="box-content">
                <div id="calendario">
                    <div id="radio_meses">
                        <select id="filtroAno">
                            <option <?php if($anoAtual == '2018') echo  'selected="selected"';?>>2018</option>
                            <option <?php if($anoAtual == '2019') echo  'selected="selected"';?>>2019</option>
                            <option <?php if($anoAtual == '2020') echo  'selected="selected"';?>>2020</option>
                            <option <?php if($anoAtual == '2021') echo  'selected="selected"';?>>2021</option>
                            <option <?php if($anoAtual == '2022') echo  'selected="selected"';?>>2022</option>
                            <option <?php if($anoAtual == '2022') echo  'selected="selected"';?>>2023</option>
                            <option <?php if($anoAtual == '2022') echo  'selected="selected"';?>>2025</option>
                        </select>
                        <a href="<?php echo base_url();?>financeiro/extrato/1/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '1') echo  'class="mesAtual"';?>>JAN</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/2/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '2') echo  'class="mesAtual"';?>>FEV</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/3/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '3') echo  'class="mesAtual"';?>>MAR</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/4/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '4') echo  'class="mesAtual"';?>>ABR</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/5/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '5') echo  'class="mesAtual"';?>>MAI</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/6/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '6') echo  'class="mesAtual"';?>>JUN</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/7/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '7') echo  'class="mesAtual"';?>>JUL</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/8/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '8') echo  'class="mesAtual"';?>>AGO</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/9/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '9') echo  'class="mesAtual"';?>>SET</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/10/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '10') echo  'class="mesAtual"';?>>OUT</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/11/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '11') echo  'class="mesAtual"';?>>NOV</a>
                        <a href="<?php echo base_url();?>financeiro/extrato/12/<?php echo $anoAtual;?>/<?php echo $movimentador;?>" <?php if($mesAtual == '12') echo  'class="mesAtual"';?>>DEZ</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="clearfix"></div>
                        <style>

                            .border-ponter-buttom {
                                border-width: 0px 0px 1px;
                                border-style: dotted;
                                color: #333;
                            }

                            .tableM {
                                width: 100%;
                                max-width: 100%;
                                margin-bottom: -5px;
                            }
                        </style>

                        <div class="table-responsive">
                            <table class="tableM table-hover" style="cursor: pointer;">

                                <?php
                                $saldoInicial = 0;
                                $dataPagamento = '';
                                $contador = 0;

                                if ($extratos) {
                                    foreach ($extratos as $extrato) {

                                        if ($extrato->status == 'ESTORNO') continue;

                                        $periodo = date('Y-m-d', strtotime($extrato->date));
                                        $fatura = $this->financeiro_model->getFaturaById($extrato->fatura);
                                        $itens =  $this->sales_model->getSaleItemByContaReceber($fatura->contas_receber);

                                        if ($periodo!= $dataPagamento) {?>
                                            <thead>
                                            <tr>
                                                <th style="text-align: left;font-size: 15px;padding: 15px;">
                                                    <i class="fa fa-minus-square-o"></i> <?php echo $this->sma->hrsd($periodo); ?>
                                                </th>
                                            </tr>
                                            </thead>
                                        <?php } ?>
                                        <tbody>
                                            <?php if ($contador == 0) {

                                                $dt = $anoAtual.'-'.$mesAtual.'-'.'01';
                                                $dataMesAnterior = date("Y-m-d", strtotime($dt. " -1 day"));
                                                $saldoDoDiaAnterior = $this->financeiro_model->getMovimentadoSaldoAnterior($dataMesAnterior, $extrato->movimentador, TRUE);
                                                $saldoInicial += $saldoDoDiaAnterior;

                                                ?>
                                                <tr class="border-ponter-buttom">
                                                    <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                    <td style="border-top: none;padding: 15px;"><span>SALDO DO MÊS ANTERIOR</span></td>
                                                    <td style="border-top: none;padding: 15px;"><i class="fa fa-barcode" style="font-size: 12px;"> Cód: 00000000</td>

                                                    <?php if ($saldoDoDiaAnterior > 0) {?>
                                                        <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                        <td style="border-top: none;color: blue;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($saldoDoDiaAnterior);?> C</td>
                                                    <?php } else { ?>
                                                        <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                        <td style="border-top: none;color: red;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($saldoDoDiaAnterior);?> D</td>
                                                    <?php } ?>
                                                    <td style="border-top: none;text-align: center;"></td>
                                                </tr>
                                            <?php } ?>

                                                <?php if ($extrato->tipoOperacao == Financeiro_model::TIPO_OPERACAO_CREDITO){
                                                    $saldoInicial += $extrato->amount;
                                                    ?>
                                                    <tr class="border-ponter-buttom">
                                                        <?php if ($extrato->tipo == Sales_model::TIPO_DE_PAGAMENTO_DEPOSITO){ ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;"><i class="fa fa-bank" style="font-size: 20px;color: blue;"></i>
                                                                <span class="number black"><i class="fa fa-arrow-circle-up" style="font-size: 14px;color: blue;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Deposito na Conta realizado por</span><br/>
                                                                <strong style="font-size: 15px;"><?php echo $extrato->nomeCliente;?> </strong>
                                                                <?php foreach ($itens as $item){?>
                                                                    <br/><strong><?php echo ' '.$item->product_name;?></strong>
                                                                <?php }?>
                                                                <?php if ($extrato->note){?>
                                                                    <br/><small><?=$extrato->note;?></small>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } else if ($extrato->tipo == Sales_model::TIPO_DE_PAGAMENTO_TRANSFERENCIA) { ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;"><i class="fa fa-exchange" style="font-size: 20px;color: blue;"></i>
                                                                <span class="number black"><i class="fa fa-arrow-circle-up" style="font-size: 14px;color: blue;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Transferência recebida da Conta</span><br/>
                                                                <strong><?php echo strtoupper($extrato->movimentador);?></strong><br/>
                                                            </td>
                                                        <?php } else { ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;"><i class="fa fa-usd" style="font-size: 25px;color: blue;"></i>
                                                                <span class="number black"><i class="fa fa-arrow-circle-up" style="font-size: 12px;color: blue;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Pagamento recebido em <span style="color: blue"> <?php echo $extrato->paid_by;?> </span>  de</span><br/>
                                                                <strong style="font-size: 15px;"><?php echo strtoupper($extrato->nomeCliente);?></strong>
                                                                <?php foreach ($itens as $item){?>
                                                                    <br/><strong><?php echo ' '.$item->product_name;?></strong>
                                                                <?php }?>
                                                                <?php if ($extrato->note){?>
                                                                    <br/><small><?=$extrato->note;?></small>
                                                                <?php } ?>
                                                                <?php if (empty($item)){?>
                                                                    <br/><small><?php echo $extrato->strReceitas;?></small>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } ?>

                                                        <td style="border-top: none;padding: 15px;"><i class="fa fa-barcode" style="font-size: 12px;"> <?php echo $extrato->reference_no;?></td>
                                                        <td style="border-top: none;color: blue;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($extrato->amount);?> C</td>
                                                        <?php if ($saldoInicial > 0){?>
                                                            <td style="border-top: none;color: blue;text-align: right;padding: 15px;"><?php echo $this->sma->formatMoney($saldoInicial);?> C</td>
                                                        <?php } else { ?>
                                                            <td style="border-top: none;color: red;text-align: right;padding: 15px;"><?php echo $this->sma->formatMoney($saldoInicial);?> D</td>
                                                        <?php } ?>

                                                        <td class="acoes" style="border-top: none;padding: 15px;">
                                                            <div class="text-center">
                                                                <div class="btn-group text-left">
                                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li><a href="<?= site_url('sales/payment_note/' . $extrato->id) ?>" target="<?php echo $extrato->id;?>"><i class="fa fa-file-text-o"></i> Emitir Recibo de pagamento</a></li>

                                                                        <?php if ($extrato->attachment) {?>
                                                                            <li><a href="<?= base_url('assets/uploads/' . $extrato->attachment); ?>" target="_blank"><i class="fa fa-chain"></i> Ver anexo de pagamento</a></li>
                                                                        <?php } ?>

                                                                        <?php if ($payment->paid_by != 'gift_card') { ?>
                                                                            <li><a href="<?= site_url('financeiro/editarPagamento/' . $extrato->id) ?>"
                                                                                   data-toggle="modal" data-target="#myModal4"><i class="fa fa-comments"></i> Ver Observações de pagamento</a></li>

                                                                            <li class="divider"></li>
                                                                            <li><a href="<?php echo base_url();?>financeiro/historico/<?php echo $extrato->fatura;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i><?php echo lang('historico_de_parcelas')?></a></li>

                                                                            <li style="display: none;">
                                                                                <a href="#" class="po"
                                                                                   title="<b><?= $this->lang->line("delete_payment") ?></b>"
                                                                                   data-content="<p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' id='<?= $extrato->id ?>' href='<?= site_url('sales/delete_payment/' . $extrato->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn po-close'><?= lang('no') ?></button>"
                                                                                   rel="popover"><i class="fa fa-recycle"></i> Estornar
                                                                                </a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>

                                                    </tr>
                                                <?php } else {

                                                    $saldoInicial -= $extrato->amount;

                                                    ?>
                                                    <tr class="border-ponter-buttom">
                                                        <?php if ($extrato->tipo == Sales_model::TIPO_DE_PAGAMENTO_SAQUE){ ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;"><i class="fa fa-bank" style="font-size: 20px;color: red;"></i>
                                                                <span class="number black"><i class="fa fa-arrow-circle-down" style="font-size: 14px;color: red;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Saque da Conta realizado por</span><br/>
                                                                <strong style="font-size: 15px;"><?php echo $extrato->nomeCliente;?> </strong>
                                                                <?php foreach ($itens as $item){?>
                                                                    <br/><strong><?php echo ' '.$item->product_name;?></strong>
                                                                <?php }?>
                                                                <?php if ($extrato->note){?>
                                                                    <br/><small><?=$extrato->note;?></small>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } else if ($extrato->tipo == Sales_model::TIPO_DE_PAGAMENTO_TRANSFERENCIA) { ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;">
                                                                <i class="fa fa-exchange" style="font-size: 25px;color: red;"></i>
                                                                <span class="number black"><i class="fa fa-money" style="font-size: 12px;color: red;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Transferência realizado para  Conta</span><br/>
                                                                <strong><?php echo strtoupper($extrato->movimentador);?></strong><br/>
                                                            </td>
                                                        <?php } else if($extrato->tipo == Sales_model::TIPO_DE_PAGAMENTO_TAXAS) { ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;">
                                                                <i class="fa fa-flag" style="font-size: 25px;color: red;"></i>
                                                                <span class="number black"><i class="fa fa-money" style="font-size: 12px;color: red;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Desconto de <br/>
                                                                <strong><?php echo strtoupper($extrato->strDespesas);?></strong><br/>
                                                            </td>
                                                        <?php } else { ?>
                                                            <td style="border-top: none;text-align: center;padding: 15px;">
                                                                <i class="fa fa-arrow-circle-down" style="font-size: 25px;color: red;"></i>
                                                                <span class="number black"><i class="fa fa-money" style="font-size: 12px;color: red;"></i></span>
                                                            </td>
                                                            <td style="border-top: none;width: 45%;padding: 15px;">
                                                                <span>Pagamento realizado em <span style="color: red"> <?php echo  $extrato->paid_by;?> </span>  para </span><br/>
                                                                <strong style="font-size: 15px;"><?php echo strtoupper($extrato->nomeCliente);?></strong>
                                                                <?php foreach ($itens as $item){?>
                                                                    <br/><strong><?php echo ' '.$item->product_name;?></strong><br/>
                                                                <?php }?>
                                                                <?php if ($extrato->note){?>
                                                                    <br/><small><?=$extrato->note;?></small>
                                                                <?php } ?>
                                                                <?php if (empty($item)){?>
                                                                    <br/><small><?php echo $extrato->strDespesas;?></small>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } ?>

                                                        <td style="border-top: none;padding: 15px;"><i class="fa fa-barcode" style="font-size: 12px;"> <?php echo $extrato->reference_no;?></td>
                                                        <td style="border-top: none;color: red;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($extrato->amount);?> D</td>

                                                        <?php if ($saldoInicial > 0){?>
                                                            <td style="border-top: none;color: blue;text-align: right;padding: 15px;"><?php echo $this->sma->formatMoney($saldoInicial);?> C</td>
                                                        <?php } else { ?>
                                                            <td style="border-top: none;color: red;text-align: right;padding: 15px;"><?php echo $this->sma->formatMoney($saldoInicial);?> D</td>
                                                        <?php } ?>

                                                        <td class="acoes" style="border-top: none;padding: 15px;">
                                                            <div class="text-center">
                                                                <div class="btn-group text-left">
                                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li><a href="<?= site_url('sales/payment_note/' . $extrato->id) ?>" target="<?php echo $extrato->id;?>"><i class="fa fa-file-text-o"></i> Emitir Recibo de pagamento</a></li>

                                                                        <?php if ($extrato->attachment) {?>
                                                                            <li><a href="<?= base_url('assets/uploads/' . $extrato->attachment); ?>" target="_blank"><i class="fa fa-chain"></i> Ver anexo de pagamento</a></li>
                                                                        <?php } ?>

                                                                        <?php if ($payment->paid_by != 'gift_card') { ?>
                                                                            <li><a href="<?= site_url('financeiro/editarPagamento/' . $extrato->id) ?>"
                                                                                   data-toggle="modal" data-target="#myModal4"><i class="fa fa-comments"></i> Ver Observações de pagamento</a></li>

                                                                            <li class="divider"></li>
                                                                            <li><a href="<?php echo base_url();?>financeiro/historico/<?php echo $extrato->fatura;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i><?php echo lang('historico_de_parcelas')?></a></li>

                                                                            <li style="display: none;"><a href="#" class="po"
                                                                                   title="<b><?= $this->lang->line("delete_payment") ?></b>"
                                                                                   data-content="<p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' id='<?= $extrato->id ?>' href='<?= site_url('sales/delete_payment/' . $extrato->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn po-close'><?= lang('no') ?></button>"
                                                                                   rel="popover"><i class="fa fa-recycle"></i> Estornar Pagamento</a>
                                                                            </li>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                        </tbody>
                                        <?php $dataPagamento = $periodo;?>
                                        <?php $contador++ ?>
                                    <?php }?>
                                <?php } else { ?>
                                    <thead>
                                        <tr><th style="text-align: left;font-size: 15px;"><i class="fa fa-minus-square-o"></i> <?php echo $this->sma->hrsd(date('Y-m-d')); ?></th></tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <?php
                                                $saldoDoDiaAnterior = $this->financeiro_model->getMovimentadoSaldoAnterior( date('Y-m-d'), $extrato->movimentador, TRUE);
                                            ?>
                                            <table class="table table-hover" style="cursor: pointer;border: none;">
                                                <tbody>
                                                <tr class="border-ponter-buttom">
                                                    <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                    <td style="border-top: none;padding: 15px;"><span>SALDO DO MÊS ANTERIOR</span></td>
                                                    <td style="border-top: none;padding: 15px;"><i class="fa fa-barcode" style="font-size: 12px;"> Cód: 0000000000</td>

                                                    <?php if ($saldoDoDiaAnterior > 0) {?>
                                                        <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                        <td style="border-top: none;color: blue;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($saldoDoDiaAnterior);?> C</td>
                                                    <?php } else { ?>
                                                        <td style="border-top: none;text-align: center;padding: 15px;"></td>
                                                        <td style="border-top: none;color: red;text-align: right;padding: 15px;"> <?php echo $this->sma->formatMoney($saldoDoDiaAnterior);?> D</td>
                                                    <?php } ?>
                                                    <td style="border-top: none;text-align: center;"></td>
                                                </tr>
                                                <tr><td colspan="6">* Não há lançamentos do mês.</td></tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $("#filtroAno").select2().on("change", function(e) {
            var ano = $('#filtroAno').val();
            var movimentador = $('#movimentador').val();

            window.location.href = "<?=site_url('financeiro/extrato/'.$mesAtual)?>/"+ano+"/"+movimentador;
        });

        $("#movimentador").select2().on("change", function(e) {
            var ano = $('#filtroAno').val();
            var movimentador = $('#movimentador').val();

            window.location.href = "<?=site_url('financeiro/extrato/'.$mesAtual)?>/"+ano+"/"+movimentador;
        });

        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/pdf/?v=1' . $v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/0/xls/?v=1' . $v)?>";
            return false;
        });
    });
</script>
