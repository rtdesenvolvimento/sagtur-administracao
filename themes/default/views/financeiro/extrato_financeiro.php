<style type="text/css" media="screen">
    #SLData td:nth-child(1) {display: none;}
    #SLData td:nth-child(2) {text-align: center;}
    #SLData td:nth-child(3) {text-align: left;width: 50%;}
    #SLData td:nth-child(4) {text-align: left;width: 50%;}
    #SLData td:nth-child(5) {text-align: left;display: none;}
    #SLData td:nth-child(6) {text-align: left;display: none;}
    #SLData td:nth-child(7) {text-align: right;}
    #SLData td:nth-child(8) {text-align: right;}
    #SLData td:nth-child(9) {text-align: right;}
    #SLData td:nth-child(10) {text-align: right;}
    #SLData td:nth-child(11) {text-align: center;display: none;}
    #SLData td:nth-child(12) {text-align: center;display: none;}
    #SLData td:nth-child(13) {text-align: center;display: none;}
    #SLData td:nth-child(14) {text-align: center;display: none;}
    #SLData td:nth-child(15) {text-align: center;display: none;}
    #SLData td:nth-child(16) {text-align: center;display: none;}
    #SLData td:nth-child(17) {text-align: center;display: none;}
    #SLData td:nth-child(18) {text-align: center;display: none;}
    #SLData td:nth-child(19) {text-align: center;display: none;}
    #SLData td:nth-child(20) {text-align: center;}
</style>

<script>

    var EXTRATO_MENSAL_JA_CARREGADO = false;

    var struture = {
        'total' : 6,
        'pago' : 7,
        'pagar': 8,
        'tipoCobranca': 15,
        'codigo' : 17,
        'tipoOperacao' : 18
    };

    function attachDescriptionToName(x, alignment, aaData) {

        if (aaData[15] !== null) {

            let codigo = aaData[struture.codigo];

            if ( aaData[struture.tipoCobranca] === 'boleto') x = '<a href="'+aaData[14]+'" target="_blank" title="Ver Boleto"><i class="fa fa-barcode"></i> '+x+'</a><br/><small>Cod. '+codigo+'</small> ';
            else  x = '<a href="'+aaData[16]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[5] !== null) x += '<br/><small><i class="fa fa-money"></i> '+aaData[5]+'</small>';

        return x;
    }

    function currencyFormatValuePay(x, alignment, aaData) {

        let tipoOperacao = aaData[struture.tipoOperacao];
        let cor;

        if (alignment === undefined) alignment = 'text-right';

        if (tipoOperacao === 'CREDITO') {
            cor = '#0e731b;';
        } else {
            cor = '#F43E61;';
            x = x*-1;
        }

        if (x != null) {
            return '<div class="'+alignment+'" style="color:'+cor+';">'+formatMoney(x)+'<br/><small>'+aaData[10]+'</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValuePaid(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let dtUltimoPagamento = aaData[11] != null ? aaData[11] : '&nbsp;';

        let tipoOperacao = aaData[struture.tipoOperacao];
        let color = '';

        if (tipoOperacao === 'CREDITO') color = '#0e731b';
        else {
            color = '#F43E61';
            x = x*-1;
        }

        if (x != null) {
            return '<div class="' + alignment + '" style="color: '+color+'" >' + formatMoney(x) + '<br/><small>' + dtUltimoPagamento + '</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValueToPay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let diferencaEmDias = aaData[12] != null ? aaData[12] : '&nbsp;';
        let status = aaData[9];

        let tipoOperacao = aaData[struture.tipoOperacao];
        let color = '';

        if (tipoOperacao === 'CREDITO') color = '#0e731b';
        else {
            color = '#F43E61';
            x = x*-1;
        }

        if (diferencaEmDias < 0 && status !== 'QUITADA') {

            if (diferencaEmDias === -1) return '<div class="' + alignment + '" style="color: '+color+';">'+formatMoney(x)+'<br/><small>'+diferencaEmDias+' dia</small></div>';
            else return '<div class="' + alignment + '" style="color: '+color+';">'+formatMoney(x)+'<br/><small>'+diferencaEmDias+' dias</small></div>';
        } else {
            return '<div class="' + alignment + '" style="color: '+color+';">'+formatMoney(x)+'<br/><small>&nbsp;</small></div>';
        }
    }

    function currencyFormatDebit(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #F43E61;!important;">'+formatMoney(x)+'</div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatCredit(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #0e731b;!important;">'+formatMoney(x)+'</div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterTipoCobranca').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterDespesa').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterReceita').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#filter_numero_documento').keydown(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('financeiro/getExtrato')?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filter_numero_documento", "value":  $('#filter_numero_documento').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterDespesa", "value":  $('#filterDespesa').val() });
                aoData.push({ "name": "filterReceita", "value":  $('#filterReceita').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                let vendaId = aData[13];

                if (vendaId !== null) {
                    nRow.id = aData[13];
                    nRow.className = "invoice_link";
                }

                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormatValuePay},
                {"mRender": currencyFormatValuePaid},
                {"mRender": currencyFormatValueToPay},
                {"mRender": row_status},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

                const totalizadorCredito = aaData
                    .filter( faturaAtua => {return faturaAtua[struture.tipoOperacao] === 'CREDITO'})
                    .reduce( (totalizadores, faturaAtual) => {
                        return createTotalizador(totalizadores, faturaAtual);
                    }, {total:0, pago: 0, pagar: 0} );

                const totalizadorDebito = aaData
                    .filter( faturaAtua => {return faturaAtua[struture.tipoOperacao] === 'DEBITO'})
                    .reduce( (totalizadores, faturaAtual) => {
                        return createTotalizador(totalizadores, faturaAtual);
                    }, {total:0, pago: 0, pagar: 0});

                var nCells = nRow.getElementsByTagName('th');

                printTotalizadorFooterTable(nCells, totalizadorCredito.total - totalizadorDebito.total, struture.total);
                printTotalizadorFooterTable(nCells, totalizadorCredito.pago - totalizadorDebito.pago, struture.pago);
                printTotalizadorFooterTable(nCells, totalizadorCredito.pagar - totalizadorDebito.pagar, struture.pagar);
                printToTotalizadoresFilter(totalizadorCredito, totalizadorDebito);

                if (isExibirExtratoMensal()) printToTotalizadoresDoMes(totalizadorCredito, totalizadorDebito);

            }
        }).fnSetFilteringDelay().dtFilter([
          //  {column_number: 1, filter_default_label: "[(ano-mes-dia)]", filter_type: "text", data: []},
           // {column_number: 2, filter_default_label: "[<?=lang('pessoa');?>]", filter_type: "text", data: []},
           // {column_number: 3, filter_default_label: "[<?=lang('servico');?>]", filter_type: "text", data: []},
           // {column_number: 4, filter_default_label: "[<?=lang('receita');?>]", filter_type: "text", data: []},
          //  {column_number: 5, filter_default_label: "[<?=lang('cobranca');?>]", filter_type: "text", data: []},
          /*  {
                column_number: 9, select_type: 'select2',
                select_type_options: {
                    placeholder: '<?=lang('payment_status');?>',
                    width: '100%',
                    minimumResultsForSearch: -1,
                    allowClear: true
                },
                data: [
                    {value: 'ABERTA', label: '<?=lang('ABERTA');?>'},
                    {value: 'PARCIAL', label: '<?=lang('PARCIAL');?>'},
                    {value: 'VENCIDA', label: '<?=lang('VENCIDA');?>'},
                    {value: 'QUITADA', label: '<?=lang('QUITADA');?>'}
                ]
            }*/

        ], "footer");
    });

    function createTotalizador(totalizadores, faturaAtual) {

        totalizadores.total = totalizadores.total + parseFloat(faturaAtual[struture.total]);
        totalizadores.pago = totalizadores.pago + parseFloat(faturaAtual[struture.pago]);
        totalizadores.pagar = totalizadores.pagar + parseFloat(faturaAtual[struture.pagar]);

        return totalizadores;
    }

    function isExibirExtratoMensal() {
        if (!EXTRATO_MENSAL_JA_CARREGADO) {EXTRATO_MENSAL_JA_CARREGADO = true;return true;};
    }

    function printTotalizadorFooterTable(nCells, total, coluns) {

        let current;

        if (total > 0) current = currencyFormatCredit(total);
        else current = currencyFormatDebit(total);

        nCells[coluns].innerHTML = current
    }

    function printToTotalizadoresDoMes(totalizadorCredito, totalizadorDebito) {
        $('#recebidas').html(currencyFormat(totalizadorCredito.pago));
        $('#aReceber').html(currencyFormat(totalizadorCredito.pagar));
        $('#totalizadorRecebimento').html(currencyFormat(totalizadorCredito.pago + totalizadorCredito.pagar));

        $('#pagas').html(currencyFormat(totalizadorDebito.pago));
        $('#aPagar').html(currencyFormat(totalizadorDebito.pagar));
        $('#totalizadorPagamentos').html(currencyFormat(totalizadorDebito.pago + totalizadorDebito.pagar));

        $('#saldoPagas').html(currencyFormat(totalizadorCredito.pago-totalizadorDebito.pago));
        $('#saldoAPagar').html(currencyFormat(totalizadorCredito.pagar-totalizadorDebito.pagar));
        $('#saldoTotalizador').html(currencyFormat(totalizadorCredito.total-totalizadorDebito.total));
    }

    function printToTotalizadoresFilter(totalizadorCredito, totalizadorDebito) {
        $('#recebidas-f').html(currencyFormat(totalizadorCredito.pago));
        $('#aReceber-f').html(currencyFormat(totalizadorCredito.pagar));
        $('#totalizadorRecebimento-f').html(currencyFormat(totalizadorCredito.pago + totalizadorCredito.pagar));

        $('#pagas-f').html(currencyFormat(totalizadorDebito.pago));
        $('#aPagar-f').html(currencyFormat(totalizadorDebito.pagar));
        $('#totalizadorPagamentos-f').html(currencyFormat(totalizadorDebito.pago + totalizadorDebito.pagar));

        $('#saldoPagas-f').html(currencyFormat(totalizadorCredito.pago-totalizadorDebito.pago));
        $('#saldoAPagar-f').html(currencyFormat(totalizadorCredito.pagar-totalizadorDebito.pagar));
        $('#saldoTotalizador-f').html(currencyFormat(totalizadorCredito.total-totalizadorDebito.total));
    }
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa fa-sign-in"></i><?=lang('extrato_financeiro');?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('contapagar/adicionarContaPagar')?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-arrow-circle-down"></i> <?=lang('adicionar_conta_pagar')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=site_url('financeiro/adicionarContaReceber')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-arrow-circle-o-up"></i> <?=lang('adicionar_conta_receber')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

    <style>
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 20px;
            padding-right: 15px;
            padding-left: 15px;
        }
    </style>
    <div class="box-content">
        <div class="row">
            <div class="col-sm-12">
                <div id="totalizadore-role" >
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="small-box padding1010 bblue col-sm-12" style="color: #ffffff">
                                <h4 class="bold" style="font-size: 17px;"><?= lang('recebimentos_mensal') ?></h4>
                                <i class="fa fa-usd"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"> Total de Recebimentos <small>(R$)</small> </div>
                                        <div style="float: right;"><div id="recebidas"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total a Receber <small>(R$)</small></div>
                                        <div style="float: right;"><div id="aReceber"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total  <small>(R$)</small></div>
                                        <div style="float: right;"><div id="totalizadorRecebimento"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="small-box padding1010 bblue col-sm-12" style="color: #ffffff">
                                <h4 class="bold" style="font-size: 17px;"><?= lang('pagamentos_mensal') ?></h4>
                                <i class="fa fa-money"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"> Total de Pagamentos <small>(R$)</small> </div>
                                        <div style="float: right;"><div id="pagas"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total a Pagar <small>(R$)</small></div>
                                        <div style="float: right;"><div id="aPagar"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total  <small>(R$)</small></div>
                                        <div style="float: right;"><div id="totalizadorPagamentos"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="small-box padding1010 bblue col-sm-12" id="divTotalizador" style="color: #ffffff">
                                <h4 class="bold" style="font-size: 17px;"><?= lang('total_mensal') ?></h4>
                                <i class="fa fa-pie-chart"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoPagas"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoAPagar"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoTotalizador"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-c.gif"></legend>
                        <div style="margin-bottom: 20px;display: none;"  class="divfilters">
                            <div class="col-sm-2">
                                <?= lang("status_viagem", "status_viagem") ?>
                                <?php
                                $opts = array(
                                    'Confirmado' => lang('confirmado'),
                                    'Inativo' => lang('produto_inativo'),
                                    'Arquivado' => lang('arquivado') ,
                                );
                                echo form_dropdown('status', $opts,  $unit, 'class="form-control" id="status"');
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?= lang("ano", "ano") ?>
                                <?php
                                $opts = array(
                                    '2020' => lang('2020'),
                                    '2021' => lang('2021'),
                                    '2022' => lang('2022'),
                                    '2023' => lang('2023'),
                                    '2024' => lang('2024'),
                                    '2025' => lang('2025'),
                                    '2026' => lang('2026'),
                                    '2027' => lang('2027'),
                                    '2028' => lang('2028'),
                                    '2029' => lang('2029'),
                                    '2030' => lang('2030'),
                                );
                                echo form_dropdown('ano', $opts,  $ano, 'class="form-control" id="ano"');
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?= lang("mes", "mes") ?>
                                <?php
                                $opts = array(
                                    'Todos' => lang('Todos'),
                                    '01' => lang('Janeiro'),
                                    '02' => lang('Fevereiro'),
                                    '03' => lang('Março'),
                                    '04' => lang('Abril'),
                                    '05' => lang('Maio'),
                                    '06' => lang('Junho'),
                                    '07' => lang('Julho'),
                                    '08' => lang('Agosto'),
                                    '09' => lang('Setembro'),
                                    '10' => lang('Outubro'),
                                    '11' => lang('Novembro'),
                                    '12' => lang('Dezembro'),
                                );
                                echo form_dropdown('mes', $opts,  $mes, 'class="form-control" id="mes"');
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="product"><?= lang("products"); ?></label>
                                <?php
                                $pgs[""] = lang('select').' '.lang('product');
                                echo form_dropdown('programacao', $pgs,  '', 'class="form-control" id="programacao" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"');
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang("data_vencimento_de", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= lang("data_vencimento_ate", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="filterStatus"><?=lang('payment_status');?></label>
                                    <select class="form-control tip" name="filterStatus" id="filterStatus">
                                        <option value="">Selecione</option>
                                        <option value="ABERTA"><?=lang('ABERTA');?></option>
                                        <option value="PARCIAL"><?=lang('PARCIAL');?></option>
                                        <option value="QUITADA"><?=lang('QUITADA');?></option>
                                        <option value="VENCIDA"><?=lang('VENCIDA');?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                    <?php
                                    $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : ""), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_receita"); ?></label>
                                    <select class="form-control tip" name="filterReceita" id="filterReceita">
                                        <option value=""><?php echo lang('select').' '.lang('receita');?></option>
                                        <?php
                                        foreach ($planoReceitas as $objReceita) {?>
                                            <optgroup label="<?php echo $objReceita->name;?>">
                                                <?php
                                                $receitasFilhas = $this->financeiro_model->getReceitaByReceitaSuperiorId($objReceita->id);
                                                foreach ($receitasFilhas as $item) {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("despesa"); ?></label>
                                    <select class="form-control tip" name="filterDespesa" id="filterDespesa">
                                        <option value=""><?php echo lang('select').' '.lang('despesa');?></option>
                                        <?php
                                        foreach ($planoDespesas as $despesa) {?>
                                            <optgroup label="<?php echo $despesa->name;?>">
                                                <?php
                                                $despesasFilhas = $this->financeiro_model->getReceitaByDespesaSuperiorId($despesa->id);
                                                foreach ($despesasFilhas as $item) {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display: none;">
                                <div class="form-group">
                                    <?= lang("numero_documento", "filter_numero_documento") ?>
                                    <?= form_input('filter_numero_documento', (isset($_POST['filter_numero_documento']) ? $_POST['filter_numero_documento'] : ''), 'class="form-control" id="filter_numero_documento"'); ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div id="totalizadore-role" >
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="small-box padding1010 bmGreen col-sm-12" style="color: #ffffff">
                                <h4 class="bold"  style="font-size: 15px;"><?= lang('recebimentos_por_filtro') ?></h4>
                                <i class="fa fa-usd"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"> Total de Recebimentos <small>(R$)</small> </div>
                                        <div style="float: right;"><div id="recebidas-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total a Receber <small>(R$)</small></div>
                                        <div style="float: right;"><div id="aReceber-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total  <small>(R$)</small></div>
                                        <div style="float: right;"><div id="totalizadorRecebimento-f"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="small-box padding1010 borange col-sm-12" style="color: #ffffff">
                                <h4 class="bold"  style="font-size: 15px;"><?= lang('pagamentos_por_filtro') ?></h4>
                                <i class="fa fa-money"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"> Total de Pagamentos <small>(R$)</small> </div>
                                        <div style="float: right;"><div id="pagas-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total a Pagar <small>(R$)</small></div>
                                        <div style="float: right;"><div id="aPagar-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;">Total  <small>(R$)</small></div>
                                        <div style="float: right;"><div id="totalizadorPagamentos-f"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="small-box padding1010 bpurple col-sm-12" id="divTotalizador" style="color: #ffffff">
                                <h4 class="bold"  style="font-size: 15px;"><?= lang('total_por_filtro') ?></h4>
                                <i class="fa fa-pie-chart"></i>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoPagas-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoAPagar-f"></div></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="bold">
                                        <div style="float: left;"><small> = </small> </div>
                                        <div style="float: right;"><div id="saldoTotalizador-f"></div></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                            <th><?php echo $this->lang->line("total"); ?></th>
                            <th><?php echo $this->lang->line("pago"); ?></th>
                            <th><?php echo $this->lang->line("pagar"); ?></th>
                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                            <th style="display: none;">Parc.</th>
                            <th style="display: none;">Ult.</th>
                            <th style="display: none;">Atras.</th>
                            <th style="display: none;">Venda.</th>
                            <th style="display: none;">Link.</th>
                            <th style="display: none;">Tipo.</th>
                            <th style="display: none;">Cartao.</th>
                            <th style="display: none;">Cod.</th>
                            <th style="display: none;">Tipo.</th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        createFilter('filters');
        buscarProdutos();

        $('#programacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $("#ano").change(function (event){
            buscarProdutos();
        });

        $("#mes").change(function (event){
            buscarProdutos();
        });

        $("#status").change(function (event){
            buscarProdutos();
        });

        function buscarProdutos() {

            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>apputil/buscarProgramacao",
                data: {
                    status: $('#status').val(),
                    ano: $('#ano').val(),
                    mes: $('#mes').val()
                },
                dataType: 'json',
                async: true,
                success: function (agendamentos) {
                    $('#programacao').empty();

                    var option = $('<option/>');

                    option.attr({ 'value': '' }).text('Selecione uma opção');
                    $('#programacao').append(option);

                    $(agendamentos).each(function( index, agendamento ) {
                        var option = $('<option/>');

                        option.attr({ 'value': agendamento.id }).text(agendamento.label);
                        $('#programacao').append(option);
                    });

                    $('#programacao').select2({minimumResultsForSearch: 7});
                }
            });
        }

        function createFilter(nameClasse) {
            $('.'+nameClasse).click(function() {
                if ($('.div'+nameClasse).is(':visible')) {
                    $('.div'+nameClasse).hide(300);
                    $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

                } else {
                    $('.div'+nameClasse).show(300).fadeIn();
                    $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
                }
            });
        }
    });
</script>