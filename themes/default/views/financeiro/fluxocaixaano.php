<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#financeiro" class="tab-grey" style="text-align: center;font-size: 13px;font-weight: bold;">
            <i class="fa fa-bank" style="font-size: 20px;"></i><br/> Fluxo de Caixa - Mensal</a></li>
</ul>
<div class="tab-content">
    <div id="financeiro" class="tab-pane fade in">

        <?php
        $v = "&customer=" . $user_id;
        if ($this->input->post('submit_sale_report')) {
            if ($this->input->post('biller')) {
                $v .= "&biller=" . $this->input->post('biller');
            }
            if ($this->input->post('warehouse')) {
                $v .= "&warehouse=" . $this->input->post('warehouse');
            }
            if ($this->input->post('user')) {
                $v .= "&user=" . $this->input->post('user');
            }
            if ($this->input->post('serial')) {
                $v .= "&serial=" . $this->input->post('serial');
            }
            if ($this->input->post('start_date')) {
                $v .= "&start_date=" . $this->input->post('start_date');
            }
            if ($this->input->post('end_date')) {
                $v .= "&end_date=" . $this->input->post('end_date');
            }
        }
        ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#form').hide();
                $('.toggle_down').click(function () {
                    $("#form").slideDown();
                    return false;
                });
                $('.toggle_up').click(function () {
                    $("#form").slideUp();
                    return false;
                });
            });
        </script>

        <style>
            #calendario {
                height: 55px;
                background-color: #FFF;
                border-bottom: 1px solid #cccccc
            }

            #calendario #content_select_ano {
                float: left;
                margin: 30px 0 0 45px;
                position: relative
            }

            #calendario #content_select_ano #ano_selected {
                font-size: 30px;
                font-family: "robotobold", Impact, Haettenschweiler, "Franklin Gothic Bold", "Arial Black", sans-serif;
                color: #428bca;
                font-size: 30px;
                text-decoration: none;
                width: 102px;
                display: block;
                text-align: left;
                background: url('../img/all/bg_images.png') no-repeat 75px -309px
            }

            #calendario #content_select_ano #ano_selected.listaAtiva {
                background-position: 75px -375px
            }

            #calendario #content_select_ano ul {
                list-style: none;
                margin: 0;
                padding: 0;
                background-color: #FFF;
                height: 200px;
                width: 100px;
                overflow: auto;
                border: 1px solid #CCC;
                position: absolute;
                top: 41px;
                left: 0;
                display: none
            }

            #calendario #content_select_ano ul li {
                border-bottom: 1px solid #C4C4C4
            }

            #calendario #content_select_ano ul li a {
                padding: 10px 20px;
                display: block;
                text-decoration: none;
                color: #727272
            }

            #calendario #content_select_ano ul li a:hover {
                text-decoration: none;
                color: #000;
                background-color: #D9D9D9
            }

            #calendario #btns_acoes {
                margin: 39px 30px 18px 0;
                float: right
            }

            #calendario #radio_meses {
                width: 100%;
                clear: both
            }

            #calendario #radio_meses a {
                color: #2c3e50;
                text-decoration: none;
                font-size: 17px;
                padding: 3px 0;
                margin: 0 18px
            }

            #calendario #radio_meses a:hover {
                border-bottom: 4px solid #428bca
            }

            #calendario #radio_meses a.mesAtual {
                border-bottom: 4px solid #428bca
            }

            #bodyPrincipal form.form_cadastro {
                text-align: left
            }
        </style>
        <div class="box sales-table">
            <div class="box-header">
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="form">
                            <?php echo form_open("reports/customer_report/" . $user_id); ?>
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select') . ' ' . lang('user');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                        <?php
                                        $bl[""] = lang('select') . ' ' . lang('biller');
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                        <?php
                                        $wh[""] = lang('select') . ' ' . lang('warehouse');
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                        ?>
                                    </div>
                                </div>
                                <?php if ($this->Settings->product_serial) { ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang('serial_no', 'serial'); ?>
                                            <?= form_input('serial', '', 'class="form-control tip" id="serial"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("start_date", "start_date"); ?>
                                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("end_date", "end_date"); ?>
                                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div
                                    class="controls"> <?php echo form_submit('submit_sale_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="clearfix"></div>
                        <style>
                            .tipo_cobranca {
                                font-size: 15px;
                                line-height: 1.71;
                                color: rgb(0, 0, 0);
                                font-style: normal;
                                font-stretch: normal;
                                letter-spacing: normal;
                                text-align: left;
                                font-weight: bold;
                            }

                            .border-dotted-buttom {
                                border-width: 0px 0px 1px;
                                border-style: dotted;
                                color: #79796A;
                            }

                            .tableM {
                                width: 100%;
                                max-width: 100%;
                            }

                            th {
                                background: #f2f2f2;
                                color: #004C00;
                                font-weight: bold;
                                padding: 15px;
                                font-size: 12px;
                            }

                            td {
                                padding: 15px;
                            }

                            .view {
                                margin: auto;
                                width: 1195px;
                            }

                            .wrapper {
                                position: relative;
                                overflow: auto;
                                border: 1px solid #ccc;
                                white-space: nowrap;
                            }

                            .sticky-col {
                                position: sticky;
                                position: -webkit-sticky;
                                background-color: white;
                            }

                            .first-col {
                                width: 100px;
                                min-width: 100px;
                                left: 0px;
                            }


                        </style>
                        <div class="table-responsive">
                            <div class="view">
                                <div class="wrapper">
                                    <table class="tableM" style="cursor: pointer;">
                                        <thead>
                                        <tr class="border-ponter-buttom">
                                            <th style="text-align: left;" class="sticky-col first-col">Categoria</th>
                                            <th style="text-align: center;" colspan="2" >Janeiro</th>
                                            <th style="text-align: center;" colspan="2">Fevereiro</th>
                                            <th style="text-align: center;" colspan="2">Março</th>
                                            <th style="text-align: center;" colspan="2">Abril</th>
                                            <th style="text-align: center;" colspan="2">Maio</th>
                                            <th style="text-align: center;" colspan="2">Junho</th>
                                            <th style="text-align: center;" colspan="2">Julho</th>
                                            <th style="text-align: center;" colspan="2">Agosto</th>
                                            <th style="text-align: center;" colspan="2">Setembro</th>
                                            <th style="text-align: center;" colspan="2">Outubro</th>
                                            <th style="text-align: center;" colspan="2">Novembro</th>
                                            <th style="text-align: center;" colspan="2">Dezembro</th>
                                        </tr>
                                        </thead>
                                        <thead>
                                        <tr>
                                            <th class="sticky-col first-col"></th>
                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;" >Realizado</th>

                                            <th style="text-align: center;" >Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;" >Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>

                                            <th style="text-align: center;">Previsto</th>
                                            <th style="text-align: center;">Realizado</th>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        <tr class="border-ponter-buttom">
                                            <td class="sticky-col first-col" style="padding: 0px;"> <span class="tipo_cobranca" style="color: green;margin-left: 20px;">Receitas </span></td>
                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>
                                        </tr>


                                        <tr class="border-ponter-buttom" style="border-left: solid 5px #3c763d !important;">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus" style="margin-left: 10px;"></i>  <span style="margin-left: 20px;">Receitas de Venda e de serviços</span> </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>

                                        <tr class="border-ponter-buttom" style="border-left: solid 5px #3c763d !important;">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus-circle" style="margin-left: 20px;"></i>  Receitas de Fretes e Entregas</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>


                                        <tr class="border-ponter-buttom" style="border-left: solid 5px #3c763d !important;">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus-circle" style="margin-left: 20px;"></i>  Receitas diversas </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>


                                        <tr class="border-ponter-buttom" style="border-left: solid 5px #3c763d !important;">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus-circle" style="margin-left: 20px;"></i>  Não categorizadas </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>


                                        <tr class="border-ponter-buttom">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus-circle" style="margin-left: 20px;"></i> Transferência entre contas </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>

                                        <tr class="border-ponter-buttom" style="border-left: solid 5px #3c763d !important;">
                                            <td class="sticky-col first-col" style="color: green;"> <i class="fa fa-plus-circle" style="margin-left: 20px;"></i> Comissão Juno </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>
                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col" style="background: #f1f4f9;"> <i style="margin-left: 20px;"></i>  Total de receitas</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>


                                        <tr class="border-ponter-buttom">
                                            <td class="sticky-col first-col" style="padding: 0px;"> <span class="tipo_cobranca" style="color: red;margin-left: 20px;">Despesas </span></td>
                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>
                                        </tr>

                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col"  style="background: #f1f4f9;color: red;"> <i class="fa fa-angle-down" style="margin-left: 20px;"></i> Custos Operacionais </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>

                                        <tr class="border-ponter-buttom">
                                            <td class="sticky-col first-col" style="color: red;"> <i class="fa fa-minus-circle" style="margin-left: 40px;"></i>  Luz </td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>


                                        <tr class="border-ponter-buttom">
                                            <td class="sticky-col first-col" style="padding: 0px;"> <span class="tipo_cobranca" style="color: gray;margin-left: 20px;">Resumo </span></td>
                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>

                                            <td> </td>
                                            <td> </td>
                                        </tr>

                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col" style="background: #f1f4f9;"> <i style="margin-left: 20px;"></i>  Saldo do Mês/Ano Anterior</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>
                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col" style="background: #f1f4f9;"> <i style="margin-left: 20px;"></i>  Total de Receitas</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>
                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col" style="background: #f1f4f9;"> <i style="margin-left: 20px;"></i>  Total de Despesas</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>
                                        <tr class="border-ponter-buttom" style="background: #f1f4f9;">
                                            <td class="sticky-col first-col" style="background: #f1f4f9;"> <i style="margin-left: 20px;"></i>  Saldo Final</td>
                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td style="background: #fff4ce;">R$150,00</td>
                                            <td style="background: #fff4ce;">R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>

                                            <td>R$150,00</td>
                                            <td>R$150,00</td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="payments-con" class="tab-pane fade in">

        <?php
        $p = "&customer=" . $user_id;
        if ($this->input->post('submit_payment_report')) {
            if ($this->input->post('pay_user')) {
                $p .= "&user=" . $this->input->post('pay_user');
            }
            if ($this->input->post('pay_start_date')) {
                $p .= "&start_date=" . $this->input->post('pay_start_date');
            }
            if ($this->input->post('pay_end_date')) {
                $p .= "&end_date=" . $this->input->post('pay_end_date');
            }
        }
        ?>
        <script>
            $(document).ready(function () {
                var pb = <?= json_encode($pb); ?>;

                function paid_by(x) {
                    return (x != null) ? pb[x] : x;
                }

                var oTable = $('#PayRData').dataTable({
                    "aaSorting": [[0, "desc"]],
                    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
                    "iDisplayLength": <?= $Settings->rows_per_page ?>,
                    'bProcessing': true, 'bServerSide': true,
                    'sAjaxSource': '<?= site_url('reports/getPaymentsReport/?v=1' . $p) ?>',
                    'fnServerData': function (sSource, aoData, fnCallback) {
                        aoData.push({
                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                            "value": "<?= $this->security->get_csrf_hash() ?>"
                        });
                        $.ajax({
                            'dataType': 'json',
                            'type': 'POST',
                            'url': sSource,
                            'data': aoData,
                            'success': fnCallback
                        });
                    },
                    "aoColumns": [{"mRender": fld}, null, null, {"bVisible": false}, {"mRender": paid_by}, {"mRender": currencyFormat}, {"mRender": row_status}],
                    'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                        nRow.id = aData[7];
                        nRow.className = "payment_link";
                        if (aData[6] == 'returned') {
                            nRow.className = "payment_link danger";
                        }
                        return nRow;
                    },
                    "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                        var total = 0;
                        for (var i = 0; i < aaData.length; i++) {
                            total += parseFloat(aaData[aiDisplay[i]][5]);
                        }
                        var nCells = nRow.getElementsByTagName('th');
                        nCells[4].innerHTML = currencyFormat(parseFloat(total));
                    }
                }).fnSetFilteringDelay().dtFilter([
                    {
                        column_number: 0,
                        filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]",
                        filter_type: "text",
                        data: []
                    },
                    {
                        column_number: 1,
                        filter_default_label: "[<?=lang('payment_ref');?>]",
                        filter_type: "text",
                        data: []
                    },
                    {column_number: 2, filter_default_label: "[<?=lang('sale_ref');?>]", filter_type: "text", data: []},
                    {column_number: 4, filter_default_label: "[<?=lang('paid_by');?>]", filter_type: "text", data: []},
                    {column_number: 6, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
                ], "footer");
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#payform').hide();
                $('.paytoggle_down').click(function () {
                    $("#payform").slideDown();
                    return false;
                });
                $('.paytoggle_up').click(function () {
                    $("#payform").slideUp();
                    return false;
                });
            });
        </script>

        <div class="box payments-table">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-money nb"></i><?= lang('customer_payments_report'); ?> <?php
                    if ($this->input->post('start_date')) {
                        echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
                    }
                    ?></h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" class="paytoggle_up tip" title="<?= lang('hide_form') ?>">
                                <i class="icon fa fa-toggle-up"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="paytoggle_down tip" title="<?= lang('show_form') ?>">
                                <i class="icon fa fa-toggle-down"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf1" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image1" class="tip" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?= lang('customize_report'); ?></p>

                        <div id="payform">

                            <?php echo form_open("reports/customer_report/" . $user_id . "/#payments-con"); ?>
                            <div class="row">

                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select') . ' ' . lang('user');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('pay_user', $us, (isset($_POST['pay_user']) ? $_POST['pay_user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("start_date", "start_date"); ?>
                                        <?php echo form_input('pay_start_date', (isset($_POST['pay_start_date']) ? $_POST['pay_start_date'] : ""), 'class="form-control date" id="start_date"'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("end_date", "end_date"); ?>
                                        <?php echo form_input('pay_end_date', (isset($_POST['pay_end_date']) ? $_POST['pay_end_date'] : ""), 'class="form-control date" id="end_date"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div
                                    class="controls"> <?php echo form_submit('submit_payment_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                            </div>
                            <?php echo form_close(); ?>

                        </div>
                        <div class="clearfix"></div>

                        <div class="table-responsive">
                            <table id="PayRData"
                                   class="table table-bordered table-hover table-striped table-condensed reports-table reports-table">
                                <thead>
                                <tr>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("payment_ref"); ?></th>
                                    <th><?= lang("sale_ref"); ?></th>
                                    <th><?= lang("purchase_ref"); ?></th>
                                    <th><?= lang("paid_by"); ?></th>
                                    <th><?= lang("amount"); ?></th>
                                    <th><?= lang("type"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td colspan="7"
                                        class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                </tr>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr class="active">
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= lang("amount"); ?></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div id="returns-con" class="tab-pane fade in">

        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bank nb"></i> Movimentação das contas no periodo
                    de <?php echo date('d/m/Y'); ?> até <?php echo date('d/m/Y'); ?>
                </h2>

                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf5" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls5" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="image5" class="tip image" title="<?= lang('save_image') ?>">
                                <i class="icon fa fa-file-picture-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="PRESLData" class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th class="col-md-4"><?= lang("contas"); ?></th>
                                    <th class="col-md-2"><?= lang("contas_saldo_dia_anterior"); ?></th>
                                    <th class="col-md-2"><?= lang("contas_entradas"); ?></th>
                                    <th class="col-md-2"><?= lang("contas_saidas"); ?></th>
                                    <th class="col-md-2"><?= lang("contas_saldo"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $totalSaldoDiaAnterior = 0;
                                $totalEntrada = 0;
                                $totalSaida = 0;
                                $totalSaldo = 0;
                                foreach ($saldosEmConta as $saldoEmConta) {

                                    $saldoDoDiaAnterior = $this->financeiro_model->getMovimentadoSaldoAnterior(date('Y-m-d'), $saldoEmConta->movimentadorId);

                                    $totalSaldoDiaAnterior += $saldoDoDiaAnterior;
                                    $totalEntrada += $saldoEmConta->entradas;
                                    $totalSaida += $saldoEmConta->saidas;
                                    $totalSaldo += $saldoEmConta->saldo + $saldoDoDiaAnterior;
                                    ?>
                                    <tr>
                                        <td style="font-weight: bold;"><i
                                                class="fa-fw fa fa-bank nb"></i> <?php echo strtoupper($saldoEmConta->movimentador); ?>
                                        </td>
                                        <td class="right" style="color: blue;">
                                            + <?php echo $this->sma->formatMoney($saldoDoDiaAnterior); ?></td>
                                        <td class="right" style="color: blue;">
                                            + <?php echo $this->sma->formatMoney($saldoEmConta->entradas); ?> C
                                        </td>
                                        <td class="right" style="color: red;">
                                            - <?php echo $this->sma->formatMoney($saldoEmConta->saidas); ?> D
                                        </td>
                                        <td class="right" style="color: blue;">
                                            + <?php echo $this->sma->formatMoney($saldoEmConta->saldo + $saldoDoDiaAnterior); ?>
                                            C
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                <tfoot class="dtFilter">
                                <tr>
                                    <td class="right"><b>TOTAL</b></td>
                                    <td class="right"><?php echo $this->sma->formatMoney($totalSaldoDiaAnterior); ?></td>
                                    <td class="right"><?php echo $this->sma->formatMoney($totalEntrada); ?></td>
                                    <td class="right"><?php echo $this->sma->formatMoney($totalSaida); ?></td>
                                    <td class="right"><?php echo $this->sma->formatMoney($totalSaldo); ?></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#pdf').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=site_url('reports/getSalesReport/pdf/?v=1' . $v)?>";
                return false;
            });
            $('#xls').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=site_url('reports/getSalesReport/0/xls/?v=1' . $v)?>";
                return false;
            });
            $('#image').click(function (event) {
                event.preventDefault();
                html2canvas($('.sales-table'), {
                    onrendered: function (canvas) {
                        var img = canvas.toDataURL()
                        window.open(img);
                    }
                });
                return false;
            });
            $('#pdf1').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=site_url('reports/getPaymentsReport/pdf/?v=1' . $p)?>";
                return false;
            });
            $('#xls1').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=site_url('reports/getPaymentsReport/0/xls/?v=1' . $p)?>";
                return false;
            });
            $('#image1').click(function (event) {
                event.preventDefault();
                html2canvas($('.payments-table'), {
                    onrendered: function (canvas) {
                        var img = canvas.toDataURL()
                        window.open(img);
                    }
                });
                return false;
            });
        });
    </script>
