<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('reembolso'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/reembolso", $attrib); ?>
        <input type="hidden" name="faturaId" value="<?php echo $fatura->id;?>">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("cliente_conta", "pessoa"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-user"  style="font-size: 1.2em;"></i></div>
                                <?php echo form_input('pessoa', $fatura->pessoa, 'id="pessoa" data-placeholder="' . lang("select") . ' ' . lang("pessoa") . '" required="required" readonly class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("data_vencimento", "dtvencimento"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-calendar"  style="font-size: 1.2em;"></i></div>
                                <input type="date" name="dtvencimento" value="<?php echo date('Y-m-d');?>" class="form-control tip" id="dtvencimento" required="required" data-original-title="Data do primeiro vencimento" title="Data do primeiro vencimento">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('tipo_cobranca_reembolso', 'tipo_cobranca'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-barcode"  style="font-size: 1.2em;"></i></div>
                                <?php
                                $cbTipoDeCobranca[''] = '';
                                foreach ($tiposCobranca as $tipoCobranca) {
                                    $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                                }
                                echo form_dropdown('tipocobranca', $cbTipoDeCobranca, $fatura->tipoCobranca, 'id="tipocobranca" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca_financeiro") . '" required="required" style="width:100%;" ');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('condicao_pagamento', 'condicao_pagamento'); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-tags"  style="font-size: 1.2em;"></i></div>
                                <?php
                                $cbCondicoesPagamento[''] = lang('select').' '.lang('condicao_pagamento');
                                foreach ($condicoesPagamento as $condicaoPagamento) {
                                    $cbCondicoesPagamento[$condicaoPagamento->id] = $condicaoPagamento->name;
                                } ?>
                                <?= form_dropdown('condicaopagamento', $cbCondicoesPagamento, $this->Settings->condicaoPagamentoAVista, 'class="form-control tip" id="condicaopagamento" required="required"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("recebido_financeiro", "recebido_financeiro"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-usd"  style="font-size: 1.2em;"></i></div>
                                <input type="text" name="valorpagar" id="valorpagar" value="<?php echo $valorReembolso;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money" readonly required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("acrescimo", "acrescimo"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-usd"  style="font-size: 1.2em;"></i></div>
                                <input type="text" name="acrescimo" id="acrescimo" value="0.00" style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("desconto", "desconto"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-usd"  style="font-size: 1.2em;"></i></div>
                                <input type="text" name="desconto" id="desconto" value="0.00" style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("valor_reembolso", "valorReembolso"); ?>
                            <div class="input-group">
                                <div class="input-group-addon no-print" style="padding: 2px 8px;"><i class="fa fa-usd"  style="font-size: 1.2em;"></i></div>
                                <input type="text" name="valorReembolso" id="valorReembolso" value="<?php echo $valorReembolso;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading" style="text-align: center"><h4><?= lang('parcelas') ?></h4></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-12">
                                    <div class="controls table-controls">
                                        <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th style="text-align: left;">#</th>
                                                <th class="col-md-3" style="text-align: left;">Valor</th>
                                                <th class="col-md-1" style="text-align: left;">Desc.</th>
                                                <th class="col-md-2" style="text-align: left;">Vencimento</th>
                                                <th class="col-md-3" style="text-align: left;">Cobrança</th>
                                                <th class="col-md-3" style="text-align: left;">Conta</th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("attachment", "attachment") ?>
                            <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                                   class="form-control file">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("motivo_reembolso", "note"); ?>
                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" required="required" id="note"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('confirmarReembolso', lang('confirmar_reembolso'), 'id="confirmarReembolso" class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">

    var mask = {
        money: function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"0.0$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"0.$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'.$1');
                }
                return v;
            };

            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(document).ready(function () {

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $('#acrescimo').focus();

        $("#tipocobranca").select2().on("change", function(e) {
            let contapadao =  $("#tipocobranca  option:selected").attr('conta');
            if (contapadao !== '') {
                $('#contadestino').val(contapadao).change();
                $('#contadestino').attr('disabled', true);
            } else {
                $('#contadestino').val('').change();
                $('#contadestino').attr('disabled', false);
            }
            getParcelamento();
        });

        $('#acrescimo').keyup(function (event) {
            atualizarValorReembolso();
            getParcelamento();
        });

        $('#desconto').keyup(function (event) {
            atualizarValorReembolso();
            getParcelamento();
        });

        $('#dtvencimento').blur(function (event) {
            getParcelamento();
        });

        $("#condicaopagamento").select2().on("change", function(e) {
            getParcelamento();
        });

        $('#pessoa').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });

    function getParcelamento() {

        var condicaoPagamento = $("#condicaopagamento  option:selected").val();
        var tipoCobranca = $("#tipocobranca  option:selected").val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: site.base_url + "saleitem/getParcelas",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    dtvencimento: $('#dtvencimento').val(),
                    valorVencimento: $('#valorReembolso').val(),
                    tipoCobrancaId: tipoCobranca,
                },
                dataType: 'html',
                success: function (html) {
                    $('#slTableParcelas tbody').html(html);

                    $(function(){
                        $('.mask_money').bind('keypress',mask.money);
                        $('.mask_money').click(function(){$(this).select();});
                    });
                }
            });
        } else {
            $('#slTableParcelas tbody').html('');
        }
    }

    function atualizarValorVencimentoAoEditarParcela(tag, index) {

        let totalVencimento = 0;
        let valorpagar = $('#valorpagar').val();
        if (valorpagar !== '') valorpagar = parseFloat(valorpagar);

        $('#acrescimo').val('0.00');
        $('#desconto').val('0.00');

        $("input[name='valorVencimentoParcela[]']").each(function(){
            var valorVencimento = $(this).val();
            if (valorVencimento !== '')  totalVencimento = totalVencimento + parseFloat(valorVencimento);
        });

        if (totalVencimento > valorpagar) $('#acrescimo').val((totalVencimento-valorpagar).toFixed(2));
        else if (totalVencimento < valorpagar) $('#desconto').val((valorpagar-totalVencimento).toFixed(2));

        atualizarValorReembolso();
    }

    function atualizarValorReembolso() {

        let valorpagar = $('#valorpagar').val();
        let acrescimo = $('#acrescimo').val();
        let desconto = $('#desconto').val();

        if (valorpagar !== '') valorpagar = parseFloat(valorpagar);
        if (acrescimo !== '') acrescimo = parseFloat(acrescimo);
        if (desconto !== '') desconto = parseFloat(desconto);

        $('#valorReembolso').val((valorpagar+acrescimo-desconto).toFixed(2));
    }

</script>
