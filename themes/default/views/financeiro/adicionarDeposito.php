<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_deposito'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/adicionarDeposito/", $attrib); ?>
        <div class="modal-body">
            <div id="payments">
                <div class="well well-sm well_1">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="form-group">
                                        <?= lang("warehouse", "slwarehouse"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-building"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <?php
                                            $wh[''] = '';
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh,  $Settings->default_warehouse, 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'slwarehouse',
                                        'value' => $this->session->userdata('warehouse_id'),
                                    );
                                    echo form_input($warehouse_input);
                                    ?>
                                <?php } ?>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= lang('conta_origem', 'contaOrigem'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?php
                                        $cbContas[''] = lang('select') . ' ' . lang('conta_origem');
                                        foreach ($movimentadores as $movimentador) {
                                            $cbContas[$movimentador->id] = $movimentador->name;
                                        }
                                        ?>
                                        <?= form_dropdown('contaOrigem', $cbContas, set_value('contaOrigem'), 'class="form-control" required="required" id="contaOrigem"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("date", "date"); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("valor_deposito", "valorDeposito"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <input name="valorDeposito" style="padding: 5px;" type="text" id="valorDeposito"
                                                   class="pa form-control kb-pad amount mask_money" required="required"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('adicionarDeposito', lang('adicionar_deposito'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }

        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
    });
</script>
