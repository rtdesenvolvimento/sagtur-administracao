<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#receitas" class="tab-grey" style="text-align: center;"><i class="fa fa-sign-in" style="font-size: 20px;"></i><br/>
            <?= lang('contas_receber') ?></a></li>
 </ul>
<div class="tab-content">
    <div id="receitas" class="tab-pane fade in">
        <?php
        $v = "&customer=" . $user_id;
        if ($this->input->post('submit_sale_report')) {

            if ($this->input->post('biller'))  $v .= "&biller=" . $this->input->post('biller');
            if ($this->input->post('user')) $v .= "&user=" . $this->input->post('user');
            if ($this->input->post('serial')) $v .= "&serial=" . $this->input->post('serial');
            if ($this->input->post('start_date')) $v .= "&start_date=" . $this->input->post('start_date');
            if ($this->input->post('end_date')) $v .= "&end_date=" . $this->input->post('end_date');
            if ($this->input->post('filterFilial')) $v .= "&filterFilial=" . $this->input->post('filterFilial');
            if ($this->input->post('filterTipoCobranca')) $v .= "&filterTipoCobranca=" . $this->input->post('filterTipoCobranca');
            if ($this->input->post('filterReceita')) $v .= "&filterReceita=" . $this->input->post('filterReceita');
            if ($this->input->post('filterCondicaoPagamento')) $v .= "&filterCondicaoPagamento=" . $this->input->post('filterCondicaoPagamento');
            if ($this->input->post('filterCliente')) $v .= "&filterCliente=" . $this->input->post('filterCliente');
            if ($this->input->post('filterStatus')) $v .= "&filterStatus=" . $this->input->post('filterStatus');
            if ($this->input->post('data_pagamento_de')) $v .= "&data_pagamento_de=" . $this->input->post('data_pagamento_de');
            if ($this->input->post('data_pagamento_ate')) $v .= "&data_pagamento_ate=" . $this->input->post('data_pagamento_ate');
            if ($this->input->post('filter_numero_documento')) $v .= "&filter_numero_documento=" . $this->input->post('filter_numero_documento');
            if ($this->input->post('filterMovimentador')) $v .= "&filterMovimentador=" . $this->input->post('filterMovimentador');
            if ($this->input->post('filterProgramacao')) $v .= "&filterProgramacao=" . $this->input->post('filterProgramacao');
            if ($this->input->post('filterCategoria')) $v .= "&filterCategoria=" . $this->input->post('filterCategoria');
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#form').hide();
                $('.toggle_down').click(function () {
                    $("#form").slideDown();
                    return false;
                });
                $('.toggle_up').click(function () {
                    $("#form").slideUp();
                    return false;
                });
            });
        </script>
        <div class="box sales-table">
            <div class="box-header">
                <h2 class="blue">Parcelas (<?php echo count($receitas)?>) <small>Gerencie aqui as parcelas <?php
                    if ($this->input->post('start_date')) {
                        echo " com vencimento de " . date('d/m/Y', strtotime($this->input->post('start_date'))) . " Até " . date('d/m/Y', strtotime($this->input->post('end_date')));
                    }
                    ?>
                    </small>
                </h2>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                            <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                <li>
                                    <a href="<?=site_url('financeiro/adicionarContaReceber')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-arrow-circle-o-up"></i> <?=lang('adicionar_conta_receber')?>
                                    </a>
                                </li>
                                <!--
                                <li class="divider"></li>
                                <li>
                                    <a href="<?=site_url('financeiro/adicionarTransferencia')?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-exchange"></i> <?=lang('adicionar_transferencia')?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=site_url('financeiro/adicionarSaque')?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-arrow-up"></i> <?=lang('adicionar_saque')?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?=site_url('financeiro/adicionarDeposito')?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <i class="fa fa-arrow-down"></i> <?=lang('adicioar_deposito')?>
                                    </a>
                                </li>
                                !-->
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="box-icon">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                                <i class="icon fa fa-search-minus"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                                <i class="icon fa fa-search-plus"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="box-icon" style="display: none;">
                    <ul class="btn-tasks">
                        <li class="dropdown">
                            <a href="#" id="pdf" class="tip" title="<?= lang('download_pdf') ?>">
                                <i class="icon fa fa-file-pdf-o"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                <i class="icon fa fa-file-excel-o"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div id="form">
                            <?php echo form_open("financeiro/receitasDURAV/" . $user_id); ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                        <?php
                                        $wh[""] = lang('select').' '.lang('warehouse');
                                        foreach ($warehouses as $warehouse) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('filterFilial', $wh, (isset($_POST['filterFilial']) ? $_POST['filterFilial'] : ""), 'class="form-control" id="filterFilial" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="filterProgramacao"><?= lang("product"); ?></label>
                                        <?php
                                        $cbProgramacao[""] = lang('select').' '.lang('product');
                                        foreach ($programacoes as $programacao) {
                                            $label =  $this->sma->hrsd($programacao->dataSaida). ' - '.$programacao->name;
                                            $cbProgramacao[$programacao->programacaoId] = $label;
                                        }
                                        echo form_dropdown('filterProgramacao', $cbProgramacao, (isset($_POST['filterProgramacao']) ? $_POST['filterProgramacao'] : ""), 'class="form-control" id="filterProgramacao" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="filterCategoria"><?= lang("category"); ?></label>
                                        <?php
                                        $bCategoria[""] = lang('select').' '.lang('category');
                                        foreach ($categories as $category) {
                                            $bCategoria[$category->id] = $category->name;
                                        }
                                        echo form_dropdown('filterCategoria', $bCategoria, (isset($_POST['filterCategoria']) ? $_POST['filterCategoria'] : ""), 'class="form-control" id="filterCategoria" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("category") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4" style="display: none;">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                        <?php
                                        $us[""] = lang('select').' '.lang('user');
                                        foreach ($users as $user) {
                                            $us[$user->id] = $user->first_name . " " . $user->last_name;
                                        }
                                        echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                        <?php
                                        $bl[""] = lang('select').' '.lang('biller');
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("condicao_pagamento"); ?></label>
                                        <?php
                                        $cps[""] = lang('select').' '.lang('condicao_pagamento');
                                        foreach ($condicoesPagamento as $condicaoPagamento) {
                                            $cps[$condicaoPagamento->id] = $condicaoPagamento->name;
                                        }
                                        echo form_dropdown('filterCondicaoPagamento', $cps, (isset($_POST['filterCondicaoPagamento']) ? $_POST['filterCondicaoPagamento'] : ""), 'class="form-control" id="filterCondicaoPagamento" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("condicao_pagamento") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("receita"); ?></label>
                                        <select class="form-control tip" name="filterReceita" id="filterReceita">
                                            <option value=""><?php echo lang('select').' '.lang('receita');?></option>
                                            <?php
                                            foreach ($planoReceitas as $objReceita) {?>
                                                <optgroup label="<?php echo $objReceita->name;?>">
                                                    <?php
                                                    $receitasFilhas = $this->financeiro_model->getReceitaByReceitaSuperiorId($objReceita->id);
                                                    foreach ($receitasFilhas as $item) {?>
                                                        <?php if ($receita == $item->id ) {?>
                                                            <option  selected="selected" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } ?>
                                                    <?php }?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("movimentador"); ?></label>
                                        <?php
                                        $mvs[""] = lang('select').' '.lang('movimentador');
                                        foreach ($movimentadores as $movimentador) {
                                            $mvs[$movimentador->id] = $movimentador->name;
                                        }
                                        echo form_dropdown('filterMovimentador', $mvs, (isset($_POST['filterMovimentador']) ? $_POST['filterMovimentador'] : ""), 'class="form-control" id="filterMovimentador" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("movimentador") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                        <?php
                                        $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                        foreach ($tiposCobranca as $tipoCobranca) {
                                            $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                        }
                                        echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : ""), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("cliente_conta", "cliente"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-user"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <?php
                                            echo form_input('filterCliente', (isset($_POST['filterCliente']) ? $_POST['filterCliente'] : ""), 'id="filterCliente" data-placeholder="' . lang("select") . ' ' . lang("cliente") . '" class="form-control input-tip" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("data_vencimento_de", "start_date"); ?>
                                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("data_vencimento_ate", "end_date"); ?>
                                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("data_pagamento_de", "data_pagamento_de"); ?>
                                        <?php echo form_input('data_pagamento_de', (isset($_POST['data_pagamento_de']) ? $_POST['data_pagamento_de'] : ''), 'class="form-control" id="data_pagamento_de"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <?= lang("data_pagamento_ate", "data_pagamento_ate"); ?>
                                        <?php echo form_input('data_pagamento_ate', (isset($_POST['data_pagamento_ate']) ? $_POST['data_pagamento_ate'] : ''), 'type="date" class="form-control" id="data_pagamento_ate"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="user"><?= lang("status"); ?></label>
                                        <?php
                                        $opts = array(
                                            '' => lang('select').' '.lang('status'),
                                            'ABERTA' => lang('ABERTA'),
                                            'PARCIAL' => lang('PARCIAL'),
                                            'VENCIDA' => lang('VENCIDA'),
                                            'QUITADA' => lang('QUITADA'),
                                            'REEMBOLSO' => lang('REEMBOLSO'),
                                        );
                                        echo form_dropdown('filterStatus', $opts, (isset($_POST['filterStatus']) ? $_POST['filterStatus'] : ""), 'class="form-control" id="filterStatus" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("status") . '"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("numero_documento", "filter_numero_documento") ?>
                                        <?= form_input('filter_numero_documento', (isset($_POST['filter_numero_documento']) ? $_POST['filter_numero_documento'] : ''), 'class="form-control" id="filter_numero_documento"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="controls">
                                    <?php echo form_submit('submit_sale_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <style>
                                th {
                                    background: #e9ebec;
                                    color: #000;
                                    font-weight: bold;
                                    font-size: 12px;
                                }
                                tr {
                                    border-left: solid 2px #428bca !important;
                                }
                            </style>
                            <table class="table-for-extrato table-hover" style="cursor: pointer;">
                                <thead>
                                <tr class="tr-border-left-color-blue">
                                    <th style="padding: 15px;"></th>
                                    <th style="text-align: left;" class="col-md-5"><?= lang("pessoa"); ?></th>
                                    <th style="text-align: left;" class="col-md-2"><?= lang("categoria"); ?></th>
                                    <th style="text-align: center;display: none;"><?= lang("tipo_cobranca"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("valor_vencimento"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("acres"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("desc"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("pagar_financeiro"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("taxas"); ?></th>
                                    <th style="text-align: right;" class="col-md-1"><?= lang("recebido_financeiro"); ?></th>
                                    <th style="text-align: center;" class="col-md-1"><?= lang("status"); ?></th>
                                    <th><?= lang("actions"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $total = 0;
                                $totalPago = 0;
                                $totalPagar = 0;
                                $totalAcrescimo = 0;
                                $totalDesconto = 0;
                                $totalTaxas = 0;

                                $totalDia = 0;
                                $totalPagoDia = 0;
                                $totalPagarDia = 0;
                                $totalTaxasDia = 0;
                                $totalDescontoDia = 0;
                                $totalAcrescimoDia = 0;

                                $dataVencimentoAtual = '';
                                $contador = 0;
                                $totalLinhas = 0;

                                if ($receitas) {
                                    foreach ($receitas as $fatura) {

                                        $isExibirOperacoesDaFatura = $this->financeiro_model->isExibirPagamentoConta($fatura->status);

                                        $parcela = $this->financeiro_model->getParcelaOneByFatura($fatura->id);
                                        $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fatura->id);

                                        $itens =  $this->sales_model->getSaleItemByContaReceber($fatura->contas_receber);
                                        $contaReceber = $this->FinanceiroRepository->getContaReceberById($fatura->contas_receber);

                                        $total += $fatura->valorfatura;
                                        $totalPago += $fatura->valorpago;
                                        $totalPagar += $fatura->valorpagar;
                                        $totalTaxas += $fatura->taxas;

                                        $totalAcrescimo += $fatura->totalAcrescimo;
                                        $totalDesconto += $fatura->totalDesconto;
                                        ?>
                                        <?php if ($dataVencimentoAtual != $fatura->dtvencimento) {?>
                                            <?php if ($contador > 0){?>
                                                 <tr style="border-left: solid 2px #f2f2f2 !important; ">
                                                     <th></th>
                                                     <th></th>
                                                     <th></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalDia);?></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalAcrescimoDia);?></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalDescontoDia);?></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalPagarDia);?></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalTaxasDia);?></th>
                                                     <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalPagoDia);?></th>
                                                     <th></th>
                                                     <th></th>
                                                     <th></th>
                                                 </tr>
                                            <?php }?>
                                            <tr><td></td></tr>
                                            <tr style="border-left: solid 5px rgb(255 255 255) !important;">
                                                <td colspan="11" style="font-size: 14px;font-weight: bold;">
                                                    <i class="fa fa-minus-square-o" style="margin-top: 15px;margin-left: 5px;margin-bottom: 15px;"></i> <span style="margin-left: 10px;"><?php echo $this->sma->hrsd($fatura->dtvencimento)?> </span>
                                                </td>
                                            </tr>
                                            <?php
                                            $totalDia = $fatura->valorfatura;
                                            $totalPagoDia = $fatura->valorpago;
                                            $totalPagarDia = $fatura->valorpagar;
                                            $dataVencimentoAtual = $fatura->dtvencimento;
                                            $totalTaxasDia = $fatura->taxas;
                                            $totalDescontoDia = $fatura->totalDesconto;
                                            $totalAcrescimoDia = $fatura->totalAcrescimo;
                                            ?>
                                        <?php } else {?>
                                            <?php
                                            $totalDia += $fatura->valorfatura;
                                            $totalPagoDia += $fatura->valorpago;
                                            $totalPagarDia += $fatura->valorpagar;
                                            $totalTaxasDia = $fatura->taxas;
                                            $totalDescontoDia = $fatura->totalDesconto;
                                            $totalAcrescimoDia = $fatura->totalAcrescimo;
                                            ?>
                                        <?php } ?>
                                        <tr class="border-top-dotted tr-border-left-color-blue">
                                            <td style="font-size: 20px;text-align: center;padding: 15px;">
                                                <i class="fa fa-newspaper-o"></i>
                                            </td>
                                            <td>
                                                <small>
                                                    <b style="font-size: 10px;">  <?php echo $fatura->reference.''. ($fatura->numero_documento != '' ? ' nº doc:. '.$fatura->numero_documento : '') ?></b>
                                                    <?php if (count($cobranca) > 0) {?>
                                                        <br/><i class="fa fa-barcode"></i><?php echo ' Cod: '.$cobranca->code;?>
                                                    <?php } ?>
                                                </small><br/>
                                                <span style="font-weight: bold;font-size: 12px;" class="title-for-extrato">
                                                    <?php echo $fatura->nomeCliente?>
                                                </span>
                                                <?php foreach ($itens as $item){?>
                                                    <br/><i class="fa fa-map"></i><?php echo ' '.$item->product_name;?>
                                                <?php }?><br/>
                                                <i class="fa fa-bank"></i> <?php echo  $fatura->tipocobranca.' '.$fatura->movimentador;?>
                                            </td>
                                            <td style="text-align: left;padding: 5px;"><?php echo $fatura->receita?></td>
                                            <td style="text-align: center;padding: 5px;display: none;"><?php echo $fatura->tipocobranca; ?></td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;">
                                                <?php echo $this->sma->formatMoney($fatura->valorfatura);?>
                                                <br/><small><?php echo '('.$parcela->numeroparcela.' / '.$parcela->totalParcelas.')'; ?></small>
                                            </td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;"><?php echo $this->sma->formatMoney($fatura->totalAcrescimo);?></td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;"><?php echo $this->sma->formatMoney($fatura->totalDesconto);?></td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;">
                                                <?php echo $this->sma->formatMoney($fatura->valorpagar); ?>
                                                <?php echo $this->sma->mostrarDiasVencimento($fatura->status, $fatura->dtvencimento);?>
                                            </td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;<?php if ($fatura->taxas>0) echo 'color: red'?>;"><?php echo $this->sma->formatMoney($fatura->taxas);?></td>
                                            <td style="text-align: right;font-weight: bold;padding: 5px;">
                                                <?php echo $this->sma->formatMoney($fatura->valorpago);?><br/>
                                                <?php if ($fatura->dtultimopagamento != null){?>
                                                    <small>&nbsp;<?php echo $this->sma->hrsd($fatura->dtultimopagamento);?></small>
                                                <?php }?>
                                            </td>
                                            <td style="text-align: center;padding: 5px;"><?php echo $this->sma->billing_status_design($fatura->status, $fatura->dtvencimento)?></td>
                                            <td class="acoes" style="padding: 5px;">
                                                <div class="text-center">
                                                    <div class="btn-group text-left">
                                                        <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                            Ações <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right" role="menu">

                                                            <li><a href="<?php echo base_url();?>customers/view/<?php echo $fatura->pessoa;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-user"></i> Visualizar Cadastro do Cliente</a></li>
                                                            <li><a href="<?php echo base_url();?>financeiro/historico/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i><?php echo lang('historico_de_parcelas')?></a></li>

                                                            <?php if($contaReceber->sale > 0){?>
                                                                <li><a href="<?php echo base_url();?>sales/edit/<?php echo $contaReceber->sale;?>"><i class="fa fa-edit"></i> Editar Venda</a></li>
                                                                <li><a href="<?php echo base_url();?>salesutil/modal_view/<?php echo $contaReceber->sale;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-ticket"></i> Visualizar Venda</a></li>
                                                                <li><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $contaReceber->sale;?>"><i class="fa fa-download"></i> <?php echo lang('download_voucher')?></a></li>
                                                            <?php }?>

                                                            <li style="display: none;"><a href="<?php echo base_url();?>reports/customer_report/<?php echo $fatura->pessoa;?>" target="<?php echo $fatura->pessoa;?>"><i class="fa fa-tasks"></i> Visualizar Histórico do Cliente</a></li>

                                                            <?php if (count($cobranca) > 0 && $isExibirOperacoesDaFatura) {?>
                                                                <li class="divider"></li>
                                                                <?php if ($cobranca->link){
                                                                    if ($fatura->whatsapp) $wz = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text=*Segue%20o%20Boleto*:%0A'.$cobranca->link;
                                                                    else  $wz = 'https://api.whatsapp.com/send?text=*Segue%20o%20Boleto*:%0A'.$cobranca->link;
                                                                    ?>
                                                                    <li><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Imprimir Boleto</a></li>
                                                                    <li><a href="<?php echo $wz?>" target="<?php echo $fatura->id;?>"><i class="fa fa-whatsapp"></i> Enviar boleto pelo Whastapp</a></li>
                                                                <?php } ?>

                                                                <?php
                                                                    if ($fatura->whatsapp) $wz = 'https://api.whatsapp.com/send?phone=55'.$fatura->whatsapp.'&text=*Segue%20o%20Link de pagamento*:%0A'.$cobranca->checkoutUrl;
                                                                    else  $wz = 'https://api.whatsapp.com/send?text=*Segue%20o%20Link de pagamento*:%0A'.$cobranca->checkoutUrl;
                                                                ?>
                                                                <?php if ($cobranca->checkoutUrl) {
                                                                    $wz = 'https://api.whatsapp.com/send?text=*Segue%20o%20Boleto*:%0A'.$cobranca->checkoutUrl;
                                                                    ?>
                                                                    <li><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-link"></i> Visualizar Link de Pagamento</a></li>
                                                                    <li><a href="<?php echo $wz?>" target="<?php echo $fatura->id;?>"><i class="fa fa-whatsapp"></i> Enviar link de pagamento Whastapp</a></li>
                                                                <?php } ?>
                                                            <?php } ?>
                                                            <li class="divider"></li>
                                                            <li><a href="<?php echo base_url();?>financeiro/pagamentos/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Ver Pagamentos</a></li>

                                                            <?php if ($isExibirOperacoesDaFatura) {?>
                                                                <li><a href="<?php echo base_url();?>faturas/adicionarPagamento/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-money"></i> Adicionar Pagamento</a></li>
                                                            <?php }?>

                                                            <!--<li><a href="<?php echo base_url();?>sales/view/<?php echo $fatura->id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Fatura</a></li>!-->
                                                            <li style="display: none;"><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $fatura->id;?>"><i class="fa fa-file-pdf-o"></i> Baixar Fatura como PDF</a></li>
                                                            <!--<li><a href="<?php echo base_url();?>sales/email/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> Envia Fatura por E-mail</a></li>!-->

                                                            <li class="divider"></li>
                                                            <li style="display: none;"><a href="<?php echo base_url();?>financeiro/negociarContaReceber/<?php echo $fatura->pessoa;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-barcode"></i><?php echo lang('segunda_via')?></a></li>
                                                            <?php if ($isExibirOperacoesDaFatura) {?>
                                                                <li><a href="<?php echo base_url();?>faturas/editarFatura/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-edit"></i><?php echo lang('editar_fatura')?></a></li>
                                                                <li style="display: none;"><a href="<?php echo base_url();?>financeiro/negociarContaReceberContrato/<?php echo $fatura->id.'/'.$fatura->pessoa;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-circle-o"></i><?php echo lang('negociar_divida_contrato')?></a></li>
                                                            <?php }?>
                                                            <li style="display: none;"><a href="<?php echo base_url();?>financeiro/negociarContaReceber/<?php echo $fatura->pessoa;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-recycle"></i><?php echo lang('negociar_divida')?></a></li>

                                                            <?php if ($isExibirOperacoesDaFatura) {?>
                                                                <li class="divider"></li>
                                                                <?php if ($fatura->status == Financeiro_model::STATUS_PARCIAL ||
                                                                    $fatura->status == Financeiro_model::STATUS_PAGO ||
                                                                    $fatura->status == Financeiro_model::STATUS_VENCIDA){?>
                                                                    <li><a href="<?= site_url('financeiro/reembolso/'.$fatura->id) ?>" data-toggle="modal" data-target="#myModal4"><i class="fa fa-angle-double-left"></i> Reembolso</a></li>
                                                                <?php }?>
                                                                <li><a href="<?= site_url('financeiro/motivoCancelamento/'.$fatura->id) ?>" data-toggle="modal" data-target="#myModal4"><i class="fa fa-creative-commons"></i> Cancelar Cobrança</a></li>
                                                            <?php } else {?>
                                                                <?php if ($fatura->status == Financeiro_model::STATUS_PARCIAL ||
                                                                    $fatura->status == Financeiro_model::STATUS_PAGO ||
                                                                    $fatura->status == Financeiro_model::STATUS_VENCIDA){?>
                                                                    <li class="divider"></li>
                                                                    <li><a href="<?= site_url('financeiro/reembolso/'.$fatura->id) ?>" data-toggle="modal" data-target="#myModal4"><i class="fa fa-angle-double-left"></i> Reembolso</a></li>
                                                                <?php }?>
                                                            <?php }?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <?php if ($contador == count($faturas)-1){?>
                                            <tr style="border-left: solid 2px #f2f2f2 !important;">
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalDia);?></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalAcrescimo);?></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalDesconto);?></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalPagarDia);?></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalTaxas);?></th>
                                                <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalPagoDia);?></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        <?php }?>
                                    <?php $contador++; }?>
                                <?php } else {?>
                                    <tr>
                                        <td colspan="11" style="text-align: center;">Nenhum lançamento foi encontrado</td>
                                    </tr>
                                <?php }?>
                                </tbody>
                                <tfoot>
                                <tr class="border-buttom-dotted" style="border-left: solid 2px #f2f2f2 !important;">
                                    <th></th>
                                    <th></th>
                                    <th class="title-for-totalizador-extato">TOTAL</th>
                                    <th class="title-for-totalizador-extato right"><?php echo $this->sma->formatMoney($total);?></th>
                                    <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalAcrescimo);?></th>
                                    <th class="title-for-totalizador-extato-data right"><?php echo $this->sma->formatMoney($totalDesconto);?></th>
                                    <th class="title-for-totalizador-extato right"><?php echo $this->sma->formatMoney($totalPagar);?></th>
                                    <th class="title-for-totalizador-extato right"><?php echo $this->sma->formatMoney($totalTaxas);?></th>
                                    <th class="title-for-totalizador-extato right"><?php echo $this->sma->formatMoney($totalPago);?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getSalesReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.sales-table'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });
        $('#pdf1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/pdf/?v=1'.$p)?>";
            return false;
        });
        $('#xls1').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=site_url('reports/getPaymentsReport/0/xls/?v=1'.$p)?>";
            return false;
        });
        $('#image1').click(function (event) {
            event.preventDefault();
            html2canvas($('.payments-table'), {
                onrendered: function (canvas) {
                    var img = canvas.toDataURL()
                    window.open(img);
                }
            });
            return false;
        });

        $('#filterCliente').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestionsAll",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });
</script>