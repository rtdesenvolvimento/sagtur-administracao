<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user" style="font-size: 1.2em;"></i>  <?php echo $pessoa->name;?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/negociarContaReceberContrato/".$faturaId, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-success" id="divParcelasAhSelecionar">
                            <div class="panel-heading"><?= lang('selecione_parcelas_reparcelar_info') ?></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-12">
                                    <div class="controls table-controls">
                                        <table id="slTableParcelasRenegociacao" class="table items table-striped table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th style="text-align: center;"><input type="checkbox" id="checkbox-all" class="checkbox-callback-all"/></th>
                                                <th style="text-align: left;" ><?= lang("categoria"); ?></th>
                                                <th style="text-align: center;"><?= lang('vencimento') ?></th>
                                                <th style="text-align: center;"><?= lang('status') ?></th>
                                                <th style="text-align: center;"><?= lang('pago_financeiro') ?></th>
                                                <th style="text-align: center;"><?= lang('multa') ?></th>
                                                <th style="text-align: center;"><?= lang('juros') ?></th>
                                                <th style="text-align: center;"><?= lang('valor_pagar') ?></th>
                                                <th style="text-align: center;"><?= lang('tipo_cobranca') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $totalValorVencimento = 0;
                                            $totalValorPago = 0;
                                            $totalMulta = 0;
                                            $totalJuros = 0;
                                            $totalValorPagar = 0;

                                            foreach ($parcelas as $parcela){

                                                $fatura = $this->financeiro_model->getParcelaFaturaByParcela($parcela->id);

                                                $jurosMulta = $this->sma->getValorVencimentoComMultaJuros($parcela->valorpagar, $parcela->dtvencimento);
                                                $multa = $this->sma->getValorMulta($parcela->valorpagar, $parcela->dtvencimento);
                                                $juros = $this->sma->getJurosMora($parcela->valorpagar, $parcela->dtvencimento);

                                                $totalValorVencimento += $parcela->valorpagar;
                                                $totalValorPago += $parcela->valorpago;
                                                $totalMulta += $multa;
                                                $totalJuros += $juros;
                                                $totalValorPagar += $jurosMulta;

                                                ?>
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <input type="checkbox" name="parcelas[]" value="<?php echo $parcela->id;?>" class="checkbox-callback"/>
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <small><b style="font-size: 11px;"><i class="fa fa-newspaper-o"></i> <?php echo $fatura->reference?></b></small><br/>
                                                        <?php echo $parcela->receita;?><br/>
                                                        <small><?php echo $parcela->note;?></small>
                                                    </td>
                                                    <td style="text-align: center;"><?php echo  $this->sma->hrsd($parcela->dtvencimento);?></td>
                                                    <td style="text-align: center;"><?php echo $this->sma->billing_status_design($parcela->status, $parcela->dtvencimento)?></td>
                                                    <td style="text-align: right;">
                                                        <?php echo $this->sma->formatMoney($parcela->valorpago);?>
                                                        <?php echo $this->sma->mostrarDiasVencimento($parcela->status, $parcela->dtvencimento);?>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <small>
                                                            <?php echo $this->sma->formatMoney($multa);?>
                                                        </small>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <?php echo$this->sma->formatMoney($juros);?>
                                                    </td>
                                                    <td style="text-align: right;">
                                                        <?php echo $this->sma->formatMoney($jurosMulta);?>
                                                        <br/><small><?php echo '('.$parcela->numeroparcela.' / '.$parcela->totalParcelas.')';?></small>
                                                        <input type="hidden" value="<?php echo $jurosMulta;?>" name="valorRenegociar[]"/>
                                                    </td>
                                                    <td style="text-align: center;"><?php echo $this->sma->forma_pagamento_design($parcela->tipocobrancaTipo, $parcela->tipocobranca, $parcela->status, $parcela->dtvencimento); ?></td>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td class="right"><?php echo $this->sma->formatMoney($totalValorPago);?></td>
                                                <td class="right"><?php echo $this->sma->formatMoney($totalMulta);?></td>
                                                <td class="right"><?php echo $this->sma->formatMoney($totalJuros);?></td>
                                                <td class="right"><?php echo $this->sma->formatMoney($totalValorPagar);?></td>
                                                <td></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer center">
                            <button type="button"  id="btnSelecionarParcelas" style="display:none;" class="btn btn-primary"><?php echo lang('selecionar_parcelas');?></button>
                            <button type="button"  id="btnNegociarParcelas" disabled class="btn btn-success"><?php echo lang('renegociar_parcelas_selecionadas');?></button>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div id="divReparcelamento" style="display: none;">
                        <div class="col-md-12 well">
                            <div class="col-md-12">
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="form-group" style="display: none;">
                                        <?= lang("warehouse", "slwarehouse"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-building"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <?php
                                            $wh[''] = '';
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, $Settings->default_warehouse, 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" readonly style="width:100%;" ');
                                            ?>
                                        </div>
                                    </div>
                                <?php } else {
                                    $warehouse_input = array(
                                        'type' => 'hidden',
                                        'name' => 'warehouse',
                                        'id' => 'slwarehouse',
                                        'value' => $Settings->default_warehouse,
                                    );
                                    echo form_input($warehouse_input);
                                    ?>
                                <?php } ?>
                            </div>
                            <div class="col-md-12" style="display: none;">
                                <div class="form-group">
                                    <?= lang("cliente_conta", "pessoa"); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-user"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?php
                                        echo form_input('pessoa', (isset($_POST['pessoa']) ? $_POST['pessoa'] : ""), 'id="pessoa" data-placeholder="' . lang("select") . ' ' . lang("pessoa") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang('receita', 'receita'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <select class="form-control tip" name="receita" id="receita" readonly required="required">
                                            <?php
                                            $receitaRenegociacao = $this->financeiro_model->getReceitaById($this->Settings->receitaNecociacao);
                                            if (count($receitaRenegociacao) > 0) {?>
                                                <option value="<?php echo $receitaRenegociacao->id;?>"><?php echo $receitaRenegociacao->name;?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("data_vencimento", "dtvencimento"); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <input type="date" name="dtvencimento" value="<?php echo date('Y-m-d');?>" class="form-control tip" id="dtvencimento" required="required" data-original-title="Data do primeiro vencimento" title="Data do primeiro vencimento">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang('tipo_cobranca_financeiro', 'tipo_cobranca'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-barcode"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <select class="form-control tip" id="tipocobranca" name="tipocobranca" required="required">
                                            <option value=""><?php echo lang('select').' '.lang('tipo_cobranca_financeiro')?></option>
                                            <?php foreach ($tiposCobranca as $tipoCobranca) {?>
                                                <option value="<?php echo $tipoCobranca->id;?>" conta="<?php echo $tipoCobranca->conta;?>"><?php echo $tipoCobranca->name;?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("valor_vencimento", "valor"); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <input type="text" name="valor" id="valor" value="0.00" style="padding: 5px;" readonly class="pa form-control kb-pad mask_money" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("desconto", "desconto"); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <input type="text" name="desconto" id="desconto" value="0.00" style="padding: 5px;" required="required" class="pa form-control kb-pad mask_money"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang('condicao_pagamento', 'condicao_pagamento'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-tags"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?php
                                        $cbCondicoesPagamento[''] = lang('select').' '.lang('condicao_pagamento');
                                        foreach ($condicoesPagamento as $condicaoPagamento) {
                                            $cbCondicoesPagamento[$condicaoPagamento->id] = $condicaoPagamento->name;
                                        } ?>
                                        <?= form_dropdown('condicaopagamento', $cbCondicoesPagamento, set_value('condicaopagamento'), 'class="form-control tip" id="condicaopagamento" required="required"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2" style="display: none;">
                                <div class="form-group">
                                    <?= lang("pagar_agora", "recebido"); ?>
                                    <?php
                                    $opts = array(
                                        'no' => lang('no'),
                                        'sim' => lang('yes'),
                                    );
                                    echo form_dropdown('recebido', $opts, (isset($_POST['recebido']) ? $_POST['recebido'] :  '') , ' class="form-control" id="recebido" required="required"');
                                    ?>
                                </div>
                            </div>
                            <div id="div-receber-agora" style="display: none;">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("valor_entrada", "valorentrada"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <input name="valorentrada" type="text" id="valorentrada" required="required" value="" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("data_pagamento_entrada", "dtParamentoEntrada"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <input type="date" name="dtParamentoEntrada" value="<?php echo date('Y-m-d');?>" required="required" class="form-control tip" id="dtParamentoEntrada" data-original-title="Data do pagamento da entrada" title="Data do pagamento da entrada">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('conta_destino', 'conta_destino'); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <?php
                                            $cbContas[''] = lang('select').' '.lang('conta_destino');
                                            foreach ($movimentadores as $movimentador) {
                                                $cbContas[$movimentador->id] = $movimentador->name;
                                            } ?>
                                            <?= form_dropdown('contadestino', $cbContas, set_value('contadestino'), 'class="form-control" id="contadestino"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 well">
                            <div class="panel panel-success">
                                <div class="panel-heading" style="text-align: center"><?= lang('parcelas') ?></div>
                                <div class="panel-body" style="padding: 5px;">
                                    <div class="col-md-12">
                                        <div class="controls table-controls">
                                            <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                                <thead>
                                                <tr>
                                                    <th style="text-align: left;">#</th>
                                                    <th class="col-md-4" style="text-align: left;">Valor</th>
                                                    <th class="col-md-1" style="text-align: left;">Desc.</th>
                                                    <th class="col-md-4" style="text-align: left;">Vencimento</th>
                                                    <th class="col-md-6" style="text-align: left;">Tipo</th>
                                                </tr>
                                                </thead>
                                                <tbody></tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("attachment", "attachment") ?>
                                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                                       class="form-control file">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("note", "note"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="display: none" id="divConfirmarReparelamento">
                <?php echo form_submit('adicionarContaReceber', lang('confirmar_renegociacao'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>

<script type="text/javascript" charset="UTF-8">

    var mask = {
        money: function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"0.0$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"0.$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'.$1');
                }
                return v;
            };

            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(document).ready(function () {

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;

        $('#recebido').select2().on("change", function(e) {
            let contapadao =  $("#recebido  option:selected").val();
            if (contapadao === 'sim') {
                $('#div-receber-agora').show();
                $('#valorentrada').attr('required','required');
            } else {
                $('#div-receber-agora').hide();
                $('#valorentrada').removeAttr('required');
            }
        });

        $("#tipocobranca").select2().on("change", function(e) {
            let contapadao =  $("#tipocobranca  option:selected").attr('conta');
            if (contapadao !== '') {
                $('#contadestino').val(contapadao).change();
                $('#contadestino').attr('disabled', true);
            } else {
                $('#contadestino').val('').change();
                $('#contadestino').attr('disabled', false);
            }
            getParcelamento();
        });

        $('#valor').blur(function (event) {
            $('#valorentrada').val($(this).val());
            getParcelamento();
        });

        $('#dtvencimento').blur(function (event) {
            getParcelamento();
        });

        $("#condicaopagamento").select2().on("change", function(e) {
            getParcelamento();
        });

        $('#desconto').blur(function (event) {

            let desconto = $(this).val();
            let valor = $('#valor').val();

            if (desconto === '') desconto = 0;
            else desconto = parseFloat(desconto);

            if (valor === '') valor = 0;
            else valor = parseFloat(valor);

            if (desconto > valor) {
                alert('O Valor do desconto não pode ser maior que o valor do vencimento da conta');
                $(this).val('0.00');
            }
            getParcelamento();
        });

        $('#valorentrada').blur(function (event) {
            let valor = $('#valor').val();
            let valorEntrada = $(this).val();

            if (valor !== '') valor = parseFloat(valor);
            else valor = 0;

            if (valorEntrada !== '') valorEntrada = parseFloat(valorEntrada);
            else valorEntrada = 0;

            if (valorEntrada > valor) {
                alert('Valor da entrada não pode ser maior que o valor do vencimento.');
                $(this).val(valor);
            }
        })

        $('#pessoa').val('<?php echo $pessoa->id?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('input.checkbox-callback').on('ifChecked', function (event) {
            computarValorReparcelar();
        });
        $('input.checkbox-callback').on('ifUnchecked', function(event) {
            computarValorReparcelar();
        });

        $('input.checkbox-callback-all').on('ifChecked', function(event) {
            $('input.checkbox-callback').each(function(index, tag){
                if (document.getElementById('checkbox-all').checked)  {
                    $(this).iCheck('check');
                } else  {
                    $(this).iCheck('uncheck');
                }
            });
        });

        $('input.checkbox-callback-all').on('ifUnchecked', function(event) {
            $('input.checkbox-callback').each(function(index, tag){
                if (document.getElementById('checkbox-all').checked)  {
                    $(this).iCheck('check');
                } else  {
                    $(this).iCheck('uncheck');
                }
            });
        });

        $('#pessoa').select2("readonly", true);
        $('#slwarehouse').select2("readonly", true);
        $('#receita').select2("readonly", true);

        $('#btnSelecionarParcelas').click(function (event) {
            $('#divReparcelamento').hide(1000);
            $('#btnSelecionarParcelas').hide();
            $('#divConfirmarReparelamento').hide();

            $('#btnNegociarParcelas').show();
            $('#divParcelasAhSelecionar').show();
        });

        $('#btnNegociarParcelas').click(function (evet) {
            $('#divReparcelamento').show(1000);
            $('#btnSelecionarParcelas').show();
            $('#divConfirmarReparelamento').show();

            $('#btnNegociarParcelas').hide();
            $('#divParcelasAhSelecionar').hide();

            getParcelamento();
        });
    });

    function getParcelamento() {

        var condicaoPagamento = $('#condicaopagamento').val();
        var tipoCobranca = $("#tipocobranca  option:selected").val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: site.base_url + "saleitem/getParcelas",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    dtvencimento: $('#dtvencimento').val(),
                    valorVencimento: $('#valor').val(),
                    tipoCobrancaId: tipoCobranca,
                    desconto : $('#desconto').val(),
                    blockUltimaParcela: true
                },
                dataType: 'html',
                success: function (html) {
                    $('#slTableParcelas tbody').html(html);

                    $(function(){
                        $('.mask_money').bind('keypress',mask.money);
                        $('.mask_money').click(function(){$(this).select();});
                    });
                }
            });
        } else {
            $('#slTableParcelas tbody').html('');
        }
    }

    function atualizarValorVencimentoAoEditarParcela(tag, pointer) {

        let valorDigitado = 0;
        let totalParcelasSuperior = 0;
        let tagVencimentos = $("input[name='valorVencimentoParcela[]']");
        let valorVencimento = $('#valor').val();
        let valorDesconto = $('#desconto').val();
        let totalParcelas =  tagVencimentos.length;

        if (valorVencimento === '') valorVencimento = 0;
        else valorVencimento = parseFloat(valorVencimento);

        if (valorDesconto === '') valorDesconto = 0;
        else valorDesconto = parseFloat(valorDesconto);

        valorVencimento = valorVencimento - valorDesconto;

        $(tagVencimentos).each(function(index, element ){

            var valorElementoSuperior = element.value;

            if (index > pointer) {

                let novoVencimentoElement = (valorVencimento - valorDigitado)/(totalParcelas-totalParcelasSuperior);

                if (novoVencimentoElement <= 0) {
                    alert('A somatória dos vencimentos não pode ultrapassou o valor da conta e o valor dos vencimentos das parcelas deve ser maior que ZERO.');
                    tag.value = tag.getAttribute('vencimento');
                    atualizarValorVencimentoAoEditarParcela(tag, pointer);
                    return false;
                } else {
                    element.value = novoVencimentoElement.toFixed(2);
                    element.setAttribute('vencimento', novoVencimentoElement);
                }
            } else {
                valorDigitado += parseFloat(valorElementoSuperior);
                totalParcelasSuperior++;
            }
        });
    }

    function computarValorReparcelar() {
        let valorRenegociar = 0;

        $('#btnNegociarParcelas').attr('disabled', 'disabled');

        $('input.checkbox-callback').each(function(index, tag){
            let valorPagar = $("input[name='valorRenegociar[]']")[index].value;
            if (tag.checked) if (valorPagar !== '')  {
                valorRenegociar = valorRenegociar + parseFloat(valorPagar);
                $('#btnNegociarParcelas').removeAttr('disabled');
            }
        });

        $('#valor').val(valorRenegociar.toFixed(2));
        $("#valorentrada").val(valorRenegociar.toFixed(2));
    }
</script>
