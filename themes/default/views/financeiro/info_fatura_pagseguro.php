<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Ver Detalhes da Fatura no PagSeguro Consulta o Status Atual do Pagamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("financeiro/adicionarSaque/", $attrib); ?>
        <div class="modal-body">
            <div id="payments">
                <div class="well well-sm well_1">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("reference_no", "reference_no"); ?>
                                    <?= form_input('reference_no',  $transaction->getReference() , 'class="form-control tip" disabled '); ?>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <?= lang("Código de Integração", "Código de Integração"); ?>
                                    <?= form_input('code',  $transaction->getCode() , 'class="form-control tip" disabled'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Data do Cadastro", "do Cadastro"); ?>
                                    <?= form_input('dataCadastro', $this->sma->hrld( $transaction->getDate()) , 'class="form-control tip" disabled '); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Data do Pagamento/Cancelamento", "Data do Pagamento/Cancelamento"); ?>
                                    <?= form_input('dataCadastro',  $this->sma->hrld($transaction->getLastEventDate()) , 'class="form-control tip" disabled '); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Total", "Total"); ?>
                                    <?= form_input('Total',  $transaction->getGrossAmount() , 'class="form-control tip" disabled'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Taxa", "Taxa"); ?>
                                    <?= form_input('Taxa',  $transaction->getFeeAmount() , 'class="form-control tip" disabled'); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Total Líquido", "Total Líquido"); ?>
                                    <?= form_input('totalLiquido',  $transaction->getNetAmount() , 'class="form-control tip" disabled'); ?>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("Status do pagamento", "tatus do pagamento"); ?>
                                    <?= form_input('status', lang($transaction->getStatus()->getTypeFromValue($transaction->getStatus()->getValue())) , 'class="form-control tip" disabled'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12">
                        <table role="table">
                            <thead>
                            <tr>
                                <th>Codigo</th>
                                <th>Significado</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td><strong>Aguardando pagamento</strong>:&nbsp;o comprador iniciou a transação, mas até o momento o PagSeguro não recebeu nenhuma informação sobre o pagamento.</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td><strong>Em análise</strong>:&nbsp;o comprador optou por pagar com um cartão de crédito e o PagSeguro está analisando o risco da transação.</td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td><strong>Paga</strong>:&nbsp;a transação foi paga pelo comprador e o PagSeguro já recebeu uma confirmação da instituição financeira responsável pelo processamento.</td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td><strong>Disponível</strong>:&nbsp;a transação foi paga e chegou ao final de seu prazo de liberação sem ter sido retornada e sem que haja nenhuma disputa aberta.</td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td><strong>Em disputa</strong>:&nbsp;o comprador, dentro do prazo de liberação da transação, abriu uma disputa.</td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td><strong>Devolvida</strong>:&nbsp;o valor da transação foi devolvido para o comprador.</td>
                            </tr>
                            <tr>
                                <td>7</td>
                                <td><strong>Cancelada</strong>:&nbsp;a transação foi cancelada sem ter sido finalizada.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>