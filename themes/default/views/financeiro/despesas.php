 <style type="text/css" media="screen">
    #SLData td:nth-child(1) {display: none;}
    #SLData td:nth-child(2) {text-align: center;}
    #SLData td:nth-child(3) {text-align: left;width: 30%;}
    #SLData td:nth-child(4) {text-align: left;width: 30%;}
    #SLData td:nth-child(5) {text-align: left;display: none;}
    #SLData td:nth-child(6) {text-align: left;display: none;}
    #SLData td:nth-child(7) {text-align: right;}
    #SLData td:nth-child(8) {text-align: right;}
    #SLData td:nth-child(9) {text-align: right;}
    #SLData td:nth-child(10) {text-align: right;}
    #SLData td:nth-child(11) {text-align: center;display: none;}
    #SLData td:nth-child(12) {text-align: center;display: none;}
    #SLData td:nth-child(13) {text-align: center;display: none;}
    #SLData td:nth-child(14) {text-align: center;display: none;}
    #SLData td:nth-child(15) {text-align: center;display: none;}
    #SLData td:nth-child(16) {text-align: center;display: none;}
    #SLData td:nth-child(17) {text-align: center;display: none;}
    #SLData td:nth-child(18) {text-align: center;display: none;}
    #SLData td:nth-child(19) {text-align: center;}
</style>

<script>

    function attachDescriptionToName(x, alignment, aaData) {
        if (aaData[15] !== null) {

            let codigo = aaData[17];

            if ( aaData[15] === 'boleto') x = '<a href="'+aaData[14]+'" target="_blank" title="Ver Boleto"><i class="fa fa-barcode"></i> '+x+'</a><br/><small>Cod. '+codigo+'</small> ';
            else  x = '<a href="'+aaData[16]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[5] !== null) x += '<br/><small><i class="fa fa-money"></i> '+aaData[5]+'</small>';

        return x;
    }

    function currencyFormatValuePay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #F43E61;">'+formatMoney(x*-1)+'<br/><small>'+aaData[10]+'</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValuePaid(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let dtUltimoPagamento = aaData[11] != null ? aaData[11] : '&nbsp;';

        if (x != null) {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + dtUltimoPagamento + '</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValueToPay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let diferencaEmDias = aaData[12] != null ? aaData[12] : '&nbsp;';
        let status = aaData[9];

        if (diferencaEmDias < 0 && status !== 'QUITADA') {
            if (diferencaEmDias === -1) return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>' + diferencaEmDias + ' dia</small></div>';
            else return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>' + diferencaEmDias + ' dias</small></div>';
        } else {
            return '<div class="' + alignment + '" style="color: #F43E61;">' + formatMoney(x*-1) + '<br/><small>&nbsp;</small></div>';
        }
    }

    function currencyFormatDebit(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="color: #F43E61;!important;">'+formatMoney(x*-1)+'</div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterTipoCobranca').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterDespesa').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').keydown(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('financeiro/getDespesas')?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filter_numero_documento", "value":  $('#filter_numero_documento').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterDespesa", "value":  $('#filterDespesa').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormatValuePay},
                {"mRender": currencyFormatValuePaid},
                {"mRender": currencyFormatValueToPay},
                {"mRender": row_status},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                let total = 0, pago = 0, pagar = 0, status, recebidas = 0, aReceber = 0, vencidas = 0, totalizadores = 0;

                for (var i = 0; i < aaData.length; i++) {
                    status = aaData[aiDisplay[i]][9];

                    total += parseFloat(aaData[aiDisplay[i]][6]);
                    pago += parseFloat(aaData[aiDisplay[i]][7]);
                    pagar += parseFloat(aaData[aiDisplay[i]][8]);

                    if (status === 'ABERTA') aReceber += parseFloat(aaData[aiDisplay[i]][8]);
                    if (status === 'PARCIAL' || status === 'QUITADA') recebidas += parseFloat(aaData[aiDisplay[i]][7]);

                    totalizadores += recebidas + aReceber;
                }

                var nCells = nRow.getElementsByTagName('th');

                nCells[6].innerHTML = currencyFormatDebit(parseFloat(total));
                nCells[7].innerHTML = currencyFormat(parseFloat(pago));
                nCells[8].innerHTML = currencyFormatDebit(parseFloat(pagar));

                $('#recebidas').html(currencyFormat(recebidas));
                $('#aReceber').html(currencyFormat(aReceber));
                $('#vencidas').html(currencyFormat(vencidas));
                $('#totalizador').html(currencyFormat(totalizadores));

            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[(ano-mes-dia)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('pessoa');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('servico');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('receita');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('cobranca');?>]", filter_type: "text", data: []},
            {
                column_number: 9, select_type: 'select2',
                select_type_options: {
                    placeholder: '<?=lang('payment_status');?>',
                    width: '100%',
                    minimumResultsForSearch: -1,
                    allowClear: true
                },
                data: [
                    {value: 'ABERTA', label: '<?=lang('ABERTA');?>'},
                    {value: 'PARCIAL', label: '<?=lang('PARCIAL');?>'},
                    {value: 'VENCIDA', label: '<?=lang('VENCIDA');?>'},
                    {value: 'QUITADA', label: '<?=lang('QUITADA');?>'}
                ]
            }

        ], "footer");
    });

</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa fa-sign-in"></i><?=lang('contas_pagar');?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('financeiro/adicionarContaPagar')?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-arrow-circle-down"></i> <?=lang('adicionar_conta_pagar')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-c.gif"></legend>
                        <div style="margin-bottom: 20px;display: none;"  class="divfilters">
                            <div class="col-sm-2">
                                <?= lang("status_viagem", "status_viagem") ?>
                                <?php
                                $opts = array(
                                    'Confirmado' => lang('confirmado'),
                                    'Inativo' => lang('produto_inativo'),
                                    'Arquivado' => lang('arquivado') ,
                                );
                                echo form_dropdown('status', $opts,  $unit, 'class="form-control" id="status"');
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?= lang("ano", "ano") ?>
                                <?php
                                $opts = array(
                                    '2020' => lang('2020'),
                                    '2021' => lang('2021'),
                                    '2022' => lang('2022'),
                                    '2023' => lang('2023'),
                                    '2024' => lang('2024'),
                                    '2025' => lang('2025'),
                                    '2026' => lang('2026'),
                                    '2027' => lang('2027'),
                                    '2028' => lang('2028'),
                                    '2029' => lang('2029'),
                                    '2030' => lang('2030'),
                                );
                                echo form_dropdown('ano', $opts,  $ano, 'class="form-control" id="ano"');
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?= lang("mes", "mes") ?>
                                <?php
                                $opts = array(
                                    'Todos' => lang('Todos'),
                                    '01' => lang('Janeiro'),
                                    '02' => lang('Fevereiro'),
                                    '03' => lang('Março'),
                                    '04' => lang('Abril'),
                                    '05' => lang('Maio'),
                                    '06' => lang('Junho'),
                                    '07' => lang('Julho'),
                                    '08' => lang('Agosto'),
                                    '09' => lang('Setembro'),
                                    '10' => lang('Outubro'),
                                    '11' => lang('Novembro'),
                                    '12' => lang('Dezembro'),
                                );
                                echo form_dropdown('mes', $opts,  $mes, 'class="form-control" id="mes"');
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <label class="control-label" for="product"><?= lang("products"); ?></label>
                                <?php
                                $pgs[""] = lang('select').' '.lang('product');
                                echo form_dropdown('programacao', $pgs,  '', 'class="form-control" id="programacao" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"');
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_de", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_ate", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="filterStatus"><?=lang('payment_status');?></label>
                                    <select class="form-control tip" name="filterStatus" id="filterStatus">
                                        <option value="">Selecione</option>
                                        <option value="ABERTA"><?=lang('ABERTA');?></option>
                                        <option value="PARCIAL"><?=lang('PARCIAL');?></option>
                                        <option value="QUITADA"><?=lang('QUITADA');?></option>
                                        <option value="VENCIDA"><?=lang('VENCIDA');?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                    <?php
                                    $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : ""), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("despesa"); ?></label>
                                    <select class="form-control tip" name="filterDespesa" id="filterDespesa">
                                        <option value=""><?php echo lang('select').' '.lang('despesa');?></option>
                                        <?php
                                        foreach ($planoDespesas as $despesa) {?>
                                            <optgroup label="<?php echo $despesa->name;?>">
                                                <?php
                                                $despesasFilhas = $this->financeiro_model->getReceitaByDespesaSuperiorId($despesa->id);
                                                foreach ($despesasFilhas as $item) {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2" style="display: none;">
                                <div class="form-group">
                                    <?= lang("numero_documento", "filter_numero_documento") ?>
                                    <?= form_input('filter_numero_documento', (isset($_POST['filter_numero_documento']) ? $_POST['filter_numero_documento'] : ''), 'class="form-control" id="filter_numero_documento"'); ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                            <th><?php echo $this->lang->line("total"); ?></th>
                            <th><?php echo $this->lang->line("pago"); ?></th>
                            <th><?php echo $this->lang->line("pagar"); ?></th>
                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                            <th style="display: none;">Parc.</th>
                            <th style="display: none;">Ult.</th>
                            <th style="display: none;">Atras.</th>
                            <th style="display: none;">Venda.</th>
                            <th style="display: none;">Link.</th>
                            <th style="display: none;">Tipo.</th>
                            <th style="display: none;">Cartao.</th>
                            <th style="display: none;">Cod.</th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

 <script type="text/javascript">
     $(document).ready(function () {
         createFilter('filters');

         buscarProdutos();

         $('#programacao').change(function(event){
             $('#SLData').DataTable().fnClearTable()
         });

         $("#ano").change(function (event){
             buscarProdutos();
         });

         $("#mes").change(function (event){
             buscarProdutos();
         });

         $("#status").change(function (event){
             buscarProdutos();
         });

         function buscarProdutos() {

             $.ajax({
                 type: "GET",
                 url: "<?php echo base_url() ?>sales/buscarProgramacao",
                 data: {
                     status: $('#status').val(),
                     ano: $('#ano').val(),
                     mes: $('#mes').val()
                 },
                 dataType: 'json',
                 success: function (agendamentos) {
                     $('#programacao').empty();

                     var option = $('<option/>');

                     option.attr({ 'value': '' }).text('Selecione uma opção');
                     $('#programacao').append(option);

                     $(agendamentos).each(function( index, agendamento ) {
                         var option = $('<option/>');

                         option.attr({ 'value': agendamento.id }).text(agendamento.label);
                         $('#programacao').append(option);
                     });

                     $('#programacao').select2({minimumResultsForSearch: 7});
                 }
             });
         }

         function createFilter(nameClasse) {
             $('.'+nameClasse).click(function() {
                 if ($('.div'+nameClasse).is(':visible')) {
                     $('.div'+nameClasse).hide(300);
                     $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

                 } else {
                     $('.div'+nameClasse).show(300).fadeIn();
                     $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
                 }
             });
         }
     });
 </script>