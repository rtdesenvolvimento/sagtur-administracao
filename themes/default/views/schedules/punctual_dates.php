<div class="col-lg-12">
    <div class="col-md-3" style="display: none;">
        <div class="form-group all">
            <?= lang("descricao", "descricao") ?>
            <?= form_input('descricao_data_pontual[]','', 'class="form-control" required="required"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group all">
            <?= lang("data_inicio", "data_inicio") ?>
            <?= form_input('data_inicio_pontual[]','', 'class="form-control" required="required"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group all">
            <?= lang("hora_saida", "hora_saida") ?>
            <?= form_input('hora_inicio_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group all">
            <?= lang("hora_retorno", "hora_retorno") ?>
            <?= form_input('hora_final_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-1 price_range_data">
        <div class="form-group all">
            <?= lang('value', 'value'); ?>
            <?= form_input('price_pontual[]', '0.00', 'class="form-control mask_money tip" placeholder="R$"'); ?>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group all">
            <?= lang("vagas", "vagas") ?>
            <?= form_input('vagas_pontual[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
        </div>
    </div>
    <div class="col-md-1" style="float: left;margin-top: 30px;text-align: right;">
        <i class="fa fa-trash fa-2x removeItemDataDatasPontuais" style="cursor: pointer"></i>
    </div>
</div>