<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
            font-size: 15px;
            margi-right: 25px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        .assinatura {
            border: 1px solid #0b0b0b;
            padding: 8px;
        }

        p {
            font-size: 15px;
            line-height: 25px;
        }

        b{
            font-weight: bold;
        }
    </style>
<body>
<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: center;width: 10%;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
        </td>
        <td style="text-align: right;width: 90%;" >
            <h1><?=$Settings->site_name;?></h1>
            <h4>
                <?php
                echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state . '<br />' . $biller->country;
                echo '<p>';
                if ($biller->vat_no != "-" && $biller->vat_no != "") {
                    echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                }
                if ($biller->cf1 != '-' && $biller->cf1 != '') {
                    echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                }
                if ($biller->cf2 != '-' && $biller->cf2 != '') {
                    echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                }
                if ($biller->cf3 != '-' && $biller->cf3 != '') {
                    echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                }
                if ($biller->cf4 != '-' && $biller->cf4 != '') {
                    echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                }
                if ($biller->cf5 != '-' && $biller->cf5 != '') {
                    echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                }
                if ($biller->cf6 != '-' && $biller->cf6 != '') {
                    echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                }
                echo '</p>';
                echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                ?>
            </h4>
            <h4><b>CARTA DE CRÉDITO N° <span style="color: red;"><?=$gift->card_no;?></span></b></h4>
        </td>
    </tr>
    </thead>
</table>

<br/>
<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td colspan="2" style="padding: 5px;border: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
            <h2>CARTA DE CRÉDITO</h2>
        </td>
    </tr>
    </thead>
</table>

<br/>
<p>
    Prezado(a):
</p>
<p style="text-align: justify;">
    É com prazer que estamos enviando a você este documento como garantia de nossa capacidade financeira para
    honrar nossos compromissos conforme acordado em nosso contrato de prestação de serviços/produto.
</p>

<table style="width: 100%;">
    <thead>
    <tr>
        <th colspan="2"><h5>DADOS DO CLIENTE</h5></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 70%;"></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Nome do Cliente: </b></td>
        <td><?=$customer->name;?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>CPF: </b></td>
        <td><?=$customer->vat_no;?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b><?=strtoupper($customer->tipo_documento);?>: </b></td>
        <td><?=$customer->cf1;?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Data de Nascimento: </b></td>
        <td><?=$this->sma->hrsd($customer->data_aniversario);?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Telefone: </b></td>
        <td><?=$customer->cf5;?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>E-mail: </b></td>
        <td><?=$customer->email;?></td>
    </tr>
    <?php if ($gift->note) {?>
        <tr>
            <td style="text-align: left;"><b>Nota do Crédito:</b></td>
            <td><?=$gift->note;?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>

<br/>

<?php if ($inv) {?>
<table style="width: 100%;">
    <thead>
    <tr>
        <th colspan="2"><h5>DADOS DA VENDA DE ORIGEM</h5></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 70%;"></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Código da Venda: </b></td>
        <td><?=$inv->reference_no;?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Data de Compra: </b></td>
        <td><?=$this->sma->hrld($inv->date);?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Data de Cancelamento: </b></td>
        <td><?=$this->sma->hrld($gift->date);?></td>
    </tr>
    <tr>
        <td style="text-align: left;"><b>Motivo do Cancelamento:</b></td>
        <td><?=$inv->motivo_cancelamento;?></td>
    </tr>
    </tbody>
</table>
<?php } ?>

<br/>

<?php if (!empty($credited_sales)) {?>
<table style="width: 100%;">
    <thead>
    <tr>
        <th colspan="3"><h5>VENDAS CREDITADAS</h5></th>
    </tr>
    <tr>
        <th>Cod.</th>
        <th style="text-align: center;">Data</th>
        <th style="text-align: right;">Valor Créditado</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($credited_sales as $credited){
        if ($payment->status == 'ESTORNO') continue; ?>
        <tr>
            <td><?=$credited->reference_no;?></td>
            <td style="text-align: center;"><?=$this->sma->hrld($credited->date);?></td>
            <td style="text-align: right;"><?=$this->sma->formatMoney($credited->amount);?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php }?>


<br/>

<table style="width: 100%;">
    <tr>
        <td style="border-bottom: 1px solid #0b0b0b;"></td>
    </tr>
</table>

<table style="width: 100%;line-height: 25px;">
    <tr>
        <td style="width: 30%;"></td>
        <td style="width: 70%;"></td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;border-bottom: 1px solid #cfcfcf;">CRÉDITO DE:</td>
        <td style="border-bottom: 1px solid #cfcfcf;"><span style="color: red;font-weight: bold;"><?=$this->sma->formatMoney($gift->value);?></span></td>
    </tr>

    <?php if ($gift->value-$gift->balance> 0) {?>
    <tr>
        <td style="text-align: left;font-weight: bold;border-bottom: 1px solid #cfcfcf;">UTILIZADO:</td>
        <td style="border-bottom: 1px solid #cfcfcf;"><span style="color: red;font-weight: bold;"><?=$this->sma->formatMoney($gift->value-$gift->balance);?></span></td>
    </tr>
    <tr>
        <td style="text-align: left;font-weight: bold;border-bottom: 1px solid #cfcfcf;">DISPONÍVEL:</td>
        <td style="border-bottom: 1px solid #cfcfcf;"><span style="color: red;font-weight: bold;"><?=$this->sma->formatMoney($gift->balance);?></span></td>
    </tr>
    <?php } ?>
    <tr>
        <td style="text-align: left;font-weight: bold;border-bottom: 1px solid #cfcfcf;">VÁLIDO ATÉ:</td>
        <td style="border-bottom: 1px solid #cfcfcf;"><span style="color: red;font-weight: bold;"><?=$this->sma->hrsd($gift->expiry);?></span></td>
    </tr>
</table>

<hr>

<p style="text-align: justify;">
    Por favor, não hesite em entrar em contato conosco caso precise de qualquer esclarecimento adicional ou para discutir quaisquer aspectos do contrato.
    Estamos comprometidos em manter uma comunicação aberta e transparente em todos os momentos.
</p>

<p style="text-align: center;">
    Atenciosamente Equipe, <b><?=$Settings->site_name;?></b>
</p>

</body>
</html>
