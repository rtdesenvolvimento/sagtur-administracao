<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('motivo_cancelamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("gifts/cancelar/" . $id, $attrib); ?>
        <div class="modal-body">
            <div class="form-group">
                <?= lang("motivo_cancelamento", "note"); ?>
                <?php echo form_textarea('note', '', 'class="form-control" required="required" id="note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('save', lang('save'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
