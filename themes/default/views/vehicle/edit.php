<link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/jquery-ui.css">

<?php echo form_open_multipart("vehicle/edit/".$vehicle->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>
<ul id="myTab" class="nav nav-tabs" style="text-align: center">
    <li><a href="#abageral" class="tab-grey"><i class="fa fa-car" style="font-size: 20px;"></i><br/><?= lang('vehicle_details') ?></a></li>
    <li id="tbDatas"><a href="#availability" class="tab-grey"><i class="fa fa-calendar-plus-o" style="font-size: 20px;"></i><br/><?= lang('availability') ?></a></li>
    <li><a href="#" id="save" class="tab-grey" style="background: #3c763d;color: #F0F0F0"><i class="fa fa-save" style="font-size: 20px;"></i><br/><?= lang('edit') ?></a></li>
</ul>
<div class="tab-content">
    <!--Detalhes!-->
    <div id="abageral" class="tab-pane fade in">
        <div class="box">
            <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-edit"></i><?= lang('edit_vehicle'); ?></h2></div>
            <div class="box-content">
                <div class="row">
                    <input type="hidden" id="id" value="<?php echo $vehicle->id;?>">
                    <div class="col-lg-12">
                        <div class="col-md-3">
                            <?= lang("status_veiculo", "status_veiculo") ?>
                            <?php
                            $catStatusVeiculo[''] = lang("select") . " " . lang("status_veiculo") ;
                            foreach ($statusVeiculo as $stVeiculo) {
                                $catStatusVeiculo[$stVeiculo->id] = $stVeiculo->name;
                            }
                            echo form_dropdown('status_veiculo', $catStatusVeiculo, (isset($_POST['status_veiculo']) ? $_POST['status_veiculo'] : ($vehicle ? $vehicle->status_veiculo : '')), 'disabled class="form-control select" id="status_veiculo" placeholder="' . lang("select") . " " . lang("status_veiculo") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group all">
                                <?= lang("reference_no", "reference_no") ?>
                                <?= form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ($vehicle ? $vehicle->reference_no : '')), 'class="form-control" id="reference_no" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?= lang("tipo_veiculo", "tipo_veiculo") ?>
                            <?php
                            $catTiposVeiculo[''] = lang("select") . " " . lang("tipo_veiculo") ;
                            foreach ($tiposVeiculo as $tipoVeiculo) {
                                $catTiposVeiculo[$tipoVeiculo->id] = $tipoVeiculo->name;
                            }
                            echo form_dropdown('tipo_veiculo', $catTiposVeiculo, (isset($_POST['tipo_veiculo']) ? $_POST['tipo_veiculo'] : ($vehicle ? $vehicle->tipo_veiculo : '')), 'class="form-control select" id="tipo_veiculo" placeholder="' . lang("select") . " " . lang("tipo_veiculo") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-md-3">
                            <?= lang("categoria", "categoria") ?>
                            <?php
                            $catCategorias[''] = lang("select") . " " . lang("categoria") ;
                            foreach ($categoriasVeiculo as $categoria) {
                                $catCategorias[$categoria->id] = $categoria->name;
                            }
                            echo form_dropdown('categoria', $catCategorias, (isset($_POST['categoria']) ? $_POST['categoria'] : ($vehicle ? $vehicle->categoria : '')), 'class="form-control select" id="categoria" placeholder="' . lang("select") . " " . lang("categoria") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group all">
                                <?= lang("name", "name") ?>
                                <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($vehicle ? $vehicle->name : '')), 'class="form-control" id="name" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("placa", "placa") ?>
                                <?= form_input('placa', (isset($_POST['placa']) ? $_POST['placa'] : ($vehicle ? $vehicle->placa : '')), 'class="form-control" id="placa" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("marca", "marca") ?>
                                <?= form_input('marca', (isset($_POST['marca']) ? $_POST['marca'] : ($vehicle ? $vehicle->marca : '')), 'class="form-control" id="marca" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("modelo", "modelo") ?>
                                <?= form_input('modelo', (isset($_POST['modelo']) ? $_POST['modelo'] : ($vehicle ? $vehicle->modelo : '')), 'class="form-control" id="modelo" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("cor", "cor") ?>
                                <?= form_input('cor', (isset($_POST['cor']) ? $_POST['cor'] : ($vehicle ? $vehicle->cor : '')), 'class="form-control" id="cor" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("ano", "ano") ?>
                                <?= form_input('ano', (isset($_POST['ano']) ? $_POST['ano'] : ($vehicle ? $vehicle->ano : '')), 'class="form-control" id="ano"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("municipio", "municipio") ?>
                                <?= form_input('municipio', (isset($_POST['municipio']) ? $_POST['municipio'] : ($vehicle ? $vehicle->municipio : '')), 'class="form-control" id="municipio"'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group all">
                                <?= lang("uf", "uf") ?>
                                <?= form_input('uf', (isset($_POST['uf']) ? $_POST['uf'] : ($vehicle ? $vehicle->uf : '')), 'class="form-control" id="uf"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?= lang("chassi", "chassi") ?>
                                <?= form_input('chassi', (isset($_POST['chassi']) ? $_POST['chassi'] : ($vehicle ? $vehicle->chassi : '')), 'class="form-control" id="chassi"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <?= lang("tipo_combustivel", "tipo_combustivel") ?>
                            <?php
                            $catTiposCombustivel[''] = lang("select") . " " . lang("tipo_combustivel") ;
                            foreach ($tiposCombustivel as $tipoCombustivel) {
                                $catTiposCombustivel[$tipoCombustivel->id] = $tipoCombustivel->name;
                            }
                            echo form_dropdown('tipo_combustivel', $catTiposCombustivel, (isset($_POST['tipo_combustivel']) ? $_POST['tipo_combustivel'] : ($vehicle ? $vehicle->tipo_combustivel : '')), 'class="form-control select" id="tipo_combustivel" placeholder="' . lang("select") . " " . lang("tipo_combustivel") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("capacidade_min", "capacidade_min"); ?>
                                <?php echo form_input('capacidade_min', (isset($_POST['capacidade_min']) ? $_POST['capacidade_min'] : ($vehicle ? $vehicle->capacidade_min : '')), 'class="form-control input-tip mask_integer" id="capacidade_min"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("capacidade_max", "capacidade_max"); ?>
                                <?php echo form_input('capacidade_max', (isset($_POST['capacidade_max']) ? $_POST['capacidade_max'] : ($vehicle ? $vehicle->capacidade_max : '')), 'class="form-control input-tip mask_integer" id="capacidade_max"'); ?>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <?= lang("km_atual", "km_atual"); ?>
                                <?php echo form_input('km_atual', (isset($_POST['km_atual']) ? $_POST['km_atual'] : ($vehicle ? $vehicle->km_atual : '')), 'class="form-control input-tip mask_integer" id="km_atual"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('cambio_automatico', '1', (isset($_POST['cambio_automatico']) ? $_POST['cambio_automatico'] : ($vehicle ? $vehicle->cambio_automatico : '')), 'id="cambio_automatico"'); ?>
                                <label for="attributes" class="padding05"><?= lang('cambio_automatico'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('active', '1', (isset($_POST['active']) ? $_POST['active'] : ($vehicle ? $vehicle->active : '')), 'id="active"'); ?>
                                <label for="attributes" class="padding05"><?= lang('active'); ?></label>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-photo"></i> <?= lang("vehicle_gallery", "vehicle_gallery") ?></div>
                                <div class="panel-body">
                                    <div class="col-md-12 alert-info" style="margin-bottom: 20px;">
                                        <h3><i class="fa fa-info-circle"></i> Atencão</h3>
                                        <ul>
                                            <li>Adicione imagens nos formatos (jpg, jpeg ou png) com no máximo 1MB de Tamanho.</li>
                                        </ul>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $vehicle->photo ?>" alt="<?= $vehicle->photo ?>" class="img-responsive img-thumbnail"/>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group all" id="div_product_details">
                                                <div class="form-group all">
                                                    <?= lang("photo", "photo") ?>
                                                    <input id="photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="photo" data-show-upload="false"
                                                           data-show-preview="false" accept="image/*" class="form-control file">
                                                </div>
                                                <div class="form-group all">
                                                    <?= lang("vehicle_gallery_images", "images") ?>
                                                    <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                                                           data-show-preview="false" class="form-control file" accept="image/*">
                                                </div>
                                                <div id="img-details" ></div>
                                                <div id="multiimages" class="padding10">
                                                    <?php if (!empty($images)) {
                                                        echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $vehicle->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $vehicle->photo . '" alt="' . $vehicle->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                        foreach ($images as $ph) {
                                                            echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                            if ($Owner || $Admin || $GP['products-edit']) {
                                                                echo '<a href="#" class="delimg" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                                                            }
                                                            echo '</div>';
                                                        }
                                                    }
                                                    ?>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("note", "note"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ($vehicle ? $vehicle->note : '')), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="form-group">
                                <?php echo form_submit('edit_vehicle', $this->lang->line("edit_vehicle"), 'id="edit_vehicle" class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Disponibilidade !-->
    <div id="availability" class="tab-pane fade">
        <div class="box">
            <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('availability'); ?></h2></div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-6">
                            <?= lang("tipo_contrato", "lctipocontrato") ?>
                            <?php
                            $cbTipoContratoLocacao[''] = lang("select") . " " . lang("tipo_contrato") ;
                            foreach ($tiposContrato as $tipoContrato) {
                                $cbTipoContratoLocacao[$tipoContrato->id] = $tipoContrato->name;
                            }
                            echo form_dropdown('tipo_contrato', $cbTipoContratoLocacao, '', 'class="form-control select" id="lctipocontrato" placeholder="' . lang("select") . " " . lang("tipo_contrato") . '"required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("data_inicio_comercializacao", "data_inicio_comercializacao") ?>
                                <?= form_input('data_inicio_comercializacao','', 'id="data_inicio_comercializacao" class="form-control" required="required"', 'date'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-lg-12">
                        <div class="col-md-12">
                            <ul id="myTab" class="nav nav-tabs" style="text-align: center">
                                <!--
                                <li><a href="#domingo" class="tab-grey" id="li_domingo"><?= lang('domingo') ?></a></li>
                                <li><a href="#segunda" class="tab-grey"><?= lang('segunda') ?></a></li>
                                <li><a href="#terca" class="tab-grey"><?= lang('terca') ?></a></li>
                                <li><a href="#quarta" class="tab-grey"><?= lang('quarta') ?></a></li>
                                <li><a href="#quinta" class="tab-grey"><?= lang('quinta') ?></a></li>
                                <li><a href="#sexta" class="tab-grey"><?= lang('sexta') ?></a></li>
                                <li><a href="#sabado" class="tab-grey"><?= lang('sabado') ?></a></li>
                                <li><a href="#datas_pontuais" class="tab-grey"><?= lang('datas_pontuais') ?></a></li>
                                !-->

                                <li><a href="#periodos" id="li_periodos" class="tab-grey"><i class="fa fa-calendar" style="font-size: 20px;"></i><br/><?= lang('periodos') ?></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="domingo" class="tab-pane fade in">
                                    <div class="row" id="div_domingo">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_domingo[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_domingo[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_domingo', $this->lang->line("add"), 'id="add_date_domingo" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="segunda" class="tab-pane fade">
                                    <div class="row" id="div_segunda">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_segunda[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_segunda[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_segunda', $this->lang->line("add"), 'id="add_date_segunda" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="terca" class="tab-pane fade">
                                    <div class="row" id="div_terca">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_terca[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_terca[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_terca', $this->lang->line("add"), 'id="add_date_terca" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="quarta" class="tab-pane fade">
                                    <div class="row" id="div_quarta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_quarta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_quarta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_quarta', $this->lang->line("add"), 'id="add_date_quarta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="quinta" class="tab-pane fade">
                                    <div class="row" id="div_quinta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_quinta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_quinta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_quinta', $this->lang->line("add"), 'id="add_date_quinta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sexta" class="tab-pane fade">
                                    <div class="row" id="div_sexta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_sexta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_sexta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_sexta', $this->lang->line("add"), 'id="add_date_sexta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sabado" class="tab-pane fade">
                                    <div class="row" id="div_sabado">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_sabado[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_sabado[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_sabado', $this->lang->line("add"), 'id="add_date_sabado" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="datas_pontuais" class="tab-pane fade">
                                    <div class="row" id="div_datas_pontuais">
                                        <div class="col-lg-12">
                                            <div class="col-md-4">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_data_pontual[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_pontual[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_pontual', $this->lang->line("add"), 'id="add_date_pontual" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="periodos" class="tab-pane fade">
                                    <div class="row" id="div_periodos">
                                        <div class="col-lg-12">
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_periodos[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_final", "data_final") ?>
                                                    <?= form_input('data_final_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_periodos', $this->lang->line("add"), 'id="add_date_periodos" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;display: none;">
                    <div class="col-lg-12">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-clock-o"></i> <?= lang("data_horario_bloqueado") ?></div>
                                <div class="panel-body">
                                    <div class="row" id="div_periodos">
                                        <div class="col-lg-12">
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_bloqueio[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_bloqueio[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_final", "data_final") ?>
                                                    <?= form_input('data_final_bloqueio[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_bloqueio[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_bloqueio[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_bloqueio', $this->lang->line("add"), 'id="add_date_bloqueio" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= form_close(); ?>

<script src="<?php echo base_url() ?>assets/rapido/js/jquery-ui.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        $('.change_img').click(function (event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            $('#pr-image').attr('src', img_src);
            return false;
        });

        $('#add_date_domingo').click(function (event){
            criar_linha_dia_semana('domingo');
        });

        $('#add_date_segunda').click(function (event){
            criar_linha_dia_semana('segunda')
        });

        $('#add_date_terca').click(function (event){
            criar_linha_dia_semana('terca')
        });

        $('#add_date_quarta').click(function (event){
            criar_linha_dia_semana('quarta')
        });

        $('#add_date_quinta').click(function (event){
            criar_linha_dia_semana('quinta')
        });

        $('#add_date_sexta').click(function (event){
            criar_linha_dia_semana('sexta')
        });

        $('#add_date_sabado').click(function (event){
            criar_linha_dia_semana('sabado')
        });

        $('#add_date_pontual').click(function (event){
            criar_linha_datas_pontuais()
        });

        $('#add_date_periodos').click(function (event){
            criar_linha_datas_periodo()
        });

        $('#save').click(function (event){$('#edit_vehicle').click();});

        setTimeout(function(){$('#li_periodos').click()}, 3000);

        function criar_linha_dia_semana(dia_semana) {
            $.ajax({
                url: site.base_url + "vehicle/add_shedule",
                data: {
                    dia_semana: dia_semana
                },
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_' + dia_semana).append(html);

                $('.removeItemData_'+dia_semana).click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }

        function criar_linha_datas_pontuais() {
            $.ajax({
                url: site.base_url + "vehicle/punctual_dates",
                data: {},
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_datas_pontuais').append(html);

                $('.removeItemDataDatasPontuais').click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }

        function criar_linha_datas_periodo() {
            $.ajax({
                url: site.base_url + "vehicle/period_dates",
                data: {},
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_periodos').append(html);

                $('.removeItemDataDatasPeriodo').click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }
    });

</script>
