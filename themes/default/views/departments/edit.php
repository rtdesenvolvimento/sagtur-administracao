<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('edit_department'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("departments/edit/".$department->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?= lang("active", "active") ?>
                <?php
                $opts = array(
                    1 => lang('ativo'),
                    0 => lang('inativo')
                );
                echo form_dropdown('active', $opts, $department->active, 'class="form-control" id="active" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $department->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('telefone', 'telefone'); ?>
                <?= form_input('telefone', $department->telefone, 'class="form-control" id="telefone"'); ?>
            </div>
            <div class="form-group">
                <?= lang("open_whatsapp", "open_whatsapp") ?>
                <?php
                $opts = array(
                    1 => lang('yes'),
                    0 => lang('no')
                );
                echo form_dropdown('open_whatsapp', $opts, $department->open_whatsapp , 'class="form-control" id="open_whatsapp" required="required"');
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('editDepartment', lang('edit_department'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function () {});
</script>
