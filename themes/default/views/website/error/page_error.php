<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?=$this->Settings->site_name;?></title>
    <script type="application/ld+json">
        {
            "@context" : "https://schema.org",
            "@type" : "WebSite",
            "name" : "<?php echo $this->Settings->site_name;?>",
            "url" : "<?=$this->Settings->url_site_domain;?>"
        }
    </script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="<?php echo$this->Settings->theme_color;?>">
    <meta name="theme-color" content="<?php echo$this->Settings->theme_color;?>"/>

    <!--Cabecalho-->
    <meta name="title" content="<?php echo $this->Settings->site_name;?>">
    <meta name="description" content="<?php echo $this->Settings->site_name;?>" >
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta name="keywords" content="">

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" />

    <?php if ($this->Settings->frase_site){?>
        <meta property="og:title" content="<?php echo $this->Settings->site_name.' | '.$this->Settings->frase_site;?>"  />
    <?php } else { ?>
        <meta property="og:title" content="<?php echo $this->Settings->site_name;?>"  />
    <?php } ?>

    <meta property="og:description" content="<?=strip_tags($this->Settings->about);?>" />
    <meta property="og:site_name" content="<?php echo $this->Settings->site_name;?>" />
    <meta property="og:image:alt" content="<?php echo $this->Settings->site_name;?>" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <!-- Favicon -->
    <link href="<?= base_url(); ?>assets/uploads/logos/<?php echo$this->Settings->logo2;?>" rel="icon">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <style>
        * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            background: linear-gradient(to bottom, <?=$this->Settings->theme_color;?>, #3a6073);
            font-family: Arial, sans-serif;
            color: #fff;
            text-align: center;
        }

        .container {
            background: rgba(255, 255, 255, 0.1);
            padding: 20px;
            border-radius: 10px;
            box-shadow: 0 4px 10px rgba(0, 0, 0, 0.3);
        }

        h1 {
            font-size: 5rem;
            margin-bottom: 20px;
        }

        p {
            font-size: 1.5rem;
            margin-bottom: 20px;
        }

        .btn-home {
            display: inline-block;
            padding: 25px 30px;
            background-color: #ff5722;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            transition: background-color 0.3s ease;
            font-size: 1.2rem;
        }

        .btn-home:hover {
            background-color: #e64a19;
        }

        img {
            margin-top: 20px;
            max-width: 100%;
            height: auto;
            border-radius: 10px;
        }

        @media (max-width: 600px) {
            h1 {
                font-size: 3rem;
            }

            p {
                font-size: 1rem;
            }
        }

    </style>
</head>
<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div class="container">
    <h1>404</h1>
    <p>
        <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" alt="logomarca">
    </p>
    <p><?=$title?></p><br/>
    <p style="font-weight: 700;text-transform: uppercase;line-height: 25px;"><?=$error?></p>
    <a href="<?=$urlLink;?>" class="btn-home">Voltar para a Página Inicial</a>
</div>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>


</body>
</html>
