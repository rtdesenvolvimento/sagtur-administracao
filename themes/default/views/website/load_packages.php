<?php foreach ($programacoes as $programacaoEstoque) {

    $isOrdenarPorData = TRUE;

    $mesSaida = $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida(), '%B');
    $anoSaida = date("Y", strtotime($programacaoEstoque->getDataSaida()));
    $product_details = $this->ProductDetailsRepository_model->getAll($programacaoEstoque->produtoId);
    $servicos_incluso =  $this->site->getAllServicosInclusoByProduct($programacaoEstoque->produtoId, 3); ?>

    <?php if ($isOrdenarPorData && ($mesSaida != $mesAtual || $anoSaida != $anoAtual)) { ?>
        <div class="col-lg-12 col-md-6 mb-4" style="text-align: left;">
            <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">PACOTES</h6>
            <h1><?php echo mb_strtoupper($mesSaida, mb_internal_encoding()).'/'.$anoSaida;?></h1>
        </div>
        <?php
        $mesAtual = $mesSaida;
        $anoAtual = $anoSaida;
        ?>
    <?php } if (!$isOrdenarPorData && ($programacaoEstoque->category_name != $category_name)) {?>
        <div class="col-lg-12 col-md-6 mb-4" style="text-align: left;">
            <h6 class="text-primary text-uppercase" style="letter-spacing: 5px;">PACOTES</h6>
            <h1><?php echo mb_strtoupper($programacaoEstoque->category_name, mb_internal_encoding());?></h1>
        </div>
        <?php $category_name = $programacaoEstoque->category_name;?>
    <?php } ?>

    <?php
    $url_click_pacote = '';
    $description = '';

    if ($this->Settings->receptive) {

        $dataSaida = $programacaoEstoque->getDataSaida();
        $horaSaida = $programacaoEstoque->getHoraSaida();

        $dataP = implode('-', array_reverse(explode('-', $dataSaida)));
        $horaP = explode(':', $horaSaida)[0] . '_' . explode(':', $horaSaida)[1];

        $description = $this->sma->slug($programacaoEstoque->name).'-data_'.$dataP.'h'.$horaP.'p'.$programacaoEstoque->produtoId;
    } else {
        $description = $this->sma->slug($programacaoEstoque->name).'-uid'.$programacaoEstoque->id;
    }

    $open_new_table = false;

    if ($this->Settings->receptive) {?>
        <?php if ($Settings->own_domain){?>
            <?php $url_click_pacote = base_url().'pacote/'.$description.'/'.$vendedor->id;?>
        <?php } else {?>
            <?php $url_click_pacote = base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id;?>
        <?php } ?>
    <?php } else if ($this->Settings->use_product_landing_page) {?>
        <?php if ($Settings->own_domain){?>
            <?php $url_click_pacote = base_url().'pacote/'.$description.'/'.$vendedor->id;?>
        <?php } else {?>
            <?php $url_click_pacote = base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id;?>
        <?php } ?>
    <?php } else if($programacaoEstoque->getTotalDisponvel() > 0) {?>
        <?php if ($programacaoEstoque->apenas_cotacao) {?>
            <?php if ($Settings->own_domain){?>
                <?php $url_click_pacote = base_url().'pacote/'. $description .'/'.$vendedor->id;?>
            <?php } else { ?>
                <?php $url_click_pacote = base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>
            <?php } ?>
        <?php } else { ?>
            <?php if ($Settings->own_domain){?>
                <?php $url_click_pacote = base_url().'pacote/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>
            <?php } else {?>
                <?php $url_click_pacote = base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>
            <?php } ?>
        <?php } ?>
    <?php } else if ($programacaoEstoque->permitirListaEmpera){?>
        <?php if ($Settings->own_domain){?>
            <?php $url_click_pacote = base_url().'pacote/'.$description.'/'.$vendedor->id;?>
        <?php } else {?>
            <?php $url_click_pacote = base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>
        <?php } ?>
    <?php } else {?>
        <?php
        $open_new_table = true;
        $url_click_pacote =  'https://api.whatsapp.com/send?phone=55' . trim($phone) . '&text=QUERO ENTRAR PARA LISTA DE ESPERA '.$programacaoEstoque->name.' - ' . $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida()); ?>
    <?php } ?>

    <style>
        /* Estilo inicial da div */
        .hover-div {
            display: inline-block;
            transition: transform 0.3s, z-index 0.3s, background-color 0.3s;
        }

        /* Estilo ao passar o mouse sobre a div */
        .hover-div:hover {
            transform: scale(1.1); /* Aumenta o tamanho da div */
            z-index: 1; /* Move a div para frente */
        }
    </style>
    <div class="col-lg-3 col-md-6 mb-4 hover-div">
        <div class="package-item bg-white mb-2">
            <a href="<?=$url_click_pacote;?>" style="text-decoration: none;" <?php if ($open_new_table) 'target="_blank"';?>>
                <?php if ($programacaoEstoque->alert_stripe){?>
                    <div class="blog-date" style="background: #ff0000">
                        <small class="text-white text-uppercase" style="font-weight: 500;"><?php echo $programacaoEstoque->alert_stripe;?></small>
                    </div>
                <?php } else if ($this->Settings->receptive) {?>
                    <?php if ($programacaoEstoque->promotion) {?>
                        <div class="blog-date" style="background: #dc3545">
                            <small class="text-white text-uppercase" style="font-weight: 500;">EM PROMOÇÃO</small>
                        </div>
                    <?php } ?>
                <?php } else if($programacaoEstoque->getTotalDisponvel() > 0) {?>
                    <?php if($programacaoEstoque->getTotalDisponvel() < $programacaoEstoque->alertar_polcas_vagas ||
                        $programacaoEstoque->getTotalDisponvel() < ((int) $programacaoEstoque->alertar_ultimas_vagas)) {?>
                        <?php if($programacaoEstoque->getTotalDisponvel() < $programacaoEstoque->alertar_ultimas_vagas ) {?>
                            <div class="blog-date" style="background: #59d600">
                                <small class="text-white text-uppercase" style="font-weight: 500;">ÚLTIMAS VAGAS</small>
                            </div>
                        <?php } else {?>
                            <div class="blog-date" style="background: #ff623e">
                                <small class="text-white text-uppercase" style="font-weight: 500;">POUCAS VAGAS</small>
                            </div>
                        <?php } ?>
                    <?php } else if ($programacaoEstoque->promotion) {?>
                        <div class="blog-date" style="background: #dc3545">
                            <small class="text-white text-uppercase" style="font-weight: 500;">EM PROMOÇÃO</small>
                        </div>
                    <?php } ?>
                <?php } else { ?>
                    <div class="blog-date" style="background: #212529">
                        <small class="text-white text-uppercase" style="font-weight: 500;">ESGOTADO</small>
                    </div>
                <?php } ?>


                <img class="img-fluid principal" id="pr-image<?php echo $programacaoEstoque->produtoId;?>"  data-src="<?php echo base_url('assets/uploads/'.$programacaoEstoque->image);?>" alt="">

                <div class="p-4" style="color: #212121;font-size: 18px;">
                    <div class="d-flex justify-content-between">
                        <small class="m-0"><i class="fa fa-map-marker text-primary mr-2"></i><?php echo $programacaoEstoque->category_name;?></small>
                        <style>
                            .duracao_atividade {
                                font-size: 10px;
                                padding: 5px 5px 5px 5px;
                                color: #000;
                                border-radius: 4px;
                                background-color: #e9ecef;
                            }
                        </style>
                        <?php if ($programacaoEstoque->desc_duracao != '' || ($programacaoEstoque->duracao_atividade > 0 && $programacaoEstoque->passeio && $this->Settings->receptive) ) {?>
                            <?php

                            $desc_duracao = $programacaoEstoque->desc_duracao;

                            if ($programacaoEstoque->passeio) {
                                $tipoDuracao = $programacaoEstoque->tipo_duracao_atividade;

                                if ($tipoDuracao == 'DIAS') {
                                    $tipoDuracao = 'dia(s)';
                                } elseif ($tipoDuracao == 'HORAS') {
                                    $tipoDuracao = 'hora(s)';
                                } elseif ($tipoDuracao == 'MINUTOS') {
                                    $tipoDuracao = 'minuto(s)';
                                }

                                $desc_duracao = 'Duração: '.$programacaoEstoque->duracao_atividade.' '.$tipoDuracao;
                            }
                            ?>
                            <small class="m-0 duracao_atividade"><i class="fa fa-clock-o text-primary mr-2"></i> <?=$desc_duracao?></small>
                        <?php } elseif (!$programacaoEstoque->use_day) {?>
                            <?php if ($programacaoEstoque->getDataSaida() !== $programacaoEstoque->getDataRetorno()){
                                $dias =  $this->sma->diferencaEntreDuasDatasEmDias($programacaoEstoque->getDataSaida(), $programacaoEstoque->getDataRetorno()) + 1;
                                if ($dias > 1) {
                                    $dias = $dias.' dias';
                                } else {
                                    $dias = $dias.' dia';
                                }
                                ?>
                                <small class="m-0"><i class="fa fa-calendar-o text-primary mr-2"></i><?php echo $dias?></small>
                            <?php } else { ?>
                                <small class="m-0"><i class="fa fa-calendar-o text-primary mr-2"></i><?php echo 'Bate Volta';?></small>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <div class="" style="text-align: left;padding: 0px 25px 0px 25px;">
                    <h4><?php echo $programacaoEstoque->name;?></h4>
                </div>
                <div style="text-align: center;">
                    <?php if (!empty($images)) {?>
                        <img class="img-responsive" src="<?php echo base_url('assets/uploads/thumbs/'.$programacaoEstoque->image);?>" alt="<?php echo base_url('assets/uploads/thumbs/'.$programacaoEstoque->image);?>" style="width:60px; height:60px;">
                        <?php foreach ($images as $image){?>
                            <img class="img-responsive" src="<?php echo base_url('assets/uploads/thumbs/'.$image->photo);?>" alt="<?php echo base_url('assets/uploads/thumbs/'.$image->photo);?>" style="width:60px; height:60px;">
                        <?php }?>
                    <?php } ?>
                </div>

                <div class="p-4" style="margin-top: -35px;">
                    <p style="margin-top: 20px;">
                    <div style="text-align: left;">
                        <?php if (!$this->Settings->use_product_landing_page) {?>
                            <?php if ($programacaoEstoque->product_details){?>
                                <details style="margin-top: 25px;">
                                    <summary>SOBRE A VIAGEM</summary>
                                    <?php echo $programacaoEstoque->product_details;?>
                                </details>
                            <?php } ?>
                            <?php if ($programacaoEstoque->itinerario){?>
                                <details>
                                    <summary>ROTEIRO</summary>
                                    <?php echo $programacaoEstoque->itinerario;?>
                                </details>
                            <?php } ?>
                            <?php if ($programacaoEstoque->oqueInclui){?>
                                <details>
                                    <summary>O QUE INCLUI</summary>
                                    <?php echo $programacaoEstoque->oqueInclui;?>
                                </details>
                            <?php } ?>

                            <?php if ($programacaoEstoque->valores_condicoes){?>
                                <details>
                                    <summary>INVESTIMENTO</summary>
                                    <?php echo $programacaoEstoque->valores_condicoes;?>
                                </details>
                            <?php } ?>

                            <?php if ($programacaoEstoque->url_video){
                                if (strpos($programacaoEstoque->url_video, 'youtu.be') !== false) {
                                    $urlParts = explode('/', $programacaoEstoque->url_video);
                                    $v = end($urlParts);
                                    if (strpos($v, '?') !== false) {
                                        $v = strstr($v, '?', true);
                                    }
                                } elseif (strpos($programacaoEstoque->url_video, 'youtube.com/watch') !== false) {
                                    $queryString = parse_url($programacaoEstoque->url_video, PHP_URL_QUERY);
                                    parse_str($queryString, $params);
                                    $v = $params['v'] ?? null;
                                } elseif (strpos($programacaoEstoque->url_video, 'youtube.com/shorts') !== false) {
                                    $urlParts = explode('/', $programacaoEstoque->url_video);
                                    $v = end($urlParts);
                                    if (strpos($v, '?') !== false) {
                                        $v = strstr($v, '?', true);
                                    }
                                }
                                ?>
                                <details>
                                    <summary>VÍDEO</summary>
                                    <div style="text-align: center;">
                                        <iframe style="width: auto" height="415" src="https://www.youtube.com/embed/<?php echo $v;?>" title="YouTube video player" frameborder="0"></iframe>
                                    </div>
                                </details>
                            <?php } ?>
                            <?php if ($programacaoEstoque->details){?>
                                <details>
                                    <summary>INFORMAÇÕES IMPORTANTES</summary>
                                    <?php echo $programacaoEstoque->details;?>
                                </details>
                            <?php } ?>

                            <?php foreach ($product_details as $pDetail) {?>
                                <details>
                                    <summary><?=mb_strtoupper($pDetail->titulo);?></summary>
                                    <?=$pDetail->note;?>
                                </details>
                            <?php } ?>
                        <?php } ?>
                    </div>

                    <?php if (!$this->Settings->receptive) {?>
                        <div class="pacote-date border-top mt-4 pt-4" style="text-align: left;height: 50px;">
                                            <span>
                                                <?php if ($programacaoEstoque->icon_img) {?>
                                                    <?=$programacaoEstoque->icon_img;?>
                                                <?php } ?>
                                                Saída <?php echo $this->sma->dataToSite($programacaoEstoque->getDataSaida()); ?>
                                                <?php if ($programacaoEstoque->getDataSaida() !== $programacaoEstoque->getDataRetorno()){?>
                                                    <br/>
                                                    <?php if ($programacaoEstoque->icon_img) {?>
                                                        <?=$programacaoEstoque->icon_img;?>
                                                    <?php } ?>
                                                    Volta <?php echo $this->sma->dataToSite($programacaoEstoque->getDataRetorno()); ?>
                                                <?php } ?>
                                            </span>
                        </div>
                    <?php } ?>

                    <?php if (!empty($servicos_incluso)){ ?>
                        <div style="margin-top: 25px;" class=" border-top">
                            <?php foreach ($servicos_incluso as $servico_incluso) {?>
                                <div class="pacote-date mt-1 pt-1" style="text-align: left;height: 25px;font-size: .8rem;">
                                    <?php if ($servico_incluso->icon) {?>
                                        <span style="display: inline;overflow: hidden;margin-right: .5rem;"><?=$servico_incluso->icon;?> </span>
                                        <span style="vertical-align: sub;"><?=$servico_incluso->name;?></span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
            </a>
            <div class="border-top mt-3 pt-3">
                <div  style="display: inline-block;font-size: 14px;">
                    Gostou compartilhe
                </div>
                <div  style="display: inline-block; float: right;">
                    <?php if ($Settings->own_domain){?>
                        <a href="https://telegram.me/share/url?url=<?php echo base_url().'pacote/'.$description.'/'.$vendedor->id; ?>"  target="_blank">
                            <svg width="18px" height="18px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="#3390ec" class="bi bi-telegram">
                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
                            </svg>
                        </a>
                    <?php  } else {?>
                        <a href="https://telegram.me/share/url?url=<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" target="_blank">
                            <svg width="18px" height="18px" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="#3390ec" class="bi bi-telegram">
                                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.287 5.906c-.778.324-2.334.994-4.666 2.01-.378.15-.577.298-.595.442-.03.243.275.339.69.47l.175.055c.408.133.958.288 1.243.294.26.006.549-.1.868-.32 2.179-1.471 3.304-2.214 3.374-2.23.05-.012.12-.026.166.016.047.041.042.12.037.141-.03.129-1.227 1.241-1.846 1.817-.193.18-.33.307-.358.336a8.154 8.154 0 0 1-.188.186c-.38.366-.664.64.015 1.088.327.216.589.393.85.571.284.194.568.387.936.629.093.06.183.125.27.187.331.236.63.448.997.414.214-.02.435-.22.547-.82.265-1.417.786-4.486.906-5.751a1.426 1.426 0 0 0-.013-.315.337.337 0 0 0-.114-.217.526.526 0 0 0-.31-.093c-.3.005-.763.166-2.984 1.09z"/>
                            </svg>
                        </a>
                    <?php } ?>
                    <?php if ($Settings->own_domain){?>
                        <a href="https://api.whatsapp.com/send?text=<?php echo base_url().'pacote/'.$description.'/'.$vendedor->id; ?>" target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#4dc247" class="bi bi-whatsapp" viewBox="0 0 16 16"> <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/> </svg>
                        </a>
                    <?php  } else {?>
                        <a href="https://api.whatsapp.com/send?text=<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>"  target="_blank">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="#4dc247" class="bi bi-whatsapp" viewBox="0 0 16 16"> <path d="M13.601 2.326A7.854 7.854 0 0 0 7.994 0C3.627 0 .068 3.558.064 7.926c0 1.399.366 2.76 1.057 3.965L0 16l4.204-1.102a7.933 7.933 0 0 0 3.79.965h.004c4.368 0 7.926-3.558 7.93-7.93A7.898 7.898 0 0 0 13.6 2.326zM7.994 14.521a6.573 6.573 0 0 1-3.356-.92l-.24-.144-2.494.654.666-2.433-.156-.251a6.56 6.56 0 0 1-1.007-3.505c0-3.626 2.957-6.584 6.591-6.584a6.56 6.56 0 0 1 4.66 1.931 6.557 6.557 0 0 1 1.928 4.66c-.004 3.639-2.961 6.592-6.592 6.592zm3.615-4.934c-.197-.099-1.17-.578-1.353-.646-.182-.065-.315-.099-.445.099-.133.197-.513.646-.627.775-.114.133-.232.148-.43.05-.197-.1-.836-.308-1.592-.985-.59-.525-.985-1.175-1.103-1.372-.114-.198-.011-.304.088-.403.087-.088.197-.232.296-.346.1-.114.133-.198.198-.33.065-.134.034-.248-.015-.347-.05-.099-.445-1.076-.612-1.47-.16-.389-.323-.335-.445-.34-.114-.007-.247-.007-.38-.007a.729.729 0 0 0-.529.247c-.182.198-.691.677-.691 1.654 0 .977.71 1.916.81 2.049.098.133 1.394 2.132 3.383 2.992.47.205.84.326 1.129.418.475.152.904.129 1.246.08.38-.058 1.171-.48 1.338-.943.164-.464.164-.86.114-.943-.049-.084-.182-.133-.38-.232z"/> </svg>
                        </a>
                    <?php } ?>
                </div>
            </div>
            <?php if ($programacaoEstoque->precoExibicaoSite > 0){?>
                <?php if ($programacaoEstoque->promotion) {?>
                    <div class="border-top mt-3 pt-4" style="margin-bottom: 15px;">
                        <div class="d-flex justify-content-between">
                            <h6 class="m-0"><small style="font-size: 16px;">A partir de</small> <div style="font-size: 16px;text-decoration: line-through;"><?php echo $this->sma->formatMoney($programacaoEstoque->precoExibicaoSite);?> </div> </h6><br/>
                            <h4 class="m-0">
                                <div style="font-size: 1.6rem;"><?=$this->sma->formatMoney($programacaoEstoque->promo_price, $programacaoEstoque->simboloMoeda);?></div><small style="font-size: 10px;float: right;">por pessoa</small>
                            </h4>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="border-top mt-2 pt-4" style="margin-bottom: 15px;">
                        <div class="d-flex justify-content-between">
                            <h6 class="m-0"><small style="font-size: 16px;"><?php echo $programacaoEstoque->valor_pacote;?></small></h6>
                            <h4 class="m-0">
                                <div style="font-size: 1.6rem;"><?=$this->sma->formatMoney($programacaoEstoque->precoExibicaoSite, $programacaoEstoque->simboloMoeda);?></div><small style="font-size: 10px;float: right;">por pessoa</small>
                            </h4>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
            <?php if ($this->Settings->receptive) {?>
                <?php if ($Settings->own_domain){?>
                    <a href="<?php echo base_url().'pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;">  <button class="btn btn-primary btn-block"><i class="fa fa-plus"></i> INFORMAÇÕES E RESERVAS</button></a>
                <?php } else {?>
                    <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"><button class="btn btn-primary btn-block"> <i class="fa fa fa-plus"></i> INFORMAÇÕES E RESERVAS</button></a>
                <?php } ?>
            <?php } else if ($this->Settings->use_product_landing_page) {?>
                <?php if ($Settings->own_domain){?>
                    <a href="<?php echo base_url().'pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;">  <button class="btn btn-primary btn-block"><i class="fa fa fa-plus"></i> INFORMAÇÕES E RESERVAS</button></a>
                <?php } else {?>
                    <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"><button class="btn btn-primary btn-block"><i class="fa fa fa-plus"></i> INFORMAÇÕES E RESERVAS </button></a>
                <?php } ?>
            <?php } else if($programacaoEstoque->getTotalDisponvel() > 0) {?>
                <?php if ($programacaoEstoque->apenas_cotacao) {?>
                    <?php if ($Settings->own_domain){?>
                        <a href="<?php echo base_url().'pacote/'. $description .'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <button class="btn btn-primary btn-block"><i class="fa fa-ticket"></i> FAZER COTAÇÃO </button></a>
                    <?php } else { ?>
                        <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"><button class="btn btn-primary btn-block"> <i class="fa fa-ticket"></i> FAZER COTAÇÃO </button> </a>
                    <?php } ?>
                <?php } else { ?>
                    <?php if ($Settings->own_domain){?>
                        <a href="<?php echo base_url().'pacote/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;">  <button class="btn btn-primary btn-block">  <i class="fa fa-shopping-cart"></i>  RESERVE AGORA </button> </a>
                    <?php } else {?>
                        <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;">   <button class="btn btn-primary btn-block">  <i class="fa fa-shopping-cart"></i>  RESERVE AGORA </button> </a>
                    <?php } ?>
                <?php } ?>
            <?php } else if ($programacaoEstoque->permitirListaEmpera){?>
                <?php if ($Settings->own_domain){?>
                    <a href="<?php echo base_url().'pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <button class="btn btn-primary btn-block"> <i class="fa fa-users"></i> LISTA DE ESPERA </button>   </a>
                <?php } else {?>
                    <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/pacote/'.$description.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <button class="btn btn-primary btn-block"> <i class="fa fa-users"></i> LISTA DE ESPERA </button></a>
                <?php } ?>
            <?php } else {?>
                <a href="https://api.whatsapp.com/send?phone=55<?php echo trim($phone)?>&text=QUERO ENTRAR PARA LISTA DE ESPERA <?php echo $programacaoEstoque->name?>  - <?php echo $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida()); ?>" target="_blank" class="btn_1 white" style="width: 100%;background: #e58801;"><button class="btn btn-primary btn-block"> LISTA DE ESPERA </button></a>
            <?php } ?>
        </div>
    </div>
    </div>
<?php } ?>