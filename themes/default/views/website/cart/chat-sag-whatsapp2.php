<?php

$registrar_produto      =  $registrar_produto != null ? $registrar_produto : FALSE;

$biller_id_chat         = $vendedor->id;
$phone_chat             = $vendedor->phone;
$nome_vendedor_chat     = $vendedor->name;

if (!$biller_id_chat) {
    $biller_id_chat = $biller->id;
}

if (!$phone_chat) {
    $phone_chat = $biller->phone;
}

if (!$nome_vendedor_chat) {
    $nome_vendedor_chat = $biller->name;
}

if ($registrar_produto) {

    $programacao_id_chat    = $programacaoEstoque->id;
    $product_id_chat        = $programacaoEstoque->produtoId;
    $product_name_chat      = $product->name;
    $data_saida_chat        = $programacaoEstoque->dataSaida;

    if (!$programacao_id_chat) {
        $programacao_id_chat = $programacao->id;
    }

    if (!$product_id_chat) {
        $product_id_chat = $programacao->produto;
    }

    if (!$data_saida_chat) {
        $data_saida_chat = $programacao->dataSaida;
    }

    if (!$product_id_chat) {
        $product_id_chat = $produto->id;
    }

    if (!$product_name_chat) {
        $product_name_chat = $produto->name;
    }

    $product_name_chat = $product_name_chat.' SAÍDA '.date("d/m/Y", strtotime($data_saida_chat));

    $text_to_chat_whatsapp = 'Venho do link da Loja do Sistema e queria uma informação sobre: *'.$product_name_chat.'*.%0AEstou sendo Atendido pelo Vendedor: *'.$nome_vendedor_chat.'*';

} else {
    $text_to_chat_whatsapp = 'Venho do link da Loja do Sistema e queria uma informação sobre um produto.%0AEstou sendo Atendido pelo Vendedor: *'.$nome_vendedor_chat.'*';
}

$phone_chat = str_replace ( '(', '', str_replace ( ')', '', $phone_chat));
$phone_chat = str_replace ( '-', '',  $phone_chat);
$phone_chat = str_replace ( ' ', '',  $phone_chat);

?>

<?php if ($this->Settings->usar_captacao) {?>


    <link rel="stylesheet" href="<?= $assets ?>styles/captacao/captacao.min.css">

    <style>
        /* Estilos específicos para dispositivos móveis */
        @media only screen and (max-width: 768px) {
            .oc__body_cart {
                width: 200px;
                position: fixed;
                margin: 0px 10px 0px;
                z-index: 15;
                right: -10px;
                bottom: 95px;
            }

            .oc-button__content {
                position: absolute;
                bottom: 0;
                left: 0;
                width: 100%;
                background: #28a745 center top;
                background-size: cover;
                border-top-left-radius: 0px;
                border-top-right-radius: 0px;
            }

            .oc-button__header {
                position: relative;
                height: 0px;
                color: #000000;
                cursor: pointer;
            }

            .oc-button__wrapper {
                position: relative;
                width: 100%;
                height: 0px;
                background-color: #28a745;
                font-family: Helvetica, Arial, sans-serif;
                border-top-left-radius: 0px;
                border-top-right-radius: 0px;
                z-index: 10;
            }

            .oc-button__header span i {
                position: absolute;
                top: 50%;
                left: 85%;
                display: block;
                margin-top: -35px;
                width: 45px;
                height: 45px;
                background-size: 100%;
            }

            .invisible_text{
                color: #FFFFFF;
                opacity: 0;
            }
        }
    </style>
    <div class="oc__body_cart">
        <div class="oc-button__wrapper">
            <div class="oc-button__content">
                <div class="oc-button__header">
                    <span> <div class="invisible_text">Chame no WhatsApp</div><i></i></span>
                </div>
                <div class="oc-button__form">
                    <div class="oc-button__form-inputs" style="display: block;">
                        <input class="oc-button__input [ oc--tel_biller ]"      type="hidden"    value="<?=$phone_chat;?>">
                        <input class="oc-button__input [ oc--biller_id ]"       type="hidden"    value="<?=$biller_id_chat;?>">
                        <input class="oc-button__input [ oc--programacao_id ]"  type="hidden"    value="<?=$programacao_id_chat;?>">
                        <input class="oc-button__input [ oc--product_id ]"      type="hidden"    value="<?=$product_id_chat;?>">
                        <input class="oc-button__input [ oc--text_chat ]"       type="hidden"    value="<?=$text_to_chat_whatsapp;?>">
                        <input class="oc-button__input [ oc--base_url ]"        type="hidden"    value="<?=base_url();?>">

                        <?php
                        $this->db->where('active', 1);
                        $departaments = $this->db->get('departments');
                        if ($departaments->num_rows() > 0) {
                            foreach (($departaments->result()) as $row) {
                                $departamentsArray[] = $row;
                            }
                        } ?>
                        <?php if (!empty($departamentsArray)) {?>
                            <select class="oc-button__input [ oc--department_id ]" >
                                <?php foreach ($departamentsArray as $departament) { ?>
                                    <option value="<?=$departament->id;?>"><?=$departament->name;?></option>
                                <?php } ?>
                            </select>
                        <?php } ?>
                        <input class="oc-button__input [ oc--name ]" type="text" placeholder="Nome">
                        <input class="oc-button__input [ oc--phone ]" type="tel" placeholder="ex.: (99) 9999-9999">
                        <button class="oc-button__button">Solicitar atendimento</button>
                    </div>
                    <div class="oc-button__form-response" style="display: none;">
                        <p></p>
                    </div>
                </div>
            </div>
            <script> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"><\/script>')</script>
            <script src="<?= $assets ?>js/captacao/jquery.inputmask.min.js"></script>
            <script src="<?= $assets ?>js/captacao/captacao.js"></script>
        </div>
    </div>
    <?php } else {
        $current_url = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $text_to_chat_whatsapp = $text_to_chat_whatsapp.'. *Página de Origem:*'.$current_url;
    ?>

    <!-- WhatsApp -->
    <div class="whatsapp">
        <a href="https://api.whatsapp.com/send?phone=55<?php echo trim($phone_chat)?>&text=<?=$text_to_chat_whatsapp;?>" target="_blank">
            <svg class="icon_whatsapp" viewBox="0 0 32 32" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="fill: rgb(255, 255, 255); stroke: none;"><path d="M19.11 17.205c-.372 0-1.088 1.39-1.518 1.39a.63.63 0 0 1-.315-.1c-.802-.402-1.504-.817-2.163-1.447-.545-.516-1.146-1.29-1.46-1.963a.426.426 0 0 1-.073-.215c0-.33.99-.945.99-1.49 0-.143-.73-2.09-.832-2.335-.143-.372-.214-.487-.6-.487-.187 0-.36-.043-.53-.043-.302 0-.53.115-.746.315-.688.645-1.032 1.318-1.06 2.264v.114c-.015.99.472 1.977 1.017 2.78 1.23 1.82 2.506 3.41 4.554 4.34.616.287 2.035.888 2.722.888.817 0 2.15-.515 2.478-1.318.13-.33.244-.73.244-1.088 0-.058 0-.144-.03-.215-.1-.172-2.434-1.39-2.678-1.39zm-2.908 7.593c-1.747 0-3.48-.53-4.942-1.49L7.793 24.41l1.132-3.337a8.955 8.955 0 0 1-1.72-5.272c0-4.955 4.04-8.995 8.997-8.995S25.2 10.845 25.2 15.8c0 4.958-4.04 8.998-8.998 8.998zm0-19.798c-5.96 0-10.8 4.842-10.8 10.8 0 1.964.53 3.898 1.546 5.574L5 27.176l5.974-1.92a10.807 10.807 0 0 0 16.03-9.455c0-5.958-4.842-10.8-10.802-10.8z"></path></svg>
        </a>
    </div>
    <!-- WhatsApp End -->
<?php } ?>
