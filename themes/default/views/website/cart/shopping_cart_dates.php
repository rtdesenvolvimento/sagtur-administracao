<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php echo $this->Settings->site_name;?> || RESERVA DE PASSAGENS</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#e58800">
    <meta name="theme-color" content="#e58800"/>

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $this->Settings->site_name;?> || RESERVA DE PASSAGENS" >
    <meta name="keywords" content="<?php echo $this->Settings->site_name;?>">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $this->Settings->site_name;?> || RESERVA DE PASSAGENS">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>

    <?php
    $category  = $this->site->getCategoryByID($produto->category_id);
    ?>

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $this->Settings->site_name;?>"  />
    <meta property="og:description" content="RESERVA ONLINE || <?=str_replace("'", "", $produto->name);?>" />
    <meta property="og:site_name" content="<?php echo $this->Settings->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $this->Settings->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="300">
    <meta property="og:image:height" content="300">

    <link rel="canonical" href="<?php echo base_url();?>">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">


    <!--FULLCALENDAR -->
    <link href='<?= $assets ?>fullcalendar/css/fullcalendar.min.css' rel='stylesheet' />
    <link href='<?= $assets ?>fullcalendar/css/fullcalendar.print.css' rel='stylesheet' media='print' />
    <link href="<?= $assets ?>fullcalendar/css/bootstrap-colorpicker.min.css" rel="stylesheet" />

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

    <style>

        body {
            background: #fbfafd;
        }
        .content-left {
            background-color: #929eaa;
            padding: 0;
        }

        .barra-progresso{width: 320px;height: 24px;background-color: #bbb;border-radius: 5px;padding: 3px;margin: 10px auto;}
        .porcentagem{width: 100%;height: 17px;border-radius: 5px;background-color: #e99419;}

        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
            width: 100%;
        }

        a.btn_1.rounded, .btn_1.rounded {
            -webkit-border-radius: 3px !important;;
            -moz-border-radius: 3px !important;
            border-radius: 3px !important;
            -webkit-box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 20%);
            -moz-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 20%);
        }

        .content-left-wrapper{
            padding: 15px 20px 17px 20px;
        }

        @media (max-width: 767px)
            .content-left-wrapper .btn_1.mobile_btn {
                margin: 5px 0 1px 0;
                display: inline-block;
            }

            .content-left-wrapper .btn_1 {
                margin: 0px 0 10px 0;
            }


            .cart_destaque {
                border: 1px solid #dcdcdc;
                padding: 15px 15px 15px 15px;
                margin-bottom: 0;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                background: #ffff;
                border-radius: 5px;
            }

            .cart_destaque label {
                font-weight: 500;
                font-size: 18px;
            }

            .cart_destaque label div {
                font-weight: 400;
                font-size: 15px;
                color: #28a745;
                margin-top: 10px;
            }

            .cart_cartao_transpartente {
                border: 1px solid #dcdcdc;
                padding: 15px 15px 15px 15px;
                margin-bottom: 0;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                background: #ffff;
                border-radius: 5px;
            }

            .cart_cartao_transpartente label {
                font-weight: 500;
            }

            .container_radio.version_2 {
                padding: 0px 0 0 45px;
                min-height: 1px !important;
            }

            .container_radio input:checked ~ .checkmark:after {
                opacity: 1;
            }

            .container_radio.version_2 .checkmark:after {
                width: 30px;
                height: 30px;
                top: -1px;
                left: -1px;
            }

            .container_radio .checkmark:after {
                display: block;
                content: "";
                position: absolute;
                opacity: 0;
                -moz-transition: all 0.3s ease-in-out;
                -o-transition: all 0.3s ease-in-out;
                -webkit-transition: all 0.3s ease-in-out;
                -ms-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
                top: 3px;
                left: 3px;
                width: 12px;
                height: 12px;
                border-radius: 50%;
                background: #e58801;
            }

            .budget_slider {
                background-color: #e9ecef;
                margin-bottom: 20px;
                padding: 1px 20px 14px 0px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                margin-top: 15px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                border-left: solid 5px #28a74557 !important;
            }

            .price_adicionais {
                color: #6c757d;
                font-size: 14px;
                line-height: 10px;
                padding: 5px;
                font-weight: 400;
            }

            .budget_slider_adicionais_add {
                background: #FFFFFF;
                margin-bottom: 20px;
                padding: 10px 10px 5px 10px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            .budget_slider_adicionais {
                background: #FFFFFF;
                margin-bottom: 20px;
                padding: 10px 10px 5px 10px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            .mais_informacoes_adicional {
                margin-bottom: 20px;
                margin-left: 15px;
            }

            .btn_selecionar_opcional {
                background: #e58801;
                color: #ffff;
                width: 100%;
                border: none;
                text-decoration: none;
                transition: background .5s ease;
                -moz-transition: background .5s ease;
                -webkit-transition: background .5s ease;
                -o-transition: background .5s ease;
                display: inline-block;
                cursor: pointer;
                outline: none;
                text-align: center;
                /* background: #434bdf; */
                position: relative;
                font-size: 0.875rem;
                font-weight: 600;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                line-height: 1;
                padding: 12px 30px;
                margin-bottom: 15px;
            }

            .profile_img {
                padding: 17px;
                width: 200px;
            }

            .error {
                border: 1px solid red;
            }

            .combo {
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                background-color: #fff;
                border-radius: 3px;
                border: 1px solid #d2d8dd;
                box-sizing: border-box;
                clear: both;
                cursor: pointer;
                display: block;
                float: left;
                font-family: inherit;
                font-size: 14px;
                font-weight: normal;
                height: 42px;
                line-height: 40px;
                outline: none;
                padding-left: 5px;
                padding-right: 0px;
                position: relative;
                text-align: left;
                transition: all 0.2s ease-in-out;
                user-select: none;
                white-space: nowrap;
                width: 100%;
                color: #6c757d;
            }

            .combo2
            {
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
            }

            .label_valores {
                font-size: 16px;
                font-weight: 400;
                line-height: 1;
                margin: 0 0 5px;
                color: black;
            }

            .cc_fieldset {
                font-size: 1.1em;
                font-weight: 500;
                text-align: left;
                width: auto;
                color: #428BCA;
                border: 1px solid #DBDEE0;
                margin: 0;
                border-radius: 5px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                border-left: solid 5px #dc354596;
                margin-bottom: 20px;
                cursor: pointer;
                background: #FFFFFF;
            }

            .imgfilters {
                float: right;
                width: 18px;
            }

            /* Tablet Portrait (devices and browsers)
            ====================================================================== */
            @media only screen and (min-width: 768px) and (max-width: 991px) {
                a#main-menu-toggle {
                    margin-left: 8.334%;
                }
                a.navbar-brand {

                    padding: 8px 0px !important;
                    position: absolute;
                    left: 15px;
                }
                a.navbar-brand span {
                    font-size: 18px;
                }
                .navbar-collapse {
                    max-height: 100%;
                }
                .container {
                    width: 100% !important;
                }
                .container #content {
                    padding: 15px;
                }
                .container .breadcrumb {
                    margin: -15px -15px 15px -15px;
                    padding: 10px;
                }
                .btn-navbar {
                    display: none !important;
                }
                .padding05 {
                    padding: 0;
                }
                .lt td.sidebar-con {
                    width: 40px;
                }
                .content-scroll { width: 300px; }
            }

            .botoes_avanca_confirma {
                /*
                display: block;
                position: fixed;
                                 */
                width: 100%;
                bottom: 0vh;
                left: 0;
                background: white;
                padding: 5px 15px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            /* All Mobile Sizes (devices and browser)
            ====================================================================== */
            @media only screen and (max-width: 767px) {
                .apresentacao_viagem {
                    /*
                    display: none;*/
                }
            }

            .form-control {
                display: block;
                width: 100%;
                padding: 0.375rem 0.75rem;
                font-size: 1rem;
                font-weight: 400;
                line-height: 1.5;
                color: #212529;
                background-color: #fff;
                background-clip: padding-box;
                border: 1px solid #ced4da;
                appearance: none;
                border-radius: 0.375rem;
                transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
            }

            .fc-day-grid-event {
                padding: 5px;
                cursor: pointer;
                font-weight: 800;
                border-radius: 0px;
            }

            .fc td, .fc th {
                border-style: solid;
                border-width: 1px;
                padding: 0;
                vertical-align: top;
                text-align: center;
                text-transform: uppercase;
            }

            .fc-ltr .fc-basic-view .fc-day-number {
                text-align: center;
            }

        .fc-title {
            color: #ffffff;
        }

        .fc td, .fc th {
            border-style: solid;
            border-width: 0px;
            padding: 0;
            vertical-align: top;
            text-align: center;
        }
        .fc {
            direction: ltr;
            text-align: left;
            border: 1px solid #dcdcdc;
        }

        h2 {
            color: #ffffff;
            font-size: 25px;
        }

    </style>

</head>

<body>

    <?php if ($this->Settings->body_code){?>
        <?=$this->Settings->body_code;?>
    <?php } ?>

    <div id="preloader">
        <div data-loader="circle-side"></div>
    </div>

    <header style="background: #6c757d;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <?php if ($this->Settings->url_site && !$Settings->own_domain) {?>
                        <a href="<?php echo $Settings->url_site; ?>" class="btn_1" style="padding: 10px 0px;background: #6c757d;font-size: 15px;"><span class="fa fa-home"></span> Início</a>
                    <?php } else { ?>
                        <?php if ($Settings->own_domain){?>
                            <a href="<?php echo base_url().$vendedor->id;?>" class="btn_1" style="padding: 10px 0px;background: #6c757d;font-size: 15px;"><span class="fa fa-home"></span> Início</a>
                        <?php } else { ?>
                            <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id;?>" class="btn_1" style="padding: 10px 0px;background: #6c757d;font-size: 15px;"><span class="fa fa-home"></span> Início</a>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <!-- /container -->
    </header>

    <?php $attrib = array('id' => 'wrapped');echo form_open("#", $attrib);  ?>
        <div class="container-fluid full-height">
            <div class="row row-height">
                <div class="col-lg-6 content-left apresentacao_viagem">
                    <div class="content-left-wrapper">
                        <div>
                            <figure>
                                <img src="<?php echo base_url().'/assets/uploads/'.$produto->image; ?>" alt="<?php echo $produto->name;?>" class="img-fluid"/>
                            </figure>
                            <h2><?php echo $produto->name?></h2>
                            <hr style="margin: 10px 0 10px 0;">
                            <a href="#" class="btn_1 rounded" data-toggle="modal" data-target="#more-information" style="background: #e58801;"> <span class="fa fa-plus-circle"></span> MAIS DETALHES</a>
                            <a href="#" class="btn_1 rounded mobile_btn" data-toggle="modal" data-target="#more-information" style="background: #e58801;"><span class="fa fa-plus-circle"></span> MAIS DETALHES</a>
                        </div>
                        <div class="copy" style="bottom: 0px;">&copy; <?= date('Y')?> <a href="https://www.sagtur.com.br" style="color: #ffffff;" target="_blank">Desenvolvido por SAGTur Sistemas</a><?php echo " | Licenciado para " . $this->Settings->site_name; ?></div>
                    </div>
                    <!-- /content-left-wrapper -->
                </div>
                <!-- /content-left -->
                <div class="col-lg-6 content-right" id="start">
                    <div id="wizard_container">
                        <div style="text-align: center;margin-bottom: 30px;margin-bottom: 5px;border: 1px solid #dcdcdc;border-radius: 5px;padding: 5px 0px 10px 0px;;background: #ffffff;box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;">
                            <div style="text-align: center;">Atendido por <span style="font-weight: bold"><?php echo $vendedor->name;?></span></div>
                        </div>
                        <div id="top-wizard">
                            <div id="progressbar"></div>
                        </div>
                        <!--SELECT DATES -->
                        <div id="middle-wizard">
                            <div class="submit step">
                                <h3 class="main_question div_calendar">Escolha Um Dia Disponível:</h3>
                                <?php if ($produto->precoExibicaoSite > 0){?>
                                    <?php if ($produto->promotion) {?>
                                        <div class="border-top mt-4 pt-4 cart_destaque" style="margin-bottom: 15px;">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="m-0"><small style="font-size: 16px;text-decoration: line-through;">De <?php echo $this->sma->formatMoney($produto->precoExibicaoSite);?></small></h6><br/>
                                                <h4 class="m-0">
                                                    <small style="font-size: 10px;">por</small> <?php echo  $this->sma->formatMoney($produto->promo_price, $programacao->simboloMoeda);?><small style="font-size: 10px;">/por pessoa</small>
                                                </h4>
                                            </div>
                                        </div>
                                    <?php } else { ?>
                                        <div class="border-top mt-4 pt-4 cart_destaque" style="margin-bottom: 15px;">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="m-0"><small style="font-size: 16px;"><?php echo $produto->valor_pacote;?></small></h6>
                                                <h4 class="m-0">
                                                    <?php echo  $this->sma->formatMoney($produto->precoExibicaoSite, $produto->simboloMoeda);?><small style="font-size: 10px;">/por pessoa</small>
                                                </h4>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="select_horarios">
                                    <div class="form-group cart_destaque">
                                        <div id="calendar"></div>
                                    </div>
                                    <h3 class="main_question" id="h3_horarios" style="margin-top: 25px;">Escolha O Melhor Horário:</h3>
                                    <div class="form-group">
                                        <div class="col-12">
                                            <div id="div_horarios"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /step-->
                        </div>
                        <!-- /middle-wizard -->
                        <header id="bottom-wizard">
                            <div class="row">
                                <div class="col-6"></div>
                                <div class="col-6">
                                    <button type="button" style="width: 100%;padding: 12px 0px;"  onclick="proximo_passo();" class="forward">PRÓXIMO PASSO <span class="fa fa-arrow-right"></span></button>
                                </div>
                            </div>
                        </header>
                        <!-- /bottom-wizard -->
                    </div>
                    <!-- /Wizard container -->
                </div>
                <!-- /content-right-->
            </div>
            <!-- /row-->
        </div>
        <!-- /container-fluid -->
    <?php echo form_close(); ?>

    <div class="cd-overlay-nav"><span></span></div>
    <!-- /cd-overlay-nav -->

    <div class="cd-overlay-content"><span></span></div>
    <!-- /cd-overlay-content -->

    <!-- Modal terms -->
    <div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <style>
                        p {
                            margin-bottom: revert;
                        }

                        strong {
                            font-weight: bold;
                        }
                    </style>

                    <?php
                    $aceistes_contrato_label = explode('@label@', $this->Settings->termos_aceite); ?>
                    <?php
                    $contadorAceite = 0;
                    foreach ($aceistes_contrato_label as $labelAceite){
                        if ($labelAceite != '') {
                            $labelAceite = explode('@quebra@', $labelAceite); ?>
                            <div class="panel-heading">
                                <?php echo $labelAceite[0];?>
                            </div>
                            <p><?php echo $labelAceite[1];?></p>
                        <?php } ?>
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal terms -->
    <div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="col-sm-12">
                        <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                        <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                        <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                        <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                        <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                        <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- COMMON SCRIPTS -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>

    <!--FULFCALENDAR SCRIPTS -->
    <script src='<?= $assets ?>fullcalendar/js/moment.min.js'></script>
    <script src="<?= $assets ?>fullcalendar/js/fullcalendar.min.js"></script>
    <script src="<?= $assets ?>fullcalendar/js/lang-all.js"></script>
    <script src='<?= $assets ?>fullcalendar/js/bootstrap-colorpicker.min.js'></script>

    <script type="application/javascript">

        $(document).ready(function() {

            let currentLangCode = 'pt-br';
            let events = [];

            jQuery.extend(jQuery.validator.messages, {
                required: "Campo é obrigatório.",
            });

            <?php foreach ($programacoes as $prog) {?>
                <?php if($prog->getTotalDisponvel() > 0) {?>
                    <?php if($prog->apenas_cotacao) {?>
                        if (!events.find(obj => obj.start === '<?=$prog->getDataSaida();?>') ) {
                            events.push(
                                {
                                    "product_id": <?=$prog->produtoId;?>,
                                    "color": "#eeeef0",
                                    'textColor ': '#ffffff',
                                    'borderColor': '#3a87ad',
                                    'backgroundColor': '#3a87ad',
                                    "title": "ORÇAMENTO",
                                    "start": '<?=$prog->getDataSaida();?>',
                                }
                            );
                        }
                    <?php } else { ?>
                        if (!events.find(obj => obj.start === '<?=$prog->getDataSaida();?>') ) {
                            events.push(
                                {
                                    "product_id": <?=$prog->produtoId;?>,
                                    "color": "#eeeef0",
                                    'textColor ': '#ffffff',
                                    'borderColor': '#e58801',
                                    'backgroundColor': '#e58801',
                                    "title": "DISPONÍVEL",
                                    "start": '<?=$prog->getDataSaida();?>',
                                }
                            );
                        }
                    <?php } ?>
                <?php } else if ($prog->permitirListaEmpera) { ?>
                    if (!events.find(obj => obj.start === '<?=$prog->getDataSaida();?>') ) {
                        events.push(
                            {
                                "product_id": <?=$prog->produtoId;?>,
                                "color": "#eeeef0",
                                'textColor ': '#ffffff',
                                'borderColor': '#28a745',
                                'backgroundColor': '#28a745',
                                "title": "ESPERA",
                                "start": '<?=$prog->getDataSaida();?>',
                            }
                        );
                    }
                <?php } ?>
            <?php } ?>

            $('#calendar').fullCalendar(
                {
                    lang: currentLangCode,
                    events: events,
                    defaultDate: '<?php echo $programacao->dataSaida;?>', // will be parsed as local
                    header: {
                        left: 'prev,next,today',
                        center: 'title',
                    },
                    // Handle Existing Event Click
                    eventClick: function(calEvent, jsEvent, view) {

                        $('#div_horarios').html('<div style="text-align: center;"><h4>Carregando Horários...</h4></div>');
                        $('#h3_horarios').hide();
                        $('.forward').hide();
                        $('#horarios').empty();

                        $.ajax({
                            type: "GET",
                            url: '<?php echo base_url();?>appcompra/search_schedule/',
                            data: {
                                product_id: calEvent.product_id,
                                date_shedule : calEvent.start._i,
                                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                            },
                            dataType: 'json',
                            success: function (programacoes) {

                                $('#horarios').empty();
                                if (programacoes !== null) {
                                    if (programacoes.length > 1) {
                                        $('#horarios').append(new Option('Selecionar um horário de saída', '', false, false));
                                    }

                                    let html = '';
                                    $(programacoes).each(function( index, programacao ) {

                                        if (programacao.estoque > 0) {

                                            let horario = 'Saída ' + programacao.horaSaida + ' até ' + programacao.horaRetorno;
                                            $('#horarios').append(new Option(horario, programacao.programacaoId, false, false));

                                            let dataSaida = programacao.dataSaida.split('-').reverse().join('/');
                                            let dataRetorno = programacao.dataRetorno.split('-').reverse().join('/');
                                            let horaSaida = programacao.horaSaida.split(':')[0] + 'h' + programacao.horaSaida.split(':')[1] + 'm';
                                            let horaRetorno = programacao.horaRetorno.split(':')[0] + 'h' + programacao.horaRetorno.split(':')[1] + 'm';

                                            let horarioExibir = '';
                                            let dataExibir = '';

                                            if (dataSaida !== dataRetorno) {
                                                dataExibir = 'Saída ' + dataSaida + ' Retorno ' + dataRetorno;
                                            } else {
                                                dataExibir = dataSaida;
                                            }

                                            if (horaSaida !== horaRetorno) {
                                                horarioExibir = 'Saída ' + horaSaida + ' até ' + horaRetorno;
                                            } else {
                                                horarioExibir = 'Saída ' + horaSaida;
                                            }

                                            if (programacoes.length > 1) {
                                                html += '<div class="row cart_destaque">';
                                                html += '   <div class="col-12"><h5>' + dataExibir + '</h5>';
                                                html += '       <label class="container_radio version_2">' + horarioExibir;
                                                html += '           <input type="radio" name="horarios" required="required" value="' + programacao.programacaoId + '" class="required">';
                                                html += '           <span class="checkmark"></span>';
                                                html += '       </label>';
                                                html += '   </div>';
                                                html += '</div>';
                                            } else {
                                                html += '<div class="row cart_destaque">';
                                                html += '   <div class="col-12"><h5>' + dataExibir + '</h5>';
                                                html += '       <label class="container_radio version_2">' + horarioExibir;
                                                html += '           <input type="radio" name="horarios" checked="checked" required="required" value="' + programacao.programacaoId + '" class="required">';
                                                html += '           <span class="checkmark"></span>';
                                                html += '       </label>';
                                                html += '   </div>';
                                                html += '</div>';
                                            }
                                        }
                                    });

                                    $('#div_horarios').html(html);
                                    $('#h3_horarios').show();
                                    $('.forward').show();

                                    setTimeout(function(){
                                        document.getElementById('div_horarios').scrollIntoView();
                                    },300);
                                }
                            }
                        });
                    }
                },
            );

            $('.fc-next-button')[0].style.display = 'none';
            $('.fc-prev-button')[1].style.display = 'none';

            $('.fc-widget-content').css('height', '15px');
            $('.fc-toolbar').css('background', '#333');
            $('.fc-toolbar').css('padding', '25px');
            $('.fc-toolbar').css('text-transform', 'uppercase');

            $('#preloader').hide();
            $('.forward').hide();
            $('#h3_horarios').hide();

            setInterval(function (){
                $('.fc-widget-content').css('height', '15px');
                $('.fc-scroller').css('height', 'auto');
                }, 100);

            setTimeout(function(){
                document.getElementsByClassName('div_calendar')[0].scrollIntoView();
                $('.fc-today-button').hide();
            },300);
        });

        function proximo_passo() {
            if ($('#wrapped').valid()) {
                let programacao_id = $('input[name="horarios"]:checked').val();
                let url = '';

                <?php if ($Settings->own_domain){?>
                    url = '<?php echo base_url().'carrinho_compra/'?>' + programacao_id + '<?php echo '/'.$vendedor->id; ?>';
                    window.location = url;
                <?php } else { ?>
                    url = '<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho_compra/'?>' + programacao_id + '<?php echo '/'.$vendedor->id; ?>';
                    window.location = url;
                <?php } ?>
            }
        }
    </script>

</body>
</html>
