<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file-text-o"></i><?= lang('indicators'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('mrr_total_ano_anterior'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $this->sma->formatMoney($arr_ano_aterior); ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('mrr_total'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $this->sma->formatMoney($arr); ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('mrr_percentual_crescimento'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $this->sma->formatQuantity($percentualCrescimento).'%'; ?></h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <canvas id="arrChart"></canvas>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('total_clientes_inativos'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $inactive_customers; ?></h1>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('total_clientes_ativos'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $active_customers; ?></h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><?= lang('total_clientes_contratados_ano_pasado'); ?></h3>
                    </div>
                    <div class="panel-body text-center">
                        <h1><?= $customers_last_year; ?></h1>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <h1><?= lang('arr_agrupado_por_grupo'); ?></h1>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Grupo</th>
                        <th>Total (R$)</th>
                        <th>Total Clientes</th>
                    </tr>
                    </thead>
                    <tbody id="tabela-indicadores">
                    <?php
                    $total_clientes = $arr_chart_group['total_clientes'];
                    $num_grupos = count($arr_chart_group['labels']); // Conta o número de grupos

                    for ($i = 0; $i < $num_grupos; $i++) {
                        $totalRecebimento = $arr_chart_group['dados'][$i];

                        ?>
                        <tr>
                            <td><?= htmlspecialchars($arr_chart_group['labels'][$i]) ; ?></td>
                            <td><?= $this->sma->formatMoney($totalRecebimento); ?></td>
                            <td><?= $total_clientes ; ?></td>
                        </tr>
                    <?php } ?>
                    <tfoot>
                    <tr>
                        <td><strong><?= lang('total'); ?></strong></td>
                        <td><strong><?= $this->sma->formatMoney(array_sum($arr_chart_group['dados'])); ?></strong></td>
                        <td><strong><?= $total_clientes; ?></strong></td>
                    </tr>
                    </tfoot>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <canvas id="ARRAgrupadoPorGrupo"></canvas>
            </div>
        </div>
    </div>
</div>
<script language="javascript">
    $(document).ready(function () {
        arrChart();
        ARRAgrupadoPorGrupo();
    });

    function arrChart() {

        const data = <?= json_encode($arr_chart);?>;

        const labels = Object.keys(data); // Meses
        const arrValues = Object.values(data).map(item => item.arr); // Valores ARR
        const novosContratosValues = Object.values(data).map(item => item.novosContratos); // Valores Novos Contratos

        const ctx = document.getElementById('arrChart').getContext('2d');
        new Chart(ctx, {
            type: 'bar', // ou 'line' se preferir gráfico de linha
            data: {
                labels: labels,
                datasets: [
                    {
                    label: 'ARR (R$)',
                    data: arrValues,
                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                    borderColor: 'rgba(75, 192, 192, 1)',
                    borderWidth: 1
                    },
                    {
                        label: 'Novos Contratos (R$)',
                        data: novosContratosValues,
                        backgroundColor: 'rgba(54, 162, 235, 0.2)',
                        borderColor: 'rgba(54, 162, 235, 1)',
                        borderWidth: 1
                    }
                ]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }

    function ARRAgrupadoPorGrupo() {

        const data = <?= json_encode($arr_chart_group);?>;
        const labels = data.labels; // Meses
        const dados = data.dados; // Valores Novos Contratos

        const ctx = document.getElementById('ARRAgrupadoPorGrupo').getContext('2d');
        new Chart(ctx, {
            type: 'pie', // Tipo de gráfico: pizza
            data: {
                labels: labels, // Labels (customer_group)
                datasets: [{
                    data: dados, // Valores (totalRecebimentoGrupo)
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ], // Cores para cada segmento
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                plugins: {
                    legend: {
                        position: 'top',
                    },
                    tooltip: {
                        callbacks: {
                            label: function(tooltipItem) {
                                return tooltipItem.label + ': R$' + tooltipItem.raw.toFixed(2);
                            }
                        }
                    }
                }
            }
        });
    }
</script>

