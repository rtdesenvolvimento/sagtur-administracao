<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('contract_settings'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="<?= site_url('contract_settings/plugsign') ?>" class="toggle_up">
                        <i class="icon fa fa-code"></i><span class="padding-right-10"><?= lang('configurar_plugsign'); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
            <?php echo form_open("contract_settings"); ?>
            <div class="row">

                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("default_biller_contract", "biller"); ?>
                            <?php
                            $bl[""] = "";
                            foreach ($billers as $biller) {
                                $bl[$biller->id] = $biller->name;
                            }
                            echo form_dropdown('default_biller_contract', $bl, $Settings->default_biller_contract, 'id="default_biller_contract" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= lang("contract", "contract_id"); ?>
                            <?php
                            $cbC[""] = lang('select').' '.lang('contract');
                            foreach ($contracts as $contract) {
                                $cbC[$contract->id] = $contract->name;
                            }
                            echo form_dropdown('contract_id', $cbC, $this->Settings->contract_id, 'class="form-control" id="contract_id" required="required" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("contract") . '"');
                            ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group all">
                            <?= lang("note", "note"); ?>
                            <?=form_textarea('note', $Settings->note_contract, 'id="note" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('note_placeholder') . '"');?>
                        </div>
                    </div>
                    <div class="col-md-12" style="display: none;">
                        <div class="form-group all">
                            <?php echo form_checkbox('monitorar_validade', '1', $Settings->monitorar_validade, 'id="monitorar_validade"'); ?>
                            <label for="attributes" class="padding05"><?= lang('monitorar_validade'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-4" id="div_validade_meses" style="<?php if (!$Settings->monitorar_validade) echo 'display: none;';?>">
                        <div class="form-group all">
                            <?= lang("validade_meses", "validade_meses") ?>
                            <?= form_input('validade_meses', $Settings->validade_meses, 'class="form-control tip mask_integer" id="validade_meses" '); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('auto_destruir_solicitacao', '1', $Settings->auto_destruir_solicitacao, 'id="auto_destruir_solicitacao"'); ?>
                            <label for="attributes" class="padding05"><?= lang('auto_destruir_solicitacao'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-4" id="validade_auto_destruir_dias" style="<?php if (!$Settings->auto_destruir_solicitacao) echo 'display: none;';?>">
                        <div class="form-group all">
                            <?= lang("validade_auto_destruir_dias", "validade_auto_destruir_dias") ?>
                            <?= form_input('validade_auto_destruir_dias', $Settings->validade_auto_destruir_dias, 'class="form-control tip mask_integer" id="validade_auto_destruir_dias" '); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('solicitar_selfie', '1', $Settings->solicitar_selfie, 'id="solicitar_selfie"'); ?>
                            <label for="attributes" class="padding05"><?= lang('solicitar_selfie'); ?></label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('solicitar_documento', '1', $Settings->solicitar_documento, 'id="solicitar_documento"'); ?>
                            <label for="attributes" class="padding05"><?= lang('solicitar_documento'); ?></label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('is_observadores', '1', $Settings->is_observadores, 'id="is_observadores"'); ?>
                            <label for="attributes" class="padding05"><?= lang('is_observadores'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12" id="div_observadores" style="<?php if (!$Settings->is_observadores) echo 'display: none;';?>">
                        <div class="form-group all">
                            <?= lang("observadores", "observadores") ?>
                            <?=form_textarea('observadores', $Settings->observadores, 'id="observadores" class="form-control kb-text skip" style="height: 100px;width: 660px;" placeholder="' . lang('observadores') . '"');?>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("signature_type", "signature_type") ?>
                            <?php
                            $ptsType = array(
                                'all' => lang('all') ,
                                'text' => lang('text'),
                                'draw' => lang('draw'),
                                'upload' => lang('upload'),
                            );
                            echo form_dropdown('signature_type', $ptsType, $Settings->signature_type , 'class="form-control" id="signature_type"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12" style="display: none;">
                        <div class="form-group all">
                            <?php echo form_checkbox('enviar_sequencial', '1', $Settings->enviar_sequencial, 'id="enviar_sequencial"'); ?>
                            <label for="attributes" class="padding05"><?= lang('enviar_sequencial'); ?></label>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('gerar_contrato_faturamento_venda_site', '1', $Settings->gerar_contrato_faturamento_venda_site, 'id="gerar_contrato_faturamento_venda_site"'); ?>
                            <label for="attributes" class="padding05"><?= lang('gerar_contrato_faturamento_venda_site'); ?></label>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('gerar_contrato_faturamento_venda', '1', $Settings->gerar_contrato_faturamento_venda, 'id="gerar_contrato_faturamento_venda"'); ?>
                            <label for="attributes" class="padding05"><?= lang('gerar_contrato_faturamento_venda'); ?></label>
                        </div>
                    </div>

                    <div class="col-md-12" id="div_enviar_contrato_signatarios_venda" style="<?php if (!$Settings->gerar_contrato_faturamento_venda) echo 'display: none;';?>">
                        <div class="form-group all">
                            <?php echo form_checkbox('enviar_contrato_signatarios_venda', '1', $Settings->enviar_contrato_signatarios_venda, 'id="enviar_contrato_signatarios_venda"'); ?>
                            <label for="attributes" class="padding05"><?= lang('enviar_contrato_signatarios_venda'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12" id="div_auto_assinar_customer_contrato" style="<?php if (!$Settings->gerar_contrato_faturamento_venda) echo 'display: none;';?>">
                        <div class="form-group all">
                            <?php echo form_checkbox('auto_assinar_customer_contrato', '1', $Settings->auto_assinar_customer_contrato, 'id="auto_assinar_customer_contrato"'); ?>
                            <label for="attributes" class="padding05"><?= lang('auto_assinar_customer_contrato'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('auto_assinar_biller_contrato', '1', $Settings->auto_assinar_biller_contrato, 'id="auto_assinar_biller_contrato"'); ?>
                            <label for="attributes" class="padding05"><?= lang('auto_assinar_biller_contrato'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?php echo form_checkbox('cancel_contract_by_sale', '1', $Settings->cancel_contract_by_sale, 'id="cancel_contract_by_sale"'); ?>
                            <label for="attributes" class="padding05"><?= lang('cancel_contract_by_sale'); ?></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="controls">
                                <?php echo form_submit('update_contract_settings', lang("update_contract_settings"), 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        $('#monitorar_validade').on('ifChecked', function (e) {$('#div_validade_meses').show(300);});
        $('#monitorar_validade').on('ifUnchecked', function (e) {$('#div_validade_meses').hide(300);});

        $('#auto_destruir_solicitacao').on('ifChecked', function (e) {$('#validade_auto_destruir_dias').show(300);});
        $('#auto_destruir_solicitacao').on('ifUnchecked', function (e) {$('#validade_auto_destruir_dias').hide(300);});

        $('#is_observadores').on('ifChecked', function (e) {$('#div_observadores').show(300);});
        $('#is_observadores').on('ifUnchecked', function (e) {$('#div_observadores').hide(300);});


        $('#gerar_contrato_faturamento_venda_site').on('ifChecked', function (e) {
            $('#gerar_contrato_faturamento_venda').iCheck('check');
        });

        $('#gerar_contrato_faturamento_venda').on('ifChecked', function (e) {
            $('#div_enviar_contrato_signatarios_venda').show(300);
            $('#div_auto_assinar_customer_contrato').show(300);
        });

        $('#gerar_contrato_faturamento_venda').on('ifUnchecked', function (e) {
            $('#div_enviar_contrato_signatarios_venda').hide(300);
            $('#div_auto_assinar_customer_contrato').hide(300);

            $('#gerar_contrato_faturamento_venda_site').iCheck('uncheck');
        });

        $('#auto_assinar_customer_contrato').on('ifChecked', function (e) {
            $('#auto_assinar_biller_contrato').iCheck('check');
        });

        $('#auto_assinar_biller_contrato').on('ifUnchecked', function (e) {
            $('#auto_assinar_customer_contrato').iCheck('uncheck');
        });
    });
</script>