<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_planta');?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("products/savePlanta/".$result->id, $attrib)
                ?>

                <div class="col-md-12">
                    <div class="form-group all">
                        <div class="row">
                            <div class="col-lg-12">

                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= lang('name', 'name') ?>
                                        <?= form_input('name', $result->name, 'class="form-control tip" required="required" id="name"') ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group all">
                                        <?= lang('andares', 'andares') ?>
                                        <?= form_input('andares', $result->andares, 'class="form-control tip" id="andares"') ?>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group all">
                                        <?= lang('totalPoltrona', 'totalPoltrona') ?>
                                        <?= form_input('totalPoltrona', $result->totalPoltrona, 'class="form-control tip" id="andares"') ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group all">
                                        <?= lang('width', 'width') ?>
                                        <?= form_input('width', $result->width, 'class="form-control tip" id="width"') ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group all">
                                        <?= lang('height', 'height') ?>
                                        <?= form_input('height',  $result->height, 'class="form-control tip" id="height"') ?>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= lang("planta", "planta") ?>
                                        <input id="planta" type="file" data-browse-label="<?= lang('browse'); ?>" name="planta" data-show-upload="false"
                                               data-show-preview="false" accept="image/*" class="form-control file">
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?php echo form_submit('edit', $this->lang->line("edit"), 'class="btn btn-primary"'); ?>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>

                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#limpar').click(function (e) {
            var visal_automovel_id = $('#visal_automovel_id').val();
            var url = '<?php echo base_url(); ?>products/iframePontosPlantaVeiculo/'+visal_automovel_id;
            $('#iframeimagem').attr('src',url);

            $('#client_x').val('');
            $('#client_y').val('');
        });
    });

    function atribuirXY(x,y) {
        $('#client_x').val(x);
        $('#client_y').val(y);
    }

    function recarregar() {
        var visal_automovel_id = $('#visal_automovel_id').val();
        var url = '<?php echo base_url(); ?>products/iframePontosPlantaVeiculo/'+visal_automovel_id;
        $('#iframeimagem').attr('src',url);
    }
</script>
