<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_local_embarque'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("products/adicionarLocalEmbarque", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    '1' => lang('ativo'),
                    '0' => lang('inativo')
                );
                echo form_dropdown('active', $opts, '' , 'class="form-control" id="active" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('local_embarque_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <!--Endereco !-->
            <div id="div_endereco">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("postal_code", "postal_code"); ?>
                            <?php echo form_input('cep', '', 'class="form-control" id="cep"'); ?>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <?= lang("endereco", "endereco"); ?>
                            <?php echo form_input('endereco', '', 'class="form-control" id="endereco" '); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?= lang("numero", "numero"); ?>
                            <?php echo form_input('numero', '', 'class="form-control" id="numero"'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <?= lang("complemento", "complemento"); ?>
                            <?php echo form_input('complemento', '', 'class="form-control" id="complemento"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("bairro", "bairro"); ?>
                            <?php echo form_input('bairro', '', 'class="form-control" id="bairro"'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("country", "country"); ?>
                            <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("cidade", "cidade"); ?>
                            <?php echo form_input('cidade', '', 'class="form-control" id="cidade"'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("estado", "estado"); ?>
                            <?php echo form_input('estado', '', 'class="form-control" id="estado"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarLocalEmbarque', lang('adicionar_local_embarque'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">

    $(document).ready(function (e) {
        $( "#cep" ).blur(function() {
            getConsultaCEP();
        });
    });

    function getConsultaCEP() {

        if($.trim($("#cep").val()) === "") return false;

        var cep = $.trim($("#cep").val());
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url,
            function (data) {
                if(data !== null){
                    $("#endereco").val(data.logradouro);
                    $("#bairro").val(data.bairro);
                    $("#cidade").val(data.localidade);
                    $("#estado").val(data.uf);
                }
            });
    }
</script>
