<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
                <?php
                $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                $nomeViagem = strtoupper($product->name).' '.$data;
                ?>
            </td>
            <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
                <h4>RELATÓRIO DE PASSAGEIROS ARTESP</h4>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
                <h5>Destino: <?=$nomeViagem?> - <?php echo strtoupper($tipoTransporte->name);?></h5>
            </td>
        </tr>
        </thead>
    </table>
    <table border="0" style="width: 100%;">
        <thead>
        <tr>
            <th style="text-align: center;width: 5%;">#</th>
            <th style="width: 75%">Passageiro</th>
            <th style="width: 20%;text-align: left;">DOC</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $contador = 1;
            $isColorBackground = false;

            foreach ($itens as $row) {

                $cliente 	 = $this->site->getCompanyByID($row->customerClient);
                $localEmbarque = $this->site->getLocalEmbarqueByID($row->localEmbarque);

                $customer               = $cliente->name;
                $rg                     = $cliente->cf1;
                $orgaoEmissor           = $cliente->cf3;
                $data_aniversario       = $cliente->data_aniversario;
                $phone                  = $cliente->phone;
                $whatsApp               = $cliente->cf5;
                $telefone_emergencia    = $cliente->telefone_emergencia;
                $tipoDocumento          = $cliente->tipo_documento;
                $poltronaCliente        = $row->poltronaClient;
                if ($phone)  {
                    $telefone  = $phone;
                }

                if ($whatsApp)  {
                    $telefone  = $telefone.' '.$whatsApp;
                }

                if ($telefone_emergencia) {
                    $telefone = $telefone.'<br/>Emergência '.$telefone_emergencia;
                }

                if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

                $background = "#eee";

                if ($isColorBackground) {
                    $background = "#eee";
                    $isColorBackground = false;
                } else {
                    $background = "#ffffff";
                    $isColorBackground = true;
                }
                ?>
                <tr style="background: <?=$background;?>">
                    <?php if (!$row->descontarVaga) {?>
                        <td style="text-align: center;">(C)</td>
                        <td align="left" style="font-weight: bold;font-size: 12px;">&nbsp;<?php echo $customer;?></td>
                    <?php } else {
                        $sContador = $contador < 10 ? '0'.$contador : $contador;
                        ?>
                        <td style="text-align: center;"><?=$sContador;?></td>
                        <td align="left" style="font-weight: bold;font-size: 12px;">&nbsp;<?php echo $customer;?></td>
                        <?php $contador++?>
                    <?php } ?>
                    <td style="text-align: left;"><?=lang($tipoDocumento).' '. $rg. ' ' .$orgaoEmissor;?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>
