<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_tipo_hospedagem'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/editarTipoHospedagem/" . $tipoHospedagem->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group">
                <?= lang('tipo_hospedagem_name', 'name'); ?>
                <?= form_input('name', $tipoHospedagem->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('hotel', 'fornecedor'); ?>
                <?php
                echo form_input('fornecedor', $tipoHospedagem->fornecedor, 'data-placeholder="' . lang("select") . ' ' . lang("fornecedor") . '"id="fornecedor" class="form-control input-tip" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('ocupacao', 'ocupacao'); ?>
                <?= form_input('ocupacao', $tipoHospedagem->ocupacao, 'class="form-control" id="ocupacao" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', $tipoHospedagem->note, 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
            <?php echo form_hidden('id', $tipoHospedagem->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarTipoHospedagem', lang('editar_tipo_hospedagem'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        <?php if ($tipoHospedagem->fornecedor){?>
            $('#fornecedor').val($('#fornecedor').val()).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } else { ?>
            $('#fornecedor').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } ?>
    });
</script>
