<?php
function row_status($x)
{
    if ($x == null) {
        return '';
    } elseif ($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">' . lang($x) . '</span></div>';
    } elseif ($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">' . lang($x) . '</span></div>';
    } elseif ($x == 'partial' || $x == 'transferring') {
        return '<div class="text-center"><span class="label label-info">' . lang($x) . '</span></div>';
    } elseif ($x == 'due') {
        return '<div class="text-center"><span class="label label-danger">' . lang($x) . '</span></div>';
    } else {
        return '<div class="text-center"><span class="label label-default">' . lang($x) . '</span></div>';
    }
}

?>

<ul id="myTab" class="nav nav-tabs">
    <li class=""><a href="#fluxoCaixa" class="tab-grey"><?= lang('fluxo_caixa') ?></a></li>
</ul>
<div class="tab-content">
    <div id="fluxoCaixa" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('fluxo_de_caixa'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">

                            <div class="box-header">
                                <h2 class="blue"><i
                                        class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas_adicionais_sinal'); ?>
                                </h2>
                            </div>

                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr class="active">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference"); ?></th>
                                    <th class="col-xs-2"><?= lang("category"); ?></th>
                                    <th class="col-xs-1"><?= lang("amount"); ?></th>
                                    <th class="col-xs-3"><?= lang("note"); ?></th>
                                    <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($outrasDespesasSinal)) {
                                    foreach ($outrasDespesasSinal as $order) {
                                        echo '
                                            <tr>
                                                <td>' . $order->date . '</td>
                                                <td>' . $order->reference . '</td>
                                                <td>' . $order->category . '</td>
                                                <td class="text-right">' . $this->sma->formatMoney($order->amount) . '</td>
                                                <td>' . $order->note . '</td>
                                                <td>' . $order->user . '</td>
                                            </tr>';
                                    }
                                } ?>
                                </tbody>

                            </table>
                            <?= lang("despesas_com_sinal_alert"); ?>
                            <br/><br/>
                            <div class="box-header">
                                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas'); ?></h2>
                            </div>
                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr>
                                    <th><?= lang("id"); ?></th>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("fornecedor_cliente"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("saldo"); ?></th>
                                    <th><?= lang("payment_status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_purchases = 0;
                                $total_pago_purchases = 0;
                                $total_saldo_purchases = 0;

                                if (!empty($purchases)) {
                                    $r = 1;
                                    foreach ($purchases as $purchase) {
                                        $payment_status = '';
                                        $total_purchases = $total_purchases + $purchase->grand_total;
                                        $total_pago_purchases = $total_pago_purchases + $purchase->paid;
                                        $total_saldo_purchases = $total_saldo_purchases + $purchase->balance;

                                        if ($purchase->paid >= $purchase->grand_total) {
                                            $payment_status = 'paid';
                                        } else {
                                            if ($purchase->paid > 0) {
                                                $payment_status = 'partial';
                                            } else {
                                                $payment_status = 'due';
                                            }
                                        }

                                        echo '
															<tr id="' . $purchase->id . '" class="purchase_link"><td>' . $r . '</td>
			                                                    <td>' . $this->sma->hrld($purchase->date) . '</td>
			                                                    <td>' . $purchase->reference_no . '</td>
			                                                    <td>' . $purchase->supplier . '</td>
			                                                    <td class="text-right">' . $this->sma->formatMoney($purchase->grand_total) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($purchase->paid) . '</td>
																<td class="text-right">' . $this->sma->formatMoney($purchase->balance) . '</td>
																<td>' . row_status($payment_status) . '</td>
			                                                </tr>';
                                        $r++;
                                    }
                                } else { ?>
                                    <tr>
                                        <td colspan="6"
                                            class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_purchases) ?></th>
                                    <th><?= $this->sma->formatMoney($total_pago_purchases) ?></th>
                                    <th><?= $this->sma->formatMoney($total_saldo_purchases) ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>

                            <br/>

                            <div class="box-header">
                                <h2 class="blue"><i
                                        class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('despesas_adicionais'); ?>
                                </h2>
                            </div>

                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr class="active">
                                    <th class="col-xs-2"><?= lang("date"); ?></th>
                                    <th class="col-xs-2"><?= lang("reference"); ?></th>
                                    <th class="col-xs-2"><?= lang("category"); ?></th>
                                    <th class="col-xs-1"><?= lang("amount"); ?></th>
                                    <th class="col-xs-3"><?= lang("note"); ?></th>
                                    <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_outras_despesas = 0;
                                if (!empty($outrasDespesas)) {
                                    foreach ($outrasDespesas as $order) {
                                        $total_outras_despesas = $total_outras_despesas + $order->amount;
                                        echo '
																<tr>
																	<td>' . $order->date . '</td>
		                                                            <td>' . $order->reference . '</td>
		                                                            <td>' . $order->category . '</td>
																	<td class="text-right">' . $this->sma->formatMoney($order->amount) . '</td>
		                                                            <td>' . $order->note . '</td>
  		                                                            <td>' . $order->user . '</td>
		                                                        </tr>';
                                    }
                                } ?>
                                </tbody>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_outras_despesas) ?></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th colspan="6" class="text-right">Total das Depesas + Outras
                                        Despesas <?= $this->sma->formatMoney($total_outras_despesas + $total_purchases) ?></th>
                                </tr>
                                </thead>
                            </table>

                            <br/>
                            <div class="box-header">
                                <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o nb"></i><?= lang('receitas'); ?></h2>
                            </div>
                            <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-hover table-striped"
                                   style="margin-bottom: 0;">
                                <thead>
                                <tr>
                                    <th><?= lang("id"); ?></th>
                                    <th><?= lang("date"); ?></th>
                                    <th><?= lang("reference_no"); ?></th>
                                    <th><?= lang("fornecedor_cliente"); ?></th>
                                    <th><?= lang("grand_total"); ?></th>
                                    <th><?= lang("paid"); ?></th>
                                    <th><?= lang("saldo"); ?></th>
                                    <th><?= lang("payment_status"); ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $total_sales = 0;
                                $total_pago_sales = 0;
                                $total_saldo_sales = 0;
                                if (!empty($sales)) {
                                    $r = 1;
                                    foreach ($sales as $order) {
                                        $total_sales = $total_sales + $order->grand_total;
                                        $total_pago_sales = $total_pago_sales + $order->paid;
                                        $total_saldo_sales = $total_saldo_sales + $order->balance;
                                        echo ' <tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                    <td>' . $this->sma->hrld($order->date) . '</td>
                                                    <td class="text-center">' . $order->reference_no . '</td>
                                                    <td>' . $order->customer . '</td>
                                                    <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                    <td class="text-right">' . $this->sma->formatMoney($order->paid) . '</td>
                                                    <td class="text-right">' . $this->sma->formatMoney($order->balance) . '</td>
                                                    <td>' . row_status($order->payment_status) . '</td>
                                                </tr>';
                                        $r++;
                                    }
                                } ?>
                                </tbody>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?= $this->sma->formatMoney($total_sales) ?></th>
                                    <th><?= $this->sma->formatMoney($total_pago_sales) ?></th>
                                    <th><?= $this->sma->formatMoney($total_saldo_sales) ?></th>
                                    <th></th>
                                </tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <?php if (($total_purchases + $total_outras_despesas) - $total_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-success">
                                                    <?= $this->sma->formatMoney((($total_purchases + $total_outras_despesas) - $total_sales) * -1) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-danger">
                                                    <?= $this->sma->formatMoney(($total_purchases + $total_outras_despesas) - $total_sales) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } ?>

                                    <?php if (($total_pago_purchases + $total_outras_despesas) - $total_pago_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-success">
                                                    <?= $this->sma->formatMoney((($total_pago_purchases + $total_outras_despesas) - $total_pago_sales) * -1) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-danger">
                                                    <?= $this->sma->formatMoney(($total_pago_purchases + $total_outras_despesas) - $total_pago_sales) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } ?>

                                    <?php if (($total_saldo_purchases + $total_outras_despesas) - $total_saldo_sales < 0) { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-success">
                                                    <?= $this->sma->formatMoney((($total_saldo_purchases + $total_outras_despesas) - $total_saldo_sales) * -1) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } else { ?>
                                        <th>
                                            <div class="text-center">
                                                <span class="label label-danger">
                                                    <?= $this->sma->formatMoney(($total_saldo_purchases + $total_outras_despesas) - $total_saldo_sales) ?>
                                                </span>
                                            </div>
                                        </th>
                                    <?php } ?>
                                    <th></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="PI" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-file-text-o nb"></i><?= $product->name . ' ' . lang('pi'); ?>
                </h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <p class="introtext"><?php echo lang('list_results'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>


