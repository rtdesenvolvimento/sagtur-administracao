
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-bus"></i><?= lang('plantas'); ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= site_url('products/addPlanta') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_planta') ?>
                            </a>
                        </li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">

            <div class="col-lg-12">

                <div class="table-responsive">
                    <table id="PRData" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="text-align: center"><?= lang('img') ?></th>
                            <th style="text-align: left;width: 70%"><?= lang('name') ?></th>
                            <th style="text-align: center"><?= lang('andares') ?></th>
                            <th style="text-align: center"><?= lang('totalPoltrona') ?></th>
                            <th style="text-align: right"><?= lang('width') ?></th>
                            <th style="text-align: right"><?= lang('height') ?></th>
                            <th style="text-align: center;"><?= lang('actions') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($plantas as $planta) {?>
                            <tr>
                                <td style="text-align: center">
                                    <div class="text-center">
                                        <a href="http://localhost/rt-php-turismo-1.0/plantas/<?php echo $planta->planta;?>" data-toggle="lightbox">
                                            <img src="http://localhost/rt-php-turismo-1.0/plantas/thumbs/<?php echo $planta->planta;?>" alt="" style="width:30px; height:30px;">
                                        </a>
                                    </div>
                                </td>
                                <td style="text-align: left"><?php echo $planta->name;?></td>
                                <td style="text-align: center"><?php echo $planta->andares;?></td>
                                <td style="text-align: center"><?php echo $planta->totalPoltrona;?></td>
                                <td style="text-align: right"><?php echo $planta->width;?>px</td>
                                <td style="text-align: right"><?php echo $planta->height;?>px</td>
                                <td>
                                    <div class="text-center"><div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo base_url();?>/products/editarPlanta/<?php echo $planta->id;?>"><i class="fa fa-edit"></i> Editar Planta</a></li>
                                                <li><a href="<?php echo base_url();?>/products/configurar_planta/<?php echo $planta->id;?>"><i class="fa fa-plus"></i> Configurar Planta</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo base_url();?>/products/excluirPlanta/<?php echo $planta->id;?>"><i class="fa fa-trash-o"></i> Excluir</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>

                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
