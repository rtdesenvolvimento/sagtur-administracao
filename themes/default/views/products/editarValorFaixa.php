<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_valor_faixa'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/editarValorFaixa/" . $valorFaixa->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group" id="div_status">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    '1' => lang('ativo'),
                    '0' => lang('inativo')
                );
                echo form_dropdown('active', $opts, $valorFaixa->active, 'class="form-control" id="active" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $valorFaixa->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('descontarVaga', '1', $valorFaixa->descontarVaga, ''); ?>
                <?= lang('descontarVaga', 'descontarVaga'); ?>
            </div>
            <h3><?= lang('captar_dados_link'); ?></h3>
            <div class="form-group">
                <?php echo form_checkbox('captarNomeSocial', '1', $valorFaixa->captarNomeSocial, ''); ?>
                <?= lang('captarNomeSocial', 'captarNomeSocial'); ?>
            </div>
            <div class="form-group" id="div_status">
                <?= lang("formato_captacao_cpf_cnpj", "formato_cpf") ?>
                <?php
                $opts = array(
                    '1' => lang('formato_cpf'),
                    '0' => lang('formato_cnpj'),
                );
                echo form_dropdown('formato_cpf', $opts, $valorFaixa->formato_cpf, 'class="form-control" id="formato_cpf" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarCPF', '1', $valorFaixa->captarCPF, ''); ?>
                <?= lang('captarCPF', 'captarCPF'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('obrigaCPF', '1', $valorFaixa->obrigaCPF, ''); ?>
                <?= lang('obrigaCPF', 'obrigaCPF'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarDataNascimento', '1', $valorFaixa->captarDataNascimento, ''); ?>
                <?= lang('captarDataNascimento', 'captarDataNascimento'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarDocumento', '1', $valorFaixa->captarDocumento, ''); ?>
                <?= lang('captarDocumento', 'captarDocumento'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarOrgaoEmissor', '1', $valorFaixa->captarOrgaoEmissor, ''); ?>
                <?= lang('captarOrgaoEmissor', 'captarOrgaoEmissor'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('obrigaOrgaoEmissor', '1', $valorFaixa->obrigaOrgaoEmissor, ''); ?>
                <?= lang('obrigaOrgaoEmissor', 'obrigaOrgaoEmissor'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarEmail', '1',  $valorFaixa->captarEmail, ''); ?>
                <?= lang('captarEmail', 'captarEmail'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarProfissao', '1', $valorFaixa->captarProfissao, ''); ?>
                <?= lang('captarProfissao', 'captarProfissao'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captarInformacoesMedicas', '1', $valorFaixa->captarInformacoesMedicas, ''); ?>
                <?= lang('captarInformacoesMedicas', 'captarInformacoesMedicas'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('faixaDependente', '1', $valorFaixa->faixaDependente, ''); ?>
                <?= lang('faixaDependente', 'faixaDependente'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('captar_observacao', '1', $valorFaixa->captar_observacao, ''); ?>
                <?= lang('captar_observacao', 'captar_observacao'); ?>
            </div>
            <div class="form-group">
                <?= lang('placeholder_observacao', 'placeholder_observacao'); ?>
                <?= form_input('placeholder_observacao', $valorFaixa->placeholder_observacao, 'class="form-control" placeholder="Informações importantes! Exemplo: Com Quem deseja compartilhar o assento ou Hospedagem." id="placeholder_observacao"'); ?>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('staff', '1', $valorFaixa->staff, ''); ?>
                <?= lang('staff', 'staff'); ?>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $valorFaixa->note), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>

            <?php echo form_hidden('id', $valorFaixa->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarValorFaixa', lang('editar_valor_faixa'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

