<?php
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                data: scdata
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-edit"></i><?= lang('edit_product'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('update_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("products/edit/" . $product->id, $attrib)
                ?>
                <div class="col-md-5">
                    <div class="form-group" style="display: none;">
                        <?= lang("product_type", "type") ?>
                        <?php
                        $opts = array( 'combo' => lang('combo')  );
                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required"');
                        ?>
                    </div>
                    
					<div class="form-group" id="div_status">
                        <?= lang("status", "status") ?>
                        <?php
                        $opts = array('Aguardando' => lang('Aguardando'), 'Confirmado' => lang('confirmado'), 'Em Viagem' => lang('em_viagem') , 'Executado' => lang('executado') , 'Cancelado' => lang('cancelado'));
                        echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                        ?>
                    </div>
					
					<div class="form-group all">
                        <?= lang("product_name", "name") ?>
                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required"'); ?>
                    </div>


                    <div class="form-group all" style="display: none;">
                        <?= lang("product_code", "code") ?>
                        <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required"') ?>
                        <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                    </div>

					<div class="form-group all">
                        <?= lang("category", "category") ?>
                        <?php
                        $cat[''] = "";
                        foreach ($categories as $category) {
                            $cat[$category->id] = $category->name;
                        }
                        echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ($product ? $product->category_id : '')), 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                        ?>
                    </div>

                    <div class="form-group" style="display: none;">
                        <?= lang("simbolo_moeda", "simbolo_moeda") ?>
                        <?php
                        $opts = array('R$' => lang('R$') ,'USD' => lang('USD') , '€' => lang('Euro'),'£' => lang('Libra') );
                        echo form_dropdown('simbolo_moeda', $opts, (isset($_POST['simbolo_moeda']) ? $_POST['simbolo_moeda'] : ($product ? $product->simbolo_moeda : '')), 'class="form-control" id="simbolo_moeda" ');
                        ?>
                    </div>
                    
					<!--###########################-->
					<!--## DATA INICIO / TERMINO ##
					<!--###########################-->
					<div id="datas_viagem">
                        <div class="well well-sm">

                            <div class="form-group">
                                <?= lang('informacoes_viagem'); ?>
                            </div>

                            <div class="form-group all">
                                <?= lang('data_saida', 'data_saida'); ?>
                                <input type="date" name="data_saida" value="<?php echo $product->data_saida;?>" required="required" class="form-control tip" id="data_saida" data-original-title="" title="">
                            </div>

                            <div class="form-group all">
                                <?= lang('hora_saida', 'hora_saida'); ?>
                                <input type="time" name="hora_saida" value="<?php echo $product->hora_saida;?>" class="form-control tip" id="hora_saida" data-original-title="" title="">
                            </div>

							<div class="form-group all">
								<?= lang("origem", "origem") ?>
								<?= form_textarea('origem', (isset($_POST['origem']) ? $_POST['origem'] : ($product ? $product->origem : '')), 'class="form-control" required="required" id="origem"'); ?>
							</div>
 
							<div class="form-group all">
								<?= lang("destino", "destino") ?>
								<?= form_textarea('destino', (isset($_POST['destino']) ? $_POST['destino'] : ($product ? $product->destino : '')), 'class="form-control" id="destino" '); ?>
							</div>

                            <div class="form-group all">
                                <?= lang("local_retorno", "local_retorno") ?>
                                <?= form_input('local_retorno', (isset($_POST['local_retorno']) ? $_POST['local_retorno'] : ($product ? $product->local_retorno : '')), 'class="form-control" id="local_retorno" '); ?>
                            </div>

                            <div class="form-group" id="div_status" style="display: none;">
                                <?= lang("enviar_site", "enviar_site") ?>
                                <?php
                                $opts = array('sim' => lang('sim') , 'nao' => lang('nao') );
                                echo form_dropdown('enviar_site', $opts, (isset($_POST['enviar_site']) ? $_POST['enviar_site'] : ($product ? $product->enviar_site : '')), 'class="form-control" id="enviar_site"');
                                ?>
                            </div>

                            <div class="form-group" id="div_status" style="display: none;">
                                <?= lang("apenas_cotacao", "apenas_cotacao") ?>
                                <?php
                                $opts = array( 'nao' => lang('nao') , 'sim' => lang('sim') );
                                echo form_dropdown('apenas_cotacao', $opts, (isset($_POST['apenas_cotacao']) ? $_POST['apenas_cotacao'] : ($product ? $product->apenas_cotacao : '')), 'class="form-control" id="apenas_cotacao"');
                                ?>
                            </div>

                            <div class="form-group all" style="display: none;">
                                <?= lang("valor_pacote", "valor_pacote") ?>
                                <?= form_textarea('valor_pacote', (isset($_POST['valor_pacote']) ? $_POST['valor_pacote'] : ($product ? $product->valor_pacote : '')), 'class="form-control" id="valor_pacote"') ?>
                                <small>Esta informação só será exibida para pacotes com cotação de passagem. Deixe em branco para que o sistema use o preço do pacote </small>
                            </div>

                            <div class="form-group" id="div_status">
                                <?= lang("tipo_transporte", "tipo_transporte") ?>
                                <?php
                                $opts = array('sem_aereo' => lang('sem_aereo') , 'com_aereo' => lang('com_aereo'), 'rodoviario' => lang('rodoviario') , 'nao_exibir' => lang('nao_exibir') );
                                echo form_dropdown('tipo_transporte', $opts, (isset($_POST['tipo_transporte']) ? $_POST['tipo_transporte'] : ($product ? $product->tipo_transporte : '')), 'class="form-control" id="tipo_transporte"');
                                ?>
                            </div>

                            <!--
                            <div class="form-group" id="div_status">
                                <?= lang("categorias", "categorias") ?>
                                <?php
                            $opts = array('bate_volta' => lang('bate_volta') , 'feriados' => lang('feriados'), 'ferias' => lang('ferias') , 'festivais' => lang('festivais') , 'finais_de_semana' => lang('finais_de_semana') , 'viagens_exclusivas' => lang('viagens_exclusivas'));
                            echo form_dropdown('categorias', $opts, (isset($_POST['categorias']) ? $_POST['categorias'] : ($product ? $product->categorias : '')), 'class="form-control" id="categorias"');
                            ?>
                            </div>
                            !-->
                            <div class="form-group" id="div_status">
                                <?= lang("categorias", "categorias") ?>
                                <?php
                                $opts = array('bate_volta' => lang('bate_volta') , 'hospedagem' => lang('hospedagem'), 'aereo' => lang('aereo') );
                                echo form_dropdown('categorias', $opts, (isset($_POST['categorias']) ? $_POST['categorias'] : ($product ? $product->categorias : '')), 'class="form-control" id="categorias"');
                                ?>
                            </div>
                            <div class="form-group all" style="display: none;">
                                <?= lang('duracao_pacote', 'duracao_pacote'); ?>
                                <input type="number" name="duracao_pacote" value="<?php echo $product->duracao_pacote ;?>" class="form-control tip" id="duracao_pacote">
                            </div>
                            <div class="form-group all" style="display: none;">
                                <?= lang('numero_pessoas', 'numero_pessoas'); ?>
                                <input type="number" name="numero_pessoas" value="<?php echo $product->numero_pessoas;?>" class="form-control tip" id="numero_pessoas">
                            </div>

                            <div class="form-group all">
                                <?= lang('data_chegada', 'data_chegada'); ?>
                                <input type="date" name="data_chegada" value="<?php echo $product->data_chegada;?>" class="form-control tip" id="data_chegada" data-original-title="" title="">
                            </div>

                            <div class="form-group all">
                                <?= lang('hora_chegada', 'hora_chegada'); ?>
                                <input type="time" name="hora_chegada" value="<?php echo $product->hora_chegada;?>" class="form-control tip" id="hora_chegada" data-original-title="" title="">
                            </div>

                            <div class="form-group all">
                                <?= lang('data_retorno', 'data_retorno'); ?>
                                <input type="date" name="data_retorno" value="<?php echo $product->data_retorno;?>" class="form-control tip" id="data_retorno" data-original-title="" title="">
                            </div>

                            <div class="form-group all">
                                <?= lang('hora_retorno', 'hora_retorno'); ?>
                                <input type="time" name="hora_retorno" value="<?php echo $product->hora_retorno;?>" class="form-control tip" id="hora_retorno" data-original-title="" title="">
                            </div>

                            <div class="form-group all">
                                <?= lang('tempo_viagem', 'tempo_viagem'); ?>
                                <input type="number" name="tempo_viagem" value="<?php echo $product->tempo_viagem;?>" class="form-control tip" id="tempo_viagem" data-original-title="" title="">
                            </div>
                        </div>
                    </div>
					<!--FIM DAS DASTAS DA VIAGEM -->
					
                    <div class="form-group all" style="display:none;">
                        <?= lang("barcode_symbology", "barcode_symbology") ?>
                        <?php
                        $bs = array('code25' => 'Code25', 'code39' => 'Code39', 'code128' => 'Code128', 'ean8' => 'EAN8', 'ean13' => 'EAN13', 'upca' => 'UPC-A', 'upce' => 'UPC-E');
                        echo form_dropdown('barcode_symbology', $bs, (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control select" id="barcode_symbology" required="required" style="width:100%;"');
                        ?>

                    </div>
                    
                    <div class="form-group all" style="display:none;">
                        <?= lang("subcategory", "subcategory") ?>
                        <div class="controls" id="subcat_data"> <?php
                            echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . lang("select_category_to_load") . '"');
                            ?>
                        </div>
                    </div>
                	
					<!--Margem-->
					<div class="form-group all" style="display:none;">
                        <?= lang("product_margem", "margem") ?>
                        <?= form_input('margem', (isset($_POST['margem']) ? $_POST['margem'] : ($product ? $this->sma->formatDecimal($product->margem) : '')), 'class="form-control tip" id="margem" ') ?>
                    </div>

                    <div class="form-group" style="display:none;">
                        <input type="checkbox" class="checkbox" value="1" name="promotion" id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?>>
                        <label for="promotion" class="padding05">
                            <?= lang('promotion'); ?>
                        </label>
                    </div>

                    <div id="promo" style="display: none;">
                        <div class="well well-sm">
                            <div class="form-group">
                                <?= lang('promo_price', 'promo_price'); ?>
                                <?= form_input('promo_price', set_value('promo_price', $product->promo_price ? $product->promo_price : ''), 'class="form-control tip" id="promo_price"'); ?>
                            </div>
                            <div class="form-group" style="display: none;">
                                <?= lang('start_date', 'start_date'); ?>
                                <?= form_input('start_date', set_value('start_date', $product->start_date ? $this->sma->hrsd($product->start_date) : ''), 'class="form-control tip date" id="start_date"'); ?>
                            </div>
                            <div class="form-group" style="display: none;">
                                <?= lang('end_date', 'end_date'); ?>
                                <?= form_input('end_date', set_value('end_date', $product->end_date ? $this->sma->hrsd($product->end_date) : ''), 'class="form-control tip date" id="end_date"'); ?>
                            </div>
                        </div>
                    </div>

                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group all">
                            <?= lang("product_tax", "tax_rate") ?>
                            <?php
                            $tr[""] = "";
                            foreach ($tax_rates as $tax) {
                                $tr[$tax->id] = $tax->name;
                            }
                            echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("tax_method", "tax_method") ?>
                            <?php
                            $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                            echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
                            ?>
                        </div>
                    <?php } ?>
                    
                    <div class="form-group all">
                        <?= lang("product_image", "product_image") ?>
                        <input id="product_image" type="file" data-browse-label="<?= lang('browse'); ?>" name="product_image" data-show-upload="false"
                               data-show-preview="false" accept="image/*" class="form-control file">
                    </div>

                    <div class="form-group all">
                        <?= lang("product_gallery_images", "images") ?>
                        <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                               data-show-preview="false" class="form-control file" accept="image/*">
                    </div>
                    <div id="img-details"></div>
                </div>
                
		                
                <div class="col-md-6 col-md-offset-1">

                
                    <div class="standard">
                         
                        <div class="clearfix"></div>

                        <div id="attrs"></div>
                        <div class="well well-sm">
                   
                            <div class="form-group">
                                 <?= lang('eg_sizes_colors'); ?>
                            </div>

                            <div id="attr-con">
                                <div class="form-group" id="ui" style="margin-bottom: 0;">
                                    <div class="input-group">
                                        <?php
                                        echo form_input('attributesInput', '', 'class="form-control select-tags" id="attributesInput" placeholder="' . $this->lang->line("enter_attributes") . '"'); ?>
                                        <div class="input-group-addon" style="padding: 2px 5px;">
                                            <a href="#" id="addAttributes">
                                                <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div style="clear:both;"></div>
                                </div>
                                <div class="table-responsive">
                                    <table id="attrTable" class="table table-bordered table-condensed table-striped"
	                                       style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:none;'; ?>margin-bottom: 0; margin-top: 10px;">
	                                    <thead>
		                                    <tr class="active">
		                                        <th style="text-align: left;"><?= lang('name') ?></th>
		                                        <th style="text-align: left;"><?= lang('fornecedor') ?></th>
		                                        <th style="display: none;"><?= lang('totalPoltrona') ?></th>
		                                        <th style="display: none;"><?= lang('totalPessoasConsiderar') ?></th>
		                                        <th style="display: none;"><?= lang('localizacao') ?></th>
		                                        <th style="display: none;"><?= lang('margem') ?></th>
		                                        <th><?= lang('price') ?></th>
		                                        <th><?= lang('cost') ?></th>
		                                        <th></th>
		                                    </tr>
	                                    </thead>
	                                    <tbody>
	                                    <?php 
		                                    if ($this->input->post('attributes')) {
		                                        $a = sizeof($_POST['attr_name']);
		                                        for ($r = 0; $r <= $a; $r++) {
		                                            if (isset($_POST['attr_name'][$r]) && (isset($_POST['attr_warehouse'][$r]) || isset($_POST['attr_quantity'][$r]))) {
		                                                echo '<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' . $_POST['attr_name'][$r] . '"><span>' . $_POST['attr_name'][$r] . '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value="' . $_POST['attr_warehouse'][$r] . '"><input type="hidden" name="attr_wh_name[]" value="' . $_POST['attr_wh_name'][$r] . '"><span>' . $_POST['attr_wh_name'][$r] . '</span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="' . $_POST['attr_quantity'][$r] . '"><span>' . $_POST['attr_quantity'][$r] . '</span></td><td class="cost text-right"><input type="hidden" name="attr_cost[]" value="' . $_POST['attr_cost'][$r] . '"><span>' . $_POST['attr_cost'][$r] . '</span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="' . $_POST['attr_price'][$r] . '"><span>' . $_POST['attr_price'][$r] . '</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';
		                                            }
		                                        }
		                                    } elseif ($product_options) {
		                                        foreach ($product_options as $option) {
		                                        	 
	                                        		$supplier= '-';
	                                        		$calculoType = '';

	                                        		if($option->fornecedor) {
	                                        			$supplier_details = $this->site->getCompanyByID ( $option->fornecedor );
	                                        			$supplier = trim ($supplier_details->company.' ('.$supplier_details->name.')');
	                                        		}


                                                    foreach ($variants as $variacao) {
	                                        		    if ($option->name == $variacao->name) {
                                                            $calculoType = $variacao->calculo_type;
                                                        }
                                                    }

	                                        		if ($option->name == 'Passagem') {
	                                        			echo '<tr class="attr" style="display:none"> ';
	                                        		} else {
	                                        			echo '<tr class="attr" >';
	                                        		}
	                                        		
		                                            echo '
															<td>
																<input type="hidden" name="deletar[]"   value="false">
																<input type="hidden" name="attr_id[]"   value="' . $option->id . '">
																<input type="hidden" name="attr_name[]" value="' . $option->name . '"><span>' . $option->name . '</span>
															</td>
															<td class="fornecedor text-left"><input type="hidden" name="attr_fornecedor[]" value="'.$option->fornecedor.'"><span>'.$supplier.'</span></td>
															<td class="totalPoltrona text-center" style="display: none;"><input type="hidden" name="att_totalPoltrona[]" value="'.$option->totalPoltrona.'"><span>'.$option->totalPoltrona.'</span></td>
															<td class="totalPessoasConsiderar text-center" style="display: none;"><input type="hidden" name="att_totalPessoasConsiderar[]" value="'.$option->totalPessoasConsiderar.'"><span>'.$option->totalPessoasConsiderar.'</span></td>
															<td class="localizacao text-right" style="display:none;"><input type="hidden" name="attr_localizacao[]" value="'.$option->localizacao.'"><span>'.$option->localizacao.'</span></td>
															<td class="cost text-right" style="display: none;"><input type="hidden" name="attr_cost[]" value="' . $option->cost. '"><span>' . $option->cost. '</span></td>
															<td class="price text-right" ><input type="hidden" name="attr_price[]" value="' . $option->price . '"><span>' . $option->price. '</span></td>
															<td class="totalPreco text-right"><input type="hidden" typeCalculo="'.$calculoType.'" name="attr_totalPreco[]" value="'.$option->totalPreco.'"><span>'.$option->totalPreco.'</span></td>
 															<td class="text-center"><i class="fa fa-times delAttr"></i></td>
														 </tr>';
		                                        }
		                                    } 
	                                    ?>
	                                    </tbody>
	                                </table>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>

                    <div class="well well-sm" >

                        <!-- #################### -->
                        <!-- ### MARGEM PADRAO ## -->
                        <!-- #################### -->
                        <div class="form-group all">
                            <?= lang("margem_padrao", "margem_padrao") ?>
                            <?= form_input('margem_padrao', (isset($_POST['margem_padrao']) ? $_POST['margem_padrao'] : ($product ? $this->sma->formatDecimal($product->margem_padrao) : '')), 'class="form-control mask_money tip" id="margem_padrao" onblur="calcularPrecoSugeridoPacote();" ') ?>
                        </div>

                        <!-- ########################################## -->
                        <!-- ### QUANTIDADE DE PESSOAS PADRAO PADRAO ## -->
                        <!-- ########################################## -->
                        <div class="form-group all">
                            <?= lang("quantidadadePessoaConsiderarPadrao", "quantidadadePessoaConsiderarPadrao") ?>
                            <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatDecimal($product->alert_quantity) : '')), 'class="form-control mask_money tip" id="alert_quantity" onblur="calcularPrecoSugeridoPacote();"') ?>
                        </div>

                        <div class="form-group standard">
                            <?= lang("preco_sugerido", "preco_sugerido") ?>
                            <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" readonly required="required"') ?>
                        </div>

                        <div class="form-group all">
                            <?= lang("price_pacote", "price_pacote") ?>
                            <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control mask_money tip" id="price" required="required"') ?>
                        </div>

                        <!-- ########################## -->
                        <!-- ### 0 - 5 anos de idade ## -->
                        <!-- ########################## -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_zero_cinco", "price_pacote_zero_cinco") ?>
                            <?= form_input('price_pacote_zero_cinco', (isset($_POST['price_pacote_zero_cinco']) ? $_POST['price_pacote_zero_cinco'] : ($product ? $this->sma->formatDecimal($product->price_pacote_zero_cinco) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_zero_cinco" ') ?>
                        </div>

                        <!-- ########################### -->
                        <!-- ### 6 - 11 anos de idade ## -->
                        <!-- ########################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_seis_onze", "price_pacote_seis_onze") ?>
                            <?= form_input('price_pacote_seis_onze', (isset($_POST['price_pacote_seis_onze']) ? $_POST['price_pacote_seis_onze'] : ($product ? $this->sma->formatDecimal($product->price_pacote_seis_onze) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_seis_onze" ') ?>
                        </div>

                        <!-- ################################ -->
                        <!-- ### acima de 60 anos de idade ## -->
                        <!-- ############################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_acima_sessenta", "price_pacote_acima_sessenta") ?>
                            <?= form_input('price_pacote_acima_sessenta', (isset($_POST['price_pacote_acima_sessenta']) ? $_POST['price_pacote_acima_sessenta'] : ($product ? $this->sma->formatDecimal($product->price_pacote_acima_sessenta) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_acima_sessenta" ') ?>
                        </div>

                        <!-- ####################### -->
                        <!-- ### desconto a vista ## -->
                        <!-- ####################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("percentual_desconto_a_vista", "percentual_desconto_a_vista") ?>
                            <?= form_input('percentual_desconto_a_vista', (isset($_POST['percentual_desconto_a_vista']) ? $_POST['percentual_desconto_a_vista'] : ($product ? $this->sma->formatDecimal($product->percentual_desconto_a_vista) : '0')), 'class="form-control tip mask_money" required="required" id="percentual_desconto_a_vista" ') ?>
                        </div>

                        <!-- ##################################################### -->
                        <!-- ### QUANTIDADE DE PESSOAS PARA CONFIRMAR A VIAGEM, ## -->
                        <!-- ##################################################### -->

                        <div class="form-group all">
                            <?= lang("quantidadePessoasViagem", "quantidadePessoasViagem") ?>
                            <?= form_input('quantidadePessoasViagem', (isset($_POST['quantidadePessoasViagem']) ? $_POST['quantidadePessoasViagem'] : ($product ? $product->quantidadePessoasViagem : '')), 'class="form-control tip mask_integer" id="quantidadePessoasViagem"') ?>
                        </div>

                        <div class="form-group all" style="display: none;">
                            <?= lang("alertar_polcas_vagas", "alertar_polcas_vagas") ?>
                            <?= form_input('alertar_polcas_vagas', (isset($_POST['alertar_polcas_vagas']) ? $_POST['alertar_polcas_vagas'] : ($product ? $product->alertar_polcas_vagas : '')), 'class="form-control tip mask_integer" id="alertar_polcas_vagas"') ?>
                        </div>

                        <div class="form-group all" style="display: none;">
                            <?= lang("alertar_ultimas_vagas", "alertar_ultimas_vagas") ?>
                            <?= form_input('alertar_ultimas_vagas', (isset($_POST['alertar_ultimas_vagas']) ? $_POST['alertar_ultimas_vagas'] : ($product ? $product->alertar_ultimas_vagas : '')), 'class="form-control tip mask_integer" id="alertar_ultimas_vagas"') ?>
                        </div>

                    </div>

                    <div class="combo" style="display:none;">

                        <div class="form-group">
                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                        </div>
                        
                        <div class="control-group table-group">
                            <label class="table-label" for="combo"><?= lang("combo_products"); ?></label>
                            <!--<div class="row"><div class="ccol-md-10 col-sm-10 col-xs-10"><label class="table-label" for="combo"><?= lang("combo_products"); ?></label></div>
                            <div class="ccol-md-2 col-sm-2 col-xs-2"><div class="form-group no-help-block" style="margin-bottom: 0;"><input type="text" name="combo" id="combo" value="" data-bv-notEmpty-message="" class="form-control" /></div></div></div>-->
                            <div class="controls table-controls">
                                <table id="prTable"
                                       class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th class="col-md-5 col-sm-5 col-xs-5"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                        <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                        <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                        <th class="col-md-1 col-sm-1 col-xs-1 text-center">
                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="digital" style="display:none;">
							<div class="form-group digital">
								<?= lang("digital_file", "digital_file") ?>
								<input id="digital_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="digital_file" data-show-upload="false"
									   data-show-preview="false" class="form-control file">
							</div>
						</div>
						<div style="display:none">
						<div class="form-group standard"  style="display:none">
							<div class="form-group">
								<?= lang("supplier", "supplier") ?>
								<button type="button" class="btn btn-primary btn-xs" id="addSupplier"><i class="fa fa-plus"></i>
								</button>
							</div>
							<div class="row" id="supplier-con">
								<div class="col-xs-12">
									<div class="form-group">
										<?php
										echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control ' . ($product ? '' : 'suppliers') . '" id="' . ($product && ! empty($product->supplier1) ? 'supplier1' : 'supplier') . '" placeholder="' . lang("select") . ' ' . lang("supplier") . '" style="width:100%;"');
										?>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<?= form_input('supplier_part_no', (isset($_POST['supplier_part_no']) ? $_POST['supplier_part_no'] : ""), 'class="form-control tip" id="supplier_part_no" placeholder="' . lang('supplier_part_no') . '"'); ?>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<?= form_input('supplier_price', (isset($_POST['supplier_price']) ? $_POST['supplier_price'] : ""), 'class="form-control tip" id="supplier_price" placeholder="' . lang('supplier_price') . '"'); ?>
									</div>
								</div>
							</div>
							<div id="ex-suppliers"></div>
						</div>
					</div>
                </div>

                <div class="col-md-12" >

                    <div class="form-group" style="display:none;">
                        <input name="cf" type="checkbox" class="checkbox" id="extras" value="" checked="checked"/><label
                            for="extras" class="padding05"><?= lang('custom_fields') ?></label>
                    </div>
					
                    <div class="row" id="extras-con" style="display:none;">

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf1', 'cf1') ?>
                                <?= form_input('cf1', (isset($_POST['cf1']) ? $_POST['cf1'] : ($product ? $product->cf1 : '')), 'class="form-control tip" id="cf1"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf2', 'cf2') ?>
                                <?= form_input('cf2', (isset($_POST['cf2']) ? $_POST['cf2'] : ($product ? $product->cf2 : '')), 'class="form-control tip" id="cf2"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf3', 'cf3') ?>
                                <?= form_input('cf3', (isset($_POST['cf3']) ? $_POST['cf3'] : ($product ? $product->cf3 : '')), 'class="form-control tip" id="cf3"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf4', 'cf4') ?>
                                <?= form_input('cf4', (isset($_POST['cf4']) ? $_POST['cf4'] : ($product ? $product->cf4 : '')), 'class="form-control tip" id="cf4"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf5', 'cf5') ?>
                                <?= form_input('cf5', (isset($_POST['cf5']) ? $_POST['cf5'] : ($product ? $product->cf5 : '')), 'class="form-control tip" id="cf5"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf6', 'cf6') ?>
                                <?= form_input('cf6', (isset($_POST['cf6']) ? $_POST['cf6'] : ($product ? $product->cf6 : '')), 'class="form-control tip" id="cf6"') ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group all">
                        Instrunções para o contrato
                        <textarea name="instrucoescontrato" style="width: 100%" rows="5"  id="instrucoescontrato"><?php echo (isset($_POST['instrucoescontrato']) ? $_POST['instrucoescontrato'] : ($product ? strip_tags($product->instrucoescontrato) : ''));?></textarea>
                        <small>Use a TAG @quebra_linha@ para instruir ao sistema que hávera uma nova linha na emissão do contrato.</small>
                    </div>

                    <div class="form-group all">
                        <?= lang("product_details", "product_details") ?>
                        <?= form_textarea('product_details', (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : '')), 'class="form-control" id="details"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("itinerario", "itinerario") ?>
                        <?= form_textarea('itinerario', (isset($_POST['itinerario']) ? $_POST['itinerario'] : ($product ? $product->itinerario : '')), 'class="form-control" id="itinerario"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("oqueInclui", "oqueInclui") ?>
                        <?= form_textarea('oqueInclui', (isset($_POST['oqueInclui']) ? $_POST['oqueInclui'] : ($product ? $product->oqueInclui : '')), 'class="form-control" id="oqueInclui"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("valores_condicoes", "valores_condicoes") ?>
                        <?= form_textarea('valores_condicoes', (isset($_POST['valores_condicoes']) ? $_POST['valores_condicoes'] : ($product ? $product->valores_condicoes : '')), 'class="form-control" id="valores_condicoes"'); ?>
                    </div>

                    <div class="form-group all">
                        <?= lang("product_details_for_invoice", "details") ?>
                        <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                    </div>

                    <div class="form-group">
                        <?php echo form_submit('edit_product', $this->lang->line("edit_product"), 'class="btn btn-primary"'); ?>
                    </div>

                </div>
                <?= form_close(); ?>

            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        var $supplier = $('#aFornecedor');

    	$supplier.change(function (e) {
      		$('#aFornecedor').val($(this).val());
    	});

        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }

        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

    	 $('#aFornecedor').select2({
    	        minimumInputLength: 0,
    	        ajax: {
    	            url: site.base_url + "suppliers/suggestions",
    	            dataType: 'json',
    	            quietMillis: 15,
    	            data: function (term, page) {
    	                return {
    	                    term: term,
    	                    limit: 10
    	                };
    	            },
    	            results: function (data, page) {
    	                if (data.results != null) {
    	                    return {results: data.results};
    	                } else {
    	                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
    	                }
    	            }
    	        }
    	});
    	 
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
        var items = {};
        <?php
        if($combo_items) {
            echo '
                var ci = '.json_encode($combo_items).';
                $.each(ci, function() { add_product_item(this); });
                ';
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");': '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) || $product->promotion ? '$("#promotion").iCheck("check");': '' ?>
        $('#promotion').on('ifChecked', function (e) {
            $('#promo').slideDown();
        });
        $('#promotion').on('ifUnchecked', function (e) {
            $('#promo').slideUp();
        });

        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });
		
        $('#type').change(function () {
				
            var t = $(this).val();
            if (t !== 'standard') {
                $('.standard').slideDown();
				$('#datas_viagem').show();
				$('#div_status').show();
				$('#div_category').show();
				$('#div_product_details').show();
                $('#cost').attr('required', 'required');
                $('#track_quantity').iCheck('uncheck');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
            } else {
                $('.standard').slideDown();
				$('#datas_viagem').show();
				$('#div_status').show();
				$('#div_category').show();
				$('#div_product_details').show();
                $('#track_quantity').iCheck('check');
                $('#cost').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#digital_file').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
            } else {
                $('.digital').slideDown();
                $('#digital_file').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
            }
			
            if (t !== 'combo') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideUp();
            }
			
			if (t == 'service') {
                $('.combo').slideUp();
				$('.standard').slideUp();
				$('.digital').slideUp();
				$('#datas_viagem').hide();
				$('#div_status').hide();
				$('#div_category').hide();
				$('#div_product_details').hide();
            }  
        });

        $("#add_item").autocomplete({
            source: '<?= site_url('products/suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 5,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                        $('#add_item').removeAttr('required');
                        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });
        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');

        function add_product_item(item) {
            if (item == null) {
                return false;
            }
            item_id = item.id;
            if (items[item_id]) {
                items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            } else {
                items[item_id] = item;
            }

            $("#prTable tbody").empty();
            $.each(items, function () {
                var row_no = this.id;
                var newTr = $('<tr id="row_' + row_no + '" class="item_' + this.id + '"></tr>');
                tr_html = '		<td><input name="combo_item_id[]" type="hidden" value="' + this.id + '"><input name="combo_item_name[]" type="hidden" value="' + this.name + '"><input name="combo_item_code[]" type="hidden" value="' + this.code + '"><span id="name_' + row_no + '">' + this.name + ' (' + this.code + ')</span></td>';
                tr_html += '	<td><input class="form-control text-center" name="combo_item_quantity[]" type="text" value="' + formatDecimal(this.qty) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '	<td><input class="form-control text-center" name="combo_item_price[]" type="text" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '	<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.prependTo("#prTable");
            });
            $('.item_' + item_id).addClass('warning');
            //audio_success.play();
            return true;

        }

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            $(this).closest('#row_' + id).remove();
            $.each(items, function (i, v) {
                if (v.id == id) {
                    delete items[i];
                }
            });
        });
		
        var su = 2;
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                $('#supplier_1').select2('destroy');
                var html = '<div style="clear:both;height:5px;"></div><div class="row"><div class="col-xs-12"><div class="form-group"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);

                    img.src = 'images/' + result[i];
                }
            }
        });

        var variants = <?=json_encode($vars);?>;
        $(".select-tags").select2({
            tags: variants,
            tokenSeparators: [","],
            multiple: true
        });

        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });

         $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });
        
         $('#addAttributes').click(function (e) {
             
             e.preventDefault();
             var attrs_val = $('#attributesInput').val(); 
             var attrs = attrs_val.split(',');
                         
             for (var i in attrs) {
                 if (attrs[i] !== '') {
                     var tableNew  = '';
                     tableNew = tableNew + '<tr class="attr">';
         			 tableNew = tableNew + '	<td><input type="hidden" name="deletar[]"   value="false"> <input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>' + attrs[i] + '</span></td>';
         			 tableNew = tableNew + '	<td class="fornecedor text-left"><input type="hidden" name="attr_fornecedor[]" value=""><span></span></td>';
         			 tableNew = tableNew + '	<td class="totalPoltrona text-center" style="display:none;"><input type="hidden" name="att_totalPoltrona[]" value=""><span></span></td>';
         			 tableNew = tableNew + '	<td class="totalPessoasConsiderar text-center" style="display:none;"><input type="hidden" name="att_totalPessoasConsiderar[]" value=""><span></span></td>';
         			 tableNew = tableNew + '	<td class="localizacao text-right" style="display:none;"><input type="hidden" name="attr_localizacao[]" value=""><span></span></span></td>';
         			 tableNew = tableNew + '	<td class="cost text-right" style="display:none;"><input type="hidden" name="attr_cost[]" value="0"><span>0</span></td>';
         			 tableNew = tableNew + '	<td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td>';
         			 tableNew = tableNew + '	<td class="totalPreco text-right"><input type="hidden" name="attr_totalPreco[]" value="0"><span>0</span></span></td>';
         			 tableNew = tableNew + '	<td class="text-center"><i class="fa fa-times delAttr"></i></td>';
         			 tableNew = tableNew + '</tr>';
         			 $('#attrTable').show().append(tableNew);
                 }
             }
             
         });

        $(document).on('click', '.delAttr', function () {
        	$(this).closest("tr").hide();
 
        	var html = $(this).closest("tr").children().eq(0).html();
			html = html.replace('name="deletar[]" value="false"', 'name="deletar[]" value="true"');
			$(this).closest("tr").children().eq(0).html(html);
			
  			var totalPreco = $(this).closest("tr").children().eq(7).find('span').text()
			var valorSugerido = $('#cost').val();
			totalPreco = parseFloat(totalPreco);
			valorSugerido = parseFloat(valorSugerido);

            calcularPrecoSugeridoPacote();
            //$('#cost').val( (valorSugerido - totalPreco).toFixed(2));
        });

        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });

        $(document).on('blur', '#acost', function () {
        	calcularValorSugerido();
        });

        $(document).on('blur', '#aprice', function () {
        	calcularValorSugerido();  
        });

        $(document).on('blur', '#aTotalPessoasConsiderar', function () {
        	calcularValorSugerido();
        });

        function calcularValorSugerido() { 
            
        	var typeCalculo = $('#typeCalculo');
        	var aMargem = $('#acost');
        	var aprice = $('#aprice');
        	var aCalculoPreco = $('#aCalculoPreco');

        	var aMargem = parseFloat(aMargem.val());
        	var aprice = parseFloat(aprice.val());
        	var calculo = 0;
        	
        	if (typeCalculo.val() == 1) {
				if (aMargem > 0 && aprice > 0) {
					aMargem = aMargem / 100;
					calculo = aprice + (aprice * aMargem)
				} else {
					calculo = aprice;
				}
            } else if (typeCalculo.val() == 2) {
				var aTotalPessoasConsiderar = $('#aTotalPessoasConsiderar');
				aTotalPessoasConsiderar = parseFloat(aTotalPessoasConsiderar.val())
				if (aprice > 0 && aTotalPessoasConsiderar > 0) {
					calculo = aprice/aTotalPessoasConsiderar;
				} else {
					calculo = aprice;
				}
            } else if (typeCalculo.val() == 3) {
            	if (aMargem > 0 && aprice > 0) {
					aMargem = aMargem / 100;
					calculo = aprice + (aprice * aMargem)
				} else { 
					calculo = aprice;
				}

            	var aTotalPessoasConsiderar = $('#aTotalPessoasConsiderar');
				aTotalPessoasConsiderar = parseFloat(aTotalPessoasConsiderar.val())
				if (calculo > 0 && aTotalPessoasConsiderar > 0) {
					calculo = calculo/aTotalPessoasConsiderar;
				} 
				
            } else {
            	calculo = aprice;
            }

        	aCalculoPreco.val(parseFloat(calculo).toFixed(2));
        }
        
		var row;
		var warehouses = <?= json_encode($warehouses); ?>;
		var fornecedores = <?= json_encode($fornecedores); ?>;
		var variants = <?= json_encode($variants); ?>;
		
        $(document).on('click', '.attr td:not(:last-child)', function () {
            
            row = $(this).closest("tr");

			var nomeVariacao = row.children().eq(0).find('span').text();
  			var is_transporte = 0;
  			var lancamento_type = 0;
  			var usar_localizacao  = 0;
  			var usar_fornecedor = 0;
  			var calculo_type = 0;
  			var categoria_fornecedor = 0;
            $('#div_informacaoLancamentoIndividual').html('');

            $.each(variants, function () {
                if (this.name == nomeVariacao) {
                 	is_transporte = this.is_transporte;
                	lancamento_type = this.lancamento_type;
                	usar_localizacao = this.usar_localizacao;  
                	calculo_type = this.calculo_type; 
                	usar_fornecedor = this.usar_fornecedor;
                	categoria_fornecedor = this.group_id;             	
                }
            });

            var aDivFornecedorVariants = $('#aDivFornecedorVariants');
            if (usar_fornecedor==1) {
            	aDivFornecedorVariants.show();
            } else {
            	aDivFornecedorVariants.hide();
            }

            var aDivTotalPoltronasVariants = $('#aDivTotalPoltronasVariants');

            if (is_transporte == 1) {
            	aDivTotalPoltronasVariants.hide();
            } else {
            	aDivTotalPoltronasVariants.hide();
            }

            var aDivTotalPessoasConsiderarVariants = $('#aDivTotalPessoasConsiderarVariants');
            var aDivMargemVariants = $('#aDivMargemVariants');

            if (calculo_type == 1) {
            	aDivTotalPessoasConsiderarVariants.hide();
            	aDivMargemVariants.hide();
            } else if (calculo_type == 2){
            	aDivTotalPessoasConsiderarVariants.hide();
            	aDivMargemVariants.hide();
                $('#div_informacaoLancamentoIndividual').html(nomeVariacao + ' terá o lançamento financeiro individual e para o calculo do preço sugerido do pacote será multiplicado pelo número mínimo de passageiros.');
            } else if (calculo_type == 3) {
            	aDivTotalPessoasConsiderarVariants.hide();
            	aDivMargemVariants.hide();
            } else if (calculo_type == 4) {
            	aDivTotalPessoasConsiderarVariants.hide();
            	aDivMargemVariants.hide();
            }

			var aDivlocalizacaoVariants = $('#aDivlocalizacaoVariants');

			if (usar_localizacao == 1) {
				aDivlocalizacaoVariants.show();
			} else {
				aDivlocalizacaoVariants.hide();
			}
            
    		var margem_padrao = $('#margem_padrao').val();
			var custo = row.children().eq(5).find('span').text();

			if (custo == '0' || custo == '') {
				custo = margem_padrao;
			}

			var quantidadadePessoaConsiderarPadrao = $('#alert_quantity').val();
			var aTotalPessoasConsiderar = row.children().eq(3).find('span').text();

			if (aTotalPessoasConsiderar == '0' || aTotalPessoasConsiderar == '') {
				aTotalPessoasConsiderar = quantidadadePessoaConsiderarPadrao;
			}			

			var typeCalculo = $('#typeCalculo');
			typeCalculo.val(calculo_type);
			
            $('#aModalLabel').text(nomeVariacao);
            
			var aFornecedor = row.children().eq(1).find('input').val() != null ?  row.children().eq(1).find('input').val()  : '';
			 
 			if (aFornecedor > 0){
	            $('#aFornecedor').select2({
	                minimumInputLength: 0,
	                data: [],
	                initSelection: function (element, callback) {
	                    $.ajax({
	                        type: "get",
	                        async: false,
	                        url: site.base_url+"suppliers/getSupplier/" + aFornecedor,
	                        dataType: "json",
	                        quietMillis: 15,
	                        success: function (data) {
  	                            callback(data[0]);
	                        }
	                    });
	                },
	                ajax: {
	                    url: site.base_url + "suppliers/suggestionsGroup",
	                    dataType: 'json',
	                    quietMillis: 15,
	                    data: function (term, page) {
	                        return {
	                            term: term,
	                            limit: 10,
	                            group: categoria_fornecedor,
	                        };
	                    },
	                    results: function (data, page) {
	                        if (data.results != null) {
	                            return {results: data.results};
	                        } else {
	                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
	                        }
	                    }
	                }
	            });
		
				$('#aFornecedor').val(aFornecedor).trigger('change');
  
 			} else {
 				$('#aFornecedor').select2({
 	    	        minimumInputLength: 0,
 	    	        ajax: {
 	    	            url: site.base_url + "suppliers/suggestionsGroup",
 	    	            dataType: 'json',
 	    	            quietMillis: 15,
 	    	            data: function (term, page) {
 	    	                return {
 	    	                    term: term,
 	    	                    limit: 10,
 	    	                   	group: categoria_fornecedor,
 	    	                };
 	    	            },
 	    	            results: function (data, page) {
 	    	                if (data.results != null) {
 	    	                    return {results: data.results};
 	    	                } else {
 	    	                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
 	    	                }
 	    	            }
 	    	        }
 	    		});
 				$('#aFornecedor').val('').trigger('change');
 	 	 	}

            $('#aTotalPoltronas').val(row.children().eq(2).find('input').val());
            //$('#aTotalPessoasConsiderar').val(aTotalPessoasConsiderar);
            //$('#acost').val(custo);
            $('#aLocalizacao').val(row.children().eq(4).find('span').text());
            $('#aprice').val(row.children().eq(6).find('span').text());
            $('#aCalculoPreco').val(row.children().eq(7).find('span').text());
            
            $('#aModal').appendTo('body').modal('show');
        });

        $(document).on('click', '#updateAttr', function () {

            var typeCalculo = $('#typeCalculo');
            var wh = $('#awarehouse').val();
            var wh_name;
			var quantidade 	= 0;
 			
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });

            var forn = $('#aFornecedor').val();
			var fornNome = '';
			$.each(fornecedores, function () {
                if (this.id == forn) {
                	fornNome = this.company+' ('+this.name+')';
                }
            });
			
            row.children().eq(1).html('<input type="hidden" name="attr_fornecedor[]" value="' + forn+ '"><span>' +fornNome+ '</span>');
            row.children().eq(2).html('<input type="hidden" name="att_totalPoltrona[]" value="' + $('#aTotalPoltronas').val() + '"><span>' + $('#aTotalPoltronas').val() + '</span>');
            row.children().eq(3).html('<input type="hidden" name="att_totalPessoasConsiderar[]" value="' + $('#aTotalPessoasConsiderar').val() + '"><span>' + $('#aTotalPessoasConsiderar').val()+ '</span>');
            row.children().eq(4).html('<input type="hidden" name="attr_localizacao[]" value="' + $('#aLocalizacao').val() + '"><span>' + $('#aLocalizacao').val() + '</span>');
            row.children().eq(5).html('<input type="hidden" name="attr_cost[]" value="' + $('#acost').val() + '"><span>' + $('#acost').val() + '</span>');
            row.children().eq(6).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + $('#aprice').val() + '</span>');
            row.children().eq(7).html('<input type="hidden" typeCalculo='+typeCalculo.val()+' name="attr_totalPreco[]" value="' + $('#aCalculoPreco').val() + '"><span>' + $('#aCalculoPreco').val() + '</span>');

            calcularValorSugerido();
 
        	var valorSugerido = 0;
            $("input[name='attr_totalPreco[]']").each(function(){
             	var attr_totalPreco = $(this).val();
            	if (attr_totalPreco != '') {
            		attr_totalPreco = parseFloat(attr_totalPreco);
            		valorSugerido = valorSugerido + attr_totalPreco;
            	}
            });
            
            //$('#cost').val(valorSugerido.toFixed(2));
            calcularPrecoSugeridoPacote();
 			$('#aModal').modal('hide');
        });
        
    });

    <?php if ($product) { ?>
    $(document).ready(function () {
        $('#enable_wh').click(function () {
            var whs = $('.wh');
            $.each(whs, function () {
                $(this).val($('#v' + $(this).attr('id')).val());
            });
            $('#warehouse_quantity').val(1);
            $('.wh').attr('disabled', false);
            $('#show_wh_edit').slideDown();
        });
        $('#disable_wh').click(function () {
            $('#warehouse_quantity').val(0);
            $('#show_wh_edit').slideUp();
        });
        $('#show_wh_edit').hide();
        $('.wh').attr('disabled', true);
		
        var t = "<?=$product->type?>";
		if (t !== 'standard') {
			$('.standard').slideDown();
			$('#datas_viagem').show();
			$('#div_status').show();
			$('#div_category').show();
			$('#div_product_details').show();
			$('#cost').attr('required', 'required');
			$('#track_quantity').iCheck('uncheck');
			$('form[data-toggle="validator"]').bootstrapValidator('addField', 'cost');
		} else {
			$('.standard').slideDown();
			$('#datas_viagem').show();
			$('#div_status').show();
			$('#div_category').show();
			$('#div_product_details').show();
			$('#track_quantity').iCheck('check');
			$('#cost').removeAttr('required');
			$('form[data-toggle="validator"]').bootstrapValidator('removeField', 'cost');
		}
		if (t !== 'digital') {
			$('.digital').slideUp();
			$('#digital_file').removeAttr('required');
			$('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
		} else {
			$('.digital').slideDown();
			$('#digital_file').attr('required', 'required');
			$('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
		}
		
		if (t !== 'combo') {
			$('.combo').slideUp();
		} else {
			$('.combo').slideUp();
		}
		
		if (t == 'service') {
			$('.combo').slideUp();
			$('.standard').slideUp();
			$('.digital').slideUp();
			$('#datas_viagem').hide();
			$('#div_status').hide();
			$('#div_category').hide();
			$('#div_product_details').hide();
		}  
		
        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");
		
        $.ajax({
            type: "get", async: false,
            url: "<?= site_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: scdata
                    });
                }
            }
        });
        <?php if ($product->supplier1) { ?>
        select_supplier('supplier1', "<?= $product->supplier1; ?>");
        $('#supplier_price').val("<?= $this->sma->formatDecimal($product->supplier1price); ?>");
        $('#supplier_part_no').val("<?= $product->supplier1_part_no; ?>");
        <?php } else { ?>
            $('#supplier1').addClass('rsupplier');
        <?php } ?>
        <?php if ($product->supplier2) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_2', "<?= $product->supplier2; ?>");
        $('#supplier_2_price').val("<?= $this->sma->formatDecimal($product->supplier2price); ?>");
        $('#supplier_2_part_no').val("<?= $product->supplier2_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier3) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_3', "<?= $product->supplier3; ?>");
        $('#supplier_3_price').val("<?= $this->sma->formatDecimal($product->supplier3price); ?>");
        $('#supplier_3_part_no').val("<?= $product->supplier3_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier4) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_4', "<?= $product->supplier4; ?>");
        $('#supplier_4_price').val("<?= $this->sma->formatDecimal($product->supplier4price); ?>");
        $('#supplier_4_part_no').val("<?= $product->supplier4_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier5) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_5', "<?= $product->supplier5; ?>");
        $('#supplier_5_price').val("<?= $this->sma->formatDecimal($product->supplier5price); ?>");
        $('#supplier_5_part_no').val("<?= $product->supplier5_part_no; ?>");
        <?php } ?>
        function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= site_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        }
    });
    <?php } ?>
    $(document).ready(function () {
        $('#enable_wh').trigger('click');

    });

    function calcularPrecoSugeridoPacote() {

        var margem_padrao = $('#margem_padrao').val();
        var quantidadadePessoaConsiderarPadrao = $('#alert_quantity').val();

        if (margem_padrao == '') {
            margem_padrao = 0;
        }

        if (quantidadadePessoaConsiderarPadrao == '') {
            quantidadadePessoaConsiderarPadrao = 0;
        }

        margem_padrao = parseFloat(margem_padrao);
        quantidadadePessoaConsiderarPadrao = parseFloat(quantidadadePessoaConsiderarPadrao);


        var valorSugerido = 0;
        $("input[name='attr_totalPreco[]']").each(function(){

            debugger;

            var attr_totalPreco = $(this).val();
            var typeCalculo   = $(this).attr('typeCalculo');
            typeCalculo = parseFloat(typeCalculo);

            if (attr_totalPreco != '') {
                attr_totalPreco = parseFloat(attr_totalPreco);

                if (typeCalculo == 2 && quantidadadePessoaConsiderarPadrao > 0) {
                    attr_totalPreco = attr_totalPreco / quantidadadePessoaConsiderarPadrao;
                }
                valorSugerido = valorSugerido + attr_totalPreco;
            }
        });

        if (margem_padrao > 0){
            margem_padrao = margem_padrao/100;
            valorSugerido = (valorSugerido * margem_padrao) + valorSugerido
        }
        $('#cost').val(valorSugerido.toFixed(2));
    }

    $(document).ready(function () {
        setTimeout(function () {
            $('#instrucoescontrato').redactor('destroy');
            var instrucoes = $('#instrucoescontrato').val();
            instrucoes = instrucoes.replace('<p>','');
            instrucoes = instrucoes.replace('</p>','');
            $('#instrucoescontrato').val(instrucoes);
        }, 2000);
    });
</script>

<!-- ################################################# -->
<!-- ######## MODAL DA COMPOSICAO DE CUSTO ########### -->
<!-- ################################################# -->
<div class="modal" id="aModal" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="aModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                
                
                    <div class="form-group" style="display: none;">
                        <label for="awarehouse" class="col-sm-4 control-label"><?= lang('warehouse') ?></label>
                        <div class="col-sm-8">
                            <?php
                            $wh[''] = '';
                            foreach ($warehouses as $warehouse) {
                                $wh[$warehouse->id] = $warehouse->name;
                            }
                            echo form_dropdown('warehouse', $wh, '', 'id="awarehouse" class="form-control"');
                            ?>
                        </div>
                    </div>
                    
                    
                    <div class="form-group" id="aDivFornecedorVariants">
                        <label for="fornecedor" class="col-sm-4 control-label"><?= lang('fornecedor') ?></label>
                        <div class="col-sm-8">
                            <input type="hidden" name="fornecedor" value="" id="aFornecedor" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("fornecedor") ?>">
                        </div>
                    </div>
                    
                    <div class="form-group" style="display: none;">
                        <label for="aquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" value="1" id="aquantity">
                        </div>
                    </div>
                    
                    <div class="form-group" id="aDivTotalPoltronasVariants" style="display:none;">
                        <label for="aTotalPoltronas" class="col-sm-4 control-label"><?= lang('aTotalPoltronas') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control mask_integer" id="aTotalPoltronas">
                        </div>
                    </div>
                    
                                      
                    <div class="form-group">
                        <label for="aprice" class="col-sm-4 control-label"><?= lang('price_total') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control mask_money" id="aprice">
                            <small><div id="div_informacaoLancamentoIndividual"></div></small>
                        </div>
                    </div>
                    
                   <div class="form-group" id="aDivMargemVariants" style="display: none;">
                        <label for="acost" class="col-sm-4 control-label"><?= lang('margem') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control mask_money" id="acost">
                        </div>
                    </div>
                    
                    
                    <div class="form-group" id="aDivTotalPessoasConsiderarVariants">
                        <label for="aTotalPessoasConsiderar" class="col-sm-4 control-label"><?= lang('aTotalPessoasConsiderar') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control mask_integer" id="aTotalPessoasConsiderar">
                        </div>
                    </div>
                    
                    <div class="form-group" style="display: none;">
                        <label for="aCalculoPreco" class="col-sm-4 control-label"><?= lang('aCalculoPreco') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" disabled id="aCalculoPreco">
                        </div>
                    </div>
                    
                    <div class="form-group" id="aDivlocalizacaoVariants">
                        <label for="aLocalizacao" class="col-sm-4 control-label"><?= lang('localizacao') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aLocalizacao">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateAttr"><?= lang('submit') ?></button>
                <input type="hidden" id="typeCalculo">
            </div>
        </div>
    </div>
</div>

