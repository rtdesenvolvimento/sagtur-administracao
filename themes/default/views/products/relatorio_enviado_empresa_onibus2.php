<?php

function tirarAcentos($string){
    return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
    //return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
} ?>

<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Helvetica Neue", Helvetica, "Noto Sans", sans-serif, Arial, sans-serif;
            font-size: 12px;
        }
    </style>
<body>
<?php foreach($listOnibus as $onibus){
    $fornecedor = $onibus->fornecedor;
    $supplier_details 	= $this->site->getCompanyByID($fornecedor);
    $supplier 			= $supplier_details->name;
    $company            = $supplier_details->company;
    ?>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th colspan="3" align="center">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
                <h3>Relat&oacute;rio de Passageiros</h3> <br/><br/>
            </th>
        </tr>
        </thead>
    </table>
    <table border="1" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th colspan="5" align="center">
                <h3><?php echo $company; ?></h3>
            </th>
        </tr>
        </thead>

        <thead>
        <tr>
            <th align="left">Nome Completo</th>
            <th align="center">Carteira de Identidade</th>
            <th align="center">Data Nascimento</th>
            <th align="center">Assento</th>
            <th align="center">Telefone</th>
        </tr>
        </thead>
        <tbody>
        <?php

        $local_saida = '';
        foreach ($itens_pedidos_groupBy as $itemGroup) {

            $itens_pedidos = $this->site->getInvoiceByProductOrderByClienteFilterLocalEmbarque($itemGroup->product_id, $itemGroup->local_saida);

            foreach ($itens_pedidos as $row) {

                $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);

                $customer               = $objCustomer->name;
                $rg                     = $objCustomer->cf1;
                $data_aniversario       = $objCustomer->data_aniversario;
                $reference_no_variacao  = $row->reference_no_variacao;
                $phone                  = $objCustomer->phone;

                $poltronaClient         = $row->poltronaClient;
                $customerClientName     = $row->customerClientName;

                if ($poltronaClient) $str_contador = $poltronaClient;
                if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

                ?>
                <?php if ($reference_no_variacao == $onibus->id )  {?>

                    <?php if ( tirarAcentos(strtoupper(trim($row->local_saida))) != tirarAcentos(strtoupper(trim($local_saida)))) {
                        $local_saida = $row->local_saida;
                        ?>
                        <tr>
                            <td colspan="5" style="background: #e08e0b"><?php echo $row->local_saida;?></td>
                        </tr>
                    <?php }?>
                    <tr>
                        <td align="left"><?php echo $customer;?></td>
                        <td align="center"><?php echo $rg;?></td>
                        <td align="center"><?php echo $data_aniversario;?></td>
                        <td align="center"><?php echo $str_contador;?></td>
                        <td align="center"><?php echo $phone;?></td>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php }?>
        </tbody>
    </table>
<?php }?>
</body>
</html>
