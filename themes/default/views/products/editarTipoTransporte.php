<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_tipo_transporte'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/editarTipoTransporte/" . $tipoTransporte->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group all">
                <?= lang("bus_map", "bus_map") ?>
                <?php
                $cbMapa[''] = lang("select") . " " . lang("bus_map");
                foreach ($automoveis as $automovel) {
                    $cbMapa[$automovel->id] = $automovel->name;
                }
                echo form_dropdown('automovel_id', $cbMapa, (isset($_POST['automovel_id']) ? $_POST['automovel_id'] : $tipoTransporte->automovel_id), 'class="form-control select" id="bus_map" placeholder="' . lang("select") . " " . lang("bus_map") . '"style="width:100%"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $tipoTransporte->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group" id="div_totalPoltronas">
                <?= lang('totalPoltronas', 'totalPoltronas'); ?>
                <?= form_input('totalPoltronas', $tipoTransporte->totalPoltronas, 'class="form-control mask_integer" id="totalPoltronas" required="required"'); ?>
            </div>
            <?php echo form_hidden('id', $tipoTransporte->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editarTipoTransporte', lang('editar_tipo_transporte'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>

    $(document).ready(function () {
        $('#bus_map').change(function (event){
            if ($(this).val() !== '') {
                let name =  $('#bus_map option:selected').text();

                $('#name').val(name);
                $('#totalPoltronas').val(0);
                $('#div_totalPoltronas').hide();
            } else  {
                $('#div_totalPoltronas').show();
            }
        });

        <?php if ($tipoTransporte->automovel_id) {?>
            $('#div_totalPoltronas').hide();
        <?php } ?>
    });

    var mask_integer = {
        money : function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'$1');
                }
                return v;
            };
            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(function(){
        $('.mask_integer').bind('keypress',mask_integer.money);
        $('.mask_integer').click(function(){$(this).select();});
    });
</script>

