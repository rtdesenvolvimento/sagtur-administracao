<div class="available-trips" id="backing">

    <div class="trip-dates">
        <ul>
            <li></li>
            <li class="active"><span
                        style="font-weight: bold;"><?php echo '' . $onibus->id . ' - ' . $onibus->name; ?></span></li>
            <li><span> </span></li>
        </ul>
    </div>

    <div class="trip-destiny">
        <strong>Sa&iacute;da</strong>: <?php echo $product->origem . ' - ' . $product->data_saida ?><br/>
        <strong>Destino</strong>: <?php echo $product->destino . ' - ' . $product->data_chegada; ?><br/>
        <strong>Retorno</strong>: <?php echo $product->data_retorno; ?>
    </div>

    <h3>Selecione a poltrona do passageiro.</h3>
    <p class="legend">
        <span class="red"></span>Ocupada<br>
        <span class="green"></span>livre<br>
        <span class="orange"></span>Reservada<br>
        <span class="grey"></span>Escadas<br>
    </p>
    <div id="onibus-volta-frente"></div>
    <div id="onibus-volta-bg">
        <center>
            <table class="cpo1" border="">
                <tbody>
                <?php
                $contador = 1;
                $total_poltronas = $onibus->totalPoltrona;
                $total_poltronas_1_andar = 39;
                $filas = $total_poltronas_1_andar / 4;

                for ($i = 0; $i < $filas; $i++):
                    $class = 'libre'; ?>
                    <tr>
                        <?php
                        $str_contador = '';
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>
                        <td class="<?php echo $class ?>" variacao="<?php echo $onibus->id ?>"
                            poltrona="<?php echo $str_contador ?>" customer="<?php echo $ocupado->customer ?>"
                            venda="<?php echo $ocupado->id ?>"
                            onclick="mostra_venda(this)"><?php echo $str_contador ?></td>

                        <?php
                        $str_contador = '';
                        $contador = $contador + 1;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }

                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>
                        <td class="<?php echo $class ?>" variacao="<?php echo $onibus->id ?>"
                            poltrona="<?php echo $str_contador ?>"
                            customer="<?php echo $ocupado->customer ?>"
                            venda="<?php echo $ocupado->id ?>"
                            onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
                        <td>&nbsp;&nbsp;</td>
                        <td class="vacia" id="ida_" onclick="mostra_venda(this)"></td>
                        <td>&nbsp;&nbsp;</td>

                        <?php
                        $str_contador = '';
                        $contador = $contador + 2;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>

                        <?php
                        $str_contador = '';
                        $contador = $contador - 1;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>

                        <?php if ($contador == 7) { ?>
                            <td class="seleccionada"><?php echo '<b>ES</b>'; ?></td>
                        <?php } else { ?>
                            <td class="<?php echo $class ?>"
                                variacao="<?php echo $onibus->id ?>"
                                poltrona="<?php echo $str_contador ?>"
                                customer="<?php echo $ocupado->customer ?>"
                                venda="<?php echo $ocupado->id ?>"
                                onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
                        <?php } ?>
                    </tr>
                    <?php $contador = $contador + 2; endfor; ?>
                </tbody>
            </table>
        </center>
    </div>
    <div id="onibus-volta-traseira"><font color="green" style="text-transform: uppercase;font-weight: bold;">
            <center>Sal&atilde;o Superior</center>
        </font></div>
</div>

<!-- ############################## -->
<!-- ##### SEGUNDO ANDAR ########## -->
<!-- ############################## -->

<div class="available-trips" id="backing">

    <div class="trip-dates">
        <ul>
            <li></li>
            <li class="active"><span style="font-weight: bold;"><?php echo '' . $onibus->id . ' - ' . $onibus->name; ?></span></li>
            <li><span> </span></li>
        </ul>
    </div>

    <div class="trip-destiny">
        <strong>Sa&iacute;da</strong>: <?php echo $product->origem . ' - ' . $product->data_saida ?><br/>
        <strong>Destino</strong>: <?php echo $product->destino . ' - ' . $product->data_chegada; ?><br/>
        <strong>Retorno</strong>: <?php echo $product->data_retorno; ?>
    </div>

    <h3>Selecione a poltrona do passageiro.</h3>
    <p class="legend">
        <span class="red"></span>Ocupada<br>
        <span class="green"></span>livre<br>
        <span class="orange"></span>Reservada<br>
        <span class="grey"></span>Escadas<br>
    </p>
</div>
<div class="available-trips" id="backing">

    <div id="onibus-volta-frente"></div>
    <div id="onibus-volta-bg">
        <center>
            <table class="cpo1" border="">
                <tbody>
                <?php
                $contador = $contador;
                $total_poltronas = $onibus->totalPoltrona;
                $filas = $onibus->totalPoltrona / 4;

                for ($i = 0; $i < $filas; $i++):
                    $class = 'libre'; ?>
                    <tr>
                        <?php
                        $str_contador = '';
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>
                        <td class="<?php echo $class ?>" customer="<?php echo $ocupado->customer ?>"
                            venda="<?php echo $ocupado->id ?>"
                            onclick="mostra_venda(this)"><?php echo $str_contador ?></td>

                        <?php
                        $str_contador = '';
                        $contador = $contador + 1;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }

                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>
                        <td class="<?php echo $class ?>"
                            variacao="<?php echo $onibus->id ?>"
                            poltrona="<?php echo $str_contador ?>"
                            customer="<?php echo $ocupado->customer ?>"
                            venda="<?php echo $ocupado->id ?>"
                            onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
                        <td>&nbsp;&nbsp;</td>
                        <td class="vacia" id="ida_" onclick="mostra_venda(this)"></td>
                        <td>&nbsp;&nbsp;</td>

                        <?php
                        $str_contador = '';
                        $contador = $contador + 2;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $ocupado = $this->products_model->getSaleByPoltronaByItemVenda($str_contador, $onibus->id);
                            if ($ocupado) {
                                $class = 'ocupada';
                            } else {
                                $class = 'libre';
                            }
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>

                        <?php
                        $str_contador = '';
                        $contador = $contador - 1;
                        if ($contador < 9) {
                            $str_contador = '0' . $contador;
                        } else {
                            $str_contador = $contador;
                        }
                        $ocupado = $this->products_model->getSaleByPoltrona($str_contador, $onibus->id);
                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $class = 'libre';
                        }

                        if ($contador > $total_poltronas) {
                            $class = 'vacia';
                        }
                        ?>

                        <?php if ($contador == 7) { ?>
                            <td class="seleccionada"><?php echo '<b>ES</b>'; ?></td>
                        <?php } else { ?>
                            <td class="<?php echo $class ?>"
                                variacao="<?php echo $onibus->id ?>"
                                poltrona="<?php echo $str_contador ?>"
                                customer="<?php echo $ocupado->customer ?>"
                                venda="<?php echo $ocupado->id ?>"
                                onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
                        <?php } ?>
                    </tr>
                    <?php $contador = $contador + 2; endfor; ?>
                </tbody>
            </table>
        </center>
    </div>
    <div id="onibus-volta-traseira"><font color="green" style="text-transform: uppercase;font-weight: bold;">
            <center>Sal&atilde;o Secund&aacute;rio</center>
        </font></div>
</div>