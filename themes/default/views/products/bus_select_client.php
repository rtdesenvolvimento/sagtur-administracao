<!DOCTYPE html>
<html>
<title>Relat&oacute;rio de acessos da viagem <?php echo $product->name; ?></title>
<META http-equiv="Content-Type" content="text/html;charset=UTF-8">
<link href="<?= $assets ?>poltronas_arquivos/layout-pt.css" rel="stylesheet" type="text/css" media="all">
<link href="<?= $assets ?>poltronas_arquivos/bus.css" rel="stylesheet"  type="text/css" media="screen">
<body style="background: whitesmoke;">
<div class="internal" id="content" style="margin-left: 80px;">
    <div class="wrap">
        <div id="current-trips">
            <?php
            $index = 1;
            foreach($listOnibus as $onibus){
                $variacao = $this->products_model->getVariantByName($onibus->name);
                $transport_type = $variacao->transport_type;?>
                <?php if ($transport_type == 1) {?>
                    <?php include 'bus_template_1_andar.php';?>
                <?php } else {?>
                    <?php  include 'bus_template_2_andares.php';?>
                <?php }?>
            <?php $index = $index + 1; } ?>
        </div>
    </div>
</div>
<script>
    function mostra_venda(tab) {
        var venda = tab.getAttribute("venda");
        if (venda != '') {
            var customer = tab.getAttribute("customer");
            if (confirm('Passageiro '+customer+' \nDeseja abrir os dados da poltrona?')) {
                var url = '<?php echo base_url() ?>sales/edit/' + venda;
                window.open(url,'_blank');
            }
        } else {
            var variacao = tab.getAttribute("variacao");
            var poltrona = tab.getAttribute("poltrona");
            window.parent.setar_poltrona_client(poltrona, variacao);
        }
    }
</script>
</body>
</html>