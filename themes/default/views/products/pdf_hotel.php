<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
			font-size: 12.5px;
		}
	</style>
<body>
    <table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
        <tr>
            <th colspan="3" align="center">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?><br/>
                <?php
                $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                $nomeViagem = strtoupper($product->name).' '.$data; ?>
                <h5>RELATÓRIO POR TIPO DE HOSPEDAGEM <br/> <?php echo $nomeViagem; ?></h5> <br/>
            </th>
        </tr>
    </thead>
    </table>
    <br/>
    <table class="table table-bordered" border="1" style="width: 100%;border-collapse:collapse;">
    <thead>
        <tr>
            <th style="background: #ccc;">&nbsp;Hospede(s)</th>
            <th style="background: #ccc;text-align: center;">Tipo de Quarto</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $tiposHospedagem = [];

        foreach ($itens_pedidos as $row) {
            $cliente 	 = $this->site->getCompanyByID($row->customerClient);
            $customer    = $cliente->name;

            if ($row->descontarVaga) {
                $contador    =  $tiposHospedagem[$row->tipoHospedagem]['qtd'] + 1;
            } else {
                $contador    =  $tiposHospedagem[$row->tipoHospedagem]['qtd'];
            }

            $tiposHospedagem[$row->tipoHospedagem] = array(
                'nome' => $row->tipoHospedagem,
                'qtd' => $contador,
            );

            ?>
            <tr>
                <td>&nbsp;<?php echo $cliente->name;?><small> (<?php echo $row->faixaNome;?>)</small></td>
                <td style="text-align: center;"><?php echo $row->tipoHospedagem;?></td>
            </tr>
        <?php } ?>
    </tbody>
    </table>
    <table class="table table-bordered" border="1" style="width: 100%;border-collapse:collapse;padding: 2px;">
    <thead>
        <tr>
            <th style="background: #ccc;">Tipo de Quarto</th>
            <th style="background: #ccc;">Qtd.</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $total_pass = 0;
    foreach ($tiposHospedagem as $tp){?>
        <tr>
            <td><?php echo $tp['nome'];?></td>
            <td style="text-align: right;"><?php echo $tp['qtd'];?></td>
        </tr>
    <?php
        $total_pass += $tp['qtd'];
    }?>
    <tr>
        <td>Total PAX</td>
        <td style="text-align: right;"><?=$total_pass;?></td>
    </tr>
    </tbody>
    </table>
</body>
</html>
