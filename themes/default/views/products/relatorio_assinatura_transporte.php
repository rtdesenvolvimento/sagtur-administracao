<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        .assinatura {
            border: 1px solid #0b0b0b;
            padding: 8px;
        }
    </style>
<body>
<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
            <?php
            $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
            $nomeViagem = strtoupper($product->name).' '.$data;
            ?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
            <h4>ASSINATURA DOS PASSAGEIROS</h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
            <h5>Destino: <?=$nomeViagem?> - <?php echo strtoupper($tipoTransporte->name);?></h5>
        </td>
    </tr>
    </thead>
</table>
<table style="width: 100%;">
    <thead>
     <tr>
        <th align="left" width="35%">PASSAGEIRO</th>
         <th style="text-align: center;" width="20%">RG</th>
         <th align="center" width="15%">CPF</th>
        <th align="center" width="30%">&nbsp;ASSINATURA&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $contador = 1;
    $isColorBackground = false;

    foreach ($itens as $row) {

            $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);

            $customer               = $objCustomer->name;
            $cpf                    = $objCustomer->vat_no;
            $rg                     = $objCustomer->cf1;
            $orgaoEmissor           = $objCustomer->cf3;
            $data_aniversario       = $objCustomer->data_aniversario;
            $tipoFaixaEtaria        = $row->faixaNome;

            if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

            $background = "#eee";

            if ($isColorBackground) {
                $background = "#eee";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }

            ?>
            <tr style="background: <?=$background;?>;">
                <td align="left" class="assinatura">&nbsp;<?php echo $customer.' <small style="font-size: 7px;">('.lang($tipoFaixaEtaria).')</small>';?></td>
                <td align="center" class="assinatura"><?php echo $rg;?></td>
                <td align="center" class="assinatura"><?php echo $cpf;?></td>
                <td align="center" class="assinatura"></td>
            </tr>

    <?php }?>
    </tbody>
</table>

</body>
</html>
