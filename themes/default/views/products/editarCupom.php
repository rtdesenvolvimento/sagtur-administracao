<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_cupom'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/editarCupom/" . $cupom->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('update_info'); ?></p>
            <div class="form-group">
                <?= lang('active', 'active'); ?>
                <?php
                $opt = array(1 => lang('active'), 0 => lang('inactive'));
                echo form_dropdown('active', $opt, $cupom->active, 'id="active" required="required" class="form-control select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('name', 'name'); ?>
                <?= form_input('name', $cupom->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('codigo_cupom', 'codigo'); ?>
                <?= form_input('codigo', $cupom->codigo, 'class="form-control" id="codigo" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("tipo_desconto", "tipo_desconto") ?>
                <?php
                $opts = array(
                    'ABSOLUTO' => lang('absoluto'),
                    'PERCENTUAL' => lang('percentual'),
                );
                echo form_dropdown('tipo_desconto', $opts, $cupom->tipo_desconto, 'class="form-control" id="tipo_desconto" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("valor", "valor"); ?>
                <?= form_input('valor', $cupom->valor, 'class="form-control tip mask_money" id="valor" style="padding-right: 5px;" required="required" ') ?>
            </div>
            <div class="form-group">
                <?= lang('all_products', 'all_products'); ?>
                <?php
                $opt = array(0 => lang('no'), 1 => lang('yes'));
                echo form_dropdown('all_products', $opt, $cupom->all_products, 'id="all_products" required="required" class="form-control select" style="width:100%;"');
                ?>
                <span>
                    <?= lang("info_cupom_desconto") ?>
                </span>
            </div>
            <div class="form-group all_products">
                <?= lang("quantidade_disponibilizada", "quantidade_disponibilizada") ?>
                <?= form_input('quantidade_disponibilizada', $cupom->quantidade_disponibilizada, 'class="form-control tip mask_integer"') ?>
            </div>
            <div class="form-group all_products">
                <?= lang("data_inicio_validade", "data_inicio") ?>
                <?= form_input('dataInicio', $cupom->dataInicio, 'class="form-control"', 'date'); ?>
            </div>
            <div class="form-group all_products">
                <?= lang("data_final_validade", "data_final") ?>
                <?= form_input('dataFinal', $cupom->dataFinal, 'class="form-control"', 'date'); ?>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('descricao', $cupom->descricao, 'class="form-control" id="descricao" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('editarCupom', lang('editar_cupom'), 'class="btn btn-primary"'); ?>
            </div>
            <?php echo form_hidden('id', $cupom->id); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
<script language="javascript">
    $(document).ready(function () {

        <?php if ($cupom->all_products){?>
            $('.all_products').show();
        <?php } else {?>
            $('.all_products').hide();
        <?php } ?>

        $('#codigo').on('keyup', (ev) => {
            let codigoDigitado = $('#codigo').val().toUpperCase();
            codigoDigitado = codigoDigitado.split(' ').join('');
            $('#codigo').val(codigoDigitado);
        });

        $('#all_products').change(function (evet){
            if ($(this).val() === '1') {
                $('.all_products').show();
            } else {
                $('.all_products').hide();
            }
        });

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });
    });
</script>
