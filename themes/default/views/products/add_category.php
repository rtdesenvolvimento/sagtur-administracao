<style>
    .selecionado {
        background: #fdf59a;
        border: 2px solid #333;
    }

    .btn_icon {
        padding: 6px 6px 6px 12px;
    }
</style>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_category'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("products/add_category", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <?php echo lang('category_code', 'code'); ?>
                <div class="controls">
                    <?php echo form_input($code); ?>
                </div>
            </div>
            <div class="form-group">
                <?php echo lang('category_name', 'name'); ?>
                <div class="controls">
                    <?php echo form_input($name); ?>
                </div>
            </div>
            <div class="form-group all">
                <input type="hidden" id="image_icon_id" name="image_icon_id" value="">
                <?php foreach ($icons as $icon) {?>
                    <button type="button" id="<?=$icon->id;?>" class="btn btn_icon" icon_id="<?=$icon->id;?>"><?=$icon->icon;?></button>
                <?php }?>
            </div>
            <div class="form-group" style="display: none;">
                <?= lang("category_image", "image") ?>
                <input id="image" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_category', lang('add_category'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>

<script type="text/javascript">

    $(document).ready(function() {

        $('.btn_icon').click(function (evnet){

            var icon_id = $(this).attr('icon_id');

            $('.btn_icon').removeClass('selecionado');
            $('#'+icon_id).addClass('selecionado');

            $('#image_icon_id').val(icon_id);
        });
    });

</script>

