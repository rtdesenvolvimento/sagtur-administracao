<?php

function tirarAcentos($string){
    return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
}


$assentosOcupados = $this->products_model->getSaleByPoltronaByItemVendaForMarcacao($tipoTransporte->id, $programacao->id);


?>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?= $assets ?>styles/bus/css/core.css" rel="stylesheet"/>

    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        .scaffold-bus-container {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            max-width: 665px
        }

        .scaffold-bus {
            background-image: url(https://gessicaalbuquerque.excapy.com/images/svg/inside-bus.svg);
            background-repeat: no-repeat;
            background-size: 100%;
            height: 207px;
            padding: 14px 0 0 90px;
            position: relative;
            width: 665px
        }

        .seat-icon {
            border-radius: 2px;
            flex-shrink: 0;
            height: 16px;
            margin-right: .5rem;
            width: 16px
        }

        .seat-free {
            background-color: #7ecc00
        }

        .seat-selected {
            background-color: #ffcc3e
        }

        .seat-not_found {
            background: red
        }

        .legend-seat-busy,.seats-summary .seat-occupied {
            background-image: url(/images/svg/legend-seat-busy.svg)
        }

        .seats-subtitles {
            display: flex;
            margin: 1rem 2rem 2rem
        }

        .seats-summary__subtitle {
            height: 1.6rem;
            overflow: hidden;
            width: 100%
        }

        .seats-summary__subtitle-action {
            cursor: pointer
        }

        .seats-subtitles li {
            align-items: center;
            color: #383838;
            display: flex;
            margin-right: 1rem
        }

        .seats-summary {
            margin: 0 2rem 3rem 0;
            width: 200px
        }

        .seats-summary__title {
            color: #000075
        }

        .seats-summary__seat-selected {
            margin: .5rem 0 0
        }

        .seats-selected-list {
            font-size: 1rem
        }

        .selected-seat-item {
            align-items: center;
            display: flex;
            height: 1.8rem;
            overflow: hidden
        }

        .selected-seat-item-action {
            color: #c00000;
            cursor: pointer;
            margin: 0 0 0 1rem
        }

        .seats-summary__no-seat-selected {
            color: #393939;
            font-size: .9rem
        }

        .seat-map-name-container {
            margin: 1rem 0 0;
            width: 100%
        }

        .seatBusApp {
            width: 100%
        }

        .bus-map-name {
            margin: 0
        }

        .bus-seats {
            display: block;
            height: 180px;
            margin: 0;
            position: relative;
            z-index: 1
        }

        .bus-seats .seat {
            border-radius: 4px 4px 4px 4px;
            cursor: pointer;
            height: 32px;
            line-height: 0px;
            opacity: 1;
            text-align: center;
            width: 36px;
            font-size: 14px;
            font-weight: 600;
        }

        .bus-seats .seatNumberLabel {
            display: inline-block
        }

        .bus-seats .seat-occupied {
            cursor: default
        }

        .bus-seats .seat-occupied .seatNumberLabel {
            background-image: url(/images/svg/x-thin.svg);
            background-repeat: no-repeat;
            background-size: 100%;
            height: 25px;
            line-height: 25px;
            margin: -28px 0 0 -14px;
            position: absolute;
            text-align: center;
            width: 25px
        }

        .bus-seats .seat-occupied:before {
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-occupied:after,.bus-seats .seat-occupied:before {
            background-color: #fff;
            border: 1px solid #c5c5c5;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block
        }

        .bus-seats .seat-occupied:after {
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-free .seatNumberLabel {
            color: #fff;
            height: 32px;
            line-height: 32px;
            margin: -32px 0 0 -18px;
            position: absolute;
            text-align: center;
            width: 32px
        }

        .bus-seats .seat-free:before {
            background-color: #7ecc00;
            border: 1px solid #7ecc00;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-free:after {
            background-color: #66a403;
            border: 1px solid #43ab2a;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-selected .seatNumberLabel {
            color: #5d1499;
            height: 32px;
            line-height: 32px;
            margin: -32px 0 0 -18px;
            position: absolute;
            text-align: center;
            width: 32px
        }

        .bus-seats .seat-selected:before {
            background-color: #ffcc3e;
            border: 1px solid #ffcc3e;
            border-image: initial;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-selected:after {
            background-color: #f1b100;
            border: 1px solid #f1b100;
            border-image: initial;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-not_found .seatNumberLabel {
            color: #fff;
            height: 32px;
            line-height: 32px;
            margin: -32px 0 0 -18px;
            position: absolute;
            text-align: center;
            width: 32px
        }

        .bus-seats .seat-not_found:before {
            background-color: red;
            border: 1px solid red;
            border-image: initial;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-not_found:after {
            background-color: #a80000;
            border: 1px solid #bf1515;
            border-image: initial;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-highlight:after {
            background: aqua
        }

        .seats-summary .seat-highlight .seat-selected {
            position: relative
        }

        .seats-summary .seat-highlight .seat-selected:after {
            border-bottom: 3px solid aqua;
            bottom: 1px;
            content: " ";
            display: block;
            left: 3px;
            position: absolute;
            width: 11px
        }

        .bus-seats .y-0 {
            bottom: 0;
            position: absolute
        }

        .bus-seats .y-1 {
            bottom: 42px;
            position: absolute
        }

        .bus-seats .y-2 {
            bottom: 84px;
            position: absolute;
            top: 42px
        }

        .bus-seats .y-3 {
            bottom: 84px;
            position: absolute;
            top: 0
        }

        .bus-seats .x-0 {
            left: 0;
            position: absolute
        }

        .bus-seats .x-1 {
            left: 40px;
            position: absolute
        }

        .bus-seats .x-2 {
            left: 80px;
            position: absolute
        }

        .bus-seats .x-3 {
            left: 120px;
            position: absolute
        }

        .bus-seats .x-4 {
            left: 160px;
            position: absolute
        }

        .bus-seats .x-5 {
            left: 200px;
            position: absolute
        }

        .bus-seats .x-6 {
            left: 240px;
            position: absolute
        }

        .bus-seats .x-7 {
            left: 280px;
            position: absolute
        }

        .bus-seats .x-8 {
            left: 320px;
            position: absolute
        }

        .bus-seats .x-9 {
            left: 360px;
            position: absolute
        }

        .bus-seats .x-10 {
            left: 400px;
            position: absolute
        }

        .bus-seats .x-11 {
            left: 440px;
            position: absolute
        }

        .bus-seats .x-12 {
            left: 480px;
            position: absolute
        }

        .bus-seats .x-13 {
            left: 520px;
            position: absolute
        }

        .bus-seats .x-14 {
            left: 560px;
            position: absolute
        }

        .base-floor.inactive,.floors.inactive,.superior-floor.inactive {
            display: none
        }

        .floor-button-first {
            margin: 0 1rem 0 0
        }

        .floor-button-active {
            background: #268d77;
            color: #fff
        }

        .floor-button-active:hover {
            background: #2ca48a;
            color: #fff
        }

        .busMapApp {
            width: 100%
        }

        .seat-enabled {
            background-color: #7ecc00
        }

        .seat-disabled {
            background-color: #ff5a5f
        }

        .seat-reservado {
            background-color: #ffcc3e
        }

        .bus-seats .seat-disabled {
            color: #fff
        }

        .bus-seats .seat-disabled .seatNumberLabel {
            height: 25px;
            line-height: 25px;
            margin: -28px 0 0 -14px;
            position: absolute;
            text-align: center;
            width: 25px
        }

        .bus-seats .seat-disabled:before {
            background-color: #ff5a5f;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-disabled:after {
            background-color: #991a1a;
            border: 1px solid #bc3030;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-enabled .seatNumberLabel {
            color: #fff;
            height: 32px;
            line-height: 32px;
            margin: -32px 0 0 -18px;
            position: absolute;
            text-align: center;
            width: 32px
        }

        .bus-seats .seat-enabled:before {
            background-color: #7ecc00;
            border: 1px solid #7ecc00;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 100%;
            width: 90%
        }

        .bus-seats .seat-enabled:after {
            background-color: #6eb300;
            border: 1px solid #6eb300;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .bus-seats .seat-reservado:after {
            background-color: #f1b100;
            border: 1px solid #f1b100;
            border-radius: 2px 0 0 2px;
            content: "";
            display: block;
            height: 76%;
            margin: -78% 0 0 85%;
            width: 22%
        }

        .form-group input,.form-group label {
            margin-bottom: .2rem
        }

        .form-group .radio-input-label {
            margin: 0 1rem 0 0
        }

        .form-group input[type=radio] {
            height: 18px;
            width: 18px
        }

        .busMapForm {
            margin: 0;
            width: 100%
        }

        .busMapForm>div {
            margin: 0 0 1rem
        }

        .busMapNameContainer {
            width: 100%
        }

        .top-parent-container {
            padding: 0 0 2rem
        }

        .parent-title {
            margin: 0
        }

        .busMapAppContainer {
            align-items: center;
            display: flex;
            flex-direction: column;
            margin: 3rem auto 0;
            max-width: 665px;
            width: 100%
        }


        .scaffold-bus {
            left:50%;
            margin: 15rem 0 0;
            position: absolute;
            transform: translate(-50%) rotate(90deg)
        }

        .bus-seats .seatNumberLabel {
            transform: rotate(-90deg)
        }

        .seat-map {
            height: 680px;
            margin: 0 auto 1rem;
            position: relative;
            width: 100%
        }

        .seats-subtitles {
            /*flex-direction: column;*/
            margin: 0;
            position: absolute;
            right: 0;
            top: -20px
        }


        .seat-map {
            margin:4rem 0 1rem
        }

        .seats-subtitles {
            right: inherit
        }

        .busMapAppContainer .floors.inactive,.seatBusAppContainer .floors.inactive {
            display: none
        }

        .busMapAppContainer .floors,.seatBusAppContainer .floors {
            display: flex;
            flex-direction: column
        }

        .floor-button-first,.floors p {
            margin: 0 0 1rem
        }


    </style>
<body>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: center;width: 10%;border-bottom: 1px solid #0b0b0b;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
                <?php
                $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                $nomeViagem = strtoupper($product->name).' - '.$data;
                ?>
            </td>
            <td style="text-align: center;width: 90%;border-bottom: 1px solid #0b0b0b;">
                <h5><?=$nomeViagem?> <br/> <?php echo strtoupper($tipoTransporte->name);?></h5>
            </td>
        </tr>
        </thead>
    </table>






    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="box">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <main>
                                    <div class="top-parent-container">
                                        <div class="busMapAppContainer">
                                            <div class="busMapApp">
                                                <div class="scaffold-bus-container bus-map">
                                                    <div class="seat-map">
                                                        <div class="scaffold-bus">
                                                            <ul class="bus-seats superior-floor active">
                                                                <?php foreach ($assentos as $assento) {

                                                                    $ocupado = 'enabled';
                                                                    $nome_passageiro = '';
                                                                    $bloqueado_id = false;
                                                                    $venda = false;

                                                                    foreach ($assentosOcupados as $ocup) {
                                                                        if ($ocup->poltrona == $assento->name) {
                                                                            $ocupado = 'disabled';
                                                                            $nome_passageiro = $ocup->customer;
                                                                            $venda = $ocup->id;
                                                                        }
                                                                    }

                                                                    foreach ($bloqueados as $bloqueado) {
                                                                        if ($bloqueado->str_assento == $assento->name) {
                                                                            $ocupado = 'reservado';
                                                                            $nome_passageiro = $bloqueado->note;
                                                                            $bloqueado_id = $bloqueado->id;
                                                                        }
                                                                    }

                                                                    ?>
                                                                    <li style="<?php if (!$assento->habilitado) echo 'display:none';?>"
                                                                        onclick="bloquie_assento(this);"
                                                                        bloqueio="<?=$bloqueado_id;?>"
                                                                        nome_passageiro="<?=$nome_passageiro;?>"
                                                                        venda="<?=$venda;?>"
                                                                        data-bs-toggle="modal"
                                                                        data-bs-target="#axisMapModal"
                                                                        class="seat seat-<?=$assento->name;?> seat-<?=$ocupado;?> x-<?=$assento->x;?> y-<?=$assento->y;?> z-<?=$assento->z;?>"
                                                                        data-seat="<?=$assento->assento;?>"
                                                                        data-status="<?=$ocupado;?>"
                                                                        data-position-x="<?=$assento->x;?>"
                                                                        data-position-y="<?=$assento->y;?>"
                                                                        data-position-z="<?=$assento->z;?>"
                                                                        data-position-order="<?=$assento->assento;?>"
                                                                        data-position-order_name="<?=$assento->name;?>">
                                                                        <span class="seatNumberLabel" title="<?=$nome_passageiro?>"><?=$assento->name;?></span>
                                                                        <div><?=$nome_passageiro;?></div>
                                                                    </li>
                                                                <?php }?>
                                                            </ul>
                                                        </div>
                                                        <ul class="seats-subtitles">
                                                            <li>
                                                                <span class="seat-icon seat-enabled"></span>
                                                                <span class="subtitle">Livre</span>
                                                            </li>
                                                            <li>
                                                                <span class="seat-icon seat-disabled"></span>
                                                                <span class="subtitle">Ocupado</span>
                                                            </li>
                                                            <li>
                                                                <span class="seat-icon seat-reservado"></span>
                                                                <span class="subtitle">Bloqueado</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </main>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <table border="0" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th style="width: 5%;text-align: center;">#</th>
            <th style="width: 10%;text-align: center;">Poltrona</th>
            <th style="width: 40%;text-align: left;">Passageiro</th>
            <th style="width: 25%;text-align: left;">Documento</th>
            <th style="width: 20%;text-align: left;">Celular</th>

            <?php if ($Settings->show_payment_report_shipment) {?>
                <th style="width: 20%;text-align: right">Receber</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $locaEmbarque = '';
        $contador = 1;
        $isColorBackground = false;
        $totalPassageirosPagantes = 0;
        $totalCriancasColo = 0;

        foreach ($locaisDeEmbarque as $localDeEmbarque) {

            $itens = $this->site->getItensVendasPorLocalEmbarque($product->id, $tipoTransporte->id, $programacao->id, $localDeEmbarque->id);

            foreach ($itens as $row) {

                $totalAbertoPorDependente = '';

                if ($row->ocultar_faixa_relatorio) continue;

                if ($Settings->dependent_shipping_report) {

                    $totalPagarItem = $row->subtotal;
                    $totalPago      = $row->paid;
                    $acrescimo      = $row->shipping;
                    $desconto       = $row->order_discount;
                    $totalVenda     = $row->grand_total - $acrescimo + $desconto;

                    $totalRealPagoPorDependente = $totalPagarItem * (($row->paid*100/$totalVenda)/100);
                    $totalAberto   =  $totalPagarItem - $totalRealPagoPorDependente;

                    if ($totalAberto > 0) {
                        $totalAbertoPorDependente = $this->sma->formatMoney($totalAberto);

                        if ($acrescimo > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Acres: '.$this->sma->formatMoney($acrescimo).'</small>';
                        }

                        if ($desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Desc:'.$this->sma->formatMoney($desconto).'</small>';
                        }

                        if ($acrescimo > 0 || $desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Pagar:'.$this->sma->formatMoney($totalAberto + $acrescimo - $desconto).'</small>';
                        }

                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                } else {
                    if ($row->customer_id == $row->customerClient) {
                        $totalEmAberto = $row->grand_total - $row->paid;

                        if ($totalEmAberto > 0) {
                            $totalAbertoPorDependente = $this->sma->formatMoney($totalEmAberto);
                        } else {
                            $totalAbertoPorDependente = 'PAGO';
                        }
                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                }

                $poltronaCliente = '';
                $telefone = '';
                $tipoFaixaEtaria = '';

                $cliente 	 = $this->site->getCompanyByID($row->customerClient);

                $nomeCliente            = $cliente->name;
                $rg                     = $cliente->cf1;
                $orgaoEmissor           = $cliente->cf3;
                $data_aniversario       = $cliente->data_aniversario;
                $phone                  = $cliente->phone;
                $whatsApp               = $cliente->cf5;
                $telefoneEmergencia     = $cliente->telefone_emergencia;
                $tipoDocumento          = $cliente->tipo_documento;
                $doenca_informar        = $cliente->doenca_informar;
                $social_name            = $cliente->social_name;

                if ($phone)  {
                    $telefone       = $phone;
                }

                if ($whatsApp) {
                    $telefone   .= ' '.$whatsApp;
                }

                if ($telefoneEmergencia) {
                    $telefone .= '<br/>Emergência '.$telefoneEmergencia;
                }

                if ($social_name) {
                    $nomeCliente .= '<br/><small> Nome Social: ' . $social_name. '</small>';
                }

                if ($doenca_informar) {
                    $nomeCliente .= '<br/><small> ' . $doenca_informar. '</small>';
                }

                if ($row->poltronaClient) $poltronaCliente = $row->poltronaClient;
                if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

                $background = "#eee";

                if ($isColorBackground) {
                    $background = "#eee";
                    $isColorBackground = false;
                } else {
                    $background = "#ffffff";
                    $isColorBackground = true;
                }
                ?>
                <?php if ($localDeEmbarque->id == $row->localEmbarque) {?>
                    <?php if ($locaEmbarque != $row->localEmbarque) {?>
                        <tr style="text-align: center;">
                            <td colspan="6" style="background: #ccc;font-size: 12px;">&nbsp;
                                <?php
                                $dados = $localDeEmbarque->name;
                                if ($localDeEmbarque->note) {
                                    $dados .= ' - '.$localDeEmbarque->note;
                                }

                                if ($localDeEmbarque->dataEmbarque != null && $localDeEmbarque->dataEmbarque != '0000-00-00'){
                                    $dados .= ' - <span style="font-weight: bold;">'.$this->sma->hrsd($localDeEmbarque->dataEmbarque).'</span>';
                                }

                                if ($localDeEmbarque->horaEmbarque != null && $localDeEmbarque->horaEmbarque != '00:00:00')  {
                                    $dados .= '  <span style="font-weight: bold;">'.date('H:i', strtotime($localDeEmbarque->horaEmbarque)).'h </span>';
                                }
                                ?>

                                <?php echo $dados?>
                            </td>
                        </tr>
                        <?php $locaEmbarque = $row->localEmbarque;?>
                    <?php }?>
                    <tr style="background: <?=$background;?>">
                        <?php if (!$row->descontarVaga) {
                            $totalCriancasColo++;
                            ?>
                            <td style="text-align: center;">(C) </td>
                        <?php } else {
                            $sContador = $contador < 10 ? '0'.$contador : $contador;
                            $totalPassageirosPagantes++;
                            ?>
                            <td style="text-align: center;"><?=$sContador;?></td>
                            <?php $contador++?>
                        <?php } ?>
                        <td align="center"><?php echo $poltronaCliente;?></td>
                        <td align="left" style="width: 40%">&nbsp;<?php echo $nomeCliente;?></td>
                        <?php if ($orgaoEmissor){?>
                            <td style="text-align: left;"><?php echo strtoupper($tipoDocumento).': '.$rg.'/'.$orgaoEmissor;?></td>
                        <?php } else {?>
                            <td style="text-align: left;"><?php echo strtoupper($tipoDocumento).': '.$rg?></td>
                        <?php } ?>
                        <td align="left"><?php echo $telefone;?></td>

                        <?php if ($Settings->show_payment_report_shipment) {?>
                            <td style="text-align: right;"><?php echo $totalAbertoPorDependente;?></td>
                        <?php } ?>

                    </tr>
                <?php } ?>
            <?php } ?>
        <?php }?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <tr style="background: #eee;">
            <td colspan="6" style="border-top: 1px solid #0b0b0b;">
                TOTAL DE PASSAGEIROS PAGANTES: <?=$totalPassageirosPagantes;?>
            </td>
        </tr>
        <tr style="background: #eee;">
            <td colspan="6">
                TOTAL NÃO PAGANTES: <?=$totalCriancasColo;?>
            </td>
        </tr>
        </tfoot>
    </table>
</body>
</html>