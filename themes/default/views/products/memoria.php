<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('add_product'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>

                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("products/add", $attrib)
                ?>

                <div class="col-md-5">


                    <div class="form-group" style="display:none">
                        <?= lang("product_type", "type") ?>
                        <?php
                        $opts = array( 'combo' => lang('combo') );
                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required"');
                        ?>
                    </div>





                    <div class="form-group" style="display: none;">
                        <?= lang("simbolo_moeda", "simbolo_moeda") ?>
                        <?php
                        $opts = array('R$' => lang('R$') ,'USD' => lang('USD') , '€' => lang('Euro'),'£' => lang('Libra') );
                        echo form_dropdown('simbolo_moeda', $opts, (isset($_POST['simbolo_moeda']) ? $_POST['simbolo_moeda'] : ($product ? $product->simbolo_moeda : '')), 'class="form-control" id="simbolo_moeda" ');
                        ?>
                    </div>

                    <!--###########################-->
                    <!--## DATA INICIO / TERMINO ##
                    <!--###########################-->
                    <div id="datas_viagem">
                        <div class="well well-sm">

                            <div class="form-group">
                                <?= lang('informacoes_viagem'); ?>
                            </div>







                            <div class="form-group" id="div_status" style="display: none;">
                                <?= lang("enviar_site", "enviar_site") ?>
                                <?php
                                $opts = array('sim' => lang('sim') , 'nao' => lang('nao') );
                                echo form_dropdown('enviar_site', $opts, (isset($_POST['enviar_site']) ? $_POST['enviar_site'] : ($product ? $product->enviar_site : '')), 'class="form-control" id="enviar_site"');
                                ?>
                            </div>

                            <div class="form-group" id="div_status" style="display: none;">
                                <?= lang("apenas_cotacao", "apenas_cotacao") ?>
                                <?php
                                $opts = array( 'nao' => lang('nao') , 'sim' => lang('sim') );
                                echo form_dropdown('apenas_cotacao', $opts, (isset($_POST['apenas_cotacao']) ? $_POST['apenas_cotacao'] : ($product ? $product->apenas_cotacao : '')), 'class="form-control" id="apenas_cotacao"');
                                ?>
                            </div>

                            <div class="form-group all" style="display: none;">
                                <?= lang("valor_pacote", "valor_pacote") ?>
                                <?= form_textarea('valor_pacote', (isset($_POST['valor_pacote']) ? $_POST['valor_pacote'] : ($product ? $product->valor_pacote : '')), 'class="form-control" id="valor_pacote"') ?>
                                <small>Esta informação só será exibida para pacotes com cotação de passagem. Deixe em branco para que o sistema use o preço do pacote </small>
                            </div>














                        </div>
                    </div>

                    <div class="form-group all" style="display:none;">
                        <?= lang("barcode_symbology", "barcodnumero pessoase_symbology") ?>
                        <?php
                        $bs = array('code25' => 'Code25', 'code39' => 'Code39', 'code128' => 'Code128', 'ean8' => 'EAN8', 'ean13' => 'EAN13', 'upca' => 'UPC-A', 'upce' => 'UPC-E');
                        echo form_dropdown('barcode_symbology', $bs, (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control select" id="barcode_symbology" required="required" style="width:100%;"');
                        ?>
                    </div>

                    <div class="form-group" style="display:none;">
                        <input type="checkbox" class="checkbox" value="1" name="promotion" id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?>>
                        <label for="promotion" class="padding05">
                            <?= lang('promotion'); ?>
                        </label>
                    </div>

                    <div id="promo" style="display: none;">
                        <div class="well well-sm">
                            <div class="form-group">
                                <?= lang('promo_price', 'promo_price'); ?>
                                <input type="number" name="promo_price" value="" class="form-control tip" id="promo_price" data-original-title="" title="">                            </div>
                            <div class="form-group" style="display: none;">
                                <?= lang('start_date', 'start_date'); ?>
                                <?= form_input('start_date', set_value('start_date'), 'class="form-control tip date" id="start_date"'); ?>
                            </div>
                            <div class="form-group" style="display: none;">
                                <?= lang('end_date', 'end_date'); ?>
                                <?= form_input('end_date', set_value('end_date'), 'class="form-control tip date" id="end_date"'); ?>
                            </div>
                        </div>
                    </div>

                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group all">
                            <?= lang("product_tax", "tax_rate") ?>
                            <?php
                            $tr[""] = "";
                            foreach ($tax_rates as $tax) {
                                $tr[$tax->id] = $tax->name;
                            }
                            echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("tax_method", "tax_method") ?>
                            <?php
                            $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                            echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
                            ?>
                        </div>
                    <?php } ?>


                </div>


                <div class="col-md-6 col-md-offset-1">
                    <div class="standard">
                        <div id="attrs"></div>



                        <div class="<?= $product ? 'text-warning' : '' ?>" style="display:none">
                            <strong><?= lang("warehouse_quantity") ?></strong><br>
                            <?php
                            if (!empty($warehouses)) {
                                if ($product) {
                                    echo '<div class="row"><div class="col-md-12"><div class="well"><div id="show_wh_edit">';
                                    if (!empty($warehouses_products)) {
                                        echo '<div style="display:none;">';
                                        foreach ($warehouses_products as $wh_pr) {
                                            echo '<span class="bold text-info">' . $wh_pr->name . ': <span class="padding05" id="rwh_qty_' . $wh_pr->id . '">' . $this->sma->formatQuantity($wh_pr->quantity) . '</span>' . ($wh_pr->rack ? ' (<span class="padding05" id="rrack_' . $wh_pr->id . '">' . $wh_pr->rack . '</span>)' : '') . '</span><br>';
                                        }
                                        echo '</div>';
                                    }
                                    foreach ($warehouses as $warehouse) {
                                        //$whs[$warehouse->id] = $warehouse->name;
                                        echo '<div class="col-md-6 col-sm-6 col-xs-6" style="padding-bottom:15px;">' . $warehouse->name . '<br><div class="form-group">' . form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : (isset($warehouse->quantity) ? $warehouse->quantity : '')), 'class="form-control wh" required="required"  id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '"') . '</div>';
                                        if ($this->Settings->racks) {
                                            echo '<div class="form-group">' . form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : (isset($warehouse->rack) ? $warehouse->rack : '')), 'class="form-control wh" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"') . '</div>';
                                        }
                                        echo '</div>';
                                    }
                                    echo '</div><div class="clearfix"></div></div></div></div>';
                                } else {
                                    echo '<div class="row"><div class="col-md-12"><div class="well">';
                                    foreach ($warehouses as $warehouse) {
                                        //$whs[$warehouse->id] = $warehouse->name;
                                        echo '<div class="col-md-6 col-sm-6 col-xs-6" style="padding-bottom:15px;">' . $warehouse->name . '<br><div class="form-group">' . form_hidden('wh_' . $warehouse->id, $warehouse->id) . form_input('wh_qty_' . $warehouse->id, (isset($_POST['wh_qty_' . $warehouse->id]) ? $_POST['wh_qty_' . $warehouse->id] : ''), 'class="form-control" id="wh_qty_' . $warehouse->id . '" placeholder="' . lang('quantity') . '"') . '</div>';
                                        if ($this->Settings->racks) {
                                            echo '<div class="form-group">' . form_input('rack_' . $warehouse->id, (isset($_POST['rack_' . $warehouse->id]) ? $_POST['rack_' . $warehouse->id] : ''), 'class="form-control" id="rack_' . $warehouse->id . '" placeholder="' . lang('rack') . '"') . '</div>';
                                        }
                                        echo '</div>';
                                    }
                                    echo '<div class="clearfix"></div></div></div></div>';
                                }
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div>

                    </div>


                    <div class="digital" style="display:none;">
                        <div class="form-group digital">
                            <?= lang("digital_file", "digital_file") ?>
                            <input id="digital_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="digital_file" data-show-upload="false"
                                   data-show-preview="false" class="form-control file">
                        </div>
                    </div>
                    <hr>

                    <div class="well well-sm" >
                        <!-- #################### -->
                        <!-- ### MARGEM PADRAO ## -->
                        <!-- #################### -->
                        <div class="form-group all">
                            <?= lang('numero_pessoas', 'numero_pessoas'); ?>
                            <input type="number" name="numero_pessoas" value="<?php  (isset($_POST['numero_pessoas']) ? $_POST['numero_pessoas'] : ($product ? $product->numero_pessoas : '1'));?>" class="form-control tip" id="numero_pessoas">
                        </div>

                        <!-- ################################################ -->
                        <!-- ### PRECO SUGERIDO PELO SISTEMAS PARA O PACOTE ## -->
                        <!-- ################################################ -->
                        <div class="form-group standard">
                            <?= lang("preco_sugerido", "preco_sugerido") ?>
                            <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '0')), 'class="form-control tip" readonly id="cost"  ') ?>
                        </div>

                        <!-- ############ -->
                        <!-- ### PRECO ## -->
                        <!-- ############ -->
                        <div class="form-group all">
                            <?= lang("price_pacote", "price_pacote") ?>
                            <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '0')), 'class="form-control tip mask_money" required="required" id="price" ') ?>
                            <span class="help-block"><?= lang('info_pontuacao_valor') ?></span>
                        </div>

                        <!-- ########################## -->
                        <!-- ### 0 - 5 anos de idade ## -->
                        <!-- ########################## -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_zero_cinco", "price_pacote_zero_cinco") ?>
                            <?= form_input('price_pacote_zero_cinco', (isset($_POST['price_pacote_zero_cinco']) ? $_POST['price_pacote_zero_cinco'] : ($product ? $this->sma->formatDecimal($product->price_pacote_zero_cinco) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_zero_cinco" ') ?>
                        </div>

                        <!-- ########################### -->
                        <!-- ### 6 - 11 anos de idade ## -->
                        <!-- ########################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_seis_onze", "price_pacote_seis_onze") ?>
                            <?= form_input('price_pacote_seis_onze', (isset($_POST['price_pacote_seis_onze']) ? $_POST['price_pacote_seis_onze'] : ($product ? $this->sma->formatDecimal($product->price_pacote_seis_onze) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_seis_onze" ') ?>
                        </div>

                        <!-- ################################ -->
                        <!-- ### acima de 60 anos de idade ## -->
                        <!-- ############################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("price_pacote_acima_sessenta", "price_pacote_acima_sessenta") ?>
                            <?= form_input('price_pacote_acima_sessenta', (isset($_POST['price_pacote_acima_sessenta']) ? $_POST['price_pacote_acima_sessenta'] : ($product ? $this->sma->formatDecimal($product->price_pacote_acima_sessenta) : '0')), 'class="form-control tip mask_money" required="required" id="price_pacote_acima_sessenta" ') ?>
                        </div>

                        <!-- ####################### -->
                        <!-- ### desconto a vista ## -->
                        <!-- ####################### -->
                        <div class="form-group all" style="display: none;">
                            <?= lang("percentual_desconto_a_vista", "percentual_desconto_a_vista") ?>
                            <?= form_input('percentual_desconto_a_vista', (isset($_POST['percentual_desconto_a_vista']) ? $_POST['percentual_desconto_a_vista'] : ($product ? $this->sma->formatDecimal($product->percentual_desconto_a_vista) : '0')), 'class="form-control tip mask_money" required="required" id="percentual_desconto_a_vista" ') ?>
                        </div>

                        <!-- ##################################################### -->
                        <!-- ### QUANTIDADE DE PESSOAS PARA CONFIRMAR A VIAGEM, ## -->
                        <!-- ##################################################### -->
                        <div class="form-group all">
                            <?= lang("quantidadePessoasViagem", "quantidadePessoasViagem") ?>
                            <?= form_input('quantidadePessoasViagem', (isset($_POST['quantidadePessoasViagem']) ? $_POST['quantidadePessoasViagem'] : ($product ? $this->sma->formatDecimal($product->quantidadePessoasViagem) : '')), 'class="form-control tip mask_integer" required="required" id="quantidadePessoasViagem"') ?>
                        </div>

                        <div class="form-group all" style="display: none;">
                            <?= lang("alertar_polcas_vagas", "alertar_polcas_vagas") ?>
                            <?= form_input('alertar_polcas_vagas', (isset($_POST['alertar_polcas_vagas']) ? $_POST['alertar_polcas_vagas'] : ($product ? $product->alertar_polcas_vagas : '')), 'class="form-control tip mask_integer" id="alertar_polcas_vagas"') ?>
                        </div>

                        <div class="form-group all" style="display: none;">
                            <?= lang("alertar_ultimas_vagas", "alertar_ultimas_vagas") ?>
                            <?= form_input('alertar_ultimas_vagas', (isset($_POST['alertar_ultimas_vagas']) ? $_POST['alertar_ultimas_vagas'] : ($product ? $product->alertar_ultimas_vagas : '')), 'class="form-control tip mask_integer" id="alertar_ultimas_vagas"') ?>
                        </div>
                    </div>
                    <!-- ########################################### -->
                    <!-- ### QUANTIDADE DE PESSOAS PARA O CALCULO ## -->
                    <!-- ########################################### -->
                    <div class="form-group standard" style="display: none">
                        <?= lang("alert_quantity", "alert_quantity") ?>
                        <div
                            class="input-group"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatQuantity($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                            <span class="input-group-addon">
                            <input type="checkbox" name="track_quantity" id="track_quantity"
                                   value="1" <?= ($product ? (isset($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?>>
						</span>
                        </div>
                        <span class="help-block"><?= lang('quantidade_minima_info') ?></span>
                    </div>

                    <div class="form-group standard" style="display:none">
                        <div class="form-group">
                            <?= lang("supplier_hotel", "supplier_hotel") ?>
                            <button type="button" class="btn btn-primary btn-xs" id="addSupplier"><i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="row" id="supplier-con">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <?php
                                    echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control ' . ($product ? '' : 'suppliers') . '" id="' . ($product && ! empty($product->supplier1) ? 'supplier1' : 'supplier') . '" placeholder="' . lang("select") . ' ' . lang("supplier") . '" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_part_no', (isset($_POST['supplier_part_no']) ? $_POST['supplier_part_no'] : ""), 'class="form-control tip" id="supplier_part_no" placeholder="' . lang('supplier_part_no') . '"'); ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_price', (isset($_POST['supplier_price']) ? $_POST['supplier_price'] : ""), 'class="form-control tip" id="supplier_price" placeholder="' . lang('supplier_price') . '"'); ?>
                                </div>
                            </div>
                        </div>
                        <div id="ex-suppliers"></div>
                    </div>

                </div>

                <div class="col-md-12" >
                    <div class="form-group" style="display:none;">
                        <input name="cf" type="checkbox" class="checkbox" id="extras" value="" <?= isset($_POST['cf']) ? 'checked="checked"' : '' ?>/>
                        <label for="extras" class="padding05"><?= lang('custom_fields') ?></label>
                    </div>
                    <div class="row" id="extras-con" style="display: none;">

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf1', 'cf1') ?>
                                <?= form_input('cf1', (isset($_POST['cf1']) ? $_POST['cf1'] : ($product ? $product->cf1 : '')), 'class="form-control tip" id="cf1"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf2', 'cf2') ?>
                                <?= form_input('cf2', (isset($_POST['cf2']) ? $_POST['cf2'] : ($product ? $product->cf2 : '')), 'class="form-control tip" id="cf2"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf3', 'cf3') ?>
                                <?= form_input('cf3', (isset($_POST['cf3']) ? $_POST['cf3'] : ($product ? $product->cf3 : '')), 'class="form-control tip" id="cf3"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf4', 'cf4') ?>
                                <?= form_input('cf4', (isset($_POST['cf4']) ? $_POST['cf4'] : ($product ? $product->cf4 : '')), 'class="form-control tip" id="cf4"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf5', 'cf5') ?>
                                <?= form_input('cf5', (isset($_POST['cf5']) ? $_POST['cf5'] : ($product ? $product->cf5 : '')), 'class="form-control tip" id="cf5"') ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang('pcf6', 'cf6') ?>
                                <?= form_input('cf6', (isset($_POST['cf6']) ? $_POST['cf6'] : ($product ? $product->cf6 : '')), 'class="form-control tip" id="cf6"') ?>
                            </div>
                        </div>

                    </div>

                    <div class="form-group all">
                        Instrunções para o contrato
                        <textarea name="instrucoescontrato" style="width: 100%" rows="5" id="instrucoescontrato"><?php echo (isset($_POST['instrucoescontrato']) ? $_POST['instrucoescontrato'] : ($product ? strip_tags($product->instrucoescontrato) : ''));?></textarea>
                        <small>Use a TAG @quebra_linha@ para instruir ao sistema que hávera uma nova linha na emissão do contrato.</small>
                    </div>


                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>