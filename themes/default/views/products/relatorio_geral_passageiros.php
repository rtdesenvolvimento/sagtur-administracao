<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }
        td {
            padding: 1px;
        }
        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>

<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
            <?php
            $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
            $nomeViagem = strtoupper($product->name).' '.$data;
            ?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
            <h4>RELATÓRIO GERAL DE PASSAGEIROS (APENAS VENDAS FATURADAS)</h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
            <h5>Destino: <?=$nomeViagem?></h5>
        </td>
    </tr>
    </thead>
</table>
<table border="0" style="width: 100%;">
    <thead>
    <tr>
        <th style="text-align: center;width: 5%;"></th>
        <th style="width: 25%;">Passageiro</th>
        <th style="width: 20%;text-align: center;">CPF</th>
        <th style="width: 18%;text-align: center;">R.G</th>
        <th style="width: 20%;text-align: center;">Contato</th>
        <th style="width: 11%;text-align: right;">Nascimento</th>
        <th style="width: 6%;text-align: right;">Idade</th>
    </tr>
    </thead>
    <tbody>
    <?php

    $contador = 1;
    $isColorBackground = false;

    foreach ($itens as $row) {

            $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);

            $customer                = $objCustomer->name;
            $rg                      = $objCustomer->cf1;
            $orgaoEmissor            = $objCustomer->cf3;
            $cpf                     = $objCustomer->vat_no;
            $poltrona                = $row->poltronaClient;

            $endereco               = '';
            $cep                    = $objCustomer->postal_code;
            $rua                    = $objCustomer->address;
            $cidade                 = $objCustomer->city;
            $estado                 = $objCustomer->state;
            $endereco               = '';

            if ($rua)     $endereco .= $rua.''.$cidade.'/'.$estado.' '.$cep;
            if ($cidade)  $endereco .= '<br/>'.$cidade.'/'.$estado.' '.$cep;
            if ($estado)  $endereco .= $estado.' '.$cep;
            if ($cep)     $endereco .= $cep;

            $contato                 = '';

            $email                  = $objCustomer->email;
            $whatsApp               = $objCustomer->cf5;
            $phone                  = $objCustomer->phone;
            $telefone_emergencia    = $objCustomer->telefone_emergencia;
            $data_aniversario       = $objCustomer->data_aniversario;

            if ($whatsApp) $contato                  = $whatsApp;
            if ($phone)    $contato                  = $contato.' '.$phone;
            //if ($telefone_emergencia) $contato       = $contato.' <br/> '.$telefone_emergencia;
            //if ($email) $contato                     = $contato.' <br/> '.$email;
            if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

            $documentos = '';

            $background = "#eee";

            if ($isColorBackground) {
                $background = "#eee";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }
            ?>
            <tr style="background: <?=$background;?>">
                <?php if (!$row->descontarVaga) {?>
                    <td style="text-align: center;">(C) </td>
                <?php } else {
                    $sContador = $contador < 10 ? '0'.$contador : $contador;
                    ?>
                    <td style="text-align: center;"><?=$sContador;?></td>
                    <?php $contador++?>
                <?php } ?>
                <td align="left">&nbsp;<?php echo $customer;?></td>
                <td align="center"><?php echo $cpf;?></td>
                <td align="center"><?php echo $rg.'/'.$orgaoEmissor;?></td>
                <td align="center"><?php echo $contato;?></td>
                <td style="text-align: right;"><?php echo $data_aniversario;?></td>
                <td align="center"><?php echo $this->sma->getIdade($objCustomer->data_aniversario);?></td>
            </tr>
    <?php }?>
    </tbody>
</table>
</body>
</html>
