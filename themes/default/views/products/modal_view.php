<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;"
                    onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $product->name; ?></h4>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="col-xs-5">
                    <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $product->image ?>"
                         alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>

                    <div id="multiimages" class="padding10">
                        <?php if (!empty($images)) {
                            echo '<a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $product->image . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $product->image . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                            foreach ($images as $ph) {
                                echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' . base_url() . 'assets/uploads/' . $ph->photo . '" style="margin-right:5px;"><img class="img-responsive" src="' . base_url() . 'assets/uploads/thumbs/' . $ph->photo . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                if ($Owner || $Admin || $GP['products-edit']) {
                                    echo '<a href="#" class="delimg" data-item-id="' . $ph->id . '"><i class="fa fa-times"></i></a>';
                                }
                                echo '</div>';
                            }
                        }
                        ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-xs-7">
                    <div class="table-responsive">
                        <table class="table table-borderless table-striped dfTable table-right-left">
                            <tbody>
                            <tr>
                                <td colspan="2" style="background-color:#FFF;"></td>
                            </tr>
                            <tr>
                                <td style="width:30%;"><?= lang("barcode_qrcode"); ?></td>
                                <td style="width:70%;"><?= $barcode ?>
                                    <?php $this->sma->qrcode('link', urlencode(site_url('products/view/' . $product->id)), 1); ?>
                                    <img
                                            src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                                            alt="<?= $product->name ?>" class="pull-right"/></td>
                            </tr>
                            <tr>
                                <td><?= lang("product_type"); ?></td>
                                <td><?= lang($product->type); ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("product_name"); ?></td>
                                <td><?= $product->name; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("product_code"); ?></td>
                                <td><?= $product->code; ?></td>
                            </tr>
                            <tr>
                                <td><?= lang("category"); ?></td>
                                <td><?= $category->name; ?></td>
                            </tr>
                            <?php if ($product->subcategory_id) { ?>
                                <tr>
                                    <td><?= lang("subcategory"); ?></td>
                                    <td><?= $subcategory->name; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td><?= lang("product_unit"); ?></td>
                                <td><?= $product->unit; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-xs-12">
                    <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                    <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                    <?= $product->itinerario ? '<div class="panel panel-info"><div class="panel-heading">' . lang('Roteiro') . '</div><div class="panel-body">' . $product->itinerario . '</div></div>' : ''; ?>
                    <?= $product->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading">' . lang('O que inclui') . '</div><div class="panel-body">' . $product->oqueInclui . '</div></div>' : ''; ?>
                    <?= $product->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading">' . lang('Valores e condições') . '</div><div class="panel-body">' . $product->valores_condicoes . '</div></div>' : ''; ?>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="buttons">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="<?= site_url('products/pdf/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('pdf') ?>">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= site_url('products/edit/' . $product->id) ?>" class="tip btn btn-warning tip"
                               title="<?= lang('edit_product') ?>">
                                <i class="fa fa-edit"></i> <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                            </a>
                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.tip').tooltip();
                    });
                </script>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.change_img').click(function (event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            $('#pr-image').attr('src', img_src);
            return false;
        });
    });
</script>
