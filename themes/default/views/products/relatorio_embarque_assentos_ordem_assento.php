<?php

function tirarAcentos($string){
    return preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $string ) );
} ?>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
                <?php
                $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                $nomeViagem = strtoupper($product->name).' - '.$data;
                ?>
            </td>
            <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;">
                <h4> LISTA DE ASSENTOS </h4>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
                <h5><?=$nomeViagem?> <br/> <?php echo strtoupper($tipoTransporte->name);?></h5>
            </td>
        </tr>
        </thead>
    </table>

    <table border="0" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th style="width: 5%;text-align: center;">#</th>
            <th style="width: 10%;text-align: ">Assento</th>
            <th style="width: 50%;">Passageiro</th>
            <th style="width: 40%;">Celular</th>
            <?php if ($Settings->show_payment_report_shipment) {?>
                <th style="width: 20%;text-align: right">Receber</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $locaEmbarque = '';
        $contador = 1;
        $isColorBackground = false;
        $totalPassageirosPagantes = 0;
        $totalCriancasColo = 0;

        foreach ($locaisDeEmbarque as $localDeEmbarque) {

            $itens = $this->site->getItensVendasPorLocalEmbarque($product->id, $tipoTransporte->id, $programacao->id, $localDeEmbarque->id, 'poltronaClient');

            foreach ($itens as $row) {

                $totalAbertoPorDependente = '';

                if ($row->ocultar_faixa_relatorio) continue;

                if ($Settings->dependent_shipping_report) {

                    $totalPagarItem = $row->subtotal;
                    $totalPago      = $row->paid;
                    $acrescimo      = $row->shipping;
                    $desconto       = $row->order_discount;
                    $totalVenda     = $row->grand_total - $acrescimo + $desconto;

                    $totalRealPagoPorDependente = $totalPagarItem * (($row->paid*100/$totalVenda)/100);
                    $totalAberto   =  $totalPagarItem - $totalRealPagoPorDependente;

                    if ($totalAberto > 0) {
                        $totalAbertoPorDependente = $this->sma->formatMoney($totalAberto);

                        if ($acrescimo > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Acres: '.$this->sma->formatMoney($acrescimo).'</small>';
                        }

                        if ($desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Desc:'.$this->sma->formatMoney($desconto).'</small>';
                        }

                        if ($acrescimo > 0 || $desconto > 0) {
                            $totalAbertoPorDependente = $totalAbertoPorDependente.'<br/><small style="font-size: 8px;">Pagar:'.$this->sma->formatMoney($totalAberto + $acrescimo - $desconto).'</small>';
                        }

                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                } else {
                    if ($row->customer_id == $row->customerClient) {
                        $totalEmAberto = $row->grand_total - $row->paid;

                        if ($totalEmAberto > 0) {
                            $totalAbertoPorDependente = $this->sma->formatMoney($totalEmAberto);
                        } else {
                            $totalAbertoPorDependente = 'PAGO';
                        }
                    } else {
                        $totalAbertoPorDependente = 'PAGO';
                    }
                }

                $poltronaCliente = '';
                $telefone = '';
                $tipoFaixaEtaria = '';

                $cliente 	 = $this->site->getCompanyByID($row->customerClient);

                $nomeCliente            = $cliente->name;
                $rg                     = $cliente->cf1;
                $orgaoEmissor           = $cliente->cf3;
                $data_aniversario       = $cliente->data_aniversario;
                $phone                  = $cliente->phone;
                $whatsApp               = $cliente->cf5;
                $telefoneEmergencia     = $cliente->telefone_emergencia;
                $tipoDocumento          = $cliente->tipo_documento;
                $social_name            = $cliente->social_name;

                if ($phone)  {
                    $telefone       = $phone;
                }

                if ($whatsApp) {
                    $telefone   .= ' '.$whatsApp;
                }

                if ($telefoneEmergencia) {
                    $telefone .= '<br/>Emergência '.$telefoneEmergencia;
                }

                if ($social_name) {
                    $nomeCliente .= '<br/><small> Nome Social: ' . $social_name. '</small>';
                }

                if ($row->poltronaClient) $poltronaCliente = $row->poltronaClient;
                if ($data_aniversario) $data_aniversario = $this->sma->hrsd($data_aniversario);

                $background = "#eee";

                if ($isColorBackground) {
                    $background = "#eee";
                    $isColorBackground = false;
                } else {
                    $background = "#ffffff";
                    $isColorBackground = true;
                }
                ?>
                <?php if ($localDeEmbarque->id == $row->localEmbarque) {?>
                    <?php if ($locaEmbarque != $row->localEmbarque) {?>
                        <tr style="text-align: center;">
                            <td colspan="5" style="background: #ccc;font-size: 12px;">&nbsp;
                                <?php
                                $dados = $localDeEmbarque->name;
                                if ($localDeEmbarque->note) {
                                    $dados .= ' - '.$localDeEmbarque->note;
                                }

                                if ($localDeEmbarque->dataEmbarque != null && $localDeEmbarque->dataEmbarque != '0000-00-00'){
                                    $dados .= ' - <span style="font-weight: bold;">'.$this->sma->hrsd($localDeEmbarque->dataEmbarque).'</span>';
                                }

                                if ($localDeEmbarque->horaEmbarque != null && $localDeEmbarque->horaEmbarque != '00:00:00')  {
                                    $dados .= '  <span style="font-weight: bold;">'.date('H:i', strtotime($localDeEmbarque->horaEmbarque)).'h </span>';
                                }
                                ?>
                                <?php echo $dados?>
                            </td>
                        </tr>
                        <?php $locaEmbarque = $row->localEmbarque;?>
                    <?php }?>
                    <tr style="background: <?=$background;?>">
                        <?php if (!$row->descontarVaga) {
                            $totalCriancasColo++;
                            ?>
                            <td style="text-align: center;">(C) </td>
                        <?php } else {
                            $sContador = $contador < 10 ? '0'.$contador : $contador;
                            $totalPassageirosPagantes++;
                            ?>
                            <td style="text-align: center;"><?=$sContador;?></td>
                            <?php $contador++?>
                        <?php } ?>
                        <td align="center" style="font-weight: bold;font-size: 12px;">
                            <?php if ($poltronaCliente) {?>
                                <img src="<?php echo $assets;?>/images/assento.svg" style="height: 15px;width: 15px;" ><?php echo $poltronaCliente;?>
                            <?php } ?>
                        </td>
                        <td align="left">
                            <span style="width: 40%;font-weight: bold;font-size: 12px">
                                <?=$nomeCliente;?>
                            </span>
                            <?php if ($row->note_item){?>
                                <br/><small><?=strip_tags(html_entity_decode($row->note_item, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));?></small>
                            <?php }?>
                        </td>
                        <td align="left"><?php echo $telefone;?></td>
                        <?php if ($Settings->show_payment_report_shipment) {?>
                            <td style="text-align: right;"><?php echo $totalAbertoPorDependente;?></td>
                        <?php } ?>
                    </tr>
                <?php } ?>
            <?php } ?>
        <?php }?>
        </tbody>

        <tfoot>

        <tr>
            <td colspan="4">&nbsp;</td>
        </tr>
        <tr style="background: #eee;">
            <td colspan="5" style="border-top: 1px solid #0b0b0b;">
                TOTAL DE PASSAGEIROS PAGANTES: <?=$totalPassageirosPagantes;?>
            </td>
        </tr>

        <tr style="background: #eee;">
            <td colspan="5">
                TOTAL NÃO PAGANTES: <?=$totalCriancasColo;?>
            </td>
        </tr>
        </tfoot>
    </table>
</body>
</html>