<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
		body {
			font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 12px;
		}
	</style>
<body>
	<table border="" style="width: 100%;border-collapse:collapse;">
 		<thead>
 			<tr>
 				<th colspan="3" align="center"> 
 					<?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
                    <?php
                    $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                    $nomeViagem = strtoupper($product->name).' '.$data;
                    ?>
                    <h5>Lista de Embarque <br/> <?php echo $nomeViagem; ?></h5> <br/>
 				</th>
 			</tr>
 		</thead>
 	</table>
	<table border="1" style="width: 100%;border-collapse:collapse;">
        <thead>
            <tr>
                <th colspan="4" align="center">
                    <h3>Aqui vai o nome do tip de transport</h3>
                </th>
            </tr>
        </thead>

        <thead>
            <tr>
                <th align="center" width="10%">Poltrona</th>
                <th align="center" width="40%">Passageiro</th>
                <th align="center" width="30%">Telefone</th>
                <th align="center" width="20%">Local sa&iacute;da</th>
            </tr>
        </thead>

        <tbody>
            <?php
            $totalPoltronas = $tipoTransporte->totalPoltronas;

            for($i=1;$i<=$totalPoltronas;$i++):

                $customer       = '';
                $telefone       = '';
                $str_contador   = '';
                $local_saida    = '';

                if ($i <= 9) $str_contador = '0'.$i;
                else $str_contador = $i;

                $ocupado = false;

                if ($ocupado) {

                    $local_saida    = '';
                    $customer       = $ocupado->customer;
                    $phone          = $ocupado->phone;
                    $whatsApp       = $ocupado->cf5;
                    $telefone_emergencia    = $ocupado->telefone_emergencia;

                    if ($phone)  $telefone  = $phone;
                    if ($whatsApp)  $telefone  = $telefone.' '.$whatsApp;
                    if ($telefone_emergencia) $telefone = $telefone.'<br/>Emergência '.$telefone_emergencia;

                    if ($ocupado->local_saida) $local_saida = $ocupado->local_saida;
                    else  $local_saida = $local_saida_descricao;

                } else {

                    $ocupado        = $this->site->getSaleByItensByPoltrona($product_id, $str_contador, $onibus->id);
                    $criancaDecolo  = $this->site->getSaleByItensByPoltrona($product_id, $str_contador.'C', $onibus->id);

                    $local_saida    = '';
                    $customer       = $ocupado->customerClientName;
                    $phone          = $ocupado->phone;
                    $whatsApp       = $ocupado->cf5;
                    $telefone_emergencia    = $ocupado->telefone_emergencia;

                    if ($phone)  $telefone  = $phone;
                    if ($whatsApp)  $telefone  = $telefone.' '.$whatsApp;
                    if ($telefone_emergencia) $telefone = $telefone.'<br/>Emergência '.$telefone_emergencia;

                    if ($ocupado->local_saida) $local_saida = $ocupado->local_saida;
                    else  $local_saida = $local_saida_descricao;

                    if ($customer == '0' || $customer == 'undefined') $customer = $ocupado->customer;

                }?>
                <tr>
                    <td align="center"><?php echo $str_contador;?></td>
                    <td align="center"><?php echo $customer;?></td>
                    <td align="center"><?php echo $telefone;?></td>
                    <td align="center"><?php echo $local_saida;?></td>
                </tr>

                <?php if ($criancaDecolo) {?>
                    <tr>
                        <td align="center"><?php echo $str_contador.'C';?></td>
                        <td align="center"><?php echo $criancaDecolo->customerClientName;?></td>
                        <td align="center"><?php echo $telefone;?></td>
                        <td align="center"><?php echo $local_saida;?></td>
                    </tr>
                <?php } ?>
            <?php endfor;?>
 		</tbody>
	</table>
</body>
</html>
