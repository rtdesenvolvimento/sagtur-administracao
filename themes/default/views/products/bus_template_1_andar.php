<style>
    #current-trips {
         margin: 45px auto;
         width: 0px;
        float: left;
    }
</style>

<div class="available-trips" id="backing" style="margin-left: 25%;">
  <div id="onibus-volta-frente"></div>
  <div id="onibus-volta-bg">
	  <center>
		<table class="cpo1" border="">
		   <Tbody>
			<?php

			$contador = 1;
			$total_poltronas = $tipoTransporte->totalPoltronas;
            $filas = $total_poltronas/4;

            $ocupados = $this->products_model->getSaleByPoltronaByItemVendaForMarcacao($tipoTransporte->id, $programacaoId);

            //print_r($ocupados);

			for($i=0;$i<$filas;$i++) {
			    $class = 'libre';
                ?>
				<tr>
					<?php
						$str_contador = '';

						if ($contador <= 9) $str_contador = '0'.$contador;
						else $str_contador = $contador;
                        $ocupado = false;

                        foreach ($ocupados as $ocup) {

                            //echo '<br/>'.$ocup->poltrona.' =  '.$str_contador;
                            if ($ocup->poltrona == $str_contador) {
                                $ocupado =   $ocup;
                            }
                        }

                        if ($ocupado)  $class = 'ocupada';
                        else $class = 'libre';

						if ($contador > $total_poltronas) $class = 'vacia';

					?>
					<td class="<?php echo $class?>" variacao="<?php echo $tipoTransporte->id ?>" poltrona="<?php echo $str_contador ?>" customer="<?php echo $ocupado->customer ?>" venda="<?php echo $ocupado->id ?>" onclick="mostra_venda(this)"><?php echo $str_contador ?></td>
					
					<?php
						$str_contador = '';
						$contador = $contador + 1;
                        $ocupado = false;

                        if ($contador < 9) $str_contador = '0'.$contador;
						else $str_contador = $contador;

                        foreach ($ocupados as $ocup) {
                            if ($ocup->poltrona == $str_contador) {
                                $ocupado =   $ocup;
                            }
                        }

                        if ($ocupado) $class = 'ocupada';
                        else $class = 'libre';

                        if ($contador > $total_poltronas) {
							$class = 'vacia';
						}
					?>
					<td class="<?php echo $class?>" variacao="<?php echo $tipoTransporte->id ?>" poltrona="<?php echo $str_contador ?>" customer="<?php echo $ocupado->customer ?>" venda="<?php echo $ocupado->id ?>" onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
					<td>&nbsp;&nbsp;</td>
					<td class="vacia" id="ida_" onclick="mostra_venda(this)"></td>
					<td>&nbsp;&nbsp;</td>
					
					<?php
						$str_contador = '';
						$contador = $contador + 2;
                        $ocupado = false;

						if ($contador < 9) $str_contador = '0'.$contador;
						else $str_contador = $contador;

                        foreach ($ocupados as $ocup) {
                            if ($ocup->poltrona == $str_contador) {
                                $ocupado =   $ocup;
                            }
                        }

                        if ($ocupado) $class = 'ocupada';
                        else $class = 'libre';

						
						if ($contador > $total_poltronas) $class = 'vacia';
					?>
					<td class="<?php echo $class?>" variacao="<?php echo $tipoTransporte->id ?>" poltrona="<?php echo $str_contador ?>" customer="<?php echo $ocupado->customer ?>" venda="<?php echo $ocupado->id ?>" onclick="mostra_venda(this)"><?php echo $str_contador; ?></td>
					
					<?php
						$str_contador = '';
						$contador = $contador - 1;
                        $ocupado = false;

                        if ($contador < 9) {
							$str_contador = '0'.$contador;
						} else {
							$str_contador = $contador;
						}

                        foreach ($ocupados as $ocup) {
                            if ($ocup->poltrona == $str_contador) {
                                $ocupado =   $ocup;
                            }
                        }

                        if ($ocupado) {
                            $class = 'ocupada';
                        } else {
                            $class = 'libre';
                        }
						
						if ($contador > $total_poltronas) {
							$class = 'vacia';
						}
					?>
					<td class="<?php echo $class?>" variacao="<?php echo $tipoTransporte->id ?>" poltrona="<?php echo $str_contador ?>" customer="<?php echo $ocupado->customer ?>" venda="<?php echo $ocupado->id ?>" onclick="mostra_venda(this)"><?php echo $str_contador;?></td>
				</tr>
			    <?php $contador = $contador + 2;?>
                <?php } ?>
			   </Tbody>
			</table>
		</center>
	  </div>
	  <div id="onibus-volta-traseira"></div>
   </div>