<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_group'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("messagegroups/editGroup/".$group->id, $attrib); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("status", "status") ?>
                                <?php
                                $opts = array(
                                    '1' => lang('ativo'),
                                    '0' => lang('inativo')
                                );
                                echo form_dropdown('active', $opts, $group->active , 'class="form-control" id="active" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("product", "product") ?>
                                <?= form_input('name', $group->name, 'class="form-control" id="name" disabled'); ?>
                            </div>
                        </div>

                        <div class="col-md-12" id="div_remover_todos" style="display: none;">
                            <div class="form-group all">
                                <?php echo form_checkbox('remover_todos', '1', false, 'id="remover_todos"'); ?>
                                <label for="attributes" class="padding05"><?= lang('remover_todos'); ?></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo form_submit('edit_group', lang('edit_group'), 'id="edit_group" class="btn btn-primary"'); ?>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>


<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<?= $modal_js ?>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $('#active').change(function (event){
           if ($(this).val() == 1) {
               $('#div_remover_todos').hide();
           } else {
               $('#div_remover_todos').show();
           }
        });
    });
</script>
