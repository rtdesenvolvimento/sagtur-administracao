<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_admin'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("messagegroups/addAdmin/".$group->id, $attrib); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("name", "name") ?>
                            <?= form_input('name', $name, 'class="form-control" id="name" required="required"'); ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("phone_admin", "phone_admin") ?>
                            <?= form_input('phone_admin', $phone_admin, 'class="form-control" id="phone_admin" required="required"'); ?>
                            <smal>Será promovido a administrador do grupo após ser criado</smal><br/>
                            <smal>Desabilite a configuração de bloqueio para ser inserido no grupo</smal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo form_submit('add_admin', lang('add_admin'), 'id="add_admin" class="btn btn-primary"'); ?>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<?= $modal_js ?>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {});
</script>
