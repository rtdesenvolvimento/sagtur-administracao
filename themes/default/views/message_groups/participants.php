<style>
    .imgs {
        width: 90%;
    }

    .highlight {
        background-color: #ffeb3b36 !important;
    }
</style>

<?php
function row_status($x)
{
    if($x == null) {
        return '';
    } else if($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    } else if($x == 'partial' || $x == 'transferring' || $x == 'ordered') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if($x == 'due' || $x == 'returned') {
        return '<div class="text-center"><span class="label label-danger">'.lang($x).'</span></div>';
    } else if ($x == 'cancel') {
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'reembolso'){
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'orcamento') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if ($x == 'lista_espera') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if ($x == 'faturada'){
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    }
}
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('message_group_participants'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('messagegroups/addAdmin/'.$group->id); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-phone"></i> <?= lang('add_admin') ?>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('messagegroups/addAllParticipants/'.$group->id); ?>">
                                <i class="fa fa-whatsapp"></i> <?= lang('add_all_participants') ?>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown" style="display: none;">
                    <a href="<?= site_url('googlecontacts/export_peoples') ?>" class="toggle_up">
                        <i class="icon fa fa-google"></i><span class="padding-right-10"><?= lang('exportar_para_agenda'); ?></span>
                    </a>
                </li>

            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-6">
                <p class="introtext"><i class="fa fa-google"></i> Este ícone indica se o contato está ou não importado no Agenda Padrão SAGTur.</p>
            </div>
            <div class="col-lg-6">
                <p class="introtext"><i class="fa fa-phone"></i> Este ícone india se o contato está no WhatsApp e já pode ser adicionado ao grupo.</p>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?= lang("name", "name") ?>
                    <?php echo form_input('name', $group->name , 'class="form-control" id="name" disabled'); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?= lang("invitationLink", "invitationLink") ?>
                    <?php echo form_input('invitationLink', $group->invitationLink , 'class="form-control" id="invitationLink" disabled'); ?>
                </div>
            </div>

            <div class="col-md-12">
                <div class="form-group all">
                    <?php echo form_checkbox('mostrar_vendas_canceladas', '1', $mostrar_vendas_canceladas, 'id="mostrar_vendas_canceladas"'); ?>
                    <label for="attributes" class="padding05"><?= lang('mostrar_vendas_canceladas'); ?></label>
                </div>
            </div>

            <?php if (!empty($admins)) { ?>
                <div class="col-lg-12">
                    <h3>Administradores</h3>
                    <small>Se você não foi incluído como administrador do grupo, entre em contato com a Sagtur para alterar seu perfil no grupo para administrador</small>
                    <div class="table-responsive">
                        <table id="tbFinanceiroGeralNivel2" style="cursor: pointer;" class="table table-bordered table-hover table-striped table-condensed reports-table">
                            <thead>
                            <tr>
                                <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                                <th style="text-align: left;"><?= lang("admin"); ?></th>
                                <th style="text-align: center;"><?= lang("phone"); ?></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-google"></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-phone"></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-whatsapp"></th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php
                                $contadoColunasAdmin = 1;
                                $allContactsWhatsApp = $this->SendMessageService_model->get_contacts();

                                foreach ($admins as $admin) {

                                    $phone = $admin->phone;

                                    $phone = str_replace('(', '', str_replace(')', '', $phone));
                                    $phone = str_replace('-', '', $phone);
                                    $phone = str_replace(' ', '', $phone);
                                    $phone = trim($phone);

                                    $participant        = $this->MessageGroupRepository_model->getParticipant($group->id, $phone);
                                    $isContactWhatsApp  = $phone != '' && $this->SendMessageService_model->exist_contact($phone, $allContactsWhatsApp);

                                    if (!$isContactWhatsApp) {
                                        $personImported = $phone != '' &&  $this->GooglePersonRepository_model->personImported($phone);
                                    } else {
                                        $personImported = TRUE;
                                    }

                                    ?>
                                    <tr>
                                        <td style="text-align: center;"><?php echo $contadoColunasAdmin;?></td>
                                        <td style="text-align: center;"><?=$admin->contact;?></td>
                                        <td style="text-align: center;"><?=$admin->phone;?></td>
                                        <td class="acoes" style="text-align: center;">
                                            <?php if($personImported) { ?>
                                                <span class="label label-success">SIM</span>
                                            <?php } else { ?>
                                                <span class="label label-danger">NÃO</span>
                                            <?php } ?>
                                        </td>
                                        <td class="acoes" style="text-align: center;">
                                            <?php if($isContactWhatsApp) { ?>
                                                <span class="label label-success">SIM</span>
                                            <?php } else { ?>
                                                <span class="label label-danger">NÃO</span>
                                            <?php } ?>
                                        </td>

                                        <?php if($personImported && $isContactWhatsApp) { ?>
                                            <td class="acoes" style="text-align: center;">
                                                <?php if ($participant) { ?>
                                                    <?php if (!$participant->invitation){?>
                                                        <a href="javascript:void(0);"
                                                           title="<?= lang('delete_participant'); ?>"
                                                           onclick="removeParticipant('<?= site_url('messagegroups/removeParticipantGroup/' . $participant->id); ?>')">
                                                            <img src="<?=base_url('assets/images/ico32_spwSucesso.png');?>" class="imgs">
                                                        </a>
                                                    <?php } else { ?>
                                                        <a href="javascript:void(0);"
                                                           title="<?= lang('delete_participant'); ?>"
                                                           onclick="enviarConvite('<?= site_url('messagegroups/enviar_convite/' . $participant->id); ?>')">
                                                            <img src="<?=base_url('assets/images/ico32_spwAlerta.png');?>" class="imgs" title="Membro tem bloqueio para ser inserido em grupos, mandar o convite usando o link do Grupo!">
                                                        </a>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);"
                                                       title="<?= lang('add_participant'); ?>"
                                                       onclick="addMemberGroup('<?= site_url('messagegroups/addParticipantAdminGroup/' . $group->id . '/'. $phone); ?>')">
                                                        <img src="<?=base_url('assets/images/ico32_spwErro.png');?>" class="imgs">
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        <?php } else { ?>
                                            <td class="acoes" style="text-align: center;">
                                                -
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php $contadoColunasAdmin++;} ?>
                            </tbody>
                            <tfoot class="dtFilter">
                            <tr>
                                <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                                <th style="text-align: left;"><?= lang("admin"); ?></th>
                                <th style="text-align: center;"><?= lang("phone"); ?></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-google"></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-phone"></th>
                                <th style="text-align: center;width: 2%"><i class="fa fa-whatsapp"></th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            <?php } ?>

            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="tbFinanceiroGeralNivel2" style="cursor: pointer;" class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                            <th style="text-align: center;width: 1%;"><?= lang("sale"); ?></th>
                            <th style="text-align: left;"><?= lang("customer"); ?></th>
                            <th style="text-align: center;"><?= lang("phone"); ?></th>
                            <th style="text-align: center;"><?= lang("payment_status"); ?></th>
                            <th style="text-align: center;"><?= lang("total"); ?></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-google"></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-phone"></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-whatsapp"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($sales)) { ?>
                            <?php
                            $contadoColunas = 0;
                            $totalValor = 0;
                            $telefonesVistos = [];

                            foreach ($sales as $sale) {

                                $customer = $this->site->getCompanyByID($sale->customerClient);

                                $contadoColunas++;

                                list($ano, $mes, $dia) = explode('-', $customer->data_aniversario);

                                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
                                $subTotal += $sale->subtotal;

                                $ddi = '55';

                                $phone = $customer->cf5 != '' ? $customer->cf5 : $customer->phone;

                                $phone = str_replace('(', '', str_replace(')', '', $phone));
                                $phone = str_replace('-', '', $phone);
                                $phone = str_replace(' ', '', $phone);
                                $phone = trim($phone);

                                $participant = $this->MessageGroupRepository_model->getParticipant($group->id, $ddi.$phone);
                                $isContactWhatsApp = $phone != '' && $this->SendMessageService_model->exist_contact($ddi.$phone, $allContactsWhatsApp);

                                if (!$isContactWhatsApp) {
                                    $personImported = $phone != '' &&  $this->GooglePersonRepository_model->personImported($ddi . $phone);
                                } else {
                                    $personImported = TRUE;
                                }

                                if (strlen($phone) == 11) {
                                    // Verifica se o telefone já foi visto
                                    $highlightRow = in_array($phone, $telefonesVistos);

                                    // Adiciona o telefone ao array de vistos
                                    $telefonesVistos[] = $phone;
                                }
                                ?>

                                <tr class="invoice_link <?= $highlightRow ? 'highlight' : '' ?>" id="<?php echo $sale->id;?>">
                                    <td style="text-align: center;" class="<?= $highlightRow ? 'highlight' : '' ?>" ><?php echo $contadoColunas;?></td>
                                    <td style="text-align: center;"  class="<?= $highlightRow ? 'highlight' : '' ?>"><?php echo $sale->reference_no;?></td>
                                    <td style="text-align: left;"  class="<?= $highlightRow ? 'highlight' : '' ?>"><?php echo $customer->name.'<small> idade '.$idade.' ['.lang($sale->faixaNome).']'.' '. (int) $sale->quantity.'un</small>';?></td>
                                    <td style="text-align: center;"  class="<?= $highlightRow ? 'highlight' : '' ?>">
                                        <?php if(strlen($phone) == 11){?>
                                            <a href="https://api.whatsapp.com/send?phone=<?=trim($ddi.$phone)?>" target="_blank">
                                                <?php if ($highlightRow) {?>
                                                    <?=($customer->cf5 != '' ? $customer->cf5 : $customer->phone); ?> <i class="fa fa-hand-o-up"></i>
                                                <?php } else { ?>
                                                    <?=($customer->cf5 != '' ? $customer->cf5 : $customer->phone); ?>
                                                <?php } ?>
                                            </a>
                                        <?php } else { ?>
                                            <?=($customer->cf5 != '' ? $customer->cf5 : $customer->phone); ?> (inválido)
                                        <?php } ?>
                                    </td>
                                    <td style="text-align: center;"  class="<?= $highlightRow ? 'highlight' : '' ?>"><?php echo row_status($sale->payment_status) ;?></td>
                                    <td style="text-align: right;"  class="<?= $highlightRow ? 'highlight' : '' ?>"><?php echo $this->sma->formatMoney($sale->subtotal);?></td>
                                    <td class="acoes <?= $highlightRow ? 'highlight' : '' ?>" style="text-align: center;">
                                        <?php if($personImported) { ?>
                                            <span class="label label-success">SIM</span>
                                        <?php } else { ?>
                                            <span class="label label-danger">NÃO</span>
                                        <?php } ?>
                                    </td>
                                    <td class="acoes <?= $highlightRow ? 'highlight' : '' ?>" style="text-align: center;">
                                        <?php if($isContactWhatsApp) { ?>
                                            <span class="label label-success">SIM</span>
                                        <?php } else { ?>
                                            <span class="label label-danger">NÃO</span>
                                        <?php } ?>
                                    </td>

                                    <?php if($personImported && $isContactWhatsApp) { ?>
                                        <td class="acoes <?= $highlightRow ? 'highlight' : '' ?>" style="text-align: center;">
                                            <?php if ($participant) { ?>
                                                <?php if (!$participant->invitation){?>
                                                    <a href="javascript:void(0);"
                                                       title="<?= lang('delete_participant'); ?>"
                                                       onclick="removeParticipant('<?= site_url('messagegroups/removeParticipantGroup/' . $participant->id); ?>')">
                                                        <img src="<?=base_url('assets/images/ico32_spwSucesso.png');?>" class="imgs">
                                                    </a>
                                                <?php } else { ?>
                                                    <a href="javascript:void(0);"
                                                       title="<?= lang('delete_participant'); ?>"
                                                       onclick="enviarConvite('<?= site_url('messagegroups/enviar_convite/' . $participant->id); ?>')">
                                                        <img src="<?=base_url('assets/images/ico32_spwAlerta.png');?>" class="imgs" title="Membro tem bloqueio para ser inserido em grupos, mandar o convite usando o link do Grupo!">
                                                    </a>
                                                <?php } ?>
                                            <?php } else { ?>
                                                <a href="javascript:void(0);"
                                                   title="<?= lang('add_participant'); ?>"
                                                   onclick="addMemberGroup('<?= site_url('messagegroups/addParticipantGroup/' . $group->id . '/'. $ddi .'/'. $phone); ?>')">
                                                    <img src="<?=base_url('assets/images/ico32_spwErro.png');?>" class="imgs">
                                                </a>
                                            <?php } ?>
                                        </td>
                                    <?php } else { ?>
                                        <td class="acoes <?= $highlightRow ? 'highlight' : '' ?>" style="text-align: center;">
                                            -
                                        </td>
                                    <?php } ?>

                                </tr>
                            <?php } ?>
                        <?php } else {?>
                            <tr>
                                <td colspan="9" class="text-center">
                                    <div class="alert alert-info">
                                        <h4><?= lang('no_data') ?></h4>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr>
                            <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                            <th style="text-align: center;width: 1%;"><?= lang("sale"); ?></th>
                            <th style="text-align: left;"><?= lang("customer"); ?></th>
                            <th style="text-align: center;"><?= lang("phone"); ?></th>
                            <th style="text-align: center;width: 5%"><?= lang("payment_status"); ?></th>
                            <th style="text-align: center;"><?= lang("total"); ?></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-google"></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-phone"></th>
                            <th style="text-align: center;width: 2%"><i class="fa fa-whatsapp"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function () {
        $('#mostrar_vendas_canceladas').on('ifChecked', function (e) {
            window.location.href = '<?= site_url('messagegroups/participants/'.$group->id.'/1'); ?>';
        });

        $('#mostrar_vendas_canceladas').on('ifUnchecked', function (e) {
            window.location.href = '<?= site_url('messagegroups/participants/'.$group->id); ?>';
        });

    });

    function addMemberGroup(url) {
        if (confirm('Tem certeza de que deseja adicionar este menbro ao grupo?')) {
            // Faz a requisição AJAX
            fetch(url)
                .then(response => {
                    if (response.ok) {
                        alert('Membro adicionado com sucesso!');
                        // Recarrega a página após sucesso
                        location.reload();
                    } else {
                        alert('Ocorreu um erro ao adicionar o menbro.');
                    }
                })
                .catch(error => {
                    console.error('Erro na requisição:', error);
                    alert('Erro inesperado. Tente novamente.');
                });
        }
    }

    function removeParticipant(url) {
        if (confirm('Tem certeza de que deseja remover este menbro?')) {
            fetch(url)
                .then(response => {
                    if (response.ok) {
                        alert('Membro removido com sucesso!');
                        location.reload(); // Recarrega a página após sucesso
                    } else {
                        alert('Ocorreu um erro ao remover o menbro.');
                    }
                })
                .catch(error => {
                    console.error('Erro na requisição:', error);
                    alert('Erro inesperado. Tente novamente.');
                });
        }
    }

    function enviarConvite(url) {
        if (confirm('Tem certeza de que deseja enviar convite para este menbro?')) {
            fetch(url)
                .then(response => {
                    if (response.ok) {
                        alert('Convite enviado com sucesso!');
                        location.reload(); // Recarrega a página após sucesso
                    } else {
                        alert('Ocorreu um erro ao enviar convite o menbro.');
                    }
                })
                .catch(error => {
                    console.error('Erro na requisição:', error);
                    alert('Erro inesperado. Tente novamente.');
                });
        }
    }
</script>