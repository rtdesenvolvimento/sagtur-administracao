<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_member'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("messagegroups/addMember/".$group->id, $attrib); ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("phone", "phone") ?>
                            <?= form_input('phone', '55', 'class="form-control" id="phone" required="required"', 'number'); ?>
                            <smal>Exemplo:5548999999999</smal>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo form_submit('add_member', lang('add_member'), 'id="add_admin" class="btn btn-primary"'); ?>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>

<?= $modal_js ?>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {});
</script>
