<style type="text/css" media="screen">
    #MGTable td:nth-child(1) {display: none;}
    #MGTable td:nth-child(2) {}
    #MGTable td:nth-child(3) {width: 5%;text-align: right;}
    #MGTable td:nth-child(4) {width: 15%;text-align: right;}
    #MGTable td:nth-child(5) {width: 15%;text-align: right;}
    #MGTable td:nth-child(6) {width: 5%;text-align: right;}
    #MGTable td:nth-child(7) {width: 15%;text-align: right;}
    #MGTable td:nth-child(8) {width: 5%;}

    .bg-danger {
        background-color: #d9534f;
    }

    .bg-success {
        background-color: #5cb85c;
    }
</style>

<script>

    $(document).ready(function () {

        $('#status').change(function(event){
            $('#MGTable').DataTable().fnClearTable()
        });

        $('#MGTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('messagegroups/getGroups') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {},
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "status", "value":  $('#status').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                {
                    "mRender": function (data, type, row) {

                        let total_customer = row[2];
                        let imported_google = row[3] ;
                        let percentage = (imported_google / total_customer) * 100;
                        let progressClass = percentage < 100 ? 'bg-danger' : 'bg-success'; // Definir a cor da barra

                        return `
                            <div class="progress">
                                <div class="progress-bar ${progressClass}" role="progressbar" style="width: ${percentage}%" aria-valuenow="${percentage}" aria-valuemin="0" aria-valuemax="100">
                                    ${Math.round(percentage)}%
                                </div>
                            </div>
                        `;
                    }
                },
                {
                    "mRender": function (data, type, row) {

                        let total_customer = row[2];
                        let imported_whatsapp = row[4] ;
                        let percentage = (imported_whatsapp / total_customer) * 100;
                        let progressClass = percentage < 100 ? 'bg-danger' : 'bg-success'; // Definir a cor da barra

                        return `
                            <div class="progress">
                                <div class="progress-bar ${progressClass}" role="progressbar" style="width: ${percentage}%" aria-valuenow="${percentage}" aria-valuemin="0" aria-valuemax="100">
                                    ${Math.round(percentage)}%
                                </div>
                            </div>
                        `;
                    }
                },
                null,
                {
                    "mRender": function (data, type, row) {

                        let total_customer = row[2];
                        let total_customers_whatsapp = row[5] ;
                        let percentage = (total_customers_whatsapp / total_customer) * 100;
                        let progressClass = percentage < 100 ? 'bg-danger' : 'bg-success'; // Definir a cor da barra

                        return `
                                <div class="progress">
                                    <div class="progress-bar ${progressClass}" role="progressbar" style="width: ${percentage}%" aria-valuenow="${percentage}" aria-valuemin="0" aria-valuemax="100">
                                        ${Math.round(percentage)}%
                                    </div>
                                </div>
                            `;
                    }
                },
                {"bSortable": false}]
        });
    });
</script>
<?= form_open('messagegroups/expense_message_groups_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-comments"></i><?= lang('message_groups'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('messagegroups/addGroup'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_new_group') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-2">
                <?= lang("status", "status") ?>
                <?php
                $opts = array(
                    '1' => lang('ativo'),
                    '0' => lang('inativo')
                );
                echo form_dropdown('type', $opts,  'status', 'class="form-control" id="status"');
                ?>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="MGTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("name"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("customers"); ?></th>
                            <th style="text-align: left;"><i class="fa fa-google"></i></th>
                            <th style="text-align: left;"><i class="fa fa-phone"></i></th>
                            <th style="text-align: left;"><?= $this->lang->line("inserteds"); ?></th>
                            <th style="text-align: left;"><i class="fa fa-whatsapp"></i></th>
                            <th style="width:100px;"><?= $this->lang->line("participants"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="3" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("name"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("customers"); ?></th>
                            <th style="text-align: left;"><i class="fa fa-google"></i></th>
                            <th style="text-align: left;"><i class="fa fa-phone"></i></th>
                            <th style="text-align: left;"><?= $this->lang->line("inserteds"); ?></th>
                            <th style="text-align: left;"><i class="fa fa-whatsapp"></i></th>
                            <th style="width:100px;text-align: center;"><?= $this->lang->line("participants"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {});
</script>

