<style type="text/css" media="screen">

    #PRData td:nth-child(1) {display: none;}
    #PRData td:nth-child(3) {width: 60%;}
    #PRData td:nth-child(4) {text-align: center;}
    #PRData td:nth-child(5) {text-align: center;}
    #PRData td:nth-child(6) {text-align: center;}
    #PRData td:nth-child(7) {text-align: center;}
</style>

<script>

    var oTable;

    $(document).ready(function () {

        $('#ano').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        $('#mes').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        $('#status').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        $('#produto').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        $('#start_date').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#PRData').DataTable().fnClearTable()
        });

        oTable = $('#PRData').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": -1,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('checkins/getProgramacoes') ?>' ,
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filter_ano", "value":  $('#ano').val() });
                aoData.push({ "name": "filter_mes", "value":  $('#mes').val() });
                aoData.push({ "name": "filter_status", "value":  $('#status').val() });
                aoData.push({ "name": "filter_produto", "value":  $('#produto').val() });
                aoData.push({ "name": "filter_start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "filter_end_date", "value":  $('#end_date').val() });
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.setAttribute('programacaoid', aData[0]);
                nRow.className = "checkin_open_link";
                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"bSortable": false, "mRender": img_hl},
                null,
                {"mRender": fld},
                null,
                null,
                null,
            ],
        }).fnSetFilteringDelay().dtFilter([], "footer");
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-qrcode"></i><?= lang('checkin'); ?>
        </h2>
    </div>
    <div class="box-content">
        <fieldset class="scheduler-border">
            <legend class="scheduler-border filters">Filtros</legend>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('produto_ativo'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('status', $opts,  $statusFilter, 'class="form-control" id="status"'); ?>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="control-label" for="ano"><?= lang("ano"); ?></label>
                        <select class="form-control" id="ano"">
                        <option value="2020" <?php if($anoFilter == '2020') echo ' selected="selected"';?>>2020</option>
                        <option value="2021" <?php if($anoFilter == '2021') echo ' selected="selected"';?>>2021</option>
                        <option value="2022" <?php if($anoFilter == '2022') echo ' selected="selected"';?>>2022</option>
                        <option value="2023" <?php if($anoFilter == '2023') echo ' selected="selected"';?>>2023</option>
                        <option value="2024" <?php if($anoFilter == '2024') echo ' selected="selected"';?>>2024</option>
                        <option value="2025" <?php if($anoFilter == '2025') echo ' selected="selected"';?>>2025</option>
                        <option value="2026" <?php if($anoFilter == '2026') echo ' selected="selected"';?>>2026</option>
                        <option value="2027" <?php if($anoFilter == '2027') echo ' selected="selected"';?>>2027</option>
                        <option value="2028" <?php if($anoFilter == '2028') echo ' selected="selected"';?>>2028</option>
                        <option value="2029" <?php if($anoFilter == '2029') echo ' selected="selected"';?>>2029</option>
                        <option value="2030" <?php if($anoFilter == '2030') echo ' selected="selected"';?>>2030</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group">
                        <label class="control-label" for="mes"><?= lang("mes"); ?></label>
                        <select class="form-control" id="mes">
                            <option value="all" <?php if($mesFilter == 'all') echo ' selected="selected"';?>>Ano Todo</option>
                            <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                            <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                            <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                            <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                            <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                            <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                            <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                            <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                            <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                            <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                            <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                            <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                            <option value="PROXIMAS" <?php if($mesFilter == 'PROXIMAS') echo ' selected="selected"';?>>Próximas Viagens do Ano</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3">
                    <?= lang("product", "product") ?>
                    <?php
                    $prod[''] =  lang("select") . " " . lang("product") ;
                    foreach ($products as $produts) {
                        $prod[$produts->id] = $produts->name;
                    }
                    echo form_dropdown('produto', $prod, (isset($_POST['produto']) ? $_POST['produto'] : ($produto ? $produto->id : '')), 'class="form-control select" id="produto" placeholder="' . lang("select") . " " . lang("product") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("data_saida", "start_date"); ?>
                        <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date_filter), 'class="form-control" id="start_date"', 'date'); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("end_date", "end_date"); ?>
                        <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date_filter), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                    </div>
                </div>
            </div>
        </fieldset>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="PRData" class="table table-bordered table-condensed table-hover table-striped" style="cursor:pointer;">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th style="text-align: left;"><?= lang("product_name") ?></th>
                            <th style="text-align: center;"><?= lang("data_saida") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("vendas") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("confirmados") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("pendentes") ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="display: none;"></th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th style="text-align: left;"><?= lang("product_name") ?></th>
                            <th style="text-align: center;"><?= lang("data_saida") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("vendas") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("confirmados") ?></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><?= lang("pendentes") ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('body').on('click', '.checkin_open_link td:not(:first-child, :nth-child(2), :last-child)', function() {
           window.location = '<?= site_url('checkins/entrance/') ?>' + $(this).parent('.checkin_open_link').attr('programacaoid')
        });
    });
</script>
