
<style type="text/css" media="screen">
    #EntranceData td:nth-child(1)  {display: none;}
    #EntranceData td:nth-child(2)  {width: 2%;text-align: center;}
    #EntranceData td:nth-child(3)  {}
    #EntranceData td:nth-child(4)  {width: 10%;text-align: center;}
</style>

<script>

    function row_situacao(x) {
        if(x == null) {
            return '<div class="text-center"><span class="label label-danger">Pendente</span></div>';
        } else {
            return '<div class="text-center"><span class="label label-success">Confirmado</span></div>';
        }
    }

    $(document).ready(function () {
        var cTable = $('#EntranceData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": -1,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('checkins/getEntrances/'.$programacaoId) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "checkin_details_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "tipo_transporte", "value":  $('#tipo_transporte').val() });
                aoData.push({ "name": "local_embarque", "value":  $('#local_embarque').val() });
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var pendente = 0;
                var confirmados = 0;
                var total_passageiros = 0;

                for (var i = 0; i < aaData.length; i++) {

                    confirmado = aaData[aiDisplay[i]][3];

                    if (confirmado !== null) {
                        confirmados++;
                    } else {
                        pendente++;
                    }

                    total_passageiros++;
                }

                $('#total_passageiros').html(total_passageiros);
                $('#confirmados').html(confirmados);
                $('#pendentes').html(pendente);

            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                {"mRender": row_situacao},
            ]
        });

        $('#myModal').on('hidden.bs.modal', function () {
            cTable.fnDraw( false );
        });


    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-qrcode"></i><?= lang('entrance'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-12" style="text-align: center;">
                <video id="video" width="300" height="300" autoplay></video>
                <canvas id="canvas" style="display:none;"></canvas>
            </div>
            <div class="col-sm-12" style="text-align: center;">
                <button id="startButton" class="btn btn-success"><i class="fa fa-play"></i> Start Câmera</button>
                <button id="invertButton" class="btn btn-primary" style="display:none;"><i class="fa fa-refresh"></i></button>
                <button id="stopButton" class="btn btn-warning"><i class="fa fa-stop"></i> Stop Câmera</button>
            </div>
        </div>
        <div class="row" style="margin-bottom: 15px;">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="small-box bblue col-sm-12" style="color: #ffffff;padding: 25px;">
                            <h4 class="bold" style="font-size: 17px;">TOTAL</h4>
                            <i class="fa fa-user"></i>
                            <div class="col-sm-12" style="font-size: 1.4rem;">
                                <div class="bold">
                                    <div style="float: left;">PASSAGEIROS</div>
                                    <div style="float: right;"><span id="total_passageiros"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="small-box bdarkGreen col-sm-12" style="color: #ffffff;padding: 25px;">
                            <h4 class="bold" style="font-size: 17px;">PASSAGEIROS</h4>
                            <i class="fa fa-check"></i>
                            <div class="col-sm-12" style="font-size: 1.4rem;">
                                <div class="bold">
                                    <div style="float: left;">CONFIRMADOS</div>
                                    <div style="float: right;"><span id="confirmados"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="small-box borange col-sm-12" style="color: #ffffff;padding: 25px;">
                            <h4 class="bold" style="font-size: 17px;">PASSAGEIROS</h4>
                            <i class="fa fa-warning"></i>
                            <div class="col-sm-12" style="font-size: 1.4rem;">
                                <div class="bold">
                                    <div style="float: left;">PENDENTES</div>
                                    <div style="float: right;"><span id="pendentes"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("local_embarque", "local_embarque"); ?>
                            <?php
                            $cbEmbarques[""] = lang("select") . ' ' . lang("local_embarque");
                            foreach ($embarques as $embarque) {
                                $cbEmbarques[$embarque->id] = $embarque->name;
                            }
                            echo form_dropdown('local_embarque', $cbEmbarques, '', 'id="local_embarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select_destroy" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("tipo_transporte", "tipo_transporte"); ?>
                            <?php
                            $cbTransportes[""] = lang("select") . ' ' . lang("tipo_transporte");
                            foreach ($transportes as $transporte) {
                                $cbTransportes[$transporte->id] = $transporte->name;
                            }
                            echo form_dropdown('tipo_transporte', $cbTransportes, '', 'id="tipo_transporte" data-placeholder="' . lang("select") . ' ' . lang("tipo_transporte") . '" class="form-control input-tip select_destroy" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="EntranceData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped" style="cursor: pointer;">
                        <thead>
                        <tr class="primary">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th style="text-align: center;"><?=lang("plr."); ?></th>
                            <th style="text-align: left;"><?=lang("name"); ?></th>
                            <th style="text-align: center;"><?=lang("situation"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="4" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?=lang("plr."); ?></th>
                            <th><?=lang("name"); ?></th>
                            <th style="text-align: center;"><?=lang("situation"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/jsqr"></script>
<script type="text/javascript" src="<?= $assets ?>js/jsqr/jsqr-0.2-min.js"></script>

<script type="text/javascript">

    const video     = document.getElementById('video');
    const canvas    = document.getElementById('canvas');
    const ctx       = canvas.getContext('2d');

    const startButton = document.getElementById('startButton');
    const stopButton = document.getElementById('stopButton');
    const invertButton = document.getElementById('invertButton');

    let stream;

    $(document).ready(function (e) {

        $('body').on('click', '.checkin_details_link td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'checkins/checkin/' + $(this).parent('.checkin_details_link').attr('id')});
            $('#myModal').modal('show');
        });

        // Event listener for start button
        startButton.addEventListener('click', startCamera);

        // Event listener for invert button
        invertButton.addEventListener('click', startCameraInvert);

        // Event listener for stop button
        stopButton.addEventListener('click', stopCamera);

        setTimeout(function (){
            $('.select_destroy').select2('destroy');
        }, 800);
    });

    $('#tipo_transporte').change(function(event){
        $('#EntranceData').DataTable().fnClearTable()
    });

    $('#local_embarque').change(function(event){
        $('#EntranceData').DataTable().fnClearTable()
    });

    async function startCamera() {
        try {

            stopCamera();

            video.style.width = document.width + 'px';
            video.style.height = document.height + 'px';
            video.setAttribute('autoplay', '');
            video.setAttribute('muted', '');
            video.setAttribute('playsinline', '');

            var constraints = {
                audio: false,
                video: {
                    facingMode: 'environment'
                }
            }

            navigator.mediaDevices.getUserMedia(constraints).then(function success(stream_l) {
                stream = stream_l;
                video.srcObject = stream;
                video.play();
                requestAnimationFrame(scanQR);
            });

            /*
            stream = await navigator.mediaDevices.getUserMedia({ video: { facingMode: "environment" } });

            video.srcObject = stream;
            video.play();
             */

        } catch (error) {
            console.error('Error accessing the camera: ', error);
            alert('Error accessing the camera: '+ error.message);
        }
    }

    async function startCameraInvert() {
        try {

            stopCamera();

            stream = await navigator.mediaDevices.getUserMedia({ video: true });

            video.srcObject = stream;
            video.play();

            requestAnimationFrame(scanQR);
        } catch (error) {
            console.error('Error accessing the camera: ', error);
            alert('Error accessing the camera: '+ error.message);
        }
    }

    function stopCamera() {
        if (stream) {
            const tracks = stream.getTracks();
            tracks.forEach(track => track.stop());
            video.srcObject = null;
        }
    }

    function scanQR() {
        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);
        const imageData = ctx.getImageData(0, 0, canvas.width, canvas.height);
        const code = jsQR(imageData.data, imageData.width, imageData.height);

        if (code) {
            console.log('QR Code detected URL:', code.data);
            registrar_entrada_via_qr_code(code.data)
        }

        requestAnimationFrame(scanQR);
    }

    function registrar_entrada_via_qr_code(url) {

        if (url.includes("/sales/view")) {
            var urlObj = new URL(url);
            var venda = urlObj.pathname.split('/').pop();

            $('#myModal').modal({remote: site.base_url + 'checkins/checkin_qrcode/' + venda + '/<?=$programacaoId;?>'});
            $('#myModal').modal('show');
        } else {
            console.log(false);
        }
    }

</script>
