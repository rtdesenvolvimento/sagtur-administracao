<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('info_checkin'); ?></h4>
        </div>
        <div class="modal-body" id="modal_html">
            <div class="row" style="text-align: center;">
                <?php if ($checked){?>
                    <div class="col-md-12">
                        <img src="<?= $assets ?>images/confirm.gif"  style="width: auto;" width="260px" height="260px" />
                    </div>
                <?php } ?>
                <div class="col-md-12">
                    <h2 style="line-height: 2.3rem;">
                        <span style="font-size: 2.5rem;">
                            <?=$customer->name;?>
                            <?php if ($customer->social_name){?>
                                <br/>
                                Nome social: <?=$customer->social_name;?>
                            <?php } ?>
                        </span>
                    </h2>
                    <h4>
                        <?php if ($customer->cf5){?>
                            <span style="margin-top: 5px;">
                                <br/>
                                <?php
                                $phone = $customer->cf5;
                                $phone = str_replace ( '(', '', str_replace ( ')', '', $phone));
                                $phone = str_replace ( '-', '',  $phone);
                                $phone = str_replace ( ' ', '',  $phone);
                                ?>
                                <a href="https://api.whatsapp.com/send?phone=55<?=$phone;?>&text=" target="_blank"><?=$customer->cf5;?></a>
                            </span>
                        <?php }?>
                        <?php if(!empty($itens) && count($itens) == 1){?>
                            <span style="line-height: 2.1rem;">
                                <?php if ($customer->telefone_emergencia){?>
                                    <br/>
                                <?php
                                    $phone_emergencia = $customer->telefone_emergencia;
                                    $phone_emergencia = str_replace ( '(', '', str_replace ( ')', '', $phone_emergencia));
                                    $phone_emergencia = str_replace ( '-', '',  $phone_emergencia);
                                    $phone_emergencia = str_replace ( ' ', '',  $phone_emergencia);
                                    ?>
                                Emergência: <a href="https://api.whatsapp.com/send?phone=55<?=$phone_emergencia;?>&text=" target="_blank"><?=$customer->telefone_emergencia;?></a>
                                <?php } ?>
                                <?php if ($customer->vat_no){?>
                                    <br/>
                                    CPF: <?=$customer->vat_no;?>
                                <?php } ?>
                                <?php if ($customer->cf1){?>
                                    <br/>
                                    <?=strtoupper($customer->tipo_documento);?>: <?=$customer->cf1;?> <?=$customer->cf3;?>
                                <?php } ?>
                                <?php
                                if ($customer->data_aniversario) {
                                    list($ano, $mes, $dia) = explode('-', $customer->data_aniversario);

                                    $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                    $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                                    $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25); ?>
                                    <br/>
                                    Nascimento: <?=$this->sma->hrsd($customer->data_aniversario) . ' ' . $idade . ' anos';?>
                                <?php } ?>
                                <?php if ($customer->doenca_informar){?>
                                    <br/>
                                    Inf Médicas: <?=$customer->doenca_informar;?>
                                <?php } ?>
                            </span>
                        <?php } ?>
                    </h4>
                    <?php if ($inv->reference_no){?>
                        <h4><?=$inv->reference_no;?></h4>
                    <?php } ?>
                    <?php if ($item->poltronaClient){?>
                        <h4>Assento <?=$item->poltronaClient?></h4>
                    <?php } ?>
                    <?php if ($checked){?>
                        <hr>
                        <h5 style="margin-bottom: 25px;">Check-in Realizado: <?=$this->sma->hrld($checked->date);?>  <i class="fa fa-trash-o deletar_checkin" itemid="<?=$item->id;?>" style="cursor: pointer;"></i> </h5>
                    <?php }?>
                </div>

                <?php if (!$checked){?>
                    <div class="col-md-12" style="margin-bottom: 25px;">
                        <?php echo form_button('fazer_checkin', lang("fazer_checkin"), 'item='.$item->id.' style="width: 100%;font-size: 2.5rem;" class="btn btn-success checkin"'); ?>
                    </div>
                <?php }?>
            </div>
            <?php if(!empty($itens) && count($itens) > 1){?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <table id="CheckinData" cellpadding="0" cellspacing="0" border="0"
                                   class="table table-bordered table-condensed table-hover table-striped">
                                <thead>
                                <tr>
                                    <th colspan="3">PASSAGEIROS NA VENDA</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($itens as $it) {
                                    $checked_item = $this->CheckinRepository_model->checked($it->sale_id, $it->customerClient);
                                    ?>
                                    <tr>
                                        <td style="width: 70%;">
                                            <b style="font-size: 1.5rem;"><?=$it->customer;?></b>
                                            <?php if ($it->poltronaClient){?>
                                                <br/>Assento <?=$it->poltronaClient?>
                                            <?php } ?>
                                            <?php if ($it->social_name){?>
                                                <br/>
                                                Nome social: <?=$it->social_name;?>
                                            <?php } ?>
                                            <?php if ($it->vat_no){?>
                                                <br/>
                                                CPF: <?=$it->vat_no;?>
                                            <?php } ?>
                                            <?php if ($it->cf1){?>
                                                <br/>
                                                <?=strtoupper($it->tipo_documento);?>: <?=$it->cf1;?> <?=$it->cf3;?>
                                            <?php } ?>
                                            <?php if ($it->cf5){?>
                                                <br/>
                                                <?php
                                                $phone_it = $customer->cf5;
                                                $phone_it = str_replace ( '(', '', str_replace ( ')', '', $phone_it));
                                                $phone_it = str_replace ( '-', '',  $phone_it);
                                                $phone_it = str_replace ( ' ', '',  $phone_it);
                                                ?>
                                                <a href="https://api.whatsapp.com/send?phone=55<?=$phone_it;?>&text=" target="_blank"><?=$it->cf5;?></a>
                                            <?php }?>
                                            <?php if ($it->telefone_emergencia){?>
                                                <br/>
                                                <?php
                                                $phone_emergencia = $it->telefone_emergencia;
                                                $phone_emergencia = str_replace ( '(', '', str_replace ( ')', '', $phone_emergencia));
                                                $phone_emergencia = str_replace ( '-', '',  $phone_emergencia);
                                                $phone_emergencia = str_replace ( ' ', '',  $phone_emergencia);
                                                ?>
                                                Emergência: <a href="https://api.whatsapp.com/send?phone=55<?=$phone_emergencia;?>&text=" target="_blank"><?=$it->telefone_emergencia;?></a>
                                            <?php } ?>
                                            <?php
                                            if ($it->data_aniversario) {
                                                list($ano, $mes, $dia) = explode('-', $it->data_aniversario);

                                                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                                $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                                                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25); ?>
                                                <br/>
                                                Nascimento: <?=$this->sma->hrsd($it->data_aniversario) . ' ' . $idade . ' anos';?>
                                            <?php } ?>
                                            <?php if ($it->doenca_informar){?>
                                                <br/>
                                                Inf Médicas: <?=$it->doenca_informar;?>
                                            <?php } ?>
                                        </td>
                                        <?php if ($checked_item) {?>
                                            <td style="text-align: center;width: 29%;">
                                                <i class="fa fa-check" style="color: #3c763d;"></i> <?=$this->sma->hrld($checked_item->date);?>
                                            </td>
                                            <td style="width: 1%;">
                                                <i class="fa fa-trash-o deletar_checkin" itemid="<?=$it->id;?>" style="cursor: pointer;"></i>
                                            </td>
                                        <?php } else {?>
                                            <td style="text-align: center;width: 30%;">
                                                <?php echo form_button('fazer_checkin', lang("fazer_checkin"), 'item="'.$it->id.'" style="padding: 6px 3px;" class="btn btn-success checkin"'); ?>
                                            </td>
                                        <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
        $('.checkin').click(function (){
            let item_id = $(this).attr('item');
            checkin(item_id);
        });

        $('.deletar_checkin').click(function (){
            let item_id = $(this).attr('itemid');
            deletar_checkin(item_id);
        });

        function checkin(item_id) {
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>checkins/checkin_action/'+item_id,
                dataType: 'json',
                success: function (resultado) {
                    if(resultado.checkin) {
                        $('#myModal').load(site.base_url + 'checkins/checkin_update/' + item_id);
                    }
                },
                error: function(error) {
                    console.error(error);
                }
            });
        }

        function deletar_checkin(itemid){
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>checkins/checkin_delete/'+itemid,
                dataType: 'json',
                success: function (resultado) {
                    if(resultado.checkin) {
                        $('#myModal').load(site.base_url + 'checkins/checkin_update/' + itemid);
                    }
                },
                error: function(error) {
                    console.error(error);
                }
            });
        }

    });
</script>
