<style type="text/css" media="screen">
    #TBRating td:nth-child(1) {display: none;}
    #TBRating td:nth-child(2) {width: 10%;text-align: center;}

    #TBRating td:nth-child(5) {width: 10%;text-align: center;}
    #TBRating td:nth-child(6) {width: 2%;text-align: center;}

    .estrela {
        font-size: 24px;
        cursor: pointer;
        color: gray;
    }

    .estrela:not(.estrela-vazia) {
        color: orange;
    }

</style>

<script>

    function criarRating(i) {
        let star = '';

        i = Math.max(0, Math.min(5, i));

        for (let j = 0; j < i; j++) {
            star += '<span class="estrela">★</span>';
        }

        for (let j = i; j < 5; j++) {
            star += '<span class="estrela estrela-vazia">★</span>';
        }

        return '<div class="text-center">' + star + '</div>';
    }

    $(document).ready(function () {
        $('#TBRating').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('ratings/getRatings') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "rating_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterProducts", "value":  $('#filter_product').val() });
                aoData.push({ "name": "filterStar", "value":  $('#star_filter').val() });
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var rating          = 0;
                var total_estrelas  = 0;

                for (var i = 0; i < aaData.length; i++) {
                    var rating_note = aaData[aiDisplay[i]][4];
                    if (!isNaN(rating_note) && rating_note !== '') {
                        rating += parseFloat(rating_note);
                        total_estrelas++;
                    }
                }

                if (total_estrelas > 0) {

                    console.log('rating ' + rating);
                    console.log('total_estrelas ' + total_estrelas);

                    var nCells = nRow.getElementsByTagName('th');
                    var average = Math.round(rating/total_estrelas);

                    console.log('average ' + average);

                    nCells[4].innerHTML = criarRating(average);
                }
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                {"mRender": criarRating},
                {"bSortable": false}
            ]
        });
    });
</script>
<?= form_open('ratings/expense_ratings_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-star-o"></i><?= lang('ratings'); ?></h2>
        <div class="box-icon">
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group">
                    <?= lang("stars", "star_filter") ?>
                    <?php
                    $cbStar['']   = lang("select") . " " . lang("stars") ;
                    $cbStar['5']  = 5;
                    $cbStar['4']  = 4;
                    $cbStar['3']  = 3;
                    $cbStar['2']  = 2;
                    $cbStar['1']  = 1;
                    $cbStar['zero']  = 0;
                    echo form_dropdown('star_filter', $cbStar, (isset($_POST['star_filter']) ? $_POST['star_filter'] : ''), 'class="form-control select" id="star_filter" placeholder="' . lang("select") . " " . lang("stars") . '" style="width:100%"')
                    ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group">
                    <?= lang("product", "filter_product") ?>
                    <?php
                    $prod[''] =  lang("select") . " " . lang("product") ;
                    foreach ($products as $produts) {
                        $prod[$produts->id] = $produts->name;
                    }
                    echo form_dropdown('filter_product', $prod, (isset($_POST['filter_product']) ? $_POST['filter_product'] : ''), 'class="form-control select" id="filter_product" placeholder="' . lang("select") . " " . lang("product") . '" style="width:100%"')
                    ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="TBRating" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("date"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("average"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="4" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("date"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("average"); ?></th>
                            <th style="width:100px;text-align: center;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {

        $('body').on('click', '.rating_link td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'ratings/view/' + $(this).parent('.rating_link').attr('id')});
            $('#myModal').modal('show');
        });

        $("#filter_product").change(function (event){
            $('#TBRating').DataTable().fnClearTable();
        });

        $("#star_filter").change(function (event){
            $('#TBRating').DataTable().fnClearTable();
        });
    });
</script>

