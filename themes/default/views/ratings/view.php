<style>
    .estrelai {
        font-size: 24px;
        cursor: pointer;
        color: gray;
    }
    .estrelai:not(.estrela-vazia) {
        color: orange;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('rating'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <?= lang("customer", "customer") ?>*
                        <?= form_input('customer', $rating->customer, 'class="form-control" readonly') ?>
                    </div>
                </div>
                <?php foreach ($responses as $response) {?>
                    <div class="col-md-12">
                        <div class="control-group col-sm-12" style="text-align: center;margin-bottom: 15px;">
                            <p style="font-size: 20px;"><?=$response->question;?></p>
                            <?php if ($response->type_question == 'rating' || $response->type_question == 'rating_note') {?>
                                <span id="star<?=$response->id;?>" class="star" rating="<?=$response->rating;?>"></span>
                            <?php } ?>
                        </div>
                        <?php if ($response->type_question == 'rating_note' || $response->type_question == 'note') {?>
                            <textarea class="form-control" rows="3" disabled><?=$response->response;?></textarea>
                        <?php } ?>
                    </div>
                <?php }?>
            </div>
        </div>
        <div class="modal-footer" style="text-align: left;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="application/javascript">
    $(document).ready(function () {

        $('.star').each(function() {
            let rating = $(this).attr('rating'); // Remove 'star' do início do ID

            let ratingHTML = criarRatingStar(rating);

            $(this).html(ratingHTML);
        });

    });

    function criarRatingStar(i) {
        let star = '';

        i = Math.max(0, Math.min(5, i));

        for (let j = 0; j < i; j++) {
            star += '<span class="estrelai">★</span>';
        }

        for (let j = i; j < 5; j++) {
            star += '<span class="estrelai estrela-vazia">★</span>';
        }

        return '<div class="text-center">' + star + '</div>';
    }
</script>
