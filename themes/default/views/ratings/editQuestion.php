<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_question'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("ratings/editQuestion/" . $question->id, $attrib); ?>
        <div class="modal-body">
            <div class="form-group">
                <?= lang('status', 'status'); ?>
                <?php
                $opt = array(1 => lang('active'), 0 => lang('inactive'));
                echo form_dropdown('active', $opt, $question->active, 'id="status" required="required" class="form-control select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('type_question', 'type_question'); ?>
                <?php
                $opt = array('rating' => lang('type_rating'), 'note' => lang('type_note'));
                echo form_dropdown('type_question', $opt, $question->type_question, 'id="type_question" required="required" class="form-control select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('question', 'question'); ?>
                <?= form_textarea('question', $question->question, 'class="form-control skip" id="question" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('ordem', 'ordem'); ?>
                <?= form_input('ordem', $question->ordem, 'class="form-control mask_integer" style="padding-right: 5px;" id="ordem" required="required"'); ?>
            </div>
            <?php echo form_hidden('id', $question->id); ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editQuestion', lang('edit_question'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>

    var mask_integer = {
        money : function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'$1');
                }
                return v;
            };
            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(function(){
        $('.mask_integer').bind('keypress',mask_integer.money);
        $('.mask_integer').click(function(){$(this).select();});
    });

</script>

