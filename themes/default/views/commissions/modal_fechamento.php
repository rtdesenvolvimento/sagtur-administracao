
<style>
    .div_sr {
        width: 100%;
        height: 350px;
        overflow: auto;
    }

    #TBFechamentoComissaoDataInner td:nth-child(1) {display: none;}
    #TBFechamentoComissaoDataInner td:nth-child(2) {display: none;}
    #TBFechamentoComissaoDataInner td:nth-child(3) {display: none;}
    #TBFechamentoComissaoDataInner td:nth-child(6) {text-align: right;width: 10%}
    #TBFechamentoComissaoDataInner td:nth-child(7) {text-align: right;width: 5%}
    #TBFechamentoComissaoDataInner td:nth-child(8) {text-align: right;width: 10%}
    #TBFechamentoComissaoDataInner td:nth-child(9) {display: none;}
    #TBFechamentoComissaoDataInner td:nth-child(10) {display: none;}
</style>

<script type="text/javascript" charset="UTF-8">

    function formatDecimal(x, d) {
        return parseFloat(x).toFixed(2) + '%';
    }

    function checkboxi(x) {
        return '<div class="text-center"><input class="checkbox multi-selecti" type="checkbox" name="val[]" value="' + x + '" /></div>';
    }

    function currencyFormatTotal(x, alignment) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(x)+'</div>';
        } else {
            return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(0)+'</div>';
        }
    }

    $('#TBFechamentoComissaoDataInner').dataTable({
        "aaSorting": [[0, "asc"], [1, "desc"]],
        "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
        "iDisplayLength": -1,
        'bProcessing': true, 'bServerSide': true,
        'sAjaxSource': '<?=site_url('commissions/getComissoesFechamento/'.$fechamento->id)?>',
        'fnServerData': function (sSource, aoData, fnCallback) {
            aoData.push({
                "name": "<?=$this->security->get_csrf_token_name()?>",
                "value": "<?=$this->security->get_csrf_hash()?>"
            });
            $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
        },
        'fnRowCallback': function (nRow, aData, iDisplayIndex) {
            nRow.id = aData[8];
            nRow.className = "invoice_link_fechamento";
            return nRow;
        },
        "fnServerParams": function (aoData) {
            aoData.push({ "name": "filterDateFrom", "value":  $('#filter_date').val() });
            aoData.push({ "name": "filterProducts", "value":  $('#filter_tour').val() });
        },
        "aoColumns": [
            {"bSortable": false, "mRender": checkboxi},
            null,
            {"mRender": fld},
            null,
            null,
            {"mRender": currencyFormat},
            {"mRender": formatDecimal},
            {"mRender": currencyFormat},
            null,
            {"bSortable": false}
        ],
        "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
            var gtotal = 0, total_comissao = 0;
            for (var i = 0; i < aaData.length; i++) {
                gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                total_comissao += parseFloat(aaData[aiDisplay[i]][7]);
            }
            var nCells = nRow.getElementsByTagName('th');

            var percentual_comissao = (total_comissao / gtotal) * 100;

            nCells[5].innerHTML = currencyFormatTotal(parseFloat(gtotal));
            nCells[6].innerHTML = percentual_comissao.toFixed(2) + '%';
            nCells[7].innerHTML = currencyFormatTotal(parseFloat(total_comissao));

            $('#total_commissionf').val(formatMoney(parseFloat(total_comissao)));

        },
        "fnDrawCallback": function (oSettings) {

            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        },
    });
</script>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('fechamento_comissao'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("commissions/fechar_comissao/".$fechamento->id, $attrib); ?>
        <div class="modal-body">

            <!--Tabela de comissoes adicionadas no fechamento da comissão !-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="box">
                        <div class="box-header">
                            <h2 class="blue">
                                <i class="fa-fw fa fa-plus-square-o"></i><?=lang('header_commissions')?>
                            </h2>
                        </div>
                        <div class="box-content div_sr">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <table id="TBFechamentoComissaoDataInner" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                            <thead>
                                            <tr>
                                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkfti" type="checkbox" name="check"/></th>
                                                <th style="display: none;"><?php echo $this->lang->line("reference_no"); ?></th>
                                                <th style="display: none;"><?php echo $this->lang->line("date"); ?></th>
                                                <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                                <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                                <th><?php echo $this->lang->line("base_value"); ?></th>
                                                <th><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                <th><?php echo $this->lang->line("commission"); ?></th>
                                                <th style="display: none;"></th>
                                                <th style="width:10px; text-align:center;display: none;"><?php echo $this->lang->line("actions"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr><td colspan="5" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                            </tbody>
                                            <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkfti" type="checkbox" name="check"/></th>
                                                <th style="display: none;"><?php echo $this->lang->line("reference_no"); ?></th>
                                                <th style="display: none;"><?php echo $this->lang->line("date"); ?></th>
                                                <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                                <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                                <th style="text-align: right;"><?php echo $this->lang->line("base_value"); ?></th>
                                                <th style="text-align: right;"><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                <th style="text-align: right;"><?php echo $this->lang->line("commission"); ?></th>
                                                <th style="display: none;"></th>
                                                <th style="width:10px; text-align:center;display: none;"><?php echo $this->lang->line("actions"); ?></th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("total_commission", "total_commission"); ?>
                            <input type="text" name="total_commission" id="total_commission" value="<?=$fechamento->total_commission;?>" readonly style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang("data_vencimento", "dtvencimento"); ?>
                            <input type="date" name="dtvencimento" value="<?php echo date('Y-m-d');?>" class="form-control tip" id="dtvencimento" required="required" data-original-title="Data do primeiro vencimento" title="Data do primeiro vencimento">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang('tipo_cobranca', 'tipocobranca'); ?>
                            <?php
                            $cbTipoDeCobranca[''] = '';
                            foreach ($tiposCobranca as $tipoCobranca) {
                                $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                            }
                            echo form_dropdown('tipocobranca', $cbTipoDeCobranca, $this->Settings->tipo_cobranca_fechamento_id, 'id="tipocobranca" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca_financeiro") . '" required="required" style="width:100%;" ');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <?= lang('condicao_pagamento', 'condicao_pagamento'); ?>
                            <?php
                            $cbCondicoesPagamento[''] = lang('select').' '.lang('condicao_pagamento');
                            foreach ($condicoesPagamento as $condicaoPagamento) {
                                $cbCondicoesPagamento[$condicaoPagamento->id] = $condicaoPagamento->name;
                            } ?>
                            <?= form_dropdown('condicaopagamento', $cbCondicoesPagamento, $this->Settings->condicaoPagamentoAVista, 'class="form-control tip" id="condicaopagamento" required="required"'); ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><h4><?= lang('parcelas') ?></h4></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="controls table-controls">
                                    <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th style="text-align: left;">#</th>
                                            <th class="col-md-4" style="text-align: left;">Vencimento</th>
                                            <th class="col-md-4" style="text-align: left;">Cobrança</th>
                                            <th class="col-md-4" style="text-align: left;">Conta</th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang("note", "note"); ?>
                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('confirmar_fechamento', lang('confirmar_fechamento'), 'id="btn-adicionar-conta-pagar" class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<?= $modal_js ?>

<script type="text/javascript" charset="UTF-8">

    $(document).ready(function () {

        $("#tipocobranca").select2().on("change", function(e) {
            let contapadao =  $("#tipocobranca  option:selected").attr('conta');
            if (contapadao !== '') {
                $('#contadestino').val(contapadao).change();
                $('#contadestino').attr('disabled', true);
            } else {
                $('#contadestino').val('').change();
                $('#contadestino').attr('disabled', false);
            }
            getParcelamento();
        });

        $('#valor').blur(function (event) {
            getParcelamento();
        });

        $('#dtvencimento').blur(function (event) {
            getParcelamento();
        });

        $("#condicaopagamento").select2().on("change", function(e) {
            getParcelamento();
        });

        getParcelamento();
    });

    function getParcelamento() {

        var condicaoPagamento = $('#condicaopagamento').val();
        var tipoCobranca = $("#tipocobranca  option:selected").val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: site.base_url + "commissions/getParcelasCommission",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    tipoCobrancaId: tipoCobranca,
                    dtvencimento: $('#dtvencimento').val(),
                },
                dataType: 'html',
                success: function (html) {
                    $('#slTableParcelas tbody').html(html);

                    $(function(){
                        $('.mask_money').bind('keypress',mask.money);
                        $('.mask_money').click(function(){$(this).select();});
                    });
                }
            });
        } else {
            $('#slTableParcelas tbody').html('');
        }
    }

</script>
