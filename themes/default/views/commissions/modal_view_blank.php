<style>
    .estrelai {
        font-size: 24px;
        cursor: pointer;
        color: gray;
    }
    .estrelai:not(.estrela-vazia) {
        color: orange;
    }
</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-2">
                        <div class="text-center">
                            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                 alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <h2 style="margin-top:0px;"><?= $this->Settings->site_name ?></h2>
                        <?= $biller->company ? "" : "Attn: " . $biller->name ?>
                        <?php

                        if ($biller->address != '') {
                            echo $biller->address . "<br>" . $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                        } else {
                            echo $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                        }

                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                        }
                        if ($biller->cf1 != "-" && $biller->cf1 != "") {
                            echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                        }
                        if ($biller->cf2 != "-" && $biller->cf2 != "") {
                            echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                        }
                        if ($biller->cf3 != "-" && $biller->cf3 != "") {
                            echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                        }
                        if ($biller->cf4 != "-" && $biller->cf4 != "") {
                            echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                        }
                        if ($biller->cf5 != "-" && $biller->cf5 != "") {
                            echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                        }
                        if ($biller->cf6 != "-" && $biller->cf6 != "") {
                            echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                        }
                        echo "<br>" .lang("tel") . ": " . $biller->phone . " " . lang("email") . ": " . $biller->email;
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="text-align: center;">
                <h2><?=lang("comissao_nao_encontrada_fechamento");?></h2>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
