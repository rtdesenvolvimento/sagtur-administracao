<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_commission_item'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'form');
        echo form_open("commissions/edit_commission_item/".$item->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("biller", "biller"); ?>
                        <?php echo form_input('biller', $item->biller, 'class="form-control input-tip" disabled id="biller"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("customer", "customer"); ?>
                        <?php echo form_input('customer', $item->customer, 'class="form-control input-tip" disabled id="biller"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("base_value", "base_value"); ?>
                        <?php echo form_input('base_value', $this->sma->formatMoney($item->base_value), 'class="form-control input-tip mask_money" disabled id="base_value"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("commission_percentage", "commission_percentage"); ?>
                        <?php echo form_input('commission_percentage', $item->commission_percentage.'%', 'class="form-control input-tip mask_money" disabled id="commission_percentage"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("commission", "commission"); ?>
                        <?php echo form_input('commission', $this->sma->formatMoney($item->commission), 'class="form-control input-tip mask_money" disabled id="commission"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("new_commission_percentage", "new_commission_percentage"); ?>
                        <?php echo form_input('new_commission_percentage', 0, 'class="form-control input-tip mask_money" readonly id="new_commission_percentage"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("new_commission", "new_commission"); ?>
                        <?php echo form_input('new_commission', 0, 'class="form-control input-tip mask_money" style="padding-right: 5px;" required="required" id="new_commission"'); ?>
                    </div>
                </div>
                <div class="col-lg-12"">
                    <div class="form-group">
                        <?= lang("motivo_alteracao_comissao", "note"); ?>
                        <?php echo form_textarea('note', '', 'class="form-control" id="note" required="required" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_commission_item', lang('edit_commission_item'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $('#new_commission').on('keyup', function() {
            var new_commission = $(this).val();
            var base_value = <?= $item->base_value ?>;
            var new_commission_percentage = (new_commission * 100) / base_value;

            $('#new_commission_percentage').val(new_commission_percentage.toFixed(2)+'%');
        });
    });
</script>