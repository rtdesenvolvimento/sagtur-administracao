<style media="screen">

    #TBFechamentoComissaoAbertaData td:nth-child(1) {text-align: center;width: 2%;}
    #TBFechamentoComissaoAbertaData td:nth-child(2) {text-align: center;width: 10%;}
    #TBFechamentoComissaoAbertaData td:nth-child(3) {text-align: center;width: 12%;}
    #TBFechamentoComissaoAbertaData td:nth-child(6) {text-align: right;width: 10%}
    #TBFechamentoComissaoAbertaData td:nth-child(7) {text-align: right;width: 5%}
    #TBFechamentoComissaoAbertaData td:nth-child(8) {text-align: right;width: 10%}
    #TBFechamentoComissaoAbertaData td:nth-child(9) {display: none;}
    #TBFechamentoComissaoAbertaData td:nth-child(10) {text-align: center;width: 2%;}

    <?php if ($fechamento->status == 'Pendente'){?>
        #TBFechamentoComissaoData td:nth-child(1) {text-align: center;width: 2%;}
    <?php } else { ?>
        #TBFechamentoComissaoData td:nth-child(1) {display: none;}
    <?php } ?>

    #TBFechamentoComissaoData td:nth-child(2) {text-align: center;width: 10%;}
    #TBFechamentoComissaoData td:nth-child(3) {text-align: center;width: 12%;}
    #TBFechamentoComissaoData td:nth-child(6) {text-align: right;width: 10%}
    #TBFechamentoComissaoData td:nth-child(7) {text-align: right;width: 5%}
    #TBFechamentoComissaoData td:nth-child(8) {text-align: right;width: 10%}
    #TBFechamentoComissaoData td:nth-child(9) {display: none;}

    <?php if ($fechamento->status == 'Pendente'){?>
        #TBFechamentoComissaoData td:nth-child(10) {text-align: center;width: 2%;}
    <?php } else { ?>
        #TBFechamentoComissaoData td:nth-child(10) {display: none;}
    <?php } ?>


</style>

<script>

    $(document).ready(function () {

        function formatDecimal(x, d) {
            return parseFloat(x).toFixed(2) + '%';
        }

        function checkboxi(x) {
            return '<div class="text-center"><input class="checkbox multi-selecti" type="checkbox" name="val[]" value="' + x + '" /></div>';
        }

        function currencyFormatTotal(x, alignment) {

            if (alignment === undefined) alignment = 'text-right';

            if (x != null) {
                return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(x)+'</div>';
            } else {
                return '<div class="'+alignment+'" style="font-size: 1.7rem;">'+formatMoney(0)+'</div>';
            }
        }

        <?php if ($fechamento->status == 'Pendente'){?>
            $('#TBFechamentoComissaoAbertaData').dataTable({
                "aaSorting": [[0, "asc"], [1, "desc"]],
                "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
                "iDisplayLength": -1,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?=site_url('commissions/getComissoesAbertas/'.$fechamento->id)?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?=$this->security->get_csrf_token_name()?>",
                        "value": "<?=$this->security->get_csrf_hash()?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[8];
                    nRow.className = "invoice_link_fechamento";
                    return nRow;
                },
                "fnServerParams": function (aoData) {
                    aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });
                    aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });

                    aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                    aoData.push({ "name": "programacaoFilter", "value":  $('#filter_programacao_id').val() });

                    aoData.push({ "name": "flDataParcelaDe", "value":  $('#flDataParcelaDe').val() });
                    aoData.push({ "name": "flDataParcelaAte", "value":  $('#flDataParcelaAte').val() });

                    aoData.push({ "name": "flStatusParcela", "value":  $('#flStatusParcela').val() });
                    aoData.push({ "name": "flLiberacaoComissao", "value":  $('#flLiberacaoComissao').val() });
                    aoData.push({ "name": "flTipoCobranca", "value":  $('#flTipoCobranca').val() });
                },
                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                    var gtotal = 0, total_comissao = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                        total_comissao += parseFloat(aaData[aiDisplay[i]][7]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    var percentual_comissao = (total_comissao / gtotal) * 100;

                    nCells[5].innerHTML = currencyFormatTotal(parseFloat(gtotal));
                    nCells[6].innerHTML = percentual_comissao.toFixed(2) + '%';
                    nCells[7].innerHTML = currencyFormatTotal(parseFloat(total_comissao));
                },
                "aoColumns": [
                    {"bSortable": false, "mRender": checkbox},
                    null,
                    {"mRender": fld},
                    null,
                    null,
                    {"mRender": currencyFormat},
                    {"mRender": formatDecimal},
                    {"mRender": currencyFormat},
                    null,
                    {"bSortable": false}
                ],
                "fnDrawCallback": function (oSettings) {
                    if (oSettings._iRecordsTotal > 0) {
                       // $('#div_comissoes_aberta').show();
                    } else {
                       // $('#div_comissoes_aberta').hide();
                    }

                    $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    });
                },
            });
        <?php } ?>

        $('#TBFechamentoComissaoData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": -1,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('commissions/getComissoesFechamento/'.$fechamento->id)?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[8];
                nRow.className = "invoice_link_fechamento";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterDateFrom", "value":  $('#filter_date').val() });
                aoData.push({ "name": "filterProducts", "value":  $('#filter_tour').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkboxi},
                null,
                {"mRender": fld},
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": formatDecimal},
                {"mRender": currencyFormat},
                null,
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, total_comissao = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                    total_comissao += parseFloat(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');

                var percentual_comissao = (total_comissao / gtotal) * 100;

                nCells[5].innerHTML = currencyFormatTotal(parseFloat(gtotal));
                nCells[6].innerHTML = percentual_comissao.toFixed(2) + '%';
                nCells[7].innerHTML = currencyFormatTotal(parseFloat(total_comissao));

                $('#total_commissionf').val(formatMoney(parseFloat(total_comissao)));

            },
            "fnDrawCallback": function (oSettings) {

                $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });
            },
        });
    });

</script>

<div class="box">
    <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('montar_fechamento_comissao'); ?></h2></div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <div class="col-sm-2">
                        <?= lang("status", "status") ?>
                        <?php
                        $opts = array(
                            'Pendente' => lang('pendente'),
                            'Confirmada' => lang('confirmada'),
                            'Cancelada' => lang('cancelada'),
                        );
                        echo form_dropdown('statusf', $opts,  $fechamento->status, 'class="form-control" disabled="disabled"');
                        ?>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("dt_competencia", "dt_competencia"); ?>
                            <?php echo form_input('dt_competenciaf', $fechamento->dt_competencia, 'class="form-control input-tip" disabled', 'date'); ?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang('title', 'title'); ?>
                            <?= form_input('titlef', $fechamento->title, 'class="form-control" disabled'); ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?= lang('total_commission', 'total_commission'); ?>
                            <?= form_input('total_commissionf', $fechamento->total_commission, 'class="form-control" disabled id="total_commissionf"'); ?>
                        </div>
                    </div>
                </div>

                <?php if ($fechamento->status == 'Pendente'){?>
                    <!--Tabela de comissoes abertas para inserir no fechamento !-->
                    <?php  echo form_open('commissions/commissions_actions', 'id="action-form-comissao-inserir"');?>
                        <div class="row" id="div_comissoes_aberta">
                            <div class="col-lg-12">
                                <div class="box">
                                    <div class="box-header">
                                        <h2 class="blue">
                                            <i class="fa-fw fa fa-plus-square-o"></i><?=lang('header_add_commissions_close')?>
                                        </h2>
                                        <div class="box-icon">
                                            <ul class="btn-tasks">
                                                <li class="dropdown">
                                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                                                    </a>
                                                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                                        <li>
                                                            <a href="#" id="insert_commissions" data-action="insert_commissions">
                                                                <i class="fa fa-plus-circle"></i> <?=lang('insert_commissions')?>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="box-content">
                                        <fieldset class="scheduler-border">
                                            <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                                            <div style="margin-bottom: 20px;" class="divfilters">
                                                <div class="row">
                                                    <div class="col-sm-10">
                                                        <?= lang("product", "product"); ?>
                                                        <?php
                                                        $pgs[""] = lang('select').' '.lang('product');
                                                        echo form_dropdown('filter_programacao_id', $pgs,  '', 'class="form-control" id="filter_programacao_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <button type="button" class="btn btn-primary" style="width: 100%;margin-top: 25px;" data-toggle="modal" data-target="#filterModal"><i class="fa fa-search"></i> <?=$this->lang->line("filter_product");?></button>
                                                    </div>

                                                    <div class="col-sm-3">
                                                        <?= lang("data_venda_de", "data_venda_de"); ?>
                                                        <?php echo form_input('flDataVendaDe', '', 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= lang("data_venda_ate", "data_venda_ate"); ?>
                                                        <?php echo form_input('flDataVendaAte', '', 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <?= lang("biller", "billerFilter"); ?>
                                                        <?php
                                                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                                                        foreach ($billers as $biller) {
                                                            $bl[$biller->id] = $biller->name;
                                                        }
                                                        echo form_dropdown('flBiller', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <?= lang("data_parcela_de", "data_parcela_de"); ?>
                                                        <?php echo form_input('flDataParcelaDe', '', 'type="date" class="form-control" id="flDataParcelaDe"', 'date'); ?>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <?= lang("data_venda_ate", "data_venda_ate"); ?>
                                                        <?php echo form_input('flDataParcelaAte', '', 'type="date" class="form-control" id="flDataParcelaAte"', 'date'); ?>
                                                    </div>
                                                    <div  class="col-sm-2">
                                                        <?= lang("status_parcela", "status_parcela") ?>
                                                        <?php
                                                        $cbStatus = array(
                                                            '' => lang('select'),
                                                            'ABERTA' => lang('aberta'),
                                                            'PARCIAL' => lang('parcial'),
                                                            'QUITADA' => lang('quitada'),
                                                            'VENCIDA' => lang('vencida'),
                                                            );
                                                        echo form_dropdown('flStatusParcela', $cbStatus,  '', 'class="form-control" id="flStatusParcela"'); ?>
                                                    </div>
                                                    <div  class="col-sm-2">
                                                        <?= lang("liberacao_comissao", "liberacao_comissao") ?>
                                                        <?php
                                                        $cbStatus = array(
                                                            '' => lang('select'),
                                                            'primeira_parcela_paga' => lang('primeira_parcela_paga'),
                                                            'ultima_parcela_paga' => lang('ultima_parcela_paga'),
                                                        );
                                                        echo form_dropdown('flLiberacaoComissao', $cbStatus,  '', 'class="form-control" id="flLiberacaoComissao"'); ?>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <?= lang("tipo_cobranca", "tipo_cobranca") ?>
                                                            <?php
                                                            $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                                            foreach ($tiposCobranca as $tipoCobranca) {
                                                                $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                                            }
                                                            echo form_dropdown('flTipoCobranca', $tcs, '', 'class="form-control" id="flTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="controls">
                                                            <input type="button" onclick="limparFormulario();" name="submit_fin" value="<?=lang('clear')?>" class="btn btn-primary input-xs">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="table-responsive">
                                                    <table id="TBFechamentoComissaoAbertaData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                                        <thead>
                                                        <tr>
                                                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                            <th><?php echo $this->lang->line("reference_no"); ?></th>
                                                            <th><?php echo $this->lang->line("date"); ?></th>
                                                            <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                                            <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                                            <th><?php echo $this->lang->line("base_value"); ?></th>
                                                            <th><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                            <th><?php echo $this->lang->line("commission"); ?></th>
                                                            <th style="display: none;"></th>
                                                            <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr><td colspan="10" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                                        </tbody>
                                                        <tfoot class="dtFilter">
                                                        <tr class="active">
                                                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                            <th><?php echo $this->lang->line("reference_no"); ?></th>
                                                            <th><?php echo $this->lang->line("date"); ?></th>
                                                            <th><?php echo $this->lang->line("biller"); ?></th>
                                                            <th><?php echo $this->lang->line("customer"); ?></th>
                                                            <th style="text-align: right;"><?php echo $this->lang->line("base_value"); ?></th>
                                                            <th style="text-align: right;"><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                            <th style="text-align: right;"><?php echo $this->lang->line("commission"); ?></th>
                                                            <th style="display: none;"></th>
                                                            <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="display: none;">
                            <input type="hidden" name="form_action" value="" id="form_action_comissao_inserir"/>
                            <input type="hidden" name="fechamento_id" value="<?=$fechamento->id?>"/>
                            <?=form_submit('performAction', 'performAction', 'id="action-form-submit-comissao-inserir"')?>
                        </div>
                    <?=form_close()?>
                <?php } ?>

                <!--Tabela de comissoes adicionadas no fechamento da comissão !-->
                <?php  echo form_open('commissions/commissions_actions', 'id="action-form-comissao-inserida"');?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box">
                                <div class="box-header">
                                    <h2 class="blue">
                                        <i class="fa-fw fa fa-plus-square-o"></i><?=lang('header_commissions_close')?>
                                    </h2>
                                    <div class="box-icon">
                                        <ul class="btn-tasks">
                                            <li class="dropdown">
                                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                    <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                                                </a>
                                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                                    <li>
                                                        <a href="#" id="delete_commissions" data-action="delete_commissions">
                                                            <i class="fa fa-plus-circle"></i> <?=lang('delete_commissions')?>
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>
                                                    <li>
                                                        <a href="<?=site_url('commissions/edit_fechamento/'.$fechamento->id)?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                                            <i class="fa fa-edit"></i> <?=lang('edit_fechamento')?>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="TBFechamentoComissaoData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                                                    <thead>
                                                    <tr>
                                                        <?php if ($fechamento->status == 'Pendente'){?>
                                                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkfti" type="checkbox" name="check"/></th>
                                                        <?php } else {?>
                                                            <th style="display: none;"></th>
                                                        <?php }?>

                                                        <th><?php echo $this->lang->line("reference_no"); ?></th>
                                                        <th><?php echo $this->lang->line("date"); ?></th>
                                                        <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                                        <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                                        <th><?php echo $this->lang->line("base_value"); ?></th>
                                                        <th><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                        <th><?php echo $this->lang->line("commission"); ?></th>
                                                        <th style="display: none;"></th>
                                                        <?php if ($fechamento->status == 'Pendente'){?>
                                                            <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                        <?php } else {?>
                                                            <th style="display: none;"></th>
                                                        <?php }?>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr><td colspan="9" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                                    </tbody>
                                                    <tfoot class="dtFilter">
                                                    <tr class="active">

                                                        <?php if ($fechamento->status == 'Pendente'){?>
                                                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkfti" type="checkbox" name="check"/></th>
                                                        <?php } else {?>
                                                            <th style="display: none;"></th>
                                                        <?php }?>

                                                        <th><?php echo $this->lang->line("reference_no"); ?></th>
                                                        <th><?php echo $this->lang->line("date"); ?></th>
                                                        <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                                                        <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: right;"><?php echo $this->lang->line("base_value"); ?></th>
                                                        <th style="text-align: right;"><?php echo $this->lang->line("commission_percentage"); ?></th>
                                                        <th style="text-align: right;"><?php echo $this->lang->line("commission"); ?></th>
                                                        <th style="display: none;"></th>
                                                        <?php if ($fechamento->status == 'Pendente'){?>
                                                            <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                        <?php } else {?>
                                                            <th style="display: none;"></th>
                                                        <?php }?>                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="display: none;">
                        <input type="hidden" name="form_action" value="" id="form_action_comissao_inserida"/>
                        <input type="hidden" name="fechamento_id" value="<?=$fechamento->id?>"/>
                        <?=form_submit('performAction', 'performAction', 'id="action-form-submit-comissao-inserida"')?>
                    </div>
                <?=form_close()?>

                <?php if ($fechamento->status == 'Pendente'){?>
                    <a href="<?=site_url('commissions/fechar_comissao/'.$fechamento->id)?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                        <?php echo form_button('fechar_comissao', '<i class="fa fa-money"></i> '.$this->lang->line("abrir_fechamento_comissao"), 'id="fechar_comissao" class="btn btn-success"'); ?>
                    </a>
                <?php }?>

                <a href="<?=site_url('commissions/modal_view/'.$fechamento->id)?>" data-toggle="modal" data-target="#myModal">
                    <?php echo form_button('commission_details', '<i class="fa fa-eye"></i> '. $this->lang->line("commission_details"), 'id="commission_details" class="btn btn-primary"'); ?>
                </a>

                <a href="<?=site_url('commissions/report/'.$fechamento->id.'/0/0/portrait')?>" target="_blank">
                    <?php echo form_button('report_commissions', '<i class="fa fa-file-pdf-o"></i> '. $this->lang->line("report_commissions"), 'id="report_commissions" class="btn btn-primary"'); ?>
                </a>

            </div>
        </div>
    </div>
</div>

<!-- Modal Filter Pacote-->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="filterModalLabel">Filtros de Pacote</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('filter_status_pacote', $opts,  'Confirmado', 'class="form-control" id="filter_status_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('filter_ano_pacote', $opts,  date('Y'), 'class="form-control" id="filter_ano_pacote"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('filter_mes_pacote', $opts,  date('m'), 'class="form-control" id="filter_mes_pacote"'); ?>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
                <button type="button"  id="applyFilters" class="btn btn-primary">Aplicar Filtros</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        createFilter('filters');
        buscarProdutos();

        $('#flDataVendaDe').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flDataVendaAte').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#billerFilter').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flDataParcelaDe').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flDataParcelaAte').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flStatusParcela').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flLiberacaoComissao').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#flTipoCobranca').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('#filter_programacao_id').change(function() {
            $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        });

        $('body').on('click', '.invoice_link_fechamento td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'salesutil/modal_view/' + $(this).parent('.invoice_link_fechamento').attr('id')});
            $('#myModal').modal('show');
        });

        $('body').on('click', '#insert_commissions', function(e) {
            e.preventDefault();
            $('#form_action_comissao_inserir').val($(this).attr('data-action'));
            $('#action-form-submit-comissao-inserir').trigger('click');
        });

        $('body').on('click', '#delete_commissions', function(e) {
            e.preventDefault();
            $('#form_action_comissao_inserida').val($(this).attr('data-action'));
            $('#action-form-submit-comissao-inserida').trigger('click');
        });

        $(document).on('ifChecked', '.checkfti', function(event) {
            $('.multi-selecti').iCheck('check');
        });
        $(document).on('ifUnchecked', '.checkfti', function(event) {
            $('.multi-selecti').iCheck('uncheck');
        });

        $('#applyFilters').click(function() {
            buscarProdutos();
        });
    });

    function addItemFechamento(commission_item_id) {
        $('#TBFechamentoComissaoAbertaData tbody').empty();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>commissions/add_item_fechamento",
            data: {
                commission_item_id: commission_item_id,
                close_commission_id : '<?=$fechamento->id;?>',
            },
            dataType: 'json',
            success: function (retorno) {
                $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
                $('#TBFechamentoComissaoData').dataTable().fnDraw();
            }
        });
    }

    function removeItemFechamento(commission_item_id) {
        $('#TBFechamentoComissaoAbertaData tbody').empty();
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>commissions/remove_item_fechamento",
            data: {
                fechamento_item_id: commission_item_id,
            },
            dataType: 'json',
            success: function (retorno) {
                $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
                $('#TBFechamentoComissaoData').dataTable().fnDraw();
            }
        });
    }

    function createFilter(nameClasse) {
        $('.'+nameClasse).click(function() {
            if ($('.div'+nameClasse).is(':visible')) {
                $('.div'+nameClasse).hide(300);
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            } else {
                $('.div'+nameClasse).show(300).fadeIn();
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
            }
        });
    }

    function limparFormulario() {
        $('#flDataVendaDe').val('');
        $('#flDataVendaAte').val('');
        $('#billerFilter').val('');
        $('#flDataParcelaDe').val('');
        $('#flDataParcelaAte').val('');
        $('#flStatusParcela').val('');
        $('#flLiberacaoComissao').val('');
        $('#flTipoCobranca').val('');
        $('#filter_programacao_id').val('').select2()

        $('#TBFechamentoComissaoAbertaData').dataTable().fnDraw();
        $('#TBFechamentoComissaoData').dataTable().fnDraw();
    }

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#filter_status_pacote').val(),
                ano: $('#filter_ano_pacote').val(),
                mes: $('#filter_mes_pacote').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {

                $('#filter_programacao_id').empty();
                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#filter_programacao_id').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#filter_programacao_id').append(option);
                });

                $('#filter_programacao_id').select2({minimumResultsForSearch: 7});
                $('#filterModal').modal('hide');
            }
        });
    }
</script>