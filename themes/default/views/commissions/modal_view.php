<style>
    .estrelai {
        font-size: 24px;
        cursor: pointer;
        color: gray;
    }
    .estrelai:not(.estrela-vazia) {
        color: orange;
    }
</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-2">
                        <div class="text-center">
                            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                 alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                        </div>
                    </div>
                    <div class="col-xs-5">
                        <p class="bold" style="padding: 20px;">
                            <?= lang("title"); ?>: <?= $fechamento->title; ?><br>
                            <?= lang("dt_competencia"); ?>: <?= $this->sma->hrsd($fechamento->dt_competencia); ?><br>
                            <?= lang("ref"); ?>: <?= $fechamento->reference_no; ?><br>
                            <?= lang("status"); ?>: <?= lang($fechamento->status); ?><br>
                        </p>
                    </div>
                    <div class="col-xs-5">
                        <h2 style="margin-top:0px;"><?= $this->Settings->site_name ?></h2>
                        <?= $biller->company ? "" : "Attn: " . $biller->name ?>
                        <?php

                        if ($biller->address != '') {
                            echo $biller->address . "<br>" . $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                        } else {
                            echo $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                        }

                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                        }
                        if ($biller->cf1 != "-" && $biller->cf1 != "") {
                            echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                        }
                        if ($biller->cf2 != "-" && $biller->cf2 != "") {
                            echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                        }
                        if ($biller->cf3 != "-" && $biller->cf3 != "") {
                            echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                        }
                        if ($biller->cf4 != "-" && $biller->cf4 != "") {
                            echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                        }
                        if ($biller->cf5 != "-" && $biller->cf5 != "") {
                            echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                        }
                        if ($biller->cf6 != "-" && $biller->cf6 != "") {
                            echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                        }
                        echo "<br>" .lang("tel") . ": " . $biller->phone . " " . lang("email") . ": " . $biller->email;
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div style="text-align: center;">
                <h2>Relatório Geral de Comissão</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th><?= lang("nº"); ?></th>
                        <th><?= lang("date"); ?></th>
                        <th><?= lang("ref"); ?></th>
                        <th style="text-align: left;"><?= lang("biller"); ?></th>
                        <th style="text-align: left;"><?= lang("customer"); ?></th>
                        <th style="text-align: left;"><?= lang("product"); ?></th>
                        <th style="text-align: right;"><?= lang("base_value"); ?></th>
                        <th style="text-align: right;"><?= lang("%"); ?></th>
                        <th style="text-align: right;"><?= lang("commission"); ?></th>
                        <th style="text-align: center;"><?= lang("status"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $vendedoresComissao = [];
                    if(!empty($itensFechamento)) {?>
                        <?php
                        $contador = 1;
                        foreach ($itensFechamento as $item) {

                            $vendedor = $item->biller;

                            if (!isset($vendedoresComissao[$vendedor])) {
                                $vendedoresComissao[$vendedor] = [
                                    'total_base' => 0,
                                    'total_comissao' => 0
                                ];
                            }

                            $vendedoresComissao[$vendedor]['biller_id'] = $item->biller_id;
                            $vendedoresComissao[$vendedor]['total_base'] += $item->base_value;
                            $vendedoresComissao[$vendedor]['total_comissao'] += $item->commission;
                            $vendedoresComissao[$vendedor]['total_paid'] += $item->paid;
                            $vendedoresComissao[$vendedor]['total_due'] += ($item->commission - $item->paid);

                            ?>
                            <tr>
                                <td><?= $contador; ?></td>
                                <td style="width: 14%;text-align: center;"><?= $this->sma->hrld($item->date); ?></td>
                                <td><?= $item->reference_no; ?></td>
                                <td><?= $item->biller; ?></td>
                                <td><?= $item->customer; ?></td>
                                <td><?= $item->product_name; ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($item->base_value); ?></td>
                                <td style="text-align: right;"><?= $item->commission_percentage.'%'; ?></td>
                                <td style="text-align: right;"><?= $this->sma->formatMoney($item->commission); ?></td>
                                <td style="width: 10%;text-align: center;"><?= lang($item->status); ?></td>
                            </tr>
                            <?php $contador++; } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="10" style="text-align: center;">Nenhuma comissão encontrado</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="7" style="text-align: right;font-weight:bold;"><?= lang("total_commission"); ?>(<?= $default_currency->code; ?>)</td>
                        <td colspan="3" style="text-align: right;right;font-weight:bold;"><?= $this->sma->formatMoney($fechamento->total_commission); ?></td>
                    </tfoot>
                </table>
            </div>
            <div style="text-align: center;">
                <h2>Relatório de Comissão por Vendedores</h2>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th style="text-align: left;"><?= lang("biller"); ?></th>
                        <th style="text-align: right;"><?= lang("base_value"); ?></th>
                        <th style="text-align: right;"><?= lang("total_comissao"); ?></th>
                        <th style="text-align: right;"><?= lang("due"); ?></th>
                        <th style="text-align: right;"><?= lang("paid"); ?></th>
                        <th><i class="fa fa-print"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($vendedoresComissao)) {?>
                        <?php foreach ($vendedoresComissao as $vendedor => $valores) { ?>
                            <tr>
                                <td><?= $vendedor; ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($valores['total_base']); ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($valores['total_comissao']); ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($valores['total_due']); ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($valores['total_paid']); ?></td>
                                <td style="width: 2%;text-align: right;">
                                    <a href="<?= site_url('commissions/report_biller/' . $fechamento->id.'/'.$valores['biller_id']) ?>" target="_blank" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                                        <i class="fa fa-print"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="6" style="text-align: center;">Nenhuma comissão encontrado</td>
                        </tr>
                    <?php } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td style="text-align: right;font-weight:bold;"><?= lang("total_geral"); ?>  (<?= $default_currency->code; ?>)</td>
                        <td style="text-align: right;font-weight:bold;">
                            <?= $this->sma->formatMoney(array_sum(array_column($vendedoresComissao, 'total_base'))); ?>
                        </td>
                        <td style="text-align: right;font-weight:bold;">
                            <?= $this->sma->formatMoney(array_sum(array_column($vendedoresComissao, 'total_comissao'))); ?>
                        </td>
                        <td style="text-align: right;font-weight:bold;">
                            <?= $this->sma->formatMoney(array_sum(array_column($vendedoresComissao, 'total_due'))); ?>
                        </td>
                        <td style="text-align: right;font-weight:bold;">
                            <?= $this->sma->formatMoney(array_sum(array_column($vendedoresComissao, 'total_paid'))); ?>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <?php if (!empty($faturas)){?>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                        <thead>
                        <tr><th colspan="9" style="text-align: center;color: #ffffff;font-weight: bold;">DADOS DAS FATURA</th></tr>
                        </thead>
                        <thead>
                        <tr>
                            <th style="text-align: center;width: 5%;">#</th>
                            <th style="text-align: left;">Vendedor</th>
                            <th style="text-align: center;">Vencimento</th>
                            <th style="text-align: center;">Cobrança</th>
                            <th style="text-align: right;">Pagar</th>
                            <th style="text-align: right;">Recebido</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalPagarFatura = 0.00;
                        $totalPagoFatura = 0.00;
                        foreach ($faturas as $fatura) {
                            $parcela = $this->site->getParcelaOneByFatura($fatura->id);
                            $totalPagarFatura += $fatura->valorfatura;
                            $totalPagoFatura += $fatura->valorpago;
                            ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $fatura->numeroparcela.'X';?></td>
                                <td style="text-align: left;"><?=$fatura->biller;?></td>
                                <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                                <td style="text-align: center;">
                                    <?php echo $fatura->tipoCobranca;?>
                                    <br/><?php echo $fatura->reference;?>
                                </td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                                <td style="text-align: center;">
                                    <?php echo lang($fatura->status);?>
                                </td>
                                <td style="text-align: center;width: 3%;">
                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                        <a href="<?php echo base_url();?>faturas/adicionarPagamento/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"><span class="fa fa-usd"></span> Pagar</a>
                                    <?php } else { ?>
                                        -
                                    <?php  } ?>
                                </td>
                                <td style="text-align: center;width: 3%;">
                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                        <a href="<?php echo base_url();?>faturas/editarFatura/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"><span class="fa fa-edit"></span> Editar</a>
                                    <?php } else { ?>
                                        -
                                    <?php  } ?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                    <table class="table table-bordered table-hover table-striped" style="width: 100%;margin-top: -20px;">
                        <tfoot>
                        <tr>
                            <td colspan="6"
                                style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($totalPagarFatura); ?></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($totalPagoFatura); ?></td>
                        </tr>
                        <tr>
                            <td colspan="6" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($totalPagarFatura - $totalPagoFatura); ?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <?php }?>
            <?php if (!empty($payments)) { ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-condensed print-table">
                        <thead>
                        <tr><th colspan="10" style="text-align: center;color: #ffffff;font-weight: bold;">PAGAMENTOS</th></tr>
                        </thead>
                        <thead>
                        <tr>
                            <th style="text-align: left;"><?= lang('biller') ?></th>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('payment_reference') ?></th>
                            <th><?= lang('paid_by') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th><?= lang('created_by') ?></th>
                            <th><?= lang('type') ?></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($payments as $payment) {
                            $parcela = $this->site->getParcelaOneByFatura($payment->fatura);
                            ?>
                            <tr <?= $payment->status == 'ESTORNO' ? 'class="warning" style="text-decoration: line-through;"' : ''; ?>>
                                <td style="text-align: left;"><?= $payment->biller; ?></td>
                                <td><?= $this->sma->hrld($payment->date) ?><?= $payment->motivo_estorno; ?></td>
                                <td><?= $payment->reference_no.'<br/>Parcela paga '.$parcela->numeroparcela.'/'.$parcela->totalParcelas; ?></td>
                                <td><?= lang($payment->paid_by);
                                    if ($payment->paid_by == 'gift_card' || $payment->paid_by == 'CC') {
                                        echo ' (' . $payment->cc_no . ')';
                                    } elseif ($payment->paid_by == 'Cheque') {
                                        echo ' (' . $payment->cheque_no . ')';
                                    }
                                    ?>
                                    <?php if ($payment->note){?>
                                        <br/><small><?=$payment->note;?></small>
                                    <?php } ?>
                                </td>
                                <td><?= $this->sma->formatMoney($payment->amount + $payment->taxa) . ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_blank"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                                <td><?= lang($payment->type); ?></td>
                                <td style="text-align: center;"><a href="<?php echo base_url();?>/sales/payment_note/<?php echo $payment->payment_id;?>" target="<?php echo $payment->payment_id;?>"><span class="fa fa-eye"></span></a></td>
                                <td style="text-align: center;"><a href="<?php echo base_url();?>/sales/payment_pdf/<?php echo $payment->payment_id;?>"><span class="fa fa-download"></span></a></td>
                                <?php if(!$payment->status == 'ESTORNO') {?>
                                    <td style="text-align: center;"><a href="<?= site_url('financeiro/motivoEstorno/' . $payment->payment_id) ?>" data-toggle="modal" data-target="#myModal4"><span class="fa fa-trash-o"></span></a></td>
                                <?php } else { ?>
                                    <td style="text-align: center;">-</td>
                                <?php }?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($fechamento->note || $fechamento->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($fechamento->note); ?></div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-6 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($fechamento->created_at); ?>
                        </p>
                        <?php if ($fechamento->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($fechamento->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="buttons">
                    <div class="btn-group btn-group-justified">
                        <div class="btn-group">
                            <a href="<?= site_url('commissions/report/' . $fechamento->id.'/0/0/portrait') ?>" target="_blank" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= site_url('commissions/montar_fechamento/' . $fechamento->id) ?>" class="tip btn btn-warning sledit" title="<?= lang('edit') ?>">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                            </a>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
