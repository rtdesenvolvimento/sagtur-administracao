<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 5px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
            background-color: #f4f4f4;
        }

        p {
            line-height: 25px;
        }

        b {
            font-weight: bold;
        }
    </style>
<body>

<div class="container">

    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: center;width: 30%;border: 1px solid #ccc;padding: 10px;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
            </td>
            <td style="text-align: left;width: 71%;border: 1px solid #ccc;padding: 10px;">
                <h2><?=$Settings->site_name;?></h2>
                <h4 style="font-size: 12px;">
                    <?php
                    echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state;
                    echo '<p>';
                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != '-' && $biller->cf1 != '') {
                        echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                    }
                    if ($biller->cf2 != '-' && $biller->cf2 != '') {
                        echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                    }
                    if ($biller->cf3 != '-' && $biller->cf3 != '') {
                        echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                    }
                    if ($biller->cf4 != '-' && $biller->cf4 != '') {
                        echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                    }
                    if ($biller->cf5 != '-' && $biller->cf5 != '') {
                        echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                    }
                    if ($biller->cf6 != '-' && $biller->cf6 != '') {
                        echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                    }
                    echo '</p>';
                    echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                    ?>

                    <br/>Título: <b><?=$fechamento->title;?></b><br/>
                    Ref: <b><?=$fechamento->reference_no;?></b><br/>
                    Competência: <b><?=$this->sma->hrsd($fechamento->dt_competencia);?></b><br/>
                    <?= lang("status"); ?>: <b><?= lang($fechamento->status); ?></b><br/>
                </h4>
            </td>
        </tr>
        </thead>
    </table>

    <h3 style="text-align: left;">Vendedor: <?=$biller->id;?> - <?=$biller->name;?></h3>

    <div style="text-align: left;background-color: #c9c4c8;padding: 4px;font-weight: bold;font-size: 1.3rem;margin-bottom: 2px;">COMISSÕES</div>

    <table style="width: 100%;border-collapse:collapse;">
        <thead>
            <tr>
                <th><?= lang("nº"); ?></th>
                <th><?= lang("date"); ?></th>
                <th><?= lang("ref"); ?></th>
                <th style="text-align: left;"><?= lang("customer"); ?></th>
                <th style="text-align: left;"><?= lang("product"); ?></th>
                <th style="text-align: right;"><?= lang("base_value"); ?></th>
                <th style="text-align: right;"><?= lang("%"); ?></th>
                <th style="text-align: right;"><?= lang("commission"); ?></th>
                <th style="text-align: right;"><?= lang("due"); ?></th>
                <th style="text-align: right;"><?= lang("paid"); ?></th>
                <th style="text-align: center;"><?= lang("status"); ?></th>
            </tr>
        </thead>
        <tbody>
        <?php
        $isColorBackground = false;
        $totalPagamento = 0;

        if(!empty($itensFechamento)) {?>
            <?php
            $contador = 1;
            foreach ($itensFechamento as $item) {

                $background = "#eee";
                $totalPagamento += $item->commission;

                if ($isColorBackground) {
                    $background = "#eee";
                    $isColorBackground = false;
                } else {
                    $background = "#ffffff";
                    $isColorBackground = true;
                }

                ?>
                <tr style="background: <?=$background;?>">
                    <td><?= $contador; ?></td>
                    <td style="width: 14%;text-align: center;"><?= $this->sma->hrld($item->date); ?></td>
                    <td><?= $item->reference_no; ?></td>
                    <td><?= $item->customer; ?></td>
                    <td><?= $item->product_name; ?></td>
                    <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($item->base_value); ?></td>
                    <td style="text-align: right;"><?= $item->commission_percentage.'%'; ?></td>
                    <td style="text-align: right;"><?= $this->sma->formatMoney($item->commission); ?></td>
                    <td style="text-align: right;"><?= $this->sma->formatMoney(($item->commission - $item->paid)); ?></td>
                    <td style="text-align: right;"><?= $this->sma->formatMoney($item->paid); ?></td>
                    <td style="width: 10%;text-align: center;"><?= lang($item->status); ?></td>
                </tr>
                <?php $contador++; } ?>
        <?php } else { ?>
            <tr>
                <td colspan="9" style="text-align: center;">Nenhuma comissão encontrado</td>
            </tr>
        <?php } ?>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="8" style="text-align: right;font-weight:bold;border-top: 1px solid #0b0b0b;"><?= lang("total_commission"); ?>(<?= $default_currency->code; ?>)</td>
            <td colspan="3" style="text-align: right;font-weight:bold;border-top: 1px solid #0b0b0b;"><?= $this->sma->formatMoney($totalPagamento); ?></td>
        </tfoot>
    </table>


    <?php if (!empty($faturas)){?>
        <div style="text-align: left;background-color: #c9c4c8;padding: 4px;font-weight: bold;font-size: 1.3rem;margin-bottom: 2px;margin-top: 25px;">FATURAS</div>

        <table style="width: 100%;">
            <thead>
            <tr>
                <th style="text-align: center;">Vencimento</th>
                <th style="text-align: center;">Cobrança</th>
                <th style="text-align: right;">Pagar</th>
                <th style="text-align: right;">Recebido</th>
                <th style="text-align: center;">Status</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $totalPagarFatura = 0.00;
            $totalPagoFatura = 0.00;
            foreach ($faturas as $fatura) {
                $parcela = $this->site->getParcelaOneByFatura($fatura->id);
                $totalPagarFatura += $fatura->valorfatura;
                $totalPagoFatura += $fatura->valorpago;

                $background = "#eee";

                if ($isColorBackground) {
                    $background = "#eee";
                    $isColorBackground = false;
                } else {
                    $background = "#ffffff";
                    $isColorBackground = true;
                }

                ?>
                <tr style="background: <?=$background;?>">
                    <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                    <td style="text-align: center;">
                        <?php echo $fatura->tipoCobranca;?>
                        <br/><?php echo $fatura->reference;?>
                    </td>
                    <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                    <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                    <td style="text-align: center;">
                        <?php echo lang($fatura->status);?>
                    </td>
                </tr>
            <?php }?>
            </tbody>
        </table>
        <table style="width: 100%;">
            <tfoot>
            <tr>
                <td colspan="5" style="text-align:right; font-weight:bold;border-top: 1px solid #0b0b0b;"><?= lang('total_amount'); ?>
                    (<?= $default_currency->code; ?>)
                </td>
                <td style="text-align:right; font-weight:bold;border-top: 1px solid #0b0b0b;"><?= $this->sma->formatMoney($totalPagarFatura); ?></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                    (<?= $default_currency->code; ?>)
                </td>
                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($totalPagoFatura); ?></td>
            </tr>
            <tr>
                <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                    (<?= $default_currency->code; ?>)
                </td>
                <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($totalPagarFatura - $totalPagoFatura); ?></td>
            </tr>
            </tfoot>
        </table>
    <?php }?>

    <?php if (!empty($payments)) { ?>
        <div style="text-align: left;background-color: #c9c4c8;padding: 4px;font-weight: bold;font-size: 1.3rem;margin-bottom: 2px;margin-top: 25px;">PAGAMENTOS</div>
        <table style="width: 100%;">
            <thead>
            <tr>
                <th><?= lang('date') ?></th>
                <th><?= lang('payment_reference') ?></th>
                <th><?= lang('paid_by') ?></th>
                <th><?= lang('amount') ?></th>
                <th><?= lang('created_by') ?></th>
                <th><?= lang('type') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($payments as $payment) {
                $parcela = $this->site->getParcelaOneByFatura($payment->fatura);
                ?>
                <tr <?= $payment->status == 'ESTORNO' ? 'class="warning" style="text-decoration: line-through;"' : ''; ?>>
                    <td><?= $this->sma->hrld($payment->date) ?><?= $payment->motivo_estorno; ?></td>
                    <td><?= $payment->reference_no.'<br/>Parcela paga '.$parcela->numeroparcela.'/'.$parcela->totalParcelas; ?></td>
                    <td><?= lang($payment->paid_by);
                        if ($payment->paid_by == 'gift_card' || $payment->paid_by == 'CC') {
                            echo ' (' . $payment->cc_no . ')';
                        } elseif ($payment->paid_by == 'Cheque') {
                            echo ' (' . $payment->cheque_no . ')';
                        }
                        ?>
                        <?php if ($payment->note){?>
                            <br/><small><?=$payment->note;?></small>
                        <?php } ?>
                    </td>
                    <td><?= $this->sma->formatMoney($payment->amount + $payment->taxa) . ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_blank"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                    <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                    <td><?= lang($payment->type); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <div class="row" style="margin-top: 25px;">
        <div class="col-xs-5">
            <?php if ($fechamento->note || $fechamento->note != "") { ?>
                <div class="well well-sm">
                    <p class="bold"><?= lang("note"); ?>:</p>
                    <div><?= $this->sma->decode_html($fechamento->note); ?></div>
                </div>
            <?php } ?>
        </div>
        <div class="col-xs-5 pull-right">
            <table border="" style="width: 100%;border-collapse:collapse;">
                <tr>
                    <td style="width: 100%;"></td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        ________________________________________
                        <br/>Assinatura do Vendedor
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br/>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center;">
                        ________________________________________
                        <br/>Assinatura do Responsável Administrativo
                    </td>
                </tr>
            </table>

        </div>
    </div>

    <?php if ($biller->billing_data) { ?>
        <div class="row" style="margin-top: 25px;">
            <div class="col-xs-12">
                <div class="well well-sm">
                    <p class="bold"><?= lang("billing_data"); ?>:</p>
                    <div style="font-weight: bold;"><?= $this->sma->decode_html($biller->billing_data); ?></div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>

</body>
</html>