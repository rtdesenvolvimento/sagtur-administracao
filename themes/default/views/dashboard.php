<?php
function row_status($x)
{
    if($x == null) {
        return '';
    } else if($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    } else if($x == 'partial' || $x == 'transferring' || $x == 'ordered') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if($x == 'due' || $x == 'returned') {
        return '<div class="text-center"><span class="label label-danger">'.lang($x).'</span></div>';
    } else if ($x == 'cancel') {
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'reembolso'){
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'orcamento') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if ($x == 'lista_espera') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if ($x == 'faturada'){
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    }
}
?>

<?php
$user =  $this->db_model->getUserById($this->session->userdata('user_id'));
?>
<?php if ($Owner || $Admin) { ?>


    <style>
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 20px;
            padding-right: 15px;
            padding-left: 15px;
        }
    </style>
    <div class="row" style="margin-bottom: 15px;">

        <div class="col-sm-12">
            <div id="totalizadore-role" >
                <div class="row">
                    <div class="col-sm-5">
                        <div class="small-box padding1010 bmGreen col-sm-12" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('recebimentos_anual') ?></h4>
                            <i class="fa fa-usd"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"> Total de Recebimentos <small>(R$)</small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoAno);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total a Receber <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalReceberAno);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total  <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoAno+$totalReceberAno);?></div>
                                </div>
                            </div>
                        </div>
                    </div>






                    <div class="col-sm-5">
                        <div class="small-box padding1010 bblue col-sm-12" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('recebimentos_mensal') ?></h4>
                            <i class="fa fa-usd"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"> Total de Recebimentos <small>(R$)</small> </div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total a Receber <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalReceberMes);?></div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;">Total  <small>(R$)</small></div>
                                    <div style="float: right;"><?php echo $this->sma->formatMoney($totalRecebimentoMes+$totalReceberMes);?></div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-2">
                        <div class="small-box padding1010 bpurple col-sm-12" style="color: #ffffff">
                            <h4 class="bold" style="font-size: 17px;"><?= lang('recebimentos_de_hoje') ?></h4>
                            <i class="fa fa-usd"></i>
                            <div class="col-sm-12">
                                <div class="bold">
                                    <div style="float: left;"> Receber Hoje<br/>   </div>
                                    <div style="float: right;    margin-bottom: 20px;"><?php echo $this->sma->formatMoney($aReceberHoje);?></div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



        <!--
        <div class="col-lg-3">
            <div class="box">
                <div class="box-content" style="font-size: 15px;">
                    <span>
                        <i class="fa fa fa-usd"> </i> Falta pagar hoje<br/>
                    </span>
                    <div style="color: #F43E61;font-weight: bold;text-align: right;"> <?php echo $this->sma->formatMoney($aPagarHoje);?></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="box">
                <div class="box-content" style="font-size: 15px;">
                    <span>
                        <i class="fa fa fa-info"> </i> Falta receber no mês de <?php echo $this->sma->dataDeHojePorExtensoRetorno(date('Y-m-d'), '%B');?><br/>
                    </span>
                    <div style="color: #0e731b;font-weight: bold;text-align: right;"> <?php echo $this->sma->formatMoney($aReceberMes);?></div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="box">
                <div class="box-content" style="font-size: 15px;">
                    <span>
                        <i class="fa fa fa-warning"> </i> Falta pagar no mês de <?php echo $this->sma->dataDeHojePorExtensoRetorno(date('Y-m-d'), '%B');?><br/>
                    </span>
                    <div style="color: #F43E61;font-weight: bold;text-align: right;"> <?php echo $this->sma->formatMoney($aPagarMEs);?></div>
                </div>
            </div>
        </div>
        !-->
    </div>
<?php } ?>


<?php if ($Owner || $Admin) { ?>
    <div class="row" style="margin-bottom: 15px;">
        <?php if ($asaasSetting->active){?>
            <div class="col-lg-2">
                <div class="box" style="text-align: center;">
                    <img width="150px" height="85px" height="85px" src="<?= $assets ?>images/asaas.svg">
                    <div class="box-content">
                        Saldo em conta<br/>
                        <span style="color: #4AB858;font-weight: bold;" id="saldo-asaas">
                            <a onclick="verSaldoAsaas();" style="cursor: pointer;">
                                <i class="fa fa-eye"></i> Clique aqui para ver saldo
                            </a>
                        </span>
                        <span id="webhook-asaas" style="display: none;"></span>
                        <span id="webhook-habilitar-asaas" style="display: none;">
                            <a onclick="habilitar_webhook_asaas();" style="cursor: pointer;">
                               <br/> <i class="fa fa-check-circle"></i> Clique aqui para Habilitar a Integração
                            </a>
                        </span>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php }?>

<?php if ($Owner || $Admin) { ?>
    <div class="row" style="margin-bottom: 15px;">

        <?php if ($junoDisponivel != null){?>
            <div class="col-lg-2">
                <div class="box">
                    <img width="150px" height="85px"height="85px" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        Disponível<br/><span style="color: #4AB858;font-weight: bold;"> <?php echo $this->sma->formatMoney($junoDisponivel);?></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="box">
                    <img width="150px" height="85px"height="85px" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        A Liberar<br/> <span style="color: #4AB858;font-weight: bold;"> <?php echo $this->sma->formatMoney($junoLiberar);?></span>
                    </div>
                </div>
            </div>

            <div class="col-lg-2">
                <div class="box">
                    <img width="150px" height="85px"height="85px" src="https://beesweb.com.br/wp-content/uploads/2020/03/integracao-juno-boleto-principal.png">
                    <div class="box-content">
                        Total<br/><span style="color: #4AB858;font-weight: bold;"><?php echo $this->sma->formatMoney($junoTotal);?></span>
                    </div>
                </div>
            </div>
        <?php }?>
    </div>
<?php }?>


<style type="text/css" media="screen">
    .tooltip-inner {
        max-width: 500px;
    }
</style>
<script src="<?= $assets; ?>js/hc/highcharts.js"></script>


<?php if (($Owner || $Admin)) {


    for($i=1;$i<30;$i++) {
        $diasDoMes[] = $i;
    }

    foreach ($relatorioVendasPorDespesas as $month_sale) {
        $months[]       = date('M-Y', strtotime($month_sale->month));

        $msales[]       = $month_sale->sales;
        $mpurchases[]   = $month_sale->purchases;
    }

    foreach ($chatDataFluxoCaixaMes as $month_sale_fcm) {
        $monthsFCM[]       = date('M-Y', strtotime($month_sale_fcm->month));

        $msalesFCM[]       = $month_sale_fcm->sales;
        $mpurchasesFCM[]   = $month_sale_fcm->purchases;

        //$mtax1[] = $month_sale->tax1;
        //$mtax2[] = $month_sale->tax2;
        //$mtax3[] = $month_sale->ptax;
    }

    foreach ($chatDataFluxoCaixaMesQuitadoMes as $month_sale_fcm) {
        $msalesFCMQuitado[]       = $month_sale_fcm->sales;
    }

    foreach ($chatDataFluxoCaixaDebitoMesQuitadoMes as $month_sale_fcm) {
        $msalesFCMQuitadoDebito[]       = $month_sale_fcm->purchases;
    }

    /*
    foreach ($contaReceberAno as $month_sale_fca) {
        $monthsFCA[]       = date('M-Y', strtotime($month_sale_fca->dtvencimento));

        $msalesFCA[]       = $month_sale_fca->valor;
    }

    foreach ($contaPagarAno as $cpAno) {
        $contaPagarAnoArr[]       = $cpAno->valor;
    }
    */

    foreach ($chatDataFluxoCaixaDia as $month_sale_fcd) {
        $monthsFCD[]       = date('d-M', strtotime($month_sale_fcd->month));

        $msalesFCD[]       = $month_sale_fcd->sales;
        $mpurchasesFCD[]   = $month_sale_fcd->purchases;
    }

    /*
    foreach ($vVendedorPorCategoria as $vPorCategoria) {
        $vPorCategoriaCat[]       = $vPorCategoria->categoria;
    }*/
    ?>
    <div class="box" style="margin-bottom: 15px;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('overview_chart'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <p class="introtext"><?php echo lang('overview_chart_heading'); ?></p>

                    <div id="ov-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box" style="margin-bottom: 15px;display: none;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('grafico_fluxo_caixa_diario'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="fcd-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="box" style="margin-bottom: 15px;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('faturamento_por_dia'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="faturamento-dia-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>

    <div class="box" style="margin-bottom: 15px;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('grafico_fluxo_caixa_mensal'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="fcm-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>


    <div class="box" style="margin-bottom: 15px;display: none;">
        <div class="box-header">
            <h2 class="blue"><i class="fa-fw fa fa-bar-chart-o"></i><?= lang('grafico_fluxo_caixa_anual'); ?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12">
                    <div id="fca-chart" style="width:100%; height:450px;"></div>
                    <p class="text-center"><?= lang("chart_lable_toggle"); ?></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<div class="row" style="margin-bottom: 15px;">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-tasks"></i> <?= lang('latest_five') ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="dbTab" class="nav nav-tabs">
                            <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                                <li class=""><a href="#sales"><?= lang('sales_faturada') ?></a></li>
                                <!--
                                <li class=""><a href="#cobrancas"><?= lang('ultimas_cobrancas') ?></a></li>
                                <li class=""><a href="#cobrancas-pagar"><?= lang('ultimas_cobrancas_pagas') ?></a></li>
                                <li class=""><a href="#cobrancas-atrasadas"><?= lang('ultimas_cobrancas_atrasadas') ?></a></li>
                                !-->

                            <?php } if ($Owner || $Admin || $GP['quotes-index']) { ?>
                                <!--<li class=""><a href="#quotes"><?= lang('quotes') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['purchases-index']) { ?>
                               <!-- <li class=""><a href="#purchases"><?= lang('purchases') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['transfers-index']) { ?>
                                <!--<li class=""><a href="#transfers"><?= lang('transfers') ?></a></li>!-->
                            <?php } if ($Owner || $Admin || $GP['customers-index']) { ?>
                                <li class=""><a href="#customers"><?= lang('customers') ?></a></li>
                            <?php } if ($Owner || $Admin || $GP['suppliers-index']) { ?>
                            <?php } ?>
                        </ul>
                        <div class="tab-content">
                            <?php if ($Owner || $Admin || $GP['sales-index']) { ?>

                                <div id="sales" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesFaturadas)) {
                                                        $r = 1;
                                                        foreach ($salesFaturadas as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sales-cotacao" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesOrcamentos)) {
                                                        $r = 1;
                                                        foreach ($salesOrcamentos as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sales-espera" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <span style="color: #f0a5a4;">Atenção: Esta listagem mostra os clientes pagantes e seus dependentes, observem que o código da venda pode ser o mesmo.</span>
                                                <table id="sales-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("date"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("reference_no"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("customer"); ?></th>
                                                        <th style="text-align: left;"><?= $this->lang->line("product"); ?></th>
                                                        <th style="text-align: center;"><?= $this->lang->line("status"); ?></th>
                                                        <th ><?= $this->lang->line("total"); ?></th>
                                                        <th><?= $this->lang->line("payment_status"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($salesListaEspera)) {
                                                        $r = 1;
                                                        foreach ($salesListaEspera as $order) {
                                                            echo '<tr id="' . $order->id . '" class="' . ($order->pos ? "receipt_link" : "invoice_link") . '"><td>' . $r . '</td>
                                                            <td>' . $this->sma->hrld($order->date) . '</td>
                                                            <td>' . $order->reference_no . '</td>
                                                            <td>' . $order->customer . '</td>
                                                            <td>' . $order->product_name . '</td>
                                                            <td>' . row_status($order->sale_status) . '</td>
                                                            <td class="text-right">' . $this->sma->formatMoney($order->grand_total) . '</td>
                                                            <td>' . row_status($order->payment_status) . '</td>
                                                        </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="7"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } if ($Owner || $Admin || $GP['customers-index']) { ?>
                                <div id="customers" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table id="customers-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("company"); ?></th>
                                                        <th><?= $this->lang->line("name"); ?></th>
                                                        <th><?= $this->lang->line("email"); ?></th>
                                                        <th><?= $this->lang->line("phone"); ?></th>
                                                        <th><?= $this->lang->line("address"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($customers)) {
                                                        $r = 1;
                                                        foreach ($customers as $customer) {
                                                            echo '<tr id="' . $customer->id . '" class="customer_link pointer"><td>' . $r . '</td>
                                                                    <td>' . $customer->company . '</td>
                                                                    <td>' . $customer->name . '</td>
                                                                    <td>' . $customer->email . '</td>
                                                                    <td>' . $customer->phone . '</td>
                                                                    <td>' . $customer->address . '</td>
                                                                </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="6"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } if ($Owner || $Admin || $GP['suppliers-index']) { ?>

                                <div id="suppliers" class="tab-pane fade">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="table-responsive">
                                                <table id="suppliers-tbl" cellpadding="0" cellspacing="0" border="0"
                                                       class="table table-bordered table-hover table-striped"
                                                       style="margin-bottom: 0;">
                                                    <thead>
                                                    <tr>
                                                        <th style="width:30px !important;">#</th>
                                                        <th><?= $this->lang->line("company"); ?></th>
                                                        <th><?= $this->lang->line("name"); ?></th>
                                                        <th><?= $this->lang->line("email"); ?></th>
                                                        <th><?= $this->lang->line("phone"); ?></th>
                                                        <th><?= $this->lang->line("address"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (!empty($suppliers)) {
                                                        $r = 1;
                                                        foreach ($suppliers as $supplier) {
                                                            echo '<tr id="' . $supplier->id . '" class="supplier_link pointer"><td>' . $r . '</td>
                                                                    <td>' . $supplier->company . '</td>
                                                                    <td>' . $supplier->name . '</td>
                                                                    <td>' . $supplier->email . '</td>
                                                                    <td>' . $supplier->phone . '</td>
                                                                    <td>' . $supplier->address . '</td>
                                                                </tr>';
                                                            $r++;
                                                        }
                                                    } else { ?>
                                                        <tr>
                                                            <td colspan="6"
                                                                class="dataTables_empty"><?= lang('no_data_available') ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.order').click(function () {
            window.location.href = '<?=site_url()?>orders/view/' + $(this).attr('id') + '#comments';
        });
        $('.invoice').click(function () {
            window.location.href = '<?=site_url()?>orders/view/' + $(this).attr('id');
        });
        $('.quote').click(function () {
            window.location.href = '<?=site_url()?>quotes/view/' + $(this).attr('id');
        });
    });
</script>

<?php if (($Owner || $Admin)) { ?>


    <script type="text/javascript">
        $(function () {


            <?php

            $date = new DateTime(date('Y-m-d'));
            $firstDay = $date->modify('first day of this month')->format('Y-m-d');
            $lastDay = $date->modify('last day of this month')->format('Y-m-d');

            $dias = $this->db->query("select t.data From sma_tempo t where t.data >= '".$firstDay."' and t.data <= '" . $lastDay . "' GROUP by data");
            if ($dias->num_rows() > 0) {
                foreach (($dias->result()) as $row) {
                    $dataDias[] = $row;
                }
            }

            foreach ($dataDias as $dia) {
                $diaArray[] = $this->sma->hrsd($dia->data);
            }

            $previsto = $this->db->query("select t.data, sum(f.valorfatura) as totalPagar, sum(f.valorpago) as totalPago From sma_tempo t left outer join sma_fatura f on f.dtvencimento = t.data and f.status in ('ABERTA', 'PARCIAL', 'QUITADA') and f.tipooperacao = 'CREDITO' where t.data >= '" . $firstDay. "' and t.data <= '" . $lastDay . "'  GROUP by t.data");
            if ($previsto->num_rows() > 0) {
                foreach (($previsto->result()) as $row) {
                    $dataPrevisto[] = (double) $row->totalPagar;
                }
            }

            $realizado = $this->db->query("select t.data, sum(f.valorpago) as totalPago From sma_tempo t left outer join sma_fatura f on f.dtvencimento = t.data and f.status in ('PARCIAL', 'QUITADA') and f.tipooperacao = 'CREDITO' where t.data >= '" . $firstDay. "' and t.data <= '" . $lastDay . "'  GROUP by t.data");
            if ($realizado->num_rows() > 0) {
                foreach (($realizado->result()) as $row) {
                    $dataRealizado[] = (double) $row->totalPago;
                }
            }
            ?>

            // Dados de faturas
            var data = [
                {type: 'areaspline', name: 'Previsto',  data: <?=json_encode($dataPrevisto);?>},
                {type: 'areaspline', name: 'Realizado', data: <?=json_encode($dataRealizado);?>}
            ];

            // Cria o gráfico
            $('#faturamento-dia-chart').highcharts({
                chart: {type: 'line'},
                title: {text: 'Faturamento por dia do mês'},
                xAxis: {
                    categories: <?=json_encode($diaArray);?>
                },
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#5cb85c', '#e68900'],
                yAxis: {
                    title: {
                        text: 'Valor (R$)'
                    }
                },
                series: data
            });

            $('#ov-chart').highcharts({
                chart: {},
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($months); ?>},
                yAxis: {min: 0, title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#428BCA' , '#f43e61'],
                series: [
                    {
                        type: 'areaspline',
                        name: '<?= lang("sales"); ?>',
                        data: [<?php
                            echo implode(', ', $msales);
                            ?>]
                    }
                    ]
            });

            $('#fcm-chart').highcharts({
                chart: {},
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($monthsFCM); ?>},
                yAxis: {  title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#428bca', '#90ed7d' , '#a94442', '#f43e61'],
                series: [
                    {
                        type: 'column',
                        name: '<?= lang("Contas a Receber"); ?>',
                        data: [<?php
                            echo implode(', ', $msalesFCM);
                            ?>]
                    },

                    {
                        type: 'column',
                        name: '<?= lang("Contas a Receber (Realizado)"); ?>',
                        data: [<?php
                            echo implode(', ', $msalesFCMQuitado);
                            ?>],
                        marker: {
                            lineWidth: 2,
                            states: {
                                hover: {
                                    lineWidth: 4
                                }
                            },
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    },

                ]
            });

            /*
            $('#fca-chart').highcharts({
                chart: {},
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($monthsFCA); ?>},
                yAxis: {  title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#4ab858' , '#f43e61'],
                series: [
                    {
                        type: 'column',
                        name: '<?= lang("recebimentos"); ?>',
                        data: [<?php
                            echo implode(', ', $msalesFCA);
                            ?>]
                    }, {
                        type: 'column',
                        name: '<?= lang("pagamentos"); ?>',
                        data: [<?php
                            echo implode(', ', $contaPagarAnoArr);
                            ?>],
                        marker: {
                            lineWidth: 2,
                            states: {
                                hover: {
                                    lineWidth: 4
                                }
                            },
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    } ]
            });*/

            $('#fcd-chart').highcharts({
                chart: {},
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($monthsFCD); ?>},
                yAxis: {  title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                colors: ['#4ab858' , '#f43e61'],
                series: [
                    {
                        type: 'line',
                        name: '<?= lang("recebimentos"); ?>',
                        data: [<?php
                            echo implode(', ', $msalesFCD);
                            ?>]
                    }, {
                        type: 'line',
                        name: '<?= lang("pagamentos"); ?>',
                        data: [<?php
                            echo implode(', ', $mpurchasesFCD);
                            ?>],
                        marker: {
                            lineWidth: 2,
                            states: {
                                hover: {
                                    lineWidth: 4
                                }
                            },
                            lineColor: Highcharts.getOptions().colors[3],
                            fillColor: 'white'
                        }
                    } ]
            });

        });



    </script>


    <script type="text/javascript">
        $(function () {
            $('#lmbschart').highcharts({
                chart: {type: 'column'},
                title: {text: ''},
                credits: {enabled: false},
                xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                yAxis: {min: 0, title: {text: ''}},
                legend: {enabled: false},
                series: [{
                    name: '<?=lang('sold');?>',
                    data: [<?php
                        foreach ($lmbs as $r) {
                            if($r->SoldQty > 0) {
                                $nomeProduto = str_replace("'", "", $r->name);
                                echo "['".$nomeProduto."', ".$r->SoldQty."],";
                            }
                        }
                        ?>],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        y: -25,
                        style: {fontSize: '12px'}
                    }
                }]
            });
            $('#bschart').highcharts({
                chart: {type: 'column'},
                title: {text: ''},
                credits: {enabled: false},
                xAxis: {type: 'category', labels: {rotation: -60, style: {fontSize: '13px'}}},
                yAxis: {min: 0, title: {text: ''}},
                legend: {enabled: false},
                series: [{
                    name: '<?=lang('sold');?>',
                    data: [<?php
                        foreach ($bs as $r) {
                            if($r->SoldQty > 0) {
                                $nomeProduto = str_replace("'", "", $r->name);
                                echo "['".$nomeProduto."', ".$r->SoldQty."],";
                            }
                        }
                        ?>],
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'right',
                        y: -25,
                        style: {fontSize: '12px'}
                    }
                }]
            });

        });
    </script>
    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><i
                                class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller'), ' (' . date('M-Y', time()) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="bschart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue"><i
                                class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="lmbschart" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <script type="text/javascript">

        $(function () {

            $('#pdMeioDivulgacaoMes').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pMeioDivulgacaoMes as $pcadD){?>
                        ['<?php echo $pcadD->name ; ?>', <?php echo $pcadD->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });


            $('#pdMeioDivulgacaoPassado').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pMeioDivulgacaoMesPassado as $pcadDP){?>
                        ['<?php echo $pcadDP->name ; ?>', <?php echo $pcadDP->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });

        });
    </script>




    <script type="text/javascript">

        $(function () {

            $('#pdTipCobrancaMes').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pTipoCobrancaMes as $pcadT){?>
                        ['<?php echo $pcadT->name ; ?>', <?php echo $pcadT->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });


            $('#pdTipoCobrancaPassado').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pTipoCobrancaMesPassado as $pcadTP){?>
                        ['<?php echo $pcadTP->name ; ?>', <?php echo $pcadTP->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });

        });
    </script>

    <script type="text/javascript">

        $(function () {

            $('#pdCategoriaMes').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pcadMes as $pcad){?>
                            ['<?php echo $pcad->name ; ?>', <?php echo $pcad->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });


            $('#pdCategoriaMesPassado').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($pcadMesPassado as $pcadP){?>
                        ['<?php echo $pcadP->name ; ?>', <?php echo $pcadP->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });

        });
    </script>


    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_divulgacao'), ' (' . date('M-Y', time()) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdMeioDivulgacaoMes" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_divulgacao') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdMeioDivulgacaoPassado" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_tipo_cobranca'), ' (' . date('M-Y', time()) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdTipCobrancaMes" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_tipo_cobranca') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdTipoCobrancaPassado" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_by_category'), ' (' . date('M-Y', time()) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdCategoriaMes" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_by_category') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pdCategoriaMesPassado" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






    <script type="text/javascript">

        $(function () {

            $('#vVendedorMes').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '12px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;font-size: 12px;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($vVendedorMes as $vVendaVendedorMes){?>
                        ['<?php echo $vVendaVendedorMes->name ; ?>', <?php echo $vVendaVendedorMes->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });


            $('#vVendedorMesAnterior').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false
                },
                title: {text: ''},
                credits: {enabled: false},
                tooltip: {
                    formatter: function () {
                        return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                    },
                    followPointer: true,
                    useHTML: true,
                    borderWidth: 0,
                    shadow: false,
                    valueDecimals: site.settings.decimals,
                    style: {fontSize: '12px', padding: '0', color: '#000000'}
                },
                plotOptions: {
                    pie: {
                        dataLabels: {
                            enabled: true,
                            formatter: function () {
                                return '<h3 style="margin:-15px 0 0 0;font-size: 12px;"><b>' + this.point.name + '</b>:<br><b> ' + currencyFormat(this.y) + '</b></h3>';
                            },
                            useHTML: true
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '<?php echo $this->lang->line("stock_value"); ?>',
                    data: [
                        <?php foreach ($vVendedorMesAnterior as $vVendaVendedorMesAnt){?>
                        ['<?php echo $vVendaVendedorMesAnt->name ; ?>', <?php echo $vVendaVendedorMesAnt->subtotal; ?>],
                        <?php } ?>
                    ]

                }]
            });

            /*
            $('#vVendedorPorCategoria').highcharts({
                chart: {
                    type: 'column',
                },
                credits: {enabled: false},
                title: {text: ''},
                xAxis: {categories: <?= json_encode($vPorCategoriaCat); ?>},
                yAxis: {min: 0, title: ""},
                tooltip: {
                    shared: true,
                    followPointer: true,
                    formatter: function () {
                        if (this.key) {
                            return '<div class="tooltip-inner hc-tip" style="margin-bottom:0;">' + this.key + '<br><strong>' + currencyFormat(this.y) + '</strong> (' + formatNumber(this.percentage) + '%)';
                        } else {
                            var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table table-striped"  style="margin-bottom:0;">';
                            $.each(this.points, function () {
                                s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                    currencyFormat(this.y) + '</b></td></tr>';
                            });
                            s += '</table></div>';
                            return s;
                        }
                    },
                    useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                    style: {fontSize: '14px', padding: '0', color: '#000000'}
                },
               // colors: ['#4ab858' , '#f43e61'],
                series: [

                    <?php foreach ($vVendedorPorCategoria as $vVendaVendedorMes){?>
                        {
                            name: '<?php echo $vVendaVendedorMes->name; ?>',
                            data: [ <?php echo rand(0, 100);?>, <?php echo rand(0, 100);?>, <?php echo rand(0, 100);?>],
                        },
                    <?php } ?>

                ],
            });
            */

        });


        function verSaldoAsaas() {
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/saldoAsaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.retorno) {
                        $('#saldo-asaas').html(retorno.saldo);

                        consultar_webhook_cobranca_asaas();
                    }
                }
            });
        }

        function consultar_webhook_cobranca_asaas() {
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/consultar_webhook_cobranca_asaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {

                    if (retorno != null) {
                        $('#webhook-asaas').show();

                        if (retorno.interrupted) {
                            $('#webhook-asaas').html('<br/>INTEGRAÇÃO: DESATIVADO');
                            $('#webhook-asaas').css('color', 'red');
                            $('#webhook-habilitar-asaas').show();
                        } else {
                            $('#webhook-asaas').html('<br/>INTEGRAÇÃO: ATIVO');
                            $('#webhook-asaas').css('color', '#4ab858');
                            $('#webhook-habilitar-asaas').hide();
                        }
                    }
                }
            });
        }

        function habilitar_webhook_asaas() {
            $('#webhook-habilitar-asaas').html('');
            $.ajax({
                type: "get",
                url: site.base_url + "welcome/habilitar_webhook_asaas",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {
                    if (retorno.errors.length) {
                        for (var i = 0; i < retorno.errors.length; i++) {
                            var errorDescription = retorno.errors[i].description;
                            $('#webhook-habilitar-asaas').append('<br/>' + errorDescription);
                            $('#webhook-habilitar-asaas').css('color', 'red');
                        }
                    } else {
                        location.reload();
                    }
                }
            });
        }
    </script>

    <div class="row" style="margin-bottom: 15px;">
        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_biller'), ' (' . date('M-Y', time()) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="vVendedorMes" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller_biller') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="vVendedorMesAnterior" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--
        <div class="col-sm-12" style="display: none;">
            <div class="box">
                <div class="box-header">
                    <h2 class="blue">
                        <i class="fa-fw fa fa-bar-chart-o"></i><?= lang('best_seller') . ' (' . date('M-Y', strtotime('-1 month')) . ')'; ?>
                    </h2>
                </div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="vVendedorPorCategoria" style="width:100%; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        !-->
    </div>
<?php } ?>