<style>
    .form-control_custom {
        display: block;
        width: 100%;
        height: 25px;
        padding: 0px 0px;
        font-size: 11px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .form-control_data {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>
<?php
if (!empty($transportes)) {
    foreach ($transportes as $transporte) {
        $varsTransporte[] = addslashes($transporte->name);
    }
} else {
    $varsTransporte = array();
}
?>
<script type="text/javascript">
    $(document).ready(function () {

        $('.estoque_hospedagem').css('display', 'none');

        $('#controle_estoque_hospedagem').on('ifChecked', function (e) {
            $('.estoque_hospedagem').css('display', '');
        });

        $('#controle_estoque_hospedagem').on('ifUnchecked', function (e) {
            $('.estoque_hospedagem').css('display', 'none');
        });

        $('#isComHospedagem').on('ifChanged', function (e) {
            if ($(this).val() === '1') {
                $('#isHospedagem').iCheck('check');
                $('#isValorPorFaixaEtaria').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').show();
            } else {
                $('#isValorPorFaixaEtaria').iCheck('check');
                $('#isHospedagem').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').hide();
            }
        });

        $('#isSemHospedagem').on('ifChanged', function (e) {
            if ($(this).val() === '1') {
                $('#isHospedagem').iCheck('check');
                $('#isValorPorFaixaEtaria').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').show();
            } else {
                $('#isValorPorFaixaEtaria').iCheck('check');
                $('#isHospedagem').iCheck('uncheck');
                $('#div_controle_estoque_hospedagem').hide();
            }
        });

        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
		
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= site_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                data: scdata
                            });
                        } else {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                                placeholder: "<?= lang('no_subcategory') ?>",
                                data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
		
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });

        $('.qtd_pessoas_hospedagem').change(function(event){
            let qtdQuartos = $(this).val();
            let tipoQuartoId = $(this).attr('tipo_quarto');
            let qtdAcomodacao = $(this).attr('qtdacomodacao');

            $('#qtd_pessoas_hospedagem'+tipoQuartoId).val(qtdQuartos*qtdAcomodacao);

            total_hospedagem();
        });
    });

    function total_hospedagem() {

        let total_pessoas_hospedagem = 0;
        let total_quartos = 0;

        $($('.total_pessoas_hospedagem')).each(function( index, pessoas ) {
            total_pessoas_hospedagem += parseInt($(this).val());
        });

        $('.qtd_pessoas_hospedagem').each(function(index, quartos){
            total_quartos += parseInt($(this).val());
        });

        $('#total_quartos').html(total_quartos);
        $('#total_pessoas_hospedagem').html(total_pessoas_hospedagem);
    }
</script>

<?php echo form_open_multipart("tours/add", array('data-toggle' => 'validator', 'role' => 'form')) ?>
<ul id="myTab" class="nav nav-tabs" style="text-align: center">
    <li class=""><a href="#abageral" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('detalhes_do_pacote') ?></a></li>
    <li class="" id="tbValorFaixaEtaria"><a href="#valores" class="tab-grey"><i class="fa fa-users" style="font-size: 20px;"></i><br/><?= lang('valor_por_faixa_etaria') ?></a></li>
    <li class="" id="tbTransporte" style="display: none;"><a href="#transporte" class="tab-grey"><i class="fa fa-bus" style="font-size: 20px;"></i><br/><?= lang('tb_transporte') ?></a></li>
    <li class="" id="tbEmbarques" style="display: none;"><a href="#embarque" class="tab-grey"><i class="fa fa-exchange" style="font-size: 20px;"></i><br/><?= lang('embarque') ?></a></li>
    <li class="" id="tbServicosAdicionais" style="display: none;"><a href="#servicosopcionais" class="tab-grey"><i class="fa fa-plus" style="font-size: 20px;"></i><br/><?= lang('tb_adicionais') ?></a></li>
    <li class="" id="tbIntegracaoSite"><a href="#site" class="tab-grey"><i class="fa fa-sitemap" style="font-size: 20px;"></i><br/><?= lang('integracao_site') ?></a></li>
    <li class="" id="tbComissao" style="display: none;"><a href="#comissao" class="tab-grey"><i class="fa fa-money" style="font-size: 20px;"></i><br/><?= lang('comissao') ?></a></li>
    <li class="" id="tbTaxasComissao" style="display: none;"><a href="#taxasPagamento" class="tab-grey"><i class="fa fa-usd" style="font-size: 20px;"></i><br/><?= lang('taxas_comissão') ?></a></li>
    <li id="tbDatas"><a href="#datas" class="tab-grey"><i class="fa fa-calendar" style="font-size: 20px;"></i><br/><?= lang('programacao_datas') ?></a></li>
    <li class=""><a href="#" id="save" class="tab-grey" style="background: #3c763d;color: #F0F0F0"><i class="fa fa-save" style="font-size: 20px;"></i><br/><?= lang('save') ?></a></li>
</ul>
<div class="tab-content">
    <!--Detalhes do pacote !-->
    <div id="abageral" class="tab-pane fade in">
        <div class="box">
            <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-map-signs"></i><?= lang('add_tour'); ?></h2></div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6" style="display: none;">
                            <div class="form-group" id="div_status">
                                <?= lang("situacao_pacote", "status") ?>
                                <?php
                                $opts = array(
                                    'Confirmado' => lang('status_confirmado_para_venda'),
                                    'Montando' => lang('status_montando_pacote'),
                                    'Executado' => lang('status_viagem_executada') ,
                                    'Cancelado' => lang('status_viagem_cancelada')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <div class="form-group all" id="div_category">
                                <?= lang("destino", "destino") ?>
                                <?php
                                $dn[""] = lang("select") . ' ' . lang("local_embarque");
                                foreach ($destinos as $destino) {
                                    $dn[$destino->id] =  $destino->name;
                                }
                                echo form_dropdown('destino', $dn, (isset($_POST['destino']) ? $_POST['destino'] : ''), 'id="destino" required="required" name="local_embarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-4" style="display: none;">
                            <div class="form-group all">
                                <?= lang("subcategory", "subcategory") ?>
                                <div class="controls" id="subcat_data"> <?php
                                    echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . lang("select_category_to_load") . '"');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?= lang("nome_pacote", "name") ?>
                                <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control" id="name" required="required"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('configuracoes_cadastro_produto');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isTransporteTuristico', '1', FALSE, 'id="isTransporteTuristico"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_transporte_turistico'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isEmbarque', '1', FALSE, 'id="isEmbarque"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_embarque'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isServicosAdicionais', '1', FALSE, 'id="isServicosAdicionais"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_servicos_adicionais'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('disponivel_para_comercializacao');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('publicar_ativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('active', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('desativar_produto'); ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-12">
                            <div class="form-group all" id="div_category">
                                <?= lang("category", "category") ?>
                                <?php
                                $cat[''] = "";
                                foreach ($categories as $category)$cat[$category->id] = $category->name;
                                echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ($product ? $product->category_id : '1')), 'class="form-control select" id="category" required="required" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('modalidade_para_venda');?></h2>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('enviar_site', '1', TRUE, 'id="enviar_site"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_reservas_online'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_reservas_online'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isComissao', '1', FALSE, 'id="isComissao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_comissao'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permiteVendaMenorIdade', '1', FALSE, 'id="permiteVendaMenorIdade"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_vender_para_menor'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_vender_para_menor'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permiteVendaClienteDuplicidade', '1', FALSE, 'id="permiteVendaClienteDuplicidade"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permite_venda_com_cliente_duplicidade'); ?></label> <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_permite_venda_com_cliente_duplicidade'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('permitirListaEmpera', '1', FALSE, 'id="permitirListaEmpera"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('permitir_lista_de_espera'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('apenas_cotacao', '0', ($product ? $product->apenas_cotacao : FALSE), 'id="apenas_cotacao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('produto_disponivel_apenas_para_orcamento'); ?></label>  <i class="fa fa-info-circle" style="cursor: help" title="<?= lang('info_produto_disponivel_apenas_para_orcamento'); ?>" ></i>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isTaxasComissao', '1', ($product ? $product->isTaxasComissao : ''), 'id="isTaxasComissao"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('configurar_taxas_comissao'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('isServicoOnline', '1', TRUE, 'id="isServicoOnline"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('com_integracao_site'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <h2 style="margin-left: 15px;"><?=lang('definir_captacao_dados');?></h2>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isApenasColetarPagador', '1', TRUE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_dados_de_todos_link'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_radio('isApenasColetarPagador', '0', FALSE, ''); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_dados_de_apenas_pagador'); ?></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="col-md-12">
                                <div class="form-group all">
                                    <?php echo form_checkbox('captarEnderecoLink', '1', FALSE, 'id="captarEnderecoLink"'); ?>
                                    <label for="attributes" class="padding05"><?= lang('captar_endereco_link'); ?></label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-shopping-cart"></i> <?= lang('informacoes_valores_exibicao_loja'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group all">
                                        <?= lang("valor_pacote", "valor_pacote") ?>
                                        <?= form_input('valor_pacote', (isset($_POST['valor_pacote']) ? $_POST['valor_pacote'] : ($product ? $product->valor_pacote : 'A partir de')), 'class="form-control" required="required" id="valor_pacote" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang("simboloMoeda", "simboloMoeda") ?>
                                        <?= form_input('simboloMoeda', (isset($_POST['simboloMoeda']) ? $_POST['simboloMoeda'] : ($product ? $product->simboloMoeda : 'R$')), 'class="form-control" required="required" id="simboloMoeda" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group all">
                                        <?= lang("precoExibicaoSite", "precoExibicaoSite") ?>
                                        <?= form_input('precoExibicaoSite', (isset($_POST['precoExibicaoSite']) ? $_POST['precoExibicaoSite'] : ($product ? $product->precoExibicaoSite : '0.00')), 'class="form-control tip mask_money" required="required" id="precoExibicaoSite" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input type="checkbox" class="checkbox" value="1" name="promotion" id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?>>
                                        <label for="promotion" class="padding05">
                                            <?= lang('promotion'); ?>
                                        </label>
                                    </div>
                                    <div id="promo" style="display:none;">
                                        <div class="well well-sm">
                                            <div class="form-group">
                                                <?= lang('promo_price', 'promo_price'); ?>
                                                <?= form_input('promo_price', set_value('promo_price'), 'class="form-control mask_money tip" id="promo_price"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= lang("alert_stripe", "alert_stripe") ?>
                                        <?= form_input('alert_stripe', (isset($_POST['alert_stripe']) ? $_POST['alert_stripe'] : ($product ? $product->alert_stripe : '')), 'class="form-control" id="alert_stripe" '); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-photo"></i> <?= lang("product_gallery", "product_gallery") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all" id="div_product_details">
                                        <div class="form-group all">
                                            <?= lang("product_image", "product_image") ?>
                                            <input id="product_image" type="file" data-browse-label="<?= lang('browse'); ?>" name="product_image" data-show-upload="false"
                                                   data-show-preview="false" accept="image/*" class="form-control file">
                                        </div>
                                        <div class="form-group all">
                                            <?= lang("product_gallery_images", "images") ?>
                                            <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                                                   data-show-preview="false" class="form-control file" accept="image/*">
                                        </div>
                                        <div id="img-details" ></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-comments"></i> <?= lang("product_details", "product_details") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all" id="div_product_details">
                                        <textarea name="product_details" id="product_details"><?php echo (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-info-circle"></i>  <?= lang("oqueInclui", "oqueInclui") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="oqueInclui" id="oqueInclui"><?php echo (isset($_POST['oqueInclui']) ? $_POST['oqueInclui'] : ($product ? $product->oqueInclui : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-map-signs"></i> <?= lang("itinerario", "itinerario") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="itinerario" id="itinerario"><?php echo (isset($_POST['itinerario']) ? $_POST['itinerario'] : ($product ? $product->itinerario : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-usd"></i><?= lang("valores_condicoes", "valores_condicoes") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <textarea name="valores_condicoes" id="valores_condicoes"><?php echo (isset($_POST['valores_condicoes']) ? $_POST['valores_condicoes'] : ($product ? $product->valores_condicoes : ''));?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("product_details_for_invoice", "details") ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group all">
                                        <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="display: none;">
                        <div class="form-group">
                            <?php echo form_submit('add_product', $this->lang->line("add_product"), 'id="add_product" class="btn btn-primary"'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vagas e Valores !-->
    <div id="valores" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('informacoes_vagas_valores_do_produto'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-user"></i> <?= lang('info_valores_por_faixa_etaria'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table id="attrTableHospedagemFaixaEtaria" class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;">
                                                <thead>
                                                <tr class="active">
                                                    <th  style="text-align: right;width: 2%;"></th>
                                                    <th class="col-md-10" style="text-align: left;"><?= lang('name') ?></th>
                                                    <th class="col-md-2" style="text-align: right;"><?= lang('price') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($valorFaixas as $valoresFaixa) {?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo form_checkbox('ativarTipoFaixaEtaria[]', $valoresFaixa->id, FALSE, ''); ?></td>
                                                        <td>
                                                            <?= form_input('tipoFaixaEtariaValorConfigure[]',   $valoresFaixa->tipo, '', 'hidden') ?>
                                                            <?= form_input('valorFaixaId[]',   $valoresFaixa->id, '', 'hidden') ?>
                                                            <span><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span>
                                                        </td>
                                                        <td class="text-right"><?= form_input('valorFaixaEtariaValorConfigure[]',   '0.00', 'class="form-control tip mask_money"') ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 10px;display: none;">
                                        <a href="<?php echo site_url('system_settings/adicionarValorFaixa'); ?>" data-toggle="modal" data-target="#myModal">
                                        <button type="button" class="btn btn-primary "  style="width: 100%;"><i class="fa fa-plus"></i> <?= lang('incluir_faixa_valor') ?></button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- comissão de vendedores !-->
    <div id="comissao" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-money"></i><?= lang('informacoes_vagas_valores_do_produto'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-dollar"></i> <?= lang('informacoes_comissao'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group" id="ui" style="margin-bottom: 0;">
                                        <?= lang("quantidadePessoasViagem", "quantidadePessoasViagem") ?>
                                        <div class="input-group">
                                            <div class="input-group-addon" style="padding: 2px 5px;">QTD</div>
                                            <?= form_input('quantidadePessoasViagem', (isset($_POST['quantidadePessoasViagem']) ? $_POST['quantidadePessoasViagem'] : ($product ? $this->sma->formatDecimal($product->quantidadePessoasViagem) : '0')), 'class="form-control tip mask_integer" id="quantidadePessoasViagem" required="required" ') ?>
                                        </div>
                                        <span style="color:#F43E61;">OBSERVAÇÃO: Você poderá alterar no Lançamento da Agenda.</span>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group" id="ui" style="margin-bottom: 0;">
                                        <?= lang("price", "price") ?>
                                        <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '0.00')), 'class="form-control tip mask_money" id="price" required="required" ') ?>
                                        <span style="color:#F43E61;">OBSERVAÇÃO: Deixe zero, se for usar o preço por dayuse ou hospedagem.</span>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="div_status">
                                        <?= lang("tipo_comissao", "tipo_comissao") ?>
                                        <?php
                                        $tipoComissao = array(
                                            'comissao_produto' => lang('status_comissao_produto'),
                                            //'comissao_vendedor' => lang('status_comissao_vendedor'),
                                            //'comissao_categoria' => lang('status_comissao_categoria'),
                                        );
                                        echo form_dropdown('tipoComissao', $tipoComissao, (isset($_POST['tipoComissao']) ? $_POST['tipoComissao'] : ($product ? $product->tipoComissao : '')), 'class="form-control" id="tipoComissao" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" id="div_status">
                                        <?= lang("tipoCalculoComissao", "tipo_pagamento_comissao") ?>
                                        <?php
                                        $tipoComissao = array(
                                            '1' => lang('pagamento_comissao_percentual'),
                                            '2' => lang('pagamento_comissao_absoluto'),
                                        );
                                        echo form_dropdown('tipoCalculoComissao', $tipoComissao, (isset($_POST['tipoCalculoComissao']) ? $_POST['tipoCalculoComissao'] : ($product ? $product->tipoCalculoComissao : '')), 'class="form-control" id="tipoCalculoComissao" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" style="margin-bottom: 0;">
                                        <?= lang("comissao", "comissao") ?>
                                        <?= form_input('comissao', (isset($_POST['comissao']) ? $_POST['comissao'] : ($product ? $this->sma->formatDecimal($product->comissao) : '0.00')), 'class="form-control tip mask_money" id="comissao"') ?>
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group">
                                        <?= lang('data_saida', 'data_saida'); ?>
                                        <input type="date" name="data_saida" value="" class="form-control tip" id="data_saida" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('data_retorno', 'data_retorno'); ?>
                                        <input type="date" name="data_retorno" value="" class="form-control tip" id="data_retorno" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('hora_chegada', 'hora_chegada'); ?>
                                        <input type="time" name="hora_chegada" value="" class="form-control tip" id="hora_chegada" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('hora_retorno', 'hora_retorno'); ?>
                                        <input type="time" name="hora_retorno" value="" class="form-control tip" id="hora_retorno" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang("local_retorno", "local_retorno") ?>
                                        <?= form_input('local_retorno', (isset($_POST['local_retorno']) ? $_POST['local_retorno'] : ($product ? $product->local_retorno : '')), 'class="form-control" id="local_retorno" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group all">
                                        <?= lang('tempo_viagem', 'tempo_viagem'); ?>
                                        <input type="number" name="tempo_viagem" value="" class="form-control tip" id="tempo_viagem" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-3" style="display: none;">
                                    <div class="form-group">
                                        <?= lang('duracao_pacote', 'duracao_pacote'); ?>
                                        <input type="number" name="duracao_pacote" value="<?php  (isset($_POST['duracao_pacote']) ? $_POST['duracao_pacote'] : ($product ? $product->duracao_pacote : '1'));?>" class="form-control tip" id="duracao_pacote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Transporte !-->
    <div id="transporte" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('adicionar_transporte_rodoviario'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-6" style="display: none;">
                        <div class="form-group">
                            <?= lang("tipo_transporte", "tipo_transporte") ?>
                            <?php
                            $opts = array(
                                'semrodoviario' => lang('semrodoviario') ,
                                'rodoviario' => lang('rodoviario') ,
                                'sem_aereo' => lang('sem_aereo'),
                                'com_aereo' => lang('com_aereo'),
                                'nao_exibir' => lang('nao_exibir')
                            );
                            echo form_dropdown('tipo_transporte', $opts, (isset($_POST['tipo_transporte']) ? $_POST['tipo_transporte'] : ($product ? $product->tipo_transporte : '')), 'class="form-control" id="tipo_transporte"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-bus"></i><?= lang('informacoes_tipo_transporte_rodoviario'); ?></div>
                            <div class="panel-body">
                                 <div class="control-group table-group">
                                    <div class="controls table-controls">
                                        <div class="table-responsive">
                                            <table id="attrTable" class="table table-bordered table-condensed table-striped table-hover" style="margin-bottom: 20px;">
                                                <thead>
                                                <tr class="active">
                                                    <th style="text-align: center;width: 2%;"></th>
                                                    <th class="col-md-10" style="text-align: left;"><?= lang('transporte') ?></th>
                                                 </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($tiposTransporte as $tipoTransporte) { ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo form_checkbox('ativarTipoTransporte[]', $tipoTransporte->id, FALSE, ''); ?></td>
                                                        <td><input type="hidden" name="tipoTransporte[]" value="<?php echo $tipoTransporte->id;?>"><span><?php echo $tipoTransporte->name;?></span></td>
                                                    </tr>
                                                <?php }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Embarque !-->
    <div id="embarque" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('adicionar_transporte_rodoviario'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-bus"></i><?= lang('info_embarque'); ?></div>
                            <div class="panel-body">
                                <div class="control-group table-group">
                                    <div class="controls table-controls">
                                        <table id="tbLocalEmbarque" class="table items table-striped table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th style="width: 1%;"></th>
                                                <th class="col-md-5" style="text-align: left;"><?= lang("local_embarque");?></th>
                                                <th class="col-md-3" style="text-align: left;"><?= lang("note");?></th>
                                                <th class="col-md-2" style="text-align: left;"><?= lang("data_embarque");?></th>
                                                <th class="col-md-2"><?= lang("hora_embarque"); ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($locais_embarque as $localEmbarque){?>
                                                <tr>
                                                    <td style="text-align: center;"><?php echo form_checkbox('ativoLocalEmbarque[]', $localEmbarque->id , FALSE, ''); ?></td>
                                                    <td><input type="hidden" name="localEmbarque[]" value="<?php echo $localEmbarque->id;?>"/> <?php echo $localEmbarque->name;?></td>
                                                    <td><textarea name="noteEmbarque[]" class="skip"></textarea></td>
                                                    <td><?= form_input('dataEmbarque[]',  '', 'class="form-control tip"', 'date') ?></td>
                                                    <td><?= form_input('horaEmbarque[]',  '', 'class="form-control tip"', 'time') ?></td>
                                                </tr>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Serviços adicionais !-->
    <div id="servicosopcionais" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('informacoes_servicos_adicionais'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="" style="display:block;">
                            <div class="form-group">
                                <?= lang("servicos_opcionais", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                            </div>
                            <div class="control-group table-group">

                                <div class="controls table-controls">
                                    <table id="prTable" class="table items table-striped table-bordered table-condensed table-hover" style="cursor: pointer;">
                                        <thead>
                                        <tr>
                                            <th class="col-md-11" style="text-align: left;"></th>
                                            <th class="col-md-1" style="text-align: center;display: none;"><?= lang("price"); ?></th>
                                            <th class="col-md-1" style="text-align: center;"</th>
                                            <th class="col-md-1 text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Taxas !-->
    <div id="taxasPagamento" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-usd"></i><?= lang('informacoes_taxas_comissoes'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">

                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa fa-usd"> </i> <?= lang('info_taxas_tipo_cobranca'); ?></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="attrTableHospedagemTaxas" class="table table-bordered table-condensed table-striped table-hover" style="margin-bottom: 20px;cursor: pointer;">
                                            <thead>
                                            <tr class="active">
                                                <th class="col-md-1" style="text-align: center;width: 1%;"><?= lang('ativar') ?></th>
                                                <th class="col-md-4" style="text-align: left;display: none;"><?= lang('forma') ?></th>
                                                <th class="col-md-1" style="text-align: center;"><?= lang('parcelas') ?></th>
                                                <th class="col-md-2" style="text-align: left;"><?= lang('tipo') ?></th>
                                                <th class="col-md-2" style="text-align: left;"></th>
                                                <th class="col-md-2" style="text-align: right;"><?= lang('valor') ?></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php foreach ($tiposCobranca as $tipoCobranca) {
                                                $contador = 0;
                                                $tb = '';
                                                ?>
                                                <?php foreach ($condicoesPagamento as $condicaoPagamento){?>
                                                    <?php if ($tipoCobranca->name != $tb){?>
                                                        <tr>
                                                            <td colspan="1"></td>
                                                            <td colspan="5" style="text-align: left;font-size: 16px;"><?php echo $tipoCobranca->name;?></td>
                                                        </tr>
                                                        <?php $tb = $tipoCobranca->name;?>
                                                    <?php }?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo form_checkbox('ativoCondicaoPagamento[]', '1', FALSE, ''); ?></td>
                                                        <td style="display: none;"><input type="hidden" name="tipoCobranca[]" value="<?php echo $tipoCobranca->id;?>" /> <?php echo $tipoCobranca->name;?></td>
                                                        <td style="text-align: center;"><input type="hidden" name="formaPagamento[]" value="<?php echo $condicaoPagamento->id;?>" /><?php echo $condicaoPagamento->name;?></td>
                                                        <td>
                                                            <?php
                                                            $opts = array(
                                                                'acrescimo' => lang('acrescimo'),
                                                                'desconto' => lang('desconto')
                                                            );
                                                            echo form_dropdown('acrescimoDescontoCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $opts = array(
                                                                'absoluto' => lang('absoluto'),
                                                                'percentual' => lang('em_percentual')
                                                            );
                                                            echo form_dropdown('tipoCobrancaCondicaoPagamento[]', $opts, '', 'class="form-control" required="required"');
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?= form_input('precoCondicaoPagamento[]', '0.00', 'class="form-control tip mask_money" required="required"') ?>
                                                        </td>
                                                    </tr>

                                                <?php }?>
                                            <?php }?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Agendamento de datas !-->
    <div id="datas" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('add_agenda'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row" style="margin-top: 15px;">
                    <div class="col-lg-12">
                        <div class="col-md-12">
                            <ul id="myTab" class="nav nav-tabs" style="text-align: center">
                                <li><a href="#domingo" class="tab-grey" id="li_domingo"><?= lang('domingo') ?></a></li>
                                <li><a href="#segunda" class="tab-grey"><?= lang('segunda') ?></a></li>
                                <li><a href="#terca" class="tab-grey"><?= lang('terca') ?></a></li>
                                <li><a href="#quarta" class="tab-grey"><?= lang('quarta') ?></a></li>
                                <li><a href="#quinta" class="tab-grey"><?= lang('quinta') ?></a></li>
                                <li><a href="#sexta" class="tab-grey"><?= lang('sexta') ?></a></li>
                                <li><a href="#sabado" class="tab-grey"><?= lang('sabado') ?></a></li>
                                <li><a href="#datas_pontuais" class="tab-grey"><?= lang('datas_pontuais') ?></a></li>
                                <li><a href="#periodos" id="li_periodos" class="tab-grey"><?= lang('periodos') ?></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="domingo" class="tab-pane fade in">
                                    <div class="row" id="div_domingo">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_domingo[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_domingo[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_domingo[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_domingo', $this->lang->line("add"), 'id="add_date_domingo" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="segunda" class="tab-pane fade">
                                    <div class="row" id="div_segunda">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_segunda[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_segunda[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_segundo[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_segunda', $this->lang->line("add"), 'id="add_date_segunda" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="terca" class="tab-pane fade">
                                    <div class="row" id="div_terca">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_terca[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_terca[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_terca[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_terca', $this->lang->line("add"), 'id="add_date_terca" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="quarta" class="tab-pane fade">
                                    <div class="row" id="div_quarta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_quarta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_quarta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_quarta[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_quarta', $this->lang->line("add"), 'id="add_date_quarta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="quinta" class="tab-pane fade">
                                    <div class="row" id="div_quinta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_quinta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_quinta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_quinta[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_quinta', $this->lang->line("add"), 'id="add_date_quinta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sexta" class="tab-pane fade">
                                    <div class="row" id="div_sexta">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_sexta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_sexta[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_sexta[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_sexta', $this->lang->line("add"), 'id="add_date_sexta" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="sabado" class="tab-pane fade">
                                    <div class="row" id="div_sabado">
                                        <div class="col-lg-12">
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_sabado[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_sabado[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_sabado[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_sabado', $this->lang->line("add"), 'id="add_date_sabado" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="datas_pontuais" class="tab-pane fade">
                                    <div class="row" id="div_datas_pontuais">
                                        <div class="col-lg-12">
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_data_pontual[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_pontual[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_pontual[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_pontual[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_pontual', $this->lang->line("add"), 'id="add_date_pontual" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="periodos" class="tab-pane fade">
                                    <div class="row" id="div_periodos">
                                        <div class="col-lg-12">
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_periodos[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_final", "data_final") ?>
                                                    <?= form_input('data_final_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="form-group all">
                                                    <?= lang("vagas", "vagas") ?>
                                                    <?= form_input('vagas_periodos[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_periodos', $this->lang->line("add"), 'id="add_date_periodos" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-lg-12">
                        <div class="col-md-12">
                            <div class="panel panel-info">
                                <div class="panel-heading"><i class="fa-fw fa fa-clock-o"></i> <?= lang("data_horario_bloqueado") ?></div>
                                <div class="panel-body">
                                    <div class="row" id="div_periodos">
                                        <div class="col-lg-12">
                                            <div class="col-md-3">
                                                <div class="form-group all">
                                                    <?= lang("descricao", "descricao") ?>
                                                    <?= form_input('descricao_bloqueio[]','', 'class="form-control" required="required"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_inicio", "data_inicio") ?>
                                                    <?= form_input('data_inicio_bloqueio[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("data_final", "data_final") ?>
                                                    <?= form_input('data_final_bloqueio[]','', 'class="form-control" required="required"', 'date'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_saida", "hora_saida") ?>
                                                    <?= form_input('hora_inicio_bloqueio[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group all">
                                                    <?= lang("hora_retorno", "hora_retorno") ?>
                                                    <?= form_input('hora_final_bloqueio[]','', 'class="form-control" required="required"', 'time'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo form_button('add_date_bloqueio', $this->lang->line("add"), 'id="add_date_bloqueio" class="btn btn-primary"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Integracao site !-->
    <div id="site" class="tab-pane fade">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('informacoes_integracao_site'); ?></h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("alertar_polcas_vagas", "alertar_polcas_vagas") ?>
                                <?= form_input('alertar_polcas_vagas', (isset($_POST['alertar_polcas_vagas']) ? $_POST['alertar_polcas_vagas'] : ($product ? $product->alertar_polcas_vagas : '10')), 'class="form-control tip mask_integer" required="required" id="alertar_polcas_vagas" '); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group all">
                                <?= lang("alertar_ultimas_vagas", "alertar_ultimas_vagas") ?>
                                <?= form_input('alertar_ultimas_vagas', (isset($_POST['alertar_ultimas_vagas']) ? $_POST['alertar_ultimas_vagas'] : ($product ? $product->alertar_ultimas_vagas : '5')), 'class="form-control tip mask_integer" required="required" id="alertar_ultimas_vagas" '); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php foreach ($tiposQuarto as $tipoQuarto){?>
    <!-- ################################################# -->
    <!-- ############## MODAL DAS HOSPEDAGEM ############# -->
    <!-- ################################################# -->
    <div class="modal" id="aModalHospedagem<?php echo $tipoQuarto->id?>" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">
                        <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                    <h2 class="modal-title" id="aModalLabel"><?= $tipoQuarto->name;?></h2>
                </div>
                <div class="modal-body" id="pr_popover_content">
                    <input type="hidden" id="aHospedagemId"/>
                    <div id="div_servicos_adicionais">
                        <h3 class="bold"><?= lang('valor_faixa_etaria_hospedagem') ?></h3>
                        <table class="table table-bordered table-striped table-condensed table-hover" id="tbServicosAdicionais">
                            <thead>
                            <tr>
                                <th style="width:2%;text-align: center;"></th>
                                <th style="width:30%;text-align: left;">Tipo</th>
                                <th style="width:10%;text-align: right;">Preço por pessoa</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($valorFaixas as $valoresFaixa) {?>
                                <tr>
                                    <td style="text-align: center;"><?php echo form_checkbox('ativarValorHospedagem[]', $tipoQuarto->id.'_'. $valoresFaixa->id, FALSE, ''); ?></td>
                                    <td>
                                        <?= form_input('faixaTipoHospedagemId[]', $tipoQuarto->id, '', 'hidden') ?>
                                        <?= form_input('tipoFaixaEtariaValorHospedagem[]',   $valoresFaixa->tipo, '', 'hidden') ?>
                                        <?= form_input('valorFaixaIdHospedagem[]',   $valoresFaixa->id, '', 'hidden') ?>
                                        <span><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span>
                                    </td>
                                    <td class="text-right"><?= form_input('valorFaixaEtariaValorHospedagem[]',   '0.00', 'class="form-control tip mask_money"') ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary updateAttr" faixaId="<?php echo $tipoQuarto->id?>"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
<?php }?>
<?= form_close(); ?>

<script type="text/javascript">

    var items = {};
    var items_faixa_etaria = [];

    var row;
    var warehouses = <?= json_encode($warehouses); ?>;
    var fornecedores = <?= json_encode($fornecedores); ?>;
    var transportes = <?= json_encode($transportes); ?>;
    var hospedagens = <?= json_encode($hospedagens); ?>;

    $(document).ready(function () {

        $('#add_date_domingo').click(function (event){
            criar_linha_dia_semana('domingo');
        });

        $('#add_date_segunda').click(function (event){
            criar_linha_dia_semana('segunda')
        });

        $('#add_date_terca').click(function (event){
            criar_linha_dia_semana('terca')
        });

        $('#add_date_quarta').click(function (event){
            criar_linha_dia_semana('quarta')
        });

        $('#add_date_quinta').click(function (event){
            criar_linha_dia_semana('quinta')
        });

        $('#add_date_sexta').click(function (event){
            criar_linha_dia_semana('sexta')
        });

        $('#add_date_sabado').click(function (event){
            criar_linha_dia_semana('sabado')
        });

        $('#add_date_pontual').click(function (event){
            criar_linha_datas_pontuais()
        });

        $('#add_date_periodos').click(function (event){
            criar_linha_datas_periodo()
        });

        $('#save').click(function (event){$('#edit_vehicle').click();});

        setTimeout(function(){$('#li_periodos').click()}, 3000);

        function criar_linha_dia_semana(dia_semana) {
            $.ajax({
                url: site.base_url + "tours/add_schedule",
                data: {
                    dia_semana: dia_semana
                },
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_' + dia_semana).append(html);

                $('.removeItemData_'+dia_semana).click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }

        function criar_linha_datas_pontuais() {
            $.ajax({
                url: site.base_url + "tours/punctual_dates",
                data: {},
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_datas_pontuais').append(html);

                $('.removeItemDataDatasPontuais').click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }

        function criar_linha_datas_periodo() {
            $.ajax({
                url: site.base_url + "tours/period_dates",
                data: {},
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#div_periodos').append(html);

                $('.removeItemDataDatasPeriodo').click(function (event) {
                    $(this).parent().parent().remove();
                });
            });
        }

        var $supplier = $('#aFornecedor');

    	$supplier.change(function (e) {
      		$('#aFornecedor').val($(this).val());
    	});

        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };
                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };
                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $('#aFornecedor').select2({
    	        minimumInputLength: 0,
    	        ajax: {
    	            url: site.base_url + "suppliers/suggestions",
    	            dataType: 'json',
    	            quietMillis: 15,
    	            data: function (term, page) {
    	                return {
    	                    term: term,
    	                    limit: 10
    	                };
    	            },
    	            results: function (data, page) {
    	                if (data.results != null) {
    	                    return {results: data.results};
    	                } else {
    	                    return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
    	                }
    	            }
    	        }
    	 });
    	
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');

        <?php
        if($combo_items) {
            foreach($combo_items as $item) {
            //echo 'ietms['.$item->id.'] = '.$item.';';
                if($item->code) {
                    echo 'add_product_item('.  json_encode($item).');';
                }
            }
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");': '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) ? '$("#promotion").iCheck("check");': '' ?>
        $('#promotion').on('ifChecked', function (e) {
            $('#promo').slideDown();
        });
        $('#promotion').on('ifUnchecked', function (e) {
            $('#promo').slideUp();
        });

        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });

        $('.delAgendamento').click(function (event) {
            if (confirm('Deseja realmente excluir o lançamento para esta data?')) {
                let agendamentoId = $(this).attr('id');
                let produtoId = $(this).attr('produto');

                window.location = site.base = 'products/excluirProgramacaoData/'+agendamentoId+'/'+produtoId;
            }
        });

        $('.visualizar-informacoes').click(function (event) {

            let id = $(this).attr('id');

            if ($( ".visualizar-informacoes-item-"+id).is( ":visible" ) ) {
                $('.visualizar-informacoes-item-'+id).hide(300);
            } else {
                $('.visualizar-informacoes-item-'+id).show(300);
            }
        });

        $('.addNewData').click(function (event){
            $.ajax({
                url: site.base_url + "agenda/view_lancamento_agenda_data",
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#new-linha-data').append(html);

                $('.removeItemData').click(function (event) {
                    $(this).parent().parent().remove();
                });

                adicionarCamposObrigatorioAoFormulario();
            });
        });

        $('#type').change(function () {
				
            var t = $(this).val();
            if (t !== 'standard') {
                $('.standard').slideDown();
				$('#datas_viagem').show();
				$('#div_status').show();
				$('#div_category').show();
				$('#div_product_details').show();
                $('#track_quantity').iCheck('uncheck');
            } else {
                $('.standard').slideDown();
				$('#datas_viagem').show();
				$('#div_status').show();
				$('#div_category').show();
				$('#div_product_details').show();
                $('#track_quantity').iCheck('check');
             }
            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#digital_file').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
            } else {
                $('.digital').slideDown();
                $('#digital_file').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
            }
			
            if (t !== 'combo') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideUp();
            }
			
			if (t == 'service') {
                $('.combo').slideUp();
				$('.standard').slideUp();
				$('.digital').slideUp();
				$('#datas_viagem').hide();
				$('#div_status').hide();
				$('#div_category').hide();
				$('#div_product_details').hide();
            }
        });

        var t = $('#type').val();
        if (t !== 'standard') {
             $('#track_quantity').iCheck('uncheck');
         } else {
            $('#track_quantity').iCheck('check');
         }
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#digital_file').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
        } else {
            $('.digital').slideDown();
            $('#digital_file').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
        }
        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideUp();
        }

        $("#add_item").autocomplete({
            source: '<?= site_url('products/suggestions_adicionais'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });

        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });

        <?php
        if($this->input->post('type') == 'combo') {
            $c = sizeof($_POST['combo_item_code']);
            for ($r = 0; $r <= $c; $r++) {
                if(isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                    $items[] = array('id' => $_POST['combo_item_id'][$r], 'name' => $_POST['combo_item_name'][$r], 'code' => $_POST['combo_item_code'][$r], 'qty' => $_POST['combo_item_quantity'][$r], 'price' => $_POST['combo_item_price'][$r]);
                }
            }
            echo '
            var ci = '.json_encode($items).';
            $.each(ci, function() { add_product_item(this); });
            ';
        }
        ?>
        function add_product_item(item) {

            if (item == null) return false;

            item_id = item.id;

            if (items[item_id]) items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            else  items[item_id] = item;

            $("#prTable tbody").empty();

            $.each(items, function () {

                var row_no  = this.id;
                var newTr   = $('<tr id="row_' + row_no + '" class="item_' + this.id + '"></tr>');

                let tr_html = '' +
                    '<td>' +
                    '   <input name="combo_item_id[]" type="hidden" value="' + this.id + '">' +
                    '   <input name="combo_item_name[]" type="hidden" value="' + this.name + '">' +
                    '   <input name="combo_item_code[]" type="hidden" value="' + this.code + '">' +
                    '   <span id="name_' + row_no + '"><span>' + this.name + ' </span>' +
                    '</td>';

                tr_html += '<td style="display: none;"><input type="text" class="form-control tip mask_money" name="combo_item_price[]" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td><button class="btn btn-primary attrServicosAdicionais"><i class="fa fa-plus"></i> Configurar Valor Por Faixa</button></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';

                tr_html += '<td style="display: none">' +
                    '<input type="hidden" class="rNomeAdicional"        value="'+this.name +'">' +
                    '<input type="hidden" class="rComissao"             name="servicoAdicionalValorComissao[]"  value="'+getRow(this.comissao)+'">' +
                    '<input type="hidden" class="rFornecedor"           name="servicoAdicionalFornecedor[]"     value="'+getRow(this.fornecedorId)+'">';

                <?php foreach ($valorFaixas as $valoresFaixa) {?>
                    tr_html += '<input type="hidden" class="rServicoId<?php echo $valoresFaixa->id;?>"                  name="servicoAdicionalId[]"       value="'+this.id+'">';
                    tr_html += '<input type="hidden" class="rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>"    name="faixaIdServicoAdicional[]"  value="<?php echo $valoresFaixa->id;?>">';
                    tr_html += '<input type="hidden" class="rValorFaixaAdicional<?php echo $valoresFaixa->id;?>"        name="servicoAdicionalValor[]"    value="'+getRow(this.valor<?php echo $valoresFaixa->id;?>)+'">';
                    tr_html += '<input type="hidden" class="isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>"     name="isAtivoServicoAdicional[]"  value="'+getRow(this.status<?php echo $valoresFaixa->id;?>)+'">';
                <?php }?>

                tr_html += '</td>';

                newTr.html(tr_html);
                newTr.prependTo("#prTable");
            });
            return true;
        }

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');

            delete items[id];
            delete items_faixa_etaria[id];

            $('#attrTableServicosAdicionaisFaixaEtaria'+id).remove();
            $(this).closest('#row_' + id).remove();
        });
		
        var su = 2;
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                var html = '<div style="clear:both;height:5px;"></div><div class="row"><div class="col-xs-12"><div class="form-group"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);
                    img.src = 'images/' + result[i];
                }
            }
        });
		
        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });
		
        $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });
        
		$(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
        	var valorSugerido = 0;
            $("input[name='attr_totalPreco[]']").each(function(){
            	var attr_totalPreco = $(this).val();
            	if (attr_totalPreco != '') {
            		attr_totalPreco = parseFloat(attr_totalPreco);
            		valorSugerido = valorSugerido + attr_totalPreco;
            	}
            });
        });
		
        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });

        $('#aModal').on('shown.bs.modal', function () {
            $('#aquantity').focus();
            $(this).keypress(function( e ) {
                if ( e.which === 13 ) {
                    $('#updateAttr').click();
                }
            });
        });

        $(document).on('click', '#adicionarFaxiaEtariaServicosAdicionais', function () {
            preencherFaixaEtariaServicoAdicionalByForm();
        });

        $(document).on('click', '.attrServicosAdicionais', function () {

            row_adicional = $(this).closest("tr");

            var fornecedorId =  row_adicional.children().children('.rFornecedor').val();
            var nomeServicoAdicional = row_adicional.children().children('.rNomeAdicional').val();

            criarFornecedorAdicional(fornecedorId);

            <?php foreach ($valorFaixas as $valoresFaixa) {?>

            let rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val();
            let rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = row_adicional.children().children('.rValorFaixaAdicional<?php echo $valoresFaixa->id;?>').val();
            let isAtivoServicoAdicional<?php echo $valoresFaixa->id;?> = getBoolean(row_adicional.children().children('.isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>').val());

            if (rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === undefined ||
                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === ''  ||
                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> === '0') {

                rValorFaixaAdicional<?php echo $valoresFaixa->id;?> = '0.00';
            }

            if (isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>) {
                $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', true);
            } else {
                $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').attr('checked', false);
            }

            $('#faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val(rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>);
            $('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val(rValorFaixaAdicional<?php echo $valoresFaixa->id;?>);
            <?php } ?>

            $('#servicoAdicionalId').val(row_adicional.children().eq(0).find('input').val());
            $('input[type="checkbox"],[type="radio"]').iCheck('update');
            $('#nmServicoAdicionalTitle').html(nomeServicoAdicional);

            $('#aModalFaixaEtariaServicoAdicional').appendTo('body').modal('show');
        });

    });

    <?php if ($product) { ?>
    $(document).ready(function () {
        
		var t = "<?=$product->type?>";
		
        if (t !== 'standard') {
            $('.standard').slideUp();
             $('#track_quantity').iCheck('uncheck');
         } else {
            $('.standard').slideDown();
            $('#track_quantity').iCheck('check');
         }
		
        if (t !== 'digital') {
            $('.digital').slideUp();
            $('#digital_file').removeAttr('required');
            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
        } else {
            $('.digital').slideDown();
            $('#digital_file').attr('required', 'required');
            $('form[data-toggle="validator"]').bootstrapValidator('addField', 'digital_file');
        }
		
        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideDown();
        }
		
        $("#code").parent('.form-group').addClass("has-error");
        $("#code").focus();
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");
        
		$.ajax({
            type: "get", async: false,
            url: "<?= site_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        data: scdata
                    });
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                        placeholder: "<?= lang('no_subcategory') ?>",
                        data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                    });
                }
            }
        });

        <?php if ($product->supplier1) { ?>
			select_supplier('supplier1', "<?= $product->supplier1; ?>");
			$('#supplier_price').val("<?= $product->supplier1price == 0 ? '' : $this->sma->formatDecimal($product->supplier1price); ?>");
        <?php } ?>
        
		<?php if ($product->supplier2) { ?>
			$('#addSupplier').click();
			select_supplier('supplier_2', "<?= $product->supplier2; ?>");
			$('#supplier_2_price').val("<?= $product->supplier2price == 0 ? '' : $this->sma->formatDecimal($product->supplier2price); ?>");
        <?php } ?>
		
        <?php if ($product->supplier3) { ?>
			$('#addSupplier').click();
			select_supplier('supplier_3', "<?= $product->supplier3; ?>");
			$('#supplier_3_price').val("<?= $product->supplier3price == 0 ? '' : $this->sma->formatDecimal($product->supplier3price); ?>");
        <?php } ?>
        
		<?php if ($product->supplier4) { ?>
			$('#addSupplier').click();
			select_supplier('supplier_4', "<?= $product->supplier4; ?>");
			$('#supplier_4_price').val("<?= $product->supplier4price == 0 ? '' : $this->sma->formatDecimal($product->supplier4price); ?>");
        <?php } ?>
        
		<?php if ($product->supplier5) { ?>
			$('#addSupplier').click();
			select_supplier('supplier_5', "<?= $product->supplier5; ?>");
			$('#supplier_5_price').val("<?= $product->supplier5price == 0 ? '' : $this->sma->formatDecimal($product->supplier5price); ?>");
        <?php } ?>
        
		function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= site_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        }
        var whs = $('.wh');
        $.each(whs, function () {
            $(this).val($('#r' + $(this).attr('id')).text());
        });
    });
    <?php } ?>

    function adicionarCamposObrigatorioAoFormulario() {

        $('form[data-toggle="validator"]').data('bootstrapValidator', null);
        $('form[data-toggle="validator"]').bootstrapValidator();

        //$('form[data-toggle="validator"]').data('bootstrapValidator').destroy();

        $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

        let fields = $('.form-control');
        $.each(fields, function() {

            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#'+id;

            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                if ($("label[for='" + id + "']").html() !== undefined) {
                    let label =  $("label[for='" + id + "']").html().replace('*', '');
                    $("label[for='" + id + "']").html(label + ' *');
                    $(document).on('change', iid, function () {
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                    });
                }
            }
        });
    }

    function preencherFaixaEtariaServicoAdicionalByForm() {

        let servicoId = $('#servicoAdicionalId').val();
        let fornecedorId = $('#fornecedorServicoAdicional').val();
        let nomeServicoAdicional = $('#nmServicoAdicionalTitle').html();
        let comissao = formatDecimal($('#comissaoServicoAdicional').val());
        let item = items[parseFloat(servicoId)];

        let html =
            ' ' +
            '<input type="hidden" class="rComissao" name="servicoAdicionalValorComissao[]" value="' + comissao + '">' +
            '<input type="hidden" class="rFornecedor" name="servicoAdicionalFornecedor[]" value="' + fornecedorId + '">' +
            '<input type="hidden" class="rNomeAdicional" value="' + nomeServicoAdicional + '">';

        <?php foreach ($valorFaixas as $valoresFaixa) {?>

        let faixaIdServicoAdicional<?php echo $valoresFaixa->id;?> = $('#faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>').val();
        let valorAdicional<?php echo $valoresFaixa->id;?> = formatDecimal($('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val());
        let statusAdicional<?php echo $valoresFaixa->id;?> = $('#statusValorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').is(":checked");

        html += '<input type="hidden" class="rServicoId<?php echo $valoresFaixa->id;?>"                 name="servicoAdicionalId[]"         value="' + servicoId + '">'
        html += '<input type="hidden" class="rFaixaIdServicoAdicional<?php echo $valoresFaixa->id;?>"   name="faixaIdServicoAdicional[]"    value="' + faixaIdServicoAdicional<?php echo $valoresFaixa->id;?> +'">';
        html += '<input type="hidden" class="rValorFaixaAdicional<?php echo $valoresFaixa->id;?>"       name="servicoAdicionalValor[]"      value="' + valorAdicional<?php echo $valoresFaixa->id;?> + '">';
        html += '<input type="hidden" class="isAtivoServicoAdicional<?php echo $valoresFaixa->id;?>"    name="isAtivoServicoAdicional[]"    value="' + statusAdicional<?php echo $valoresFaixa->id;?> + '">';

        $('#valorFaixaServicoAdicional<?php echo $valoresFaixa->id;?>').val('0.00');

        item.faixaId<?php echo $valoresFaixa->id;?> = faixaIdServicoAdicional<?php echo $valoresFaixa->id;?>;
        item.valor<?php echo $valoresFaixa->id;?>   = valorAdicional<?php echo $valoresFaixa->id;?>;
        item.status<?php echo $valoresFaixa->id;?>  = statusAdicional<?php echo $valoresFaixa->id;?>;

        <?php } ?>

        row_adicional.children().eq(4).html(html);

        $('#aModalFaixaEtariaServicoAdicional').modal('hide');
    }

    function criarFornecedorAdicional(aFornecedor) {

        if (aFornecedor !== '' && aFornecedor !== undefined){
            $('#fornecedorServicoAdicional').select2({
                minimumInputLength: 0,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"suppliers/getSupplier/" + aFornecedor,
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('#fornecedorServicoAdicional').val(aFornecedor).trigger('change');

        } else {
            $('#fornecedorServicoAdicional').select2({
                minimumInputLength: 0,
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        }
    }
</script>

<!-- #################################################################### -->
<!-- ######## MODAL VALOR POR FAIXA ETARIA SERVICOS OPCIONAIS ########### -->
<!-- #################################################################### -->
<div class="modal" id="aModalFaixaEtariaServicoAdicional" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="nmServicoAdicionalTitle"><?= lang('add_faixa_etaria') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <input type="hidden" value="" id="servicoAdicionalId"/>
                    <div class="form-group" style="display: none;">
                        <label for="fornecedor" class="col-sm-4 control-label"><?= lang('fornecedor') ?></label>
                        <div class="col-sm-8">
                            <input type="hidden" name="fornecedorServicoAdicional" value="" id="fornecedorServicoAdicional"
                                   class="form-control"  placeholder="<?= lang("select") . ' ' . lang("fornecedor") ?>" >
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="acost" class="col-sm-4 control-label"><?= lang('comissao') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control mask_money" id="comissaoServicoAdicional">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped table-condensed table-hover" id="tbValorFaixaEtariaServicosAdicionais">
                        <thead>
                        <tr>
                            <th style="width:1%;text-align: center;"><?= lang('ativar') ?></th>
                            <th style="width:30%;text-align: left;">Tipo</th>
                            <th style="width:10%;text-align: right;">Preço por pessoa</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($valorFaixas as $valoresFaixa) {?>
                            <tr>
                                <td style="text-align: center;">
                                    <?php echo form_checkbox('', $valoresFaixa->id, FALSE, 'id="statusValorFaixaServicoAdicional'.$valoresFaixa->id.'"'); ?>
                                    <?= form_input('faixaIdServicoAdicional', $valoresFaixa->id, 'id=faixaIdServicoAdicional'.$valoresFaixa->id, 'hidden') ?>
                                </td>
                                <td><?php echo $valoresFaixa->name;?><br/><small><?php echo $valoresFaixa->note;?> </small></span></td>
                                <td class="text-right"><?= form_input('',   '0.00', 'id="valorFaixaServicoAdicional'.$valoresFaixa->id.'" class="form-control tip mask_money"') ?></td>
                            </tr>
                        <?php }?>
                        </tbody>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="adicionarFaxiaEtariaServicosAdicionais"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>