<div class="col-lg-12">
    <div class="col-md-2">
        <div class="form-group all">
            <?= lang("descricao", "descricao") ?>
            <?= form_input('descricao_periodos[]','', 'class="form-control" required="required"'); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group all">
            <?= lang("data_inicio", "data_inicio") ?>
            <?= form_input('data_inicio_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group all">
            <?= lang("data_final", "data_final") ?>
            <?= form_input('data_final_periodos[]','', 'class="form-control" required="required"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group all">
            <?= lang("hora_saida", "hora_saida") ?>
            <?= form_input('hora_inicio_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group all">
            <?= lang("hora_retorno", "hora_retorno") ?>
            <?= form_input('hora_final_periodos[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group all">
            <?= lang("vagas", "vagas") ?>
            <?= form_input('vagas_periodosl[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
        </div>
    </div>
    <div class="col-md-1" style="float: left;margin-top: 30px;text-align: right;">
        <i class="fa fa-trash fa-2x removeItemDataDatasPeriodo" style="cursor: pointer"></i>
    </div>
</div>