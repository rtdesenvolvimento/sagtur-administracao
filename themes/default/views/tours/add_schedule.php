<div class="col-lg-12">
    <div class="col-md-5">
        <div class="form-group all">
            <?= lang("hora_saida", "hora_saida") ?>
            <?= form_input('hora_inicio_'. $dia_semana .'[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group all">
            <?= lang("hora_retorno", "hora_retorno") ?>
            <?= form_input('hora_final_'. $dia_semana .'[]','', 'class="form-control" required="required"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-1">
        <div class="form-group all">
            <?= lang("vagas", "vagas") ?>
            <?= form_input('vagas_' . $dia_semana . '[]', '', 'class="form-control tip mask_integer" required="required" ') ?>
        </div>
    </div>
    <div class="col-md-1" style="float: left;margin-top: 5px;text-align: right;">
        <i class="fa fa-trash fa-2x removeItemData_<?=$dia_semana;?>" style="cursor: pointer"></i>
    </div>
</div>