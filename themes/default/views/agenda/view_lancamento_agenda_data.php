<div>
    <div class="col-md-3">
        <div class="form-group" id="ui" style="margin-bottom: 0;">
            <?= lang("data_do_dia", "doDia") ?>
            <?= form_input('doDia[]', '', 'class="form-control_data tip" required="required"', 'date') ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group" id="ui" style="margin-bottom: 0;">
            <?= lang("hora_saida", "horaSaida") ?>
            <?= form_input('horaSaida[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group" id="ui" style="margin-bottom: 0;">
            <?= lang("data_ao_dia", "aoDia") ?>
            <?= form_input('aoDia[]', '', 'class="form-control_data tip"', 'date') ?>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group" id="ui" style="margin-bottom: 0;">
            <?= lang("hora_retorno", "horaRetorno") ?>
            <?= form_input('horaRetorno[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
        </div>
    </div>
    <div class="col-md-1">
        <?= lang("vagas", "vagas") ?>
        <?= form_input('vagas[]', (isset($_POST['vagas']) ? $_POST['vagas'] : ($produto ? $produto->quantidadePessoasViagem : '')), 'class="form-control_data tip mask_integer" required="required" ') ?>
    </div>
    <div class="col-md-1" style="float: left;margin-top: 25px;font-size: 20px;">
        <i class="fa fa-trash removeItemData" style="cursor: pointer"></i>
    </div>
</div>