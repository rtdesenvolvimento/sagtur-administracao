<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('atribuir_cupom'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agenda/atribuir_cupom/" . $agenda->id, $attrib); ?>
        <div class="modal-body">
            <input type="hidden" value="<?php echo $agenda->id; ?>" name="id"/>
            <input type="hidden" value="<?php echo $agenda->produto; ?>" name="produto" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', $product->name, 'class="form-control tip" id="name" readonly'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("data_do_dia", "doDia"); ?>
                        <?php echo form_input('doDia', $agenda->dataSaida, 'class="form-control tip" readonly id="doDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("data_ao_dia", "aoDia"); ?>
                        <?php echo form_input('aoDia', $agenda->dataRetorno, 'class="form-control tip" readonly id="aoDia"', 'date'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 20px;">
                <div class="col-md-12">
                    <h3 class="bold"><?= lang('cupons_desconto') ?></h3>
                    <table class="table table-bordered table-striped table-condensed" id="tbServicosAdicionais">
                        <thead>
                        <tr>
                            <th style="width:5%;display: none;">#</th>
                            <th style="width:30%;text-align: left;">Nome</th>
                            <th>Desconto</th>
                            <th style="width:10%;text-align: right;">Qtd</th>
                            <th style="width:5%;text-align: left;">Usados</th>
                            <th style="width:5%;text-align: left;">Disponível</th>
                            <th style="width:15%;text-align: right;">Data Início</th>
                            <th style="width:15%;text-align: right;">Data Final</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($cupons as $cupom){
                            $cupomItem = $this->CupomDescontoRepository_model->getCupomDescontoByProgramacaoId($cupom->id, $agenda->id); ?>
                            <?php if ($cupomItem) {

                                $totalCupomUsado = $this->CupomDescontoRepository_model->total_cupom_usado($agenda->id, $cupom->id);
                                $totalCupomDisponibilizado = $cupomItem->quantidade_disponibilizada;
                                $disponvel = $totalCupomDisponibilizado - $totalCupomUsado;

                                ?>
                                <tr>
                                    <td style="text-align: center;display: none;"><?php echo form_checkbox('ativarCupom[]', $cupom->id, TRUE, ''); ?></td>
                                    <td>
                                        <?= form_input('cupomId[]',   $cupom->id, 'class="form-control tip"', 'hidden') ?>
                                        <?php echo $cupom->name;?>
                                        <?php if ($cupom->descricao){?>
                                            <br/><?php echo $cupom->descricao;?>
                                        <?php } ?>
                                        <?php echo '<br/>Cód. do Cupom: <b>'.$cupom->codigo.'</b>'?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php
                                        $tipo = $cupom->tipo_desconto;
                                        if ($tipo == 'PERCENTUAL'){
                                            echo $cupom->valor.'%';
                                        } else {
                                            echo 'R$'.$cupom->valor;
                                        }
                                        ?>
                                    </td>
                                    <td><?= form_input('quantidadeCupom[]',   $cupomItem->quantidade_disponibilizada, 'class="form-control tip mask_integer"') ?></td>
                                    <td><?= form_input('quantidadeCupomUsados[]',   $totalCupomUsado, 'class="form-control tip mask_integer" disabled') ?></td>
                                    <td><?= form_input('quantidadedCupomDisponivel[]',   $disponvel, 'class="form-control tip mask_integer" disabled') ?></td>
                                    <td><?= form_input('dataInicio_cupom[]',    $cupomItem->dataInicio, 'class="form-control tip"', 'date') ?></td>
                                    <td><?= form_input('dataFinal_cupom[]',    $cupomItem->dataFinal, 'class="form-control tip"', 'date') ?></td>
                                    <td>
                                        <a href="<?= site_url('agenda/relatorio_cupom/'.$agenda->id.'/'.$cupom->id)?>"
                                           class="link_relatorio_venda" target="_blank"><span class="fa fa-print"></span></a>

                                    </td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td style="text-align: center;display: none;"><?php echo form_checkbox('ativarCupom[]', $cupom->id, FALSE, ''); ?></td>
                                    <td>
                                        <?= form_input('cupomId[]',   $cupom->id, 'class="form-control tip"', 'hidden') ?>
                                        <?php echo $cupom->name;?>
                                        <?php if ($cupom->descricao){?>
                                            <br/><?php echo $cupom->descricao;?>
                                        <?php } ?>
                                        <?php echo '<br/>Cód. do Cupom: <b>'.$cupom->codigo.'</b>'?>
                                    </td>
                                    <td style="text-align: center;">
                                        <?php
                                        $tipo = $cupom->tipo_desconto;

                                        if ($tipo == 'PERCENTUAL'){
                                            echo $cupom->valor.'%';
                                        } else {
                                            echo 'R$'.$cupom->valor;
                                        }
                                        ?>
                                    </td>
                                    <td><?= form_input('quantidadeCupom[]',   '0', 'class="form-control tip mask_integer"') ?></td>
                                    <td><?= form_input('quantidadeCupomUsados[]',   '0', 'class="form-control tip mask_integer" disabled') ?></td>
                                    <td><?= form_input('quantidadedCupomDisponivel[]',   '0', 'class="form-control tip mask_integer" disabled') ?></td>
                                    <td><?= form_input('dataInicio_cupom[]',   '', 'class="form-control tip"', 'date') ?></td>
                                    <td><?= form_input('dataFinal_cupom[]',   '', 'class="form-control tip"', 'date') ?></td>
                                    <td><span class="fa fa-print"></span></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('atribuir_cupom', lang('atribuir_cupom'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
