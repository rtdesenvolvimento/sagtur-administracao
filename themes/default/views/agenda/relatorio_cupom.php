<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }
        td {
            padding: 1px;
        }
        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>

<table border="" style="width: 100%;border-collapse:collapse;">
    <thead>
    <tr>
        <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
            <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
        </td>
        <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
            <h4>RELATÓRIO DE CUPOM DE DESCONTO</h4>
        </td>
    </tr>
    <tr>
        <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
            <h5>Cupom: <?=$cupom->name?> (<?=$cupom->codigo?> ) </h5>
        </td>
    </tr>
    </thead>
</table>
<table border="0" style="width: 100%;">
    <thead>
    <tr>
        <th style="text-align: center;width: 5%;">#</th>
        <th style="width: 15%">N:.Ref</th>
        <th style="width: 85%;">Passageiro</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $contador = 1;
    $isColorBackground = false;

    foreach ($itens as $row) {

        $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);
        $customer        = $objCustomer->name;

        $background = "#eee";

        if ($isColorBackground) {
            $background = "#eee";
            $isColorBackground = false;
        } else {
            $background = "#ffffff";
            $isColorBackground = true;
        }
        ?>
        <tr style="background: <?=$background;?>">
            <td style="text-align: center;"><?=$contador;?></td>
            <td style="text-align: center;"><?php echo $row->reference_no;?></td>
            <td align="left">&nbsp;<?php echo $customer;?></td>
        </tr>
        <?php $contador++?>
    <?php }?>
    </tbody>
</table>
</body>
</html>
