<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_agendamento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agenda/editByProduto/" . $agenda->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <input type="hidden" value="<?php echo $agenda->id; ?>" name="id"/>
            <input type="hidden" value="<?php echo $agenda->produto; ?>" name="produto" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("data_do_dia", "doDia"); ?>
                        <?php echo form_input('doDia', $agenda->dataSaida, 'class="form-control tip" required="required" id="doDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_saida", "horaSaida"); ?>
                        <?php echo form_input('horaSaida', $agenda->horaSaida, 'class="form-control tip" id="horaSaida"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("data_ao_dia", "aoDia"); ?>
                        <?php echo form_input('aoDia', $agenda->dataRetorno, 'class="form-control tip" id="aoDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_retorno", "horaRetorno"); ?>
                        <?php echo form_input('horaRetorno', $agenda->horaRetorno, 'class="form-control tip" id="horaRetorno"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("vagas", "vagas"); ?>
                        <?php echo form_input('vagas', $agenda->vagas, 'class="form-control tip mask_integer" id="vagas" required="required"', 'number'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('active', 'active'); ?>
                        <?php
                        $wm = array('0' => lang('no'), '1' => lang('yes'));
                        echo form_dropdown('active', $wm,$agenda->active, 'class="tip form-control" required="required" id="active" style="width:100%;"');
                        ?>
                    </div>
                </div>

                <?php if ($product->cat_precificacao == 'preco_por_data'){?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <?= lang('value', 'value'); ?>
                            <?php echo form_input('preco', $agenda->preco, 'class="form-control tip mask_money" style="padding-right: 5px;" id="preco" required="required"'); ?>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php if ($exibir_marcacao_assento) {?>
               <div class="row" style="display: none;">
                   <div class="col-md-12">
                       <div class="panel panel-info">
                           <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("info_marcacao_assento", "details") ?></div>
                           <div class="panel-body">
                               <div class="col-md-12">
                                   <div class="form-group all">
                                       <?php echo form_checkbox('habilitar_marcacao_area_cliente', '1', $agenda->habilitar_marcacao_area_cliente, ''); ?>
                                       <label for="attributes" class="padding05"><?= lang('habilitar_marcacao_area_cliente'); ?></label>
                                   </div>
                               </div>
                               <div class="col-md-12">
                                   <div class="form-group all">
                                       <?php echo form_checkbox('permitir_marcacao_dependente', '1', $agenda->permitir_marcacao_dependente, ''); ?>
                                       <label for="attributes" class="padding05"><?= lang('permitir_marcacao_dependente'); ?></label>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <?= lang('data_inicio_marcacao', 'data_inicio_marcacao'); ?>
                                       <input type="date" name="data_inicio_marcacao" value="<?=$agenda->data_inicio_marcacao;?>" class="form-control tip" id="data_inicio_marcacao">
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <?= lang('data_final_marcacao', 'data_final_marcacao'); ?>
                                       <input type="date" name="data_final_marcacao" value="<?=$agenda->data_final_marcacao;?>" class="form-control tip" id="data_final_marcacao">
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            <?php } ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('editar_agendamento', lang('editar_agendamento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>


<script type="text/javascript">

    $(document).ready(function () {
        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };
                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });
    })
</script>