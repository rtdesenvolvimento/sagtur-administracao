<style>
    .form-control_custom {
        display: block;
        width: 100%;
        height: 25px;
        padding: 0px 0px;
        font-size: 11px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .form-control_data {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>

<div class="row">
    <?php if (!empty($datasAgendadas)) {?>
        <div class="col-lg-12">
            <table id="tbDatas" class="table table-condensed table-striped" style="cursor: pointer;">
                <tbody>
                <?php
                $mesAno = '';
                foreach ($datasAgendadas as $dataAgendada) {

                    $totalReservas = 0;
                    $totalVendas = 0;

                    //$dataAgendada = new AgendaViagem_model();
                    $mesAnoViagem =  strtoupper($this->sma->dataDeHojePorExtensoRetornoMensAno($dataAgendada->getDataSaida()));
                    ?>
                    <?php if ($mesAno != $mesAnoViagem){?>
                        <tr class="active">
                            <th colspan="2" style="text-align: left;border: 1px solid #ffffff;background: #ffffff;">
                                <h3> <?php echo $mesAnoViagem;?></h3>
                            </th>
                        </tr>
                    <?php } ?>
                    <tr class="active">
                        <th class="col-md-12" colspan="12" style="text-align: left;background: #428bca;color: #ffffff;">
                            <?php echo strtoupper($this->sma->dataDeHojePorExtensoRetornoComSemana($dataAgendada->getDataSaida()))?>
                        </th>
                    </tr>
                    <tr>
                        <td><?= lang('data_saida') ?><br/><?= form_input('dataSaidaData[]', '' . $this->sma->hrsd($dataAgendada->getDataSaida()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td><?= lang('hora') ?><br/><?= form_input('horaSaidaData[]', '' . $dataAgendada->getHoraSaida() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td><?= lang('data_retorno') ?><br/><?= form_input('dataRetornoData[]', '' . $this->sma->hrsd($dataAgendada->getDataRetorno()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled required="required"') ?></td>
                        <td><?= lang('hora') ?><br/><?= form_input('horaRetornoData[]', '' . $dataAgendada->getHoraRetorno() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td style="text-align: right;"><?= lang('vagas') ?><br/><?= form_input('vagasData[]', '' . $dataAgendada->getVagas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                        <td style="text-align: right;"><?= lang('orcamento') ?><br/><?= form_input('', '' . $dataAgendada->getTotalOrcamento() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td style="text-align: right;"><?= lang('faturadas') ?><br/><?= form_input('', '' . $dataAgendada->getTotalVendasFaturas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td style="text-align: right;"><?= lang('disponivel') ?><br/><?= form_input('', '' . $dataAgendada->getTotalDisponvel() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                        <td style="text-align: right;"><?= lang('espera') ?><br/><?= form_input('', '' . $dataAgendada->getTotalListaEspera() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                        <td class="">
                            <br/>
                            <div class="text-center">
                                <div class="btn-group text-left">
                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle"
                                            data-toggle="dropdown">Ações <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <?php if ($Owner || $Admin) { ?>
                                            <li><a href="<?php echo base_url();?>agenda/edit/<?php echo $dataAgendada->id;?>" data-toggle="modal" data-target="#myModal4"><i class="fa fa-edit"></i>Editar Agenda</a></li>
                                            <li><a href="<?php echo base_url();?>products/edit/<?php echo $dataAgendada->produto;?>"><i class="fa fa-map-signs"></i>Editar Serviço</a></li>
                                        <?php }?>
                                        <li><a id="<?php echo $dataAgendada->id;?>" class="visualizar-informacoes"><i class="fa fa-eye"></i>Ver Detalhes</a></li>

                                        <?php if ($Owner || $Admin) { ?>
                                            <li><a href="<?= site_url('reports/relatorioPassageirosProgramacao/'.$dataAgendada->id)?>"><i class="fa fa-users"></i><?= lang('Relatório Geral da Viagem') ?></a></li>
                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros_todos/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i> Lista de Passageiros</a></li>
                                        <?php }?>

                                        <?php if ($Owner || $Admin) { ?>
                                            <li class="divider"></li>
                                            <?php foreach ($transportes as $transporte) {?>
                                                <?php if ($transporte->status == 'ATIVO'){?>
                                                    <li>
                                                        <a href="<?= site_url('sales/montarPoltronas/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>">
                                                            <i class="fa fa-map-pin"></i> Configurar Assentos <?php echo $transporte->text;?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                            <li class="divider"></li>
                                            <li><a href="<?= site_url('sales/pdf_hotel/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-bed"></i><?= lang('relatorio_room_list') ?></a></li>
                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i><?= lang('relatorio_geral_de_passageiros') ?></a></li>
                                            <li><a href="<?= site_url('sales/relatorio_seguradora_de_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id) ?>" target="_blank"><i class="fa fa-ambulance"></i><?= lang('relatorio_seguradora_de_passageiros') ?></a></li>
                                            <?php foreach ($transportes as $transporte) {?>
                                                <?php if ($transporte->status == 'ATIVO'){
                                                    $itens = $this->site->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                    $totalVendasItem = 0;

                                                    if ($itens) $totalVendasItem = count($itens);
                                                    ?>
                                                    <li>
                                                        <a href="<?= site_url('sales/relatorio_enviado_empresa_onibus/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                            <i class="fa fa-bus"></i>Lista de Passageiros para ANTT <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>

                                            <li class="divider"></li>
                                            <?php
                                            $roomlists = $this->Roomlist_model->getAll($dataAgendada->id);
                                            ?>
                                            <?php foreach ($roomlists as $roomlist) {?>
                                                <li>
                                                    <a href="<?php echo site_url('roomlist/montar/'.$roomlist->id); ?>">
                                                        <i class="fa fa fa-cogs"></i><?= lang('configurar').' '.$roomlist->name;?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <?php foreach ($roomlists as $roomlist) {?>
                                                <li>
                                                    <a href="<?php echo site_url('roomlist/relatorio/'.$roomlist->id); ?>" target="_blank">
                                                        <i class="fa fa-print"></i><?= lang('print').' '.$roomlist->name;?>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                            <li>
                                                <a href="<?php echo site_url('roomlist/adicionarRoomList/'.$dataAgendada->id); ?>" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-bed"></i><?= lang('adicionar_novo_room_list') ?>
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <?php foreach ($transportes as $transporte) {?>
                                                <?php if ($transporte->status == 'ATIVO'){
                                                    $itens = $this->site->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                    $totalVendasItem = 0;

                                                    if ($itens) $totalVendasItem = count($itens);
                                                    ?>
                                                    <li>
                                                        <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                            <i class="fa fa-hand-stop-o"></i>Lista de Embarque <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                        </td>
                        <?php if ($Owner || $Admin) { ?>
                            <td class="text-center"><i class="fa fa-times delAttr" id="<?php echo $dataAgendada->id;?>" produto="<?php echo $dataAgendada->getProduto();?>" ></i></td>
                        <?php } else {?>
                            <td></td>
                        <?php } ?>
                    </tr>
                    <?php $mesAno = $mesAnoViagem; ?>
                <?php } ?>
                </tbody>
            </table>
        </div>
    <?php } else {?>
    <div class="col-lg-12">
        Nenhuma data futura agendada
    </div>
    <?php } ?>
</div>