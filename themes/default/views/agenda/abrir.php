<style>
    .form-control_custom {
        display: block;
        width: 100%;
        height: 25px;
        padding: 0px 0px;
        font-size: 11px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        border-radius: 0px;
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
        -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    }
    .form-control_data {
        display: block;
        font-weight: 700;
        width: 95%;
        height: 27px;
        padding: 0px 3px;
        font-size: 10px;
        line-height: 1.42857143;
        color: #555;
        background-color: #fff;
        background-image: none;
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
        -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
        -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    }
</style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('adicionar_agenda'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <?php
            $attrib = array('data-toggle' => 'validator', 'role' => 'form');
            echo form_open_multipart("agenda/agendar", $attrib) ?>
            <div class="col-lg-12">
                <div class="col-lg-3" style="display: none;">
                    <?= lang("status_viagem", "status_viagem") ?>
                    <?php
                    $opts = array(
                        'Confirmado' => lang('status_confirmado_para_venda'),
                        'Montando' => lang('status_montando_pacote'),
                        'Executado' => lang('status_viagem_executada') ,
                        'Cancelado' => lang('status_viagem_cancelada')
                    );
                    echo form_dropdown('status', $opts,  (isset($_POST['status']) ? $_POST['status'] : $produto->unit ), 'class="form-control" id="status"'); ?>
                </div>
                <div class="col-lg-12" style="margin-bottom: 25px;">
                    <?= lang("product", "product") ?>
                    <?php
                    $prod[''] = "";
                    foreach ($produtos as $produts) {
                        $prod[$produts->id] = $produts->name;
                    }
                    echo form_dropdown('produto', $prod, (isset($_POST['produto']) ? $_POST['produto'] : ($produto ? $produto->id : '')), 'class="form-control select" id="produto" placeholder="' . lang("select") . " " . lang("product") . '" required="required" style="width:100%"')
                    ?>
                </div>
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-md-3">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("data_do_dia", "doDia") ?>*
                            <?= form_input('doDia[]', '', 'class="form-control_data tip" required="required"', 'date') ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("hora_saida", "horaSaida") ?>
                            <?= form_input('horaSaida[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("data_ao_dia", "aoDia") ?>
                            <?= form_input('aoDia[]', '', 'class="form-control_data tip"', 'date') ?>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group" id="ui" style="margin-bottom: 0;">
                            <?= lang("hora_retorno", "horaRetorno") ?>
                            <?= form_input('horaRetorno[]', '', 'class="form-control_data tip" required="required"', 'time') ?>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <?= lang("vagas", "vagas") ?>
                        <?= form_input('vagas[]', (isset($_POST['vagas']) ? $_POST['vagas'] : ($produto ? $produto->quantidadePessoasViagem : '')), 'class="form-control_data tip mask_integer" required="required" ') ?>
                    </div>
                    <div class="col-md-1" style="float: left;margin-top: 25px;font-size: 20px;">
                        <i class="fa fa-plus addNewData" style="cursor: pointer;"></i>
                    </div>
                    <span id="new-linha-data"></span>
                    <div class="col-md-12" style="margin-bottom: -30px;">
                        <div style="text-align: right;margin-top: 20px;">
                            <?php echo form_submit('add_agenda_pacote', lang("save"), 'id="add_agenda_pacote" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                        </div>
                    </div>
                <?php }?>
            </div>
            <?= form_close(); ?>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <ul id="myTab" class="nav nav-tabs" style="text-align: center">
                    <li><a href="#abageral" class="tab-grey"><?= lang('availability') ?></a></li>

                    <?php if ($produto->id) {?>
                        <li><a href="#availability" class="tab-grey"><?= lang('calendar') ?></a></li>
                    <?php } ?>
                </ul>

                <div class="tab-content">
                    <!--Detalhes do pacote !-->
                    <div id="abageral" class="tab-pane fade in" style="padding: 0px;">

                        <div class="box">
                            <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('availability'); ?></h2></div>
                            <div class="box-content">
                                <div class="row">
                                    <?php if ($datasAgendadas) {?>
                                        <div class="col-lg-12">
                                            <table id="tbDatas" class="table table-condensed table-striped" style="cursor: pointer;">
                                                <tbody>
                                                <?php
                                                $mesAno = '';
                                                foreach ($datasAgendadas as $dataAgendada) {

                                                    $totalReservas = 0;
                                                    $totalVendas = 0;

                                                    //$dataAgendada = new AgendaViagem_model();
                                                    $mesAnoViagem =  strtoupper($this->sma->dataDeHojePorExtensoRetornoMensAno($dataAgendada->getDataSaida()));
                                                    ?>
                                                    <?php if ($mesAno != $mesAnoViagem){?>
                                                        <tr class="active">
                                                            <th colspan="2" style="text-align: left;border: 1px solid #ffffff;background: #ffffff;">
                                                                <h3> <?php echo $mesAnoViagem;?></h3>
                                                            </th>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr class="active">
                                                        <th class="col-md-12" colspan="12" style="text-align: left;background: #428bca;color: #ffffff;">
                                                            <?php echo strtoupper($this->sma->dataDeHojePorExtensoRetornoComSemana($dataAgendada->getDataSaida()))?>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td><?= lang('data_saida') ?><br/><?= form_input('dataSaidaData[]', '' . $this->sma->hrsd($dataAgendada->getDataSaida()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td><?= lang('hora') ?><br/><?= form_input('horaSaidaData[]', '' . $dataAgendada->getHoraSaida() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td><?= lang('data_retorno') ?><br/><?= form_input('dataRetornoData[]', '' . $this->sma->hrsd($dataAgendada->getDataRetorno()) . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled required="required"') ?></td>
                                                        <td><?= lang('hora') ?><br/><?= form_input('horaRetornoData[]', '' . $dataAgendada->getHoraRetorno() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td style="text-align: right;"><?= lang('vagas') ?><br/><?= form_input('vagasData[]', '' . $dataAgendada->getVagas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                                                        <td style="text-align: right;"><?= lang('orcamento') ?><br/><?= form_input('', '' . $dataAgendada->getTotalOrcamento() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td style="text-align: right;"><?= lang('faturadas') ?><br/><?= form_input('', '' . $dataAgendada->getTotalVendasFaturas() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td style="text-align: right;"><?= lang('disponivel') ?><br/><?= form_input('', '' . $dataAgendada->getTotalDisponvel() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #fdf59a;font-weight: bold;" disabled') ?></td>
                                                        <td style="text-align: right;"><?= lang('espera') ?><br/><?= form_input('', '' . $dataAgendada->getTotalListaEspera() . '', 'class="form-control_custom tip mask_integer" style="padding: 5px;background: #eeeeee" disabled') ?></td>
                                                        <td class="">
                                                            <br/>
                                                            <div class="text-center">
                                                                <div class="btn-group text-left">
                                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle"
                                                                            data-toggle="dropdown">Ações <span class="caret"></span>
                                                                    </button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">

                                                                        <?php if ($Owner || $Admin) { ?>
                                                                            <li><a href="<?php echo base_url();?>agenda/edit/<?php echo $dataAgendada->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i>Editar Agenda</a></li>
                                                                            <li><a href="<?php echo base_url();?>products/edit/<?php echo $dataAgendada->produto;?>"><i class="fa fa-map-signs"></i>Editar Serviço</a></li>
                                                                            <li><a id="<?php echo $dataAgendada->id;?>" class="visualizar-informacoes"><i class="fa fa-eye"></i>Ver Detalhes</a></li>
                                                                        <?php }?>

                                                                        <li class="divider"></li>
                                                                        <li><a href="<?= site_url('sales/relatorio_geral_ferroviario/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Lista de Passageiros PDF</a></li>
                                                                        <li><a href="<?= site_url('sales/relatorio_geral_ferroviario_excel/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-file-excel-o"></i> Lista de Passageiros Excel</a></li>


                                                                        <?php if ($Owner || $Admin) { ?>
                                                                            <li class="divider"></li>
                                                                            <li><a href="<?= site_url('reports/relatorioPassageirosProgramacao/'.$dataAgendada->id)?>"><i class="fa fa-users"></i><?= lang('Relatório Geral da Viagem') ?></a></li>
                                                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros_todos/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i> Lista de Passageiros</a></li>
                                                                        <?php }?>

                                                                        <?php if ($Owner || $Admin) { ?>
                                                                            <li class="divider"></li>
                                                                            <?php foreach ($transportes as $transporte) {?>
                                                                                <?php if ($transporte->status == 'ATIVO'){?>
                                                                                    <li>
                                                                                        <a href="<?= site_url('sales/montarPoltronas/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>">
                                                                                            <i class="fa fa-map-pin"></i> Configurar Assentos <?php echo $transporte->text;?>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <li class="divider"></li>
                                                                            <li><a href="<?= site_url('sales/pdf_hotel/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-bed"></i><?= lang('relatorio_room_list') ?></a></li>
                                                                            <li><a href="<?= site_url('sales/relatorio_geral_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id)?>" target="_blank"><i class="fa fa-list"></i><?= lang('relatorio_geral_de_passageiros') ?></a></li>
                                                                            <li><a href="<?= site_url('sales/relatorio_seguradora_de_passageiros/'.$dataAgendada->produto.'/'.$dataAgendada->id) ?>" target="_blank"><i class="fa fa-ambulance"></i><?= lang('relatorio_seguradora_de_passageiros') ?></a></li>
                                                                            <?php foreach ($transportes as $transporte) {?>
                                                                                <?php if ($transporte->status == 'ATIVO'){
                                                                                    $itens = $this->site->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                                                    $totalVendasItem = 0;

                                                                                    if ($itens) $totalVendasItem = count($itens);
                                                                                    ?>
                                                                                    <li>
                                                                                        <a href="<?= site_url('sales/relatorio_enviado_empresa_onibus/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                                                            <i class="fa fa-bus"></i>Lista de Passageiros para ANTT <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            <?php } ?>

                                                                            <li class="divider"></li>
                                                                            <?php
                                                                            $roomlists = $this->Roomlist_model->getAll($dataAgendada->id);
                                                                            ?>
                                                                            <?php foreach ($roomlists as $roomlist) {?>
                                                                                <li>
                                                                                    <a href="<?php echo site_url('roomlist/montar/'.$roomlist->id); ?>">
                                                                                        <i class="fa fa fa-cogs"></i><?= lang('configurar').' '.$roomlist->name;?>
                                                                                    </a>
                                                                                </li>
                                                                            <?php } ?>
                                                                            <?php foreach ($roomlists as $roomlist) {?>
                                                                                <li>
                                                                                    <a href="<?php echo site_url('roomlist/relatorio/'.$roomlist->id); ?>" target="_blank">
                                                                                        <i class="fa fa-print"></i><?= lang('print').' '.$roomlist->name;?>
                                                                                    </a>
                                                                                </li>
                                                                            <?php } ?>
                                                                            <li>
                                                                                <a href="<?php echo site_url('roomlist/adicionarRoomList/'.$dataAgendada->id); ?>" data-toggle="modal" data-target="#myModal">
                                                                                    <i class="fa fa-bed"></i><?= lang('adicionar_novo_room_list') ?>
                                                                                </a>
                                                                            </li>
                                                                            <li class="divider"></li>
                                                                            <?php foreach ($transportes as $transporte) {?>
                                                                                <?php if ($transporte->status == 'ATIVO'){
                                                                                    $itens = $this->site->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($dataAgendada->produto, $transporte->id, $dataAgendada->id);
                                                                                    $totalVendasItem = 0;

                                                                                    if ($itens) $totalVendasItem = count($itens);
                                                                                    ?>
                                                                                    <li>
                                                                                        <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque/' . $dataAgendada->produto.'/'.$transporte->id.'/'.$dataAgendada->id) ?>" target="_blank">
                                                                                            <i class="fa fa-hand-stop-o"></i>Lista de Embarque <?php echo $transporte->text. ' ('.$totalVendasItem.')';?>
                                                                                        </a>
                                                                                    </li>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <?php if ($Owner || $Admin) { ?>
                                                            <td class="text-center"><i class="fa fa-times delAttr" id="<?php echo $dataAgendada->id;?>" produto="<?php echo $dataAgendada->getProduto();?>" ></i></td>
                                                        <?php } else {?>
                                                            <td></td>
                                                        <?php } ?>
                                                    </tr>
                                                    <?php $mesAno = $mesAnoViagem; ?>
                                                <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php if ($produto->id) {?>
                        <div id="availability" class="tab-pane fade in">
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-lg-12">
                                    <div class="panel panel-info">
                                        <div class="panel-heading"><i class="fa-fw fa fa-calendar-o"></i> <?= lang("availability") ?></div>
                                        <div class="panel-body">
                                            <iframe src="<?=base_url().'agenda/availability/'.$produto->id;?>" style="width: 100%;height: 800px;border:none;" ></iframe>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#produto').change(function (event) {
            if ($(this).val() !== '') window.location = site.base = 'agenda/abrir/'+$(this).val();
        });

        $("#status").change(function (event){
           buscarProdutos();
        });

        function buscarProdutos() {

            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>agenda/buscarProdutos",
                data: {
                    status: $('#status').val()
                },
                dataType: 'json',
                success: function (produtos) {

                    $('#produto').empty();
                    var option = $('<option/>');

                    option.attr({ 'value': '' }).text('Selecione uma opção');
                    $('#produto').append(option);

                    $(produtos).each(function( index, produto ) {
                        var option = $('<option/>');

                        option.attr({ 'value': produto.id }).text(produto.name);
                        $('#produto').append(option);
                    });

                    $('#produto').select2({minimumResultsForSearch: 7});

                }
            });
        }

        $('.delAttr').click(function (event) {
            if (confirm('Deseja realmente excluir o lançamento?')) {
                let id = $(this).attr('id');
                let produto = $(this).attr('produto');

                window.location = site.base = 'agenda/excluir/' + id + '/' + produto;
            }
        });

        $('.visualizar-informacoes').click(function (event) {

            let id = $(this).attr('id');

            if ($( ".visualizar-informacoes-item-"+id).is( ":visible" ) ) {
                $('.visualizar-informacoes-item-'+id).hide(300);
            } else {
                $('.visualizar-informacoes-item-'+id).show(300);
            }
        });

        $('.addNewData').click(function (event){
            $.ajax({
                url: site.base_url + "agenda/view_lancamento_agenda_data",
                dataType: 'html',
                type: 'get',
            }).done(function (html) {
                $('#new-linha-data').append(html);

                $('.removeItemData').click(function (event) {
                    $(this).parent().parent().remove();
                });

                adicionarCamposObrigatorioAoFormulario();
            });
        });
    });

    function adicionarCamposObrigatorioAoFormulario() {

        $('form[data-toggle="validator"]').data('bootstrapValidator', null);
        $('form[data-toggle="validator"]').bootstrapValidator();

        //$('form[data-toggle="validator"]').data('bootstrapValidator').destroy();

        $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Digite / selecione um valor', submitButtons: 'input[type="submit"]' });

        let fields = $('.form-control');
        $.each(fields, function() {

            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#'+id;

            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                if ($("label[for='" + id + "']").html() !== undefined) {
                    let label =  $("label[for='" + id + "']").html().replace('*', '');
                    $("label[for='" + id + "']").html(label + ' *');
                    $(document).on('change', iid, function () {
                        $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                    });
                }
            }
        });
    }
</script>

