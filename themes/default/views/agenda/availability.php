<link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
<link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
<link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">

<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

<style>
    .table th {
        text-align: center;
    }

    .table td {
        padding: 2px;

    }

    .table td .table td:nth-child(odd) {
        text-align: left;
        color: #0b0b0b;
    }

    .table td .table td:nth-child(even) {
        text-align: right;
    }

    .table a:hover {
        text-decoration: none;
    }

    .cl_wday {
        text-align: center;
        font-weight: bold;
    }

    .cl_equal {
        width: 14%;
    }

    td.day {
        width: 14%;
        padding: 0 !important;
        vertical-align: top !important;
    }

    .day_num {
        width: 100%;
        text-align: left;
        cursor: pointer;
        margin: 0;
        padding: 8px;
    }

    .day_num:hover {
        background: #F5F5F5;
    }

    .content {
        width: 100%;
        text-align: left;
        color: #428bca;
        padding: 8px;
    }

    .highlight {
        color: #0088CC;
        font-weight: bold;
    }

    @media print {
        .noprint {
            display: none;
        }
    }
</style>
<div class="box" id="conteudoParaImprimir">
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12" style="text-align: center;margin-bottom: 25px;">
                <h1><?=$product->name;?></h1>
            </div>
            <div class="col-lg-12">
                <div>
                    <?php echo $calender; ?>
                </div>
            </div>
            <div class="col-lg-12">

                <button onclick="abrirEmTelaCheia()" class="btn btn-secondary noprint" id="botaoImprimir">Abrir Em Tela Cheia</button>

                <button onclick="imprimirDiv('conteudoParaImprimir')" class="btn btn-primary noprint" id="botaoImprimir">Imprimir Conteúdo</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {});

    function abrirEmTelaCheia () {
        var url = "<?=base_url().'agenda/availability/'.$product->id;?>";
        window.open(url, '_blank');
    }

    function imprimirDiv(divId) {

        var conteudo = document.getElementById(divId).innerHTML;
        var janelaImpressao = window.open('', '', 'height=600,width=800');

        // Copiar estilos da página atual
        var styles = document.head.innerHTML;

        janelaImpressao.document.write('<html><head><title>Impressão</title>');
        janelaImpressao.document.write(styles);  // Inserir os estilos
        janelaImpressao.document.write('</head><body>');
        janelaImpressao.document.write(conteudo);
        janelaImpressao.document.write('</body></html>');

        removerEstilo(janelaImpressao.document, '<?= $assets ?>styles/theme.css');
        removerEstilo(janelaImpressao.document, '<?= $assets ?>styles/style.css');

        janelaImpressao.document.close();
        janelaImpressao.focus();
        janelaImpressao.print();
        janelaImpressao.close();
    }

    // Função para remover o arquivo CSS
    function removerEstilo(janelaImpressao, href) {
        var links = janelaImpressao.getElementsByTagName("link");

        for (var i = 0; i < links.length; i++) {
            if (links[i].getAttribute("href") === href) {
                links[i].parentNode.removeChild(links[i]);
            }
        }
    }
</script>
