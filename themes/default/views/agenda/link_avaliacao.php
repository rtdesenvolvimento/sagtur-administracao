<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('link_avaliacao'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-xs-6" style="text-align: center;">
                    <?php $qrimage = $this->sma->qrcode_pdf_sales('link', urlencode($url), 9, FALSE, $programacao->id.'_'.time().'_'.$this->session->userdata('cnpjempresa')); ?>
                    <img src="data:image/png;base64,<?php echo $qrimage;?>"
                         alt="<?= $product->name ?>"/><br/>
                </div>
                <div  class="col-xs-6">
                    <h1 style="font-size: 25px;"><?=$product->name;?></h1>
                    <hr>
                    <b style="font-size: 12px;"> URL:<a href="<?=$url;?>" target="_blank"><?=$url;?></a></b><br/>
                    <a class="btn btn-success mobile_btn" id="copy"
                       style="background: #77b43f;margin-top: 10px;color: #ffffff;font-size: 1.5rem;width: 100%;"> COPIAR URL </a>
                    <hr><br/>
                    <p style="font-size: 2rem;line-height: 22px;">Para salvar o código QR em seu computador, clique com o botão direito do mouse na imagem e selecione a opção Salvar imagem como...</p>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="text-align: left;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="application/javascript">

    $(document).ready(function () {
        $('#copy').click(function (){
            copyTextToClipboardQRCode('<?=$url;?>', '');
        });
    });

    function copyTextToClipboardQRCode(text) {

        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            return;
        }

        document.body.focus();

        navigator.clipboard.writeText(text)
            .then(() => {
                $('.mobile_btn').css('background', '#28a745');
                $('.mobile_btn').html('COPIADO!')
                console.log('Texto copiado com sucesso para a área de transferência:', text);
            })
            .catch(err => {
                console.error('Ocorreu um erro ao copiar o texto para a área de transferência:', err);
            });
    }

    function fallbackCopyTextToClipboard(text) {

        // Foca no documento
        document.body.focus();

        var textArea = document.createElement("textarea");
        textArea.value = text;

        // Adiciona o textarea ao DOM
        document.body.appendChild(textArea);

        // Seleciona e copia o texto dentro do textarea
        textArea.select();
        document.execCommand('copy');

        // Remove o textarea do DOM
        document.body.removeChild(textArea);

        $('.mobile_btn').css('background', '#28a745');
        $('.mobile_btn').html('COPIADO!')
    }
</script>
