
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_blue.css">

<style>
    :root {
        --fc-small-font-size: 1em;
        --fc-list-event-dot-width: 10px;
        --fc-list-event-hover-bg-color: #42a5f5;
    }

    .fc .fc-button-primary {
        background-color: #428BCA;
        border-color: #428BCA;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('consulta_disponibilidade_header'); ?></h2>
    </div>
    <div class="box-content">

        <div class="row">
            <div class="col-md-12">
                <div class="col-lg-6">
                    <?= lang("date", "date") ?>
                    <input class="flatpickr form-control tip" type="text">
                </div>
                <div class="col-lg-6">
                    <?= lang("tours", "tours") ?>
                    <select id="tours" name="tours" class="form-control select" style="width: 100%;">
                        <option value=""><?= lang("select") . " " . lang("tours");?></option>
                        <?php foreach ($products as $product) {?>
                            <option value="<?=$product->id;?>"><?=$product->name;?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 25px;">
                <div class="col-lg-12">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal" id="cal_modal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="close_bus" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_venda') ?></h4>
            </div>
            <iframe id="iframe_new_sale" width="598px" height="600px;" frameborder="0" scrolling="no"></iframe>
        </div>
    </div>
</div>

<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr@4.6.13/dist/l10n/pt.js"></script>

<script type="text/javascript">

    var calendar = null;
    var calendarEl = document.getElementById('calendar');

    var optional_config = {
        enableTime: false,
        dateFormat: "Y-m-d",
        locale : "pt",
        inline: false,
        minDate: "today",
        weekNumbers: false,

        onChange: function(selectedDates, dateStr, instance) {
            console.log(selectedDates);
            console.log(dateStr);
            console.log(instance);

            carregar_agenda('listDay', dateStr);
        },
    };

    $(document).ready(function () {

        carregar_agenda('listMonth');

        $('#tours').change(function (){
            carregar_agenda('listWeek');
        });

        setTimeout(function (){
            $('.flatpickr-monthDropdown-months').select2('destroy');
            $('.flatpickr-calendar.inline').css('margin-left', '15px');
            $('.fc-toolbar-title').css('text-transform', 'uppercase');
            //$('#main-menu-act').click();
        }, 200);
    });

    var fp = flatpickr(".flatpickr",  optional_config);

    function carregar_agenda(initialView, default_date) {

        if (calendar !== null) {
            calendar.destroy();
        }

        let productsID = $('#tours').val();

        if (default_date === undefined) {
            default_date = '<?=date('Y-m-d');?>'
        }

        calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: initialView,
            initialDate: default_date,
            timeZone: 'local',
            showNonCurrentDates: false,
            nowIndicator: true,
            businessHours: false,
            //editable: true,
            //selectable: true,
            //businessHours: true,
            locale: 'pt-br',
            views: {
                listDay: { buttonText: '<?=lang('list_by_day');?>' },
                listWeek: { buttonText: '<?=lang('list_by_week');?>' },
                listMonth: { buttonText: '<?=lang('list_by_month');?>' },
                listYear: { buttonText: '<?=lang('list_by_year');?>' }
            },
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                //right: 'dayGridMonth,timeGridWeek,timeGridDay,listDay,listWeek,listMonth,listYear'
                right: 'listDay,listWeek,listMonth,timeGridWeek,timeGridDay,dayGridMonth'

            },
            eventClick: function(info) {
                info.jsEvent.preventDefault();

                window.open(info.event.url, '_blank');

                /*
                if (info.event.url) {
                    $('#iframe_new_sale').attr('src', info.event.url).on('load', function() {

                        $('#iframe_new_sale').contents().find('.page_header').hide();
                        $('#iframe_new_sale').contents().find('.header-contador').hide();

                        $('#iframe_new_sale').contents().find('.wizard-form').css('overflow-y', 'scroll');
                        $('#iframe_new_sale').contents().find('#middle-wizard').css('height', '175%');

                        $('#cal_modal').modal('show');

                    });
                }
                 */
            },
            events: site.base_url+'agenda/search_tours/' + productsID,
        });

        calendar.render();

        carregar_calendario();
    }

    function carregar_calendario() {

        let productsID = $('#tours').val();

        optional_config.enable = [];

        $.ajax({
            type: "GET",
            url: site.base_url+'agenda/search_tours/'+productsID,
            data: {},
            dataType: 'json',
            success: function (events) {

                $(events).each(function(index, event) {
                    optional_config.enable.push({from: event.start, to: event.end})
                });

                fp.destroy();
                fp = flatpickr(".flatpickr",  optional_config);

                $('.flatpickr-calendar.inline').css('margin-left', '15px');
                $('.fc-toolbar-title').css('text-transform', 'uppercase');
            }
        });
    }
</script>

