<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('habilitar_marcacao_assento'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agenda/habilitar_marcacao_assento/" . $agenda->id, $attrib); ?>
        <div class="modal-body">
            <input type="hidden" value="<?php echo $agenda->id; ?>" name="id"/>
            <input type="hidden" value="<?php echo $agenda->produto; ?>" name="produto" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', $product->name, 'class="form-control tip" id="name" readonly'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("data_do_dia", "doDia"); ?>
                        <?php echo form_input('doDia', $agenda->dataSaida, 'class="form-control tip" readonly required="required" id="doDia"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("data_ao_dia", "aoDia"); ?>
                        <?php echo form_input('aoDia', $agenda->dataRetorno, 'class="form-control tip" readonly id="aoDia"', 'date'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_saida", "horaSaida"); ?>
                        <?php echo form_input('horaSaida', $agenda->horaSaida, 'class="form-control tip" id="horaSaida"', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("hora_retorno", "horaRetorno"); ?>
                        <?php echo form_input('horaRetorno', $agenda->horaRetorno, 'class="form-control tip" id="horaRetorno"', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("vagas", "vagas"); ?>
                        <?php echo form_input('vagas', $agenda->vagas, 'class="form-control tip mask_integer" id="vagas" required="required"', 'number'); ?>
                    </div>
                </div>
            </div>
            <?php if ($exibir_marcacao_assento) {?>
               <div class="row" style="margin-top: 20px;">
                   <div class="col-md-12">
                       <div class="panel panel-info">
                           <div class="panel-heading"><i class="fa-fw fa fa-user"></i> <?= lang("info_marcacao_assento", "details") ?></div>
                           <div class="panel-body">
                               <div class="col-md-12">
                                   <div class="form-group all">
                                       <?php echo form_checkbox('habilitar_marcacao_area_cliente', '1', $agenda->habilitar_marcacao_area_cliente, ''); ?>
                                       <label for="attributes" class="padding05"><?= lang('habilitar_marcacao_area_cliente'); ?></label>
                                   </div>
                               </div>
                               <div class="col-md-12">
                                   <div class="form-group all">
                                       <?php echo form_checkbox('permitir_marcacao_dependente', '1', $agenda->permitir_marcacao_dependente, ''); ?>
                                       <label for="attributes" class="padding05"><?= lang('permitir_marcacao_dependente'); ?></label>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <?= lang('data_inicio_marcacao', 'data_inicio_marcacao'); ?>
                                       <input type="date" name="data_inicio_marcacao" value="<?=$agenda->data_inicio_marcacao;?>" class="form-control tip" id="data_inicio_marcacao">
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <div class="form-group">
                                       <?= lang('data_final_marcacao', 'data_final_marcacao'); ?>
                                       <input type="date" name="data_final_marcacao" value="<?=$agenda->data_final_marcacao;?>" class="form-control tip" id="data_final_marcacao">
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
            <?php } else { ?>
                <div class="row">
                    <div class="col-md-12" style="text-align: center;">
                        <div class="form-group">
                            <h3 style="text-transform: uppercase;color: red;">Este pacote não possui Ônibus com Layout Configurado para Realizar Check-in</h3>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('habilitar_marcacao_assento', lang('habilitar_marcacao_assento'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
