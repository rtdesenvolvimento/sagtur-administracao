<!DOCTYPE html>

<style>
    .img-fluid {
        width: 332px!important;
        height: 250px!important;
    }
</style>
<div data-role="page" data-theme="a" data-url="servico-view" id="servico-view">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h2 style="font-weight: bold;text-shadow: none;">Viagem</h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <h1>
            <img src="<?php echo base_url().'/assets/uploads/'.$product->image; ?>" alt="<?php echo $product->name;?>" class="img-fluid"/>
        </h1>
        <h1><?=$product->name?></h1>
        <div data-role="collapsibleset" data-content-theme="a" data-iconpos="right" id="set">
            <?php if ($product->product_details){?>
                <div data-role="collapsible" data-collapsed="true">
                    <h3><?=lang('product_details');?></h3>
                    <p><?=$product->product_details?></p>
                </div>
            <?php } ?>
            <?php if ($product->itinerario){?>
                <div data-role="collapsible" data-collapsed="true">
                    <h3><?=lang('Roteiro');?></h3>
                    <p><?=$product->itinerario?></p>
                </div>
            <?php } ?>
            <?php if ($product->oqueInclui){?>
                <div data-role="collapsible" data-collapsed="true">
                    <h3><?=lang('O Que Inclui / Não Inclui');?></h3>
                    <p><?=$product->oqueInclui?></p>
                </div>
            <?php } ?>
            <?php if ($product->valores_condicoes){?>
                <div data-role="collapsible" data-collapsed="true">
                    <h3><?=lang('Valores e Condições');?></h3>
                    <p><?=$product->valores_condicoes?></p>
                </div>
            <?php } ?>
            <?php if ($product->details){?>
                <div data-role="collapsible" data-collapsed="true">
                    <h3><?=lang('+ Informações');?></h3>
                    <p><?=$product->details?></p>
                </div>
            <?php } ?>
        </div>
    </div>
    <div data-role="footer" data-position="fixed">
        <div data-role="navbar">
            <ul>
                <li>
                    <a href="<?php echo base_url().'appcompra/pdf/'.$inv->id.'?token='.$this->session->userdata('cnpjempresa'); ?>" target="_blank"><button data-icon="eye">Baixar Voucher</button></a>
                </li>
            </ul>
        </div>
    </div>
</div>