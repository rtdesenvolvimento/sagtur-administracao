<!DOCTYPE html>
<div data-role="page" data-theme="a" data-url="minhas-faturas" id="minhas-faturas">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h2 style="font-weight: bold;text-shadow: none;">FATURAS</h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <form class="ui-filterable">
            <input id="search-faturas" data-type="search" placeholder="Digite os termos da busca...">
        </form>
        <ul data-role="listview" id="listview-faturas" data-filter="true" data-inset="true" data-input="#search-faturas">
            <?php if (empty(!$sales)) { ?>
                <?php foreach ($sales as $sale) {
                    $faturas = $this->site->getParcelasFaturaByContaReceber($sale->sale_id);

                    $totalFatura = 0;
                    $totalPago = 0;
                    $totalPagar = 0;

                    foreach ($faturas as $fatura) {
                        $totalFatura = $totalFatura + $fatura->valorfatura;
                        $totalPago = $totalPago + $fatura->valorpago;
                        $totalPagar = $totalPagar + $fatura->valorpagar;
                    }
                    ?>

                    <?php
                    $class_ui = '';
                    if ($sale->payment_status == 'due') {?>
                        <?php $class_ui = 'ui_pagar';?>
                    <?php } else if ($sale->payment_status == 'parcial') {?>
                        <?php $class_ui = 'ui_parcial';?>
                    <?php } else if ($sale->payment_status == 'paid') { ?>
                        <?php $class_ui = 'ui_pago';?>
                    <?php } ?>

                    <li data-role="list-divider" style="background: #f7f7f8;"><?=$sale->produtc_name?><span class="ui-li-count <?=$class_ui;?>"><?=lang($sale->payment_status)?></span></li>
                    <li>
                        <a href="<?=base_url().'myapp/faturas/'.$sale->customerClient.'/'.$sale->sale_id;?>" data-transition="flow">
                            <img style="width: 70px;" src="<?php echo $assets ?>myapp/img/faturas.png">
                            <h3>Fatura <?php echo $this->sma->formatMoney($totalFatura);?></h3>
                            <p><strong>Pago <?php echo $this->sma->formatMoney($totalPago);?></strong></p>
                            <p><strong>Pagar <?php echo $this->sma->formatMoney($totalPagar);?></strong></p>
                            <p class="ui-li-aside"><strong><?=$sale->tipo_cobranca;?> em <?=$sale->condicao_pagamento;?></strong></p>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>