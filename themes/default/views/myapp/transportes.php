<!DOCTYPE html>
<div data-role="page" data-theme="a" data-url="transportes" id="transportes">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h2 style="font-weight: bold;text-shadow: none;">TRANSPORTES</h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <h5 style="text-align: center;">Selecione o Transporte:</h5>
        <ul data-role="listview" id="listview-transportes" data-filter="true" data-inset="true" data-input="#search-transportes">
            <?php foreach ($tiposTransporte as $tipoTransporte) {
                $tipoTransporteEncontrado =  $this->ProdutoRepository_model->getTransportesRodoviario($product_id, $tipoTransporte->id);?>
                <?php if (!empty($tipoTransporteEncontrado) ){?>
                    <?php foreach ($tipoTransporteEncontrado as $transporte) {
                        $ativo = $transporte->status == 'ATIVO' ? true  : false;?>
                        <?php if ($ativo){?>
                            <li><a href="<?=base_url().'myapp/marcar_assento/'.$sale_id.'/'.$customer_id.'/'.$tipoTransporte->id;?>" data-transition="slidedown"><?php echo $transporte->text;?></a></li>
                        <?php } ?>
                    <?php }?>
                <?php }?>
            <?php } ?>
        </ul>
    </div>
</div>