<!DOCTYPE html>

<div data-role="page" data-theme="a" data-url="meus-pagamentos" id="meus-pagamentos">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h4 style="font-weight: bold;text-shadow: none;">PARCELAS</h4>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <?php if (empty(!$sales)) { ?>
            <?php foreach ($sales as $sale) {
                $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($sale->sale_id); ?>
                <div style="text-align: center;">
                    <h2><?=$sale->produtc_name?></h2>
                </div>
                <div data-role="collapsibleset" data-content-theme="b" data-iconpos="right" id="set">
                    <?php if(!empty($faturas)) {?>
                        <?php foreach ($faturas as $fatura){
                            $parcela = $this->financeiro_model->getParcelaOneByFatura($fatura->id);
                            $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fatura->id);
                            $tipoCobranca = $this->financeiro_model->getTipoCobrancaById($fatura->tipoCobrancaId); ?>
                            <div data-role="collapsible" data-collapsed="true">
                                <h3><?php echo $fatura->numeroparcela.'X';?> <div><?php echo lang($fatura->status);?></div></h3>
                                <table data-role="table" id="movie-table" data-filter="true" data-input="#filterTable-input" class="ui-responsive">
                                    <thead>
                                    <tr>
                                        <th data-priority="2">Vencimento</th>
                                        <th data-priority="3">Cobrança</th>
                                        <th data-priority="4">Vencimento</th>
                                        <th data-priority="5">Pagar</th>
                                        <th data-priority="6">Pago</th>
                                        <th data-priority="7">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                                        <td>
                                            <?php echo $fatura->tipoCobranca;?>
                                            <br/><?php echo $fatura->reference;?>
                                            <?php if (count($cobranca) > 0 && $tipoCobranca->tipo == 'boleto') {?>
                                                <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo $this->sma->formatMoney($fatura->valorfatura);?></td>
                                        <td><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                                        <td><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                                        <td>
                                            <?php echo lang($fatura->status);?>
                                            <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL' || $fatura->status == 'QUITADA') {?>
                                                <?php if ($cobranca->link) {?>
                                                    <?php if ($fatura->status == 'QUITADA') {?>
                                                        <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-check"></i> Ver comprovante de pagamento</a>
                                                    <?php } else { ?>
                                                        <?php if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') {?>
                                                            <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Boleto</a>
                                                        <?php } else if ($tipoCobranca->tipo == 'pix') { ?>
                                                            <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Pagar com Pix</a>
                                                        <?php } else { ?>
                                                            <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Fatura p/ Pagamento</a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php  } else if ($cobranca->checkoutUrl) {?>
                                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                                        <?php if (  ($cobranca->integracao == 'pagseguro' && $tipoCobranca->tipo == 'carne_cartao') ||
                                                            ($cobranca->integracao == 'mercadopago' && $tipoCobranca->tipo == 'link_pagamento') ||
                                                            ($cobranca->integracao == 'valepay' && $tipoCobranca->tipo == 'link_pagamento')) {?>
                                                            <?php if ($cobranca->integracao == 'valepay' && $tipoCobranca->tipo == 'link_pagamento') {?>
                                                                <br/><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Link de Pagamento</a>
                                                            <?php } else {?>
                                                                <br/>
                                                                <span>Copie o Link Abaixo:<br/></span>
                                                                <span><?php echo $cobranca->checkoutUrl?></span>
                                                            <?php } ?>
                                                        <?php } else {?>
                                                            <br/><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-money"></i> Pagar Agora</a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>