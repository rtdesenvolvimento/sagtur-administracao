<!DOCTYPE html>
<div data-role="page" data-theme="a" data-url="proximas_viagens" id="proximas_viagens">
    <div data-role="header" data-position="fixed" style="background: <?=$this->Settings->theme_color;?>;color: #ffffff;">
        <h2 style="font-weight: bold;text-shadow: none;">VIAGENS</h2>
        <a href="#home" data-icon="home" data-display="overlay" data-iconpos="notext">Menu</a>
    </div>
    <div role="main" class="ui-content">
        <h5 style="text-align: center;">Selecione a Viagem para Check-In:</h5>
        <ul data-role="listview" id="listview-proximas_viagens" data-filter="true" data-inset="true" data-input="#search-proximas_viagens">
            <?php if (empty(!$sales)) { ?>
                <?php foreach ($sales as $sale) {
                    if ($this->MyappService_model->onibus_ativo_marcacao($sale)) {

                        $local = $this->site->getLocalEmbarqueRodoviarioById($sale->product_id, $sale->localEmbarqueId);
                        $dataEmbarque = $sale->dtSaida;
                        $dataRetorno  = date('d/m/Y', strtotime($sale->dtRetorno)).' às  ' . $this->sma->hf($sale->hrRetorno);
                        $horaEmbarque = '<span style="font-weight: bold;"> às '.$this->sma->hf($sale->hrSaida).'</span>';

                        $itens = $this->MyappRepository_model->getAllInvoiceItemsBySaleID($sale->sale_id);

                        if ($local->dataEmbarque != null && $local->dataEmbarque != '0000-00-00') {
                            $dataEmbarque = $local->dataEmbarque;
                        }

                        if ($local->horaEmbarque != null && $local->horaEmbarque != '00:00:00.0'&& $local->horaEmbarque != '00:00:00') {
                            $horaEmbarque = '  <span style="font-weight: bold;"> às '.$this->sma->hf($local->horaEmbarque).'</span>';
                        }
                        ?>
                        <li data-role="list-divider" style="background: #f7f7f8;">
                            <svg xmlns="http://www.w3.org/2000/svg" id="Camada_2" viewBox="0 0 322.51 383.23" height="16px" width="16px" style="fill: <?=$this->Settings->theme_color;?>"><g id="Camada_1-2"><g><path d="M132.13,383.23c-7.42-2.5-12.85-6.88-14.55-14.97-.98-4.66,.24-8.85,2.16-13.01,11.18-24.05,22.36-48.11,33.41-72.22,.99-2.16,1.52-4.77,1.42-7.13-.81-19.06-2.19-38.11-2.63-57.17-.26-11.43,.79-22.9,1.41-34.34,.56-10.21,1.4-20.4,2.01-30.61,.41-6.91,2.68-12.68,8.42-17.07,5.73-4.39,11.08-9.28,16.32-14.48-.85,.41-1.71,.79-2.54,1.24-8.14,4.33-16.32,8.58-24.36,13.08-1.57,.88-3.08,2.54-3.79,4.2-7.05,16.48-13.87,33.07-20.88,49.57-2.81,6.6-8.57,10-15.44,9.54-6.18-.41-11.9-4.62-13.25-10.85-.72-3.34-.69-7.4,.56-10.51,7.67-19.19,15.61-38.28,23.92-57.2,1.43-3.26,4.58-6.33,7.7-8.17,9.88-5.79,20.12-10.96,30.22-16.37,1.08-.58,2.14-1.21,3.06-2.27-1.72,.42-3.45,.84-5.78,1.4,.25-1.43,.34-2.59,.67-3.67,2.82-9.35,11.46-16.24,21.21-15.87,14.45,.55,28.9,1.39,43.29,2.81,12.7,1.25,21.05,11.15,20.27,23.83-.31,4.99,1.08,8.42,4.38,11.88,4.82,5.04,9.1,10.59,13.86,15.71,1.42,1.52,3.5,2.8,5.51,3.33,13.97,3.68,28.02,7.04,41.99,10.68,7.89,2.06,12.41,8.6,11.74,16.42-.63,7.36-6.24,13.08-13.89,13.79-2.17,.2-4.48-.23-6.63-.76-15.72-3.87-31.39-7.92-47.12-11.74-5.25-1.28-9.11-4.35-12.95-9.4-.31,4.33-.55,7.59-.78,10.84-1.32,18.65-2.66,37.29-3.91,55.94-.08,1.17,.48,2.42,.88,3.58,4.56,13.57,9.6,27.01,13.54,40.76,2.2,7.69,2.96,15.87,3.73,23.88,2.26,23.7,4.32,47.43,6.23,71.16,1.12,13.93-1.53,18.14-14.13,24.19h-7.49c-8.68-2.93-14.12-8.49-14.92-17.99-2.03-24.1-4.11-48.2-6.22-72.3-.48-5.46-.29-11.16-1.9-16.29-6.03-19.25-12.65-38.31-19.19-57.4-.35-1.03-1.9-1.99-3.08-2.32-1.72-.48-3.61-.35-5.68-.5,0,1.61-.05,2.83,0,4.05,.94,19.06,1.79,38.12,2.9,57.17,.34,5.88-.53,11.28-3.06,16.65-11.46,24.34-22.87,48.71-33.94,73.22-3.28,7.26-7.41,13.12-15.22,15.71h-7.49Z"></path><path d="M17.6,383.23c-8.25-4.15-9.77-9.15-5.05-17.58-1.63-.79-3.27-1.56-4.89-2.37-6.72-3.35-9.5-10.19-6.41-16.95,14.31-31.24,28.72-62.44,43.19-93.61,3.14-6.76,10.14-9.2,17-6.22,5.34,2.32,10.59,4.84,16.2,7.42,7.31-15.82,14.5-31.38,21.58-46.69,4.65,1.67,9.14,3.29,14,5.03-.21,.56-.51,1.56-.94,2.51-6.56,14.25-13.11,28.51-19.74,42.73-.88,1.9-.99,3.12,.68,4.85,4.19,4.36,4.41,9.49,1.92,14.87-8.78,18.99-17.53,37.99-26.29,56.98-5.16,11.19-10.31,22.39-15.47,33.59-4.33,9.38-10.5,11.64-20.02,7.36-.9-.4-1.82-.74-3.3-1.34-.76,4.83-3.63,7.82-7.97,9.43h-4.49Z"></path><path d="M177.67,33.47C177.78,14.73,192.88-.15,211.64,0c18.42,.15,33.47,15.43,33.33,33.85-.14,18.64-15.41,33.64-34.08,33.47-18.51-.17-33.33-15.27-33.22-33.85Z"></path></g></g></svg>
                            <?=$this->sma->dataToAreaCliente($dataEmbarque)?> <?=$horaEmbarque?>
                        </li>
                        <li>
                            <a href="<?=base_url().'myapp/transportes/'.$customer->id.'/'.$sale->sale_id;?>" data-transition="slidedown">
                                <h2><?=$sale->produtc_name?></h2>
                                <p>COD. VENDA: <?=$sale->reference_no?></p>
                                <p>Retorno <?=$dataRetorno?></p>
                                <?php if ($sale->localEmbarque) { ?>
                                    <p class="ui-li-aside"><strong>Embarque <?=$sale->localEmbarque?></strong></p>
                                <?php } ?>
                                <?php if(!empty($itens)) {?>
                                    <hr/>
                                    <?php foreach ($itens as $item) {?>
                                        <?php if ($item->poltronaClient) {?>
                                            <p style="font-weight: bold;"><?=$item->customerClientName;?> | Assento <?=$item->poltronaClient;?></p>
                                        <?php } else { ?>
                                            <p style="font-weight: bold;"><?=$item->customerClientName;?> | AINDA NÃO MARCADO</p>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>