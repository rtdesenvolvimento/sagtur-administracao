<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="theme-color" content="#e58800"/>

    <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link href="<?= $assets ?>styles/theme.css" rel="stylesheet"/>
    <link href="<?= $assets ?>styles/style.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js"></script>
    <!--[if lt IE 9]>
    <script src="<?= $assets ?>js/jquery.js"></script>
    <![endif]-->
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <?php if ($Settings->user_rtl) { ?>
        <link href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css" rel="stylesheet"/>
        <link href="<?= $assets ?>styles/style-rtl.css" rel="stylesheet"/>
        <script type="text/javascript">
            $(document).ready(function () { $('.pull-right, .pull-left').addClass('flip'); });
        </script>
    <?php } ?>
    <script type="text/javascript">
        $(window).load(function () {
            $("#loading").fadeOut("slow");
        });
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-165016020-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-165016020-1');
    </script>

</head>

<body>
<noscript>
    <div class="global-site-notice noscript">
        <div class="notice-inner">
            <p><strong>JavaScript seems to be disabled in your browser.</strong><br>You must have JavaScript enabled in
                your browser to utilize the functionality of this website.</p>
        </div>
    </div>
</noscript>
<div id="loading"></div>
<div id="app_wrapper">
    <header id="header" class="navbar">
        <div class="container">
            <!--
            <a class="navbar-brand" href="<?= site_url() ?>">
            	<span class="logo">
            		<?php echo '<img src="' . base_url('assets/uploads/logos/logo.png') . '" alt="' . $Settings->site_name .'" style="margin-bottom:10px;width: 225px;height:30px;" />' ;?>
            	</span>
            </a>
            !-->
            <style>
                .i-logo {
                    margin-right: 0px;
                }
            </style>
            <a class="navbar-brand" href="<?= site_url() ?>"><span class="logo"><i class="fa fa-globe fa-spin i-logo"></i>  <?= $Settings->site_name ?></span></a>

            <div class="btn-group visible-xs pull-right btn-visible-sm">
                <button class="navbar-toggle btn" type="button" data-toggle="collapse" data-target="#sidebar_menu">
                    <span class="fa fa-bars"></span>
                </button>
                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>" class="btn">
                    <span class="fa fa-user"></span>
                </a>
                <a href="<?= site_url('logout'); ?>" class="btn">
                    <span class="fa fa-sign-out"></span>
                </a>
            </div>
            <div class="header-nav">
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown">
                        <a class="btn account dropdown-toggle" data-toggle="dropdown" href="#">
                            <img alt="" src="<?= $this->session->userdata('avatar') ? site_url() . 'assets/uploads/avatars/thumbs/' . $this->session->userdata('avatar') : base_url('assets/images/' . $this->session->userdata('gender') . '.png'); ?>" class="mini_avatar img-rounded">

                            <div class="user">
                                <span><?= lang('welcome') ?> <?= $this->session->userdata('username'); ?></span>
                            </div>
                        </a>
                        <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id')); ?>">
                                    <i class="fa fa-user"></i> <?= lang('profile'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('users/profile/' . $this->session->userdata('user_id') . '/#cpassword'); ?>"><i class="fa fa-key"></i> <?= lang('change_password'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= site_url('logout'); ?>">
                                    <i class="fa fa-sign-out"></i> <?= lang('logout'); ?>
                                </a>
                            </li>
                        </ul>
                        <?php } ?>
                    </li>
                </ul>
                <ul class="nav navbar-nav pull-right">
                    <li class="dropdown hidden-xs"><a class="btn tip" title="<?= lang('dashboard') ?>" data-placement="bottom" href="<?= site_url('welcome') ?>"><i class="fa fa-home"></i></a></li>
                    <?php if ($Owner) { ?>
                        <li class="dropdown hidden-sm" style="display: none;">
                            <a class="btn tip" title="<?= lang('settings') ?>" data-placement="bottom" href="<?= site_url('system_settings') ?>">
                                <i class="fa fa-cogs"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calculator') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                            <i class="fa fa-calculator"></i>
                        </a>
                        <ul class="dropdown-menu pull-right calc">
                            <li class="dropdown-content">
                                <span id="inlineCalc"></span>
                            </li>
                        </ul>
                    </li>
                    <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                        <?php if ($info) { ?>
                            <li class="dropdown hidden-sm">
                                <a class="btn tip" title="<?= lang('notifications') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                    <i class="fa fa-info-circle"></i> Recados
                                    <span class="number blightOrange black"><?= sizeof($info) ?></span>
                                </a>
                                <ul class="dropdown-menu pull-right content-scroll">
                                    <li class="dropdown-header"><i class="fa fa-info-circle"></i> <?= lang('notifications'); ?></li>
                                    <li class="dropdown-content">
                                        <div class="scroll-div">
                                            <div class="top-menu-scroll">
                                                <ol class="oe">
                                                    <?php foreach ($info as $n) {
                                                        echo '<li>' . $n->comment . '</li>';
                                                    } ?>
                                                </ol>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        <?php } ?>
                    <?php } ?>

                    <?php if ($Owner || $Admin || $GP['sales-index']) { ?>
                        <?php if ($events) { ?>
                            <li class="dropdown hidden-xs">
                                <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="#" data-toggle="dropdown">
                                    <i class="fa fa-calendar"></i> <?= lang('agenda_de_vagens') ?>
                                    <span class="number blightOrange black"><?= sizeof($events) ?></span>
                                </a>
                                <ul class="dropdown-menu pull-right content-scroll">
                                    <li class="dropdown-header">
                                    <i class="fa fa-calendar"></i> <?= lang('upcoming_events'); ?>
                                    </li>
                                    <li class="dropdown-content">
                                        <div class="top-menu-scroll">
                                            <ol class="oe">
                                                <?php foreach ($events as $event) {
                                                    echo '<li>' . date($dateFormats['php_ldate'], strtotime($event->start)) . ' <strong>' . $event->title . '</strong><br>'.$event->description.'</li>';
                                                } ?>
                                            </ol>
                                        </div>
                                    </li>
                                    <li class="dropdown-footer">
                                        <a href="<?= site_url('calendar') ?>" class="btn-block link">
                                            <i class="fa fa-calendar"></i> <?= lang('calendar') ?> <?= lang('agenda_de_vagens'); ?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        <?php } else { ?>
                        <li class="dropdown hidden-xs">
                            <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="<?= site_url('calendar') ?>">
                                <i class="fa fa-calendar"></i>
                            </a>
                        </li>
                        <?php } ?>
                    <?php } ?>
                    <!--
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('calendar') ?>" data-placement="bottom" href="<?= site_url('calendar') ?>">
                            <i class="fa fa-tasks" style="font-size: 14px;"></i> tarefas
                            <span class="number blightGrey black">2</span>
                        </a>
                    </li>
                    !-->
                    <?php if (($Owner || $Admin || $GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts']) && ($qty_alert_num > 0 || $exp_alert_num > 0)) { ?>
                    <!--
                        <li class="dropdown hidden-sm" style="display: none;">
                        <a class="btn blightOrange tip" title="<?= lang('alerts') ?>"
                           data-placement="left" data-toggle="dropdown" href="#">
                            <i class="fa fa-exclamation-triangle"></i> Alertas!
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li>
                                <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                    <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                    <span style="padding-right: 35px;">Contratos vencidos</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                    <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                    <span style="padding-right: 35px;">Vendas de hoje</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= site_url('reports/quantity_alerts') ?>" class="">
                                    <span class="label label-danger pull-right" style="margin-top:3px;"><?= $qty_alert_num; ?></span>
                                    <span style="padding-right: 35px;"><?= lang('quantity_alerts') ?></span>
                                </a>
                            </li>
                            <?php if ($Settings->product_expiry) { ?>
                                <li>
                                    <a href="<?= site_url('reports/expiry_alerts') ?>" class="">
                                        <span class="label label-danger pull-right" style="margin-top:3px;"><?= $exp_alert_num; ?></span>
                                        <span style="padding-right: 35px;"><?= lang('expiry_alerts') ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                    !-->
                    <?php  } ?>

                    <li class="dropdown hidden-sm">
                        <a class="btn tip" title="<?= lang('styles') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <i class="fa fa-css3"></i>
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <li class="bwhite noPadding">
                                <a href="#" id="fixed" class="">
                                    <i class="fa fa-angle-double-left"></i>
                                    <span id="fixedText">Fixed</span>
                                </a>
                                <a href="#" id="cssLight" class="grey">
                                    <i class="fa fa-stop"></i> Grey
                                </a>
                                <a href="#" id="cssBlue" class="blue">
                                    <i class="fa fa-stop"></i> Blue
                                </a>
                                <a href="#" id="cssBlack" class="black">
                                   <i class="fa fa-stop"></i> Black
                               </a>
                           </li>
                        </ul>
                    </li>
                    <li class="dropdown hidden-xs">
                        <a class="btn tip" title="<?= lang('language') ?>" data-placement="bottom" data-toggle="dropdown"
                           href="#">
                            <img src="<?= base_url('assets/images/' . $Settings->user_language . '.png'); ?>" alt="">
                        </a>
                        <ul class="dropdown-menu pull-right">
                            <?php $scanned_lang_dir = array_map(function ($path) {
                                return basename($path);
                            }, glob(APPPATH . 'language/*', GLOB_ONLYDIR));
                            foreach ($scanned_lang_dir as $entry) { ?>
                                <li>
                                    <a href="<?= site_url('welcome/language/' . $entry); ?>">
                                        <img src="<?= base_url(); ?>assets/images/<?= $entry; ?>.png" class="language-img">
                                        &nbsp;&nbsp;<?= ucwords($entry); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <li class="divider"></li>
                            <li>
                                <a href="<?= site_url('welcome/toggle_rtl') ?>">
                                    <i class="fa fa-align-<?=$Settings->user_rtl ? 'right' : 'left';?>"></i>
                                    <?= lang('toggle_alignment') ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <?php if ($Owner && $Settings->update) { ?>
                    <li class="dropdown hidden-sm">
                        <a class="btn blightOrange tip" title="<?= lang('update_available') ?>"
                            data-placement="bottom" data-container="body" href="<?= site_url('system_settings/updates') ?>">
                            <i class="fa fa-download"></i>
                        </a>
                    </li>
                        <?php } ?>


                    <?php if ($Owner) { ?>
                        <li class="dropdown" style="display: none;">
                            <a class="btn bdarkGreen tip" id="today_profit" title="<?= lang('today_profit') ?>"
                                data-placement="bottom" data-html="true" href="<?= site_url('reports/profit') ?>"
                                data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-hourglass-2"></i>
                            </a>
                        </li>
                    <?php } ?>
                    <?php if ($Owner || $Admin) { ?>
                            <!--
						<li class="dropdown hidden-xs" style="display: none;">
							<a class="btn bred tip" title="<?= lang('clear_ls') ?>" data-placement="bottom" id="clearLS" href="#">
								<i class="fa fa-eraser"></i>
							</a>
						</li>
						!-->
                    <?php } ?>
                </ul>
            </div>
        </div>
    </header>

    <div class="container" id="container">
        <div class="row" id="main-con">
        <table class="lt"><tr><td class="sidebar-con">
            <div id="sidebar-left">

                <style>
                    .profile_img {
                        padding: 17px;
                        width: 200px;
                    }
                </style>
                <div class="profile clearfix" style="text-align: center;">
                    <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" alt="<?php echo $Settings->site_name ;?>" class="profile_img">
                </div>

                 <div class="sidebar-nav nav-collapse collapse navbar-collapse" id="sidebar_menu">
                    <ul class="nav main-menu">
                        <li class="mm_welcome">
                            <a href="<?= site_url() ?>">
                                <i class="fa fa-home"></i>
                                <span class="text"> <?= lang('dashboard'); ?></span>
                            </a>
                        </li>
                        <?php if ($Owner || $Admin) {?>

                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-map-signs"></i>
                                    <span class="text"> <?= lang('products'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= site_url('products'); ?>">
                                            <i class="fa fa-list"></i>
                                            <span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <li id="products_add">
                                        <a class="submenu" href="<?= site_url('products/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_product'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'settings' ? '' : 'mm_pos' ?>">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-ticket"></i>
                                    <span class="text"> <?= lang('sales'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= site_url('sales'); ?>">
                                            <i class="fa fa-ticket"></i>
                                            <span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>

                                    <li id="sales_add">
                                        <a class="submenu" href="<?= site_url('sales/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_sale'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_canceled">
                                        <a class="submenu" href="<?= site_url('sales/canceled'); ?>">
                                            <i class="fa fa-trash-o"></i>
                                            <span class="text"> <?= lang('list_sales_canceled'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_itinerario" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-car"></i>
                                    <span class="text"> <?= lang('itinerarios'); ?></span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= site_url('itinerario'); ?>">
                                            <i class="fa fa-car"></i>
                                            <span class="text"> <?= lang('gerar_itineario'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>


                            <li class="mm_purchases"  style="display:none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="purchases_index">
                                        <a class="submenu" href="<?= site_url('purchases'); ?>">
                                            <i class="fa fa-star"></i>
                                            <span class="text"> <?= lang('list_purchases'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_add">
                                        <a class="submenu" href="<?= site_url('purchases/add'); ?>">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('add_purchase'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_return_purchases">
                                        <a class="submenu" href="<?= site_url('purchases/return_purchases'); ?>">
                                            <i class="fa fa-reply"></i>
                                            <span class="text"> <?= lang('list_return_purchases'); ?></span>
                                        </a>
                                    </li>
									<li id="purchases_expenses">
                                        <a class="submenu" href="<?= site_url('purchases/expenses'); ?>">
                                            <i class="fa fa-dollar"></i>
                                            <span class="text"> <?= lang('Outras Despesas'); ?></span>
                                        </a>
                                    </li>

                                    <li id="purchases_add_expense">
                                        <a class="submenu" href="<?= site_url('purchases/add_expense'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i>
                                            <span class="text"> <?= lang('Adicionar Outras Despesas'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_contracts mm_folders mm_contract_settings">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-archive"></i>
                                    <span class="text"> <?= lang('contracts'); ?></span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="contracts_index">
                                        <a class="submenu" href="<?= site_url('contracts'); ?>">
                                            <i class="fa fa-files-o"></i>
                                            <span class="text"> <?= lang('my_documents'); ?></span>
                                        </a>
                                    </li>
                                    <li id="folders_index">
                                        <a class="submenu" href="<?= site_url('folders'); ?>">
                                            <i class="fa fa-folder-open-o"></i>
                                            <span class="text"> <?= lang('my_folders'); ?></span>
                                        </a>
                                    </li>
                                    <li id="contracts_contracts">
                                        <a class="submenu" href="<?= site_url('contracts/contracts'); ?>">
                                            <i class="fa fa-file"></i>
                                            <span class="text"> <?= lang('template_contracts'); ?></span>
                                        </a>
                                    </li>
                                    <li id="contract_settings_index">
                                        <a class="submenu" href="<?= site_url('contract_settings'); ?>">
                                            <i class="fa fa fa-cogs"></i>
                                            <span class="text"> <?= lang('contract_settings'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                            <li class="mm_financeiro">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-usd"></i>
                                    <span class="text"> <?= lang('financeiro'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="financeiro_index">
                                        <a class="submenu" href="<?= site_url('contareceber'); ?>">
                                            <i class="fa fa-arrow-circle-up"></i>
                                            <span class="text"> <?= lang('contas_receber'); ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>


                            <?php if ($this->Settings->emitir_nota_fiscal) {?>
                                <!-- Notas Fiscais !-->
                                <li class="mm_notafiscal">
                                    <a class="dropmenu" href="#">
                                        <i class="fa fa-leaf"></i>
                                        <span class="text"> <?= lang('notasfiscal'); ?> </span>
                                        <span class="chevron closed"></span>
                                    </a>
                                    <ul>
                                        <li id="notafiscal_agendamento">
                                            <a class="submenu" href="<?= site_url('notafiscal/agendamento'); ?>">
                                                <i class="fa fa-list"></i>
                                                <span class="text"> <?= lang('notasfiscal_agendamento'); ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>

                            <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                <a class="dropmenu" href="#">
                                <i class="fa fa-users"></i>
                                <span class="text"> <?= lang('people'); ?> </span>
                                <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= site_url('customers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>
                                    <li id="customers_bloqueados">
                                        <a class="submenu" href="<?= site_url('customers/bloqueados'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('Clientes Cancelados'); ?></span>
                                        </a>
                                    </li>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= site_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_customer'); ?></span>
                                        </a>
                                    </li>

                                </ul>
                            </li>

                            <li class="mm_indicators">
                                <a href="<?= site_url() ?>indicators">
                                    <i class="fa fa fa-pie-chart"></i>
                                    <span class="text"> <?= lang('Indicadores'); ?></span>
                                </a>
                            </li>

                            <li class="mm_administracao">
                                <a href="<?= site_url() ?>administracao">
                                    <i class="fa fa-cogs"></i>
                                    <span class="text"> <?= lang('Administração'); ?></span>
                                </a>
                            </li>

                            <li class="mm_administracao_acesso">
                                <a href="<?= site_url() ?>administracao/acesso">
                                    <i class="fa fa-eye"></i>
                                    <span class="text"> <?= lang('Acessar Clientes'); ?></span>
                                </a>
                            </li>

                        <?php
                        } else { // not owner and not admin
                            ?>
                            <?php if ($GP['products-index'] || $GP['products-add'] || $GP['products-barcode']) { ?>
                            <li class="mm_products">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bus"></i>
                                    <span class="text"> <?= lang('products'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="products_index">
                                        <a class="submenu" href="<?= site_url('products'); ?>">
                                            <i class="fa fa-map-signs"></i><span class="text"> <?= lang('list_products'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['products-add']) { ?>
										<li id="products_add">
											<a class="submenu" href="<?= site_url('products/add'); ?>">
												<i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_product'); ?></span>
											</a>
										</li>
                                    <?php } ?>


                                    <?php if ($GP['products-edit']) { ?>
										<li id="products_quantity_adjustments">
											<a class="submenu" href="<?= site_url('products/quantity_adjustments'); ?>">
												<i class="fa fa-filter"></i><span class="text"> <?= lang('quantity_adjustments'); ?></span>
											</a>
										</li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['sales-index'] || $GP['sales-add'] || $GP['sales-deliveries'] || $GP['sales-gift_cards'] || $GP['sales-return_sales']) { ?>
                            <li class="mm_sales <?= strtolower($this->router->fetch_method()) == 'settings' ? '' : 'mm_pos' ?>">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-ticket"></i>
                                    <span class="text"> <?= lang('sales'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= site_url('sales/salesFilter/all/Confirmado'); ?>">
                                            <i class="fa fa-ticket"></i><span class="text"> <?= lang('list_sales'); ?></span>
                                        </a>
                                    </li>

                                    <?php if ($GP['sales-add']) { ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= site_url('sales/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_sale'); ?></span>
                                        </a>
                                    </li>
                                    <li id="sales_canceled">
                                        <a class="submenu" href="<?= site_url('sales/canceled'); ?>">
                                            <i class="fa fa-trash-o"></i>
                                            <span class="text"> <?= lang('list_sales_canceled'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['sales-deliveries']) { ?>
                                    <li id="sales_deliveries">
                                        <a class="submenu" href="<?= site_url('sales/deliveries'); ?>">
                                            <i class="fa fa-truck"></i><span class="text"> <?= lang('deliveries'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['sales-gift_cards']) { ?>
                                    <li id="sales_gift_cards">
                                        <a class="submenu" href="<?= site_url('sales/gift_cards'); ?>">
                                            <i class="fa fa-gift"></i><span class="text"> <?= lang('gift_cards'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['sales-return_sales']) { ?>
                                    <li id="sales_return_sales">
                                        <a class="submenu" href="<?= site_url('sales/return_sales'); ?>">
                                            <i class="fa fa-reply"></i><span class="text"> <?= lang('list_return_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['quotes-index'] || $GP['quotes-add']) { ?>
                            <li class="mm_quotes">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-ticket-o"></i>
                                    <span class="text"> <?= lang('quotes'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <li id="sales_index">
                                        <a class="submenu" href="<?= site_url('quotes'); ?>">
                                            <i class="fa fa-ticket-o"></i><span class="text"> <?= lang('list_quotes'); ?></span>
                                        </a>
                                    </li>
                                    <?php if ($GP['quotes-add']) { ?>
                                    <li id="sales_add">
                                        <a class="submenu" href="<?= site_url('quotes/add'); ?>">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_quote'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['purchases-index'] || $GP['purchases-add'] || $GP['purchases-expenses'] || $GP['purchases-return_purchases']) { ?>
                            <li class="mm_purchases" style="display: none;">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-star"></i>
                                    <span class="text"> <?= lang('purchases'); ?>
                                    </span> <span class="chevron closed"></span>
                                </a>
                                <ul>

                                    <?php if ($GP['purchases-add']) { ?>
										<li id="purchases_index">
											<a class="submenu" href="<?= site_url('purchases'); ?>">
												<i class="fa fa-star"></i><span class="text"> <?= lang('list_purchases'); ?></span>
											</a>
										</li>
										<li id="purchases_add">
											<a class="submenu" href="<?= site_url('purchases/add'); ?>">
												<i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_purchase'); ?></span>
											</a>
										</li>
                                    <?php } ?>
                                    <?php if ($GP['purchases-return_purchases']) { ?>
                                    <li id="purchases_return_purchases">
                                        <a class="submenu" href="<?= site_url('purchases/return_purchases'); ?>">
                                            <i class="fa fa-reply"></i>
                                            <span class="text"> <?= lang('list_return_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>


                            <?php if ($GP['customers-index'] || $GP['customers-add'] || $GP['suppliers-index'] || $GP['suppliers-add']) { ?>
                            <li class="mm_auth mm_customers mm_suppliers mm_billers">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-users"></i>
                                    <span class="text"> <?= lang('people'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($GP['customers-index']) { ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= site_url('customers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_customers'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['customers-add']) { ?>
                                    <li id="customers_index">
                                        <a class="submenu" href="<?= site_url('customers/add'); ?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_customer'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['suppliers-index']) { ?>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= site_url('suppliers'); ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('list_suppliers'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['suppliers-add']) { ?>
                                    <li id="suppliers_index">
                                        <a class="submenu" href="<?= site_url('suppliers/add'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i><span class="text"> <?= lang('add_supplier'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['reports-quantity_alerts'] || $GP['reports-expiry_alerts'] || $GP['reports-products'] || $GP['reports-monthly_sales'] || $GP['reports-sales'] || $GP['reports-payments'] || $GP['reports-purchases'] || $GP['reports-customers'] || $GP['reports-suppliers'] || $GP['reports-expenses']) { ?>
                            <li class="mm_reports">
                                <a class="dropmenu" href="#">
                                    <i class="fa fa-bar-chart-o"></i>
                                    <span class="text"> <?= lang('reports'); ?> </span>
                                    <span class="chevron closed"></span>
                                </a>
                                <ul>
                                    <?php if ($GP['reports-quantity_alerts']) { ?>
                                    <li id="reports_quantity_alerts">
                                        <a href="<?= site_url('reports/quantity_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_quantity_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-expiry_alerts']) { ?>
                                    <?php if ($Settings->product_expiry) { ?>
                                    <li id="reports_expiry_alerts">
                                        <a href="<?= site_url('reports/expiry_alerts') ?>">
                                            <i class="fa fa-bar-chart-o"></i><span class="text"> <?= lang('product_expiry_alerts'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <?php }
                                    if ($GP['reports-products']) { ?>
                                    <li id="reports_products" style="display: none;">
                                        <a href="<?= site_url('reports/products') ?>">
                                            <i class="fa fa-bus"></i><span class="text"> <?= lang('products_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-daily_sales']) { ?>
                                    <li id="reports_daily_sales" style="display: none;">
                                        <a href="<?= site_url('reports/daily_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-monthly_sales']) { ?>
                                    <li id="reports_monthly_sales" style="display: none;">
                                        <a href="<?= site_url('reports/monthly_sales') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_sales'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-sales']) { ?>
                                    <li id="reports_sales">
                                        <a href="<?= site_url('reports/sales') ?>">
                                            <i class="fa fa-ticket"></i><span class="text"> <?= lang('sales_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-payments']) { ?>
                                    <li id="reports_payments">
                                        <a href="<?= site_url('reports/payments') ?>">
                                            <i class="fa fa-money"></i><span class="text"> <?= lang('payments_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-daily_purchases']) { ?>
                                    <li id="reports_daily_purchases">
                                        <a href="<?= site_url('reports/daily_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('daily_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-monthly_purchases']) { ?>
                                    <li id="reports_monthly_purchases">
                                        <a href="<?= site_url('reports/monthly_purchases') ?>">
                                            <i class="fa fa-calendar-o"></i><span class="text"> <?= lang('monthly_purchases'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-purchases']) { ?>
                                    <li id="reports_purchases">
                                        <a href="<?= site_url('reports/purchases') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('purchases_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-expenses']) { ?>
                                    <li id="reports_expenses">
                                        <a href="<?= site_url('reports/expenses') ?>">
                                            <i class="fa fa-star"></i><span class="text"> <?= lang('expenses_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-customers']) { ?>
                                    <li id="reports_customer_report">
                                        <a href="<?= site_url('reports/customers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('customers_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php }
                                    if ($GP['reports-suppliers']) { ?>
                                    <li id="reports_supplier_report">
                                        <a href="<?= site_url('reports/suppliers') ?>">
                                            <i class="fa fa-users"></i><span class="text"> <?= lang('suppliers_report'); ?></span>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php } ?>

                            <?php if ($GP['sales-index']) { ?>
                            <li id="products_add">
                                <a class="submenu" href="<?= site_url('agenda/abrir'); ?>">
                                    <i class="fa fa-calendar"></i>
                                    <span class="text"> Agenda de Viagem</span>
                                </a>
                            </li>
                            <?php }?>
                            <!--
                            <li>
                                <a class="submenu" href="<?= site_url('financeiro/comissao'); ?>">
                                    <i class="fa fa-dollar"></i>
                                    <span class="text"> <?= lang('Comissões'); ?></span>
                                </a>
                            </li>
                            !-->
                        <?php } ?>
                    </ul>
                </div>
                <a href="#" id="main-menu-act" class="full visible-md visible-lg">
                    <i class="fa fa-angle-double-left"></i>
                </a>
            </div>
            </td><td class="content-con">
            <div id="content">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <ul class="breadcrumb">
                            <?php
                            foreach ($bc as $b) {
                                if ($b['link'] === '#') {
                                    echo '<li class="active">' . $b['page'] . '</li>';
                                } else {
                                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                                }
                            }
                            ?>
                            <li class="right_log hidden-xs">
                              <span class='hidden-sm'><?php echo $this->sma->dataDeHojePorExtensoRetornoComSemana();?></span>

                                <!--
                                <?= lang('your_ip') . ' ' . $ip_address . " <span class='hidden-sm'>( " . lang('last_login_at') . ": " . date($dateFormats['php_ldate'], $this->session->userdata('old_last_login')) . " " . ($this->session->userdata('last_ip') != $ip_address ? lang('ip:') . ' ' . $this->session->userdata('last_ip') : '') . " )</span>" ?>
                                !-->
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($message) { ?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoSucesso">&nbsp;</a>
                                <span class="spwTextoRef">Sucesso!</span>
                                <p class="spwTextoRefMensagem"><?php echo $message;?></p>
                            </div>
                        <?php } ?>

                        <?php if ($error) { ?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoErro">&nbsp;</a>
                                <span class="spwTextoRef">Erro!</span>
                                <p class="spwTextoRefMensagem"><?php echo $error;?></p>
                            </div>
                        <?php }?>

                        <?php if ($warning) { ?>
                            <div class="alert alert-warning">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoAlerta">&nbsp;</a>
                                <span class="spwTextoRef">Atenção!</span>
                                <p class="spwTextoRefMensagem"><?php echo $warning;?></p>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('info')) { ?>
                            <div class="alert alert-info">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <a class="spwHtmlIcoInfo">&nbsp;</a>
                                <span class="spwTextoRef">Informações!</span>
                                <p class="spwTextoRefMensagem"><?php echo $this->session->flashdata('info');?></p>
                            </div>
                        <?php } ?>

                        <?php
                        if ($info) {
                            foreach ($info as $n) {
                                if (!$this->session->userdata('hidden' . $n->id)) {
                                    ?>
                                    <div class="alert alert-info">
                                        <a href="#" id="<?= $n->id ?>" class="close hideComment external"
                                           data-dismiss="alert">&times;</a>
                                        <a class="spwHtmlIcoInfo">&nbsp;</a>
                                        <span class="spwTextoRef">Lembrete!</span>
                                        <p style="margin: 0px;margin-left: 35px;font-size: 14px;" id="<?php echo 'notificacion'.$n->id;?>"><?= $n->comment; ?></p>
                                    </div>
                                    <?php
                                }
                            }
                        } ?>
                        <div class="alerts-con"></div>

