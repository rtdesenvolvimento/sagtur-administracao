<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]>
<html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $Settings->site_name; ?>|| AGENDAMENTO</title>

    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta name="description" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO">
    <meta name="keywords" content="">
    <meta name="application-name" content="Resultatec Sistamas Digitais">
    <meta name="title" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO">
    <meta name="robots" content="all"/>
    <meta name="language" content="br"/>
    <meta name="robots" content="follow"/>
    <meta property="og:image" content="<?php echo base_url('assets/uploads/logos/'.$Settings->logo2); ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO"/>
    <meta property="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="author" content="Resultec Sistemas Digitais"/>

    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/toastr/toastr.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->


    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/lightbox2/css/lightbox.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/themes/theme-default.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/estilo.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/datepicker/css/datepicker.css">

    <link rel="icon" href="<?php echo base_url() ?>/assets/images/favicon.ico"/>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<div class="main-content" style="min-height: 292px;">
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <!-- start: TOOLBAR -->
        <div class="toolbar row" style="display: none;">
            <div class="col-sm-6 hidden-xs">
                <div class="page-header" style="text-align: center;">
                    <div style="float: left"><img src="<?php echo base_url('assets/uploads/logos/'.$Settings->logo2);?>" style="    width: 93%;" alt="<?php echo $Settings->site_name ;?>"></div>
                    <div style="display: none;"><h1><?php echo $Settings->site_name; ?> <small>Agende seu ensaio</small></h1></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-heading" style="text-align: center;">
                        <h4 class="panel-title">
                            <img src="<?php echo base_url('assets/uploads/logos/'.$Settings->logo2);?>" alt="<?php echo $Settings->site_name ;?>">
                        </h4>
                    </div>
                    <div class="panel-body">
                        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form' ,'id' => 'form' , 'class' => 'smart-wizard form-horizontal');
                        echo form_open_multipart("agendamento/confirmarAgendamento", $attrib); ?>
                        <div id="wizard" class="swMain">
                            <ul class="anchor">
                                <li>
                                    <a href="#step-1" class="selected" isdone="1" rel="1">
                                        <div class="stepNumber">1</div>
                                        <span class="stepDesc">1º Passo<br><small>Pacote</small> </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-2" class="disabled" isdone="0" rel="2">
                                        <div class="stepNumber">2</div>
                                        <span class="stepDesc"> 2º Passo<br><small>Cadastro</small> </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#step-3" class="disabled" isdone="0" rel="3">
                                        <div class="stepNumber">3</div>
                                        <span class="stepDesc"> 3º Passo<br><small>Confirmação</small> </span>
                                    </a>
                                </li>
                            </ul>
                            <div class="stepContainer" style="height: 353px;">
                                <div class="progress progress-xs transparent-black no-radius active content">
                                    <div aria-valuemax="100" aria-valuemin="0" role="progressbar"
                                         class="progress-bar partition-green step-bar" style="width: 25%;">
                                        <span class="sr-only"> 0% Complete (success)</span>
                                    </div>
                                </div>
                                <?php if ($error) { ?>
                                    <div class="alert alert-danger">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <a class="spwHtmlIcoErro"></a>
                                        <span class="spwTextoRef">Ops! Tivemos problema para processar seu pagamento</span>
                                        <p style="margin: 0px;margin-left: 35px;font-size: 14px;"><?php echo $error;?></p>
                                    </div>
                                <?php }?>
                                <div id="step-1" class="content" style="display: block;">
                                    <div id="timeline" class="demo1">
                                        <div class="timeline">
                                            <div class="spine"></div>
                                            <div class="date_separator" id="november" data-appear-top-offset="-400">
                                                <span style="font-size: 15px;background: #1fbba6;">ESCOLHA SEU PACOTE</span>
                                            </div>
                                            <ul class="columns">
                                                <style>
                                                    .preco_pacote {
                                                        color: #1fbba6;
                                                        font-weight: bold;
                                                        font-size: 27px;
                                                    }
                                                </style>
                                                <?php foreach ($servicos as $servico){
                                                    $images = $this->products_model->getProductPhotos($servico->id);
                                                    ?>
                                                    <li>
                                                        <div class="timeline_element partition-white" id="timeline_element<?php echo $servico->id;?>">
                                                            <div class="timeline_date" style="text-align: center;">
                                                                <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$servico->image);?>"
                                                                   data-lightbox="<?php echo $servico->id;?>" data-title="Agendar">
                                                                    <img src="<?php echo base_url('assets/uploads/'.$servico->image);?>">
                                                                </a>
                                                                <?php foreach ($images as $image){?>
                                                                    <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$image->photo);?>"
                                                                       data-lightbox="<?php echo $servico->id;?>" data-title="Agendar">
                                                                    </a>
                                                                <?php }?>
                                                            </div>
                                                            <div class="readmore" style="text-align: center;margin-top: -16px;">
                                                                <a  href="#" id="<?php echo $servico->id;?>" price="<?php echo $servico->price;?>" class="btn btn-green addServico"
                                                                    style="border-radius: 23px;" >
                                                                    <span class="thumb-info-title" style='font-size: 20px;font-weight: 700;'>Agendar Clique Aqui</span>
                                                                    <i class="fa fa-birthday-cake" style="font-size: 35px;"></i>
                                                                </a>
                                                            </div>
                                                            <div class="inline-block" style="width: 100%;text-align: center;">
                                                                <span class="block week-day text-extra-large"><?php echo $servico->category;?></span>
                                                                <span class="block week-day text-extra-large"><?php echo $servico->name;?></span>
                                                                <span class="block month preco_pacote"><?php echo $this->sma->formatMoney($servico->price);?></span>
                                                            </div>
                                                            <div class="timeline_content" style="text-align: center;">
                                                                <?php echo $servico->product_details;?>
                                                            </div>
                                                        </div>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="tbHorarios" style="display: none;">
                                        <div class="panel panel-white">
                                            <div class="panel-heading border-light" style="background: #1fbba6; color: #ffffff">
                                                <h4 class="panel-title"><i class="fa fa-clock-o"></i> ESCOLHA O MELHOR DIA </h4>
                                            </div>
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label"></label>
                                                    <div class="col-sm-7">
                                                        <input type="date" value="<?php echo (isset($_POST['date']) ? $_POST['date'] : ($memoryAgendameto ? $memoryAgendameto->getData() : date('Y-m-d') ));?>"
                                                               style='font-size: 20px;font-weight: 600;'
                                                               class="form-control" name="dataAgendamento" id="dataAgendamento" required="required">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-white">
                                            <div class="panel-heading border-light" style="background: #1fbba6; color: #ffffff">
                                                <h4 class="panel-title"><i class="fa fa-clock-o"></i> ESCOLHA O MELHOR HORÁRIO </h4>
                                            </div>
                                            <div class="panel-body">
                                                <div id="horariosServico"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="primeiroPasso" style="display: none;">
                                        <div class="col-sm-2 col-sm-offset-8">
                                            <button class="btn btn-blue next-step btn-block" id="btn-primeiro-passo">
                                                PRÓXIMO PASSO <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2" class="content well" style="display: none;">
                                    <div class="form-group">
                                        <input type="hidden" id="servicoId" value="<?php echo (isset($_POST['servicoId']) ? $_POST['servicoId'] : ($memoryAgendameto ? $memoryAgendameto->getProduto() : ''));?>" name="servicoId"/>
                                        <input type="hidden" id="price" value="<?php echo (isset($_POST['price']) ? $_POST['price'] : 0);?>" name="price"/>
                                        <label class="col-sm-3 control-label">
                                            Nome Completo <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="text" value="<?php echo (isset($_POST['nomeCompleto']) ? $_POST['nomeCompleto'] : ($memoryAgendameto ? $memoryAgendameto->getNomeCompleto() : ''));?>"
                                                   class="form-control" id="nomeCompleto" name="nomeCompleto"
                                                   placeholder="Digite seu nome completo aqui">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            Celular com (DDD)<span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="tel" value="<?php echo (isset($_POST['celular']) ? $_POST['celular'] : ($memoryAgendameto ? $memoryAgendameto->getCelular() : ''));?>"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   maxlength="15"
                                                   id="celular"
                                                   name="celular" placeholder="Celular">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">
                                            E-mail <span class="symbol required"></span>
                                        </label>
                                        <div class="col-sm-7">
                                            <input type="email" class="form-control" value="<?php echo (isset($_POST['email']) ? $_POST['email'] : ($memoryAgendameto ? $memoryAgendameto->getEmail() : ''));?>"
                                                   id="email"
                                                   name="email" placeholder="Seu e-mail">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <button class="btn btn-light-grey back-step btn-block">
                                                <i class="fa fa-circle-arrow-left"></i> VOLTAR
                                            </button>
                                        </div>
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <button class="btn btn-blue next-step btn-block" id="btn-segundo-passo">
                                                PRÓXIMO PASSO <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div id="step-3" class="content" style="display: none;">
                                    <h2 class="StepTitle" style="text-align: center;font-size: 22px;">Como deseja pagar?</h2>
                                    <input type="hidden" name="senderHash"/>
                                    <input type="hidden" name="creditCardToken"/>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7">
                                            <?php foreach ($tiposCobranca as $tipoCobranca) {
                                                $tipoCobrancaFilter = (isset($_POST['tipoCobranca']) ? $_POST['tipoCobranca'] : ($memoryAgendameto ? $memoryAgendameto->getTipoCobranca() : ''));
                                                $checkedTipoCobranca = '';
                                                if ($tipoCobrancaFilter == $tipoCobranca->id) $checkedTipoCobranca = 'checked="checked"'; ?>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="radio" tipo="<?php echo $tipoCobranca->tipo;?>" <?php echo $checkedTipoCobranca;?> class="square-green tipoCobranca" value="<?php echo $tipoCobranca->id;?>" name="tipoCobranca">
                                                        <?php echo $tipoCobranca->name;?>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="panel panel-white" id="divCartaoCredito" style="display: none;">
                                        <div class="panel-heading border-light">
                                            <h4 class="panel-title"><i class="fa fa-credit-card"></i>
                                                <img style="vertical-align: middle;margin: -2px 0 0 .5em;" src="<?php echo base_url() ?>themes/default/assets/images/credit-cards/pagseguro.png">
                                            </h4>
                                        </div>
                                        <div class="panel-body well">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Nome do Titular<span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" value="<?php echo (isset($_POST['card_name']) ? $_POST['card_name'] : ($memoryAgendameto ? $memoryAgendameto->getCardName() : ''));?>"
                                                           id="card_name" name="card_name"
                                                           autocomplete="off"
                                                           placeholder="Nome do Titular (como gravado no cartão)">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    CPF do Titular<span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="tel" class="form-control" value="<?php echo (isset($_POST['cpfTitular']) ? $_POST['cpfTitular'] : ($memoryAgendameto ? $memoryAgendameto->getCpf() : ''));?>"
                                                           id="cpfTitular" name="cpfTitular"
                                                           autocomplete="off"
                                                           maxlength="14"
                                                           placeholder="CPF">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Data de nascimento do Titular<span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <select class="form-control" id="diaNascimento"
                                                                    name="diaNascimento">
                                                                <option>DIA</option>
                                                                <option value="01">01</option>
                                                                <option value="02">02</option>
                                                                <option value="03">03</option>
                                                                <option value="04">04</option>
                                                                <option value="05">05</option>
                                                                <option value="06">06</option>
                                                                <option value="07">07</option>
                                                                <option value="08">08</option>
                                                                <option value="09">09</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                                <option value="13">13</option>
                                                                <option value="14">14</option>
                                                                <option value="15">15</option>
                                                                <option value="16">16</option>
                                                                <option value="17">17</option>
                                                                <option value="18">18</option>
                                                                <option value="19">19</option>
                                                                <option value="20">20</option>
                                                                <option value="21">21</option>
                                                                <option value="22">22</option>
                                                                <option value="23">23</option>
                                                                <option value="24">24</option>
                                                                <option value="25">25</option>
                                                                <option value="26">26</option>
                                                                <option value="27">27</option>
                                                                <option value="28">28</option>
                                                                <option value="29">29</option>
                                                                <option value="30">30</option>
                                                                <option value="31">31</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <select class="form-control" id="mesNascimento"
                                                                    name="mesNascimento">
                                                                <option>MÊS</option>
                                                                <option value="01">JANEIRO</option>
                                                                <option value="02">FEVEREIRO</option>
                                                                <option value="03">MARÇO</option>
                                                                <option value="04">ABRIL</option>
                                                                <option value="05">MAIO</option>
                                                                <option value="06">JUNHO</option>
                                                                <option value="07">JULHO</option>
                                                                <option value="08">AGOSTO</option>
                                                                <option value="09">SETEMBRO</option>
                                                                <option value="10">OUTUBRO</option>
                                                                <option value="11">NOVEMBRO</option>
                                                                <option value="12">DEZEMBRO</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control"
                                                                   id="anoNascimento" name="anoNascimento"
                                                                   placeholder="ANO (YYYY)">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Número do cartão<span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            <img src="" id="band" style="display: none;max-width: none;">
                                                        </div>
                                                        <input type="tel" class="form-control"
                                                               autocomplete="off"
                                                               id="card_number"
                                                               name="card_number" maxlength="19" placeholder="•••• •••• •••• ••••">
                                                    </div>
                                                    <small><span id="erro-cartao" style="color: red;"></span></small>
                                                </div>
                                            </div>
                                            <div class="form-group" id="divParcelas">
                                                <label class="col-sm-3 control-label">
                                                    Parcelas <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-4">
                                                    <select id="parcelas" required="required" class="form-control" name="parcelas">
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    CVC <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-4">
                                                    <input type="number" class="form-control" id="card_cvc" name="card_cvc"
                                                           placeholder="CVC">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    Validade(MM/YYYY) <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-4">
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <select class="form-control" id="card_expiry_mm"
                                                                    name="card_expiry_mm">
                                                                <option value="">MM</option>
                                                                <option value="01">1</option>
                                                                <option value="02">2</option>
                                                                <option value="03">3</option>
                                                                <option value="04">4</option>
                                                                <option value="05">5</option>
                                                                <option value="06">6</option>
                                                                <option value="07">7</option>
                                                                <option value="08">8</option>
                                                                <option value="09">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <select class="form-control" id="card_expiry_yyyy"
                                                                    name="card_expiry_yyyy">
                                                                <option value="">YYYY</option>
                                                                <option value="2019">2019</option>
                                                                <option value="2020">2020</option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                                <option value="2025">2025</option>
                                                                <option value="2026">2026</option>
                                                                <option value="2027">2027</option>
                                                                <option value="2028">2028</option>
                                                                <option value="2029">2029</option>
                                                                <option value="2030">2030</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-white" id="divBoletoBancario" style="display: none;">
                                        <div class="panel-heading border-light">
                                            <h4 class="panel-title"><i class="fa fa-barcode"></i>
                                                Pagseguro/Boleto Bancário
                                                <img style="vertical-align: middle;margin: -2px 0 0 .5em;" src="<?php echo base_url() ?>themes/default/assets/images/credit-cards/pagseguro.png">
                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    CPF <span class="symbol required"></span>
                                                </label>
                                                <div class="col-sm-7">
                                                    <input type="text" class="form-control" id="cpf" name="cpf"
                                                           placeholder="CPF">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    <i class="fa fa-barcode fa-2x"></i>
                                                </label>
                                                <div class="col-sm-7">
                                                    <p class="form-control-static display-value"
                                                       data-display="username">
                                                        O pedido será confirmado apenas após a confirmação do pagamento.
                                                        Taxa: R$ 1,00 (taxa aplicada para cobrir custos de gestão de risco do meio de pagamento).
                                                        <br/><br/>
                                                        * Após clicar em "Próximo Passo e Confirmar" você receberá o seu boleto bancário, é possível imprimi-lo e pagar pelo site do seu banco ou por uma casa lotérica.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-white" id="divDeposito" style="display: none;">
                                        <div class="panel-heading border-light">
                                            <h4 class="panel-title"><i class="fa fa-bank"></i>
                                                Deposito / Transferência Bancária ou PIX Chave - 48996757932
                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    <i class="fa fa-bank fa-2x"></i>
                                                </label>
                                                <div class="col-sm-7">
                                                    <p class="form-control-static display-value"
                                                       data-display="username">
                                                        O agendamento será confirmado apenas após a confirmação do deposito/transferência ou PIX.

                                                        <br/><br/>
                                                        * Após aceitar os termos do agendamento e clicar em "CONFIRMAR" você será direcionado para nosso WhasApp e vamos encaminhar nossos
                                                        dados bancários/chave PIX. <br/>
                                                        Se preferir ao "CONFIRMAR" vamos enviar para seu e-mail nossas contas para depósito com todas as informações do ensaio,
                                                        basta apenas envira o comprovante para nosso WhasApp e pronto!!! 😉
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-white" id="divEmMaos">
                                        <div class="panel-heading border-light">
                                            <h4 class="panel-title"><i class="fa fa-photo"></i>
                                                Em Mãos / No dia do Ensaio
                                            </h4>
                                        </div>
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label">
                                                    <i class="fa fa-photo fa-2x"></i>
                                                </label>
                                                <div class="col-sm-7">
                                                    <p class="form-control-static display-value"
                                                       data-display="username">
                                                        O pedido será confirmado apenas após a confirmação do deposito/transferência.

                                                        <br/><br/>
                                                        * Após clicar em "Realizar pagamento" você receberá o seu boleto bancário, é possível imprimi-lo e pagar pelo site do seu banco ou por uma casa lotérica.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                    $arquivo = fopen('files/aceite_contrato.txt', 'r');
                                    $aceistes_contrato = '';

                                    while (!feof($arquivo)) :
                                        $linha          = fgets($arquivo, 1024);
                                        $aceistes_contrato    .= $linha;
                                    endwhile;
                                    fclose($arquivo);
                                    $aceistes_contrato_label = explode('@label@',$aceistes_contrato); ?>

                                    <?php
                                    $contadorAceite = 0;
                                    foreach ($aceistes_contrato_label as $labelAceite){
                                        if ($labelAceite != '') {
                                            $labelAceite = explode('@quebra@', $labelAceite); ?>

                                            <div class="panel panel-white temos_aceite" style="display: none;">
                                                <div class="panel-heading border-light" style="background: #1fbba6; color: #ffffff">
                                                    <h4 class="panel-title"><?php echo $labelAceite[0];?></h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="form-group">
                                                        <div class="col-sm-7" style="color:  #1fbba6;">
                                                            <p class="form-control-static display-value"
                                                               data-display="username">
                                                                <?php echo $labelAceite[1];?>
                                                            </p>
                                                        </div>
                                                        <label class="col-sm-3 control-label">
                                                            <input type="checkbox" class="grey" value="" required="required" name="temos_de_aceite<?php echo $contadorAceite;?>[]" id="">ACEITO / CONCORDO
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php $contadorAceite = $contadorAceite + 1; ?>
                                    <?php }?>
                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <button class="btn btn-light-grey back-step btn-block">
                                                <i class="fa fa-circle-arrow-left"></i> Voltar
                                            </button>
                                        </div>
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <button class="btn btn-success finish-step btn-block">
                                                CONFIRMAR <i class="fa fa-arrow-circle-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="actionBar">
                                <div class="msgBox">
                                    <div class="content"></div>
                                    <a href="#" class="close">X</a></div>
                                <div class="loader">Loading</div>
                                <a href="#" class="buttonFinish buttonDisabled">Finish</a>
                                <a href="#" class="buttonNext">Next</a>
                                <a href="#" class="buttonPrevious buttonDisabled">Previous</a></div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="inner">
        <div class="footer-inner">
            <div class="pull-left">
                <?php echo date('Y');?> © <?php echo $Settings->site_name?>.
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="fa fa-chevron-up"></i></span>
            </div>
        </div>
    </footer>
</div>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url() ?>/assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/lightbox2/js/lightbox.min.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url() ?>assets/rapido/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/pages-timeline.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script src="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js" charset="UTF-8"></script>

<script type="text/javascript" src="<?= $assets ?>js/valida_cpf_cnpj.js"></script>


<?php if ($configPagSeguro->sandbox == 1) {?>
    <!-- API PagSeguro -->
    <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<?php } else {?>
    <!-- API PagSeguro -->
    <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
<?php }?>

<style>
    .servico_selecionado {
        background: #0044cc;!important;
        color: #ffff;!important;
    }

    .focus-class {
        background: #0044cc;!important;
        color: #ffff;!important;
    }
</style>

<script>
    var contador = 1;

    var FormWizard = function () {
        "use strict";

        var wizardContent = $('#wizard');
        var wizardForm = $('#form');
        var numberOfSteps = $('.swMain > ul > li').length;

        var initWizard = function () {
            // function to initiate Wizard Form
            wizardContent.smartWizard({
                selected: 0,
                keyNavigation: false,
                onLeaveStep: leaveAStepCallback,
                onShowStep: onShowStep,
            });
            var numberOfSteps = 0;
            animateBar();
            initValidator();
        };
        var animateBar = function (val) {
            if ((typeof val == 'undefined') || val == "") {
                val = 1;
            };

            var valueNow = Math.floor(100 / numberOfSteps * val);
            $('.step-bar').css('width', valueNow + '%');
        };
        var validateCheckRadio = function (val) {
            $("input[type='radio'], input[type='checkbox']").on('ifChecked', function(event) {
                debugger;
                $(this).parent().closest(".has-error").removeClass("has-error").addClass("has-success").find(".help-block").remove().end().find('.symbol').addClass('ok');
            });
        };
        var initValidator = function () {
            $.validator.addMethod("cardExpiry", function () {
                //if all values are selected
                if ($("#card_expiry_mm").val() != "" && $("#card_expiry_yyyy").val() != "") {
                    return true;
                } else {
                    return false;
                }
            }, 'Selecione um mês e ano');
            $.validator.setDefaults({
                errorElement: "span", // contain the error msg in a span tag
                errorClass: 'help-block',
                errorPlacement: function (error, element) { // render error placement for each input type
                    if (element.attr("type") == "radio" || element.attr("type") == "checkbox") { // for chosen elements, need to insert the error after the chosen container
                        //$('#dataAgendamento').focus();
                        document.getElementsByClassName('panel-title')[1].scrollIntoView()

                        error.insertAfter($(element).closest('.form-group').children('div').children().last());
                    } else if (element.attr("name") == "card_expiry_mm" || element.attr("name") == "card_expiry_yyyy") {
                        error.appendTo($(element).closest('.form-group').children('div'));
                    } else {
                        error.insertAfter(element);
                        // for other inputs, just perform default behavior
                    }
                },
                ignore: ':hidden',
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    nomeCompleto: {
                        required: true,
                        minlength: 2,
                    },
                    celular: {
                        required: true,
                        minlength: 15,
                        maxlength: 15,
                    },
                    cpf: {
                        required: true
                    },
                    cpfTitular: {
                        required: true
                    },
                    diaNascimento: {
                        required: true,
                        minlength: 1,
                    },
                    anoNascimento: {
                        required: true,
                        minlength: 4,
                        maxlength: 4,
                    },
                    mesNascimento: {
                        required: true,
                        minlength: 1,
                    },
                    tipoCobranca: {
                        required: true
                    },
                    card_name: {
                        required: true
                    },
                    horarios: {
                        required: true
                    },
                    card_number: {
                        minlength: 19,
                        maxlength: 19,
                        required: true
                    },
                    card_cvc: {
                        digits: true,
                        required: true,
                        minlength: 3,
                        maxlength: 4
                    },
                    card_expiry_yyyy: "cardExpiry",
                    payment: {
                        required: true,
                        minlength: 1
                    }
                },
                messages: {
                    firstname: "Please specify your first name"
                },
                highlight: function (element) {
                    $(element).closest('.help-block').removeClass('valid');
                    // display OK icon
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error').find('.symbol').removeClass('ok').addClass('required');
                    // add the Bootstrap error class to the control group
                },
                unhighlight: function (element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error');
                    // set error class to the control group
                },
                success: function (label, element) {
                    label.addClass('help-block valid');
                    // mark the current input as valid and display OK icon
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success').find('.symbol').removeClass('required').addClass('ok');
                }
            });
        };
        var displayConfirm = function () {
            $('.display-value', form).each(function () {
                var input = $('[name="' + $(this).attr("data-display") + '"]', form);
                if (input.attr("type") == "text" || input.attr("type") == "email" || input.is("textarea")) {
                    $(this).html(input.val());
                } else if (input.is("select")) {
                    $(this).html(input.find('option:selected').text());
                } else if (input.is(":radio") || input.is(":checkbox")) {

                    $(this).html(input.filter(":checked").closest('label').text());
                } else if ($(this).attr("data-display") == 'card_expiry') {
                    $(this).html($('[name="card_expiry_mm"]', form).val() + '/' + $('[name="card_expiry_yyyy"]', form).val());
                }
            });
        };
        var onShowStep = function (obj, context) {
            if(context.toStep == numberOfSteps){
                $('.anchor').children("li:nth-child(" + context.toStep + ")").children("a").removeClass('wait');
                displayConfirm();
            }
            $(".next-step").unbind("click").click(function (e) {
                e.preventDefault();
                wizardContent.smartWizard("goForward");
            });
            $(".back-step").unbind("click").click(function (e) {
                e.preventDefault();
                wizardContent.smartWizard("goBackward");
            });
            $(".finish-step").unbind("click").click(function (e) {
                e.preventDefault();
                onFinish(obj, context);
            });
        };
        var leaveAStepCallback = function (obj, context) {
            return validateSteps(context.fromStep, context.toStep);
            // return false to stay on step and true to continue navigation
        };
        var onFinish = function (obj, context) {
            if (validateAllSteps()) {

                if (wizardForm.valid()) {
                    wizardForm.validate().focusInvalid();
                    wizardForm.submit();


                    if ($('input[name="tipoCobranca"]:checked').attr('tipo') === 'carne_cartao_transparent') {
                        $.blockUI(
                            {
                                message: '<h3><img src="https://www.smarticontrol.com.br/assets/js/images/busy.gif" />  Confirmando seu pagamento...<br/>Aguarde... </h3>',
                                css: {width: '70%', left: '15%'}
                            }
                        );

                    } else {
                        $.blockUI(
                            {
                                message: '<h3><img src="https://www.smarticontrol.com.br/assets/js/images/busy.gif" />  Confirmando seu agendamento...<br/>Aguarde...  </h3>',
                                css: {width: '70%', left: '15%'}
                            }
                        );
                    }


                } else {
                    alert("CAMPO OBRIGATÓRIO! Você não preencheu algumas das informações solicitadas. O campo obrigatório será destacado em vermelho. Navegue acima na tela para verificar.");
                }

            }
        };
        var validateSteps = function (stepnumber, nextstep) {
            var isStepValid = false;

            if (numberOfSteps >= nextstep && nextstep > stepnumber) {

                // cache the form element selector
                if (wizardForm.valid()) { // validate the form
                    wizardForm.validate().focusInvalid();
                    for (var i=stepnumber; i<=nextstep; i++){
                        $('.anchor').children("li:nth-child(" + i + ")").not("li:nth-child(" + nextstep + ")").children("a").removeClass('wait').addClass('done').children('.stepNumber').addClass('animated tada');
                    }
                    //focus the invalid fields
                    animateBar(nextstep);
                    isStepValid = true;
                    return true;
                };
            } else if (nextstep < stepnumber) {
                for (i=nextstep; i<=stepnumber; i++){
                    $('.anchor').children("li:nth-child(" + i + ")").children("a").addClass('wait').children('.stepNumber').removeClass('animated tada');
                }

                animateBar(nextstep);
                return true;
            }
        };
        var validateAllSteps = function () {
            var isStepValid = true;
            // all step validation logic
            return isStepValid;
        };
        return {
            init: function () {
                initWizard();
                validateCheckRadio();
            }
        };
    }();

    jQuery(document).ready(function() {

        // cria uma sessão
        PagSeguroDirectPayment.setSessionId('<?php echo $sessionCode;?>');

        Main.init();
        FormWizard.init();
        Timeline.init();

        $('.date-picker').datepicker({
            autoclose: true,
            language: 'pt-BR',
            format: 'dd/mm/yyyy',
        });

        $('#dataAgendamento').change(function (event){
            carregarHorario();
        });

        $('#card_number').change(function (event) {
            buscarBandeiraCartao();
        });

        $('#card_cvc').change(function (event) {
            createCardToken();
        });

        $('#card_expiry_mm').change(function (event) {
            createCardToken();
        });

        $('#card_expiry_yyyy').change(function (event) {
            createCardToken();
        });

        $('#celular').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('#card_number').keyup(function (event) {
            mascaraCartaoCredito( this, mcc );
        });

        $('#cpfTitular').keyup(function (event) {
            mascaraCpf( this, formataCPF );
        });

        $('#cpfTitular').blur(function (event) {

            if ($(this).val() === '') return false;

            if (!valida_cpf($(this).val())) {
                alert('CPF inválido!');

                $(this).val('');
                $(this).focus();
                return false;
            }

            $(this).val(formataCPF($(this).val()));
        });

        $('#cpf').blur(function (event) {

            if ($(this).val() === '') return false;

            if (!valida_cpf($(this).val())) {
                alert('CPF inválido!');

                $(this).val('');
                $(this).focus();
                return false;
            }

            $(this).val(formataCPF($(this).val()));
        });

        $('.addServico').click(function (event) {
            event.preventDefault();
            let servicoId = $(this).attr('id');
            let price = $(this).attr('price');

            preencherServico(servicoId, price);
        });

        <?php
        $servicoId = (isset($_POST['servicoId']) ? $_POST['servicoId'] : ($memoryAgendameto ? $memoryAgendameto->getProduto() : ''));
        $price =  $produto->price;
        if($servicoId != null ) echo 'preencherServico('.$servicoId.', '.$price.' );'
        ?>
    });

    function preencherServico(servicoId, price) {

        $('#tbHorarios').show(1000);

        $('.timeline_element').removeClass('servico_selecionado');
        $('#servicoId').val(servicoId);
        $('#price').val(price);

        $('#timeline_element'+servicoId).addClass('servico_selecionado');

        carregarHorario();

        pagSeguro();

    }

    function condicoesPagamento(brand) {

        PagSeguroDirectPayment.getInstallments({
            amount: $('#price').val(),
            maxInstallmentNoInterest: 1,
            brand: brand,
            success: function(response){
                let parcelas = eval('response.installments.'+brand);

                $(parcelas).each(function( index, parcela ) {

                    let quantity = parcela.quantity;

                    if (quantity > 2) return false;

                    let valorPorParcela = parcela.installmentAmount;
                    let totalAmount = parcela.totalAmount;
                    let interestFree = parcela.interestFree;
                    let text = '';

                    if (interestFree) text = quantity+'X R$'+valorPorParcela+' sem juros';
                    else text = quantity+'X R$'+valorPorParcela+' (R$'+totalAmount+')';

                    var option = $('<option/>');
                    option.attr({ 'value': quantity }).attr({ 'totalAmount': totalAmount }).text(text);
                    $('#parcelas').append(option);

                    console.log( index + ": quantity = " + quantity + ' ');
                });

                $('#parcelas').change(function () {
                    let totalAmount = $( '#parcelas :selected' ).attr('totalAmount');
                    $('#price').val(totalAmount);
                });
            },
            error: function(response) {},
            complete: function(response){}
        });
    }

    function pagSeguro() {
        // obter meios de pagamento
        PagSeguroDirectPayment.getPaymentMethods({
            success: function(json){
                console.log(json);
            },
            error: function(json){
                console.log(json);
                var erro = "";

                for(i in json.errors){
                    erro = erro + json.errors[i];
                }
                alert(erro);
            },
            complete: function(json){}
        });

        // Obter identificação do comprador
        var senderHash = PagSeguroDirectPayment.getSenderHash();
        $("input[name='senderHash']").val(senderHash);
    }

    function buscarBandeiraCartao() {

        $('#band').hide();
        $('#parcelas').empty();

        if ($('#card_number').val() === '') return;

        // consultar a bandeira do cartão
        PagSeguroDirectPayment.getBrand({
            cardBin: $("input[name='card_number']").val().replace(/ /g, ''),
            success: function (json) {
                var brand = json.brand.name;

                $('#band').attr('src', '<?php echo base_url() ?>themes/default/assets/images/credit-cards/'+brand+'.png');
                $('#band').show()

                console.log(brand);

                condicoesPagamento(brand);
                createCardToken();
            },
            error: function (json) {
                console.log(json);
            },
            complete: function (json) {}
        });
    }

    function createCardToken() {

        if ($('#card_number').val() === '') return;

        // consultar a bandeira do cartão
        PagSeguroDirectPayment.getBrand({
            cardBin: $("input[name='card_number']").val().replace(/ /g, ''),
            success: function (json) {
                var brand = json.brand.name;
                $('#erro-cartao').html('');

                if ($('#card_number').val() === '') return;
                if ($('#card_cvc').val() === '') return;
                if ($('#card_expiry_mm').val() === '') return;
                if ($('#card_expiry_yyyy').val() === '') return;

                var param = {
                    cardNumber: $("input[name='card_number']").val().replace(/ /g, ''),
                    brand: brand,
                    cvv: $("input[name='card_cvc']").val(),
                    expirationMonth: $('#card_expiry_mm').val(),
                    expirationYear: $('#card_expiry_yyyy').val(),

                    success: function (json) {
                        var token = json.card.token;
                        $("input[name='creditCardToken']").val(token);
                        console.log("creditCardToken: " + token);
                    },
                    error: function (json) {
                        console.log(json);

                        if (json.error) {
                            $(json.errors).each(function (index, error) {
                                $('#erro-cartao').append(JSON.stringify(error));
                                console.log(index + ": " + error);
                            });
                        }

                    },
                    complete: function (json) {}
                }

                // obter token do cartão de crédito
                PagSeguroDirectPayment.createCardToken(param);
            },
            error: function (json) {
                console.log(json);
            },
            complete: function (json) {}
        });
    }

    function carregarHorario() {

        let data = $('#dataAgendamento').val();

        $('#horariosServico').html('');

        $.blockUI({message: '<h3> Carregando Agenda <br/> Aguarde ...</h3>', css: {width: '70%', left: '15%'}});

        $('#horariosServico').load('<?php echo base_url().'agendamento/getHorarios/';?>'+data+'/'+$('#servicoId').val(), function (event) {

            Main.init();

            $.unblockUI();
            $('#primeiroPasso').show();
            document.getElementsByClassName('panel-title')[1].scrollIntoView()

            let horarios = $( $('input[name ="horarios"]') );

            $('.square-green').on('ifChecked', function (e) {
                $('.focus-class').removeClass('focus-class');
                $(this).parent().parent().parent().addClass('focus-class');
                $('#btn-primeiro-passo').focus();
            });

            $('.tipoCobranca').on('ifChecked', function (e) {
                exibirFormularioTipoCobranca();
            });

            if ( horarios.length > 0) {
                $( horarios ).each(function( index ) {

                    <?php $horaInicio = (isset($_POST['horarios']) ? $_POST['horarios'] : ($memoryAgendameto ? $memoryAgendameto->getHoraInicio() : ''));?>

                    if ($(this).val() ===  '<?php echo $horaInicio;?>') {
                        console.log( index + ": " + $( this ).val() );
                        $(this).iCheck('check');

                        $('#btn-primeiro-passo').click();
                        exibirFormularioTipoCobranca();
                    }
                });
            } else {
                $('#primeiroPasso').hide();
            }

        });
    }

    function exibirFormularioTipoCobranca() {

        $( $('input[name ="tipoCobranca"]') ).each(function( index ) {

            if ( $(this).is(':checked') ) {

                let tipo = $(this).attr('tipo');

                $('#divCartaoCredito').hide();
                $('#divBoletoBancario').hide();
                $('#divDeposito').hide();
                $('#divEmMaos').hide();
                $('.temos_aceite').show();

                if (tipo === 'boleto') {
                    $('#divBoletoBancario').show();
                } else if (tipo === 'carne_cartao_transparent') {
                    $('#divCartaoCredito').show();
                    $('#card_name').focus();
                } else if (tipo === 'em_maos') {
                    $('#divEmMaos').show();
                } else {
                    $('#divDeposito').show();
                }
            }
        });
    }

    /* Máscaras ER */
    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mascaraCartaoCredito(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracartaocredito()",1)
    }

    function execmascaracartaocredito(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    function mcc(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{4})(\d)/g,"$1 $2");
        v=v.replace(/^(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3");
        v=v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3 $4");
        return v;
    }

    function formataCPF(v){
        v = v.replace( /\D/g , ""); //Remove tudo o que não é dígito
        v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace( /(\d{3})(\d)/ , "$1.$2"); //Coloca um ponto entre o terceiro e o quarto dígitos
        v = v.replace( /(\d{3})(\d{1,2})$/ , "$1-$2"); //Coloca um hífen entre o terceiro e o quarto dígitos
        return v;
    }

    function id( el ){
        return document.getElementById( el );
    }

    window.onload = function(){


    }

</script>

</body>
<!-- end: BODY -->
</html>