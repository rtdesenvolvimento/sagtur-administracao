<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('adicionar_agendamento_bloqueio_header'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("agendamento/adicionarAgendamento/", $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('date', 'dataDe'); ?>
                        <?= form_input('data', date('Y-m-d'), 'class="form-control" id="data" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('dataAte', 'dataAte'); ?>
                        <?= form_input('dataAte', date('Y-m-d'), 'class="form-control" id="dataAte" required="required"', 'date'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('horaInicio', 'horaInicio'); ?>
                        <?= form_input('horaInicio', '', 'class="form-control" id="horaInicio" required="required"', 'time'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('horaTermino', 'horaTermino'); ?>
                        <?= form_input('horaTermino', '', 'class="form-control" id="horaTermino" required="required"', 'time'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('details', 'nomeCompleto'); ?>
                        <?= form_input('nomeCompleto', (isset($_POST['nomeCompleto']) ? $_POST['nomeCompleto'] : ''), 'class="form-control" required="required" id="nomeCompleto" '); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_hidden('isBloqueado', TRUE); ?>
        <div class="modal-footer">
            <?php echo form_submit('adicionarAgendamento', lang('add'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $('#status').attr('readonly', true);
        $("#cpf").mask("999.999.999-99");
    });
</script>
