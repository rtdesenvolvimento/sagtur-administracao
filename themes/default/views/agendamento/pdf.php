<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->lang->line('agendamento') . ' ' . $agendamento->id; ?></title>
    <link href="<?php echo $assets ?>styles/style.css" rel="stylesheet">
    <style type="text/css">
        html, body {
            height: 100%;
            background: #FFF;
        }
        body:before, body:after {
            display: none !important;
        }
        .table th {
            text-align: center;
            padding: 5px;
        }
        .table td {
            padding: 4px;
        }
    </style>
</head>

<body>
<div id="wrap">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="col-xs-5">
                    <h2 class="">
                        <?= $biller->company != '-' ? $biller->company .'<br/>'.$biller->name : $biller->name; ?>
                    </h2>
                    <?= $biller->company ? '' : 'Attn: ' . $biller->name; ?>
                    <?php
                        echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state . '<br />' . $biller->country;
                        echo '<p>';
                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                        }
                        if ($biller->cf1 != '-' && $biller->cf1 != '') {
                            echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                        }
                        if ($biller->cf2 != '-' && $biller->cf2 != '') {
                            echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                        }
                        if ($biller->cf3 != '-' && $biller->cf3 != '') {
                            echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                        }
                        if ($biller->cf4 != '-' && $biller->cf4 != '') {
                            echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                        }
                        if ($biller->cf5 != '-' && $biller->cf5 != '') {
                            echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                        }
                        if ($biller->cf6 != '-' && $biller->cf6 != '') {
                            echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                        }
                        echo '</p>';
                        echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-5">
                    <h2 class=""><?= $customer->name ?></h2>
                    <?php
                        echo '<p>';
                        if ($customer->vat_no != "-" && $customer->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $customer->vat_no;
                        }
                        if ($customer->cf1 != '-' && $customer->cf1 != '') {
                            echo '<br>' . lang('ccf1') . ': ' . $customer->cf1;
                        }
                        if ($customer->cf2 != '-' && $customer->cf2 != '') {
                            echo '<br>' . lang('ccf2') . ': ' . $customer->cf2;
                        }
                        if ($customer->cf3 != '-' && $customer->cf3 != '') {
                            echo '<br>' . lang('ccf3') . ': ' . $customer->cf3;
                        }
                        if ($customer->cf4 != '-' && $customer->cf4 != '') {
                            echo '<br>' . lang('ccf4') . ': ' . $customer->cf4;
                        }
                        if ($customer->cf5 != '-' && $customer->cf5 != '') {
                            echo '<br>' . lang('ccf5') . ': ' . $customer->cf5;
                        }
                        if ($customer->cf6 != '-' && $customer->cf6 != '') {
                            echo '<br>' . lang('ccf6') . ': ' . $customer->cf6;
                        }
                        echo '</p>';
                        echo lang('tel') . ': ' . $customer->phone . '<br />' . lang('email') . ': ' . $customer->email;
                    ?>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="col-xs-5">
                    <span class="bold"><?php echo $agendamento->nomeCategoria.' '.$agendamento->nomeProduto; ?></span><br/>
                    Dada do cadastro: <span class="bold"><?= date('d/m/Y', strtotime($agendamento->dataReserva)); ?></span><br/>
                    Valor: <span class="bold"><?php echo $this->sma->formatMoney($agendamento->valor);?></span><br/>
                    Pagamento em: <span class="bold"><?=$agendamento->nomeTipoCobranca ; ?></span><br/>
                    <?php if ($agendamento->parcelas > 0) {?>
                        Parcelas: <span class="bold"><?=$agendamento->parcelas.'X' ; ?></span><br/>
                    <?php } ?>
                    Status: <span class="bold"><?=lang($agendamento->status); ?></span><br/>
                    <div class="clearfix"></div>
                </div>
                <div class="col-xs-5">
                    <div>
                        Data do Agendamento: <span class="bold"><?= date('d/m/Y', strtotime($agendamento->data)); ?></span><br/>
                        Dia: <span class="bold"><?php echo ''.$this->sma->dataDeHojePorExtensoRetornoComSemana($agendamento->data)?></span><br/>
                        Hora Início: <span class="bold"><?=$agendamento->horaInicio ; ?></span><br/>
                        Hora Termino: <span class="bold"><?=$agendamento->horaTermino ; ?></span><br/>
                        Total de horas do ensaio: <span class="bold"><?php echo $this->sma->diferencaEntreduasHoras($agendamento->horaInicio, $agendamento->horaTermino);?></span><br/>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-sm-12">
                    <?= $product->product_details ? '<div class="well well-sm"><div class="panel-heading">Ensaio</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                    <?= $product->details ? '<div class="well well-sm"><div class="panel-heading">' . lang('Notas importantes') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
</body>
</html>