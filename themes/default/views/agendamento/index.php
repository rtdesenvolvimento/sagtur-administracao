<?php
$v = "";

if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}

if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}

if ($this->input->post('start_hour')) {
    $v .= "&start_hour=" . $this->input->post('start_hour');
}
if ($this->input->post('end_hour')) {
    $v .= "&end_hour=" . $this->input->post('end_hour');
}

if ($this->input->post('produto_filter')) {
    $v .= "&produto_filter=" . $this->input->post('produto_filter');
}

if ($this->input->post('statusFilter')) {
    $v .= "&statusFilter=" . $this->input->post('statusFilter');
}

if ($this->input->post('nomeClienteFilter')) {
    $v .= "&nomeClienteFilter=" . $this->input->post('nomeClienteFilter');
}

?>
<style type="text/css" media="screen">
    #PRData td:nth-child(1) {display: none;}
    #PRData td:nth-child(5) {text-align: center;}
    #PRData td:nth-child(6) {text-align: right;}
    #PRData td:nth-child(7) {text-align: center;}
    #PRData td:nth-child(7) {text-align: center;}
    #PRData td:nth-child(8) {text-align: center;}
    #PRData td:nth-child(9) {text-align: center;}

</style>

<script>

    $(document).ready(function () {
        var oTable = $('#PRData').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= site_url('agendamento/getAgendamentos/?v=1' . $v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": fsd},
                null,
                {"mRender": row_status_agendamento},
                null
                ],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "compromisso_link";
                return nRow;
            },
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 2, filter_default_label: "[<?=lang('product_code');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('product_name');?>]", filter_type: "text", data: []},
        ], "footer");

    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('agendamentos');?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="<?php echo site_url('agendamento/agenda'); ?>" title="Link da agenda online" target="_blank"><i class="icon fa fa-calendar-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?php echo site_url('agendamento/agendar'); ?>" title="Link de agendamento" target="_blank"><i class="icon fa fa-link"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?php echo site_url('calendar/model'); ?>"
                       data-toggle="modal" data-target="#myModal" title="Abrir Agenda do Sistema"><i class="icon fa fa-calendar"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?php echo site_url('agendamento/adicionarAgendamentoBloqueio'); ?>"
                       data-toggle="modal" data-target="#myModal" title="Adicionar Bloqueio de Agendamento"><i class="icon fa fa-clock-o"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="<?php echo site_url('agendamento/adicionarAgendamento'); ?>"
                       data-toggle="modal" data-target="#myModal" title="Adicionar Agendamento Manual"><i class="icon fa fa-plus"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <?php echo form_open("agendamento"); ?>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <?= lang('servico', 'servico'); ?>
                    <?php
                    $cbServicos[''] = lang('select').' '.lang('servico');
                    foreach ($servicos as $servico) {
                        $cbServicos[$servico->id] = $servico->name;
                    } ?>
                    <?= form_dropdown('produto_filter', $cbServicos, (isset($_POST['produto_filter']) ? $_POST['produto_filter'] : "") , 'class="form-control" id="produto_filter"'); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?= lang("status", "statusFilter") ?>
                    <?php
                    $opts = array(
                        '' => lang('select').' '.lang('status'),
                        'PAGO' => lang('PAGO'),
                        'AGUARDANDO_PAGAMENTO' => lang('AGUARDANDO_PAGAMENTO'),
                        'BLOQUEADO' => lang('BLOQUEADO'),
                        'CANCELADA' => lang('CANCELADA'),
                    );
                    echo form_dropdown('statusFilter', $opts, (isset($_POST['statusFilter']) ? $_POST['statusFilter'] : "") , 'class="form-control" id="statusFilter"');
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?= lang("nome_telefone", "nome_telefone"); ?>
                    <?php echo form_input('nomeClienteFilter', (isset($_POST['nomeClienteFilter']) ? $_POST['nomeClienteFilter'] : ""), 'class="form-control" id="nomeClienteFilter"'); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?= lang("start_date", "start_date"); ?>
                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control date" id="start_date"'); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?= lang("end_date", "end_date"); ?>
                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'class="form-control date" id="end_date"'); ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <?= lang("start_hour", "start_hour"); ?>
                    <?php echo form_input('start_hour', (isset($_POST['start_hour']) ? $_POST['start_hour'] : ""), 'class="form-control" id="start_hour"', 'time'); ?>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                    <?= lang("end_hour", "end_hour"); ?>
                    <?php echo form_input('end_hour', (isset($_POST['end_hour']) ? $_POST['end_hour'] : ""), 'class="form-control" id="end_hour"', 'time'); ?>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
        </div>
        <?php echo form_close(); ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="PRData" class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="display: none;"></th>
                            <th style="text-align:left;width: 10%;"><?= lang("category") ?></th>
                            <th style="text-align:left;width: 15%;"><?= lang("servico") ?></th>
                            <th style="text-align:left;width: 20%;"><?= lang("cliente") ?></th>
                            <th style="text-align:center;width: 15%;"><?= lang("cobranca") ?></th>
                            <th style="text-align:center;width: 10%;"><?= lang("valor") ?></th>
                            <th style="text-align:center;width: 10%;"><?= lang("date") ?></th>
                            <th style="text-align:center;width: 15%;"><?= lang("hora") ?></th>
                            <th style="text-align:center;width: 10%;"><?= lang("status") ?></th>
                            <th style="text-align:center;"><?= lang("actions") ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td></tr>
                        </tbody>
                        <tfoot class="dtFilter"></tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>