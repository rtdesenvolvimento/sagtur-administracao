<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]>
<html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $Settings->site_name; ?>|| AGENDAMENTO</title>

    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta name="description" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO">
    <meta name="keywords" content="">
    <meta name="application-name" content="Resultatec Sistamas Digitais">
    <meta name="title" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO">
    <meta name="robots" content="all"/>
    <meta name="language" content="br"/>
    <meta name="robots" content="follow"/>
    <meta property="og:image" content="<?php echo base_url('assets/uploads/logos/logo.png'); ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $Settings->site_name; ?> || AGENDAMENTO"/>
    <meta property="og:url" content="<?=current_url();?>" />
    <meta name="author" content="Resultec Sistemas Digitais"/>

    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/toastr/toastr.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/themes/theme-default.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/estilo.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/datepicker/css/datepicker.css">

    <link rel="icon" href="<?php echo base_url() ?>/assets/images/favicon.ico"/>
    <script type="text/javascript">
        var  site = <?=json_encode(array('base_url' => base_url()));?>;
    </script>


    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
        }

        body,
        html {
            height: 100%;
        }

        .md-demo-synchronized-views {
            position: absolute;
            height: 100%;
        }

        .md-demo-synchronized-views .mbsc-row {
            height: 100%;
        }

        .md-demo-synchronized-views .md-col-right {
            overflow: auto;
            height: 100%;
            border-left: 1px solid #ccc;
        }
    </style>

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/mobiscroll/css/mobiscroll.jquery.min.css">

</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<div class="main-content" style="min-height: 292px;">
    <!-- start: PANEL CONFIGURATION MODAL FORM -->
    <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×
                    </button>
                    <h4 class="modal-title">Panel Configuration</h4>
                </div>
                <div class="modal-body">
                    Here will be a configuration form
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary">
                        Save changes
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->
        <!-- start: TOOLBAR -->
        <div class="toolbar row" style="display: none;">
            <div class="col-sm-6 hidden-xs">
                <div class="page-header">
                    <div style="float: left"><img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" style="    width: 93%;" alt="Logotipo da empresa"></div>
                    <div style="display: none;"><h1><?php echo $Settings->site_name; ?> <small>Agende seu ensaio</small></h1></div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <a href="#" class="back-subviews">
                    <i class="fa fa-chevron-left"></i> BACK
                </a>
                <a href="#" class="close-subviews">
                    <i class="fa fa-times"></i> CLOSE
                </a>
                <div class="toolbar-tools pull-right">
                    <!-- start: TOP NAVIGATION MENU -->
                    <ul class="nav navbar-right">
                        <!-- start: TO-DO DROPDOWN -->
                        <li class="dropdown">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle"
                               data-close-others="true" href="#">
                                <i class="fa fa-plus"></i> SUBVIEW
                                <div class="tooltip-notification fadeIn hide" style="opacity: 0;">
                                    <div class="tooltip-notification-arrow"></div>
                                    <div class="tooltip-notification-inner">
                                        <div>
                                            <div class="semi-bold">
                                                HI THERE!
                                            </div>
                                            <div class="message">
                                                Try the Subview Live Experience
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <ul class="dropdown-menu dropdown-light dropdown-subview">
                                <li class="dropdown-header">
                                    Notes
                                </li>
                                <li>
                                    <a href="#newNote" class="new-note"><span class="fa-stack"> <i
                                                    class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Add new note</a>
                                </li>
                                <li>
                                    <a href="#readNote" class="read-all-notes"><span class="fa-stack"> <i
                                                    class="fa fa-file-text-o fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Read all notes</a>
                                </li>
                                <li class="dropdown-header">
                                    Calendar
                                </li>
                                <li>
                                    <a href="#newEvent" class="new-event"><span class="fa-stack"> <i
                                                    class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Add new event</a>
                                </li>
                                <li>
                                    <a href="#showCalendar" class="show-calendar"><span class="fa-stack"> <i
                                                    class="fa fa-calendar-o fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Show calendar</a>
                                </li>
                                <li class="dropdown-header">
                                    Contributors
                                </li>
                                <li>
                                    <a href="#newContributor" class="new-contributor"><span class="fa-stack"> <i
                                                    class="fa fa-user fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-plus fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Add new contributor</a>
                                </li>
                                <li>
                                    <a href="#showContributors" class="show-contributors"><span class="fa-stack"> <i
                                                    class="fa fa-user fa-stack-1x fa-lg"></i> <i
                                                    class="fa fa-share fa-stack-1x stack-right-bottom text-danger"></i> </span>
                                        Show all contributor</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle"
                               data-close-others="true" href="#">
                                <span class="messages-count badge badge-default hide">3</span> <i
                                        class="fa fa-envelope"></i> MESSAGES
                            </a>
                            <ul class="dropdown-menu dropdown-light dropdown-messages">
                                <li>
                                    <span class="dropdown-header"> You have 9 messages</span>
                                </li>
                                <li>
                                    <div class="drop-down-wrapper ps-container">
                                        <ul>
                                            <li class="unread">
                                                <a href="javascript:;" class="unread">
                                                    <div class="clearfix">
                                                        <div class="thread-image">
                                                            <img src="./assets/images/avatar-2.jpg" alt="">
                                                        </div>
                                                        <div class="thread-content">
                                                            <span class="author">Nicole Bell</span>
                                                            <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span class="time"> Just Now</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;" class="unread">
                                                    <div class="clearfix">
                                                        <div class="thread-image">
                                                            <img src="./assets/images/avatar-3.jpg" alt="">
                                                        </div>
                                                        <div class="thread-content">
                                                            <span class="author">Steven Thompson</span>
                                                            <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span class="time">8 hrs</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <div class="clearfix">
                                                        <div class="thread-image">
                                                            <img src="./assets/images/avatar-5.jpg" alt="">
                                                        </div>
                                                        <div class="thread-content">
                                                            <span class="author">Kenneth Ross</span>
                                                            <span class="preview">Duis mollis, est non commodo luctus, nisi erat porttitor ligula...</span>
                                                            <span class="time">14 hrs</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="view-all">
                                    <a href="pages_messages.html">
                                        See All
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="menu-search">
                            <a href="#">
                                <i class="fa fa-search"></i> SEARCH
                            </a>
                            <!-- start: SEARCH POPOVER -->
                            <div class="popover bottom search-box transition-all">
                                <div class="arrow"></div>
                                <div class="popover-content">
                                    <!-- start: SEARCH FORM -->
                                    <form class="" id="searchform" action="#">
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Search">
                                            <span class="input-group-btn">
																<button class="btn btn-main-color btn-squared"
                                                                        type="button">
																	<i class="fa fa-search"></i>
																</button> </span>
                                        </div>
                                    </form>
                                    <!-- end: SEARCH FORM -->
                                </div>
                            </div>
                            <!-- end: SEARCH POPOVER -->
                        </li>
                    </ul>
                    <!-- end: TOP NAVIGATION MENU -->
                </div>
            </div>
        </div>
        <!-- end: TOOLBAR -->
        <!-- end: PAGE HEADER -->
        <!-- start: BREADCRUMB -->
        <div class="row" style="display:none;">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="#">
                            Dashboard
                        </a>
                    </li>
                    <li class="active">
                        Form Wizard
                    </li>
                </ol>
            </div>
        </div>
        <!-- end: BREADCRUMB -->
        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-sm-12">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-heading" style="text-align: center;">
                        <h4 class="panel-title">
                            <img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="Logotipo da empresa">
                        </h4>
                    </div>
                    <div class="panel-body">
                        <form action="<?php base_url()?>" role="form" method="post" class="smart-wizard form-horizontal" id="form">
                            <div id="" class="swMain">
                                <div class="stepContainer" style="height: 353px;">
                                    <div class="progress progress-xs transparent-black no-radius active content">
                                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar"
                                             class="progress-bar partition-green step-bar" style="width: 100%;">
                                            <span class="sr-only"> 0% Complete (success)</span>
                                        </div>
                                    </div>
                                    <div id="step-1" class="content" style="display: block;">
                                        <div id="tbHorarios">
                                            <div class="panel panel-white">
                                                <div class="panel-heading border-light">
                                                    <h4 class="panel-title"><i class="fa fa-calendar"></i> DISPONIBILÍDADE DE HORÁRIOS </h4>
                                                </div>
                                                <div class="panel-body">
                                                    <div>
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">
                                                                Consultar na data <span class="symbol required"></span>
                                                            </label>
                                                            <div class="col-sm-7">
                                                                <input type="date" value="<?php echo date('Y-m-d');?>" class="form-control" name="dataAgendamento" id="dataAgendamento" required="required">
                                                            </div>
                                                        </div>
                                                        <div id="horariosServico"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div id="calendario" style="display: none;">
                                            <div class="panel panel-white">
                                                <div class="panel-heading border-light">
                                                    <h4 class="panel-title"><i class="fa fa-calendar"></i> ENSAIOS POR DATA </h4>
                                                </div>
                                                <div class="panel-body">

                                                    <div class="row" style="height: 1000px;">

                                                        <div mbsc-page class="demo-synchronized-views">
                                                            <div style="height:100%">
                                                                <div class="mbsc-grid md-demo-synchronized-views">
                                                                    <div class="mbsc-row mbsc-no-padding">
                                                                        <div class="mbsc-col-md-4 mbsc-col-12">
                                                                            <div id="demo-month"></div>
                                                                        </div>
                                                                        <div class="mbsc-col-md-8 mbsc-col-12 md-col-right">
                                                                            <div id="demo-day"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="actionBar">
                                    <div class="msgBox">
                                        <div class="content"></div>
                                        <a href="#" class="close">X</a></div>
                                    <div class="loader">Loading</div>
                                    <a href="#" class="buttonFinish buttonDisabled">Finish</a><a href="#"
                                                                                                 class="buttonNext">Next</a><a
                                            href="#" class="buttonPrevious buttonDisabled">Previous</a></div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- end: FORM WIZARD PANEL -->
            </div>
        </div>
        <!-- end: PAGE CONTENT-->
    </div>

    <footer class="inner">
        <div class="footer-inner">
            <div class="pull-left">
                <?php echo date('Y');?> © RJFotografia.
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="fa fa-chevron-up"></i></span>
            </div>
        </div>
    </footer>
    <div class="subviews">
        <div class="subviews-container"></div>
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/rapido/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url() ?>/assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js"
        charset="UTF-8"></script>

<script src="<?php echo base_url() ?>assets/rapido/js/subview.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/subview-examples.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/form-wizard.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url() ?>assets/rapido/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/pages-timeline.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/mobiscroll/js/mobiscroll.jquery.min.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->


<script>
    jQuery(document).ready(function() {

        carregarHorario();

        FormWizard.init();
        Timeline.init();

        $('#dataAgendamento').change(function (event){
            carregarHorario();
        });
    });

    function carregarHorario() {
        let data = $('#dataAgendamento').val();
        $.blockUI({message: '<h3> Carregando Agenda <br/> Aguarde ...</h3>'});

        $('#horariosServico').load('<?php echo base_url().'agendamento/getHorariosConsulta/';?>'+data, function (event) {
            Main.init();
            $.unblockUI();
        });
    }
</script>


<script>
    /*
    mobiscroll.settings = {
        theme: 'ios',
        themeVariant: 'light',
        lang: 'en'
    };

    $(function () {

        var monthInst,
            dayInst,
            preventSet;

        dayInst = $('#demo-day').mobiscroll().eventcalendar({

            lang: 'en',               // Specify language like: lang: 'pl' or omit setting to use default
            theme: 'ios',                           // Specify theme like: theme: 'ios' or omit setting to use default
            themeVariant: 'light',                  // More info about themeVariant: https://docs.mobiscroll.com/4-10-6/eventcalendar#opt-themeVariant
            display: 'inline',                      // Specify display mode like: display: 'bottom' or omit setting to use default
            view: {                                 // More info about view: https://docs.mobiscroll.com/4-10-6/eventcalendar#opt-view
                eventList: { type: 'day' }
            },
            onPageChange: function (event, inst) {  // More info about onPageChange: https://docs.mobiscroll.com/4-10-6/eventcalendar#event-onPageChange
                preventSet = true;
                navigate(monthInst, event.firstDay);
            }
        }).mobiscroll('getInst');

        monthInst = $('#demo-month').mobiscroll().eventcalendar({

            lang: 'en',               // Specify language like: lang: 'pl' or omit setting to use default
            theme: 'ios',                           // Specify theme like: theme: 'ios' or omit setting to use default
            themeVariant: 'light',                  // More info about themeVariant: https://docs.mobiscroll.com/4-10-6/eventcalendar#opt-themeVariant
            display: 'inline',                      // Specify display mode like: display: 'bottom' or omit setting to use default
            view: {                                 // More info about view: https://docs.mobiscroll.com/4-10-6/eventcalendar#opt-view
                calendar: { type: 'month' }
            },
            onSetDate: function (event, inst) {     // More info about onSetDate: https://docs.mobiscroll.com/4-10-6/eventcalendar#event-onSetDate
                if (!preventSet) {
                    navigate(dayInst, event.date);
                }
                preventSet = false;
            }
        }).mobiscroll('getInst');

        function navigate(inst, val) {
            if (inst) inst.navigate(val);
        }

        mobiscroll.util.getJson('https://trial.mobiscroll.com/events/', function (events) {
            events = null;
            $.ajax({
                type: "GET",
                url: site.base_url + "calendar/get_events_jsonP",
                data: {
                    start : '2020-01-01',
                    end : '2020-12-31'
                },
                dataType: 'json',
                success: function (events) {
                    dayInst.setEvents(events);
                    monthInst.setEvents(events);

                }
            });

        }, 'jsonp');

    });
     */
</script>

</body>
<!-- end: BODY -->
</html>