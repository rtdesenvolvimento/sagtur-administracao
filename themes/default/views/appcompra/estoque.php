<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Resultatec">
    <meta name="robots" content="noindex">

    <title><?php echo $this->Settings->site_name;?> || RESERVA</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="theme-color" content="#e58800"/>

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $this->Settings->site_name;?> || RESERVA" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $this->Settings->site_name;?> || RESERVA">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $this->Settings->site_name;?>"  />
    <meta property="og:description" content="RESERVA ONLINE" />
    <meta property="og:site_name" content="<?php echo $this->Settings->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $this->Settings->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">


    <?php

    $corPrincipalDoSite = '#ffffff';

    ?>
    <style>
        .content-left {
            background-color: <?=$corPrincipalDoSite; ?>;
            padding: 0;
        }

        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            color: #fff;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
        }

        .profile_img {
            padding: 17px;
            width: 200px;
        }

        .pacotes-home h2 {
            text-align: center;
            font-size: 53px;
            color: #333;
            margin-bottom: 10px;
            margin-top: 30px;
        }

        .pacotes-home h4 {
            text-align: center;
            color: #333;
            font-size: 33px;
        }

        .pacotes-home p {
            text-align: center;
            font-size: 20px;
            color: #333;
        }

        .combobox-combox {
            display: flex;
            justify-content: space-around;
            margin-bottom: -30px;
            margin-top: 30px;
        }

        .mes-titulo-pacotes {
            display: flex;
            justify-content: space-around;
            margin-bottom: 30px;
            margin-top: 60px;
        }

        .linha-titulo-mes {
            background-color: #e58801;
            height: 5px;
            width: 25%;
            margin-top: 15px;
        }

        .nice-select .list {
            background-color: #fff;
            border-radius: 3px;
            box-shadow: 0 0 0 1px rgb(68 68 68 / 11%);
            box-sizing: border-box;
            margin-top: 4px;
            opacity: 0;
            overflow: hidden;
            padding: 0;
            pointer-events: none;
            position: absolute;
            top: 100%;
            left: 0;
            transform-origin: 50% 0;
            transform: scale(0.75) translateY(-25px);
            transition: all 0.2s cubic-bezier(0.5, 0, 0, 1.25), opacity 0.15s ease-out;
            z-index: 9999;
            height: max-content;
            overflow: auto;
        }

        .nice-select.wide {
            width: 100%;
            background: #f9f9f9;
            color: #222222;
            font-size: 12px;
        }

        .box_compra  {
            font-size: 25px;
            font-weight: 400;
            color: #000;
            margin: 0px 0px 15px 0px;
        }

        .continer {
            background-color: #ffffff;
            padding: 10px;
            border: solid 1px #d8d8d8;
            margin: 10px 10px 15px 10px;
            box-shadow: 0 2px 15px #ededed;
        }

        .combo {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            background-color: #fff;
            border-radius: 3px;
            border: 1px solid #d2d8dd;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            display: block;
            float: left;
            font-family: inherit;
            font-size: 14px;
            font-weight: normal;
            height: 42px;
            line-height: 40px;
            outline: none;
            padding-left: 15px;
            padding-right: 27px;
            position: relative;
            text-align: left !important;
            transition: all 0.2s ease-in-out;
            user-select: none;
            white-space: nowrap;
            width: 100%;
            color: #6c757d;
        }
    </style>
</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<main id="general_page">

    <div class="pacotes-home">
        <div class="grid">
            <h2>Vagas Por Pacotes</h2>
        </div>
    </div>

    <div class="combobox-combox" style="box-shadow: 0 2px 15px #ededed;    text-align: center;">
        <div class="container margin_30">
            <div class="col-12">
                <div class="styled-select clearfix" >
                    <label> <div class="pacotes-home"> <h4>Qual Mês Deseja? </h4>  </div></label><br/>
                    <select class="combo" id="mes" onchange="buscar();">
                        <option value="all" <?php if($mesFilter == 'all') echo ' selected="selected"';?>>Ver todos</option>
                        <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                        <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                        <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                        <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                        <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                        <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                        <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                        <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                        <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                        <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                        <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                        <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                    </select>
                </div>
            </div>
            <div class="col-12">
                <div class="styled-select clearfix" >
                    <label> <div class="pacotes-home"> <h4>Qual Viagem? </h4>  </div></label><br/>
                    <select class="combo" id="destino" onchange="buscar();">
                        <option value="" >VER TODOS OS DESTINOS</option>
                        <?php
                        $programacoesFilterByName = array_column($programacoesFilter, 'name');
                        array_multisort($programacoesFilterByName, SORT_ASC, $programacoesFilter);

                        $produtoId = null;
                        foreach ($programacoesFilter as $programacao) {
                            if ($produtoId !== $programacao->produtoId){
                                $produtoId = $programacao->produtoId;
                                ?>
                                <?php if ($programacao->getTotalDisponvel() > 0) {?>
                                    <?php if ($destinoFilter == $programacao->produtoId){?>
                                        <option selected value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php }?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <?php
    $mesAtual = '';
    if (!empty($programacoes)){
        foreach ($programacoes as $programacaoEstoque) {

        if ($programacaoEstoque->getTotalDisponvel() <= 0) continue; ?>
            <?php
                $mesSaida = $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida(), '%B');
                $anoSaida = date("Y", strtotime($programacaoEstoque->getDataSaida()));

            if ($mesSaida != $mesAtual || $anoSaida != $anoAtual) {?>
                    <div class="mes-titulo-pacotes">
                        <div class="linha-titulo-mes"></div>
                        <h3><?php echo strtoupper($mesSaida).'/'.$anoSaida;?></h3>
                        <div class="linha-titulo-mes"></div>
                    </div>
                    <?php
                    $mesAtual = $mesSaida;
                    $anoAtual = $anoSaida;
                    ?>
                <?php }?>
                <div class="container_styled_1">
                    <div class="container margin_30">
                        <div class="row continer">
                            <div class="col-lg-12 ml-lg-12 add_top_30">

                                <table width="100%">
                                    <tr>
                                        <td style="padding: 10px;text-align: center;">
                                            <h4 class="nomargin_top"><?php echo $programacaoEstoque->name;?></h4>
                                            <div style="font-weight: bold;font-size: 25px;color: #e58801;">
                                            <?php if ($programacaoEstoque->getTotalDisponvel() > 0) {?>
                                                <?php echo $programacaoEstoque->getTotalDisponvel().'VAGAS';?>
                                            <?php } else {?>
                                                <?php echo $programacaoEstoque->getTotalDisponvel().'VAGA';?>
                                            <?php } ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 1px solid #ccc;padding: 10px;">
                                            <p><span style="font-weight: bold;color: #e58801;">IDA</span><strong><?php echo ' '.$this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida()); ?> <?php if ($programacaoEstoque->horaSaida != '00:00:00') echo ' <br/><i class="icon-clock">'.date('H:i:s', strtotime($programacaoEstoque->horaSaida)).'h' ;?></i></strong></p>
                                            <p style="margin-top: 10px;"><span style="font-weight: bold;color: #e58801;">VOLTA</span><strong><?php echo  ' '.$this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataRetorno()); ?>  <?php if ($programacaoEstoque->horaRetorno != '00:00:00') echo ' <br/><i class="icon-clock">'.date('H:i:s', strtotime($programacaoEstoque->horaRetorno)).'h';?></i></strong></p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: 1px solid #ccc;padding: 10px;background: #e9ecef;">
                                            <?php if ($programacaoEstoque->precoExibicaoSite > 0){?>
                                                <span style="float: right;text-align: right;">
                                                    <small style="font-size: 16px;color: #e58801;font-weight: bold;"> <i class="icon-alert"></i> VALOR EXIBIDO NA LOJA</small><br/>
                                                    <small style="font-size: 16px;"><?php echo $programacaoEstoque->valor_pacote;?></small><br/>
                                                    <span class="box_compra"><?php echo  $this->sma->formatMoney($programacaoEstoque->precoExibicaoSite, $programacaoEstoque->simboloMoeda);?> </span><small> / por pessoa</small>
                                                </span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>
                                <table class="table table-bordered table-condensed table-striped table-hover" style="cursor: pointer;">
                                    <thead>
                                    <tr class="active">
                                        <th class="col-md-8" style="text-align: left;"><?= lang('tipo') ?></th>
                                        <th class="col-md-1" style="text-align: center;"><?= lang('valor') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($valorFaixas as $valoresFaixa) {
                                        $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaByFaixaId($programacaoEstoque->produto, $valoresFaixa->id);
                                        if ($faixa) {?>
                                            <?php
                                            $ativo = $faixa->status == 'ATIVO' ? true  : false; ?>
                                            <?php if ($ativo){?>
                                                <tr>
                                                    <td><?php echo $faixa->name;?></td>
                                                    <td style="text-align: center;"><?php if($faixa->valor > 0) echo $this->sma->formatMoney($faixa->valor); else echo 'Grátis';?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <?php
                                    foreach ($tiposQuarto as $tipoQuarto){?>
                                        <?php
                                        foreach ($valorFaixas as $valoresFaixa) {
                                            $faixa = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemRodoviarioByTipoHospedagemTipoFaixa($programacaoEstoque->produto, $tipoQuarto->id, $valoresFaixa->id);

                                            $quarto = $programacaoEstoque->getQuartos()[(int)$tipoQuarto->id];
                                            $tipoEspedagemSemEstoque = false;
                                            $nomeTipoQuartoEstoque = '';
                                            $nomeTipoQuarto = $tipoQuarto->name;

                                            if ($quarto && $programacaoEstoque->getControleEstoqueHospedagem() )  {
                                                $nomeTipoQuarto = $nomeTipoQuarto.' - Acomoda '.$quarto->acomoda.' pessoa(s) por quarto';
                                                $nomeTipoQuartoEstoque = 'Quarto(s) Disponíveis: <b>' . $quarto->qtd_quartos_disponivel . ' quarto(s) </b> <br/>Vagas Disponíveis: <b>'.$quarto->estoque_atual.' pessoa(s)'.'</b>';
                                            }

                                            if ($faixa) {
                                                $ativo = $faixa->status == 'ATIVO' ? true  : false; ?>
                                                <?php if ($ativo){?>
                                                    <?php if ($tipoQuartoId != $tipoQuarto->id) {
                                                        $tipoQuartoId = $tipoQuarto->id;
                                                        ?>
                                                        <tr>
                                                            <td colspan="3" class="warning" style="font-weight: 500;"><h4><?php echo $nomeTipoQuarto;?></h4><?php echo $nomeTipoQuartoEstoque;?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    <tr>
                                                        <td><?php echo $faixa->name;?></td>
                                                        <td style="text-align: center;"><?php if($faixa->valor > 0) echo $this->sma->formatMoney($faixa->valor); else echo 'Grátis';?></td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="border-top: 1px solid #ccc;padding: 10px;">
                                            <?php if ($Settings->own_domain){?>
                                                <a href="<?php echo base_url().'carrinho/'.$programacaoEstoque->programacaoId.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> IR PARA LINK</a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->programacaoId.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> IR PARA LINK</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php if($programacaoEstoque->getTotalDisponvel() > 0) {?>
                                                <?php if ($Settings->own_domain){?>
                                                    <a href="https://api.whatsapp.com/send?text=<?php echo base_url().'carrinho/'.$programacaoEstoque->programacaoId.'/'.$vendedor->id; ?>" class="btn_1 white" style="background: #333333;margin-top: 10px;width: 100%;" target="_blank"><img src="<?php echo base_url();?>/assets/images/whatsapp-icon.png" style="width: 5%;" /> COMPARTILHAR</a>
                                                <?php  } else {?>
                                                    <a href="https://api.whatsapp.com/send?text=<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->programacaoId.'/'.$vendedor->id; ?>" class="btn_1 white" style="background: #333333;margin-top: 10px;width: 100%;" target="_blank"><img src="<?php echo base_url();?>/assets/images/whatsapp-icon.png" style="width: 5%;" /> COMPARTILHAR</a>
                                                <?php } ?>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        <?php }?>
    <?php } else { ?>
    <div class="mes-titulo-pacotes">
        <div class="container margin_30">
            <div class="row continer">
                <div class="col-lg-12 ml-lg-12 add_top_30" style="text-align: center">
                    <div class="mes-titulo-pacotes">
                        <h3>Nenhuma viagem encontrada</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</main>

<footer class="clearfix">
    <div class="container">
        <p>&copy; <?= date('Y')?> <a href="https://www.sagtur.com.br" target="_blank">Desenvolvido por SAGTur Sistema para Agências de Turismo</a><?php echo " | Licenciado para " . $this->Settings->site_name; ?></p>
    </div>
</footer>
<!-- end footer-->

<div class="cd-overlay-nav">
    <span></span>
</div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content">
    <span></span>
</div>

<!-- /cd-overlay-content -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js"></script>

<script>
    function buscar() {
        let destino = $("#destino option:selected" ).val();
        let mes = $("#mes option:selected" ).val();

        <?php if ($Settings->own_domain){?>
            window.location = '<?php echo base_url().'estoque/'.$vendedor->id.'?mes=';?>'+mes+'&destino='+destino;
        <?php } else { ?>
            window.location = '<?php echo base_url().$this->session->userdata('cnpjempresa').'/estoque/'.$vendedor->id.'?mes=';?>'+mes+'&destino='+destino;
        <?php } ?>
    }

</script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>

</body>
</html>