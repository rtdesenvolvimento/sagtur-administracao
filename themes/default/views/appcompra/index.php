<?php
$category  = $this->site->getCategoryByID($produto->category_id);
$isSelecionarCompradorAutomatico = true;

function coletarPagador($product)
{
    return $product->isApenasColetarPagador == 1 || $product->isApenasColetarPagador == 2;
}

$plugsign = false;
$lista_espera = $programacao->getTotalDisponvel() <= 0 && $produto->permitirListaEmpera;
$apenas_cotacao = (bool) $produto->apenas_cotacao;

if ($this->Settings->gerar_contrato_faturamento_venda_site && !$apenas_cotacao && !$lista_espera) {
    $plugsignSetting = $this->site->getPlugsignSettings();

    if ($plugsignSetting->active && $plugsignSetting->token) {
        $plugsign = true;
    } else {
        $plugsign = false;
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
<head>

    <?php if ($produto->tag_title) {?>
        <title><?=$produto->tag_title;?> - <?=$configuracaoGeral->site_name;?> </title>
        <meta name="title" content="<?=$configuracaoGeral->site_name;?> - <?=str_replace("'", "", $produto->tag_title);?>">

        <meta property="og:title" content="<?=str_replace("'", "", $produto->tag_title) ;?>"/>
    <?php } else { ?>
        <title><?php echo $configuracaoGeral->site_name;?> - <?=$produto->name;?></title>
        <meta name="title" content="<?php echo $configuracaoGeral->site_name;?> - <?=$produto->name;?>">

        <meta property="og:title" content="<?=$configuracaoGeral->site_name;?>"  />
    <?php } ?>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#e58800">
    <meta name="theme-color" content="#e58800"/>

    <!--Cabecalho-->
    <meta name="keywords" content="<?=$configuracaoGeral->site_name.','.implode(', ', explode(' ', $produto->name));?>">

    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">

    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta name="author" content="<?=$configuracaoGeral->site_name;?>"/>
    <meta name="generator" content="<?=$configuracaoGeral->site_name;?>" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_BR" />
    <meta property="og:image" content="<?php echo base_url().'/assets/uploads/'.$produto->image; ?>" />

    <?php if ($produto->meta_tag_description) {?>
        <meta name="description" content="<?=$configuracaoGeral->site_name;?> - <?=$produto->meta_tag_description;?> || RESERVA ONLINE" >
        <meta property="og:description" content="<?php echo $produto->meta_tag_description;?> || RESERVA ONLINE" />
        <meta name="twitter:description" content="<?php echo $produto->meta_tag_description;?> || RESERVA ONLINE" />
    <?php } else { ?>
        <meta name="description" content="<?=$configuracaoGeral->site_name;?> || RESERVA ONLINE" >
        <meta property="og:description" content="<?=str_replace("'", "", $produto->name).' DIA '.date("d/m/Y", strtotime($programacao->dataSaida)) ;?>" />
        <meta name="twitter:description" content="<?=str_replace("'", "", $produto->name).' DIA '.date("d/m/Y", strtotime($programacao->dataSaida)) ;?>" />
    <?php } ?>

    <meta property="og:site_name" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />

    <meta name="twitter:card" content="product" />
    <meta name="twitter:site" content="@None" />
    <meta name="twitter:creator" content="@None" />
    <meta name="twitter:domain" content="<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id;?>" />
    <meta name="twitter:url" content="<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id;?>?utm_source=twitter&utm_medium=twitter&utm_campaign=twitter" />
    <meta name="twitter:title" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta name="twitter:image" content="<?php echo base_url().'/assets/uploads/'.$produto->image; ?>" />

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">

    <!-- CONFIRM JQUERY -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/jquery-confirm.min.css" rel="stylesheet">

    <style>

        /*
Theme Name: Wilio
Theme URI: http://www.ansonika.com/wilio/
Author: Ansonika
Author URI: http://themeforest.net/user/Ansonika/

[Table of contents]

1. SITE STRUCTURE and TYPOGRAPHY
- 1.1 Typography
- 1.2 Buttons
- 1.3 Structure

2. CONTENT
- 2.1 Wizard
- 2.2 Success submit
- 2.3 Inner pages

3. COMMON
- 3.1 Misc
- 3.2 Spacing

/*============================================================================================*/
        /* 1.  SITE STRUCTURE and TYPOGRAPHY */
        /*============================================================================================*/
        /*-------- 1.1 Typography --------*/
        /* rem reference
        10px = 0.625rem
        12px = 0.75rem
        14px = 0.875rem
        16px = 1rem (base)
        18px = 1.125rem
        20px = 1.25rem
        24px = 1.5rem
        30px = 1.875rem
        32px = 2rem
        */
        html,
        body {
            height: 100%;
        }

        html * {
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        body {
            background: white;
            font-size: 0.875rem;
            line-height: 1.4;
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            color: #555555;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            color: #222222;
        }

        p {
            margin-bottom: 5px;
        }

        strong {
            font-weight: 500;
        }

        label {
            font-weight: 400;
            margin-bottom: 3px;
            color: #222222;
        }

        hr {
            margin: 30px 0 30px 0;
            border-color: #dddddd;
        }

        ul, ol {
            /*
            list-style: none;
            margin: 0 0 25px 0;
            padding: 0;
            */
        }

        /*General links color*/
        a {
            color: #1b0088;
            text-decoration: none;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            outline: none;
        }
        a:hover, a:focus {
            color: #111;
            text-decoration: none;
            outline: none;
        }

        a.animated_link {
            position: relative;
            text-decoration: none;
        }

        a.animated_link {
            position: relative;
            text-decoration: none;
        }
        a.animated_link:before {
            content: "";
            position: absolute;
            width: 100%;
            height: 1px;
            bottom: -5px;
            opacity: 1;
            left: 0;
            background-color: #1b0088;
            visibility: hidden;
            -webkit-transform: scaleX(0);
            transform: scaleX(0);
            -moz-transition: all 0.3s ease;
            -o-transition: all 0.3s ease;
            -webkit-transition: all 0.3s ease;
            -ms-transition: all 0.3s ease;
            transition: all 0.3s ease;
        }
        a.animated_link:hover:before {
            visibility: visible;
            -webkit-transform: scaleX(1);
            transform: scaleX(1);
        }

        a.animated_link.active {
            position: relative;
            text-decoration: none;
            color: #1b0088;
        }
        a.animated_link.active:before {
            content: "";
            position: absolute;
            width: 100%;
            height: 1px;
            bottom: -5px;
            opacity: 1;
            left: 0;
            background-color: #1b0088;
            visibility: visible;
            -webkit-transform: scaleX(1);
            transform: scaleX(1);
        }

        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            color: #fff;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
        }
        a.btn_1:hover,
        .btn_1:hover {
            background-color: #e58800;
        }
        a.btn_1.full-width,
        .btn_1.full-width {
            display: block;
            width: 100%;
            text-align: center;
            margin-bottom: 5px;
        }
        a.btn_1.small,
        .btn_1.small {
            padding: 7px 10px;
            font-size: 13px;
            font-size: 0.8125rem;
        }
        a.btn_1.medium,
        .btn_1.medium {
            font-size: 16px;
            font-size: 1rem;
            padding: 18px 30px;
        }
        a.btn_1.rounded,
        .btn_1.rounded {
            -webkit-border-radius: 25px !important;
            -moz-border-radius: 25px !important;
            -ms-border-radius: 25px !important;
            border-radius: 25px !important;
            -webkit-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.2);
            -moz-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.2);
        }

        /*-------- 1.3 Structure --------*/
        /* Preloader */
        #preloader {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 100%;
            bottom: 0;
            background-color: #fff;
            z-index: 999999;
        }

        [data-loader="circle-side"] {
            position: absolute;
            width: 50px;
            height: 50px;
            top: 50%;
            left: 50%;
            margin-left: -25px;
            margin-top: -25px;
            -webkit-animation: circle infinite .95s linear;
            -moz-animation: circle infinite .95s linear;
            -o-animation: circle infinite .95s linear;
            animation: circle infinite .95s linear;
            border: 2px solid #333;
            border-top-color: rgba(0, 0, 0, 0.2);
            border-right-color: rgba(0, 0, 0, 0.2);
            border-bottom-color: rgba(0, 0, 0, 0.2);
            border-radius: 100%;
        }

        #loader_form {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            width: 100%;
            height: 100%;
            bottom: 0;
            background-color: #fff;
            background-color: rgba(255, 255, 255, 0.6);
            z-index: 999999;
            display: none;
        }

        [data-loader="circle-side-2"] {
            position: absolute;
            width: 50px;
            height: 50px;
            top: 50%;
            left: 50%;
            margin-left: -25px;
            margin-top: -25px;
            -webkit-animation: circle infinite .95s linear;
            -moz-animation: circle infinite .95s linear;
            -o-animation: circle infinite .95s linear;
            animation: circle infinite .95s linear;
            border: 2px solid #333;
            border-top-color: rgba(0, 0, 0, 0.2);
            border-right-color: rgba(0, 0, 0, 0.2);
            border-bottom-color: rgba(0, 0, 0, 0.2);
            border-radius: 100%;
        }

        @-webkit-keyframes circle {
            0% {
                -webkit-transform: rotate(0);
                -ms-transform: rotate(0);
                -o-transform: rotate(0);
                transform: rotate(0);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-moz-keyframes circle {
            0% {
                -webkit-transform: rotate(0);
                -ms-transform: rotate(0);
                -o-transform: rotate(0);
                transform: rotate(0);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @-o-keyframes circle {
            0% {
                -webkit-transform: rotate(0);
                -ms-transform: rotate(0);
                -o-transform: rotate(0);
                transform: rotate(0);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        @keyframes circle {
            0% {
                -webkit-transform: rotate(0);
                -ms-transform: rotate(0);
                -o-transform: rotate(0);
                transform: rotate(0);
            }

            100% {
                -webkit-transform: rotate(360deg);
                -ms-transform: rotate(360deg);
                -o-transform: rotate(360deg);
                transform: rotate(360deg);
            }
        }

        .row-height {
            height: 100vh;
        }
        @media (max-width: 991px) {
            .row-height {
                height: auto;
            }
        }

        .content-left {
            background-color: #1b0088;
            padding: 0;
        }

        .content-left-wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            height: 100%;
            min-height: 100%;
            padding: 15px 90px 35px 90px;
            background-color: #1b0088;
            color: #fff;
            text-align: center;
            position: relative;
            background: transparent;
            background: -webkit-linear-gradient(top, transparent, rgba(0, 0, 0, 0.5));
            background: linear-gradient(to bottom, transparent, rgba(0, 0, 0, 0.5));
        }
        @media (max-width: 991px) {
            .content-left-wrapper {
                height: auto;
                padding: 15px 30px 35px 30px;
            }
        }
        @media (max-width: 767px) {
            .content-left-wrapper {
                padding: 15px 15px 35px 15px;
            }
        }
        @media (max-width: 991px) {
            .content-left-wrapper figure img {
                height: auto;
            }
        }
        .content-left-wrapper h2 {
            color: #fff;
            font-size: 32px;
            font-size: 2rem;
            margin: 20px 0 15px 0;
            font-weight: 400;
        }
        @media (max-width: 767px) {
            .content-left-wrapper h2 {
                font-size: 26px;
                font-size: 1.625rem;
            }
        }
        .content-left-wrapper p {
            font-size: 15px;
            font-size: 0.9375rem;
            opacity: 0.8;
        }
        @media (max-width: 767px) {
            .content-left-wrapper p {
                font-size: 14px;
                font-size: 0.875rem;
            }
        }
        .content-left-wrapper .btn_1 {
            margin: 25px 0 25px 0;
        }
        @media (max-width: 991px) {
            .content-left-wrapper .btn_1 {
                display: none;
            }
        }
        .content-left-wrapper .btn_1.mobile_btn {
            display: none;
        }
        @media (max-width: 767px) {
            .content-left-wrapper .btn_1.mobile_btn {
                margin: 5px 0 1px 0;
                display: inline-block;
            }
        }

        .content-right {
            padding: 60px;
            min-height: 100%;
            display: flex;
            justify-content: center;
        }
        @media (max-width: 991px) {
            .content-right {
                height: auto;
                padding: 30px 15px;
            }
        }

        a#logo {
            position: absolute;
            left: 20px;
            top: 15px;
            display: block;
            height: 35px;
        }
        @media (max-width: 991px) {
            a#logo {
                left: 15px;
                top: 10px;
            }
        }

        #social {
            position: absolute;
            top: 15px;
            right: 20px;
        }
        @media (max-width: 991px) {
            #social {
                right: 70px;
            }
        }
        #social ul {
            margin: 0;
            padding: 0;
            text-align: center;
        }
        #social ul li {
            float: left;
            margin: 0 5px 10px 0;
            list-style: none;
        }
        #social ul li a {
            /*color: #fff;*/
            opacity: 0.6;
            text-align: center;
            line-height: 35px;
            display: block;
            font-size: 16px;
            font-size: 1rem;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        #social ul li a:hover {
            opacity: 1;
        }

        .copy {
            position: absolute;
            bottom: 25px;
            left: 0;
            width: 100%;
            opacity: 0.5;
        }
        @media (max-width: 991px) {
            .copy {
                display: none;
            }
        }

        /*============================================================================================*/
        /* 2.  CONTENT */
        /*============================================================================================*/
        /*-------- 2.1 Wizard --------*/
        #left_form {
            text-align: center;
        }
        #left_form h2 {
            font-size: 28px;
            font-size: 1.75rem;
            color: #0686D8;
        }
        @media (max-width: 767px) {
            #left_form figure img {
                height: 130px;
                width: auto;
            }
        }

        input#website {
            display: none;
        }

        #wizard_container {
            width: 100%;
        }
        @media (max-width: 767px) {
            #wizard_container {
                width: 100%;
            }
        }

        h3.main_question {
            margin: 0 0 20px 0;
            padding: 0;
            font-weight: 500;
            font-size: 18px;
            font-size: 1.125rem;
        }
        h3.main_question strong {
            display: block;
            color: #999;
            margin-bottom: 5px;
        }

        /* Wizard Buttons*/
        button.backward,
        button.forward,
        button.submit {
            border: none;
            color: #fff;
            text-decoration: none;
            transition: background .5s ease;
            -moz-transition: background .5s ease;
            -webkit-transition: background .5s ease;
            -o-transition: background .5s ease;
            display: inline-block;
            cursor: pointer;
            outline: none;
            text-align: center;
            background: <?php echo$this->Settings->theme_color;?>;
            position: relative;
            font-size: 14px;
            font-size: 0.875rem;
            font-weight: 600;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            line-height: 1;
            padding: 12px 30px;
        }

        button.backward {
            color: #777;
            background: #e8e8e8;
        }

        button[disabled] {
            display: none;
        }

        button.submit:before {
            font-family: 'ElegantIcons';
            position: absolute;
            top: 8px;
            right: 10px;
            font-size: 18px;
            font-size: 1.125rem;
        }

        .backward:hover,
        .forward:hover {
            background: <?php echo$this->Settings->theme_color;?>;
            color: #fff;
        }

        #top-wizard {
            padding-bottom: 20px;
        }

        #middle-wizard {
            min-height: 330px;
        }
        @media (max-width: 991px) {
            #middle-wizard {
                min-height: inherit;
            }
        }

        #bottom-wizard {
            border-top: 2px solid #ededed;
            padding-top: 20px;
            text-align: right;
            margin-top: 25px;
        }

        .ui-widget-content {
            background-color: transparent;
        }

        .ui-widget-content a {
            color: #222222;
        }

        .ui-widget-header {
            background: #6C3;
        }

        .ui-widget-header a {
            color: #222222;
        }

        .ui-progressbar {
            height: 5px;
            width: 100%;
        }

        .ui-progressbar .ui-progressbar-value {
            height: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }

        .summary ul {
            margin: 0;
            padding: 0;
        }
        .summary ul li {
            margin: 0;
            padding: 0;
            border-bottom: 1px solid #ededed;
            position: relative;
            padding-left: 45px;
            margin-bottom: 25px;
        }
        .summary ul li:last-child {
            margin-bottom: 0;
            border-bottom: none;
        }
        .summary ul li strong {
            display: block;
            line-height: 26px;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            -ms-border-radius: 50%;
            border-radius: 50%;
            width: 30px;
            height: 30px;
            position: absolute;
            left: 0;
            top: 0;
            text-align: center;
            border: 2px solid #ddd;
        }
        .summary ul li h5 {
            padding-top: 6px;
            font-size: 15px;
            font-size: 0.9375rem;
            font-weight: 500;
            color: #0686D8;
        }
        .summary ul li ul {
            margin: 20px 0 25px 0;
            padding: 0;
        }
        .summary ul li ul li {
            margin: 0;
            padding: 0;
            border-bottom: 0;
        }
        .summary label {
            font-weight: 500;
        }

        /*-------- 2.2 Success submit --------*/
        #success {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 300px;
            height: 190px;
            margin-top: -85px;
            margin-left: -150px;
            text-align: center;
        }
        #success h4 {
            font-weight: 400;
            margin: 20px 0 0 0;
            font-size: 18px;
            font-size: 1.125rem;
        }
        #success h4 span {
            display: block;
            margin-bottom: 0;
            font-weight: 500;
            font-size: 21px;
            font-size: 1.3125rem;
        }

        @-webkit-keyframes checkmark {
            0% {
                stroke-dashoffset: 50px;
            }

            100% {
                stroke-dashoffset: 0;
            }
        }

        @-ms-keyframes checkmark {
            0% {
                stroke-dashoffset: 50px;
            }

            100% {
                stroke-dashoffset: 0;
            }
        }

        @keyframes checkmark {
            0% {
                stroke-dashoffset: 50px;
            }

            100% {
                stroke-dashoffset: 0;
            }
        }

        @-webkit-keyframes checkmark-circle {
            0% {
                stroke-dashoffset: 240px;
            }

            100% {
                stroke-dashoffset: 480px;
            }
        }

        @-ms-keyframes checkmark-circle {
            0% {
                stroke-dashoffset: 240px;
            }

            100% {
                stroke-dashoffset: 480px;
            }
        }

        @keyframes checkmark-circle {
            0% {
                stroke-dashoffset: 240px;
            }

            100% {
                stroke-dashoffset: 480px;
            }
        }

        .inlinesvg .svg svg {
            display: inline;
        }

        .icon--order-success svg path {
            -webkit-animation: checkmark 0.25s ease-in-out 0.7s backwards;
            animation: checkmark 0.25s ease-in-out 0.7s backwards;
        }

        .icon--order-success svg circle {
            -webkit-animation: checkmark-circle 0.6s ease-in-out backwards;
            animation: checkmark-circle 0.6s ease-in-out backwards;
        }

        /*-------- 2.3 Inner pages --------*/
        header {
            position: relative;
            padding: 15px 0;
            background-color: #fff;
            border-bottom: 1px solid #d9e1e6;
        }
        header .cd-nav-trigger {
            top: -5px !important;
        }
        header #social {
            right: 80px;
            top: 0;
        }
        header #social ul li a {
            color: #333;
        }

        /* Footer */
        footer {
            border-top: 1px solid #ededed;
            padding: 30px 0;
        }
        footer p {
            margin: 0;
            padding: 0;
            float: right;
        }
        @media (max-width: 991px) {
            footer p {
                float: none;
            }
        }
        footer ul {
            float: left;
            margin: 0;
            padding: 0;
        }
        @media (max-width: 991px) {
            footer ul {
                float: none;
                margin-top: 10px;
            }
        }
        footer ul li {
            display: inline-block;
            margin-right: 15px;
        }
        footer ul li:after {
            content: "|";
            font-weight: 300;
            position: relative;
            left: 9px;
            color: #999;
        }
        footer ul li:last-child {
            margin-right: 0;
        }
        footer ul li:last-child:after {
            content: "";
        }
        footer ul li a {
            color: #555;
        }
        footer ul li a:hover {
            color: #121921;
        }

        .main_title {
            text-align: center;
        }
        .main_title h2 {
            margin: 0 0 10px 0;
            padding: 0;
            font-size: 42px;
            font-size: 2.625rem;
            color: #1b0088;
            text-transform: uppercase;
        }
        @media (max-width: 767px) {
            .main_title h2 {
                font-size: 32px;
                font-size: 2rem;
            }
        }
        .main_title h2 em {
            display: block;
            width: 40px;
            height: 4px;
            background-color: #ededed;
            margin: auto;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
            margin-bottom: 15px;
        }
        .main_title p {
            font-size: 18px;
            font-size: 1.125rem;
            padding: 0 10%;
            margin-bottom: 45px;
            color: #777;
        }
        @media (max-width: 767px) {
            .main_title p {
                font-size: 16px;
                font-size: 1rem;
            }
        }

        .owl-theme .owl-dots .owl-dot.active span,
        .owl-theme .owl-dots .owl-dot:hover span {
            background: #1b0088 !important;
        }

        main#general_page {
            background-color: #fff;
        }

        iframe#map_iframe {
            width: 100%;
            height: 450px;
            border: 0;
        }
        @media (max-width: 991px) {
            iframe#map_iframe {
                height: 400px;
            }
        }

        .box_style_2 {
            background-color: #f8f8f8;
            padding: 25px 30px 30px 30px;
            position: relative;
            margin-bottom: 25px;
        }
        .box_style_2 .form-control {
            background-color: white !important;
        }

        .box_style_2 hr {
            margin: 10px -30px 20px -30px;
            border: 0;
            border-top: 2px solid #fff;
        }

        ul.contacts_info {
            list-style: none;
            padding: 0;
            margin: 15px 0 0 0;
        }

        ul.contacts_info li {
            margin-bottom: 15px;
        }

        ul.contacts_info li:last-child {
            margin-bottom: 0;
        }

        .parallax_window_in {
            height: 420px;
            position: relative;
            display: table;
            width: 100%;
        }

        #sub_content_in {
            display: table-cell;
            padding: 45px 15% 0 15%;
            vertical-align: middle;
            text-align: center;
            background: rgba(0, 0, 0, 0.5);
        }
        @media (max-width: 767px) {
            #sub_content_in {
                padding: 45px 30px 0 30px;
            }
        }
        #sub_content_in h1 {
            color: #fff;
            font-weight: 600;
            text-transform: uppercase;
            font-size: 46px;
            font-size: 46px;
            font-size: 2.875rem;
            margin-bottom: 0;
        }
        @media (max-width: 767px) {
            #sub_content_in h1 {
                font-size: 36px;
                font-size: 2.25rem;
            }
        }
        #sub_content_in p {
            color: #fff;
            font-size: 24px;
            font-size: 1.5rem;
            font-weight: 300;
        }
        @media (max-width: 767px) {
            #sub_content_in p {
                font-size: 21px;
                font-size: 1.3125rem;
            }
        }

        .container_styled_1 {
            background: #ced4da;
        }

        .team-item-img {
            position: relative;
        }

        .team-item-img .team-item-detail {
            background: none repeat scroll 0 0 rgba(0, 0, 0, 0.8);
            text-align: center;
            color: #fff;
            display: -webkit-flex;
            display: flex;
            height: 100%;
            width: 100%;
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            visibility: hidden;
            overflow: hidden;
            transition: all 0.5s ease-in-out 0s;
            -moz-transition: all 0.5s ease-in-out 0s;
            -webkit-transition: all 0.5s ease-in-out 0s;
            -o-transition: all 0.5s ease-in-out 0s;
        }

        .team-item:hover .team-item-detail {
            opacity: 1;
            visibility: visible;
        }

        .team-item-img .team-item-detail .team-item-detail-inner {
            margin: auto;
            padding: 25px;
        }

        .team-item-detail-inner h4 {
            color: #fff;
            text-transform: uppercase;
            font-weight: 500;
        }

        .team-item-detail-inner .social {
            margin: 0 0px 25px 0px;
            padding: 0px;
        }

        .team-item-detail-inner .social li {
            list-style: none;
            display: inline-block;
            margin: 0px 5px;
        }
        .team-item-detail-inner .social li a {
            color: #fff;
        }
        .team-item-detail-inner .social li a:hover {
            color: #e58800;
        }

        .team-item-info {
            padding-top: 15px;
            text-align: center;
            text-transform: uppercase;
        }
        .team-item-info h4 {
            margin-bottom: 0px;
        }

        /*============================================================================================*/
        /* 3.  COMMON */
        /*============================================================================================*/
        /*-------- 3.1 Misc --------*/
        .modal-content {
            border: none;
            -webkit-box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.3);
            -moz-box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.3);
            box-shadow: 0px 0px 20px 0px rgba(0, 0, 0, 0.3);
        }

        .form-group {
            position: relative;
        }
        .form-group.terms {
            padding: 12px 0 0 0;
        }
        .form-group i {
            font-size: 18px;
            font-size: 1.125rem;
            position: absolute;
            right: 5px;
            top: 11px;
            color: #ccc;
            width: 25px;
            height: 25px;
            display: block;
            font-weight: 400 !important;
        }

        span.error {
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            font-size: 12px;
            position: absolute;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            top: -20px;
            right: -15px;
            z-index: 2;
            height: 25px;
            line-height: 1;
            background-color: #e34f4f;
            color: #fff;
            font-weight: normal;
            display: inline-block;
            padding: 6px 8px;
        }
        span.error:after {
            content: '';
            position: absolute;
            border-style: solid;
            border-width: 0 6px 6px 0;
            border-color: transparent #e34f4f;
            display: block;
            width: 0;
            z-index: 1;
            bottom: -6px;
            left: 20%;
        }

        .container_radio.version_2 .error, .container_check.version_2 .error {
            left: -15px;
            top: -30px;
            right: inherit;
        }

        .radio_input .error {
            left: -15px;
            top: -30px;
            right: inherit;
        }

        .styled-select span.error {
            top: -20px;
        }

        .terms span.error {
            top: -30px;
            left: -15px;
            right: inherit;
        }

        .form-control {
            border: 1px solid #d2d8dd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            font-size: 14px;
            font-size: 0.875rem;
            height: calc(2.55rem + 2px);
        }
        .form-control:focus {
            box-shadow: none;
            border-color: #1b0088;
        }

        .rating_wp {
            background-color: #f9f9f9;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
            padding: 14px 10px 10px 15px;
            margin-bottom: 5px;
        }

        .rating {
            display: inline-block;
            font-size: 0;
            float: right;
            position: relative;
        }
        @media (max-width: 767px) {
            .rating {
                float: left;
                display: block;
            }
        }

        .rating span.error {
            top: -30px;
        }

        .rating_type {
            float: left;
            font-size: 16px;
            font-size: 1rem;
            font-weight: 500 !important;
            font-weight: normal;
            color: #1b0088;
        }
        @media (max-width: 767px) {
            .rating_type {
                float: none;
                display: block;
                margin-bottom: 5px;
            }
        }

        .rating-input {
            float: right;
            padding: 0;
            margin: 0 0 0 0;
            opacity: 0;
            width: 4px;
        }

        .rating:hover .rating-star:hover,
        .rating:hover .rating-star:hover ~ .rating-star,
        .rating-input:checked ~ .rating-star {
            background-position: 0 0;
        }

        .rating-star,
        .rating:hover .rating-star {
            cursor: pointer;
            float: right;
            display: block;
            width: 25px;
            height: 25px;
            margin-right: 0;
            background: url(../img/stars.svg) 0 -27px;
        }

        .review_message {
            height: 250px !important;
        }
        @media (max-width: 767px) {
            .review_message {
                height: 200px !important;
            }
        }

        /* Checkbox style */
        .container_check {
            display: block;
            position: relative;
            font-size: 14px;
            font-size: 0.875rem;
            padding-left: 30px;
            line-height: 1.3;
            margin-bottom: 10px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .container_check input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
        }
        .container_check input:checked ~ .checkmark {
            background-color: #1b0088;
            border: 1px solid transparent;
        }
        .container_check .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            border: 1px solid #d2d8dd;
            background-color: #fff;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        .container_check .checkmark:after {
            content: "";
            position: absolute;
            display: none;
            left: 7px;
            top: 3px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }
        .container_check.version_2 {
            padding: 6px 0 0 45px;
            min-height: 30px;
        }
        .container_check.version_2 .checkmark {
            height: 30px;
            width: 30px;
        }
        .container_check.version_2 .checkmark:after {
            left: 12px;
            top: 8px;
            width: 5px;
            height: 10px;
        }

        .container_check input:checked ~ .checkmark:after {
            display: block;
        }

        /* Radio buttons */
        .container_radio {
            display: block;
            position: relative;
            font-size: 14px;
            font-size: 0.875rem;
            padding-left: 30px;
            line-height: 1.3;
            margin-bottom: 10px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        .container_radio input {
            position: absolute;
            opacity: 0;
        }
        .container_radio input:checked ~ .checkmark:after {
            opacity: 1;
        }
        .container_radio .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 20px;
            width: 20px;
            background-color: #fff;
            border: 1px solid #cccccc;
            border-radius: 50%;
        }
        .container_radio .checkmark:after {
            display: block;
            content: "";
            position: absolute;
            opacity: 0;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            top: 3px;
            left: 3px;
            width: 12px;
            height: 12px;
            border-radius: 50%;
            background: #1b0088;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
        }
        .container_radio.version_2 {
            padding: 6px 0 0 45px;
            min-height: 30px;
        }
        .container_radio.version_2 input:checked ~ .checkmark:before {
            opacity: 1;
        }
        .container_radio.version_2 .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 30px;
            width: 30px;
            border: 1px solid #cccccc;
            border-radius: 50%;
        }
        .container_radio.version_2 .checkmark:after {
            width: 30px;
            height: 30px;
            top: -1px;
            left: -1px;
        }
        .container_radio.version_2 .checkmark:before {
            display: block;
            content: "";
            position: absolute;
            opacity: 0;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            left: 12px;
            top: 8px;
            width: 5px;
            height: 10px;
            border: solid white;
            z-index: 999;
            border-width: 0 2px 2px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        /* Show/hide password */
        .my-toggle {
            background: transparent;
            border: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            color: #888;
            cursor: pointer;
            font-size: 10px;
            font-size: 10px;
            font-size: 0.625rem;
            font-weight: bold;
            margin-right: 5px;
            height: 30px;
            line-height: 30px;
            padding: 0 10px;
            text-transform: uppercase;
            -moz-appearance: none;
            -webkit-appearance: none;
            background-color: #fff;
        }
        .my-toggle:hover, .my-toggle:focus {
            background-color: #eee;
            color: #555;
            outline: transparent;
        }

        .hideShowPassword-wrapper {
            width: 100% !important;
        }

        /*Password strength */
        #pass-info {
            width: 100%;
            margin-bottom: 15px;
            color: #555;
            text-align: center;
            font-size: 12px;
            font-size: 0.75rem;
            padding: 5px 3px 3px 3px;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
        }
        #pass-info.weakpass {
            border: 1px solid #FF9191;
            background: #FFC7C7;
            color: #94546E;
        }
        #pass-info.stillweakpass {
            border: 1px solid #FBB;
            background: #FDD;
            color: #945870;
        }
        #pass-info.goodpass {
            border: 1px solid #C4EEC8;
            background: #E4FFE4;
            color: #51926E;
        }
        #pass-info.strongpass {
            border: 1px solid #6ED66E;
            background: #79F079;
            color: #348F34;
        }
        #pass-info.vrystrongpass {
            border: 1px solid #379137;
            background: #48B448;
            color: #CDFFCD;
        }

        .radio_input .container_radio {
            display: inline-block;
            margin: 12px 0 0 12px;
        }

        /* Fileupload */
        .fileupload {
            position: relative;
            width: 100%;
            margin-top: 5px;
        }

        input[type=file] {
            border: 1px solid #d2d8dd;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            padding: 5px;
            height: auto;
            width: 100%;
            color: #999;
        }
        input[type=file]:focus {
            box-shadow: none;
            outline: none;
        }

        input[type=file]::-webkit-file-upload-button, input[type=file].invalid::-webkit-file-upload-button, input[type=file].valid::-webkit-file-upload-button {
            color: #fff;
            font-size: 13px;
            border: 0;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            padding: 8px 10px 8px 38px;
            background: #1b0088 url(../img/upload_icon.svg) 8px center no-repeat;
            outline: none;
        }
        input[type=file]::-webkit-file-upload-button:focus, input[type=file].invalid::-webkit-file-upload-button:focus, input[type=file].valid::-webkit-file-upload-button:focus {
            box-shadow: none;
            outline: none;
        }

        /* Jquery select */
        .nice-select {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            background-color: #fff;
            border-radius: 3px;
            border: 1px solid #d2d8dd;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            display: block;
            float: left;
            font-family: inherit;
            font-size: 14px;
            font-weight: normal;
            height: 42px;
            line-height: 40px;
            outline: none;
            padding-left: 15px;
            padding-right: 27px;
            position: relative;
            text-align: left !important;
            transition: all 0.2s ease-in-out;
            user-select: none;
            white-space: nowrap;
            width: auto;
            color: #6c757d;
        }
        .nice-select:active, .nice-select.open, .nice-select:focus {
            border-color: #1b0088;
        }
        .nice-select:after {
            border-bottom: 2px solid #cccccc;
            border-right: 2px solid #cccccc;
            content: '';
            display: block;
            height: 8px;
            margin-top: -5px;
            pointer-events: none;
            position: absolute;
            right: 20px;
            top: 50%;
            transform-origin: 66% 66%;
            transform: rotate(45deg);
            transition: all 0.15s ease-in-out;
            width: 8px;
        }
        .nice-select.open:after {
            transform: rotate(-135deg);
        }
        .nice-select.open .list {
            opacity: 1;
            pointer-events: auto;
            transform: scale(1) translateY(0);
        }
        .nice-select.disabled {
            border-color: #9e9e9e;
            color: #999999;
            pointer-events: none;
        }
        .nice-select.disabled:after {
            border-color: white;
        }
        .nice-select.wide {
            width: 100%;
        }
        .nice-select.wide .list {
            left: -1px !important;
            right: -1px !important;
        }
        .nice-select.right {
            float: right;
        }
        .nice-select.right .list {
            left: auto;
            right: 0;
        }
        .nice-select.small {
            font-size: 12px;
            height: 42px;
            line-height: 40px;
        }
        .nice-select.small:after {
            height: 4px;
            width: 4px;
        }
        .nice-select.small .option {
            line-height: 40px;
            min-height: 40px;
        }
        .nice-select .list {
            background-color: #fff;
            border-radius: 3px;
            box-shadow: 0 0 0 1px rgba(68, 68, 68, 0.11);
            box-sizing: border-box;
            margin-top: 4px;
            opacity: 0;
            overflow: hidden;
            padding: 0;
            pointer-events: none;
            position: absolute;
            top: 100%;
            left: 0;
            transform-origin: 50% 0;
            transform: scale(0.75) translateY(-25px);
            transition: all 0.2s cubic-bezier(0.5, 0, 0, 1.25), opacity 0.15s ease-out;
            z-index: 9999;
            height: 23vh;
            overflow: auto;
        }
        .nice-select .list:hover .option:not(:hover) {
            background-color: transparent !important;
        }
        .nice-select .list::-webkit-scrollbar {
            width: 14px;
            height: 18px;
        }
        .nice-select .list::-webkit-scrollbar-thumb {
            height: 6px;
            border: 4px solid rgba(0, 0, 0, 0);
            background-clip: padding-box;
            -webkit-border-radius: 7px;
            background-color: rgba(0, 0, 0, 0.15);
            -webkit-box-shadow: inset -1px -1px 0px rgba(0, 0, 0, 0.05), inset 1px 1px 0px rgba(0, 0, 0, 0.05);
        }
        .nice-select .list::-webkit-scrollbar-button {
            width: 0;
            height: 0;
            display: none;
        }
        .nice-select .list::-webkit-scrollbar-corner {
            background-color: transparent;
        }
        .nice-select .option {
            cursor: pointer;
            font-weight: 400;
            line-height: 38px;
            list-style: none;
            min-height: 38px;
            outline: none;
            padding-left: 15px;
            padding-right: 26px;
            text-align: left;
            transition: all 0.2s;
        }
        .nice-select .option:hover, .nice-select .option.focus, .nice-select .option.selected.focus {
            background-color: #f6f6f6;
        }
        .nice-select .option.selected {
            font-weight: 500;
        }
        .nice-select .option.disabled {
            background-color: transparent;
            color: #999999;
            cursor: default;
        }

        .no-csspointerevents .nice-select .list {
            display: none;
        }
        .no-csspointerevents .nice-select.open .list {
            display: block;
        }

        /* Budget slider */
        .budget_slider {
            background-color: #f8f8f8;
            margin-bottom: 20px;
            padding: 1px 30px 15px 30px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
        }
        .budget_slider span {
            display: block;
            font-weight: 600;
            color: #212529;
            font-size: 24px;
            font-size: 1.5rem;
            margin-top: 25px;
            text-align: right;
        }
        .budget_slider span:before {
            content: 'Total R$ ';
        }

        .rangeslider__handle {
            border: 2px solid #1b0088 !important;
            box-shadow: 0px 1px 3px 0px rgba(0, 0, 0, 0.15);
        }

        .rangeslider__fill {
            background: #1b0088 !important;
        }

        /*-------- 3.2 Spacing --------*/
        .add_bottom_10 {
            margin-bottom: 10px;
        }

        .add_bottom_15 {
            margin-bottom: 15px;
        }

        .add_bottom_30 {
            margin-bottom: 30px;
        }

        .add_bottom_45 {
            margin-bottom: 45px;
        }

        .add_bottom_60 {
            margin-bottom: 60px;
        }

        .add_bottom_75 {
            margin-bottom: 75px;
        }

        .add_top_10 {
            margin-top: 10px;
        }

        .add_top_15 {
            margin-top: 15px;
        }

        .add_top_20 {
            margin-top: 20px;
        }

        .add_top_30 {
            margin-top: 30px;
        }

        .add_top_60 {
            margin-top: 60px;
        }

        .more_padding_left {
            padding-left: 40px;
        }

        .nomargin_top {
            margin-top: 0;
        }

        .nopadding {
            margin: 0 !important;
            padding: 0 !important;
        }

        .nomargin {
            margin: 0 !important;
        }

        .margin_30 {
            padding-top: 10px;
            padding-bottom: 30px;
        }

        .margin_60 {
            padding-top: 60px;
            padding-bottom: 60px;
        }

        .margin_60_35 {
            padding-top: 60px;
            padding-bottom: 35px;
        }


        body {
            background: #fff;
        }
        .content-left {
            background-color: #fff;
            padding: 0;
            padding: 15px 50px 15px 50px;
        }

        .content-left-wrapper {
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
            height: 100%;
            min-height: 100%;
            padding: 15px 90px 35px 90px;
            background-color: #fff;
            color: #222;
            text-align: center;
            position: relative;
            background: transparent;
            background: #fff;
        }

        .content-left-wrapper h2 {
            color: #222;
            font-size: 32px;
            font-size: 2rem;
            margin: 20px 0 15px 0;
            font-weight: 600;
        }

        .barra-progresso{width: 320px;height: 24px;background-color: #bbb;border-radius: 5px;padding: 3px;margin: 10px auto;display: none;}
        .porcentagem{width: 100%;height: 17px;border-radius: 5px;background-color: #e99419;}

        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
            width: 100%;
        }

        a.btn_1.rounded, .btn_1.rounded {
            -webkit-border-radius: 3px !important;;
            -moz-border-radius: 3px !important;
            border-radius: 3px !important;
            -webkit-box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 20%);
            -moz-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.2);
            box-shadow: 0px 0px 30px 0px rgb(0 0 0 / 20%);
        }

        .content-left-wrapper{
            padding: 15px 20px 17px 20px;
        }

        @media (max-width: 767px)
            .content-left-wrapper .btn_1.mobile_btn {
                margin: 5px 0 1px 0;
                display: inline-block;
            }

            .content-left-wrapper .btn_1 {
                margin: 0px 0 10px 0;
            }


            .cart {

            }

            .cart_destaque {
                border: 1px solid #dcdcdc;
                /*padding: 15px 15px 15px 15px;*/
                margin-bottom: 0;
                background: #ffff;
                padding: 1.5em 2em;
                margin-bottom: 5px;
            }

            .cart_destaque_m {
                border: 1px solid #dcdcdc;
                /*padding: 15px 15px 15px 15px;*/
                background: #ffff;
                padding: 1.5em 2em;
                margin-bottom: 0px;
            }

            .cart_destaque_m label {
                font-weight: 500;
                font-size: 18px;
            }

            .cart_destaque_hospedagem {
                -webkit-box-pack: justify;
                justify-content: space-between;
                padding: 12px;
                font-size: 16px;
                font-weight: 600;
                line-height: 24px;
                text-transform: uppercase;
                box-shadow: 1px 1px 8px 10px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            .cart_destaque label {
                font-weight: 500;
                font-size: 18px;
            }

            .cart_destaque label div {
                font-weight: 400;
                font-size: 15px;
                color: #28a745;
                margin-top: 10px;
            }

            .cart_cartao_transpartente {
                border: 1px solid #dcdcdc;
                padding: 15px 15px 15px 15px;
                margin-bottom: 0;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                background: #ffff;
                border-radius: 5px;
            }

            .cart_cartao_transpartente label {
                font-weight: 500;
            }

        .cart_cartao_round {
            border: 1px solid #dcdcdc;
            padding: 15px 15px 15px 15px;
            margin-bottom: 0;
            background: #ffff;
        }

        .cart_cartao_round label {
            font-weight: 500;
        }

            .container_radio.version_2 {
                padding: 0px 0 0 45px;
                min-height: 1px !important;
            }

            .container_radio input:checked ~ .checkmark:after {
                opacity: 1;
            }

            .container_radio.version_2 .checkmark:after {
                width: 30px;
                height: 30px;
                top: -1px;
                left: -1px;
            }

            .container_radio .checkmark:after {
                display: block;
                content: "";
                position: absolute;
                opacity: 0;
                -moz-transition: all 0.3s ease-in-out;
                -o-transition: all 0.3s ease-in-out;
                -webkit-transition: all 0.3s ease-in-out;
                -ms-transition: all 0.3s ease-in-out;
                transition: all 0.3s ease-in-out;
                top: 3px;
                left: 3px;
                width: 12px;
                height: 12px;
                border-radius: 50%;
                background: #28a745;
            }

            .budget_slider {
                background-color: #e9ecef;
                margin-bottom: 20px;
                padding: 1px 20px 14px 0px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                margin-top: 15px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
                border-left: solid 5px #28a74557 !important;
            }

            .price_adicionais {
                color: #6c757d;
                font-size: 14px;
                line-height: 10px;
                padding: 5px;
                font-weight: 400;
            }

            .budget_slider_adicionais_add {
                background: #FFFFFF;
                margin-bottom: 20px;
                padding: 10px 10px 5px 10px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            .budget_slider_adicionais {
                background: #FFFFFF;
                margin-bottom: 20px;
                padding: 10px 10px 5px 10px;
                -webkit-border-radius: 5px;
                -moz-border-radius: 5px;
                border-radius: 5px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            .mais_informacoes_adicional {
                margin-bottom: 20px;
                margin-left: 15px;
            }

            .btn_selecionar_opcional {
                background: #e58801;
                color: #ffff;
                width: 100%;
                border: none;
                text-decoration: none;
                transition: background .5s ease;
                -moz-transition: background .5s ease;
                -webkit-transition: background .5s ease;
                -o-transition: background .5s ease;
                display: inline-block;
                cursor: pointer;
                outline: none;
                text-align: center;
                /* background: #434bdf; */
                position: relative;
                font-size: 0.875rem;
                font-weight: 600;
                -webkit-border-radius: 3px;
                -moz-border-radius: 3px;
                border-radius: 3px;
                line-height: 1;
                padding: 12px 30px;
                margin-bottom: 15px;
            }

            .profile_img {
                padding: 17px;
                width: 200px;
            }

            .error {
                border: 1px solid red;
            }

            .combo {
                -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
                background-color: #fff;
                border-radius: 3px;
                border: 1px solid #d2d8dd;
                box-sizing: border-box;
                clear: both;
                cursor: pointer;
                display: block;
                float: left;
                font-family: inherit;
                font-size: 14px;
                font-weight: normal;
                height: 43px;
                line-height: 40px;
                outline: none;
                padding-left: 0px;
                padding-right: 0px;
                position: relative;
                text-align: left;
                transition: all 0.2s ease-in-out;
                user-select: none;
                white-space: nowrap;
                width: 100%;
                color: #6c757d;
            }

            .combo2
            {
                -webkit-appearance: none;
                -moz-appearance: none;
                text-indent: 1px;
                text-overflow: '';
            }

            .label_valores {
                font-size: 16px;
                font-weight: 400;
                line-height: 1;
                margin: 0 0 5px;
                color: black;
            }

            .cc_fieldset {
                font-size: 1.1em;
                font-weight: 500;
                text-align: left;
                width: auto;
                color: #428BCA;
                border: 1px solid #DBDEE0;
                margin: 0px;
                cursor: pointer;
                background: #FFFFFF;
                text-transform: uppercase;
            }

            .imgfilters {
                float: right;
                width: 18px;
            }

            .orcamento {
                text-align: center;
                line-height: 20px;
                font-size: 18px;
                border: 1px solid #dcdcdc;
                padding: 1.5em 2em;
                border-radius: 15px;
            }

            /* Tablet Portrait (devices and browsers)
            ====================================================================== */
            @media only screen and (min-width: 768px) and (max-width: 991px) {
                a#main-menu-toggle {
                    margin-left: 8.334%;
                }
                a.navbar-brand {

                    padding: 8px 0px !important;
                    position: absolute;
                    left: 15px;
                }
                a.navbar-brand span {
                    font-size: 18px;
                }
                .navbar-collapse {
                    max-height: 100%;
                }
                .container {
                    width: 100% !important;
                }
                .container #content {
                    padding: 15px;
                }
                .container .breadcrumb {
                    margin: -15px -15px 15px -15px;
                    padding: 10px;
                }
                .btn-navbar {
                    display: none !important;
                }
                .padding05 {
                    padding: 0;
                }
                .lt td.sidebar-con {
                    width: 40px;
                }
                .content-scroll { width: 300px; }
            }

            #top-wizard {
                padding-bottom: 0px;
            }

            .botoes_avanca_confirma {
                display: block;
                position: fixed;

                width: 100%;
                bottom: 0vh;
                left: 0;
                background: white;
                padding: 5px 15px;
                box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            }

            /* All Mobile Sizes (devices and browser)
            ====================================================================== */
            @media only screen and (max-width: 767px) {
                .apresentacao_viagem {
                    display: none;
                }

                .apresentacao_viagem_visivel {
                    display: block;
                }
            }

            .content-right {
                min-height: 100%;
                display: flex;
                justify-content: center;
                padding: 15px 100px 15px 15px;
            }

            @media (max-width: 991px) {
                .content-right {
                    height: auto;
                    padding: 0px 15px 100px 15px;
                }
            }


            #bottom-wizard {
                border-top: 1px solid #1b0088;
                padding: 0px 15px 10px 15px;
                text-align: right;
                margin-top: 25px;
            }

        .desabilitar_tipo_hospedagem {
            text-decoration: line-through;
            pointer-events: none;
            user-select: none;
            opacity: 0.4;
        }

        .label_clientes {
            color: #444;
            font-size: 12px;
            font-weight: 500;
            text-transform: uppercase;
            letter-spacing: 1pt;
        }

        .fa-2x {
            font-size: 20px;
        }

        .assento_selecionado {
            width: 40px;
            height: 40px;
            background: rgb(27, 0, 136);
            color: white;
            border-radius: 6px;
            display: flex;
            align-self: flex-start;
            -webkit-box-align: center;
            align-items: center;
            -webkit-box-pack: center;
            justify-content: center;
            font-weight: 600;
            white-space: nowrap;
            flex: 0 0 40px;
        }

        .assento_selecionado_name {
            display: flex;
            -webkit-box-flex: 1;
            flex-grow: 1;
            flex-flow: column wrap;
            justify-content: space-around;
            width: calc(100% - 40px);
        }

        .assento_selecionado_name_inner  {
            color: rgb(16, 0, 79);
            margin-left: 0.5rem;
            margin-top: 0.5rem;
            font-size: 0.9rem;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            width: 100%;
            font-weight: bold;
        }

        .combo_assento {
            background: #1b0088;
            color: #ffffff;
            border: 0px;
            outline: none;
        }

        .cc_assentos {
            border-radius: 8px;
        }

        .cc_assento_selecionado {
            border-left: 4px solid rgb(27, 0, 136);
            box-shadow: rgba(16, 0, 79, 0.12) 0px 2px 8px 0px;
        }

        .cc_assentos_opaco {
            opacity: 0.2;
            /*box-shadow: rgba(16, 0, 79, 0.16) 0px 3px 8px 0px;*/
        }

        .fild {
            font-size: 1em;
            bottom: 200px;
            right: -5px;
            background-color: white;
            padding: 0px 10px 10px 10px;
            border: 1px solid #ccc;
            max-height: 75%;
            overflow-y: auto;
            z-index: 999;
            text-align: center;
        }

        .fild_legend {
            font-weight: 700;
            color: #10004f;
            margin-bottom: 15px;
            font-size: 20px;
        }

        /* width */
        ::-webkit-scrollbar {
            width: 8px;
        }

        /* Track */
        ::-webkit-scrollbar-track {
            box-shadow: inset 0 0 5px grey;
            border-radius: 5px;
        }

        /* Handle */
        ::-webkit-scrollbar-thumb {
            background: #6c757d;
            border-radius: 5px;
        }

        /* Handle on hover */
        ::-webkit-scrollbar-thumb:hover {
            background: #6c757d;
        }


        .div_parcelas {
            font-size: 1rem;
            line-height: 32px;
            font-weight: 500;
            margin-bottom: 10px;
            color: #222222;
        }

        .div_parcelas  select {
            font-size: 20px;
            color: #222222;
        }

        .combo_parcela {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            background-color: #fff;
            border-radius: 3px;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            display: block;
            float: left;
            font-family: inherit;
            font-size: 1rem;
            height: 43px;
            line-height: 40px;
            outline: none;
            padding-left: 0px;
            padding-right: 0px;
            position: relative;
            transition: all 0.2s ease-in-out;
            user-select: none;
            white-space: nowrap;
            width: 100%;
            color: #222222;
            text-align: center;
            font-weight: 600;
        }
    </style>

    <?php if ($this->Settings->pixelFacebook) {
        $category  = $this->site->getCategoryByID($produto->category_id);?>
        <!-- Meta Pixel Code | InitiateCheckout -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'InitiateCheckout', {
                content_name: '<?=str_replace("'", "", $produto->name);?>',
                content_category: '<?=$category->name;?>',
                content_ids: [<?=$produto->id;?>],
                contents: [{'id': '<?=$produto->id;?>', 'quantity': 1}],
                content_type: 'product',
                value: <?=$produto->precoExibicaoSite;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=InitiateCheckout&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div>

<header style="background: <?php echo$this->Settings->theme_color;?>;" class="page_header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-6">
                <?php if ($configuracaoGeral->url_site && !$Settings->own_domain) {?>
                    <a href="<?php echo $Settings->url_site; ?>" class="btn_1" style="padding: 10px 0px;background: #6c757d;font-size: 15px;text-align: left;"><span class="fa fa-home"></span> INÍCIO</a>
                <?php } else { ?>
                     <?php if ($Settings->own_domain){?>
                        <a href="<?php echo base_url().$vendedor->id;?>" class="btn_1" style="padding: 10px 0px;background: <?php echo$this->Settings->theme_color;?>;font-size: 15px;text-align: left;"><span class="fa fa-home"></span> INÍCIO</a>
                     <?php } else { ?>
                        <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id;?>" class="btn_1" style="padding: 10px 0px;background: <?php echo$this->Settings->theme_color;?>;font-size: 15px;text-align: left;"><span class="fa fa-home"></span> INÍCIO</a>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="col-6">
                <a href="#" class="btn_1" data-toggle="modal" data-target="#more-information" style="padding: 10px 0px;background: <?php echo$this->Settings->theme_color;?>;font-size: 15px;text-align: right;">VER DETALHES <span class="fa fa-bars"></span></a>
            </div>
        </div>
    </div>
    <!-- /container -->
</header>

<?php $attrib = array('id' => 'wrapped' ,'name' => 'fomulario_reservas');echo form_open_multipart("appcompra/add", $attrib);  ?>
<div class="container-fluid full-height">
    <div class="row row-height">

        <div class="col-lg-1 content-left apresentacao_viagem"></div>
        <div class="col-lg-6 content-left apresentacao_viagem">
            <div class="content-left-wrapper">
                <div>
                    <div id="social">
                        <ul>
                            <?php if ($configuracaoGeral->facebook){?>
                                <li><a href="<?php echo $configuracaoGeral->facebook;?>" target="_blank"><i class="icon-facebook"></i></a></li>
                            <?php } ?>
                            <?php if ($configuracaoGeral->twitter){?>
                                <li><a href="<?php echo $configuracaoGeral->twitter;?>" target="_blank"><i class="icon-twitter"></i></a></li>
                            <?php } ?>
                            <?php if ($configuracaoGeral->youtube){?>
                                <li><a href="<?php echo $configuracaoGeral->youtube;?>" target="_blank"><i class="icon-youtube"></i></a></li>
                            <?php } ?>
                            <?php if ($configuracaoGeral->instagram){?>
                                <li><a href="<?php echo $configuracaoGeral->instagram;?>" target="_blank"><i class="icon-instagram"></i></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- /social -->
                <div>
                    <figure>
                        <img src="<?php echo base_url().'/assets/uploads/'.$produto->image; ?>" alt="<?php echo $produto->name;?>" class="img-fluid"/>
                    </figure>
                    <h2><?php echo $produto->name?></h2>
                    <hr style="margin: 10px 0 10px 0;">
                    <div class="content-left-wrapper" style="width: 100%;">
                        <div style="width: inherit;">
                            <style>
                                details {
                                    cursor: pointer;
                                    position: relative;
                                    font-size: 100%;
                                    border: 1px solid #dee2e6;
                                    background: #fff;
                                    font-weight: 400;
                                    color: #222
                                }

                                details summary{
                                    padding: 1em 1em 1em 1em;
                                    border-left: solid 5px <?=$this->Settings->theme_color;?>;
                                }

                                details p {
                                    padding: 0px 15px 0px 25px;
                                    margin-top: 15px;;
                                }

                                .whatsapp {
                                    position: fixed;
                                    right: 10px;
                                    bottom: 20px;
                                    z-index: 11;
                                }

                                .icon_whatsapp {
                                    flex-shrink: 0;
                                    width: 50px;
                                    height: 50px;
                                    order: 2;
                                    padding: 5px;
                                    box-sizing: border-box;
                                    border-radius: 50%;
                                    cursor: pointer;
                                    overflow: hidden;
                                    box-shadow: rgb(0 0 0 / 40%) 2px 2px 6px;
                                    transition: all 0.5s ease 0s;
                                    position: relative;
                                    z-index: 200;
                                    display: block;
                                    border: 0px;
                                    background-color: rgb(77, 194, 71) !important;
                                }
                            </style>

                            <?php
                                $product_details = $this->ProductDetailsRepository_model->getAll($produto->id);
                                $servicos_incluso =  $this->site->getAllServicosInclusoByProduct($produto->id);
                            ?>
                            <div style="text-align: left;">

                                <?php if ($produto->product_details){?>
                                    <?php echo $produto->product_details;?>
                                <?php } ?>

                                <?php if (!empty($servicos_incluso)){ ?>
                                    <div style="margin-top: 25px;">
                                        <table style="width: 100%;">
                                            <?php foreach ($servicos_incluso as $servico_incluso) {
                                                $icon_html_alterado = str_replace('width="18"', 'width="48"', $servico_incluso->icon);
                                                $icon_html_alterado = str_replace('height="18"', 'height="48"', $icon_html_alterado);

                                                ?>
                                                <tr>
                                                    <td style="width: 5%;vertical-align: middle;padding: 10px;fill: <?php echo$this->Settings->theme_color;?>;"><?=$icon_html_alterado;?></td>
                                                    <td style="text-align: left;vertical-align: middle;padding: 10px;"><b><?=$servico_incluso->name;?> </b><br/><?=$servico_incluso->note_incluso;?></td>
                                                </tr>
                                            <?php } ?>
                                        </table>
                                    </div>
                                <?php } ?>

                                <?php if ($produto->itinerario){?>
                                    <details style="margin-top: 25px;">
                                        <summary>ROTEIRO</summary>
                                        <?php echo $produto->itinerario;?>
                                    </details>
                                <?php } ?>
                                <?php if ($produto->oqueInclui){?>
                                    <details>
                                        <summary>O QUE INCLUI</summary>
                                        <?php echo $produto->oqueInclui;?>
                                    </details>
                                <?php } ?>

                                <?php if ($produto->valores_condicoes){?>
                                    <details>
                                        <summary>INVESTIMENTO</summary>
                                        <?php echo $produto->valores_condicoes;?>
                                    </details>
                                <?php } ?>
                                <?php if ($produto->url_video) {
                                    if (strpos($produto->url_video, 'youtu.be') !== false) {
                                        $urlParts = explode('/', $produto->url_video);
                                        $v = end($urlParts);
                                        if (strpos($v, '?') !== false) {
                                            $v = strstr($v, '?', true);
                                        }
                                    } elseif (strpos($produto->url_video, 'youtube.com/watch') !== false) {
                                        $queryString = parse_url($produto->url_video, PHP_URL_QUERY);
                                        parse_str($queryString, $params);
                                        $v = $params['v'] ?? null;
                                    } elseif (strpos($produto->url_video, 'youtube.com/shorts') !== false) {
                                        $urlParts = explode('/', $produto->url_video);
                                        $v = end($urlParts);
                                        if (strpos($v, '?') !== false) {
                                            $v = strstr($v, '?', true);
                                        }
                                    }
                                    ?>
                                    <details>
                                        <summary>VÍDEO</summary>
                                        <iframe width="560" height="415" src="https://www.youtube.com/embed/<?php echo $v;?>" title="YouTube video player" frameborder="0"></iframe>
                                    </details>
                                <?php } ?>
                                <?php if ($produto->endereco) {?>
                                    <details>
                                        <summary>MAPA</summary>
                                        <div class="col-lg-12 address">
                                            <h2>Local</h2>
                                            <h5><?=$produto->complemento?></h5>
                                            <p><?=$produto->endereco?> <?=$produto->numero?>, <?=$produto->bairro?></p>
                                            <p><?=$produto->cidade?>/<?=$produto->estado?> | CEP: <?=$produto->cep?></p>
                                            <div class="mt-2">
                                                <div id="mapa" style="height: 450px;"> </div>
                                            </div>
                                        </div>
                                    </details>

                                    <!--
                                    <script src="https://maps.googleapis.com/maps/api/js?key=XXX&callback=initMap"async defer></script>
                                    !-->

                                    <script type="application/javascript">
                                        var map = null;
                                        var geocoder = null;

                                        function initMap() {
                                            var map = new google.maps.Map(document.getElementById('mapa'), {
                                                zoom: 10,
                                            });

                                            var geocoder = new google.maps.Geocoder();

                                            geocodeAddressAberto(geocoder, map);
                                        }

                                        function geocodeAddressAberto(geocoder, map) {

                                            var infowindow = new google.maps.InfoWindow({
                                                content: '<?=$produto->product_details?>'
                                            });

                                            geocoder.geocode({'address': '<?=$produto->endereco?> <?=$produto->numero?>, <?=$produto->bairro?> - <?=$produto->cidade?> <?=$produto->estado?> <?=$produto->cep?>' }, function(results, status) {
                                                if (status === google.maps.GeocoderStatus.OK) {
                                                    map.setCenter(results[0].geometry.location);
                                                    var marker = new google.maps.Marker({
                                                        title: 'TESTE',
                                                        map: map,
                                                        animation: google.maps.Animation.DROP,
                                                        position: results[0].geometry.location,
                                                    });

                                                    marker.addListener('click', function() {
                                                        infowindow.open(map, marker);
                                                    });
                                                }
                                            });
                                        }
                                    </script>
                                <?php } ?>
                                <?php if ($produto->details){?>
                                    <details>
                                        <summary>INFORMAÇÕES IMPORTANTES</summary>
                                        <?php echo $produto->details;?>
                                    </details>
                                <?php } ?>

                                <?php if (!empty($product_details)) { ?>
                                    <?php foreach ($product_details as $pDetail) {?>
                                        <details>
                                            <summary><?=mb_strtoupper($pDetail->titulo);?></summary>
                                            <?=$pDetail->note;?>
                                        </details>
                                    <?php } ?>
                                <?php } ?>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <!-- /content-left-wrapper -->
        </div>
        <div class="col-lg-5 content-right" id="start">
            <div id="wizard_container">

                <span class="header-contador">
                    <hr style="margin: 10px 0 10px 0;">
                    <div id="tempo-sessao" style="text-align: right;"></div>
                    <div style="text-align: right;">Atendido por <span style="font-weight: bold"><?php echo $vendedor->name;?></span></div>

                    <div class="barra-progresso">
                        <div class="porcentagem" id="progresso-da-barra"></div>
                        <div id="mostracontagem" style="margin-top: 5px;color: #0a91ff;font-weight: bold;"></div>
                    </div>
                    <hr style="margin: 10px 0 10px 0;">
                </span>
                <div class="apresentacao_viagem_visivel">
                    <h5 style="text-align: right;line-height: 20px;font-size: 18px;"> <span style="font-style: italic;"><?php echo $produto->name?></span><br/></h5>
                </div>

                <?php if ($lista_espera) {?>
                    <div class="orcamento">
                        <h5 style="line-height: 20px;font-size: 18px;"> <span style="font-style: italic;">LISTA DE ESPERA</span>
                    </div>
                <?php } elseif ($apenas_cotacao){ ?>
                    <div class="orcamento">
                        <h5 style="line-height: 20px;font-size: 18px;"> <span style="font-style: italic;">APENAS COTAÇÃO</span>
                    </div>
                <?php } ?>

                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>

                <input id="website" name="website" type="text" value="">
                <input id="vendedor" name="vendedor" type="hidden" value="<?php echo $vendedor->id;?>">
                <input id="subTotal" name="subTotal" type="hidden" value="0">
                <input id="totalParcelaPagSeguro" name="totalParcelaPagSeguro" type="hidden" value="0">

                <input id="subTotalTaxas" name="subTotalTaxas" type="hidden" value="0">
                <input id="programacaoId" name="programacaoId" type="hidden" value="<?php echo $programacao->id;?>">
                <input id="tipoTransporteId" name="tipoTransporteId" type="hidden" value="<?php echo $transporteDisponivel->id;?>">
                <input id="totalPassageiros" name="totalPassageiros" type="hidden" value="0">
                <input id="listaEmpera" name="listaEmpera" type="hidden" value="<?php echo $programacao->getTotalDisponvel();?>">
                <input id="valorSinal" name="valorSinal" type="hidden" value="0">
                <input id="tipoCobrancaSinal" name="tipoCobrancaSinal" type="hidden" value="">
                <input id="contaMovimentadorSinal" name="contaMovimentadorSinal" type="hidden" value="">
                <input id="pagamentoComCartaoSinal" name="tipoCobrancaSinal" type="hidden" value="0">

                <input id="file_pdf" type="hidden" name="file_pdf">

                <input type="hidden" name="senderHash">
                <input type="hidden" name="creditCardToken"/>
                <input type="hidden" name="deviceId" id="deviceId">

                <?php
                // Verificar se o servidor fornece o cabeçalho 'HTTP_X_FORWARDED_FOR'
                if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                }
                // Verificar se o servidor fornece o cabeçalho 'HTTP_CLIENT_IP'
                elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
                }
                // Caso contrário, obter o endereço IP padrão do servidor
                else {
                    $ip = $_SERVER['REMOTE_ADDR'];
                }
                ?>
                <input type="hidden" name="ip_client" id="ip_client" value="<?=$ip;?>">

                <!-- Leave for security protection, read docs for details -->
                <div id="middle-wizard" style="height: 105%;">

                    <!-- Passo 2 - Tipo de Hospedagem e Quantidade de Passageiro -->
                    <div class="step" id="select_qtd_comprador">
                        <?php if(!empty($tiposHospedagem)){?>
                            <?php if (  $this->session->userdata('cnpjempresa') == 'deboraexcursoes' ||
                                        $this->session->userdata('cnpjempresa') == 'eleveviagens' ||
                                        $this->session->userdata('cnpjempresa') == 'caicaraexpedicoes' ||
                                        $this->session->userdata('cnpjempresa') == 'viagensdetrem' ||
                                        $this->session->userdata('cnpjempresa') == 'brexcursoes' ||
                                        $this->session->userdata('cnpjempresa') == 'fenixturismo' ||
                                        $this->session->userdata('cnpjempresa') == 'busaodocabral' ||
                                        $this->session->userdata('cnpjempresa') == 'novotempoturismo'){?>
                                <h3 class="main_question"><strong>1/6</strong>Categoria?<br/><small style="font-size: 11px;">*valor por pessoa</small></h3>
                            <?php } else { ?>
                                <h3 class="main_question"><strong>1/6</strong>Tipo de Hospedagem?<br/><small style="font-size: 11px;">*valor por pessoa</small></h3>
                            <?php }?>
                            <?php if ($Settings->instrucoes_faixa_valores){?>
                                <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;margin-bottom: 15px;"><?=$Settings->instrucoes_faixa_valores;?></div>
                            <?php } ?>
                            <?php foreach ($tiposHospedagem as $tipoHospedagem) {
                                $ativo = $tipoHospedagem->status == 'ATIVO' ? true  : false;?>
                                <?php if ($ativo) {
                                    $faixaEtariaValoresHospedagem = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemByTipoHopedagem($programacao->produto, $tipoHospedagem->id);
                                    $quarto = $programacao->getQuartos()[(int)$tipoHospedagem->id];
                                    $nomeTipoHospedagem =  $tipoHospedagem->name;
                                    $tipoEspedagemSemEstoque = false;

                                    if ($quarto && $produto->controle_estoque_hospedagem && $quarto->estoque_atual <= 0)  {
                                        $tipoEspedagemSemEstoque = true;
                                        $nomeTipoHospedagem =  $tipoHospedagem->name.' [Indisponível Para Venda]';
                                    }

                                    ?>
                                    <?php if (!empty($faixaEtariaValoresHospedagem)){?>
                                            <div class="cart ">
                                                <div class="form-group cart_destaque_m cart_destaque_hospedagem <?php if ($tipoEspedagemSemEstoque) echo 'desabilitar_tipo_hospedagem';?>">
                                                    <label class="container_radio version_2" style="color: #222222;font-weight: 600;"><span class="fa fa-users"></span> <?= $nomeTipoHospedagem;?>
                                                        <?php if ($tipoHospedagem->preco > 0) echo $this->sma->formatMoney($tipoHospedagem->preco);?>
                                                        <input type="radio"
                                                               name="tipoHospedagem"
                                                               valor="<?= $tipoHospedagem->preco;?>"
                                                               value="<?= $tipoHospedagem->id;?>"
                                                               class="required"
                                                               onclick="exibirFaixasValorHospedagem('fvh_<?php echo $tipoHospedagem->id;?>', this);calcularTotalPagar(true);"
                                                               onchange="getVals(this, 'question_1');">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                    <div style="margin-top: 25px;margin-left: 50px;">
                                                        <small style="font-size: 12px;color: #222222;"><?php echo $tipoHospedagem->note;?></small>
                                                    </div>
                                                </div>
                                                <div class="">
                                                    <?php foreach ($faixaEtariaValoresHospedagem as $fv){

                                                        $preco_item = $fv->valor;

                                                        if ($produto->cat_precificacao == 'preco_por_data') {
                                                            $preco_item = $programacao->preco;
                                                        }

                                                        ?>
                                                        <div class="fvhHospedagem fvh_<?php echo $tipoHospedagem->id;?>" style="display: none;">
                                                            <div class="form-group cart_destaque_m">

                                                                <table style="width: 100%">
                                                                    <tr>
                                                                        <td style="width: 70%">
                                                                            <label><span class="fa fa-user"></span> <?php echo $fv->name;?></label><br/>
                                                                            <small style="font-size: 12px;font-weight: 500;color: #6c757d;"><?php echo $fv->note;?></small>
                                                                            <?php if($preco_item > 0){?>
                                                                                <small style="color: #28a745;font-size: 20px;font-weight: 500;"><?php echo $this->sma->formatMoney($preco_item);?></small>
                                                                            <?php } else {?>
                                                                                <?php if(!$produto->apenas_cotacao){?>
                                                                                    <small style="color: #28a745;font-size: 20px;font-weight: 500;">Grátis</small>
                                                                                <?php }?>
                                                                            <?php } ?>
                                                                        </td>
                                                                        <td style="width: 30%">
                                                                            <div class="row">
                                                                                <div class="col-4" style="padding: 0px;"><button type="button" class="btn btn-default" onclick="menos('<?php echo $fv->id;?>')" style="width: 100%;height: 100%;border-radius: 0px;background: #ffffff;outline: none;box-shadow: none;""><span class="fa fa-minus fa-2x" style="color: #28a745;"></span></button></div>
                                                                                <div class="col-4" style="padding: 0px;;">
                                                                                    <select class="combo combo2 fvDependentes"
                                                                                            descontarVaga="<?php echo $fv->descontarVaga;?>"
                                                                                            faixaId="<?php echo $fv->faixaId;?>"
                                                                                            tipoHospedagem="<?php echo $tipoHospedagem->id;?>"
                                                                                            faixaDependente="<?php echo $fv->faixaDependente;?>"
                                                                                            readonly="readonly"
                                                                                            name="Clientes_<?php echo $fv->faixaId.'_'.$preco_item.'_'.$fv->descontarVaga.'_'.$tipoHospedagem->id;?>"
                                                                                            id="<?php echo $fv->id;?>"
                                                                                            style="border-radius: 0px;text-align: center;font-size: 2em;border: 1px solid #212529;color: #212529;border-radius: 12px;">
                                                                                        <?php for ($cr = 0; $cr<=$configuracaoGeral->qty_client_records;$cr++){?>
                                                                                            <option valor="<?php echo $preco_item;?>" value="<?=$cr?>"><?=$cr?></option>
                                                                                        <?php } ?>
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-4" style="padding: 0px;"><button type="button" class="btn btn-default" onclick="mais('<?php echo $fv->id;?>');" style="width: 100%;height: 100%;border-radius: 0px;background: #ffffff;outline: none;box-shadow: none;"><span class="fa fa-plus fa-2x" style="color: #28a745;"></span></button></div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    <?php }?>
                                                </div>
                                            </div>

                                    <?php } ?>
                                <?php }?>
                            <?php } ?>
                        <?php } else{ ?>
                            <h3 class="main_question"><strong>1/6</strong>Quantas pessoas vão viajar?</h3>
                            <?php if ($Settings->instrucoes_faixa_valores){?>
                                <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;margin-bottom: 15px;"><?=$Settings->instrucoes_faixa_valores;?></div>
                            <?php } ?>
                            <div class="">
                                <?php if (!empty($faixaEtariaValores)){?>
                                    <?php foreach ($faixaEtariaValores as $fv){
                                        $preco_item = $fv->valor;

                                        if ($produto->cat_precificacao == 'preco_por_data') {
                                            $preco_item = $programacao->preco;
                                        }

                                        ?>
                                        <div class="form-group cart_destaque">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="width: 70%">
                                                        <div>
                                                            <label> <span class="fa fa-users"></span><span style="font-size: 20px;"> <?php echo $fv->name;?></span></label><br/>
                                                            <small style="font-size: 12px;font-weight: 500;color: #6c757d;"><?php echo $fv->note;?></small>
                                                            <?php if($preco_item > 0){?>
                                                                <small style="color: #28a745;font-size: 20px;font-weight: 500;"><?php echo $this->sma->formatMoney($preco_item);?></small>
                                                            <?php } else {?>
                                                                <?php if(!$produto->apenas_cotacao){?>
                                                                    <small style="color: #28a745;font-size: 20px;font-weight: 500;">Grátis</small>
                                                                <?php }?>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                    <td style="width: 30%">
                                                        <div class="row">
                                                            <div class="col-4" style="padding: 0px;"><button type="button" class="btn btn-default" onclick="menos('<?php echo $fv->id;?>')" style="width: 100%;height: 100%;border-radius: 0px;background: #ffffff;outline: none;box-shadow: none;""><span class="fa fa-minus fa-2x" style="color: #28a745;"></span></button></div>
                                                            <div class="col-4" style="padding: 0px;">
                                                                <select class="combo combo2 fvDependentes"
                                                                        descontarVaga="<?php echo $fv->descontarVaga;?>"
                                                                        faixaId="<?php echo $fv->faixaId;?>"
                                                                        faixaDependente="<?php echo $fv->faixaDependente;?>"
                                                                        readonly="readonly"
                                                                        name="Clientes_<?php echo $fv->faixaId.'_'.$preco_item.'_'.$fv->descontarVaga.'_null'?>"
                                                                        id="<?php echo $fv->id;?>"
                                                                        style="border-radius: 0px;text-align: center;font-size: 2em;border: 1px solid #212529;color: #212529;border-radius: 12px;">
                                                                    <?php for ($cr = 0; $cr<=$configuracaoGeral->qty_client_records;$cr++){?>
                                                                        <option valor="<?php echo $preco_item;?>" value="<?=$cr?>"><?=$cr?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-4" style="padding: 0px;"><button type="button" class="btn btn-default" onclick="mais('<?php echo $fv->id;?>');" style="width: 100%;height: 100%;border-radius: 0px;background: #ffffff;outline: none;box-shadow: none;""><span class="fa fa-plus fa-2x" style="color: #28a745;"></span></button></div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>

                                    <?php }?>
                                <?php } ?>
                            </div>
                        <?php } ?>
                        <div style="margin-top: 15px;display: none;" id="msn_compra_somente_principal">
                            <div class="form-group cart_destaque" style="background-color: #fcf8e3;border-color: #faebcc;color: #8a6d3b;">
                                Não é possível realizar a compra de passeios apenas para dependentes.
                            </div>
                        </div>
                    </div>
                    <!-- /step-->

                    <!-- Passo 2 -  Quantidade de Passageiro -->
                    <?php if ($produto->isApenasColetarPagador == 0 || $produto->isApenasColetarPagador == 2){?>
                        <div class="step">
                            <h3 class="main_question"><strong>1/6</strong>Passageiros:</h3>
                            <?php if(!empty($faixaEtariaValores)){?>
                                <?php
                                $contador = 1;
                                foreach ($faixaEtariaValores as $fv){?>
                                    <?=getDependentes($contador, $produto, $programacao, $fv, $assets, null, $configuracaoGeral->qty_client_records); ?>
                                    <?php $contador++; } ?>
                            <?php } else if (!empty($tiposHospedagem)){?>
                                <?php foreach ($tiposHospedagem as $tipoHospedagem) {
                                    $ativo = $tipoHospedagem->status == 'ATIVO' ? true  : false;?>
                                    <?php if ($ativo) {
                                        $faixaEtariaValoresHospedagem = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemByTipoHopedagem($programacao->produto, $tipoHospedagem->id); ?>
                                        <?php if (!empty($faixaEtariaValoresHospedagem)) {?>
                                            <?php $contador = 1;
                                            foreach ($faixaEtariaValoresHospedagem as $fv){?>
                                                <?=getDependentes($contador, $produto, $programacao, $fv, $assets, $tipoHospedagem, $configuracaoGeral->qty_client_records); ?>
                                                <?php $contador++; } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php }?>
                            <div class="form-group cart_destaque error_preenchimento_passageiros" style="background-color: #f2dede;border-color: #ebccd1;color: #a94442;display: none;">
                                Ops! Você esqueceu de preencher alguma informação dos passageiros.
                            </div>
                            <div class="form-group cart_destaque error_preenchimento_responsavel_venda" style="background-color: #f2dede;border-color: #ebccd1;color: #a94442;display: none;margin-top: 10px;">
                                Ops! Você não informou nenhum responsável pela compra.
                            </div>
                        </div>
                    <?php }?>

                    <!-- Passo 3 - Local de embarque -->
                    <?php if(!empty($embarques)) {?>
                        <div class="step">
                            <h3 class="main_question"><strong>3/6</strong>Escolha o seu local de embarque:</h3>
                            <?php if ($Settings->instrucoes_local_embarque){?>
                                <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;margin-bottom: 15px;"><?=$Settings->instrucoes_local_embarque;?></div>
                            <?php } ?>
                            <?php foreach ($embarques as $embarque) {
                                $label = $embarque->name;
                                $horarioEmbarque = '';
                                $dataEmbarque = '';
                                $note = '';

                                if ($embarque->note != '') {
                                    $note = '<span class="fa fa-info-circle"></span> '.$embarque->note.'<br/>';
                                }

                                if ($embarque->dataEmbarque != null) {
                                    if ($embarque->dataEmbarque != '0000-00-00'){
                                        $dataEmbarque = '<span class="fa fa-calendar"></span> '.$this->sma->hrsd($embarque->dataEmbarque).'<br/>';
                                    }
                                }

                                if ($embarque->horaEmbarque != null &&
                                    $embarque->horaEmbarque != '00:00:00.0'&&
                                    $embarque->horaEmbarque != '00:00:00') {
                                    $horarioEmbarque = '<span class="fa fa-clock-o"></span> '.$this->sma->hf($embarque->horaEmbarque);
                                } ?>
                                <div class="form-group cart_destaque">
                                    <label class="container_radio version_2"><span class="fa fa-map-marker fa-2x" style="font-size: 20px;"></span> <?= $label;?>
                                        <div style="font-size: 15px;">
                                            <?php echo $note.''.$dataEmbarque.''.$horarioEmbarque;?>

                                            <?php

                                            $local = $this->site->getLocalEmbarqueRodoviarioById($produto->id, $embarque->id);

                                            $endereco = '';

                                            if ($local->endereco) {
                                                $endereco .= $local->endereco;
                                            }

                                            if ($local->numero) {
                                                $endereco .= ' '.$local->numero;
                                            }

                                            if ($local->complemento) {
                                                $endereco .= ' - '.$local->complemento;
                                            }

                                            if ($local->bairro) {
                                                $endereco .= ' - '.$local->bairro;
                                            }

                                            if ($local->cidade) {
                                                $endereco .= '<br/>'.$local->cidade;
                                            }

                                            if ($local->cidade) {
                                                $endereco .= '/'.$local->estado;
                                            }

                                            if ($local->cep) {
                                                $endereco .= ' CEP '.$local->cep.' ';
                                            }

                                            $endereco_texto = str_replace('Compl.', ' ', $endereco);
                                            $endereco_texto = str_replace('-', ' ', $endereco);
                                            $endereco_texto = str_replace('/', ' ', $endereco);
                                            $endereco_texto = str_replace('<br/>', ' ', $endereco);
                                            $endereco_texto = strip_tags($endereco_texto);
                                            // Substitui espaços por "+" para URL amigável
                                            $endereco_url   = urlencode($endereco_texto);

                                            // Gera o link do Google Maps
                                            $maps_link = "https://www.google.com/maps/search/?api=1&query=$endereco_url";
                                            ?>

                                            <?php if($local->endereco){?>
                                                <span class="color_theme"><?=$endereco;?></span>
                                                <br/><a href="<?=$maps_link;?>" target="_blank" style="text-decoration: underline;">Ver no Google Maps</a>
                                            <?php } ?>
                                        </div>
                                        <input type="radio" name="localEmbarque" value="<?= $embarque->id;?>" class="required"
                                               onchange="getVals(this, 'question_1');">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            <?php } ?>
                        </div>
                    <?php }?>
                    <!-- /step-->


                    <!-- Passo 3 - Local de embarque -->
                    <?php if($produto->captarEnderecoLink) {?>
                        <div class="step">
                            <h3 class="main_question"><strong>3/6</strong>Informe seu endereço:</h3>

                            <div class="form-group">
                                <label for="cep">CEP</label>
                                <input type="tel"
                                       class="form-control"
                                       name="cep"
                                       id="cep"
                                       autocomplete="off"
                                       maxlength="9"
                                       required="required"
                                       placeholder="xxxxx-xxx">
                            </div>
                            <div class="form-group">
                                <label for="endereco">Endereço</label>
                                <input type="text"
                                       class="form-control"
                                       name="endereco"
                                       id="endereco"
                                       autocomplete="off"
                                       required="required"
                                       placeholder="">
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <label for="numeroEndereco">Número</label>
                                    <input type="text" class="form-control"
                                           name="numeroEndereco"
                                           id="numeroEndereco"
                                           autocomplete="off"
                                           placeholder="">
                                </div>
                                <div class="col-8">
                                    <label for="complementoEndereco">Complemento</label>
                                    <input type="text" class="form-control"
                                           name="complementoEndereco"
                                           autocomplete="off"
                                           placeholder="">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="bairro">Bairro</label>
                                <input type="text" class="form-control"
                                       name="bairro"
                                       id="bairro"
                                       autocomplete="off"
                                       required="required"
                                       placeholder="">
                            </div>
                            <div class="row">
                                <div class="col-8">
                                    <label for="cidade">Cidade</label>
                                    <input type="text" class="form-control"
                                           name="cidade"
                                           id="cidade"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="">
                                </div>
                                <div class="col-4">
                                    <label for="estado">UF</label>
                                    <input type="text" class="form-control"
                                           name="estado"
                                           id="estado"
                                           autocomplete="off"
                                           required=""
                                           placeholder="">
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <!-- Passo 4 - OPICIONAIS -->
                    <?php if(!empty($opcionais)) {?>
                        <div class="step">
                            <h3 class="main_question"><strong>3/6</strong>Selecione os itens do seu pacote:</h3>
                            <?php foreach ($opcionais as $opcional) {
                                $nomeOpcional = $opcional->name;
                                $preco = $opcional->price;

                                $valoresPorFaixaEtariaAdicional = $this->ProdutoRepository_model->getServicosAdicionaisFaixaEtariaRodoviarioParaLinkDeReservas($produto->id, $opcional->id);
                                ?>
                                <div class="budget_slider_adicionais">
                                    <div class="row">
                                        <div class="col-12">
                                            <div style="background: #e9ecef;padding: 10px;">
                                                <label style="font-size: 20px;font-weight: 500;"><span class="fa fa-check"></span> <?php echo $nomeOpcional; ?></label>
                                            </div>
                                            <?php if ($opcional->product_details){?>
                                                <div style="border: 1px solid #dcdcdc;">
                                                    <div style="margin-top: 15px;padding: 10px;">
                                                        <?php echo $opcional->product_details?>
                                                        <?php if ($opcional->details) {?>
                                                            <div class="mais_informacoes_adicional">
                                                                <a href="#0" data-toggle="modal" style="text-decoration: underline;" data-target="#more-information-adicionais<?php echo $opcional->id?>"> <span class="fa fa-comment"></span> INFORMAÇÕES</a>
                                                            </div>
                                                        <?php }?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <!-- Modal terms -->
                                            <div class="modal fade" id="more-information-adicionais<?php echo $opcional->id?>" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-dialog-centered">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title" id="termsLabel">Mais Informações do Serviço Opcional</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="col-sm-12">
                                                                <?= $opcional->details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang(' Sobre o Pacote') . '</div><div class="panel-body">' . $opcional->details . '</div></div>' : ''; ?>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
                                                        </div>
                                                    </div>
                                                    <!-- /.modal-content -->
                                                </div>
                                                <!-- /.modal-dialog -->
                                            </div>
                                            <!-- /.modal -->

                                            <div style="margin-top: 15px;text-align: right;">
                                                <a class="btn_selecionar_opcional rounded" onclick="adicionarOpcional('<?php echo $opcional->id;?>')" id="btn_adicional<?php echo $opcional->id;?>" style="background: #e58801;color: #ffff;"> <span class="fa fa-save"></span> SELECIONAR</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-top: 15px;display: none;" id="div_adicional<?php echo $opcional->id;?>">
                                        <?php foreach ($valoresPorFaixaEtariaAdicional as $fva) {?>
                                            <input type="hidden" value="<?php echo $fva->valor;?>"   name="valorServicoAdicional[]" />
                                            <input type="hidden" value="<?php echo $fva->faixaId;?>" name="faixaIdAdiconal[]" />
                                            <input type="hidden" value="<?php echo $opcional->id;?>" name="servicosAdicionais[]" />

                                            <div class="col-12">
                                                <label style="font-weight: bold;" class="main_question"><span class="fa fa-user"></span>
                                                    QTD <?php echo strtoupper($fva->name);?>
                                                    <?php if ($fva->note) {?>
                                                        <br/><?php echo $fva->note;?>
                                                    <?php } ?>
                                                </label>
                                                <small style="color: #28a745;font-size: 20px;font-weight: 500; float: right;">
                                                    <?php if($fva->valor > 0) {?>
                                                        <?php echo $this->sma->formatMoney($fva->valor);?>
                                                    <?php } else { ?>
                                                        Grátis
                                                    <?php } ?>
                                                </small>
                                                <div class="form-group">
                                                    <div class="styled-select clearfix">
                                                        <select class="combo qtdADicionais<?php echo $opcional->id;?> qtdAdicionais<?php echo $fva->faixaId;?> adicionais"
                                                                opcional="<?php echo $opcional->id;?>"
                                                                valor="<?php echo $fva->valor;?>"
                                                                name="qtdAdicionais[]"
                                                                onchange="getCondicoesDePagamento();getParcelamento();calcularTotalPagar(false);"
                                                                id="qtdADicionais<?php echo $opcional->id.'_'.$fva->faixaId;?>">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <div class="col-12">
                                            <button type="button"  onclick="excluirOpcional('<?php echo $opcional->id;?>');" id="btn_excluir_adicional<?php echo $opcional->id;?>" style="margin-top: 10px;cursor: pointer;display: none;margin-top: -15px;margin-bottom: 10px;"><span class="icon-trash"></span> Excluir</button>
                                        </div>
                                    </div>
                                </div>
                            <?php }?>
                        </div>
                    <?php }?>
                    <!-- /step-->

                    <!-- Passo 4 - OPICIONAIS -->
                    <?php if(!empty($opcionais)) {?>
                        <div class="step" id="preview_tb_adicional">
                            <h3 class="main_question"><strong>4/6</strong>Preview De Contratação de Adicionais:</h3>
                            <?php foreach ($opcionais as $opcional) {?>
                                <div class="budget_slider_adicionais_add" id="item_adicional_preview_add<?php echo $opcional->id;?>" style="display: none;">
                                    <div class="row">
                                        <div class="col-12">
                                            <h5><?php echo $opcional->name; ?></h5>
                                            <div class="price_adicionais"> <i class="icon-user"></i> <span id="preview-qtdadultos<?php echo $opcional->id;?>">0</span> ADULTO(S) TOTAL <span id="preview-priceadultos<?php echo $opcional->id;?>">R$ 0,00</span></div>
                                            <div class="price_adicionais"> <i class="icon-child"></i> <span id="preview-qtdcriancas<?php echo $opcional->id;?>">0</span>  CRIANÇA(S) TOTAL <span id="preview-pricecriancas<?php echo $opcional->id;?>"> R$ 0,00</span></div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <?php if ($transporteDisponivel && $programacao->getTotalDisponvel() > 0){?>
                        <!-- Passo 5 - ONIBUS -->
                        <div class="step">
                            <h3 class="main_question"><strong>4/6</strong>Clique no(s) assento(s) disponível(is):</h3>
                            <div id="client_sel_assentos"></div>

                            <?php if ($Settings->instrucoes_planta_onibus){?>
                                <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;"><?=$Settings->instrucoes_planta_onibus;?></div>
                            <?php } ?>

                            <!--MAPA DO ONIBUS!-->
                            <iframe width="100%" frameborder="0" scrolling="no" id="iframe_onibus" height="800px"></iframe>

                            <?php if (!empty($rotulos)) {?>
                                <div class="fild">
                                    <div class="fild_legend">Cobrança Extra de Assentos</div>
                                    <?php foreach ($rotulos as $rotulo) {?>
                                        <span style="background: <?=$rotulo->cor;?>;color: #ffffff;border-radius: 0.25em;margin-left: 5px;">
                                            <span style="padding: 0.2em 0.6em 0.3em;font-size: 14px;font-weight: 600;"><?=$this->sma->formatMoney($rotulo->valor)?></span>
                                        </span>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                         </div>
                    <?php } ?>

                    <?php if (coletarPagador($produto)) {?>
                        <!-- Passo 6 - PAGADOR RESPONSAVEL PELA COMPRA -->
                        <div class="step">
                            <h3 class="main_question"><strong>4/6</strong><i class="fa fa-user"></i> Dados do Responsável pela Compra:</h3>
                            <?=getPagador($this->ProdutoRepository_model->getValorFaixaByID($this->Settings->faixa_pagador)); ?>
                        </div>
                    <?php } ?>

                    <?php
                        $com_sinal  = FALSE;
                        $taxasArray = [];

                        foreach ($tiposCobranca as $tipoCobranca) {

                            if ($produto->isTaxasComissao) {
                                $taxas  = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobranca->id, null, $produto->id);
                            } else {
                                $taxas  = $this->settings_model->getTaxaConfiguracaoSettings($tipoCobranca->id);
                            }

                            $taxasArray[$tipoCobranca->id]  = $taxas;

                            if ($taxas->sinal > 0 && $taxas->is_sinal) {
                                $com_sinal = TRUE;
                            }
                        }
                    ?>

                    <!-- Passo 6 - SINAL -->

                    <?php if ($com_sinal) {?>
                        <div class="step" id="step_tipo_cobranca_sinal">
                            <h3 class="main_question"><strong>4/6</strong>Sinal de Pagamento:</h3>

                            <?php if ($Settings->instrucoes_sinal_pagamento) {?>
                                <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;margin-bottom: 15px;"><?=$Settings->instrucoes_sinal_pagamento;?></div>
                            <?php } ?>
                            <?php if ($produto->permite_pagamento_total) {?>
                                <div class="form-group cart_destaque_m">
                                    <label class="container_radio version_2">
                                        Pagar o Valor Total
                                        <input type="radio" checked="checked" name="pagar_com_sinal" value="2" onclick="pagamentoSinal(this);">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            <?php } ?>

                            <?php if (!$produto->obriga_sinal) {?>
                                <div class="form-group cart_destaque_m">
                                    <label class="container_radio version_2">
                                        Pagar Sem Dar Um Sinal
                                        <input type="radio" name="pagar_com_sinal" value="0" onclick="pagamentoSinal(this);">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            <?php } ?>

                            <div class="form-group cart_destaque_m">
                                <label class="container_radio version_2">
                                    Pagar Com Sinal de Pagamento
                                    <input type="radio"
                                           name="pagar_com_sinal"
                                           <?php if (!$produto->permite_pagamento_total) echo 'checked="checked"';?>
                                           value="1"
                                           onclick="pagamentoSinal(this);">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                            <?php
                            $contador_sinal = 0;
                            foreach ($tiposCobranca as $tipoCobranca) {

                                $taxas = $taxasArray[$tipoCobranca->id];

                                if ($taxas->sinal <= 0 || !$taxas->is_sinal) {
                                    continue;
                                }

                                if ($produto->isTaxasComissao) {
                                    $diasMaximoPagamentoAntesViagem = $tipoCobranca->diasMaximoPagamentoAntesViagem;
                                    $exibirNaSemanaDaViagem         = $tipoCobranca->exibirNaSemanaDaViagem;
                                } else {
                                    $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
                                    $exibirNaSemanaDaViagem         = $taxas->exibirNaSemanaDaViagem;
                                }

                                $dataMaximaParaRealizarPagamento = getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao);

                                if (!$exibirNaSemanaDaViagem) {
                                    if (strtotime(date('Y-m-d')) >= strtotime($dataMaximaParaRealizarPagamento)) {
                                        continue;
                                    } else { ?>
                                        <div class="div_pagamento_sinal" <?php if ($produto->permite_pagamento_total) echo 'style="display: none;"';?> >
                                            <?php if ($contador_sinal == 0) {?>
                                                <h5 style="margin-top: 25px;">Como você prefere pagar o Sinal?</h5>
                                            <?php }?>
                                            <div class="form-group cart_destaque">
                                                <label class="container_radio version_2">
                                                    <?php if ($tipoCobranca->tipo == 'boleto'
                                                        || strpos(strtoupper($tipoCobranca->name), 'BOLETO')) {?>
                                                        <span class="fa fa-barcode fa-2x" style="font-size: 22px;"></span>
                                                        <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                        <div><?= $tipoCobranca->note;?></div>
                                                    <?php } else if ($tipoCobranca->tipo == 'pix' || strpos(strtoupper($tipoCobranca->name), 'PIX')) { ?>
                                                        <img src="<?= $assets ?>images/pix.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                        <div> <?= $tipoCobranca->note;?></div>
                                                    <?php } else if ($tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                                                        $tipoCobranca->tipo == 'carne_cartao_transparent'  ||
                                                        $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
                                                        strpos(strtoupper($tipoCobranca->name), 'CARTAO')) {?>
                                                        <span class="fa fa-credit-card fa-2x" style="font-size: 22px;"></span>
                                                        <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                        <div><?= $tipoCobranca->note;?></div>
                                                    <?php } else if ($tipoCobranca->tipo == 'carne_cartao' || $tipoCobranca->tipo == 'link_pagamento' || strpos(strtoupper($tipoCobranca->name), 'LINK')) { ?>
                                                        <span class="fa fa-link fa-2x" style="font-size: 22px;"></span>
                                                        <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                        <div><?= $tipoCobranca->note;?></div>
                                                    <?php } else  if(strpos(strtoupper($tipoCobranca->name), 'DEPOSITO') || strpos(strtoupper($tipoCobranca->name), 'DEPóSITO')) { ?>
                                                        <img src="<?= $assets ?>images/deposito.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                        <div> <?= $tipoCobranca->note;?></div>
                                                    <?php } else { ?>
                                                        <span class="fa fa-money fa-2x" style="font-size: 22px;"></span>
                                                        <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                        <div><?= $tipoCobranca->note;?></div>
                                                    <?php } ?>
                                                    <input type="radio"
                                                           tipo="<?= $tipoCobranca->tipo;?>"
                                                           conta="<?= $tipoCobranca->conta;?>"
                                                           name="tipoCobrancaSinal"
                                                           onclick="antesTipoCobrancaSinal();getCondicoesDePagamento();calcularTotalPagar(false);"
                                                           value="<?= $tipoCobranca->id;?>"
                                                           sinal="<?=$taxas->sinal;?>"
                                                           tipo_sinal="<?=$taxas->tipo_sinal;?>"
                                                           acrescimo_desconto_sinal="<?=$taxas->acrescimo_desconto_sinal;?>"
                                                           valor_acres_desc_sinal="<?=$taxas->valor_acres_desc_sinal;?>"
                                                           tipo_acres_desc_sinal="<?=$taxas->tipo_acres_desc_sinal;?>"
                                                           nome_cobranca="<?= $tipoCobranca->name;?>"
                                                           class="required"
                                                           onchange="getVals(this, 'question_1');">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    <?php }
                                } else { ?>
                                    <div class="div_pagamento_sinal" <?php if ($produto->permite_pagamento_total) echo 'style="display: none;"';?>>
                                        <?php if ($contador_sinal == 0) {?>
                                            <h5 style="margin-top: 25px;">Como você prefere pagar o Sinal?</h5>
                                        <?php }?>
                                        <div class="form-group cart_destaque">
                                            <label class="container_radio version_2">
                                                <?php if ($tipoCobranca->tipo == 'boleto' || strpos(strtoupper($tipoCobranca->name), 'BOLETO')) {?>
                                                    <span class="fa fa-barcode fa-2x" style="font-size: 22px;"></span>
                                                    <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                    <div><?= $tipoCobranca->note;?></div>
                                                <?php } else if ($tipoCobranca->tipo == 'pix' || strpos(strtoupper($tipoCobranca->name), 'PIX')) { ?>
                                                    <img src="<?= $assets ?>images/pix.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                    <div> <?= $tipoCobranca->note;?></div>
                                                <?php } else if ($tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                                                    $tipoCobranca->tipo == 'carne_cartao_transparent'  ||
                                                    $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
                                                    strpos(strtoupper($tipoCobranca->name), 'CARTAO')) {?>
                                                    <span class="fa fa-credit-card fa-2x" style="font-size: 22px;"></span>
                                                    <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                    <div><?= $tipoCobranca->note;?></div>
                                                <?php } else if ($tipoCobranca->tipo == 'carne_cartao' || $tipoCobranca->tipo == 'link_pagamento' || strpos(strtoupper($tipoCobranca->name), 'LINK')) { ?>
                                                    <span class="fa fa-link fa-2x" style="font-size: 22px;"></span>
                                                    <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                    <div><?= $tipoCobranca->note;?></div>
                                                <?php } else  if(strpos(strtoupper($tipoCobranca->name), 'DEPOSITO') || strpos(strtoupper($tipoCobranca->name), 'DEPóSITO')) { ?>
                                                    <img src="<?= $assets ?>images/deposito.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                    <div> <?= $tipoCobranca->note;?></div>
                                                <?php } else { ?>
                                                    <span class="fa fa-money fa-2x" style="font-size: 22px;"></span>
                                                    <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                    <div><?= $tipoCobranca->note;?></div>
                                                <?php } ?>
                                                <input type="radio"
                                                       tipo="<?= $tipoCobranca->tipo;?>"
                                                       conta="<?= $tipoCobranca->conta;?>"
                                                       name="tipoCobrancaSinal"
                                                       onclick="antesTipoCobrancaSinal();getCondicoesDePagamento();calcularTotalPagar(false);"
                                                       value="<?= $tipoCobranca->id;?>"
                                                       class="required"
                                                       sinal="<?=$taxas->sinal;?>"
                                                       tipo_sinal="<?=$taxas->tipo_sinal;?>"
                                                       acrescimo_desconto_sinal="<?=$taxas->acrescimo_desconto_sinal;?>"
                                                       valor_acres_desc_sinal="<?=$taxas->valor_acres_desc_sinal;?>"
                                                       tipo_acres_desc_sinal="<?=$taxas->tipo_acres_desc_sinal;?>"
                                                       nome_cobranca="<?= $tipoCobranca->name;?>"
                                                       onchange="getVals(this, 'question_1');">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                <?php }?>
                            <?php $contador_sinal++;} ?>
                        </div>
                    <?php } ?>

                    <!-- Passo 6.1 - TIPOS DE COBRANCA -->
                    <div class="step" id="step_tipo_cobranca">
                        <?php if ($com_sinal) {?>
                            <h3 class="main_question"><strong>4/6</strong><span id="title_tipo_cobranca">Como você prefere pagar?</span></h3>
                        <?php } else { ?>
                            <h3 class="main_question"><strong>4/6</strong>Como você prefere pagar?</h3>
                        <?php } ?>

                        <?php if ($Settings->instrucoes_tipo_cobranca){?>
                            <div class="cart_destaque" style="margin-top: 15px;border-radius: 15px;margin-bottom: 15px;"><?=$Settings->instrucoes_tipo_cobranca;?></div>
                        <?php } ?>
                        <?php foreach ($tiposCobranca as $tipoCobranca) {

                            $taxas = $taxasArray[$tipoCobranca->id];

                            if ($produto->isTaxasComissao) {
                                $diasMaximoPagamentoAntesViagem = $tipoCobranca->diasMaximoPagamentoAntesViagem;
                                $exibirNaSemanaDaViagem         = $tipoCobranca->exibirNaSemanaDaViagem;
                            } else {
                                $diasMaximoPagamentoAntesViagem = $taxas->diasMaximoPagamentoAntesViagem;
                                $exibirNaSemanaDaViagem         = $taxas->exibirNaSemanaDaViagem;
                            }

                            $dataMaximaParaRealizarPagamento = getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao);

                            if (!$exibirNaSemanaDaViagem) {
                                if (strtotime(date('Y-m-d')) >= strtotime($dataMaximaParaRealizarPagamento)) {
                                    continue;
                                } else { ?>
                                    <div class="form-group cart_destaque class_tipo_cobranca" id="div_pagamento_tipo_cobranca<?=$tipoCobranca->id;?>">
                                        <label class="container_radio version_2">
                                            <?php if ($tipoCobranca->tipo == 'boleto' || strpos(strtoupper($tipoCobranca->name), 'BOLETO')) {?>
                                                <span class="fa fa-barcode fa-2x" style="font-size: 22px;"></span>
                                                <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                <div><?= $tipoCobranca->note;?></div>
                                            <?php } else if ($tipoCobranca->tipo == 'pix' || strpos(strtoupper($tipoCobranca->name), 'PIX')) { ?>
                                                <img src="<?= $assets ?>images/pix.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                <div> <?= $tipoCobranca->note;?></div>
                                            <?php } else if ($tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                                                            $tipoCobranca->tipo == 'carne_cartao_transparent'  ||
                                                            $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
                                                            strpos(strtoupper($tipoCobranca->name), 'CARTAO')) {?>
                                                <span class="fa fa-credit-card fa-2x" style="font-size: 22px;"></span>
                                                <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                <div><?= $tipoCobranca->note;?></div>
                                            <?php } else if ($tipoCobranca->tipo == 'carne_cartao' || $tipoCobranca->tipo == 'link_pagamento' || strpos(strtoupper($tipoCobranca->name), 'LINK')) { ?>
                                                <span class="fa fa-link fa-2x" style="font-size: 22px;"></span>
                                                <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                <div><?= $tipoCobranca->note;?></div>
                                            <?php } else  if(strpos(strtoupper($tipoCobranca->name), 'DEPOSITO') || strpos(strtoupper($tipoCobranca->name), 'DEPóSITO')) { ?>
                                                <img src="<?= $assets ?>images/deposito.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                                <div> <?= $tipoCobranca->note;?></div>
                                            <?php } else { ?>
                                                <span class="fa fa-money fa-2x" style="font-size: 22px;"></span>
                                                <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                                <div><?= $tipoCobranca->note;?></div>
                                            <?php } ?>
                                            <input type="radio"
                                                   tipo="<?= $tipoCobranca->tipo;?>"
                                                   conta="<?= $tipoCobranca->conta;?>"
                                                   name="tipoCobranca"
                                                   onclick="getCondicoesDePagamento();"
                                                   id="tipoCobranca"
                                                   value="<?= $tipoCobranca->id;?>"
                                                   class="required"
                                                   onchange="getVals(this, 'question_1');">
                                            <span class="checkmark"></span>
                                        </label>
                                    </div>
                                <?php }
                            } else {?>
                                <div class="form-group cart_destaque class_tipo_cobranca" id="div_pagamento_tipo_cobranca<?=$tipoCobranca->id;?>">
                                    <label class="container_radio version_2">
                                        <?php if ($tipoCobranca->tipo == 'boleto' || strpos(strtoupper($tipoCobranca->name), 'BOLETO')) {?>
                                            <span class="fa fa-barcode fa-2x" style="font-size: 22px;"></span>
                                            <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                            <div><?= $tipoCobranca->note;?></div>
                                        <?php } else if ($tipoCobranca->tipo == 'pix' || strpos(strtoupper($tipoCobranca->name), 'PIX')) { ?>
                                            <img src="<?= $assets ?>images/pix.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                            <div> <?= $tipoCobranca->note;?></div>
                                        <?php } else if ($tipoCobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                                                        $tipoCobranca->tipo == 'carne_cartao_transparent'  ||
                                                        $tipoCobranca->tipo == 'cartao_credito_transparent_valepay' ||
                                                        strpos(strtoupper($tipoCobranca->name), 'CARTAO')) {?>
                                            <span class="fa fa-credit-card fa-2x" style="font-size: 22px;"></span>
                                            <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                            <div><?= $tipoCobranca->note;?></div>
                                        <?php } else if ($tipoCobranca->tipo == 'carne_cartao' || $tipoCobranca->tipo == 'link_pagamento' || strpos(strtoupper($tipoCobranca->name), 'LINK')) { ?>
                                            <span class="fa fa-link fa-2x" style="font-size: 22px;"></span>
                                            <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                            <div><?= $tipoCobranca->note;?></div>
                                        <?php } else  if(strpos(strtoupper($tipoCobranca->name), 'DEPOSITO') || strpos(strtoupper($tipoCobranca->name), 'DEPóSITO')) { ?>
                                            <img src="<?= $assets ?>images/deposito.png" style="width: 30px;"/><span style="text-transform: uppercase;"><?= $tipoCobranca->name;?></span>
                                            <div> <?= $tipoCobranca->note;?></div>
                                        <?php } else { ?>
                                            <span class="fa fa-money fa-2x" style="font-size: 22px;"></span>
                                            <span style="text-transform: uppercase;"><?= $tipoCobranca->name;?><br/></span>
                                            <div><?= $tipoCobranca->note;?></div>
                                        <?php } ?>
                                        <input type="radio"
                                               tipo="<?= $tipoCobranca->tipo;?>"
                                               conta="<?= $tipoCobranca->conta;?>"
                                               name="tipoCobranca"
                                               onclick="getCondicoesDePagamento();"
                                               id="tipoCobranca"
                                               value="<?= $tipoCobranca->id;?>"
                                               class="required"
                                               onchange="getVals(this, 'question_1');">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            <?php }?>
                        <?php } ?>

                        <style>
                            .cupom  {
                                margin-bottom: 10px;
                                margin-top: 25px;
                                font-weight: bold;
                                background: #dee2e6;
                                font-size: 16px;
                                text-align: center;
                                padding: 16px 15px 0px 15px;
                            }

                            .cupom_button {
                                padding: 10px 0px;
                                background: #6c757d;
                                font-size: 15px;
                            }
                        </style>
                        <div class="cart_destaque cupom" style="<?php if (!$configuracaoGeral->habilitar_cupom_desconto) echo 'display: none;'; ?>">
                            <div class="form-group">
                                <div class="row" style="text-align: left;">
                                    <div class="col-12">
                                        <a id="inserir-cupom" style="cursor: pointer;text-decoration: underline #6c757d;font-size: 12px;"><span class="fa fa-ticket"></span> CLIQUE E INSIRA SEU CUPOM DE DESCONTO</a>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group" id="div_cupom" style="display: none;">
                                <div class="row">
                                    <div class="col-8">
                                        <input type="text"
                                               class="form-control"
                                               id="cupom"
                                               autocomplete="off"
                                               name="cupom"
                                               style="text-transform: uppercase;font-size: 18px;font-weight: 800;"
                                               placeholder="CÓDIGO DO CUPOM">
                                    </div>
                                    <div class="col-4">
                                        <input type="button" class="btn_1 cupom_button" style="height: 100%;background: #e58801;" id="cupom-consulta" value="APLICAR"/>
                                    </div>
                                </div>
                                <div class="col-12" style="margin-top: 10px;font-size: 14px;">
                                    <span id="validade-cupom"></span>
                                    <input type="hidden" name="tipo_cupom_desconto"     id="tipo_cupom_desconto"/>
                                    <input type="hidden" name="valor_cupom_desconto"    id="valor_cupom_desconto"/>
                                    <input type="hidden" name="cupom_desconto_id"       id="cupom_desconto_id"/>
                                </div>
                            </div>
                        </div>
                        <br/><br/>
                    </div>

                    <div class="step" id="condicoes-pagamento">
                        <h3 class="main_question"><strong>5/6</strong>Selecione o número de Parcelas: </h3>
                        <div class="form-group">
                            <div class="styled-select clearfix">
                                <select class="combo_parcela" name="condicaoPagamento" id="condicaoPagamento" required="required"></select>
                            </div>
                        </div>
                        <span id="parcelamento"></span>
                    </div>

                    <!-- CARTÃO DE CRÉDITO PAGSEGURO TRANSPARENTE !-->
                    <div class="step" id="cartao-credito">
                        <h3 class="main_question"><strong>5/6</strong>Cartão de Crédito:<br/>
                            <span style="font-size: 11px;color: #28a745;font-weight: 500;">
                                    Você está pagamento com a segurança do <img src="<?= $assets ?>images/pagseguro.png"  style="width: 60px;" /><br/>
                                    <span id="cc-aceitos-pagseguro"></span>
                                </span>
                        </h3>
                        <div class="form-group cart_cartao_transpartente">
                            <div class="styled-select clearfix">
                                <div class="card-wrapper"></div>
                                <div id="erro-cartao" style="color: red;text-align: center;"></div>
                                <div class="form-group">
                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Preencha os dados do seu cartão</h3>
                                </div>
                                <div class="form-group" style="margin-top: 10px">
                                    <label for="number">Número do Cartão de Crédito</label>
                                    <input type="tel" class="form-control"
                                           autocomplete="off"
                                           name="number"
                                           id="cardNumberoPagSeguro"
                                           maxlength="19"
                                           required="required"
                                           placeholder="•••• •••• •••• ••••">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <img src="" id="band" style="display: none;max-width: none;">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">Títular do cartão</label>
                                    <input type="text" class="form-control"
                                           name="nameCartaoPagSeguro"
                                           id="nomeTitularCartaoPagSeguro"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="Nome do Titular (como gravado no cartão)">
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <label for="cardExpirationMonth">Vencimento</label>
                                        <div class="input-group expiration-date">
                                            <select class="form-control" id="card_expiry_mm"
                                                    name="card_expiry_mm">
                                                <option value="">MM</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <select class="form-control" id="card_expiry_yyyy"
                                                    name="card_expiry_yyyy">
                                                <option value="">YYYY</option>
                                                <option value="2022">2022</option>
                                                <option value="2023">2023</option>
                                                <option value="2024">2024</option>
                                                <option value="2025">2025</option>
                                                <option value="2026">2026</option>
                                                <option value="2027">2027</option>
                                                <option value="2028">2028</option>
                                                <option value="2029">2029</option>
                                                <option value="2030">2030</option>
                                                <option value="2031">2031</option>
                                                <option value="2032">2032</option>
                                                <option value="2033">2033</option>
                                                <option value="2034">2034</option>
                                                <option value="2035">2035</option>
                                                <option value="2036">2036</option>
                                                <option value="2037">2037</option>
                                                <option value="2038">2038</option>
                                                <option value="2039">2039</option>
                                                <option value="2040">2040</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label for="name">Cód.Segurança</label>
                                            <input type="tel"
                                                   class="form-control"
                                                   name="cvc"
                                                   required="required"
                                                   placeholder="CVV">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="name">Número de Parcelas</label>
                                    <div class="input-group expiration-date">
                                        <select id="parcelas-cartao-credito" required="required" class="combo" name="parcelas"></select>
                                    </div>
                                </div>
                                <div class="form-group terms">
                                    <label class="container_check" style="color: red;font-weight: bold;">
                                        ATENÇÃO: CASO NÃO SEJA O TÍTULAR DO CARTÃO CLIQUE AQUI.<br/>
                                        <input type="checkbox" id="ck-titular-cartao" name="ck-titular-cartao" value="Yes">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div id="div-titular-cartao-pagseguro" style="display: none;">
                                    <div class="form-group">
                                        <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Preencha os dados do títular do cartão</h3>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="cpfTitular">CPF do Titular</label>
                                                <input type="tel"
                                                       class="form-control"
                                                       name="cpfTitular"
                                                       id="cpfTitular"
                                                       autocomplete="off"
                                                       maxlength="14"
                                                       placeholder="CPF do Titular do Cartão">
                                            </div>
                                            <div class="col-6">
                                                <label>Telefone do Titular</label>
                                                <input type="tel"
                                                       class="form-control"
                                                       id="telTitular"
                                                       name="telTitular"
                                                       autocomplete="off"
                                                       maxlength="15"
                                                       placeholder="Com DDD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Endereço da Fatura do Cartão</h3>
                                </div>
                                <div class="form-group">
                                    <label for="cepTitular">CEP</label>
                                    <input type="tel"
                                           class="form-control"
                                           name="cepTitular"
                                           id="cepTitular"
                                           autocomplete="off"
                                           maxlength="9"
                                           required="required"
                                           placeholder="xxxxx-xxx">
                                </div>
                                <div class="form-group">
                                    <label for="enderecoTitular">Endereço</label>
                                    <input type="text"
                                           class="form-control"
                                           name="enderecoTitular"
                                           id="enderecoTitular"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="">
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="numeroEnderecoTitular">Número</label>
                                        <input type="text" class="form-control"
                                               id="numeroEnderecoTitular"
                                               name="numeroEnderecoTitular"
                                               autocomplete="off"
                                               placeholder="">
                                    </div>
                                    <div class="col-8">
                                        <label for="complementoEnderecoTitular">Complemento</label>
                                        <input type="text" class="form-control"
                                               name="complementoEnderecoTitular"
                                               autocomplete="off"
                                               placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bairroTitular">Bairro</label>
                                    <input type="text" class="form-control"
                                           name="bairroTitular"
                                           id="bairroTitular"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="">
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <label for="cidadeTitular">Cidade</label>
                                        <input type="text" class="form-control"
                                               name="cidadeTitular"
                                               id="cidadeTitular"
                                               autocomplete="off"
                                               required="required"
                                               placeholder="">
                                    </div>
                                    <div class="col-4">
                                        <label for="estadoTitular">UF</label>
                                        <input type="text" class="form-control"
                                               name="estadoTitular"
                                               id="estadoTitular"
                                               autocomplete="off"
                                               required=""
                                               placeholder="">
                                    </div>
                                </div>
                            </div>
                            <span style="font-size: 11px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                                Você está pagando com segurança pelo <img src="<?= $assets ?>images/pagseguro.png"  style="width: 60px;" />
                            </span>
                        </div>
                        <span id="parcelamento"></span>
                    </div>

                    <!-- CARTÃO DE CRÉDITO MERCADO PAGO TRANSPARENTE !-->
                    <div class="step" id="cartao-credito_mercadopago">
                        <h3 class="main_question"><strong>5/6</strong>Cartão de Crédito:<br/>
                            <span style="font-size: 11px;color: #28a745;font-weight: 500;">
                                <span style="margin-left: 10px;">Você está pagando com segurança pelo <img src="<?= $assets ?>images/mercadopago.png"  style="width: 60px;" /></span>
                            </span>
                        </h3>
                        <div class="form-group cart_cartao_transpartente">
                            <!-- Payment -->
                            <section class="payment-form dark">
                                <div class="container__payment">
                                    <div class="form-payment">
                                        <div class="payment-details">
                                            <div id="form-checkout-">
                                                <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Preencha os dados do seu cartão</h3>
                                                <div class="row">
                                                    <div class="form-group col-sm-8">
                                                        <label for="cardholderName">Nome do titular</label>
                                                        <input id="form-checkout__cardholderName" name="cardholderName" required="required" placeholder="Dono do Cartão" type="text" class="form-control"/>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="cardExpirationMonth">Vencimento</label>
                                                        <div class="input-group expiration-date">
                                                            <input id="form-checkout__cardExpirationMonth" maxlength="2" name="cardExpirationMonth" type="tel"  required="required" class="form-control"/>
                                                            <input id="form-checkout__cardExpirationYear" maxlength="2" name="cardExpirationYear" type="tel"  required="required" class="form-control"/>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-sm-8">
                                                        <label for="cardNumber">Número do cartão</label>
                                                        <input id="form-checkout__cardNumber" name="cardNumber" maxlength="19"
                                                               placeholder="•••• •••• •••• ••••" type="tel" required="required" class="form-control"/>
                                                    </div>
                                                    <div class="form-group col-sm-4">
                                                        <label for="cardholderName">Código de segurança</label>
                                                        <input id="form-checkout__securityCode" name="securityCode" placeholder="CVV" required="required" type="tel" class="form-control"/>
                                                    </div>
                                                    <div id="issuerInput" class="form-group col-sm-12 hidden">
                                                        <label for="issuer">Bandeira do cartão</label>
                                                        <select id="form-checkout__issuer" name="issuer" class="form-control"></select>
                                                    </div>
                                                    <div class="form-group col-sm-12">
                                                        <label for="installments">Selecione o número de parcelas</label>
                                                        <select id="form-checkout__installments" name="installments" type="text" class="form-control"></select>
                                                    </div>
                                                </div>
                                                <div class="form-group terms">
                                                    <label class="container_check" style="color: red;font-weight: bold;font-size: 12px;">
                                                        ATENÇÃO: CASO NÃO SEJA O TÍTULAR DO CARTÃO CLIQUE AQUI.<br/>
                                                        <input type="checkbox" id="ck-titular-cartao-mercadopago" name="ck-titular-cartao" value="Yes">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div id="div-titular-cartao-mercadopago" style="display: none;">
                                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Informe os dados do titular</h3>
                                                    <div class="row">
                                                        <div class="form-group col">
                                                            <label for="cardholderEmail">E-mail</label>
                                                            <input id="form-checkout__cardholderEmail" name="cardholderEmail" type="email" class="form-control"/>
                                                            <span style="font-size: 11px;">Enviaremos os detalhes do pagamento para você assim que confirmar sua compra.</span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="form-group col-sm-5">
                                                            <label for="identificationType">Identificação</label>
                                                            <select id="form-checkout__identificationType" name="identificationType" class="form-control"></select>
                                                        </div>
                                                        <div class="form-group col-sm-7">
                                                            <label for="docNumber">Documento do Titular do Cartão</label>
                                                            <input id="form-checkout__identificationNumber" name="docNumber" type="tel" class="form-control"/>
                                                            <span style="font-size: 11px;">Somente números sem pontos ou hifen.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <span style="font-size: 11px;">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                                                    Você está pagando com segurança pelo <img src="<?= $assets ?>images/mercadopago.png"  style="width: 60px;" />
                                                </span>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>

                    <!-- CARTÃO DE CRÉDITO VALE PAY TRANSPARENTE !-->
                    <div class="step" id="cartao-credito_valepay">
                        <h3 class="main_question"><strong>5/6</strong>Cartão de Crédito:<br/>
                            <span style="font-size: 11px;color: #28a745;font-weight: 500;">Você está pagamento com a segurança da <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" /></span>
                        </h3>
                        <div class="form-group cart_cartao_transpartente">
                            <div class="styled-select clearfix">
                                <div class="form-group">
                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Preencha os dados do seu cartão</h3>
                                </div>
                                <div class="row">
                                    <div class="col-7">
                                        <div class="form-group">
                                            <label for="name_card_valepay">Títular do cartão</label>
                                            <input type="text" class="form-control"
                                                   name="name_card_valepay"
                                                   id="name_card_valepay"
                                                   autocomplete="off"
                                                   required="required"
                                                   placeholder="Nome do Titular (como gravado no cartão)">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <label for="card_expiry_mm_valepay">Vencimento</label>
                                        <div class="input-group expiration-date">
                                            <select class="form-control" id="card_expiry_mm_valepay"
                                                    name="card_expiry_mm_valepay">
                                                <option value="">MM</option>
                                                <option value="01">01</option>
                                                <option value="02">02</option>
                                                <option value="03">03</option>
                                                <option value="04">04</option>
                                                <option value="05">05</option>
                                                <option value="06">06</option>
                                                <option value="07">07</option>
                                                <option value="08">08</option>
                                                <option value="09">09</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                            </select>
                                            <select class="form-control" id="card_expiry_yyyy_valepay"
                                                    name="card_expiry_yyyy_valepay">
                                                <option value="">YYYY</option>
                                                <option value="22">2022</option>
                                                <option value="23">2023</option>
                                                <option value="24">2024</option>
                                                <option value="25">2025</option>
                                                <option value="26">2026</option>
                                                <option value="27">2027</option>
                                                <option value="28">2028</option>
                                                <option value="29">2029</option>
                                                <option value="30">2030</option>
                                                <option value="31">2031</option>
                                                <option value="32">2032</option>
                                                <option value="33">2033</option>
                                                <option value="34">2034</option>
                                                <option value="35">2035</option>
                                                <option value="36">2036</option>
                                                <option value="37">2037</option>
                                                <option value="38">2038</option>
                                                <option value="39">2039</option>
                                                <option value="40">2040</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-7">
                                        <div class="form-group">
                                            <label for="card_number_valepay">Número do Cartão de Crédito</label>
                                            <input type="tel"
                                                   class="form-control"
                                                   autocomplete="off"
                                                   name="card_number_valepay"
                                                   id="card_number_valepay"
                                                   maxlength="19"
                                                   required="required"
                                                   placeholder="•••• •••• •••• ••••">
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="form-group">
                                            <label for="cvc_valepay">Cód.Segurança</label>
                                            <input type="tel"
                                                   class="form-control"
                                                   name="cvc_valepay"
                                                   required="required"
                                                   placeholder="CVV">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Selecione o parcelamento</h3>
                                </div>
                                <div class="form-group cart_destaque">
                                    <div class="input-group">
                                        <table style="width: 100%;" id="tb-parcelamento-valepay">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Parcela</th>
                                                    <th>Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot><tr><td colspan="3"></td></tr></tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div class="form-group terms">
                                    <label class="container_check" style="color: red;font-weight: bold;">
                                        ATENÇÃO: CASO NÃO SEJA O TÍTULAR DO CARTÃO CLIQUE AQUI.<br/>
                                        <input type="checkbox" id="ck-titular-cartao-valepay" name="ck-titular-cartao-valepay" value="Yes">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div id="div-titular-cartao-valepay" style="display: none;">
                                    <div class="form-group">
                                        <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Preencha os dados do títular do cartão</h3>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-6">
                                                <label for="cpf_titular_valepay">CPF do Titular</label>
                                                <input type="tel"
                                                       class="form-control"
                                                       name="cpf_titular_valepay"
                                                       id="cpf_titular_valepay"
                                                       autocomplete="off"
                                                       maxlength="14"
                                                       placeholder="CPF do Titular do Cartão">
                                            </div>
                                            <div class="col-6">
                                                <label for="tel_titular_valepay">Telefone do Titular</label>
                                                <input type="tel"
                                                       class="form-control"
                                                       id="tel_titular_valepay"
                                                       name="tel_titular_valepay"
                                                       autocomplete="off"
                                                       maxlength="15"
                                                       placeholder="Com DDD">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h3 class="title" style="font-size: 18px;background: #e9ecef;padding: 10px;">Endereço da Fatura do Cartão</h3>
                                </div>
                                <div class="form-group">
                                    <label for="cep_titular_valepay">CEP</label>
                                    <input type="tel"
                                           class="form-control"
                                           name="cep_titular_valepay"
                                           id="cep_titular_valepay"
                                           autocomplete="off"
                                           maxlength="9"
                                           required="required"
                                           placeholder="xxxxx-xxx">
                                </div>
                                <div class="form-group">
                                    <label for="endereco_titular_valepay">Endereço</label>
                                    <input type="text"
                                           class="form-control"
                                           name="endereco_titular_valepay"
                                           id="endereco_titular_valepay"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="">
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <label for="numero_endereco_titular_valepay">Número</label>
                                        <input type="text"
                                               class="form-control"
                                               id="numero_endereco_titular_valepay"
                                               name="numero_endereco_titular_valepay"
                                               autocomplete="off"
                                               placeholder="">
                                    </div>
                                    <div class="col-8">
                                        <label for="complemento_endereco_titular">Complemento</label>
                                        <input type="text"
                                               class="form-control"
                                               name="complemento_endereco_titular"
                                               autocomplete="off"
                                               placeholder="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="bairro_titular_valepay">Bairro</label>
                                    <input type="text"
                                           class="form-control"
                                           name="bairro_titular_valepay"
                                           id="bairro_titular_valepay"
                                           autocomplete="off"
                                           required="required"
                                           placeholder="">
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <label for="cidade_titular_valepay">Cidade</label>
                                        <input type="text"
                                               class="form-control"
                                               name="cidade_titular_valepay"
                                               id="cidade_titular_valepay"
                                               autocomplete="off"
                                               required="required"
                                               placeholder="">
                                    </div>
                                    <div class="col-4">
                                        <label for="estado_titular_valepay">UF</label>
                                        <input type="text"
                                               class="form-control"
                                               name="estado_titular_valepay"
                                               id="estado_titular_valepay"
                                               autocomplete="off"
                                               required=""
                                               placeholder="">
                                    </div>
                                </div>
                            </div>
                            <span style="font-size: 11px;">
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                                Você está pagando com segurança pela <img src="<?= $assets ?>images/valepay.png"  style="width: 60px;" />
                            </span>
                        </div>
                    </div>

                    <?php if($plugsign) { ?>
                            <style>
                                .buttom_currentPage {
                                    border: none;
                                    color: #fff;
                                    text-decoration: none;
                                    transition: background .5s ease;
                                    -moz-transition: background .5s ease;
                                    -webkit-transition: background .5s ease;
                                    -o-transition: background .5s ease;
                                    display: inline-block;
                                    cursor: pointer;
                                    outline: none;
                                    text-align: center;
                                    background: #2196f3;
                                    position: relative;
                                    font-size: 0.875rem;
                                    font-weight: 600;
                                    -webkit-border-radius: 3px;
                                    -moz-border-radius: 3px;
                                    border-radius: 3px;
                                    line-height: 1;
                                    padding: 12px 15px;
                                }

                                .canvas_div {
                                    text-align: center;
                                    border: 1px solid #dcdcdc;
                                    width: 100%;
                                    overflow-y: auto;
                                    overflow-x: auto;
                                    cursor: pointer;
                                }

                                .zoom_buttom {
                                    border: none;
                                    color: #fff;
                                    text-decoration: none;
                                    transition: background .5s ease;
                                    -moz-transition: background .5s ease;
                                    -webkit-transition: background .5s ease;
                                    -o-transition: background .5s ease;
                                    display: inline-block;
                                    cursor: pointer;
                                    outline: none;
                                    text-align: center;
                                    background: #2196f3;
                                    position: relative;
                                    font-size: 0.875rem;
                                    font-weight: 600;
                                    -webkit-border-radius: 3px;
                                    -moz-border-radius: 3px;
                                    border-radius: 3px;
                                }

                                .pdfViewer_c {

                                }
                                .zoom_div {
                                    text-align: right;
                                    padding: 0px 0px 5px 1px;
                                }
                            </style>
                        <div class="submit step">
                            <h3 class="main_question"><strong>6/6</strong>Leia Seu Contrato de Serviço:</h3>

                            <!--
                            <iframe src="" id="contrato_pdf" width="100%" height="700px" style="border: none;"></iframe>
                            !-->

                            <div class="zoom_div">
                                <button id="zoomOut" class="zoom_buttom">A-</button>
                                <button id="zoomIn" class="zoom_buttom">A+</button>
                            </div>

                            <div class="canvas_div">
                                <canvas id="pdfViewer" class="pdfViewer_c"></canvas>
                            </div>
                            <div style="text-align: center;">

                                <p>Página: <span id="currentPage">1</span> de <span id="totalPages">?</span></p>

                                <button id="prevPage" style="cursor: pointer;" class="buttom_currentPage">Página Anterior</button>
                                <button id="nextPage" style="cursor: pointer;" class="buttom_currentPage">Próxima Página</button>

                            </div>
                            <p style="font-weight: 500; margin-top: 25px;">
                                Uma cópia do contrato será enviada para o seu WhatsApp após a assinatura<br/>
                            </p>
                            <p style="font-weight: 500; margin-top: 10px;font-style: italic">
                                <a href="https://www.planalto.gov.br/ccivil_03/_ato2019-2022/2020/lei/l14063.htm" target="_blank">Documento com validade jurídica<br/> Lei Nº 14.063 de 23 de setembro de 2020.</a>
                            </p>

                        </div>
                    <?php } ?>

                </div>
                <!-- /middle-wizard -->

                <header id="bottom-wizard" class="botoes_avanca_confirma">
                    <style>
                        .colulna_totalizador_left {
                            color: rgb(16, 0, 79);
                            font-size: 1.3rem;
                        }

                        .colulna_totalizador_right {
                            color: rgb(16, 0, 79);
                            font-weight: bold;
                            font-size: 1.2rem;
                        }

                        .MuiButton-root {
                            background: none rgb(232, 17, 75);
                            border-radius: 0.5rem;
                            font-family: "LATAM Sans", "Trebuchet MS", sans-serif;
                            font-weight: 600;
                            line-height: 1;
                            text-transform: none;
                            transition: none 0s ease 0s;
                            border: 0.0625rem solid rgb(232, 17, 75);
                            color: rgb(255, 255, 255);
                            font-size: 1.125rem;
                            height: 3rem;
                            padding: 0.75rem 2rem;
                            cursor: pointer;
                        }

                        .div_totalizadores_card {
                            margin-top: 10px;
                        }
                    </style>

                    <div style="display: none;" id="div_assentos_marcacao_confirma">
                        <div class="row" style="font-size: 20px;font-weight: 700;margin-bottom: 5px;">
                            <div class="col-12">
                                <button type="button" class="close" onclick="close_marcacao();" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x" style="font-size: 38px;">×</i></button>
                            </div>
                            <div class="col-6 colulna_totalizador_left" id="pop_up_seleciona_assento_name" style="text-align: left;text-transform: uppercase;"></div>
                            <div class="col-6 colulna_totalizador_right" id="pop_up_seleciona_assento_poltrona"></div>

                            <div class="col-6 colulna_totalizador_left div_compra_extra" id="pop_up_seleciona_assento_compra_extra_note"  style="text-align: left;text-transform: initial;color: #dc3545;display: none;">Valor Extra</div>
                            <div class="col-6 colulna_totalizador_right div_compra_extra" id="pop_up_seleciona_assento_compra_extra_valor"  style="color: #dc3545;display: none;"></div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="button" onclick="confirmarAssentoMarcado(this);" style="width: 100%;padding: 12px 0px;" name="backward" class="MuiButton-root">
                                    <span class="fa fa-check"></span> CONFIRMAR ASSENTO
                                </button>
                            </div>
                        </div>
                    </div>

                    <div style="display: none;" id="div_assentos_marcacao_desmarca">
                        <div class="row" style="font-size: 20px;font-weight: 700;margin-bottom: 5px;">
                            <div class="col-12">
                                <button type="button" class="close" onclick="close_desmarcar();" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x" style="font-size: 38px;">×</i></button>
                            </div>
                            <div class="col-6 colulna_totalizador_left" id="pop_down_seleciona_assento_name" style="text-align: left;text-transform: uppercase;"></div>
                            <div class="col-6 colulna_totalizador_right" id="pop_down_seleciona_assento_poltrona"></div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="button" onclick="desmarcarAssento(this);" style="width: 100%;padding: 12px 0px;" name="backward" class="MuiButton-root">
                                    <span class="fa fa-trash-o"></span> REMOVER ASSENTO
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="div_totalizadores_card">

                        <!--sinal !-->
                        <div class="row div-sinal" style="font-size: 13px;font-weight: 700;line-height: 25px;display: none;">
                            <div class="col-8 colulna_totalizador_left" style="text-align: left;"><small><i class="fa fa-money"></i> <span class="nm_sinal">Sinal</span></small></div>
                            <div class="col-4 colulna_totalizador_right"><small><span class="sinalExibir" style="font-weight: bold;"></span></small></div>
                        </div>

                        <!--restante !-->
                        <div class="row div-sinal" style="font-size: 13px;font-weight: 700;line-height: 25px;display: none;">
                            <div class="col-6 colulna_totalizador_left" style="text-align: left;"><small><i class="fa fa-money"></i> <span class="nm_restante">Restante</span></small></div>
                            <div class="col-6 colulna_totalizador_right"><small><span class="restanteExibir" style="font-weight: bold;"></span></small></div>
                        </div>

                        <!--Desconto !-->
                        <div class="row div-taxas" style="font-size: 13px;font-weight: 700;display: none;">
                            <div class="col-6 colulna_totalizador_left" style="text-align: left;"><small><i class="fa fa-plus"></i> Taxa Admin</small></div>
                            <div class="col-6 colulna_totalizador_right"><small style="font-weight: 700;"><span class="taxaExibir" style="color: #dc3545;"></span></small></div>
                        </div>

                        <!--Total !-->
                        <div class="row" style="font-size: 20px;font-weight: 700;margin-bottom: 5px;">
                            <div class="col-6 colulna_totalizador_left" style="text-align: left;"><i class="fa fa-shopping-cart"></i> Total</div>
                            <div class="col-6 colulna_totalizador_right"><span class="subTotalExibir"></span></div>
                        </div>

                        <!--Desconto !-->
                        <div class="row div-cupom-desconto" style="font-size: 13px;font-weight: 700;margin-bottom: 5px;">
                            <div class="col-6 colulna_totalizador_left" style="text-align: left;"><small><i class="fa fa-ticket"></i> Cupom</small></div>
                            <div class="col-6 colulna_totalizador_right"><small style="font-weight: 700;"><span class="cupomDescontoExibir" style="color: #28a745;"></span></small></div>
                        </div>

                        <!--SubTotal !-->
                        <div class="row div-cupom-desconto" style="font-size: 17px;font-weight: 700;margin-bottom: 5px;">
                            <div class="col-6 colulna_totalizador_left" style="text-align: left;"><i class="fa fa-money"></i> Subtotal</div>
                            <div class="col-6 colulna_totalizador_right"><span class="pagamentoTotalExibir" style="color: #dc3545"></span></div>
                        </div>

                        <div class="row" id="botoes-proximo">
                            <div class="col-6">
                                <button type="button" style="width: 100%;padding: 12px 0px;" name="backward" class="backward"><span class="fa fa-arrow-left"></span> PASSO ANTERIOR</button>
                            </div>
                            <div class="col-6">
                                <button type="button" style="width: 100%;padding: 12px 0px;" name="forward" class="forward">PRÓXIMO PASSO <span class="fa fa-arrow-right"></span></button>

                                <?php if($plugsign) { ?>
                                    <button type="submit" style="width: 100%;padding: 12px 0px;background: #f73b00;" name="process" id="enviarInformacoesAoServidor" class="submit"><span class="fa fa-check"></span> ASSINAR</button>
                                <?php } else { ?>
                                    <button type="submit" style="width: 100%;padding: 12px 0px;" name="process" id="enviarInformacoesAoServidor" class="submit"><span class="fa fa-save"></span> CONFIRMAR</button>
                                <?php }?>

                            </div>
                        </div>
                    </div>
                </header>
                <!-- /bottom-wizard -->
            </div>
            <!-- /Wizard container -->
        </div>


    </div>
</div>
<?php echo form_close(); ?>

<div class="cd-overlay-nav"><span></span></div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"><span></span></div>
<!-- /cd-overlay-content -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <style>
                    p {
                        margin-bottom: revert;
                    }

                    strong {
                        font-weight: bold;
                    }
                </style>

                <?php
                $aceistes_contrato_label = explode('@label@', $configuracaoGeral->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <?php echo $labelAceite[0];?>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- Modal terms -->
<div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                    <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang(' Sobre o Pacote') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                    <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                    <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                    <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                    <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-confirm.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?= $assets ?>js/valida_cpf_cnpj.js" type="text/javascript"></script>

<?php if ($configPagSeguro->active) { ?>
    <!-- API PagSeguro is active-->
    <?php if ($configPagSeguro->sandbox == 1) {?>
        <!-- API PagSeguro -->
        <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <?php } else {?>
        <!-- API PagSeguro -->
        <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <?php }?>
<?php } ?>


<?php if ($configMercadoPago->active) { ?>
    <!-- API PagSeguro is active-->
    <script src="https://sdk.mercadopago.com/js/v2"></script>

    <script src="https://www.mercadopago.com/v2/security.js" view="checkout"></script>

<?php } ?>

<!-- Wizard script !-->
<script src="<?php echo base_url() ?>assets/appcompra/js/app_compra_v4.js"></script>


<?php if ($plugsign) {?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.10.377/pdf.min.js"></script>
<?php } ?>

<script type="application/javascript">

    var site = <?=json_encode(array('base_url' => base_url(), 'settings' => $configuracaoGeral, 'dateFormats' => $dateFormats))?>

    var g_iCount        = new Number();
    var g_iCount        = 600 + 1;
    var tempoMinutos    = 10;
    var segundos        = 60;
    var minutos         = tempoMinutos - 1;

    var cliente_selecionado_assento = null;
    var assento_selecionado_tag = null;
    var tipoHospedagemSelected = null;

    var j_passageiros_selecionados_assento = [];
    var dependentes = [];

    function contagem(){

        var prg         = document.getElementById('progresso-da-barra');
        var progresso   = document.getElementById('tempo-sessao');

        if((g_iCount - 1) >= 0){

            g_iCount = g_iCount - 1;

            tempo_restante = g_iCount / 6;
            prg.style.width = tempo_restante + '%';

            segundos = segundos - 1;

            if (minutos < 0) {
                progresso.innerHTML = '<span class="fa fa-spin fa-clock-o"></span> TEMPO DE SESSÃO 0m 0s';
            } else {

                if (segundos < 0) {
                    minutos = minutos - 1;
                    segundos = 59;
                    progresso.innerHTML = '<span class="fa fa-spin fa-clock-o"></span> TEMPO DE SESSÃO '+minutos + "m " + segundos + "s";
                }
                progresso.innerHTML = '<span class="fa fa-spin fa-clock-o"></span> TEMPO DE SESSÃO '+minutos + "m " + segundos + "s";
            }

            if (g_iCount === 0) {
                $.confirm({
                    title: 'Atençao!',
                    type: 'red',
                    typeAnimated: true,
                    content: 'Sua sessão está preste a expirar!<br/><small>Os dados informados até agora serão perdidos.<br/>Deseja renovar o tempo desta sessão?</small>',
                    buttons: {
                        continuar: function () {
                            g_iCount = 600 + 1;
                            minutos = tempoMinutos - 1;
                            bloqueio_assento_site();
                            setTimeout('contagem()',1000);
                        },
                        sair: function () {
                            window.location = '<?php echo $this->Settings->url_site_domain.'/carrinho_compra/'.$programacao->id.'/'.$vendedor->id; ?>';
                        },
                    }
                });
            } else {
                setTimeout('contagem()',1000);
            }
        }
    }

    setInterval(function (event) {
        consultaVagasEmTempoReal();
    }, 120000);//de 30 em 30 segundos consulta o numero de vagas

    jQuery(document).ready(function() {

        jQuery.extend(jQuery.validator.messages, {
            required: "Campo é obrigatório.",
            email: "Por favor insira um endereço de e-mail válido.",
            date: "Please enter a valid date.",
            number: "Por favor insira um número válido.",
            digits: "Por favor insira um número válido.",
            maxlength: jQuery.validator.format("Não insira mais de {0} caracteres."),
            minlength: jQuery.validator.format("Insira pelo menos {0} caracteres."),
        });

        $('#celular').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('.celular').keyup(function (event){
            mascaraTelefone( this, mtel );
        });

        $('#cpfTitular').keyup(function (event){
            mascaraCpf( this, formataCPF );
        });

        $('#cpf_titular_valepay').keyup(function (event){
            mascaraCpf( this, formataCPF );
        });

        $('#telTitular').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('#tel_titular_valepay').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('.fvDependentes').change(function(event){

            calcularTotalPagar(true);
            exibirBotaoProximo();

            consultaVagasEmTempoReal();

            if ($('input[name="tipoCobranca"]:checked').val() !== undefined) {
                getCondicoesDePagamento();
                getParcelamento();
            }
        });

        $('#email').change(function(event) {
            $('#form-checkout__cardholderEmail').val($('#email').val());
        });

        $('#ck-titular-cartao').click(function (){

            if ($('#div-titular-cartao-pagseguro').is(':visible')) {
                $('#div-titular-cartao-pagseguro').hide();

                $('#cpfTitular').removeAttr('required');
                $('#telTitular').removeAttr('required');
            } else {
                $('#div-titular-cartao-pagseguro').show();

                $('#cpfTitular').attr('required', 'required');
                $('#telTitular').attr('required', 'required');
            }
        });

        $('#ck-titular-cartao-valepay').click(function() {
            if ($('#div-titular-cartao-valepay').is(':visible')) {
                $('#div-titular-cartao-valepay').hide();

                $('#cpf_titular_valepay').removeAttr('required');
                $('#tel_titular_valepay').removeAttr('required');
            } else {
                $('#div-titular-cartao-valepay').show();

                $('#cpf_titular_valepay').attr('required', 'required');
                $('#tel_titular_valepay').attr('required', 'required');
            }
        });

        $('#ck-titular-cartao-mercadopago').click(function (){

            if ($('#div-titular-cartao-mercadopago').is(':visible')) {
                $('#div-titular-cartao-mercadopago').hide();

                $('#form-checkout__identificationNumber').removeAttr('required');
                $('#form-checkout__cardholderEmail').removeAttr('required');
            } else {
                $('#div-titular-cartao-mercadopago').show();

                $('#form-checkout__identificationNumber').attr('required', 'required');
                $('#form-checkout__cardholderEmail').attr('required', 'required');
            }
        });

        $('#enviarInformacoesAoServidor').click(function (event){

            event.preventDefault();

            if ($('#wrapped').valid()) {

                <?php if($plugsign) { ?>
                    $.blockUI(
                        {
                            message: '<h3><img style="width: 150px;height: 150px;"  src="<?php echo base_url() ?>assets/images/signing.gif" /></h3>',
                            css: {width: '70%', left: '15%'}
                        }
                    );
                <?php } else {?>
                    $.blockUI(
                        {
                            message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                            css: {width: '70%', left: '15%'}
                        }
                    );
                <?php } ?>

                <?php if ($configMercadoPago->active) { ?>

                if ($('input[name="tipoCobranca"]:checked').attr('tipo')        === 'carne_cartao_transparent_mercado_pago' ||
                    $('input[name="tipoCobrancaSinal"]:checked').attr('tipo')   === 'carne_cartao_transparent_mercado_pago') {//TODO GERA TOKEN PARA CARTAO ANTES DE ENVIAR O FORMULARIO

                    preencherInformacoesTitularCartaoMercadoPago();

                    cardForm.createCardToken().then(function () {

                        $("input[name='senderHash']").val(cardForm.getCardFormData().token);
                        $("input[name='creditCardToken']").val(cardForm.getCardFormData().token);
                        $("input[name='nameCartaoPagSeguro']").val(cardForm.getCardFormData().paymentMethodId + '#' + cardForm.getCardFormData().issuerId);

                        <?php if($plugsign) { ?>
                            if (confirm('ASSINAR CONTRATO?')) {
                                $('#wrapped').submit();
                            } else {
                                $.unblockUI();
                            }
                        <?php } else { ?>
                            $('#wrapped').submit();
                        <?php  } ?>

                    }, function (errors) {
                        let erros = 'Encontramos um erro na digitação dos dados do cartão: ';
                        $(errors).each(function (indexd, error) {
                            erros += "(" + error.code +  ") " + error.message;
                        });
                        $.unblockUI();
                        alert(erros);
                    });
                } else {
                    <?php if($plugsign) { ?>
                        if (confirm('ASSINAR CONTRATO?')) {
                            $('#wrapped').submit();
                        } else {
                            $.unblockUI();
                        }
                    <?php } else { ?>
                        $('#wrapped').submit();
                    <?php  } ?>
                }

                <?php } else { ?>
                    <?php if($plugsign) { ?>
                        if (confirm('ASSINAR CONTRATO?')) {
                            $('#wrapped').submit();
                        } else {
                            $.unblockUI();
                        }
                    <?php } else { ?>
                        $('#wrapped').submit();
                    <?php  } ?>
                <?php } ?>
            } else {
                if (!$('input[name="terms"]').is(':checked')) {
                    window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' });
                }
            }
        });

        $('#inserir-cupom').click(function(){
            $('#div_cupom').show(500);
        });

        $('#cancelar-cupom').click(function() {
            $('#div_cupom').hide(100);
            $('#cupom').val('');
            $('#validade-cupom').html('');
            $('#valor_cupom_desconto').val('0');
            $('#tipo_cupom_desconto').val('');

            calcularTotalPagar(false);
        }) ;

        $('.responsavel_compra').click(function () {

            removerObrigatoriedadeInformacoesContato();

            let sequencial = $(this).attr('sequencial');
            let obrigaCPF = $('#obrigaCPF'+sequencial).val();
            let obrigaEmail = $('#obrigaEmail'+sequencial).val();
            let faixaDependente = $('#faixaDependente'+sequencial).val();

            if (obrigaCPF === '1') {
                $('#cpfDependente'+sequencial).attr('required', true);
            }

            if (obrigaEmail === '1') {
                $('#emailDependente'+sequencial).attr('required', true);
            }

            if (faixaDependente === '1') {
                $('#emailDependente'+sequencial).attr('required', false);
                $('#celularDependente'+sequencial).attr('required', false);
            } else {
                $('#celularDependente'+sequencial).attr('required', true);
            }

            $('.responsavel_compra').each(function( index, tag ) {
                let sequencialSelect = $(this).attr('sequencial');
                if (sequencialSelect !== sequencial)  tag.value = 'NAO';
            });

        });

        function removerObrigatoriedadeInformacoesContato() {
            $('.responsavel_compra').each(function( index ) {
                let sequencial = $(this).attr('sequencial');
                //$('#celularDependente'+sequencial).attr('required', false);
                //$('#emailDependente'+sequencial).attr('required', false);
                //$('#cpfDependente'+sequencial).attr('required', false);
            });
        }

        $('#cupom-consulta').click(function() {
           consultarCupom();
        });

        $('#cupom').keypress(function(event) {
            if(event.which === 13) {
                consultarCupom();
            }
        });

        $('#cupom').change(function() {
            consultarCupom();
        });

        $('#condicaoPagamento').change(function (e){

            let totalAmount = $( '#condicaoPagamento :selected' ).attr('totalAmount');

            calcularTotalPagar(false);

            if ($('input[name="tipoCobranca"]:checked').attr('tipo') === 'cartao_credito_transparent_valepay') {
                popular_table_parcelamento_valepay();//TODO CALCULA A TAXA LOGO APOS CALCULAR A TAXA
            }

            if ($('input[name="tipoCobrancaSinal"]:checked').attr('tipo') === 'cartao_credito_transparent_valepay') {
                popular_table_parcelamento_valepay();//TODO CALCULA A TAXA LOGO APOS CALCULAR A TAXA
            }
        });

        <?php if ($configPagSeguro->active) { ?>
        setInterval(function (event){
            if ($("input[name='senderHash']").val() === '') {
                carregarPagSeguro();
            }
        },1000);
        <?php } ?>

        $('select[name="tipo_documentoDependente[]"]').change(function (event){

            let id = $(this).attr('contador');
            let obrigaOrgaoEmissor = $('#obrigaOrgaoEmissor'+id).val();

            if ($(this).val() === 'rg') {
                $('#div_orgao_emissor' + id).show();
                $('#rgDependente' + id).attr('placeholder', 'R.G');

                if (obrigaOrgaoEmissor === '1') {
                    $('#orgaoEmissorDependente' + id).attr("required", true);
                }

            } else {
                $('#div_orgao_emissor' + id).hide();
                $('#orgaoEmissorDependente' + id).removeAttr("required");
                $('#rgDependente' + id).attr('placeholder', $(this).val().toUpperCase());
            }
        });

        $('input[name="cpfDependentePagador[]"]').on('input', function () {

            mascaraCpf(this, formataCPF);

            let tipo = $(this).attr('tipo');
            let cpfValue = this.value;
            let tamanhoCPF = cpfValue.length

            if (cpfValue === '' || tamanhoCPF < 11) {
                return false;
            }

            if (!valida_cpf(cpfValue)) {

                alert('CPF inválido!');

                this.value = '';
                this.focus();

                return false;
            } else {
                preencherDadosDoDependentePagador(cpfValue, this);
            }
        });

        $('input[name="cpfDependentePagador[]"]').on('blur', function() {
            if ($(this).val().length < 11) {
                $(this).val('');
            }
        });

        $('input[name="cpfDependente[]"]').on('input', function () {

            let tipo = $(this).attr('tipo');

            if (tipo === 'CPF') {
                formatarCPF(this);
            } else if (tipo === 'CNPJ') {
                formatarCNPJ(this);
            }
        });

        $('input[name="cpfDependente[]"]').on('blur', function() {

            let tipo = $(this).attr('tipo');

            if (tipo === 'CPF') {
                if ($(this).val().length < 11) {
                    $(this).val('');
                }
            } else if (tipo === 'CNPJ') {
                if ($(this).val().length < 18) {
                    $(this).val('');
                }
            }
        });

        $('input[name="cpfTitular"]').keyup(function (event) {

            mascaraCpf( this, formataCPF );

            if (this.value.length === 11) {

                if (!valida_cpf(this.value)) {

                    alert('CPF inválido!');

                    this.value = '';
                    this.focus();

                    return false;
                }
            }
        });

        $('input[name="cpf_titular_valepay"]').keyup(function (event) {

            mascaraCpf( this, formataCPF );

            if (this.value.length === 11) {

                if (!valida_cpf(this.value)) {

                    alert('CPF inválido!');

                    this.value = '';
                    this.focus();

                    return false;
                }
            }
        });

        $('#form-checkout__cardholderName').on('keyup', (ev) => {
            $('#form-checkout__cardholderName').val($('#form-checkout__cardholderName').val().toUpperCase());
        });

        $('#nomeTitularCartaoPagSeguro').on('keyup', (ev) => {
            $('#nomeTitularCartaoPagSeguro').val($('#nomeTitularCartaoPagSeguro').val().toUpperCase());
        });

        $('#name_card_valepay').on('keyup', (ev) => {
            $('#name_card_valepay').val($('#name_card_valepay').val().toUpperCase());
        });

        $( "#cepTitular" ).blur(function() {
            mascara(this, mcep );
            getConsultaCEPTitular();
        });

        $( "#cepTitular" ).keyup(function() {
            mascara(this, mcep );
            getConsultaCEPTitular();
        });

        $( "#cep_titular_valepay" ).blur(function() {
            mascara(this, mcep );
            getConsultaCEPTitularValePay();
        });

        $( "#cep_titular_valepay" ).keyup(function() {
            mascara(this, mcep );
            getConsultaCEPTitularValePay();
        });

        $( "#cep" ).blur(function() {
            mascara(this, mcep );
            getConsultaCEP();
        });

        $( "#cep" ).keyup(function() {
            mascara(this, mcep );
            getConsultaCEP();
        });

        $('.nomesDependente').keyup(function(){
            let sequencial = $(this).attr('sequencial');

            let name = $(this).val();

            $(this).val(name);

            $('#div_name_'+sequencial).html(name);

            if ($("#assento_dependente"+sequencial).is("*")) $('#assento_dependente'+sequencial).html(name);

        });

        $('#cupom').on('keyup', (ev) => {
            let codigoDigitado = $('#cupom').val().toUpperCase();
            codigoDigitado = codigoDigitado.split(' ').join('');
            $('#cupom').val(codigoDigitado);
        });

        desabilitarCartaoDeCreditoPagSeguro();
        desabilitarCartaoDeCreditoMercadoPago();
        desabilitarCartaoDeCreditoValePay();

        desabilitarCondicaoPagamento();

        contagem();//todo inicia contagem
        consultaVagasEmTempoReal();
        calcularTotalPagar(false);

        $('#preloader').hide();
        $('.forward').hide();

        <?php if($produto->apenas_cotacao) {?>
            $('#step_tipo_cobranca').removeClass('wizard-step');
            $('#step_tipo_cobranca').removeClass('step');

            $('#step_tipo_cobranca_sinal').removeClass('wizard-step');
            $('#step_tipo_cobranca_sinal').removeClass('step');
        <?php } else { ?>
            $('#step_tipo_cobranca').addClass('wizard-step');
            $('#step_tipo_cobranca').addClass('step');

            $('#step_tipo_cobranca_sinal').addClass('wizard-step');
            $('#step_tipo_cobranca_sinal').addClass('step');
        <?php } ?>

        desabilitarCartaoDeCreditoPagSeguro()
        desabilitarCartaoDeCreditoMercadoPago();
        desabilitarCartaoDeCreditoValePay();

        <?php if(coletarPagador($produto)) {?>
            habilitarCamposObrigatoriosFormularioPagador('<?=$this->Settings->faixa_pagador?>', 'pagador');
        <?php }?>

        <?php if ($isSelecionarCompradorAutomatico) {?>
            $(".fvDependentes").val('1').trigger('change');
            $(".forward").click();
            $('.cc_fieldset').eq(1).children().first().click();
            document.getElementsByClassName('page_header')[0].scrollIntoView();
        <?php } else { ?>
            document.getElementsByClassName('page_header')[0].scrollIntoView();
        <?php } ?>

        /*
        $("html, body").animate({
            scrollTop: $(
                'html, body').get(0).scrollHeight
        });*/
    });

    <?php if(!empty($faixaEtariaValores)){ ?>
        <?php foreach ($faixaEtariaValores as $fv) { ?>
            <?php for ($cr = 0; $cr <= $configuracaoGeral->qty_client_records; $cr++) { ?>
                dependentes.push({ faixaId: '<?php echo $fv->id ?>', sequencial: <?= $cr ?>, tipoHospedagem: '' });
            <?php } ?>
            <?php } ?>
            <?php } else if (!empty($tiposHospedagem)) { ?>
            <?php foreach ($tiposHospedagem as $tipoHospedagem) {
            $ativo = $tipoHospedagem->status == 'ATIVO'; ?>
            <?php if ($ativo) {
            $faixaEtariaValoresHospedagem = $this->ProdutoRepository_model->getValoresPorFaixaEtariaTipoHospedagemByTipoHopedagem($programacao->produto, $tipoHospedagem->id); ?>
            <?php if (!empty($faixaEtariaValoresHospedagem)) {
                foreach ($faixaEtariaValoresHospedagem as $fv) { ?>
                <?php for ($cr = 0; $cr <= $configuracaoGeral->qty_client_records; $cr++) { ?>
                dependentes.push({ faixaId: '<?php echo $fv->id ?>', sequencial: <?= $cr ?>, tipoHospedagem: '<?php echo $tipoHospedagem->id ?>' });
                <?php } ?>
                <?php } ?>
            <?php } ?>
            <?php } ?>
        <?php } ?>
    <?php } ?>

    function limparDadosOnibusSelecaoAssento() {

        j_passageiros_selecionados_assento  = [];
        cliente_selecionado_assento         = null;
        assento_selecionado_tag             = null;

        $('.combo_assento ').empty();
        $('#client_sel_assentos').html("");

        desbloquear_todos_assentos_session_card();
        carregar_onibus();
    }

    function removerAllWizardDependentes(dependentes) {

        let tipoHospedagemSelectedAtual = document.querySelector('input[name="tipoHospedagem"]:checked')?.value;

        limparDadosOnibusSelecaoAssento();

        dependentes.forEach(dep => {
            removerWizardDependentes(dep.faixaId, dep.sequencial, dep.tipoHospedagem, tipoHospedagemSelectedAtual);
        });

        tipoHospedagemSelected = $('input[name="tipoHospedagem"]:checked').val();
    }


    /* Máscaras ER */
    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2");
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");
        return v;
    }

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCPF(cpf){
        cpf = cpf.replace(/[^\d]/g, "");
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function mascaraCnpj(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracnpj()",1)
    }

    function execmascaracnpj(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCNPJ(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{2})(\d)/,"$1.$2");
        v=v.replace(/^(\d{2})\.(\d{3})(\d)/,"$1.$2.$3");
        v=v.replace(/\.(\d{3})(\d)/,".$1/$2");
        v=v.replace(/(\d{4})(\d)/,"$1-$2");
        return v;
    }

    function exibirFaixasValorHospedagem(tipoHospedagemId) {

        $('.fvDependentes').val(0)
        $('.fvhHospedagem').hide();
        $('.'+tipoHospedagemId).show(300);

        exibirBotaoProximo();
        consultaVagasEmTempoReal();

        if ($('input[name="tipoCobranca"]:checked').val() !== undefined) {
            getCondicoesDePagamento();
            getParcelamento();
        }
    }

    function calcularTotalPagar(atualizarDependentes) {

        let totalVF         = 0;
        let totalAdicionais = calcularTotalPagarAdicionais();

        $('.budget_slider_adicionais_add').hide();
        $('.div-sinal').hide();
        $('.div-taxas').hide();

        $('#preview_tb_adicional').removeClass('wizard-step');
        $('#preview_tb_adicional').removeClass('step');

        $('.nm_sinal').html('');

        //zerar sinal
        $('#valorSinal').val(0);
        $('#tipoCobrancaSinal').val('');
        $('#contaMovimentadorSinal').val('');

        $('.fvDependentes').each(function() {
            let faixaId =  $(this).attr('id');
            let qtd = getDouble($("#"+ faixaId +" option:selected" ).val());
            let valorFaixa = getDouble($("#"+ faixaId +" option:selected" ).attr('valor'));

            totalVF = totalVF + (valorFaixa*qtd);
        });

        let valorTotal          = totalVF + totalAdicionais;

        let tipoCupomDesconto   = $('#tipo_cupom_desconto').val();
        let valorCupom          = getDouble($('#valor_cupom_desconto').val());
        let valorDescontoCupom  = 0;
        let valor_extra         = 0;
        var valorSinal          = 0;
        var valorExtraSinal     = 0;

        $('input[name="valor_extra_assento[]"]').each(function(index) {
            var valor = $(this).val();
            if (!isNaN(valor) && parseFloat(valor) > 0) {
                valor_extra = + parseFloat(valor);
            }
        });

        valorTotal          = valorTotal + valor_extra;

        let subTotalTaxas   = getDouble($( '#condicaoPagamento :selected' ).attr('totalAmount'));
        let sinal           = getDouble($('input[name="tipoCobrancaSinal"]:checked').attr('sinal'));

        if (sinal > 0) {

            let valor_acres_desc_sinal          = getDouble($('input[name="tipoCobrancaSinal"]:checked').attr('valor_acres_desc_sinal'));
            let tp_sinal_id                     = $('input[name="tipoCobrancaSinal"]:checked').val();
            let tipo_sinal                      = $('input[name="tipoCobrancaSinal"]:checked').attr('tipo_sinal');
            let acrescimo_desconto_sinal        = $('input[name="tipoCobrancaSinal"]:checked').attr('acrescimo_desconto_sinal');
            let tipo_acres_desc_sinal           = $('input[name="tipoCobrancaSinal"]:checked').attr('tipo_acres_desc_sinal');
            let nome_cobranca                   = $('input[name="tipoCobrancaSinal"]:checked').attr('nome_cobranca');
            let conta_sinal_id                  = $('input[name="tipoCobrancaSinal"]:checked').attr('conta');

            if (tipo_sinal === 'percentual') {
                valorSinal = (sinal/100) * valorTotal;
            } else  {
                valorSinal = sinal;
            }

            if (valor_acres_desc_sinal > 0) {
                if (tipo_acres_desc_sinal === 'percentual') {
                    valorExtraSinal = (valor_acres_desc_sinal/100) * valorSinal;
                } else  {
                    valorExtraSinal = valor_acres_desc_sinal;
                }

                if (acrescimo_desconto_sinal === 'acrescimo') {
                    valorSinal = valorSinal + valorExtraSinal;
                } else {
                    valorSinal = valorSinal - valorExtraSinal;
                }

                valorTotal = valorTotal + valorExtraSinal;
            }

            $('.sinalExibir').html('R$'+formatMoney(valorSinal));
            $('.restanteExibir').html('R$'+formatMoney(valorTotal - valorSinal));

            $('#valorSinal').val(valorSinal);
            $('#tipoCobrancaSinal').val(tp_sinal_id);
            $('#contaMovimentadorSinal').val(conta_sinal_id);

            $('.nm_sinal').html('Sinal no ' + nome_cobranca);
            $('.div-sinal').show();

            if (subTotalTaxas > 0) {
                subTotalTaxas = subTotalTaxas + valorSinal;
            }
        }

        $('#subTotal').val(valorTotal);

        let taxas = subTotalTaxas - valorTotal;

        if (subTotalTaxas === 0 && taxas < 0) {
            taxas = 0;
        }

        let valorTotalComTaxa = valorTotal + taxas;

        if ($('#cupom').val() !== '' && valorCupom > 0) {
            if (tipoCupomDesconto === 'PERCENTUAL') {
                valorDescontoCupom = (valorCupom/100) * valorTotalComTaxa;
            } else  {
                valorDescontoCupom = valorCupom;
            }
        }

        let valorTotalPagar = valorTotalComTaxa - valorDescontoCupom;

        $('#subTotalTaxas').val(valorTotalPagar);//TOTAL PAGAR

        if (valorDescontoCupom > 0){
            $('.subTotalExibir').html('R$'+formatMoney(valorTotalComTaxa));
            $('.cupomDescontoExibir').html('-R$'+formatMoney(valorDescontoCupom));
            $('.pagamentoTotalExibir').html('R$'+formatMoney(valorTotalPagar));
            $('.div-cupom-desconto').show();
        } else {
            $('.subTotalExibir').html('R$'+formatMoney(valorTotalPagar));
            $('.div-cupom-desconto').hide();
        }

        if (taxas > 0) {
            $('.taxaExibir').html('+R$'+formatMoney(taxas));
            <?php if ($configuracaoGeral->mostrar_taxas){?>
                $('.div-taxas').show();
            <?php } ?>
        }

        recriarFormularioMercadoPago();

        if (atualizarDependentes) {

            removerAllWizardDependentes(dependentes);

            limparComboxAdicionais();

            let selecao_pagador = false;

            $('.fvDependentes').each(function() {

                let valorFaixaId    =  $(this).attr('id');
                let faixaId         = $(this).attr('faixaId');
                let tipoHospedagem  = $(this).attr('tipoHospedagem') != null ?  $(this).attr('tipoHospedagem') : '';

                let qtd = getDouble($("#"+ valorFaixaId +" option:selected" ).val());

                for (let i = 0; i < qtd; i++) {

                    let seq = (i+1);

                    adiconarWizardDependentes(valorFaixaId, seq, tipoHospedagem);

                    if (!selecao_pagador) {

                        let sequencial_id = seq;

                        if (tipoHospedagem !== '') {
                            sequencial_id = sequencial_id + '_' +tipoHospedagem;
                        }

                        let tagResponsavelPelaCompra = $('#responsavelPelaCompra' + valorFaixaId + '_' + sequencial_id);

                        tagResponsavelPelaCompra.val('SIM');
                        tagResponsavelPelaCompra.click();

                        selecao_pagador = true;
                    }
                }

                if (qtd > 0) {
                    adicionarOpcionais(faixaId, qtd);
                }
            });

            onlick_events_selecionar_cliente_assento();
        }

        getParcelamento();
    }

    function pagamentoSinal() {

        let com_sinal = $('input[name="pagar_com_sinal"]:checked').val();

        if (com_sinal === '1') {
            $('.div_pagamento_sinal').show();
            $('#title_tipo_cobranca').html('Como você prefere pagar o restante do valor?');
        } else if (com_sinal === '2') {
            $('input[name="tipoCobrancaSinal"]').prop('checked', false);
            $('input[name="tipoCobranca"]').prop('checked', false);
            $('#condicaoPagamento').empty();
            $('.class_tipo_cobranca').show();
            $('.div_pagamento_sinal').hide();
            $('#title_tipo_cobranca').html('Como você prefere pagar?');

            getCondicoesDePagamento();
            calcularTotalPagar(false);
            document.getElementsByClassName('page_header')[0].scrollIntoView();
        } else {
            $('input[name="tipoCobrancaSinal"]').prop('checked', false);
            $('input[name="tipoCobranca"]').prop('checked', false);
            $('#condicaoPagamento').empty();
            $('.class_tipo_cobranca').show();
            $('.div_pagamento_sinal').hide();
            $('#title_tipo_cobranca').html('Como você prefere pagar?');

            getCondicoesDePagamento();
            calcularTotalPagar(false);
            document.getElementsByClassName('page_header')[0].scrollIntoView();
        }
    }

    function limparComboxAdicionais() {
        $(".adicionais").empty();
        $(".adicionais").each(function() {
            $(this).append($('<option>', {
                value: 0,
                text: 0
            }));
        });
    }
    function adicionarOpcionais(faixaId, qtd) {
        $(".qtdAdicionais" + faixaId).each(function() {
            for(let i=1; i<=qtd;i++) {
                $(this).append($('<option>', {
                    value: i,
                    text: i
                }));
            }
        });
    }

    function calcularTotalPagarAdicionais() {

        var totalAdicional = 0;

        $(".adicionais").each(function () {

            let valorAdicional = getDouble($(this).attr('valor'));
            let qtd = getDouble($(this).val());
            let opcional = $(this).attr('opcional');

            totalAdicional = totalAdicional + (valorAdicional * qtd);

            $('#preview-qtdcriancas' + opcional).html(qtd);
            $('#preview-pricecriancas' + opcional).html('R$ ' + (valorAdicional * qtd));

            if (qtd > 0) {
                $('#item_adicional_preview_add' + opcional).show();
                $('#preview_tb_adicional').addClass('wizard-step');
                $('#preview_tb_adicional').addClass('step');
            }
        });

        return totalAdicional;
    }

    function removerWizardDependentes(faixaId, sequencial, tipoHospedagem, tipoHospedagemSelectedAtual) {

        if (tipoHospedagem !== '') {
            sequencial = sequencial + '_' +tipoHospedagem;
        }

        let id = faixaId+'_'+sequencial;

        $('#dependente' + id).hide();

        desabilitarCamposObrigatoriosFormularioCliente(faixaId, sequencial);

        limparCamposDependentes(id, faixaId, tipoHospedagemSelectedAtual);
    }

    function adiconarWizardDependentes(faixaId, sequencial, tipoHospedagem) {

        let sequencial_id = sequencial;

        if (tipoHospedagem !== '') {
            sequencial_id = sequencial_id + '_' +tipoHospedagem;
        }

        let id = 'dependente'+faixaId+'_'+sequencial_id;

        $('#'+id).show();

        habilitarCamposObrigatoriosFormularioClientes(faixaId, sequencial_id);

        $('.combo_assento').attr('required', false);

        let descontarVaga   =  $('#descontarVaga' + faixaId+'_'+sequencial_id).val();

        if (descontarVaga === '1') {
            if ($("#cc_assento_" + id).length === 0) {

                let nome_completo_cliente  = $('#nomeDependente' + + faixaId+'_'+sequencial_id).val();

                //criar campos para incluir a poltrona
                let html_passageiro_sel_assento = '' +
                    '<fieldset class="cc_fieldset cc_assentos cc_assentos_opaco" id="cc_assento_' + id + '" sel="' + id + '" > ' +
                    '   <div style="padding: 5px;"> ' +
                    '       <div style="display: flex;text-align: left;"> ' +
                    '           <div class="col-12"> ' +
                    '               <div class="assento_selecionado" style="float: left;"> ' +
                    '                   <select required="required" class="combo_assento combo2" id="select_assento_selecionado' + id + '"></select> ' +
                    '               </div> ' +
                    '               <div class="assento_selecionado_name" style="float:right;"> ' +
                    '                   <div class="assento_selecionado_name_inner" id="assento_' + id +'">' + nome_completo_cliente +'</div> ' +
                    '               </div> ' +
                    '           </div>' +
                    '       </div> ' +
                    '   </div> ' +
                    '</fieldset>';
                $('#client_sel_assentos').append(html_passageiro_sel_assento);
            } else {
                $('#select_assento_selecionado' + id).attr('required', true);
                $('#cc_assento_' + id).show();
            }
        }
    }

    function habilitarCamposObrigatoriosFormularioPagador(faixaId, sequencial) {

        let cpfObrigatorio = $('#obrigaCPF'+faixaId+'_'+sequencial).val();
        let obrigaOrgaoEmissor = $('#obrigaOrgaoEmissor'+faixaId+'_'+sequencial).val();
        let obrigaDataNascimento = $('#obrigaDataNascimento'+faixaId+'_'+sequencial).val();
        let obrigaDocumento = $('#obrigaDocumento'+faixaId+'_'+sequencial).val();
        let tipo_documento = $('#tipo_documentoDependente' + +faixaId+'_'+sequencial).val();

        $('#nomeDependente'+faixaId+'_'+sequencial).attr('required', true);

        if (cpfObrigatorio === '1') {
            $('#cpfDependente'+faixaId+'_'+sequencial).attr('required', true);
        }

        if (obrigaDocumento === '1') {

            $('#rgDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#tipo_documentoDependente' + faixaId + '_' + sequencial).attr('required', true);

            if (obrigaOrgaoEmissor === '1') {
                if (tipo_documento === 'rg') {
                    $('#orgaoEmissorDependente'+faixaId+'_'+sequencial).attr('required', true);
                }
            }
        }

        if (obrigaDataNascimento === '1') {
            $('#diaDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#mesDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#anoDependente' + faixaId + '_' + sequencial).attr('required', true);
        }

        $('#sexoDependente'+faixaId+'_'+sequencial).attr('required', true);
    }

    function habilitarCamposObrigatoriosFormularioClientes(faixaId, sequencial) {

        let cpfObrigatorio = $('#obrigaCPF'+faixaId+'_'+sequencial).val();
        let obrigaOrgaoEmissor = $('#obrigaOrgaoEmissor'+faixaId+'_'+sequencial).val();
        let obrigaDataNascimento = $('#obrigaDataNascimento'+faixaId+'_'+sequencial).val();
        let obrigaDocumento = $('#obrigaDocumento'+faixaId+'_'+sequencial).val();
        let tipo_documento = $('#tipo_documentoDependente' + +faixaId+'_'+sequencial).val();

        $('#nomeDependente'+faixaId+'_'+sequencial).attr('required', true);

        if (cpfObrigatorio === '1') {
            $('#cpfDependente'+faixaId+'_'+sequencial).attr('required', true);
        }

        if (obrigaDocumento === '1') {

            $('#rgDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#tipo_documentoDependente' + faixaId + '_' + sequencial).attr('required', true);

            if (obrigaOrgaoEmissor === '1') {
                if (tipo_documento === 'rg') {
                    $('#orgaoEmissorDependente'+faixaId+'_'+sequencial).attr('required', true);
                }
            }
        }

        if (obrigaDataNascimento === '1') {
            $('#diaDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#mesDependente' + faixaId + '_' + sequencial).attr('required', true);
            $('#anoDependente' + faixaId + '_' + sequencial).attr('required', true);
        }

        $('#sexoDependente'+faixaId+'_'+sequencial).attr('required', true);
    }

    function desabilitarCamposObrigatoriosFormularioCliente(faixaId, sequencial) {
        $('#nomeDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#rgDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#tipo_documentoDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#diaDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#mesDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#anoDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#sexoDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#cpfDependente'+faixaId+'_'+sequencial).attr('required', false);
        $('#orgaoEmissorDependente'+faixaId+'_'+sequencial).attr('required', false);
    }

    function desabilitarCartaoDeCreditoPagSeguro() {
        $('#cartao-credito').removeClass('wizard-step');
        $('#cartao-credito').removeClass('step');
    }

    function habilitarCartaoDeCreditoPagSeguro() {
        $('#cartao-credito').addClass('wizard-step');
        $('#cartao-credito').addClass('step');
    }

    function desabilitarCartaoDeCreditoMercadoPago() {
        $('#cartao-credito_mercadopago').removeClass('wizard-step');
        $('#cartao-credito_mercadopago').removeClass('step');
    }

    function desabilitarCartaoDeCreditoValePay() {
        $('#cartao-credito_valepay').removeClass('wizard-step');
        $('#cartao-credito_valepay').removeClass('step');
    }

    function habilitarCartaoDeCreditoMercadoPago() {
        $('#cartao-credito_mercadopago').addClass('wizard-step');
        $('#cartao-credito_mercadopago').addClass('step');
    }

    function habilitarCartaoDeCreditoValePay() {
        $('#cartao-credito_valepay').addClass('wizard-step');
        $('#cartao-credito_valepay').addClass('step');
    }

    function desabilitarCondicaoPagamento() {
        $('#condicoes-pagamento').removeClass('wizard-step');
        $('#condicoes-pagamento').removeClass('step');
    }

    function habilitarCondicaoPagamento() {
        $('#condicoes-pagamento').addClass('wizard-step');
        $('#condicoes-pagamento').addClass('step');
    }

    function limparCamposDependentes(id, faixaId, tipoHospedagemSelectedAtual) {

        let faixaElement = document.getElementById(faixaId);
        let qtdAtualPasageiros = getDouble(document.getElementById('totalPassageiros').value) || 0;
        let qtdSelecionada = faixaElement ? getDouble(faixaElement.options[faixaElement.selectedIndex].value) || 0 : 0;

         let dependenteFields = [
             '#cpfDependente' + id,
             '#nomeDependente' + id,
             '#company_name' + id,
             '#nome_responsavel' + id,
             '#rgDependente' + id,
             '#orgaoEmissorDependente' + id,
             '#diaDependente' + id,
             '#mesDependente' + id,
             '#anoDependente' + id,
             '#celularDependente' + id,
             '#telefone_emergenciaDependente' + id,
             '#emailDependente' + id,
             '#profissao' + id,
             '#doenca_informar' + id,
             '#social_name' + id,
             '#assento' + id,
             '#valor_extra_assento' + id
         ];

         if (tipoHospedagemSelectedAtual !== tipoHospedagemSelected || qtdSelecionada < qtdAtualPasageiros) {
             dependenteFields.forEach(selector => $(selector).val(''));
             $('#div_name_' + id).html($('#div_name_' + id).attr('nome_faixa'));
             if ($("#assento_dependente" + id).length) {
                 $('#assento_dependente' + id).html('');
             }
             $('.responsavel_compra').val('NAO');
         }

     }

     function formatarCPF(tag) {

         mascaraCpf(tag, formataCPF);

         let tipo        = $(tag).attr('tipo');
         let cpfValue    = tag.value;
         let tamanhoCPF  = cpfValue.length

         if (cpfValue === '' || tamanhoCPF < 11) {
             return false;
         }

         if (!valida_cpf(cpfValue)) {

             alert('CPF inválido!');

             tag.value = '';
             tag.focus();

             return false;
         } else {
             preencherDadosDoDependente(cpfValue, tipo, tag);
         }
     }

     function formatarCNPJ(tag) {

         mascaraCnpj(tag, formataCNPJ);

         let tipo        = $(tag).attr('tipo');
         let cpfValue    = tag.value;
         let tamanhoCPF  = cpfValue.length

         if (cpfValue === '' || tamanhoCPF < 18) {
             return false;
         }

         if (!valida_cnpj(cpfValue)) {

             alert('CNPJ inválido!');

             tag.value = '';
             tag.focus();

             return false;
         } else {
             preencherDadosDoDependente(cpfValue, tipo, tag);
         }
     }

     function preencherDadosDoDependente(cpf, tipo, tag) {

         let contador = tag.id.replace('cpfDependente', '');

         if (tipo === 'CPF') {
             cpf = formataCPF(cpf);

             if (cpf.length === 14) {
                 preencherDadosDoDependenteCPF(contador, cpf, tag);
             }
         } else if (tipo === 'CNPJ') {
             cpf = formataCNPJ(cpf);

             if (cpf.length === 18) {
                 preencherDadosDoDependenteCNPJ(contador, cpf, tag);
             }
         }
    }

    function preencherDadosDoDependenteCPF(contador, cpf, tag) {

        $('input[name="cpfDependente[]"]').each(function( index, cpft ) {

            let cpfDependente = cpft.value;
            let sequencialDependente = $(this).attr('sequencial');

            if (cpfDependente !== '') {
                if (cpfDependente === cpf && sequencialDependente !== contador) {
                    alert('CPF Já está sendo utilizado por outra pessoa neste compra.');
                    tag.value = '';
                    tag.focus();
                    return false;
                }
            }
        });

        bloquear_digitacao_cadastro('Buscando Informações...');

        $.ajax({
            type: "GET",
            url: '<?php echo base_url();?>apputil/getVerificaCustomeByCPF',
            data: {
                cpf: cpf,
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                permite_duplicidade:'<?=$produto->permiteVendaClienteDuplicidade;?>',
                programacaoId: '<?=$programacao->id;?>',
            },
            dataType: 'json',
            success: function (resultado) {

                pessoa = resultado.pessoa;

                if (pessoa == null) {
                    desbloquear_digitacao_cadastro();
                    $('#nomeDependente' + contador).focus();
                    return false;
                }

                if (pessoa.bloqueado === '1') {
                    alert("Olá, no momento não é possível iniciar sua contratação, qualquer dúvida entre em contato com a SAGTUR!");

                    $('#cpfDependente' + contador).val('');
                    $('#cpfDependente' + contador).focus();
                } else {
                    $('#nomeDependente' + contador).val(pessoa.name);
                    $('#rgDependente' + contador).val(pessoa.cf1);
                    $('#orgaoEmissorDependente' + contador).val(pessoa.cf3);

                    if (pessoa.tipo_documento != null) {
                        $('#tipo_documentoDependente' + contador).val(pessoa.tipo_documento);
                    } else {
                        $('#tipo_documentoDependente' + contador).val('rg');
                    }

                    $('#sexoDependente' + contador).val(pessoa.sexo);
                    $('#celularDependente' + contador).val(pessoa.cf5);
                    $('#telefone_emergenciaDependente' + contador).val(pessoa.telefone_emergencia);
                    $('#emailDependente' + contador).val(pessoa.email);
                    $('#profissao' + contador).val(pessoa.profession);
                    $('#doenca_informar' + contador).val(pessoa.doenca_informar);
                    $('#social_name' + contador).val(pessoa.social_name);

                    if (pessoa.data_aniversario !== '' && pessoa.data_aniversario !== null) {
                        let dia = pessoa.data_aniversario.split('-').reverse()[0];
                        let mes = pessoa.data_aniversario.split('-').reverse()[1];
                        let ano = pessoa.data_aniversario.split('-').reverse()[2];

                        $('#diaDependente' + contador).val(dia);
                        $('#mesDependente' + contador).val(mes);
                        $('#anoDependente' + contador).val(ano);
                    }

                    //endereco
                    $('#cep').val(pessoa.postal_code);
                    $('#endereco').val(pessoa.address);
                    $('#numeroEndereco').val(pessoa.numero);
                    $('#complementoEndereco').val(pessoa.complemento);
                    $('#bairro').val(pessoa.bairro);
                    $('#cidade').val(pessoa.city);
                    $('#estado').val(pessoa.state);

                    $('#div_name_'+contador).html(pessoa.name);

                    if ($("#assento_dependente"+contador).is("*")) {
                        $('#assento_dependente'+contador).html(pessoa.name);
                    }

                    desbloquear_digitacao_cadastro();
                    $('#nomeDependente' + contador).focus();

                    verificarDuplicidadeClienteDependente(tag, resultado.verifica_duplicidade, contador);
                }

            },
            error: function(error) {
                console.error(error);
                desbloquear_digitacao_cadastro();
            }
        });
    }

    function preencherDadosDoDependenteCNPJ(contador, cnpj, tag) {

        $('input[name="cpfDependente[]"]').each(function( index, cpft ) {

            let cpfDependente = cpft.value;
            let sequencialDependente = $(this).attr('sequencial');

            if (cpfDependente !== '') {
                if (cpfDependente === cnpj && sequencialDependente !== contador) {
                    alert('CNPJ Já está sendo utilizado por outra pessoa neste compra.');
                    tag.value = '';
                    tag.focus();
                    return false;
                }
            }
        });

        bloquear_digitacao_cadastro('Buscando Informações...');

        $.ajax({
            type: "GET",
            url: '<?php echo base_url();?>apputil/getVerificaCustomeByCPF',
            data: {
                cpf: cnpj,
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                permite_duplicidade:'<?=$produto->permiteVendaClienteDuplicidade;?>',
                programacaoId: '<?=$programacao->id;?>',
            },
            dataType: 'json',
            success: function (resultado) {

                pessoa = resultado.pessoa;

                if (pessoa == null) {
                    desbloquear_digitacao_cadastro();
                    consultarPessoaJurifica(cnpj, contador, tag);
                    return false;
                }

                if (pessoa.bloqueado === '1') {

                    alert("Olá, no momento não é possível iniciar sua contratação, qualquer dúvida entre em contato com a SAGTUR!");

                    $('#cpfDependente' + contador).val('');
                    $('#cpfDependente' + contador).focus();
                } else {
                    $('#nomeDependente' + contador).val(pessoa.name);
                    $('#company_name' + contador).val(pessoa.company);
                    $('#nome_responsavel' + contador).val(pessoa.nome_responsavel);
                    $('#rgDependente' + contador).val(pessoa.cf1);
                    $('#orgaoEmissorDependente' + contador).val(pessoa.cf3);

                    if (pessoa.tipo_documento != null) {
                        $('#tipo_documentoDependente' + contador).val(pessoa.tipo_documento);
                    } else {
                        $('#tipo_documentoDependente' + contador).val('rg');
                    }

                    $('#sexoDependente' + contador).val(pessoa.sexo);
                    $('#celularDependente' + contador).val(pessoa.cf5);
                    $('#telefone_emergenciaDependente' + contador).val(pessoa.telefone_emergencia);
                    $('#emailDependente' + contador).val(pessoa.email);
                    $('#profissao' + contador).val(pessoa.profession);
                    $('#doenca_informar' + contador).val(pessoa.doenca_informar);
                    $('#social_name' + contador).val(pessoa.social_name);

                    if (pessoa.data_aniversario !== '' && pessoa.data_aniversario !== null) {
                        let dia = pessoa.data_aniversario.split('-').reverse()[0];
                        let mes = pessoa.data_aniversario.split('-').reverse()[1];
                        let ano = pessoa.data_aniversario.split('-').reverse()[2];

                        $('#diaDependente' + contador).val(dia);
                        $('#mesDependente' + contador).val(mes);
                        $('#anoDependente' + contador).val(ano);
                    }

                    //endereco
                    $('#cep').val(pessoa.postal_code);
                    $('#endereco').val(pessoa.address);
                    $('#numeroEndereco').val(pessoa.numero);
                    $('#complementoEndereco').val(pessoa.complemento);
                    $('#bairro').val(pessoa.bairro);
                    $('#cidade').val(pessoa.city);
                    $('#estado').val(pessoa.state);

                    $('#div_name_'+contador).html(pessoa.name);

                    if ($("#assento_dependente"+contador).is("*")) {
                        $('#assento_dependente'+contador).html(pessoa.name);
                    }

                    desbloquear_digitacao_cadastro();

                    $('#nomeDependente' + contador).focus();

                    verificarDuplicidadeClienteDependente(tag, resultado.verifica_duplicidade, contador);
                }
            },
            error: function(error) {
                console.error(error);
                desbloquear_digitacao_cadastro();
            }
        });
    }

    function consultarPessoaJurifica(cnpj, contador, tag) {

        var cnpj = cnpj.replace(/[^0-9]/g, '');

        $.ajax({
            type: "POST",
            url:  '<?=base_url();?>apputil/wscliente',
            data : {cnpj : cnpj},
            dataType: 'json',
            success: function (empresa) {

                empresa = JSON.parse(empresa);

                if (empresa.situacao !== 'ATIVA') {
                    if (confirm('Esta empresa encontra-se na situação '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Não será possível continuar sua contratação.')) {
                        tag.value = '';
                        tag.focus();
                    }
                } else {
                    preencherDadosSEFAZ(empresa, contador);
                }

            }
        });
    }

    function preencherDadosSEFAZ(empresa, contador) {

        var data_abertura = empresa.abertura;

        //$('#nomeDependente' + contador).val(empresa.nome);
        $('#company_name' + contador).val(empresa.nome);
        $('#sexoDependente' + contador).val('OUTROS');

        //$('#celularDependente' + contador).val(empresa.telefone);
        //$('#emailDependente' + contador).val(empresa.email);

        if (data_abertura !== '' && data_abertura !== null) {
            let ano = data_abertura.split('/').reverse()[0];
            let mes = data_abertura.split('/').reverse()[1];
            let dia = data_abertura.split('/').reverse()[2];

            $('#diaDependente' + contador).val(dia);
            $('#mesDependente' + contador).val(mes);
            $('#anoDependente' + contador).val(ano);
        }

        $('#div_name_'+contador).html(empresa.name);

        if ($("#assento_dependente"+contador).is("*")) {
            $('#assento_dependente'+contador).html(empresa.name);
        }

        $('#company').val(empresa.fantasia);

        $('#cep').val(empresa.cep.replace(/[\s.-]/g, ''));
        $('#endereco').val(empresa.logradouro);

        $('#numeroEndereco').val(empresa.numero);
        $('#complementoEndereco').val(empresa.complemento);
        $('#bairro').val(empresa.bairro);

        $('#cidade').val(empresa.municipio);
        $('#estado').val(empresa.uf);

        let atividades = '';

        for (let i = 0; i < empresa.atividade_principal.length; i++) {
            atividades = atividades + empresa.atividade_principal[i].code + ' - ' + empresa.atividade_principal[i].text + '<br/>';
        }

        for (let j = 0; j < empresa.atividades_secundarias.length; j++) {
            atividades = atividades + empresa.atividades_secundarias[j].code + ' - ' + empresa.atividades_secundarias[j].text + '<br/>';
        }

        for (let k = 0; k < empresa.qsa.length; k++) {
            atividades = atividades + empresa.qsa[k].qual + ' - ' + empresa.qsa[k].nome + '<br/>';
        }

        $('#observacao_item' + contador).val(atividades);

        desbloquear_digitacao_cadastro();

        $('#nomeDependente' + contador).focus();
    }

    function preencherDadosDoDependentePagador(cpf, tag) {

        let contador = tag.id.replace('cpfDependente', '');

        cpf = formataCPF(cpf);

        if (cpf.length === 14 ) {

            bloquear_digitacao_cadastro('Buscando Informações...');

            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>apputil/getVerificaCustomeByCPF',
                data: {
                    cpf: cpf,
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                    programacaoId: '<?=$programacao->id;?>',
                    permite_duplicidade: false,
                },
                dataType: 'json',
                success: function (resultado) {

                    pessoa = resultado.pessoa;

                    if (pessoa == null) {
                        desbloquear_digitacao_cadastro();
                        $('#nomeDependente' + contador).focus();
                        return false;
                    }

                    if (pessoa.bloqueado === '1') {
                        alert("Olá, no momento não é possível iniciar sua contratação, qualquer dúvida entre em contato com a SAGTUR!");

                        $('#cpfDependente' + contador).val('');
                        $('#cpfDependente' + contador).focus();
                    } else {
                        $('#nomeDependente' + contador).val(pessoa.name);
                        $('#rgDependente' + contador).val(pessoa.cf1);
                        $('#orgaoEmissorDependente' + contador).val(pessoa.cf3);

                        if (pessoa.tipo_documento != null) {
                            $('#tipo_documentoDependente' + contador).val(pessoa.tipo_documento);
                        } else {
                            $('#tipo_documentoDependente' + contador).val('rg');
                        }

                        $('#sexoDependente' + contador).val(pessoa.sexo);
                        $('#celularDependente' + contador).val(pessoa.cf5);
                        $('#telefone_emergenciaDependente' + contador).val(pessoa.telefone_emergencia);
                        $('#emailDependente' + contador).val(pessoa.email);
                        $('#profissao' + contador).val(pessoa.profession);
                        $('#doenca_informar' + contador).val(pessoa.doenca_informar);
                        $('#social_name' + contador).val(pessoa.social_name);

                        if (pessoa.data_aniversario !== '' && pessoa.data_aniversario !== null) {
                            let dia = pessoa.data_aniversario.split('-').reverse()[0];
                            let mes = pessoa.data_aniversario.split('-').reverse()[1];
                            let ano = pessoa.data_aniversario.split('-').reverse()[2];

                            $('#diaDependente' + contador).val(dia);
                            $('#mesDependente' + contador).val(mes);
                            $('#anoDependente' + contador).val(ano);
                        }

                        $('#div_name_'+contador).html(pessoa.name);

                        if ($("#assento_dependente"+contador).is("*")) {
                            $('#assento_dependente'+contador).html(pessoa.name);
                        }

                        desbloquear_digitacao_cadastro();

                        $('#nomeDependente' + contador).focus();
                    }
                },
                error: function(error) {
                    console.error(error);
                    desbloquear_digitacao_cadastro();
                }
            });
        }
    }

    function bloquear_digitacao_cadastro(frase) {
        $('input:visible, select:visible, textarea:visible').prop('readonly', true);
        $('.carregando').html(frase);
        $('.carregando').show();
        $( ".forward" ).hide();
    }

    function desbloquear_digitacao_cadastro() {
        $('input:visible, select:visible, textarea:visible').prop('readonly', false);
        $('.carregando').html('');
        $('.carregando').hide();
        $( ".forward" ).show();
    }

    function verificarDuplicidadeClienteDependente(tag, item, contador) {

        if (item) {
            if (confirm('VOCÊ JÁ REALIZOU UMA RESERVA.DESEJA ABRIR?')) {
                if (item.sale_status === 'orcamento') {
                    windowObjectReference = window.open(
                        "<?=base_url().'appcompra/orcamento';?>/"+item.sale_id+"/<?=$programacao->id;?>/<?=$vendedor->id;?>?token=<?=$this->session->userdata('cnpjempresa');?>",
                        "Orçamento de Passagem",
                        "resizable,scrollbars,status"
                    );
                } else {
                    windowObjectReference = window.open(
                        "<?=base_url().'appcompra/confirmacaoDeReserva';?>/"+item.sale_id+"/<?=$programacao->id;?>/<?=$vendedor->id;?>?token=<?=$this->session->userdata('cnpjempresa');?>",
                        "Reserva Online",
                        "resizable,scrollbars,status"
                    );
                }
                tag.value = '';
            } else {
                tag.value = '';
            }
            $('#cpfDependente' + contador).focus();
        }
    }

    function getDouble(valor) {

        if (valor === null) return 0;
        if (valor === undefined) return 0;
        if (valor === '') return 0;

        return parseFloat(valor);
    }

    function popular_table_parcelamento_valepay() {

        var tipoCobranca            = $('input[name="tipoCobranca"]:checked').val();
        let valorSubTotalTaxas      = getDouble($('#subTotalTaxas').val());
        let valorSinal              = getDouble($('#valorSinal').val());
        let amout                   = (valorSubTotalTaxas - valorSinal);
        let pagamentoComCartaoSinal = $('#pagamentoComCartaoSinal').val();

        if (pagamentoComCartaoSinal === '1') {//TODO SE O PAGAMENTO DO SINAL FOR COM CARTAO ENTAO ATRIBUI O SINAL COMO CARTAO
            amout           = valorSinal;
            tipoCobranca    = $('input[name="tipoCobrancaSinal"]:checked').val();
        }

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/simulation_installments_valepay",
            data: {
                programacaoId: $('#programacaoId').val(),
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                valor_total_taxas: amout,
                tipo_cobranca: tipoCobranca,
                istaxasComissaoAtivo: <?=$produto->isTaxasComissao?>,
                produtoID: <?=$produto->id?>,
            },
            dataType: 'json',
            success: function (parcelas) {

                let table = $('#tb-parcelamento-valepay tbody');
                let dataRow;

                table.empty();

                if (parcelas.status === 'success') {

                    for (let i = 0; i < parcelas.data.length; i++) {

                        dataRow = $('<tr>', {style: 'border-bottom: 1px solid #ccc;padding: 15px;font-size: 20px;font-weight: 700;'});

                        let numero_parcela = parcelas.data[i].parcela
                        let valor_parcela = parcelas.data[i].valor_parcela;
                        let totalPagar = parcelas.data[i].total;
                        let sem_juros = parcelas.data[i].sem_juros;

                        var radioButton = $("<input>", {type: "radio", required: 'required', name: "parcelamento_valepay", style: 'width: 25px;height: 30px;border: 1px solid #ccc;cursor: pointer;', value: numero_parcela});
                        let dataCellRadioSelecao = $('<td>').append(radioButton);
                        let dataCellParcela = $('<td>').text(numero_parcela + 'X');
                        let dataCellValorParcela

                        if (sem_juros) {
                            dataCellValorParcela = $('<td>').text('R$'+formatMoney(valor_parcela) + ' (Sem Juros)');
                        } else {
                            dataCellValorParcela = $('<td>').text('R$'+formatMoney(valor_parcela) + ' (' + 'R$'+formatMoney(totalPagar) + ')');
                        }

                        dataRow.append(dataCellRadioSelecao);
                        dataRow.append(dataCellParcela);
                        dataRow.append(dataCellValorParcela);

                        table.append(dataRow);
                    }

                    $('#name_card_valepay').focus();
                }
            }
        });
    }

    var notificacaoSemVagasVisivel = false;

    function consultaVagasEmTempoReal() {
        let listaEmpera = parseInt($('#listaEmpera').val());

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/vagas",
            data: {
                programacaoId: $('#programacaoId').val(),
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                tipo_hospedagem : $('input[name="tipoHospedagem"]:checked').val(),
                controle_estoque_hospedagem : '<?php echo $produto->controle_estoque_hospedagem;?>',
            },
            dataType: 'html',
            success: function (vagas) {

                vagas = getDouble(vagas);

                $('.fvDependentes').each(function() {
                    let faixaId         =  $(this).attr('id');
                    let qtd             = getDouble($("#"+ faixaId +" option:selected" ).val());
                    let descontarVaga   =  $(this).attr('descontarVaga');

                    if (descontarVaga === "1") vagas = vagas - qtd;

                });

                if(vagas < 0) {
                    if (listaEmpera <= 0) {//lista de espera
                        $('#step_tipo_cobranca').removeClass('wizard-step');
                        $('#step_tipo_cobranca').removeClass('step');

                        $('#step_tipo_cobranca_sinal').removeClass('wizard-step');
                        $('#step_tipo_cobranca_sinal').removeClass('step');

                        desabilitarCondicaoPagamento();
                    } else {
                        if (!notificacaoSemVagasVisivel) {
                            notificacaoSemVagasVisivel = true;
                            $.confirm({
                                title: 'Atençao!',
                                content: '️Número máximo de vagas esgotado! Entre em contato com vendedor para mais informações.',
                                type: 'red',
                                typeAnimated: true,
                                buttons: {
                                    ok: function () {
                                        notificacaoSemVagasVisivel = false;
                                    },
                                }
                            });
                        }
                        $( ".forward" ).hide();
                    }
                }
            }
        });
    }

    function antesTipoCobrancaSinal() {

        let elementos = $('input[name="tipoCobranca"]');

        $('.class_tipo_cobranca').show();
        elementos.prop('checked', false);//TODO DESABILITA TODOS OS CHECKBOX DO TIPO COBRANCA
        $('#condicaoPagamento').empty();//limpa a lista de parcelas


        for (let i = 0; i < elementos.length; i++) {
            let id = $(elementos[i]).val();
            let tipo = $(elementos[i]).attr('tipo');

            if (tipo === 'carne_cartao_transparent' ||
                tipo === 'carne_cartao_transparent_mercado_pago' ||
                tipo === 'cartao_credito_transparent_valepay' ||
                tipo === 'carne_cartao') {
                $('#div_pagamento_tipo_cobranca' + id).hide();
            }
        }

    }

    function depoisTipoCobrancaSinal()  {

        var tipoCobranca    = $('input[name="tipoCobrancaSinal"]:checked').val();
        let tipo            = $('input[name="tipoCobrancaSinal"]:checked').attr('tipo')

        $('#pagamentoComCartaoSinal').val('0');

        if (tipoCobranca === '' || tipoCobranca === undefined) {
            return false;
        }

        habilitarCondicaoPagamento();

        if (tipo === 'carne_cartao_transparent') {//pagseguro
            habilitarCartaoDeCreditoPagSeguro();
            $('#pagamentoComCartaoSinal').val('1');
        }

        if (tipo === 'carne_cartao_transparent_mercado_pago') {//mercado pago
            habilitarCartaoDeCreditoMercadoPago();
            $('#pagamentoComCartaoSinal').val('1');
        }

        if (tipo === 'cartao_credito_transparent_valepay') {//valepay
            habilitarCartaoDeCreditoValePay();
            $('#pagamentoComCartaoSinal').val('1');
        }

        <?php if ($configMercadoPago->active) { ?>
            loadCardFormMercadoPago();
        <?php } ?>
    }

    function getCondicoesDePagamento() {

        var tipoCobranca        = $('input[name="tipoCobranca"]:checked').val();
        let tipo                = $('input[name="tipoCobranca"]:checked').attr('tipo');
        let listaEmpera         = parseInt($('#listaEmpera').val());

        if (listaEmpera <= 0) {//lista de espera
            $('#step_tipo_cobranca').removeClass('wizard-step');
            $('#step_tipo_cobranca').removeClass('step');

            $('#step_tipo_cobranca_sinal').removeClass('wizard-step');
            $('#step_tipo_cobranca_sinal').removeClass('step');

            desabilitarCondicaoPagamento();
        } else {
             <?php if(!$produto->apenas_cotacao) {?>
                habilitarCondicaoPagamento();
             <?php } ?>
        }

        if (tipoCobranca === '' || tipoCobranca === undefined) {
            return false;
        }

        habilitarCondicaoPagamento();

        if (tipo === 'carne_cartao_transparent') {//pagseguro
            habilitarCartaoDeCreditoPagSeguro();
            desabilitarCondicaoPagamento();
        } else {
            desabilitarCartaoDeCreditoPagSeguro();
        }

        if (tipo === 'carne_cartao_transparent_mercado_pago') {//mercado pago
            habilitarCartaoDeCreditoMercadoPago();
            desabilitarCondicaoPagamento();
        } else {
            desabilitarCartaoDeCreditoMercadoPago();
        }

        if (tipo === 'cartao_credito_transparent_valepay') {//valepay
            habilitarCartaoDeCreditoValePay();
            desabilitarCondicaoPagamento();
        } else {
            desabilitarCartaoDeCreditoValePay();
        }

        if (tipo  === 'carne_cartao') {//carto padrao
            desabilitarCondicaoPagamento();
        }

        if (tipo  === 'carne_cartao_transparent') {//carto padrao
            desabilitarCondicaoPagamento();
        }

        if (tipo  === 'link_pagamento') {
            desabilitarCondicaoPagamento();
        }

        depoisTipoCobrancaSinal();

        $.blockUI(
            {
                message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                css: {width: '70%', left: '15%'}
            }
        );

        $('button[name="forward"]').css('display', 'none');

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/condicoes",
            data: {
                token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                tipoCobrancaId: tipoCobranca,
                tipo: $('input[name="tipoCobranca"]:checked').attr('tipo'),
                data_Saida: '<?php echo $programacao->dataSaida;?>',
                programacaoId: $('#programacaoId').val(),
                istaxasComissaoAtivo: <?=$produto->isTaxasComissao?>,
                produtoID: <?=$produto->id?>,
            },
            dataType: 'json',
            success: function (condicoes) {

                $('#condicaoPagamento').empty();
                $('input[name="number"]').val('');
                $.unblockUI();

                let taxa_parcela = [];

                $(condicoes).each(function( index, condicao ) {
                    taxa_parcela[index] = parseFloat(condicao.valor);
                });

                if (condicoes) {
                    $(condicoes).each(function( index, condicao ) {

                        let text        = '';
                        let parcelas    = getDouble(condicao.parcelas);
                        let valor       = getDouble(condicao.valor);

                        let total       = getDouble($('#subTotal').val());
                        let valorSinal  = getDouble($('#valorSinal').val());

                        let acrescimoDescontoTipo = condicao.acrescimoDescontoTipo;
                        let tipo = condicao.tipo;
                        let taxaFixaIntermediacao = getDouble(condicao.taxaFixaIntermediacao);
                        let taxaIntermediacao = getDouble(condicao.taxaIntermediacao);

                        if (valorSinal > 0) {
                            text = 'SINAL + ' + condicao.condicaoPagamento + ' de ';
                        } else {
                            text = condicao.condicaoPagamento + ' de ';
                        }

                        total = total - valorSinal;

                        if (taxaIntermediacao > 0) {
                            //let taxa = total*(taxaIntermediacao/100);
                            //let totalComTaxaIntermediaria = total + taxa;

                            totalComTaxaIntermediaria = total / (1 - taxaIntermediacao/100);

                            total = totalComTaxaIntermediaria;

                            //let taxaIntermediaria =  totalComTaxaIntermediaria*(taxaIntermediacao/100);
                            //let diferencaTaxa = taxaIntermediaria - taxa;
                            //total = total + taxa + diferencaTaxa;
                        }

                        if (taxaFixaIntermediacao > 0) {
                            total = total + taxaFixaIntermediacao;
                        }

                        let subTotal = total/parcelas;
                        let subTotalPorParcela;

                        if (valor > 0) {

                            let valorCalculado = 0;

                            if (tipo === 'percentual') {

                                let totalComTaxas = subTotal*parcelas;
                                let percentualDeAcrescimoParcela = taxa_parcela[index];

                                valorCalculado  =  totalComTaxas*(percentualDeAcrescimoParcela/100);

                                if (acrescimoDescontoTipo === 'acrescimo') subTotal = (subTotal*parcelas) + valorCalculado;
                                else subTotal = (subTotal*parcelas) - valorCalculado;

                                subTotal = subTotal.toFixed(2);
                                subTotalPorParcela  = subTotal;
                                text = text+' R$'+subTotal+' (R$'+((subTotal/parcelas).toFixed(2))+')';

                            } else if (tipo === 'absoluto') {
                                valorCalculado = valor;

                                if (acrescimoDescontoTipo === 'acrescimo') subTotal = subTotal + valorCalculado;
                                else subTotal = subTotal - valorCalculado;

                                subTotal = subTotal.toFixed(2);
                                subTotalPorParcela  = (subTotal*parcelas).toFixed(2);
                                text = text+' R$'+subTotal+' (R$'+(subTotalPorParcela)+')';
                            }

                        } else {
                            subTotalPorParcela  = (subTotal*parcelas).toFixed(2);
                            subTotal = subTotal.toFixed(2);

                            if (taxaFixaIntermediacao === 0 || taxaIntermediacao === 0) {
                                text = text+ ' R$'+subTotal;
                            } else {
                                text = text+ ' R$'+subTotal+' com taxas';
                            }
                        }

                        $('#condicaoPagamento').append($('<option>', {
                            value: condicao.condicaoPagamentoId,
                            condicaoPagamentoId: condicao.condicaoPagamentoId,
                            numero_parcelas: condicao.parcelas,
                            tipo: condicao.tipoCobranca,
                            text: text,
                            totalAmount: subTotalPorParcela,
                            acrescimoDescontoTipo: acrescimoDescontoTipo,
                        }));

                        if (condicao.tipoCobranca === 'cartao' || condicao.tipoCobranca === 'carne_cartao') {
                            $('#parcelamento').hide();
                        } else {
                            $('#parcelamento').show();
                        }
                    });

                    let com_sinal = $('input[name="pagar_com_sinal"]:checked').val();

                    if ($('input[name="tipoCobranca"]:checked').attr('tipo') === 'carne_cartao_transparent') {
                        $('#condicaoPagamento').change();
                    } else {
                        $('#condicaoPagamento').change();
                    }
                    $('button[name="forward"]').css('display', '');

                    let qtdParcelasCondicaoPagamento = $('#condicaoPagamento > option').length;

                    if (qtdParcelasCondicaoPagamento === 1 || com_sinal === '2') {
                        desabilitarCondicaoPagamento();
                    }
                }
            },
            error: function (request, status, error) {
                $.unblockUI();
                $('button[name="forward"]').css('display', '');
            }
        });
    }

    function getParcelamento() {

        var condicaoPagamento   = $("#condicaoPagamento  option:selected").val();
        var tipoCobranca        = $('input[name="tipoCobranca"]:checked').val();
        var numero_parcelas     = $("#condicaoPagamento  option:selected").attr('numero_parcelas');

        if (condicaoPagamento !== '' && tipoCobranca !== '' && condicaoPagamento !== undefined && tipoCobranca !== undefined) {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>apputil/getParcelamento",
                data: {
                    subTotal: $('#subTotalTaxas').val(),//TODO PASSAR O VALOR COM TAXA
                    valorSinal: $('#valorSinal').val(),//TODO PASSAR O VALOR DO SINAL
                    tipoCobrancaSinal: $('#tipoCobrancaSinal').val(),
                    nome_cobranca_sinal: $('input[name="tipoCobrancaSinal"]:checked').attr('nome_cobranca'),
                    contaMovimentadorSinal: $('#contaMovimentadorSinal').val(),
                    condicaoPagamentoId: condicaoPagamento,
                    numero_parcelas: numero_parcelas,
                    tipoCobrancaId: tipoCobranca,
                    programacaoId: $('#programacaoId').val(),
                    tipo: $('input[name="tipoCobranca"]:checked').attr('tipo'),
                    conta: $('input[name="tipoCobranca"]:checked').attr('conta'),
                    data_Saida: '<?php echo $programacao->dataSaida;?>',
                    istaxasComissaoAtivo: <?=$produto->isTaxasComissao?>,
                    produtoID: <?=$produto->id?>,
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                dataType: 'html',
                success: function (html) {
                    $('#parcelamento').html(html);

                    $("input[name='dtVencimentos[]']").change(function (event) {

                        let isReadonly = $(this).attr('readonly');

                        let dataSplit = $(this).val().split('-');
                        let dataSplitMax = $(this).attr('dataMaximaPagamento').split('-');
                        let diasAntesDaViagem = $(this).attr('diasAntesDaViagem');
                        let dtOriginal = $(this).attr('original');
                        let dtOriginalSplit = $(this).attr('original').split('-');

                        let dtVencimento = new Date(dataSplit[0], dataSplit[1] - 1, dataSplit[2]);
                        let dtMaxiaPagamento = new Date(dataSplitMax[0], dataSplitMax[1] - 1, dataSplitMax[2]);
                        let dtOriginalDt = new Date(dtOriginalSplit[0], dtOriginalSplit[1] - 1, dtOriginalSplit[2]);

                        let dtHoje = new Date();

                        if (isReadonly) {
                            if (dtVencimento.getTime() !== dtOriginalDt.getTime()) {
                                alert("A data do primeiro vencimento não pode ser alterada");
                                $(this).val(dtOriginal);
                                return false;
                            }
                        }

                        if (dtVencimento.getTime() > dtMaxiaPagamento.getTime()) {
                            alert('A data de vencimento deverá ser quitado até '+diasAntesDaViagem+' dias antes da viagem.')

                            $(this).val(dtOriginal);
                            return false;
                        }

                        if (dtVencimento.getTime()  < dtHoje.getTime() ) {
                            alert('A data do vencimento não pode ser menor que a data de hoje!')

                            $(this).val(dtOriginal);
                            return false;
                        }

                        if (dtVencimento.getMonth() !== dtOriginalDt.getMonth() ) {

                            const meses = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"];
                            let nomeMes = meses[dtOriginalDt.getMonth()+1];

                            alert('Você não pode alterar o vencimento para outro mês somente dias dentro de ' + nomeMes+ ' serão válidos!')

                            $(this).val(dtOriginal);
                            return false;
                        }
                    });
                }
            });
        } else {
            $('#parcelamento').html('');
        }
    }

    function carregarPagSeguro() {

        <!-- API PagSeguro is active-->
        <?php if ($configPagSeguro->active) { ?>
        // cria uma sessão
        PagSeguroDirectPayment.setSessionId('<?php echo $sessionCode;?>');

        // obter meios de pagamento
        PagSeguroDirectPayment.getPaymentMethods({
            success: function(data){
                $('#cc-aceitos-pagseguro').html('');

                $.each(data.paymentMethods.CREDIT_CARD.options, function(i, obj) {

                    let url = "http://stc.pagseguro.uol.com.br/"+obj.images.SMALL.path;

                    $('#cc-aceitos-pagseguro').append("<img src="+url+">");
                });
            },
            error: function(json){
                var erro = "";
                for(i in json.errors){
                    erro = erro + json.errors[i];
                }
                alert(erro);
            },
            complete: function(json){
            }
        });

        // Obter identificação do comprador
        var senderHash = PagSeguroDirectPayment.getSenderHash();
        $("input[name='senderHash']").val(senderHash);

        <?php } else { ?>
        $("input[name='senderHash']").val('');
        <?php } ?>
    }

    $('#parcelas-cartao-credito').change(function () {
        let valorPorParcela = $( '#parcelas-cartao-credito :selected' ).attr('valorPorParcela');

        if (valorPorParcela !== undefined) {
            $('#totalParcelaPagSeguro').val(valorPorParcela);
        }
    });

    $("input[name='number']").keypress(function (event){
        buscarBandeiraCartao();
    });

    $("input[name='number']").change(function (event){
        buscarBandeiraCartao();
    });

    $("input[name='cvc']").change(function (event) {
        createCardTokenPagSeguro();
    });

    $("input[name='cvc']").keypress(function (event) {
        createCardTokenPagSeguro();
    });

    $("select[name='card_expiry_mm']").change(function (event) {
        createCardTokenPagSeguro();
    });

    $("select[name='card_expiry_yyyy']").change(function (event) {
        createCardTokenPagSeguro();
    });

    function buscarBandeiraCartao() {

        if ($("input[name='number']").val() === '') return;
        if ($("input[name='number']").val().length < 19) return;

        // consultar a bandeira do cartão
        PagSeguroDirectPayment.getBrand({
            cardBin: $("input[name='number']").val().replace(/ /g, ''),
            success: function (json) {
                var brand = json.brand.name;

                $('#band').attr('src', '<?php echo base_url() ?>themes/default/assets/images/credit-cards/'+brand+'.png');
                $('#band').show();

                getParcelamentoCartaoCreditoPagSeguro(brand);
                createCardTokenPagSeguro();
            },
            error: function (json) {
                console.log(json);
            },
            complete: function (json) {}
        });
    }

    function getParcelamentoCartaoCreditoPagSeguro(brand) {

        let pagamentoComCartaoSinal = $('#pagamentoComCartaoSinal').val();
        let valorSubTotalTaxas      = getDouble($('#subTotalTaxas').val());
        let valorSinal              = getDouble($('#valorSinal').val());
        let amout                   = (valorSubTotalTaxas - valorSinal);

        if (pagamentoComCartaoSinal === '1') {//TODO SE O PAGAMENTO DO SINAL FOR COM CARTAO ENTAO ATRIBUI O SINAL COMO CARTAO
            amout = valorSinal;
        }

        PagSeguroDirectPayment.getInstallments({
            //amount: getDouble($( '#condicaoPagamento :selected' ).attr('totalAmount')),
            amount: amout,
            //maxInstallmentNoInterest: 1,
            brand: brand,
            success: function(response){
                $('#parcelas-cartao-credito').empty();

                let parcelas = eval('response.installments.'+brand);

                $(parcelas).each(function( index, parcela ) {

                    let quantity = parcela.quantity;
                    let valorPorParcela = parcela.installmentAmount;
                    let totalAmount = parcela.totalAmount;
                    let interestFree = parcela.interestFree;
                    let text = '';

                    if (interestFree) {
                        text = quantity+'X R$'+valorPorParcela+' sem juros';
                    } else {
                        text = quantity+'X R$'+valorPorParcela+' (R$'+totalAmount+')';
                    }

                    var option = $('<option/>');
                    option.attr({ 'value': quantity }).attr({ 'valorPorParcela': valorPorParcela }).attr({ 'totalAmount': totalAmount }).text(text);
                    $('#parcelas-cartao-credito').append(option);
                });

                $('#parcelas-cartao-credito').change();
            },
            error: function(error) {
                console.log(error);
            },
            complete: function(complete){
                console.log(complete);
            }
        });
    }

    function createCardTokenPagSeguro() {

        if ($("input[name='number']").val() === '') return;

        let cardNumber = $("input[name='number']").val().replace(/ /g, '');

        // consultar a bandeira do cartão
        PagSeguroDirectPayment.getBrand({
            cardBin: cardNumber,
            success: function (json) {
                var brand = json.brand.name;
                $('#erro-cartao').html('');

                if ($("input[name='number']").val() === '') return;
                if ($("input[name='cvc']").val() === '') return;
                if ($("select[name='card_expiry_mm']").val() === '') return;
                if ($("select[name='card_expiry_yyyy']").val() === '') return;

                let mes = $("select[name='card_expiry_mm']").val().trim();
                let ano = $("select[name='card_expiry_yyyy']").val().trim();
                let cvc =  $("input[name='cvc']").val();

                var param = {
                    cardNumber: cardNumber,
                    brand: brand,
                    cvv: cvc,
                    expirationMonth: mes,
                    expirationYear: ano,

                    success: function (json) {
                        var token = json.card.token;
                        $("input[name='creditCardToken']").val(token);
                    },
                    error: function (json) {
                        if (json.error) {
                            $(json.errors).each(function (index, error) {
                                $('#erro-cartao').append(JSON.stringify(error));
                            });
                        }

                    },
                    complete: function (json) {}
                }

                // obter token do cartão de crédito
                PagSeguroDirectPayment.createCardToken(param);
            },
            error: function (json) {
                console.log(json);
            },
            complete: function (json) {}
        });
    }

    function adicionarOpcional(adicionalId) {

        $('#btn_adicional'+adicionalId).text('SELECIONADO');
        $('#btn_adicional'+adicionalId).css('background', '#28a745');

        $('#div_adicional'+adicionalId).show();
        $('#btn_excluir_adicional'+adicionalId).show();
    }

    function excluirOpcional(adicionalId) {

        $('#btn_adicional'+adicionalId).text('SELECIONAR');
        $('#btn_adicional'+adicionalId).css('background', '#e58801');

        $('#div_adicional'+adicionalId).hide();
        $('#btn_excluir_adicional'+adicionalId).hide();

        $('.qtdADicionais'+adicionalId).val('0');

        getCondicoesDePagamento();
        getParcelamento();
        calcularTotalPagar();
    }

    function formatMoney(x, symbol) {
        if(!symbol) { symbol = ""; }
        if(site.settings.sac == 1) {
            return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
                ''+formatSA(parseFloat(x).toFixed(site.settings.decimals)) +
                (site.settings.display_symbol == 2 ? site.settings.symbol : '');
        }
        var fmoney = accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
        return fmoney;
    }

    (function (p, z) {
        function q(a) {
            return !!("" === a || a && a.charCodeAt && a.substr)
        }

        function m(a) {
            return u ? u(a) : "[object Array]" === v.call(a)
        }

        function r(a) {
            return "[object Object]" === v.call(a)
        }

        function s(a, b) {
            var d, a = a || {}, b = b || {};
            for (d in b)b.hasOwnProperty(d) && null == a[d] && (a[d] = b[d]);
            return a
        }

        function j(a, b, d) {
            var c = [], e, h;
            if (!a)return c;
            if (w && a.map === w)return a.map(b, d);
            for (e = 0, h = a.length; e < h; e++)c[e] = b.call(d, a[e], e, a);
            return c
        }

        function n(a, b) {
            a = Math.round(Math.abs(a));
            return isNaN(a) ? b : a
        }

        function x(a) {
            var b = c.settings.currency.format;
            "function" === typeof a && (a = a());
            return q(a) && a.match("%v") ? {
                pos: a,
                neg: a.replace("-", "").replace("%v", "-%v"),
                zero: a
            } : !a || !a.pos || !a.pos.match("%v") ? !q(b) ? b : c.settings.currency.format = {
                pos: b,
                neg: b.replace("%v", "-%v"),
                zero: b
            } : a
        }

        var c = {
            version: "0.4.1",
            settings: {
                currency: {symbol: "$", format: "%s%v", decimal: ".", thousand: ",", precision: 2, grouping: 3},
                number: {precision: 0, grouping: 3, thousand: ",", decimal: "."}
            }
        }, w = Array.prototype.map, u = Array.isArray, v = Object.prototype.toString, o = c.unformat = c.parse = function (a, b) {
            if (m(a))return j(a, function (a) {
                return o(a, b)
            });
            a = a || 0;
            if ("number" === typeof a)return a;
            var b = b || ".", c = RegExp("[^0-9-" + b + "]", ["g"]), c = parseFloat(("" + a).replace(/\((.*)\)/, "-$1").replace(c, "").replace(b, "."));
            return !isNaN(c) ? c : 0
        }, y = c.toFixed = function (a, b) {
            var b = n(b, c.settings.number.precision), d = Math.pow(10, b);
            return (Math.round(c.unformat(a) * d) / d).toFixed(b)
        }, t = c.formatQuantity = c.format = function (a, b, d, i) {
            if (m(a))return j(a, function (a) {
                return t(a, b, d, i)
            });
            var a = o(a), e = s(r(b) ? b : {
                precision: b,
                thousand: d,
                decimal: i
            }, c.settings.number), h = n(e.precision), f = 0 > a ? "-" : "", g = parseInt(y(Math.abs(a || 0), h), 10) + "", l = 3 < g.length ? g.length % 3 : 0;
            return f + (l ? g.substr(0, l) + e.thousand : "") + g.substr(l).replace(/(\d{3})(?=\d)/g, "$1" + e.thousand) + (h ? e.decimal + y(Math.abs(a), h).split(".")[1] : "")
        }, A = c.formatMoney = function (a, b, d, i, e, h) {
            if (m(a))return j(a, function (a) {
                return A(a, b, d, i, e, h)
            });
            var a = o(a), f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format);
            return (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal))
        };
        c.formatColumn = function (a, b, d, i, e, h) {
            if (!a)return [];
            var f = s(r(b) ? b : {
                symbol: b,
                precision: d,
                thousand: i,
                decimal: e,
                format: h
            }, c.settings.currency), g = x(f.format), l = g.pos.indexOf("%s") < g.pos.indexOf("%v") ? !0 : !1, k = 0, a = j(a, function (a) {
                if (m(a))return c.formatColumn(a, f);
                a = o(a);
                a = (0 < a ? g.pos : 0 > a ? g.neg : g.zero).replace("%s", f.symbol).replace("%v", t(Math.abs(a), n(f.precision), f.thousand, f.decimal));
                if (a.length > k)k = a.length;
                return a
            });
            return j(a, function (a) {
                return q(a) && a.length < k ? l ? a.replace(f.symbol, f.symbol + Array(k - a.length + 1).join(" ")) : Array(k - a.length + 1).join(" ") + a : a
            })
        };
        if ("undefined" !== typeof exports) {
            if ("undefined" !== typeof module && module.exports)exports = module.exports = c;
            exports.accounting = c
        } else"function" === typeof define && define.amd ? define([], function () {
            return c
        }) : (c.noConflict = function (a) {
            return function () {
                p.accounting = a;
                c.noConflict = z;
                return c
            }
        }(p.accounting), p.accounting = c)
    })(this);

    <?php if ($configPagSeguro->active) { ?>
    $('#cardNumberoPagSeguro').keyup(function (event) {
        mascaraCartaoCredito( this, mcc );
    });
    <?php } ?>

    $('#card_number_valepay').keyup(function (event) {
        mascaraCartaoCredito( this, mcc );
    });

    function recriarFormularioMercadoPago() {
        <?php if ($configMercadoPago->active) { ?>
            $('#form-checkout__issuer').empty();
            $('#form-checkout__installments').empty();
            loadCardFormMercadoPago();
        <?php } ?>
    }

    <?php if ($configMercadoPago->active) { ?>

    const mercadopago = new MercadoPago('<?php echo $configMercadoPago->account_token_public;?>');
    var cardForm = null;

    $('#form-checkout__cardNumber').keyup(function (event) {
        mascaraCartaoCredito( this, mcc );
    });

    $('#form-checkout__cardNumber').keypress(function (event) {
        recriarFormularioMercadoPago();
    });

    function loadCardFormMercadoPago() {

        let tipo_cobranca           = $('input[name="tipoCobranca"]:checked').attr('tipo');
        let tipo_sinal              = $('input[name="tipoCobrancaSinal"]:checked').attr('tipo');
        let pagamentoComCartaoSinal = $('#pagamentoComCartaoSinal').val();

        let valorSubTotalTaxas      = getDouble($('#subTotalTaxas').val());
        let valorSinal              = getDouble($('#valorSinal').val());
        let amout                   = valorSubTotalTaxas - valorSinal;

        if (pagamentoComCartaoSinal === '1') {//TODO SE O PAGAMENTO DO SINAL FOR COM CARTAO ENTAO ATRIBUI O SINAL COMO CARTAO
            amout = valorSinal;
        }

        if (tipo_cobranca === 'carne_cartao_transparent_mercado_pago' ||
            tipo_sinal === 'carne_cartao_transparent_mercado_pago') {

            if (valorSubTotalTaxas > 0) {

                if (cardForm !== null ) {
                    try {
                        cardForm.unmount();
                    } catch (ex) {
                        console.log(ex);
                    }
                }

                cardForm = mercadopago.cardForm({
                    amount:  '' + amout,
                    autoMount: true,
                    form: {
                        id: "wrapped",
                        cardholderName: {
                            id: "form-checkout__cardholderName",
                            placeholder: "COMO GRAVADO NO CARTÃO",
                        },
                        cardholderEmail: {
                            id: "form-checkout__cardholderEmail",
                            placeholder: "E-MAIL",
                        },
                        cardNumber: {
                            id: "form-checkout__cardNumber",
                            placeholder: "•••• •••• •••• ••••",
                        },
                        cardExpirationMonth: {
                            id: "form-checkout__cardExpirationMonth",
                            placeholder: "MM",
                        },
                        cardExpirationYear: {
                            id: "form-checkout__cardExpirationYear",
                            placeholder: "YY",
                        },
                        securityCode: {
                            id: "form-checkout__securityCode",
                            placeholder: "CVV",
                        },
                        installments: {
                            id: "form-checkout__installments",
                            placeholder: "PARCELAS",
                        },
                        identificationType: {
                            id: "form-checkout__identificationType",
                        },
                        identificationNumber: {
                            id: "form-checkout__identificationNumber",
                            placeholder: "Número da Identificação",
                        },
                        issuer: {
                            id: "form-checkout__issuer",
                            placeholder: "Bandeira",
                        },
                    },
                    callbacks: {
                        onFormMounted: error => {
                            if (error) {
                                return console.warn("Form Mounted handling error: ", error);
                            }
                            console.log("Form mounted");
                        },
                        onFetching: (resource) => {
                            console.log("Fetching resource: ", resource);
                        },
                    },
                });
            }

        }
    }

    <?php } ?>

    function mascaraCartaoCredito(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracartaocredito()",1)
    }

    function execmascaracartaocredito(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mascara(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascara()",1)
    }
    function execmascara(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mcc(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{4})(\d)/g,"$1 $2");
        v=v.replace(/^(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3");
        v=v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3 $4");
        return v;
    }

    function mais(id) {

        let quantidadeAtual = getDouble($('#'+id).val());
        let quantidade = quantidadeAtual + 1;

        if (quantidade <= <?=$configuracaoGeral->qty_client_records;?>) $('#'+id).val(quantidade);

        calcularTotalPagar(true);
        consultaVagasEmTempoReal();
        exibirBotaoProximo();

        if ($('input[name="tipoCobranca"]:checked').val() !== undefined) {
            getCondicoesDePagamento();
            getParcelamento();
        }
    }

    function menos(id) {
        let quantidadeAtual = getDouble($('#'+id).val());
        let quantidade = quantidadeAtual - 1;

        if (quantidade >= 0) $('#'+id).val(quantidade);

        calcularTotalPagar(true);
        consultaVagasEmTempoReal();
        exibirBotaoProximo();

        if ($('input[name="tipoCobranca"]:checked').val() !== undefined) {
            getCondicoesDePagamento();
            getParcelamento();
        }
    }

    function exibirBotaoProximo() {

        $('.forward').hide();
        $('#msn_compra_somente_principal').hide();

        var qtdDependete = 0;
        var qtdPrincipal = 0;

        $('.fvDependentes').each(function() {
            let faixaId =  $(this).attr('id');
            let faixadependente = $(this).attr('faixadependente');

            if (faixadependente === '1') {
                qtdDependete += getDouble($("#"+ faixaId +" option:selected" ).val());
            } else {
                qtdPrincipal += getDouble($("#"+ faixaId +" option:selected" ).val());
            }
        });

        if (qtdDependete > 0 && qtdPrincipal === 0) {
            $('#msn_compra_somente_principal').show();
        } else  if (qtdPrincipal > 0) {
            $('.forward').show();
        }

        $('#totalPassageiros').val( (qtdDependete + qtdPrincipal));
    }

    function mcep(v){
        v=v.replace(/\D/g,"")                    //Remove tudo o que n?o ? d?gito
        v=v.replace(/^(\d{5})(\d)/,"$1-$2") 	//Esse ? t?o f?cil que n?o merece explica??es
        return v
    }

    function getConsultaCEPTitular() {

        if($.trim($("#cepTitular").val()) === "") {
            return false;
        }

        if ($('#cepTitular').val().length === 9) {

            var cep = $.trim($("#cepTitular").val());
            cep = cep.replace('-', '');
            cep = cep.replace('.', '');
            cep = cep.replace(' ','');

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url,
                function (data) {
                    if (data !== null) {
                        $("#enderecoTitular").val(data.logradouro);
                        $("#bairroTitular").val(data.bairro);
                        $("#cidadeTitular").val(data.localidade);
                        $("#estadoTitular").val(data.uf);

                        $('#numeroEnderecoTitular').focus();
                    }
                });
        }
    }

    function getConsultaCEPTitularValePay() {

        if($.trim($("#cep_titular_valepay").val()) === "") {
            return false;
        }

        if ($('#cep_titular_valepay').val().length === 9) {

            var cep = $.trim($("#cep_titular_valepay").val());
            cep = cep.replace('-', '');
            cep = cep.replace('.', '');
            cep = cep.replace(' ','');

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url,
                function (data) {
                    if (data !== null) {
                        $("#endereco_titular_valepay").val(data.logradouro);
                        $("#bairro_titular_valepay").val(data.bairro);
                        $("#cidade_titular_valepay").val(data.localidade);
                        $("#estado_titular_valepay").val(data.uf);

                        $('#numero_endereco_titular_valepay').focus();
                    }
                });
        }
    }

    function getConsultaCEP() {

        if($.trim($("#cep").val()) === "") {
            return false;
        }

        if ($('#cep').val().length === 9) {

            var cep = $.trim($("#cep").val());
            cep = cep.replace('-','');
            cep = cep.replace('.','');
            cep = cep.replace(' ','');

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url,
                function (data) {
                    if(data !== null){
                        $("#endereco").val(data.logradouro);
                        $("#bairro").val(data.bairro);
                        $("#cidade").val(data.localidade);
                        $("#estado").val(data.uf);
                        $('#numeroEndereco').focus();
                    }
                });
        }
    }

    function openDigitacaoDados(id) {
        if ($('#dependente_dados'+id).is(':visible')) {
            $('#dependente_dados'+id).hide();
            $('#dependente_dados'+id+'-img').attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
        } else {
            $('#dependente_dados'+id).show();
            $('#dependente_dados'+id+'-img').attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            if ($('#cpfDependente'+id).is(':visible')) {
                $('#cpfDependente'+id).focus();
            } else {
                $('#nomeDependente'+id).focus();
            }
        }
    }

    function salvarCliente(faixaid, seq) {
        if ($('#wrapped').valid()) {
            openDigitacaoDados(faixaid+'_'+seq);
            $('#cc_fieldset_'+faixaid+'_'+seq).css('border-left', 'solid 5px #28a74557');
            document.getElementsByClassName('page_header')[0].scrollIntoView();
        } else {
            $('#cc_fieldset_'+faixaid+'_'+seq).css('border-left', 'solid 5px #dc354596');
        }
    }

    function preencherInformacoesTitularCartaoMercadoPago() {
        <?php if (coletarPagador($produto)) {?>

            let sequencial = '<?=$this->Settings->faixa_pagador;?>_pagador';
            let cpfPagador = $('#cpfDependente'+sequencial).val();
            let emailPagador = $('#emailDependente'+sequencial).val();

            cpfPagador = cpfPagador.replace(/[^\d]/g, "");

            if ($('#form-checkout__cardholderEmail').val() === '') {
                $('#form-checkout__cardholderEmail').val(emailPagador);
            }

            if ($('#form-checkout__identificationNumber').val() === '') {
                $('#form-checkout__identificationNumber').val(cpfPagador);
            }

        <?php } else {?>
            $('.responsavel_compra').each(function( index, tag ) {

                if (tag.value === 'SIM') {

                    let sequencial = $(this).attr('sequencial');
                    let cpfPagador = $('#cpfDependente'+sequencial).val();
                    let emailPagador = $('#emailDependente'+sequencial).val();

                    cpfPagador = cpfPagador.replace(/[^\d]/g, "");

                    if ($('#form-checkout__cardholderEmail').val() === '') {
                        $('#form-checkout__cardholderEmail').val(emailPagador);
                    }

                    if ($('#form-checkout__identificationNumber').val() === '') {
                        $('#form-checkout__identificationNumber').val(cpfPagador);
                    }
                }
            });
        <?php }?>
    }

    function consultarCupom() {
        if ($('#cupom').val() !== '') {
            let url = "<?php echo base_url() ?>appcompra/validar_cupom";
            let cupom = encodeURI($('#cupom').val().trim());

            $.get(url,
                {
                    cupom: cupom,
                    programacao_id: <?php echo $programacao->id; ?>,
                },
                function (data) {
                    if (data) {

                        if (data.status === 'Valid') {

                            $('#validade-cupom').css('color', 'green');

                            if (data.tipo_desconto === 'PERCENTUAL') {
                                $('#validade-cupom').html('Cupom Válido. '+data.valor+'% de desconto no valor da compra.');
                            } else {
                                $('#validade-cupom').html('Cupom Válido. R$'+data.valor+' de desconto no valor da compra.');
                            }

                            $('#valor_cupom_desconto').val(parseFloat(data.valor));
                            $('#tipo_cupom_desconto').val(data.tipo_desconto);
                            $('#cupom_desconto_id').val(data.cupom);
                        } else {

                            $('#valor_cupom_desconto').val(0);
                            $('#tipo_cupom_desconto').val('');
                            $('#cupom_desconto_id').val('');

                            $('#validade-cupom').html('O cupom que você está acessando não está mais disponível, tente outro código de cupom.');
                            $('#validade-cupom').css('color', 'red');
                        }
                    } else  {

                        $('#valor_cupom_desconto').val(0);
                        $('#tipo_cupom_desconto').val('');
                        $('#cupom_desconto_id').val('');

                        $('#validade-cupom').html('Este cupom não é válido. Por favor, tente outro código de cupom.');
                        $('#validade-cupom').css('color', 'red');
                    }

                    calcularTotalPagar(false);

                    if ($('input[name="tipoCobranca"]:checked').attr('tipo') === 'cartao_credito_transparent_valepay') {
                        popular_table_parcelamento_valepay();//TODO CALCULA A TAXA LOGO APOS CALCULAR A TAXA
                    }

                    if ($('input[name="tipoCobrancaSinal"]:checked').attr('tipo') === 'cartao_credito_transparent_valepay') {
                        popular_table_parcelamento_valepay();//TODO CALCULA A TAXA LOGO APOS CALCULAR A TAXA
                    }

                });
        }
    }

    var bt = $(".botoes_avanca_confirma");

    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();

        if (windowpos <= ($(document).height() - $(window).height() - 120)) {
            //bt.css('position', 'fixed').css('zIndex', 2);
        } else {
            //bt.css('position', 'sticky').css('bottom', 0).css('zIndex', 2);
        }
    });

    var audio_error = new Audio('<?=$assets?>sounds/sound3.mp3');

    function selecionar_assento(tag) {

        $('#div_assentos_marcacao_desmarca').hide();
        $('#botoes-proximo').hide();

        let assento     = tag.getAttribute('data-position-order_name');
        let valor_extra = tag.getAttribute('valor_extra');
        let name_class  = tag.getAttribute('name_class');

        if (cliente_selecionado_assento == null) {
            alert("Nenhum passageiro selecionado para atribuir um assento.");

            tag.classList.remove('seat-reservado');
            tag.classList.add(name_class);

            $('#botoes-proximo').show();

            return;
        }

        if ($('#div_assentos_marcacao_confirma').is(':visible')) {

            let assento_selecionado = assento_selecionado_tag.getAttribute('data-position-order_name');

            if (assento !== assento_selecionado) {
                tag.classList.remove('seat-reservado');
                tag.classList.add(name_class);
            }

            return;
        }

        if (assento_selecionado_tag !== null) {

            let assento_selecionado = assento_selecionado_tag.getAttribute('data-position-order_name');
            let name_class_selecionado  = assento_selecionado_tag.getAttribute('name_class');

            if (assento !== assento_selecionado) {

                var assentoJaSelecionado = j_passageiros_selecionados_assento.find(function(cliente) {
                    return cliente.assento === assento && cliente.id !== cliente_selecionado_assento;
                });

                if (assentoJaSelecionado) {//verificar se o assento ja foi selecionado para outro passageiro da venda
                    audio_error.play();
                    $('#botoes-proximo').show();

                    return;
                } else {
                    assento_selecionado_tag.classList.remove('seat-reservado');
                    assento_selecionado_tag.classList.add(name_class_selecionado);

                    $('#assento' + cliente_selecionado_assento).val('');
                    $('#valor_extra_assento' + cliente_selecionado_assento).val('');
                }
            }
        } else {

            var assentoJaSelecionado = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.assento === assento && cliente.id !== cliente_selecionado_assento;
            });

            if (assentoJaSelecionado) {//verificar se o assento ja foi selecionado para outro passageiro da venda
                audio_error.play();
                $('#botoes-proximo').show();

                return;
            }
        }

        let clienteName = $('#assento_dependente' + cliente_selecionado_assento).html();
        let nomePartes = clienteName.trim().split(' ');

        if (nomePartes.length >= 2) {
            let primeiroNome = nomePartes[0];
            let ultimoNome = nomePartes[nomePartes.length - 1];

            $('#pop_up_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
            $('#pop_down_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
        }

        $('#pop_up_seleciona_assento_poltrona').html('Assento ' + assento);
        $('#pop_down_seleciona_assento_poltrona').html('Assento ' + assento);

        if (!isNaN(valor_extra) && parseFloat(valor_extra) > 0) {
            $('#pop_up_seleciona_assento_compra_extra_valor').html('R$'+formatMoney(valor_extra));

            $('.div_compra_extra').show();
        }

        $('#div_assentos_marcacao_confirma').show(300);

        assento_selecionado_tag = tag;//todo selecionar tag do assento para o cliente selecionado
    }

    function confirmarAssentoMarcado() {

        $('#div_assentos_marcacao_confirma').hide(100, function() {

            $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();
            $('#botoes-proximo').show();
            $('.div_compra_extra').hide();

            if (assento_selecionado_tag == null) {
                return;
            }

            let assento     = assento_selecionado_tag.getAttribute('data-position-order_name');
            let valor_extra = assento_selecionado_tag.getAttribute('valor_extra');

            $('#assento' + cliente_selecionado_assento).val(assento);
            $('#valor_extra_assento' + cliente_selecionado_assento).val(valor_extra);

            // Adiciona a nova opção ao select
            $("#select_assento_selecionadodependente" + cliente_selecionado_assento).append($("<option></option>").val(assento).text(assento));

            var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.id === cliente_selecionado_assento;
            });

            if (clienteExist) {
                clienteExist.assento        = assento;
                clienteExist.valor_extra    = valor_extra;
                clienteExist.tag            = assento_selecionado_tag;

                $('.cc_assento_selecionado').next().click();//TODO PROXIMO ASSENTO
            }

            calcularTotalPagar(false);
        });
    }

    function desmarcarAssento() {

        $('#div_assentos_marcacao_desmarca').hide(100);

        $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();

        let name_class_selecionado  = assento_selecionado_tag.getAttribute('name_class');

        assento_selecionado_tag.classList.remove('seat-reservado');
        assento_selecionado_tag.classList.add(name_class_selecionado);
        assento_selecionado_tag = null;

        $('#assento' + cliente_selecionado_assento).val('');
        $('#valor_extra_assento' + cliente_selecionado_assento).val('');

        var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
            return cliente.id === cliente_selecionado_assento;
        });

        if (clienteExist) {
            clienteExist.assento        = '';
            clienteExist.valor_extra    = '';
            clienteExist.tag            = null;
        }

        $('#botoes-proximo').show();
        calcularTotalPagar(false);
    }

    function close_marcacao() {

        $('#div_assentos_marcacao_confirma').hide(100);
        $('.div_compra_extra').hide();

        $("#select_assento_selecionadodependente" + cliente_selecionado_assento).empty();

        let name_class_selecionado  = assento_selecionado_tag.getAttribute('name_class');

        assento_selecionado_tag.classList.remove('seat-reservado');
        assento_selecionado_tag.classList.add(name_class_selecionado);
        assento_selecionado_tag = null;

        $('#assento' + cliente_selecionado_assento).val('');
        $('#valor_extra_assento' + cliente_selecionado_assento).val('');

        var clienteExist = j_passageiros_selecionados_assento.find(function(cliente) {
            return cliente.id === cliente_selecionado_assento;
        });

        if (clienteExist) {
            clienteExist.assento        = '';
            clienteExist.valor_extra    = '';
            clienteExist.tag            = null;
        }

        $('#botoes-proximo').show();
        calcularTotalPagar(false);

    }

    function close_desmarcar() {
        $('#div_assentos_marcacao_desmarca').hide(100);
        $('#botoes-proximo').show();
        calcularTotalPagar(false);
    }


    function onlick_events_selecionar_cliente_assento() {

        $('.cc_assentos').click(function(event){

            let id = $(this).attr('sel').replace("dependente", "");

            if ($('#div_assentos_marcacao_confirma').is(':visible')) {
                if (cliente_selecionado_assento !== id) {
                    return;
                }
            }

            $('.cc_assentos').addClass('cc_assentos_opaco');
            $('.cc_assentos').removeClass('cc_assento_selecionado')

            $(this).removeClass('cc_assentos_opaco');
            $(this).addClass('cc_assento_selecionado');

            cliente_selecionado_assento = id;

            let clienteName = $('#assento_dependente' + id).html();

            var novo_passageiro_selecionado = {
                id: id,
                nome_cliente: clienteName,
                valor_extra: '',
                assento: "",
                tag: null,
            };

            var clienteEncontrado = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.id === cliente_selecionado_assento;
            });

            if (!clienteEncontrado) {
                j_passageiros_selecionados_assento.push(novo_passageiro_selecionado);
                assento_selecionado_tag = null;
            } else {
                assento_selecionado_tag = clienteEncontrado.tag;
                if (clienteEncontrado.assento !== '') {
                    $('#botoes-proximo').hide();
                    $('#div_assentos_marcacao_desmarca').show(300);
                    $('#pop_down_seleciona_assento_poltrona').html('Assento '+clienteEncontrado.assento);
                }
            }

            let nomePartes = clienteName.trim().split(' ');

            if (nomePartes.length >= 2) {
                let primeiroNome = nomePartes[0];
                let ultimoNome = nomePartes[nomePartes.length - 1];

                $('#pop_up_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
                $('#pop_down_seleciona_assento_name').html(primeiroNome+' ' +ultimoNome);
            }
        });

        if ($('.cc_assentos')[0] !== undefined) {
            $('.cc_assentos')[0].click();//TODO CLICA NO PRIMEIRO PASSAGEIRO PARA INICIAR A SELECAO DE POLTRONAS
        }
    }

    function carregar_onibus() {
        <?php if ($transporteDisponivel){?>
            $('#iframe_onibus').attr('src', site.base_url + "bus/marcacao/" + <?php echo $produto->id;?> + '/' + <?php echo $programacao->id;?> + '/<?php echo $transporteDisponivel->id;?>');
        <?php }?>
    }

    function bloqueio_assento_site() {

        <?php if ($transporteDisponivel){?>

            var assentoPreenchido = j_passageiros_selecionados_assento.find(function(cliente) {
                return cliente.assento !== '';
            });

            if (assentoPreenchido) {

                $.blockUI(
                    {
                        message: '<h3><img src="<?php echo base_url() ?>assets/images/carregando.gif" /></h3>',
                        css: {width: '70%', left: '15%'}
                    }
                );

                var jsonData = JSON.stringify({
                    tipo_transporte_id: <?php echo $transporteDisponivel->id;?>,
                    programacao_id: <?php echo $programacao->id;?>,
                    product_id: <?php echo $produto->id;?>,
                    assentos: j_passageiros_selecionados_assento,
                });

                $.ajax({
                    type: "get",
                    data: {
                        dados: jsonData,
                        token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                    },
                    url: site.base_url + "bus/bloqueio_assento_site",
                    contentType: 'application/json',
                    dataType: 'json', // Define o tipo de dados que você espera receber do servidor
                    success: function (retorno) {

                        $.unblockUI();

                        if (retorno.ocupado) {

                            $.confirm({
                                title: 'Atençao!',
                                closeIcon: true,
                                content: retorno.error,
                                type: 'red',
                                typeAnimated: true,
                                buttons: {
                                    ok: function () {
                                        console.log('ok');
                                    },
                                }
                            });

                            $('.backward').click();
                        }
                    }
                });
            }
        <?php } ?>
    }

    function desbloquear_todos_assentos_session_card() {
        <?php if ($transporteDisponivel){?>
            $.ajax({
                type: "get",
                url: site.base_url + "bus/desbloquear_todos_assentos_session_card",
                data: {
                    token: '<?php echo $this->session->userdata('cnpjempresa');?>',
                },
                contentType: 'application/json',
                dataType: 'json',
                success: function (retorno) {}
            });
        <?php } ?>
    }

    <?php if ($transporteDisponivel){?>
        window.addEventListener('beforeunload', desbloquear_todos_assentos_session_card);
    <?php } ?>


    // URL do PDF
    var url_pdf = '';

    <?php if ($plugsign) {?>
        let pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            canvas = document.getElementById('pdfViewer'),
            ctx = canvas.getContext('2d');

        let scale = setDynamicScale();
        const scaleStep = 0.1;

        function createPDF() {
            <?php if ($plugsign) {?>

                $.blockUI({message: '<h3><img style="width: 150px;height: 150px;" src="<?php echo base_url() ?>assets/images/contract.gif" /> <br/>Gerando Contrato....</h3>', css: {width: '70%', left: '15%'}})

                let formData = $('#wrapped').serialize();

                $.ajax({
                    type: "POST",
                    url: '<?php echo base_url();?>apputil/createPDF',
                    data: formData + '&token=<?php echo $this->session->userdata('cnpjempresa');?>',
                    dataType: 'json',
                    success: function (resultado) {

                        if (resultado.success) {

                            url_pdf = '<?=base_url().'assets/uploads/documents/'.$this->session->userdata('cnpjempresa').'/';?>' + resultado.file_name;

                            //$('#contrato_pdf').attr('src', '<?=base_url().'assets/uploads/documents/'.$this->session->userdata('cnpjempresa').'/';?>' + resultado.file_name);

                            if (resultado.extensao === 'docx') {
                                $('#file_pdf').val(resultado.file_name_docx);
                            } else {
                                $('#file_pdf').val(resultado.file_name);
                            }

                            pdfjsLib.getDocument(url_pdf).promise.then(function(pdfDoc_) {
                                pdfDoc = pdfDoc_;
                                document.getElementById('totalPages').textContent = pdfDoc.numPages;
                                setDynamicScale();
                                renderPage(pageNum);
                            });

                        }

                        $.unblockUI();
                    },
                    error: function(error) {
                        console.error(error);
                        $.unblockUI();
                    }
                });
            <?php } ?>
        }

        function setDynamicScale() {

            const width = window.screen.width;

            let scale;

            if (width <= 600) {              // Celulares
                scale = 0.6;
            } else if (width <= 1366) {       // Tablets e laptops menores
                scale = 0.8;
            } else if (width <= 1600) {       // Laptops de 15 polegadas
                scale = 0.9;
            } else if (width <= 1920) {       // Laptops de 17 polegadas
                scale = 1.2;
            } else {                          // Monitores maiores
                scale = 1.4;
            }

            return scale;
        }


        function renderPage(num) {

            pageRendering = true;

            pdfDoc.getPage(num).then(function(page) {
                let viewport = page.getViewport({ scale: scale  });
                canvas.height = viewport.height;
                canvas.width = viewport.width;

                // Renderiza a página no canvas
                let renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                };
                let renderTask = page.render(renderContext);

                renderTask.promise.then(function() {
                    pageRendering = false;

                    if (pageNumPending !== null) {
                        renderPage(pageNumPending);
                        pageNumPending = null;
                    }
                });
            });

            document.getElementById('currentPage').textContent = num;
        }

        // Muda para a página desejada
        function queueRenderPage(num) {
            if (pageRendering) {
                pageNumPending = num;
            } else {
                renderPage(num);
            }
        }

        // Exibe a página anterior
        document.getElementById('prevPage').addEventListener('click', function(e) {
            e.preventDefault();
            if (pageNum <= 1) {
                return;
            }
            pageNum--;
            queueRenderPage(pageNum);
        });

        // Exibe a próxima página
        document.getElementById('nextPage').addEventListener('click', function(e) {
            e.preventDefault();

            if (pageNum >= pdfDoc.numPages) {
                return;
            }
            pageNum++;
            queueRenderPage(pageNum);
        });

        // Botões de zoom
        document.getElementById('zoomIn').addEventListener('click', function(e) {
            e.preventDefault();

            scale += scaleStep; // Aumenta a escala
            renderPage(pageNum); // Re-renderiza a página atual
        });

        document.getElementById('zoomOut').addEventListener('click', function(e) {
            e.preventDefault();

            if (scale > scaleStep) { // Evita escala negativa
                scale -= scaleStep; // Diminui a escala
                renderPage(pageNum); // Re-renderiza a página atual
            }
        });

    <?php } ?>
</script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>

</body>
</html>

<?php
function getDataMaximaParaRealizarOhPagamento($diasMaximoPagamentoAntesViagem, $programacao) {

    if ($diasMaximoPagamentoAntesViagem > 0) {
        $dtMaximaDePagamento =  date('Y-m-d', strtotime("-".$diasMaximoPagamentoAntesViagem." day", strtotime($programacao->dataSaida)));
    } else {
        $dtMaximaDePagamento = $programacao->dataSaida;//data da saida da viagem
    }

    //se a data de hoje for maior que a data maxima do vencimento na semana do vencimento
    if (strtotime(date('Y-m-d')) > strtotime($dtMaximaDePagamento)) {
        $dtMaximaDePagamento = date('Y-m-d');
    }

    return $dtMaximaDePagamento;
}

function getDependentes($contador, $produto, $programacao, $fv, $assets, $tipoHospedagem, $qty_client_records) {
    $html = '';
    for ($cr = 0; $cr<=$qty_client_records; $cr++){
        $html .= getDependente($cr, $contador, $produto, $programacao, $fv, $assets, $tipoHospedagem);
    }
    return $html;
}

function getDependente($sequencial, $contador, $produto, $programacao, $fv, $assets, $tipoHospedagem = null)
{

    $faixaId = $fv->faixaId;
    $nomeFaixa = $fv->name;
    $descontarVaga = $fv->descontarVaga;
    $valor = $fv->valor;

    if ($produto->cat_precificacao == 'preco_por_data') {
        $valor = $programacao->preco;
    }

    $obrigaCPF  = $fv->obrigaCPF;
    $obrigaOrgaoEmissor  = $fv->obrigaOrgaoEmissor;

    $captarCPF = $fv->captarCPF;
    $captarDocumento = $fv->captarDocumento;
    $captarOrgaoEmissor = $fv->captarOrgaoEmissor;
    $formato_cpf = $fv->formato_cpf;

    $captarDataNascimento = $fv->captarDataNascimento;
    $captarEmail = $fv->captarEmail;
    $captarProfissao = $fv->captarProfissao;
    $captarInformacoesMedicas = $fv->captarInformacoesMedicas;
    $captarNomeSocial = $fv->captarNomeSocial;
    $ehFaixaDependente = $fv->faixaDependente;
    $captar_observacao = $fv->captar_observacao;
    $placeholder_observacao = $fv->placeholder_observacao;

    $seq = $sequencial;

    if ($tipoHospedagem) {
        $sequencial = $sequencial.'_'.$tipoHospedagem->id;
    }

    $sequencial_id = $fv->id.'_'.$sequencial;

    $html = '';

    /*INICIO*/
    $html .= '<div id="dependente'.$fv->id.'_'.$sequencial.'">
                <fieldset class="cc_fieldset" id="cc_fieldset_'.$fv->id.'_'.$sequencial.'">
                    <div onclick=openDigitacaoDados("'.$fv->id.'_'.$sequencial.'"); style="padding: 15px;">
                        <span class="fa fa-user"></span> <span nome_faixa="'.$seq.'º '.$fv->name.'" id="div_name_'.$fv->id.'_'.$sequencial.'">'.$seq.'º | '.$fv->name.'</span>
                        <img class="imgfilters" id="dependente_dados'.$fv->id.'_'.$sequencial.'-img" src="'.$assets.'images/abrirSubTitulo-o.gif">
                    </div>
                    <div  id="dependente_dados'.$fv->id.'_'.$sequencial.'" style="display: none;padding: 15px;">';

    $html .= ' 
                    <input class="hidden" type="hidden" name="obrigaCPFH"               id="obrigaCPF' . $sequencial_id . '"            value="'.$obrigaCPF.'" />
                    <input class="hidden" type="hidden" name="obrigaOrgaoEmissorH"      id="obrigaOrgaoEmissor' . $sequencial_id . '"   value="'.$obrigaOrgaoEmissor.'" />
                    <input class="hidden" type="hidden" name="obrigaDataNascimentoH"    id="obrigaDataNascimento' . $sequencial_id . '" value="'.$captarDataNascimento.'" />
                    <input class="hidden" type="hidden" name="obrigaDocumentoH"         id="obrigaDocumento' . $sequencial_id . '"      value="'.$captarDocumento.'" />
                    <input class="hidden" type="hidden" name="obrigaEmailH"             id="obrigaEmail' . $sequencial_id . '"          value="'.$captarEmail.'" />
                    <input class="hidden" type="hidden" name="faixaDependenteH"         id="faixaDependente' . $sequencial_id . '"      value="'.$ehFaixaDependente.'" />

                    <input class="hidden" type="hidden" id="tipoFaixaEtariaId' . $sequencial_id . '"        name="tipoFaixaEtariaId[]"     value="'.$faixaId.'" />
                    <input class="hidden" type="hidden" id="tipoFaixaEtariaValor' . $sequencial_id . '"     name="tipoFaixaEtariaValor[]"  value="'.$valor.'" />
                    <input class="hidden" type="hidden" id="tipoFaixaEtariaNome' . $sequencial_id . '"      name="tipoFaixaEtariaNome[]"   value="'.$nomeFaixa.'" />
                    <input class="hidden" type="hidden" id="descontarVaga' . $sequencial_id . '"            name="descontarVaga[]"         value="'.$descontarVaga.'" />
                    <input class="hidden" type="hidden" id="sequencialCompra' . $sequencial_id . '"         name="sequencialCompra[]"      value="'.$contador.'" />';

    /*CPF*/
    $html .= '   <div class="form-group" ' . ($captarCPF == false ? 'style="display: none;"' : '')  . '>
                    <div class="row">
                        <div class="col-12">';

    if ($formato_cpf) {
        $html .= '          <label class="label_clientes">CPF</label>';

        $html .= '              <input type="tel" class="form-control valid " 
                                    id="cpfDependente' . $sequencial_id . '"
                                    tipo="CPF" 
                                    name="cpfDependente[]" 
                                    sequencial="'.$sequencial_id.'" 
                                    placeholder="xxx.xxx.xxx-xx">';
    } else {
        $html .= '          <label class="label_clientes">CNPJ</label>';

        $html .= '              <input type="tel" class="form-control valid " 
                                    id="cpfDependente' . $sequencial_id . '"
                                    tipo="CNPJ"  
                                    name="cpfDependente[]" 
                                    sequencial="'.$sequencial_id.'" 
                                    placeholder="xx.xxx.xxx/xxx-xx">';
    }

    $html .= '              <span class="carregando"  style="display: none;font-size: 10px;"></span>
                       </div>
                    </div>              
                </div>';


    if (!$formato_cpf) {

        /*Nome Completo e Sexo*/
        $html .='   <div class="form-group">
                        <div class="row">
                            <div class="col-12">
                                <label class="label_clientes">Nome da Agência</label>
                                <input type="text" style="text-transform: uppercase;" class="form-control nomesDependente" sequencial="'.$sequencial_id.'" id="nomeDependente' . $sequencial_id . '" name="nomeDependente[]" placeholder="Digite o nome da Agência" required="required">
                            </div>
                             <div class="col-4" style="display: none;">
                                <label class="label_clientes">Sexo <br/><small>&nbsp;</small></label>
                                <select class="combo" id="sexoDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="sexoDependente[]">
                                     <option value="OUTROS" selected="selected">OUTROS</option>
                                </select>
                            </div>
                        </div>
                    </div>';

        /*Razão Social quando capta CNPJ*/
        $html .= '<div class="form-group" style="display: none;">
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Nome Fanstasia</label>
                            <input type="text" class="form-control" id="company_name' . $sequencial_id . '" sequencial="' . $sequencial_id . '" name="company_name[]" placeholder="">
                        </div>
                    </div>
                </div>';

        /*Nome do representante da empresa*/
        $html .= '<div class="form-group">
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Nome Do Responsável</label>
                            <input type="text" class="form-control" id="nome_responsavel' . $sequencial_id . '" sequencial="' . $sequencial_id . '" required="required" name="nome_responsavel[]" placeholder="DIGITE O NOME DO RESPONSÁVEL DA AGÊNCIA">
                        </div>
                    </div>
                </div>';
    } else {
        /*Nome Completo e Sexo*/
        $html .='       <div class="form-group">
                        <div class="row">
                            <div class="col-8">
                                <label class="label_clientes">Nome Completo<br/><small>(SEM ABREVIAÇÕES)</small></label>
                                <input type="text" style="text-transform: uppercase;" class="form-control nomesDependente" sequencial="'.$sequencial_id.'" id="nomeDependente' . $sequencial_id . '" name="nomeDependente[]" placeholder="Conforme documento" required="required">
                            </div>
                             <div class="col-4">
                                <label class="label_clientes">Sexo <br/><small>&nbsp;</small></label>
                                <select class="combo" id="sexoDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="sexoDependente[]">
                                     <option value="">Selecionar</option>
                                     <option value="FEMININO">FEMININO</option>
                                     <option value="MASCULINO">MASCULINO</option>
                                     <option value="OUTROS">OUTROS</option>
                                </select>
                            </div>
                        </div>
                    </div>';

        /*Razão Social quando capta CNPJ*/
        $html .= '<div class="form-group" style="display: none;">
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Razão Social</label>
                            <input type="text" class="form-control" id="company_name' . $sequencial_id . '" sequencial="' . $sequencial_id . '" name="company_name[]" placeholder="">
                        </div>
                    </div>
                </div>';

        /*Nome do representante da empresa*/
        $html .= '<div class="form-group" style="display: none;">
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Nome Responsável</label>
                            <input type="text" class="form-control" id="nome_responsavel' . $sequencial_id . '" sequencial="' . $sequencial_id . '" name="nome_responsavel[]" placeholder="">
                        </div>
                    </div>
                </div>';
    }

    /*Nome Social*/
    $html .='   <div class="form-group" ' . ($captarNomeSocial == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Nome Social</label>
                            <input type="text" class="form-control" id="social_name' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="social_name[]" placeholder="">
                        </div>
                    </div>
                </div>';

    /*Tipo de documento */
    $html .= '<div class="form-group" ' . ($captarDocumento == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Tipo Documento</label>
                            <div class="styled-select clearfix">
                                <select class="combo tipo_documento" id="tipo_documentoDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" contador="' . $sequencial_id . '" name="tipo_documentoDependente[]">
                                    <option value="rg">RG</option>
                                    <option value="novo rg">(NOVO RG) CPF</option>
                                    <option value="passaporte">Passaporte</option>
                                    <option value="RNE">RNE</option>
                                    <option value="CNH">CNH</option>
                                    <option value="CERTIDAO">Certidão de Nascimento</option>
                                    <option value="PIS">PIS</option>
                                </select>
                            </div>
                        </div>                       
                    </div>               
                </div>';

    /*Numero documento*/
    $html .='   <div class="form-group" ' . ($captarDocumento == false ? 'style="display: none;"' : '')  . '>
                    <div class="row">
                        <div class="col-12" id="div_documento' . $sequencial_id . '">
                            <label class="label_clientes">Nº Documento</label>
                            <input type="text" class="form-control" id="rgDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="rgDependente[]" placeholder="Número">
                        </div>
                    </div>
                </div>';

    /*Orgao emissor*/
    $html .= '<div class="form-group" ' . ($captarDocumento == false || $captarOrgaoEmissor == false ? 'style="display: none;"' : '')  . '>
                <div class="row">       
                    <div class="col-12" id="div_orgao_emissor' . $sequencial_id . '">
                        <label class="label_clientes">Orgão Emissor</label>
                        <input type="text" class="form-control valid " id="orgaoEmissorDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="orgaoEmissorDependente[]" placeholder="Ex. SSP/SC">
                    </div>
                </div>
              </div>';

    /*Data de Nascimento*/
    $html .= '    <div class="form-group" ' . ($captarDataNascimento == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12"><label class="label_clientes">Data de Nascimento</label></div>
                        <div class="col-4">
                            <div class="styled-select clearfix">
                                <select class="combo" id="diaDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="diaDependente[]">
                                    <option value="">Dia</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4" style="padding-right: 6px;padding-left: 3px;">
                            <div class="styled-select clearfix">
                                <select class="combo" id="mesDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="mesDependente[]">
                                    <option value="">Mês</option>
                                    <option value="01">Janeiro</option>
                                    <option value="02">Fevereiro</option>
                                    <option value="03">Março</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Maio</option>
                                    <option value="06">Junho</option>
                                    <option value="07">Julho</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Setembro</option>
                                    <option value="10">Outubro</option>
                                    <option value="11">Novembro</option>
                                    <option value="12">Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="styled-select clearfix">';
    $html .= '<select class="combo" id="anoDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="anoDependente[]">
                                                                    <option value="">Ano</option>';
    $contadorD = (int)date('Y');
    $menorAno = 1905;
    for ($i = $contadorD; $i > $menorAno; $i--) {
        $html .= '<option value="' . $i . '">' . $i . '</option>';
    }
    $html .= '</select>
                            </div>
                        </div>
                    </div>
                </div>';

    /*Telefone celular + telefone de emergencia*/
    $html .= '  <div class="form-group" ' . ($ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12"> 
                            <span style="color: #428BCA;"><span class="fa fa fa-phone"> </span> INFORMAÇÕES DE CONTATO</span>   
                        </div>      
                    </div>
                </div>
                <div class="form-group" ' . ($ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-6">
                            <label class="label_clientes">Tel.  Agência</label>
                            <input type="tel" class="form-control celular" id="celularDependente' . $sequencial_id . '" ' . ($ehFaixaDependente == false ? 'minlength="14"' : '') . ' sequencial="'.$sequencial_id.'" name="celularDependente[]"  placeholder="ex.: (99) 9999-9999">
                        </div>
                        <div class="col-6">
                            <label class="label_clientes">Tel. Responsável</label>
                            <input type="text" class="form-control celular" id="telefone_emergenciaDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="telefone_emergenciaDependente[]" placeholder="ex.: (99) 9999-9999">
                        </div>
                    </div>
                </div>';

    /*E-mail*/
    $html .= '<div class="form-group" ' . ($captarEmail == false || $ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                <div class="row">
                    <div class="col-12">
                        <label class="label_clientes">E-mail</label>
                        <input type="email" class="form-control" id="emailDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="emailDependente[]" placeholder="email@dominio.com.br" placeholder="ex.: (99) 9999-9999">
                    </div>
                </div>
            </div>';

    if ($captarProfissao || $captarInformacoesMedicas) {
        $html .= '<div class="form-group">
                    <div class="row">
                        <div class="col-12"> 
                            <span style="color: #428BCA;"><span class="fa fa fa-plus-circle"> </span> DADOS ADICIONAIS</span>   
                        </div>      
                    </div>
                </div>';
    }

    /*Profissao*/
    $html .= '<div class="form-group" ' . ($captarProfissao == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Profissão</label>
                            <input type="text" class="form-control" id="profissao' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="profissaoDependente[]" placeholder="Ex:. Autônomo">
                        </div>
                    </div>
                </div>';

    /*Alguma doenca*/
    $html .= '<div class="form-group" ' . ($captarInformacoesMedicas == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Alguma Doença Pré-existente ou Medicamento a Informar?</label>
                            <textarea type="text" class="form-control" id="doenca_informar' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="doenca_informar[]" placeholder=""></textarea>
                        </div>
                    </div>
                </div>';

    /*Assento*/
    $html .= '<div class="form-group" style="display: none;">
                <div class="row">
                    <div class="col-12">
                        <label class="label_clientes">Assento</label>
                        <input type="tel" class="form-control" id="assento' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="assento[]" placeholder="">
                    </div>
                </div>
            </div>';


    /*Valor Extra Assento*/
    $html .= '<div class="form-group" style="display: none;">
                <div class="row">
                    <div class="col-12">
                        <label class="label_clientes">Valor Extra Assento</label>
                        <input type="number" class="form-control" id="valor_extra_assento' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="valor_extra_assento[]" placeholder="">
                    </div>
                </div>
            </div>';

    if ($captar_observacao) {
        $html .= '<div class="form-group">
                    <div class="row">
                        <div class="col-12"> 
                            <span style="color: #428BCA;"><span class="fa fa fa-comment"> </span> INFORMAÇÕES ADICIONAIS</span>   
                        </div>      
                    </div>
                </div>';
    }

    /*Informacoes do item de venda*/
    $html .= '<div class="form-group" ' . ($captar_observacao == false ? 'style="display: none;"' : '') .'>
                <div class="row">
                    <div class="col-12">
                        <label class="label_clientes">Observações</label>
                        <textarea class="form-control" style="height: 110px;" id="observacao_item' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="observacao_item[]" placeholder="'. $placeholder_observacao .'"></textarea>
                    </div>
                </div>
            </div>';

    $html .='  <div class="form-group" '. ( ($ehFaixaDependente == true || coletarPagador($produto) == true || $formato_cpf == false) ? 'style="display: none;"' : '') .'>
                    <div class="row">
                         <div class="col-12">
                         <label style="font-weight: 500;color: #dc3545;" class="label_clientes">VOCÊ É RESPONSÁVEL PELA COMPRA?</label>
                           <select class="combo responsavel_compra" id="responsavelPelaCompra' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="responsavelPelaCompra[]">
                                 <option value="NAO">NÃO</option>
                                 <option value="SIM">SIM</option>
                            </select>
                         </div>       
                     </div>        
                </div>';

    $html .= '      <div class="form-group" style="margin-top: 25px;">
                            <div class="row">
                                <div class="col-12">
                                    <button type="button" onclick=salvarCliente('.$fv->id.',"' . $sequencial. '"); class="btn btn-default" style="color: #ffffff;width: 100%;height: 100%;border-radius: 0px;border: 1px solid #d2d8dd;background: #e58801"> <span class="fa fa-save"></span> SALVAR</button>
                                </div>
                            </div>
                    </div>';

    /*FIM*/
    $html .= '     </div>      
                </fieldset>
              </div>';

    return $html;
}


function getPagador($fv)
{

    $sequencial = 'pagador';
    $obrigaCPF  = $fv->obrigaCPF;
    $obrigaOrgaoEmissor  = $fv->obrigaOrgaoEmissor;
    $captarCPF = $fv->captarCPF;
    $captarDocumento = $fv->captarDocumento;
    $captarOrgaoEmissor = $fv->captarOrgaoEmissor;
    $captarDataNascimento = $fv->captarDataNascimento;
    $captarEmail = $fv->captarEmail;
    $captarProfissao = $fv->captarProfissao;
    $captarInformacoesMedicas = $fv->captarInformacoesMedicas;
    $captarNomeSocial = $fv->captarNomeSocial;
    $ehFaixaDependente = $fv->faixaDependente;
    $sequencial_id = $fv->id.'_'.$sequencial;

    $html = '';

    /*INICIO*/
    $html .= '<div class="cart_cartao_round" id="dependente'.$fv->id.'_'.$sequencial.'">';

    $html .= ' 
            <input class="hidden" type="hidden" name="obrigaCPFH"               id="obrigaCPF' . $sequencial_id . '"            value="'.$obrigaCPF.'" />
            <input class="hidden" type="hidden" name="obrigaOrgaoEmissorH"      id="obrigaOrgaoEmissor' . $sequencial_id . '"   value="'.$obrigaOrgaoEmissor.'" />
            <input class="hidden" type="hidden" name="obrigaDataNascimentoH"    id="obrigaDataNascimento' . $sequencial_id . '" value="'.$captarDataNascimento.'" />
            <input class="hidden" type="hidden" name="obrigaDocumentoH"         id="obrigaDocumento' . $sequencial_id . '"      value="'.$captarDocumento.'" />
            <input class="hidden" type="hidden" name="obrigaEmailH"             id="obrigaEmail' . $sequencial_id . '"          value="'.$captarEmail.'" />
            <input class="hidden" type="hidden" name="faixaDependenteH"         id="faixaDependente' . $sequencial_id . '"      value="'.$ehFaixaDependente.'" />';

    /*CPF*/
    $html .= '   <div class="form-group" ' . ($captarCPF == false ? 'style="display: none;"' : '')  . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">CPF</label>
                            <input type="tel" class="form-control valid " id="cpfDependente' . $sequencial_id . '" name="cpfDependentePagador[]" sequencial="'.$sequencial_id.'" placeholder="xxx.xxx.xxx-xx">
                            <span class="carregando"  style="display: none;font-size: 10px;"></span>
                       </div>
                    </div>              
                </div>';

    /*Nome Completo e Sexo*/
    $html .='       <div class="form-group">
                        <div class="row">
                            <div class="col-8">
                                <label class="label_clientes">Nome Completo</label>
                                <input type="text" style="text-transform: uppercase;" class="form-control nomesDependente" sequencial="'.$sequencial_id.'" id="nomeDependente' . $sequencial_id . '" name="nomeDependentePagador[]" placeholder="Conforme documento" required="required">
                            </div>
                             <div class="col-4">
                                <label class="label_clientes">sexo</label>
                                <select class="combo" id="sexoDependente' . $sequencial_id . '" name="sexoDependentePagador[]">
                                     <option value="">Selecionar</option>
                                     <option value="FEMININO">FEMININO</option>
                                     <option value="MASCULINO">MASCULINO</option>
                                     <option value="OUTROS">OUTROS</option>
                                </select>
                            </div>
                        </div>
                    </div>';

    /*Nome Social*/
    $html .='   <div class="form-group" ' . ($captarNomeSocial == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Nome Social</label>
                            <input type="text" class="form-control" id="social_name' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="social_namePagador[]" placeholder="">
                        </div>
                    </div>
                </div>';

    /*Tipo de documento */
    $html .= '<div class="form-group" ' . ($captarDocumento == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Tipo Documento</label>
                            <div class="styled-select clearfix">
                                <select class="combo tipo_documento" id="tipo_documentoDependente' . $sequencial_id . '" contador="' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="tipo_documentoDependentePagador[]">
                                    <option value="rg">R.G</option>
                                    <option value="passaporte">Passaporte</option>
                                    <option value="RNE">RNE</option>
                                    <option value="CNH">CNH</option>
                                    <option value="CERTIDAO">Certidão de Nascimento</option>
                                    <option value="PIS">PIS</option>
                                </select>
                            </div>
                        </div>                       
                    </div>               
                </div>';

    /*Numero documento*/
    $html .='   <div class="form-group" ' . ($captarDocumento == false ? 'style="display: none;"' : '')  . '>
                    <div class="row">
                        <div class="col-12" id="div_documento' . $sequencial_id . '">
                            <label class="label_clientes">Nº Documento</label>
                            <input type="text" class="form-control" id="rgDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="rgDependentePagador[]" placeholder="Número">
                        </div>
                    </div>
                </div>';

    /*Orgao emissor*/
    $html .= '<div class="form-group" ' . ($captarDocumento == false || $captarOrgaoEmissor == false ? 'style="display: none;"' : '')  . '>
                <div class="row">       
                    <div class="col-12" id="div_orgao_emissor' . $sequencial_id . '">
                        <label class="label_clientes">Orgão Emissor</label>
                        <input type="text" class="form-control valid " id="orgaoEmissorDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="orgaoEmissorDependentePagador[]" placeholder="Ex. SSP/SC">
                    </div>
                </div>
              </div>';

    /*Data de Nascimento*/
    $html .= '    <div class="form-group" ' . ($captarDataNascimento == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12"><label class="label_clientes">Data de Nascimento</label></div>
                        <div class="col-4">
                            <div class="styled-select clearfix">
                                <select class="combo" id="diaDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="diaDependentePagador[]">
                                    <option value="">Dia</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                    <option value="03">03</option>
                                    <option value="04">04</option>
                                    <option value="05">05</option>
                                    <option value="06">06</option>
                                    <option value="07">07</option>
                                    <option value="08">08</option>
                                    <option value="09">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4" style="padding-right: 6px;padding-left: 3px;">
                            <div class="styled-select clearfix">
                                <select class="combo" id="mesDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'"  name="mesDependentePagador[]">
                                    <option value="">Mês</option>
                                    <option value="01">Janeiro</option>
                                    <option value="02">Fevereiro</option>
                                    <option value="03">Março</option>
                                    <option value="04">Abril</option>
                                    <option value="05">Maio</option>
                                    <option value="06">Junho</option>
                                    <option value="07">Julho</option>
                                    <option value="08">Agosto</option>
                                    <option value="09">Setembro</option>
                                    <option value="10">Outubro</option>
                                    <option value="11">Novembro</option>
                                    <option value="12">Dezembro</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="styled-select clearfix">';
                                $html .= '<select class="combo" id="anoDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="anoDependentePagador[]">
                                                                                                <option value="">Ano</option>';
                                $contadorD = (int)date('Y');
                                $menorAno = 1905;
                                for ($i = $contadorD; $i > $menorAno; $i--) {
                                    $html .= '<option value="' . $i . '">' . $i . '</option>';
                                }
                                $html .= '</select>
                            </div>
                        </div>
                    </div>
                </div>';

    /*Telefone celular + telefone de emergencia*/
    $html .= '  <div class="form-group" ' . ($ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12"> 
                            <span style="color: #428BCA;"><span class="fa fa fa-phone"> </span> INFORMAÇÕES DE CONTATO</span>   
                        </div>      
                    </div>
                </div>
                <div class="form-group" ' . ($ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-6">
                            <label class="label_clientes">Celular</label>
                            <input type="tel" class="form-control celular" id="celularDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="celularDependentePagador[]" required="required" minlength="14" placeholder="ex.: (99) 9999-9999">
                        </div>
                        <div class="col-6">
                            <label class="label_clientes">Tel.Emergência</label>
                            <input type="text" class="form-control celular" id="telefone_emergenciaDependente' . $sequencial_id . '"  sequencial="'.$sequencial_id.'" name="telefone_emergenciaDependentePagador[]" placeholder="ex.: (99) 9999-9999">
                        </div>
                    </div>
                </div>';

    /*E-mail*/
    $html .= '<div class="form-group" ' . ($captarEmail == false || $ehFaixaDependente == true ? 'style="display: none;"' : '') . '>
                <div class="row">
                    <div class="col-12">
                        <label class="label_clientes">E-mail</label>
                        <input type="email" class="form-control" id="emailDependente' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="emailDependentePagador[]" required="required" placeholder="email@dominio.com.br" placeholder="ex.: (99) 9999-9999">
                    </div>
                </div>
            </div>';

    if ($captarProfissao || $captarInformacoesMedicas) {
        $html .= '<div class="form-group">
                    <div class="row">
                        <div class="col-12"> 
                            <span style="color: #428BCA;"><span class="fa fa fa-plus-circle"> </span> DADOS ADICIONAIS</span>   
                        </div>      
                    </div>
                </div>';
    }

    /*Profissao*/
    $html .= '<div class="form-group" ' . ($captarProfissao == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Profissão</label>
                            <input type="text" class="form-control" id="profissao' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="profissaoDependentePagador[]" placeholder="Ex:. Autônomo">
                        </div>
                    </div>
                </div>';

    /*Alguma doenca*/
    $html .= '<div class="form-group" ' . ($captarInformacoesMedicas == false ? 'style="display: none;"' : '') . '>
                    <div class="row">
                        <div class="col-12">
                            <label class="label_clientes">Alguma Doença Pré-existente ou Medicamento a Informar?</label>
                            <textarea type="text" class="form-control" id="doenca_informar' . $sequencial_id . '" sequencial="'.$sequencial_id.'" name="doenca_informarPagador[]" placeholder=""></textarea>
                        </div>
                    </div>
                </div>';

    /*FIM*/
    $html .= '</div>';

    return $html;
}
?>