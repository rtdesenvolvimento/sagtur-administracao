<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
    <meta name="author" content="Ansonika">
    <title><?php echo $configuracaoGeral->site_name;?> || RESERVA DE PASSAGENS</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA DE PASSAGENS" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA DE PASSAGENS">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>" />
    <meta property="og:title" content="<?php echo $configuracaoGeral->site_name;?>"  />
    <meta property="og:description" content="RESERVA ONLINE" />
    <meta property="og:site_name" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url"  content="<?php echo base_url().'appcompra/'.$this->session->userdata('cnpjempresa').'/'.$vendedor->id;?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <link rel="canonical" href="<?php echo base_url();?>">

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <?php

    $corPrincipalDoSite = '#ffffff';

    ?>
    <style>
        .content-left {
            background-color: <?=$corPrincipalDoSite; ?>;
            padding: 0;
        }

        .budget_slider {
            background-color: #f8f8f8;
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
            font-weight: bold;
        }
    </style>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-165016020-1');
    </script>
</head>

<body>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<div id="loader_form">
    <div data-loader="circle-side-2"></div>
</div><!-- /loader_form -->


<div class="container-fluid full-height">
    <div class="row row-height">

        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <!-- /top-wizard -->
                <!-- Leave for security protection, read docs for details -->
                <div id="middle-wizard" style="margin-top: 30%;">
                    <!-- Passo 1 - Dados do cliente -->
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34">Sua reserva foi confirmada com sucesso.</h2>
                        <div class="budget_slider">
                            <p>
                                Clique no botão abaixo para confirmar ao vendedor que finalizou seu cadastro
                            </p>
                        </div>
                        <a href="<?php echo base_url().'orcamento/confirmacao/'.$programacao->id.'/'.$vendedor->id.'/'.$venda->id.'?token='.$this->session->userdata('cnpjempresa'); ?>"
                           class="btn_1" style="width: 100%;margin-top: 10px;background: #30a113" target="_blank">
                            Envie Sua Reserva Pelo WhatsApp
                        </a>
                        <div class="budget_slider">
                            <p>
                                O preenchimento desse formulário de reserva só terá validade quando for enviado pelos vendedores da Trip Sampa após terem recebido seu comprovante de pagamento. Não seguir os procedimentos acima, automaticamente sua reserva será cancelada.
                            </p>
                        </div>
                        <?php if ($configuracaoGeral->isIntegracaoSite == 0) {?>
                            <a href="<?php echo base_url().'orcamento/'.$this->session->userdata('cnpjempresa').'/'.$programacao->id.'/'.$vendedor->id; ?>" class="btn_1" style="width: 100%;margin-top: 10px;background: #495057;">Fazer Uma Nova Reserva</a>
                        <?php } else { ?>
                            <a href="<?php echo $configuracaoGeral->url_site; ?>" class="btn_1" style="width: 100%;margin-top: 10px;background: #495057;">Fazer Uma Nova Reserva</a>
                        <?php } ?>
                        <p><br/>Atenciosamente,<br/><?php echo $configuracaoGeral->site_name;?></p>
                    </div>
                    <!-- /step-->
                </div>
                <!-- /middle-wizard -->
            </div>
            <!-- /Wizard container -->
        </div>
        <!-- /content-left -->

    </div>
    <!-- /row-->
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"><span></span></div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"><span></span></div>
<!-- /cd-overlay-content -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $aceistes_contrato_label = explode('@label@', $configuracaoGeral->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <strong> <?php echo $labelAceite[0];?></strong>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal terms -->
<div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                    <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                    <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                    <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                    <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                    <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                </div>
             </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/velocity.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/file-validator.js" type="text/javascript"></script>
<script src="<?= $assets ?>js/valida_cpf_cnpj.js" type="text/javascript"></script>

<!-- Wizard script -->
<script src="<?php echo base_url() ?>assets/appcompra/js/quotation_func.js"></script>

<script type="application/javascript">

    var CPF_PASSAGEIRO_PRINCIPAL = '';
    var listCPFS = [];

    jQuery(document).ready(function() {

        $('#celular').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('#cpf').keyup(function (event) {
            mascaraCpf( this, formataCPF );

            if ($('#cpf').val().length === 11) buscarCPF();
        });

        $('#cpf').blur(function (event) {
            buscarCPF();
        });

        $('#condicaoPagamento').change(function (e){
            getParcelamento();
        });

        removerAllWizardDependentes();
    });

    function removerAllWizardDependentes() {
        removerWizardDependentesAdulto(1);
        removerWizardDependentesAdulto(2);
        removerWizardDependentesAdulto(3);
        removerWizardDependentesAdulto(4);
        removerWizardDependentesAdulto(5);
        removerWizardDependentesAdulto(6);
        removerWizardDependentesAdulto(7);
        removerWizardDependentesAdulto(8);
        removerWizardDependentesAdulto(9);
        removerWizardDependentesAdulto(10);

        removerWizardDependentesCrianca(1);
        removerWizardDependentesCrianca(2);
        removerWizardDependentesCrianca(3);
        removerWizardDependentesCrianca(4);
        removerWizardDependentesCrianca(5);
        removerWizardDependentesCrianca(6);
        removerWizardDependentesCrianca(7);
        removerWizardDependentesCrianca(8);
        removerWizardDependentesCrianca(9);
        removerWizardDependentesCrianca(10);

        removerWizardDependentesBebe(1);
        removerWizardDependentesBebe(2);
        removerWizardDependentesBebe(3);
        removerWizardDependentesBebe(4);
        removerWizardDependentesBebe(5);
        removerWizardDependentesBebe(6);
        removerWizardDependentesBebe(7);
        removerWizardDependentesBebe(8);
        removerWizardDependentesBebe(9);
        removerWizardDependentesBebe(10);
    }

    function buscarCPF() {

        let tagCPF = $('#cpf');

        if (tagCPF.val() === '') return false;

        $('#nome').attr('readonly', false);

        if (!valida_cpf(tagCPF.val())) {
            alert('CPF inválido!');
            CPF_PASSAGEIRO_PRINCIPAL = '';

            tagCPF.val('');
            tagCPF.focus();

            return false;
        }

        tagCPF.val(formataCPF(tagCPF.val()));
        CPF_PASSAGEIRO_PRINCIPAL =tagCPF.val();

        verificaCadastroPassageiroPrincipal(tagCPF.val());
    }

    function verificaCadastroPassageiroPrincipal(cpf) {
        if (cpf  !== '') {
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>/linkcompra/getVerificaCustomeByCPF/'+cpf,
                data: {
                    cpf: $('#cpf').val()
                },
                dataType: 'json',
                success: function (pessoa) {
                    if (pessoa != null) {

                        $('#nome').val(pessoa.name);
                        $('#cpf').val(pessoa.vat_no);
                        $('#rg').val(pessoa.cf1);

                        if (pessoa.sexo === 'MASCULINO') {
                            jQuery("input[value='MASCULINO']").attr('checked', true);
                        } else if(pessoa.sexo === 'FEMININO') {
                            jQuery("input[value='FEMININO']").attr('checked', true);
                        }

                        $('#telefone').val(pessoa.phone);
                        $('#celular').val(pessoa.cf5);
                        $('#telefone_emergencia').val(pessoa.telefone_emergencia);
                        $('#alergia_medicamento').val(pessoa.alergia_medicamento);
                        $('#email').val(pessoa.email);

                        if (pessoa.data_aniversario !== '') {
                            let dia = pessoa.data_aniversario.split('-').reverse()[0];
                            let mes = pessoa.data_aniversario.split('-').reverse()[1];
                            let ano = pessoa.data_aniversario.split('-').reverse()[2];

                            $('#dia').val(dia);
                            $('#mes').val(mes);
                            $('#ano').val(ano);
                            $('select').niceSelect('update');
                        }

                        $('#nome').attr('readonly', true);
                        $('#celular').focus();
                    } else {

                        $('#alertaCadastroEncontado').hide();
                        $('#nome').attr('readonly', false);
                        $('#nome').focus();
                    }
                }
            });
        }
    }

    /* Máscaras ER */
    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCPF(cpf){
        //retira os caracteres indesejados...
        cpf = cpf.replace(/[^\d]/g, "");

        //realizar a formatação...
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }

    function calcularTotalPagar() {

        let valorPacoteBebe = getDouble($("#qtdBebesColo option:selected" ).attr('valor'));
        let valorPacoteAdulto = getDouble($("#qtdAdultos option:selected" ).attr('valor'));
        let valorPacoteCrianca = getDouble($("#qtdCriancas option:selected" ).attr('valor'));

        let qtdBebes = parseInt($('#qtdBebesColo').val());
        let qtdCriancas = parseInt($('#qtdCriancas').val());
        let qtdAdultos = parseInt($('#qtdAdultos').val());

        let valorGeral = getDouble($('input[name="tipoHospedagem"]:checked').attr('valor') );
        let valorBebeColo = getDouble($('input[name="tipoHospedagem"]:checked').attr('bebe') );
        let valorCrianca  = getDouble($('input[name="tipoHospedagem"]:checked').attr('crianca') );
        let valorAdulto   = getDouble($('input[name="tipoHospedagem"]:checked').attr('adulto') );

        let valorTotalBebeColo;
        let valorTotalCrianca;
        let valorTotalAdulto;

        if (valorAdulto > 0) valorTotalAdulto = (qtdAdultos*valorAdulto);
        else valorTotalAdulto = (qtdAdultos*valorGeral);


        if (valorCrianca > 0) valorTotalCrianca = (qtdCriancas*valorCrianca);
        else valorTotalCrianca = (qtdCriancas*valorGeral);

        if (valorBebeColo > 0) valorTotalBebeColo = (qtdBebes*valorBebeColo);
        else valorTotalBebeColo = (qtdBebes*valorGeral);

        if (valorPacoteAdulto > 0) valorTotalAdulto = valorTotalAdulto + (qtdAdultos*valorPacoteAdulto);
        if (valorPacoteCrianca > 0) valorTotalCrianca = valorTotalCrianca + (qtdCriancas*valorPacoteCrianca);
        if (valorPacoteBebe > 0) valorTotalBebeColo = valorTotalBebeColo + (qtdBebes*valorPacoteBebe);

        let valorTotal = (valorTotalBebeColo+valorTotalCrianca+valorTotalAdulto);

        $('#valorHospedagemAdulto').val(valorAdulto);
        $('#valorHospedagemCrianca').val(valorCrianca);
        $('#valorHospedagemBebe').val(valorBebeColo);

        $('#valorPacoteAdulto').val(valorPacoteAdulto);
        $('#valorPacoteCrianca').val(valorPacoteCrianca);
        $('#valorPacoteBebe').val(valorPacoteBebe);

        $('#subTotal').val(valorTotal);
        $('input[type="range"]').val(valorTotal).change();

        getParcelamento();
        removerAllWizardDependentes();

        for (var i=1;i<qtdAdultos;i++){
            adiconarWizardDependentesAdulto(i+1);
        }

        for (var i=0;i<qtdCriancas;i++){
            adiconarWizardDependentesCrianca(i+1);
        }

        for (var i=0;i<qtdBebes;i++){
            adiconarWizardDependentesBebe(i+1);
        }
    }

    function removerWizardDependentesCrianca(id) {
        $('#dependenteCriancas'+id).removeClass('wizard-step');
        $('#dependenteCriancas'+id).removeClass('step');
    }

    function adiconarWizardDependentesCrianca(id) {
        $('#dependenteCriancas'+id).addClass('wizard-step');
        $('#dependenteCriancas'+id).addClass('step');

        getDependentes(id, 'Criancas');
    }

    function removerWizardDependentesAdulto(id) {
        $('#dependenteAdulto'+id).removeClass('wizard-step');
        $('#dependenteAdulto'+id).removeClass('step');
    }

    function adiconarWizardDependentesAdulto(id) {
        $('#dependenteAdulto'+id).addClass('wizard-step');
        $('#dependenteAdulto'+id).addClass('step');

        getDependentes(id, 'Adulto');
    }

    function removerWizardDependentesBebe(id) {
        $('#dependenteBebe'+id).removeClass('wizard-step');
        $('#dependenteBebe'+id).removeClass('step');
    }

    function adiconarWizardDependentesBebe(id) {
        $('#dependenteBebe'+id).addClass('wizard-step');
        $('#dependenteBebe'+id).addClass('step');

        getDependentes(id, 'Bebe');
    }

    function getDependentes(id, tipo) {

        let url = "<?php echo base_url() ?>appcompra/getDependente"+tipo;

        $.ajax({
            type: "GET",
            url: url,
            data: {
                contador: id
            },
            dataType: 'html',
            success: function (html) {
                $('#dependente'+tipo+id).html(html);
                $('.styled-select select').niceSelect();
            }
        });
    }

    function getDouble(valor) {
        if (valor === null) return 0;
        if (valor === undefined) return 0;
        if (valor === '') return 0;

        return parseFloat(valor);
    }

    function getParcelamento() {

        var condicaoPagamento = $("#condicaoPagamento  option:selected").val();
        var tipoCobranca = $('input[name="tipoCobranca"]:checked').val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>appcompra/getParcelamento",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    tipoCobrancaId: tipoCobranca,
                    subTotal: $('#subTotal').val()
                },
                dataType: 'html',
                success: function (html) {
                    $('#parcelamento').html(html);
                }
            });
        } else {
            $('#parcelamento').html('');
        }

    }
</script>
</body>
</html>