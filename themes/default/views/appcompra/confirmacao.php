<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
    <meta name="author" content="Ansonika">
    <title><?php echo $configuracaoGeral->site_name;?> || RESERVA</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA">
    <meta name="robots" content="noindex, nofollow" />
    <meta name="language" content="br" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>" />
    <meta property="og:title" content="<?php echo $configuracaoGeral->site_name;?>"  />
    <meta property="og:description" content="RESERVA ONLINE" />
    <meta property="og:site_name" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <?php if ($this->session->userdata('cnpjempresa') == 'caicaraexpedicoes') {?>
        <!-- Google tag (gtag.js) event -->
        <script>
            gtag('event', 'ads_conversion_Formul_rio_1', {
                // <event_parameters>
            });
        </script>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <?php

    $corPrincipalDoSite = '#ffffff';

    ?>
    <style>
        .content-left {
            background-color: <?=$corPrincipalDoSite; ?>;
            padding: 0;
        }

        .budget_slider {
            background-color: #f8f8f8;
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
            font-weight: bold;
        }

        .cho-container button {
            width: 100%;
        }


        .budget_slider_info {
            background-color: #ffffff;
            margin-bottom: 20px;
            padding: 20px 15px 15px 10px;
            border-radius: 5px;
            border: 1px solid #2e2e2e;
            font-size: 16px;
            text-align: center;
        }

    </style>


    <?php if ($this->Settings->pixelFacebook) {
        foreach ($faturas as $fat) {
            $fatura_obj = $fat;
        }
        ?>
        <!-- Meta Pixel Code | Purchase -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'Purchase', {
                content_name: '<?=str_replace("'", "", $fatura_obj->product_name);?>',
                content_type: 'product',
                value: <?=$venda->total;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=Purchase&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<div id="loader_form">
    <div data-loader="circle-side-2"></div>
</div><!-- /loader_form -->


<div class="container-fluid full-height">
    <div class="row row-height">

        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <div id="middle-wizard">
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34">
                            <img src="<?= $assets ?>images/confirm.gif"  style="width: auto;" width="260px" height="260px" />
                        </h2>
                        <h2 class="main_question" style="color: #1e7e34">Sua reserva foi enviada com sucesso.</h2>
                        <?php if (!$tipoCobranca->chave_pix) {?>
                            <div class="budget_slider_info">
                                <p>A Reserva só será confirmada mediante o primeiro pagamento ou pagamento do sinal.</p>
                                <p>Em breve você receberá um e-mail com todos os detalhes de sua reserva.</p>
                            </div>
                        <?php } ?>
                        <?php  if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                <a href="<?php echo $cobranca->link;?>"
                                   target="_blank"
                                   class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                   BAIXAR BOLETO <?php echo $contador;?>º PARCELA
                                </a>
                            <?php $contador++;}?>
                            <div class="budget_slider_info" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Instruções:<br/>
                                    1. Imprima seu boleto e pague-o no banco.<br/>
                                    2. Você também pode pagar pela internet usando o código de barras.<br/>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'carne') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1) {
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->link;?>"
                                       target="_blank"
                                       class="btn_1" style="width: 100%;margin-top: 10px;background: #495057">
                                        BAIXAR CARNÊ
                                    </a>
                                <?php } ?>
                                <?php $contador++;
                            }?>
                            <div class="budget_slider_info" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Instruções:<br/>
                                    1. Imprima seu carnê e pague-o no banco.<br/>
                                    2. Você também pode pagar pela internet usando o código de barras.<br/>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'dinheiro') { ?>
                            <div class="budget_slider_info" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Deposito/transferência, assim que realizar o pagamento nos envia o comprovante para confirmar a reserva.<br/>
                                </p>
                                <p>
                                    <br/><?php if ($tipoCobranca->note) {
                                        echo 'Dados para depósito: '.str_replace('%0A', '<br/>', $tipoCobranca->note);
                                    }?>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'cartao') { ?>
                            <?php foreach ($faturas as $fat) {
                                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                <a href="<?php echo $cobranca->checkoutUrl;?>"
                                   target="_blank"
                                   class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                    PAGUE AGORA COM CARTÃO
                                </a>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'carne_cartao'){ ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->checkoutUrl;?>"
                                       target="_blank"
                                       class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                        PAGUE AGORA COM CARTÃO
                                    </a>
                                <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'link_pagamento' && $tipoCobranca->integracao != 'mercadopago') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->checkoutUrl;?>"
                                       target="_blank"
                                       class="btn_1"
                                       style="width: 100%;margin-top: 10px;background: #1661b1">
                                        PAGUE AGORA
                                    </a>
                                    <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'link_pagamento' && $tipoCobranca->integracao == 'mercadopago') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <div class="cho-container" style="width: 100%;font-size: 25px;"></div>
                                    <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'pix') { ?>
                            <p>
                                <?php if ($tipoCobranca->note) {
                                    echo str_replace('%0A', '<br/>', $tipoCobranca->note);
                                }?>
                            </p>
                        <?php } ?>
                        <?php if ($tipoCobranca->chave_pix) {?>
                                <br/>
                            <div class="budget_slider_info">
                                Efetue agora mesmo o pagamento via PIX e envie o comprovante.<br/>
                                <?php if ($tipoCobranca->telefone) {
                                    $telefone = $tipoCobranca->telefone;
                                    $telefone = str_replace ( '(', '', str_replace ( ')', '', $telefone));
                                    $telefone = str_replace ( '-', '',  $telefone);
                                    $telefone = str_replace ( ' ', '',  $telefone);
                                    ?>
                                    Whatsapp para
                                    <a href="https://api.whatsapp.com/send?phone=55<?=trim($telefone);?>" target="_blank" style="color: #77b43f;text-decoration: underline;"><b>+55 <?=$tipoCobranca->telefone;?></b>.</a><br/>
                                <?php } ?>
                                <?php if ($tipoCobranca->email) {?>
                                    E-mail para <b><?=$tipoCobranca->email;?></b>.<br/>
                                <?php } ?>
                                <hr/>
                                Chave PIX: <b><?=$tipoCobranca->chave_pix;?></b><br/>
                                <?php if ($tipoCobranca->titular) {?>
                                    Títular: <b><?=$tipoCobranca->titular;?></b><br/>
                                <?php } ?>
                                <?php
                                $contador = 1;
                                foreach ($faturas as $fat) {
                                      if ($contador == 1) {?>
                                          Valor: <b><?=$this->sma->formatMoney($fat->valorpagar);?></b>
                                      <?php } ?>
                                <?php $contador++;}?>

                                <hr/>
                                <a class="btn_1 mobile_btn" id="copy" style="background: #77b43f;margin-top: 10px;color: #ffffff;font-size: 1rem;width: 100%;"> COPIAR A CHAVE DO PIX </a>
                                <br/>
                            </div>
                            <div class="budget_slider_info" style="text-align: left;">
                                <h6>Como fazer o pagamento via PIX?</h6>
                                <p><b>1º Etapa:</b> Copie a chave Pix acima (E-mail/Telefone/CNPJ/CPF)</p>
                                <p><b>2º Etapa:</b> Abra o app do seu Banco </p>
                                <p><b>3º Etapa:</b> Procure a função pix e cole a chave copiada.</p>
                                <p><b>4º Etapa:</b> Confirme as informações do Pix se estão corretas e finalize a transferência.</p>
                            </div>
                        <?php } ?>
                        <div class="budget_slider_info">
                            <p>Você também pode clicar no botão abaixo e receber no WhatsApp o comprovante da reserva com todos os detalhes. 👇🏻</p>
                        </div>

                        <?php if ($venda->contract_id) {?>
                            <a href="<?php echo base_url().'apputil/downloadDocument/'.$venda->id.'?token='.$this->session->userdata('cnpjempresa'); ?>"
                               class="btn_1" style="width: 100%;margin-top: 10px;background: #1b0088" target="_blank">
                                BAIXE SEU CONTRATO
                            </a>
                        <?php } ?>

                        <a href="<?=$this->Settings->url_site_domain.'/'.$vendedor->id ?>" class="btn_1" style=";background: #0B2C5F;width: 100%;margin-top: 10px;">VOLTAR PARA O SITE</a>
                        <p><br/>Atenciosamente,<br/><?php echo $configuracaoGeral->site_name;?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cd-overlay-nav"><span></span></div>

<div class="cd-overlay-content"><span></span></div>

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $aceistes_contrato_label = explode('@label@', $configuracaoGeral->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <strong> <?php echo $labelAceite[0];?></strong>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal terms -->
<div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                    <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                    <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                    <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                    <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                    <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                </div>
             </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/velocity.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/file-validator.js" type="text/javascript"></script>
 
<!-- Wizard script -->
<script src="<?php echo base_url() ?>assets/appcompra/js/app_compra_v4.js"></script>

<!-- MERCADO PAGO !-->
<script src="https://sdk.mercadopago.com/js/v2"></script>

<script type="application/javascript">

    var CPF_PASSAGEIRO_PRINCIPAL = '';
    var listCPFS = [];

    jQuery(document).ready(function() {

        $('#celular').keyup(function (event) {
            mascaraTelefone( this, mtel );
        });

        $('#cpf').keyup(function (event) {
            mascaraCpf( this, formataCPF );

            if ($('#cpf').val().length === 11) buscarCPF();
        });

        $('#cpf').blur(function (event) {
            buscarCPF();
        });

        $('#condicaoPagamento').change(function (e){
            getParcelamento();
        });

        removerAllWizardDependentes();

        $('#copy').click(function (){
            copyTextToClipboard('<?=$tipoCobranca->chave_pix;?>', '')
        });
    });

    function copyTextToClipboard(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }

        document.body.removeChild(textArea);
        $('.mobile_btn').css('background', '#28a745');
        $('.mobile_btn').html('COPIADO!')
    }

    function removerAllWizardDependentes() {
        removerWizardDependentesAdulto(1);
        removerWizardDependentesAdulto(2);
        removerWizardDependentesAdulto(3);
        removerWizardDependentesAdulto(4);
        removerWizardDependentesAdulto(5);
        removerWizardDependentesAdulto(6);
        removerWizardDependentesAdulto(7);
        removerWizardDependentesAdulto(8);
        removerWizardDependentesAdulto(9);
        removerWizardDependentesAdulto(10);

        removerWizardDependentesCrianca(1);
        removerWizardDependentesCrianca(2);
        removerWizardDependentesCrianca(3);
        removerWizardDependentesCrianca(4);
        removerWizardDependentesCrianca(5);
        removerWizardDependentesCrianca(6);
        removerWizardDependentesCrianca(7);
        removerWizardDependentesCrianca(8);
        removerWizardDependentesCrianca(9);
        removerWizardDependentesCrianca(10);

        removerWizardDependentesBebe(1);
        removerWizardDependentesBebe(2);
        removerWizardDependentesBebe(3);
        removerWizardDependentesBebe(4);
        removerWizardDependentesBebe(5);
        removerWizardDependentesBebe(6);
        removerWizardDependentesBebe(7);
        removerWizardDependentesBebe(8);
        removerWizardDependentesBebe(9);
        removerWizardDependentesBebe(10);
    }

    function buscarCPF() {

        let tagCPF = $('#cpf');

        if (tagCPF.val() === '') return false;

        $('#nome').attr('readonly', false);

        if (!valida_cpf(tagCPF.val())) {
            alert('CPF inválido!');
            CPF_PASSAGEIRO_PRINCIPAL = '';

            tagCPF.val('');
            tagCPF.focus();

            return false;
        }

        tagCPF.val(formataCPF(tagCPF.val()));
        CPF_PASSAGEIRO_PRINCIPAL =tagCPF.val();

        verificaCadastroPassageiroPrincipal(tagCPF.val());
    }

    function verificaCadastroPassageiroPrincipal(cpf) {
        if (cpf  !== '') {
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>/linkcompra/getVerificaCustomeByCPF/'+cpf,
                data: {
                    cpf: $('#cpf').val()
                },
                dataType: 'json',
                success: function (pessoa) {
                    if (pessoa != null) {

                        $('#nome').val(pessoa.name);
                        $('#cpf').val(pessoa.vat_no);
                        $('#rg').val(pessoa.cf1);

                        if (pessoa.sexo === 'MASCULINO') {
                            jQuery("input[value='MASCULINO']").attr('checked', true);
                        } else if(pessoa.sexo === 'FEMININO') {
                            jQuery("input[value='FEMININO']").attr('checked', true);
                        }

                        $('#telefone').val(pessoa.phone);
                        $('#celular').val(pessoa.cf5);
                        $('#telefone_emergencia').val(pessoa.telefone_emergencia);
                        $('#alergia_medicamento').val(pessoa.alergia_medicamento);
                        $('#email').val(pessoa.email);

                        if (pessoa.data_aniversario !== '') {
                            let dia = pessoa.data_aniversario.split('-').reverse()[0];
                            let mes = pessoa.data_aniversario.split('-').reverse()[1];
                            let ano = pessoa.data_aniversario.split('-').reverse()[2];

                            $('#dia').val(dia);
                            $('#mes').val(mes);
                            $('#ano').val(ano);
                            $('select').niceSelect('update');
                        }

                        $('#nome').attr('readonly', true);
                        $('#celular').focus();
                    } else {

                        $('#alertaCadastroEncontado').hide();
                        $('#nome').attr('readonly', false);
                        $('#nome').focus();
                    }
                }
            });
        }
    }

    /* Máscaras ER */
    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }

    function mascaraCpf(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracpf()",1)
    }

    function execmascaracpf(){
        v_obj.value=v_fun(v_obj.value)
    }

    function formataCPF(cpf){
        //retira os caracteres indesejados...
        cpf = cpf.replace(/[^\d]/g, "");

        //realizar a formatação...
        return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, "$1.$2.$3-$4");
    }


    function removerWizardDependentesCrianca(id) {
        $('#dependenteCriancas'+id).removeClass('wizard-step');
        $('#dependenteCriancas'+id).removeClass('step');
    }

    function adiconarWizardDependentesCrianca(id) {
        $('#dependenteCriancas'+id).addClass('wizard-step');
        $('#dependenteCriancas'+id).addClass('step');

        getDependentes(id, 'Criancas');
    }

    function removerWizardDependentesAdulto(id) {
        $('#dependenteAdulto'+id).removeClass('wizard-step');
        $('#dependenteAdulto'+id).removeClass('step');
    }

    function adiconarWizardDependentesAdulto(id) {
        $('#dependenteAdulto'+id).addClass('wizard-step');
        $('#dependenteAdulto'+id).addClass('step');

        getDependentes(id, 'Adulto');
    }

    function removerWizardDependentesBebe(id) {
        $('#dependenteBebe'+id).removeClass('wizard-step');
        $('#dependenteBebe'+id).removeClass('step');
    }

    function adiconarWizardDependentesBebe(id) {
        $('#dependenteBebe'+id).addClass('wizard-step');
        $('#dependenteBebe'+id).addClass('step');

    }

    function getDependentes(id, tipo) {

        let url = "<?php echo base_url() ?>appcompra/getDependente"+tipo;

        $.ajax({
            type: "GET",
            url: url,
            data: {
                contador: id
            },
            dataType: 'html',
            success: function (html) {
                $('#dependente'+tipo+id).html(html);
                $('.styled-select select').niceSelect();
            }
        });
    }

    function getDouble(valor) {
        if (valor === null) return 0;
        if (valor === undefined) return 0;
        if (valor === '') return 0;

        return parseFloat(valor);
    }

    function getParcelamento() {

        var condicaoPagamento = $("#condicaoPagamento  option:selected").val();
        var tipoCobranca = $('input[name="tipoCobranca"]:checked').val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: "<?php echo base_url() ?>appcompra/getParcelamento",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    tipoCobrancaId: tipoCobranca,
                    subTotal: $('#subTotal').val()
                },
                dataType: 'html',
                success: function (html) {
                    $('#parcelamento').html(html);
                }
            });
        } else {
            $('#parcelamento').html('');
        }
    }


    <?php if ($tipoCobranca->tipo == 'link_pagamento' && $tipoCobranca->integracao == 'mercadopago') {
    $contador = 1;
    foreach ($faturas as $fat) {
        if ($contador == 1){
            $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id);
        }

        $contador = $contador++;
    }
    ?>

    <?php if ($configMercadoPago->active) { ?>
    // Adicione as credenciais do SDK
    const mp = new MercadoPago('<?php echo $configMercadoPago->account_token_public;?>', {locale: 'pt-BR'});

    // Inicialize o checkout
    mp.checkout({
        preference: {
            id: '<?php echo $cobranca->code;?>',
        },
        autoOpen: false, // Habilita a abertura automática do Checkout Pro
        render: {
            container: '.cho-container', // Indique o nome da class onde será exibido o botão de pagamento
            label: 'PAGAR AGORA', // Muda o texto do botão de pagamento (opcional)
        },
        type: 'wallet', // Aplica a marca do Mercado Pago ao botão
        theme: {
            elementsColor: '#e68900' // Cor clara
        }
    });
    <?php } ?>

    <?php }?>

</script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>

</body>
</html>