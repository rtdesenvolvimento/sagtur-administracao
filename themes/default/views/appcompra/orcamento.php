<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
    <meta name="author" content="Ansonika">
    <title><?php echo $configuracaoGeral->site_name;?> || ORÇAMENTO</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $configuracaoGeral->site_name;?> || ORÇAMENTO" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $configuracaoGeral->site_name;?> || ORÇAMENTO">

    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>" />
    <meta property="og:title" content="<?php echo $configuracaoGeral->site_name;?>"  />
    <meta property="og:description" content="ORÇAMENTO ONLINE" />
    <meta property="og:site_name" content="<?php echo $configuracaoGeral->site_name;?> || ORÇAMENTO ONLINE" />
    <meta property="og:image:alt" content="<?php echo $configuracaoGeral->site_name;?> || ORÇAMENTO ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <?php

    $corPrincipalDoSite = '#ffffff';

    ?>
    <style>
        .content-left {
            background-color: <?=$corPrincipalDoSite; ?>;
            padding: 0;
        }

        .budget_slider {
            background-color: #f8f8f8;
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -ms-border-radius: 5px;
            border-radius: 5px;
            font-weight: bold;
        }

        .cho-container button {
            width: 100%;
        }
    </style>
</head>

<body>
<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<div id="loader_form">
    <div data-loader="circle-side-2"></div>
</div><!-- /loader_form -->


<div class="container-fluid full-height">
    <div class="row row-height">

        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <div id="middle-wizard">
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34">Seu orçamento foi enviada com sucesso.</h2>
                        <div class="budget_slider">
                            <p>
                                Em breve você receberá um e-mail com todos os detalhes do seu orçamento.<br/><br/>
                                Você também pode clicar no botão abaixo e receber no WhatsApp o comprovante do seu orçamento
                                com todos os detalhes. 👇🏻
                            </p>
                        </div>
                        <?php  if ($tipoCobranca->tipo == 'boleto') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                <a href="<?php echo $cobranca->link;?>"
                                   target="_blank"
                                   class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                   BAIXAR BOLETO <?php echo $contador;?>º PARCELA
                                </a>
                            <?php $contador++;}?>
                            <div class="budget_slider" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Instruções:<br/>
                                    1. Imprima seu boleto e pague-o no banco.<br/>
                                    2. Você também pode pagar pela internet usando o código de barras.<br/>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'carne') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1) {
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->link;?>"
                                       target="_blank"
                                       class="btn_1" style="width: 100%;margin-top: 10px;background: #495057">
                                        BAIXAR CARNÊ
                                    </a>
                                <?php } ?>
                                <?php $contador++;
                            }?>
                            <div class="budget_slider" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Instruções:<br/>
                                    1. Imprima seu carnê e pague-o no banco.<br/>
                                    2. Você também pode pagar pela internet usando o código de barras.<br/>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'dinheiro') { ?>
                            <div class="budget_slider" style="margin-top: 10px;">
                                <p style="text-align: left;">
                                    Deposito/transferência, assim que realizar o pagamento nos envia o comprovante para confirmar a reserva.<br/>
                                </p>
                                <p>
                                    <br/><?php if ($tipoCobranca->note) {
                                        echo 'Dados para depósito: '.str_replace('%0A', '<br/>', $tipoCobranca->note);
                                    }?>
                                </p>
                            </div>
                        <?php } else if ($tipoCobranca->tipo == 'cartao') { ?>
                            <?php foreach ($faturas as $fat) {
                                $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                <a href="<?php echo $cobranca->checkoutUrl;?>"
                                   target="_blank"
                                   class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                    PAGUE AGORA COM CARTÃO
                                </a>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'carne_cartao'){ ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->checkoutUrl;?>"
                                       target="_blank"
                                       class="btn_1" style="width: 100%;margin-top: 10px;background: #1661b1">
                                        PAGUE AGORA COM CARTÃO
                                    </a>
                                <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'link_pagamento' && $tipoCobranca->integracao != 'mercadopago') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <a href="<?php echo $cobranca->checkoutUrl;?>"
                                       target="_blank"
                                       class="btn_1"
                                       style="width: 100%;margin-top: 10px;background: #1661b1">
                                        PAGUE AGORA
                                    </a>
                                    <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'link_pagamento' && $tipoCobranca->integracao == 'mercadopago') { ?>
                            <?php
                            $contador = 1;
                            foreach ($faturas as $fat) {
                                if ($contador == 1){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fat->id); ?>
                                    <div class="cho-container" style="width: 100%;font-size: 25px;"></div>
                                    <?php $contador++; } ?>
                            <?php } ?>
                        <?php } else if ($tipoCobranca->tipo == 'pix') { ?>
                            <p>
                                <?php if ($tipoCobranca->note) {
                                    echo str_replace('%0A', '<br/>', $tipoCobranca->note);
                                }?>
                            </p>
                        <?php } ?>
                        <a href="<?php echo base_url().'appcompra/confirmacao/'.$programacao->id.'/'.$vendedor->id.'/'.$venda->id.'?token='.$this->session->userdata('cnpjempresa'); ?>"
                           class="btn_1" style="width: 100%;margin-top: 10px;background: #30a113" target="_blank">
                            ENVIAR SEU ORÇAMENTO PELO WHATSAPP
                        </a>
                        <a href="<?php echo base_url().'appcompra/pdf/'.$venda->id.'?token='.$this->session->userdata('cnpjempresa'); ?>"
                           class="btn_1" style="width: 100%;margin-top: 10px;background: #e68900">
                            BAIXE COMPROVANTE DE ORÇAMENTO
                        </a>
                        <?php if ($configuracaoGeral->own_domain){?>
                            <a href="<?php echo base_url().'/carrinho/'.$programacao->id.'/'.$vendedor->id; ?>" class="btn_1" style="width: 100%;margin-top: 10px;background: #495057;">FAZER UM NOVO ORÇAMENTO</a>
                        <?php } else { ?>
                            <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacao->id.'/'.$vendedor->id; ?>" class="btn_1" style="width: 100%;margin-top: 10px;background: #495057;">FAZER UM NOVO ORÇAMENTO</a>
                        <?php }?>
                        <p><br/>Atenciosamente,<br/><?php echo $configuracaoGeral->site_name;?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cd-overlay-nav"><span></span></div>

<div class="cd-overlay-content"><span></span></div>

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $aceistes_contrato_label = explode('@label@', $configuracaoGeral->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <strong> <?php echo $labelAceite[0];?></strong>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal terms -->
<div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                    <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                    <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                    <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                    <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                    <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                </div>
             </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/velocity.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/file-validator.js" type="text/javascript"></script>
<script src="<?= $assets ?>js/valida_cpf_cnpj.js" type="text/javascript"></script>

<!-- Wizard script -->
<script src="<?php echo base_url() ?>assets/appcompra/js/app_compra_v4.js"></script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>
</body>
</html>