<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Wilio Survey, Quotation, Review and Register form Wizard by Ansonika.">
    <meta name="author" content="Ansonika">
    <title><?php echo $configuracaoGeral->site_name;?> || RESERVA</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>" />
    <meta property="og:title" content="<?php echo $configuracaoGeral->site_name;?>"  />
    <meta property="og:description" content="RESERVA ONLINE" />
    <meta property="og:site_name" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:image:alt" content="<?php echo $configuracaoGeral->site_name;?> || RESERVA ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <?php

    $corPrincipalDoSite = '#ffffff';

    ?>
    <style>
        .content-left {
            background-color: <?=$corPrincipalDoSite; ?>;
            padding: 0;
        }
        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            color: #fff;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            -ms-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
        }
    </style>
</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div id="preloader">
    <div data-loader="circle-side"></div>
</div><!-- /Preload -->

<div id="loader_form">
    <div data-loader="circle-side-2"></div>
</div><!-- /loader_form -->

<nav>
    <ul class="cd-primary-nav">
        <li><a href="<?php echo base_url().'loja/'.$this->session->userdata('cnpjempresa').'/'.$vendedor->id; ?>" class="animated_link">Página inicial</a></li>
        <li style="display: none;"><a href="about.html" class="animated_link">Sobre Nós</a></li>
        <li style="display: none;"><a href="contacts.html" class="animated_link">Contato</a></li>
    </ul>
</nav>
<!-- /menu -->


<style>
    .profile_img {
        padding: 17px;
        width: 200px;
    }
</style>


<div class="container-fluid full-height">
    <div class="row row-height">
        <div class="col-lg-6 content-left">
            <div class="content-left-wrapper">
                <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/loja/'.$vendedor->id; ?>" id="logo">
                    <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $configuracaoGeral->logo2;?>"
                         alt="<?php echo $configuracaoGeral->site_name ;?>" class="profile_img">
                </a>
                <div id="social">
                    <ul>
                        <?php if ($configuracaoGeral->facebook){?>
                            <li><a href="<?php echo $configuracaoGeral->facebook;?>" target="_blank"><i class="icon-facebook"></i></a></li>
                        <?php } ?>
                        <?php if ($configuracaoGeral->twitter){?>
                            <li><a href="<?php echo $configuracaoGeral->twitter;?>" target="_blank"><i class="icon-twitter"></i></a></li>
                        <?php } ?>
                        <?php if ($configuracaoGeral->youtube){?>
                            <li><a href="<?php echo $configuracaoGeral->youtube;?>" target="_blank"><i class="icon-youtube"></i></a></li>
                        <?php } ?>
                        <?php if ($configuracaoGeral->instagram){?>
                            <li><a href="<?php echo $configuracaoGeral->instagram;?>" target="_blank"><i class="icon-instagram"></i></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <!-- /social -->
                <div>
                    <figure>
                        <img src="<?php echo base_url().'/assets/uploads/'.$produto->image; ?>" alt="<?php echo $produto->name;?>" class="img-fluid"/>
                    </figure>
                    <h2><span style="text-decoration: line-through;"><?php echo $produto->name?></span><br/>Esgotado </h2>
                    <p><?php echo $produto->product_details;?></p>
                    <hr>
                    <p>
                        <i class="icon-user">Vendedor: <?php echo $vendedor->name;?></i>
                    </p>
                    <p><strong><i class="icon-bus">Saída: <?php echo date('d/m/Y', strtotime($programacao->dataSaida)).' </i><br/><i class="icon-clock">'. date('H:i', strtotime($programacao->horaSaida)).'h' ;?></strong></p>
                    <p><strong><i class="icon-bus">Retorno: <?php echo date('d/m/Y', strtotime($programacao->dataRetorno)).'</i><br/><i class="icon-clock">'. date('H:i', strtotime($programacao->horaRetorno)).'h';?></strong></p>
                    <hr/>
                    <a href="#0" class="btn_1 rounded" data-toggle="modal" data-target="#more-information"><i class="icon-info"></i> Mais Informações clique aqui</a>
                    <a href="#0" class="btn_1 rounded mobile_btn" data-toggle="modal" data-target="#more-information"><i class="icon-info"></i> Mais Informações do pacote aqui.</a>
                </div>
                <div class="copy">&copy; <?= date('Y')?> <a href="https://www.sagtur.com.br" style="color: #ffffff;" target="_blank">SAGTur</a><?php echo " | Licenciado para " . $configuracaoGeral->site_name; ?></div>
            </div>
            <!-- /content-left-wrapper -->
        </div>
        <!-- /content-left -->

        <div class="col-lg-6 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <!-- /top-wizard -->
                <?php $attrib = array('id' => 'wrapped');
                    echo form_open_multipart("appcompra/add", $attrib);  ?>

                    <input id="website" name="website" type="text" value="">
                    <input id="vendedor" name="vendedor" type="hidden" value="<?php echo $vendedor->id;?>">
                    <input id="subTotal" name="subTotal" type="hidden" value="0">
                    <input id="programacaoId" name="programacaoId" type="hidden" value="<?php echo $programacao->id;?>">

                    <!-- Leave for security protection, read docs for details -->
                    <div id="middle-wizard">
                        <!-- Passo 1 - Dados do cliente -->
                        <div class="step">
                            <img src="https://http2.mlstatic.com/D_NQ_NP_980205-MLB40515134983_012020-O.jpg">
                        </div>
                        <!-- /step-->
                    </div>
                    <!-- /middle-wizard -->
                </form>
            </div>
            <!-- /Wizard container -->
        </div>
        <!-- /content-right-->
    </div>
    <!-- /row-->
</div>
<!-- /container-fluid -->

<div class="cd-overlay-nav"><span></span></div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content"><span></span></div>
<!-- /cd-overlay-content -->

<a href="#0" class="cd-nav-trigger">Menu<span class="cd-icon"></span></a>
<!-- /menu button -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $aceistes_contrato_label = explode('@label@', $configuracaoGeral->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <strong> <?php echo $labelAceite[0];?></strong>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<!-- Modal terms -->
<div class="modal fade" id="more-information" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12">
                    <?= $produto->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$produto->image.'" alt="'.$produto->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                    <?= $produto->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $produto->product_details . '</div></div>' : ''; ?>
                    <?= $produto->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $produto->itinerario . '</div></div>' : ''; ?>
                    <?= $produto->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $produto->oqueInclui . '</div></div>' : ''; ?>
                    <?= $produto->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $produto->valores_condicoes . '</div></div>' : ''; ?>
                    <?= $produto->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $produto->details . '</div></div>' : ''; ?>
                </div>
             </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/velocity.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/file-validator.js" type="text/javascript"></script>
<script src="<?= $assets ?>js/valida_cpf_cnpj.js" type="text/javascript"></script>

<!-- Wizard script -->
<script src="<?php echo base_url() ?>assets/appcompra/js/app_compra_v4.js"></script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>
</body>
</html>