<!DOCTYPE html>
<html lang="pt-br">

<head>
    <title><?php echo $this->Settings->site_name;?></title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>


    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="#e58800">
    <meta name="theme-color" content="#e58800"/>

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $this->Settings->site_name;?>" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $this->Settings->site_name;?>">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $this->Settings->site_name;?>"  />
    <meta property="og:description" content="RESERVAS ONLINE" />
    <meta property="og:site_name" content="<?php echo $this->Settings->site_name;?> || RESERVAS ONLINE" />
    <meta property="og:image:alt" content="<?php echo $this->Settings->site_name;?> || RESERVAS ONLINE" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo$this->Settings->logo2;?>" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?= base_url(); ?>assets/uploads/logos/<?php echo$this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/lightbox2/css/lightbox.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/jquery-ui.css">


    <style>
        /*-------- 1.2 Buttons --------*/
        a.btn_1,
        .btn_1 {
            border: none;
            color: #fff;
            background: #dc3545;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            font-size: 20px;
        }

        .profile_img {
            padding: 17px;
            width: 200px;
            box-shadow: 0 2px 15px #ededed;
        }

        .pacotes-home h2 {
            text-align: center;
            font-size: 53px;
            color: #333;
            margin-bottom: 10px;
            margin-top: 30px;
        }

        .pacotes-home h4 {
            text-align: center;
            color: #333;
            font-size: 33px;
        }

        .pacotes-home p {
            text-align: center;
            font-size: 20px;
            color: #333;
        }

        .combobox-combox {
            display: flex;
            justify-content: space-around;
            margin-bottom: -30px;
            margin-top: 30px;
        }

        .mes-titulo-pacotes {
            display: flex;
            justify-content: space-around;
            margin-bottom: 30px;
            margin-top: 60px;
        }

        .linha-titulo-mes {
            background-color: #31586a;
            height: 5px;
            width: 25%;
            margin-top: 15px;
        }

        .nice-select .list {
            background-color: #fff;
            border-radius: 3px;
            box-shadow: 0 0 0 1px rgb(68 68 68 / 11%);
            box-sizing: border-box;
            margin-top: 4px;
            opacity: 0;
            padding: 0;
            pointer-events: none;
            position: absolute;
            top: 100%;
            left: 0;
            transform-origin: 50% 0;
            transform: scale(0.75) translateY(-25px);
            transition: all 0.2s cubic-bezier(0.5, 0, 0, 1.25), opacity 0.15s ease-out;
            z-index: 9999;
            height: max-content;
            overflow: auto;
        }

        .f {
            background-color: #ffffff;
            padding: 10px;
            border: solid 1px #d8d8d8;
            margin: 10px 10px 15px 10px;
            box-shadow: 0 2px 15px #ededed;
        }

        .elementor-ribbon.elementor-ribbon-left {
            -webkit-transform: rotate(0);
            -ms-transform: rotate(0);
            transform: rotate(0);
            left: 0px;
            right: 14px;
        }

        .elementor-ribbon.elementor-ribbon-left-esgotado {
            -webkit-transform: rotate(0);
            -ms-transform: rotate(0);
            transform: rotate(0);
            left: 14px;
            right: 14px;
        }

        .elementor-ribbon {
            position: absolute;
            z-index: 2;
            top: 0;
            left: auto;
            right: 0;
            -webkit-transform: rotate(
                    90deg
            );
            -ms-transform: rotate(90deg);
            transform: rotate(
                    90deg
            );
            width: 150px;
            overflow: hidden;
            height: 150px;
        }

        .elementor-ribbon-inner {
            text-align: center;
            left: 16px;
            width: 200%;
            -webkit-transform: translateY(-50%) translateX(-50%) translateX(35px) rotate(
                    -45deg
            );
            -ms-transform: translateY(-50%) translateX(-50%) translateX(35px) rotate(-45deg);
            transform: translateY(-50%) translateX(-50%) translateX(35px) rotate(
                    -45deg
            );
            margin-top: 90px;
            font-size: 12px;
            line-height: 3;
            font-weight: 800;
            text-transform: uppercase;
            background: #434bdf;
            color: #fff;
            padding: 1px 1px 1px 52px;
        }

        .box_compra  {
            font-size: 40px;
            font-weight: 400;
            color: #000;
            margin: 0px 0px 15px 0px;
        }

        .whatsapp {
            position: fixed;
            top: 90%;
            left: 1%;
            padding: 10px;
            z-index: 10000000;
        }

        .nice-select .option {
            cursor: pointer;
            font-weight: 400;
            line-height: 38px;
            list-style: none;
            min-height: 50px;
            outline: none;
            padding-left: 22px;
            padding-right: 33px;
            text-align: left;
            transition: all 0.2s;
            /* font-size: 20px; */
            border: 1px solid #ccc;
            font-size: 14px;
        }

        .combo {
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
            background-color: #fff;
            border-radius: 3px;
            border: 1px solid #d2d8dd;
            box-sizing: border-box;
            clear: both;
            cursor: pointer;
            display: block;
            float: left;
            font-family: inherit;
            font-size: 14px;
            font-weight: normal;
            height: 42px;
            line-height: 40px;
            outline: none;
            padding-left: 15px;
            padding-right: 27px;
            position: relative;
            text-align: left !important;
            transition: all 0.2s ease-in-out;
            user-select: none;
            white-space: nowrap;
            width: 100%;
            color: #6c757d;
        }

        ul, ol {
             list-style: initial;
             margin: 0 0 25px;
             padding: revert;
        }

        strong {
            font-weight: bold;
        }

        .img-thumbnail-product {
            display: inline-block;
            width: 100% \9;
            max-width: 100%;
            height: auto;
            padding: 4px;
            line-height: 1.42857143;
            background-color: #fff;
            border: 1px solid #ddd;
            border-radius: 4px;
            -webkit-transition: all .2s ease-in-out;
            -o-transition: all .2s ease-in-out;
            transition: all .2s ease-in-out;
        }

        .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
            border: 1px solid #e58801;
            background: #e58801;
            font-weight: 400;
            color: #fff;
        }

        .caixa_datas {
            margin-top: 25px;
            border: 1px solid #dcdcdc;
            padding: 15px 15px 15px 15px;
            margin-bottom: 0;
            box-shadow: 1px 1px 1px 1px rgb(0 0 0 / 6%), 1px 2px 5px 1px #dee2e6;
            background: #f6f6f6;
            border-radius: 5px;
            line-height: revert;
        }
    </style>
</head>
<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<section class="parallax_window_in" data-parallax="scroll" data-image-src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->logo_shop ?>" data-natural-width="1400" data-natural-height="800">
    <div id="sub_content_in">
        <div class="row">
            <div class="col-12">
                <img src="<?= base_url(); ?>assets/uploads/logos/<?php echo $this->Settings->logo2;?>"
                     alt="<?php echo $this->Settings->site_name ;?>" class="profile_img">
            </div>
        </div>
        <span>
            <h1><?php echo $this->Settings->site_name;?></h1>
            <?php if ($this->Settings->frase_site != '') echo '<p>'.$this->Settings->frase_site.'</p>' ?>
        </span>
    </div>
</section>
<!-- /section -->

<main id="general_page">
    <div class="combobox-combox" style="box-shadow: 0 2px 15px #ededed;    text-align: center;">
        <div class="container margin_30">
            <div class="col-12">
                <div class="styled-select clearfix" >
                    <label> <div class="pacotes-home"> <h5 style="font-weight: bold;">QUERO VIAJAR NO MÊS DE:</h5>  </div></label><br/>
                    <select class="combo" id="mes" onchange="buscarMes();">
                        <option value="all" <?php if($mesFilter == 'all') echo ' selected="selected"';?>>Ver todos</option>
                        <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                        <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                        <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                        <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                        <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                        <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                        <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                        <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                        <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                        <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                        <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                        <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                    </select>
                </div>
            </div>

            <script>
                var searchString = null;
                var topSearchIds = []

            </script>
            <div class="col-12">
                <div class="styled-select clearfix" >
                    <label> <div class="pacotes-home" style="margin-top: 15px;"> <h5 style="font-weight: bold;">PARA O DESTINO: </h5>  </div></label><br/>
                    <select class="combo" id="destino" onchange="buscarDestino();">
                        <option value="" >VER TODOS OS DESTINOS</option>
                        <?php
                        $programacoesFilterByName = array_column($programacoesFilter, 'name');
                        array_multisort($programacoesFilterByName, SORT_ASC, $programacoesFilter);

                        $produtoId = null;
                        $name_product_filter = null;
                        foreach ($programacoesFilter as $programacao) {
                            if ($produtoId !== $programacao->produtoId){
                                $produtoId = $programacao->produtoId; ?>
                                <?php if ($programacao->getTotalDisponvel() > 0) {?>
                                    <?php if ($destinoFilter == $programacao->produtoId){
                                        $name_product_filter = str_replace("'", "", $programacao->name); ?>
                                        <option selected value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $programacao->produtoId;?>"><?php echo $programacao->name?></option>
                                    <?php } ?>
                                <?php } ?>
                            <?php }?>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <?php if (!empty($programacoes)) {?>
        <?php
        $mesAtual = '';
        $phone = $vendedor->phone;
        $phone = str_replace ( '(', '', str_replace ( ')', '', $phone));
        $phone = str_replace ( '-', '',  $phone);
        $phone = str_replace ( ' ', '',  $phone);

        foreach ($programacoes as $programacaoEstoque) {?>

        <?php
            $mesSaida = $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida(), '%B');
            $anoSaida = date("Y", strtotime($programacaoEstoque->getDataSaida()));

            if ($mesSaida != $mesAtual || $anoSaida != $anoAtual) {?>
                <div class="mes-titulo-pacotes">
                    <div class="linha-titulo-mes"></div>
                    <h3><?php echo strtoupper($mesSaida).'/'.$anoSaida;?></h3>
                    <div class="linha-titulo-mes"></div>
                </div>
                <?php
                $mesAtual = $mesSaida;
                $anoAtual = $anoSaida;
                ?>
            <?php }?>
            <div class="container_styled_1">
                <div class="container margin_30">
                    <div class="row f">
                        <div class="col-lg-5 ml-lg-5 add_top_30" style="text-align: center;">
                            <?php
                            $images = $this->products_model->getProductPhotos($programacaoEstoque->produtoId);
                            $servicos_incluso =  $this->site->getAllServicosInclusoByProduct($programacaoEstoque->produtoId);

                            if($programacaoEstoque->getTotalDisponvel() > 0) {?>

                                <div class="row">
                                    <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$programacaoEstoque->image);?>"
                                       data-lightbox="<?php echo $programacaoEstoque->produtoId;?>" data-title="<?php echo $programacaoEstoque->name;?>">
                                        <img src="<?php echo base_url().'/assets/uploads/'.$programacaoEstoque->image; ?>"
                                             alt="<?php echo $programacaoEstoque->name;?>" id="pr-image<?php echo $programacaoEstoque->produtoId;?>" class="img-fluid img_class" style="margin-bottom: 15px;">
                                        <?php if($programacaoEstoque->getTotalDisponvel() < $programacaoEstoque->alertar_polcas_vagas ||
                                            $programacaoEstoque->getTotalDisponvel() < $programacaoEstoque->alertar_ultimas_vagas) {?>
                                            <?php if($programacaoEstoque->getTotalDisponvel() < $programacaoEstoque->alertar_ultimas_vagas ) {?>
                                                <div class="elementor-ribbon elementor-ribbon-left"><div class="elementor-ribbon-inner" style="background: #28a745;">ÚLTIMAS VAGAS!</div></div>
                                            <?php } else {?>
                                                <div class="elementor-ribbon elementor-ribbon-left"><div class="elementor-ribbon-inner">POUCAS VAGAS!</div></div>
                                            <?php } ?>
                                        <?php }?>
                                    </a>
                                    <a class="img-thumbnail-product change_img" produto="<?php echo $programacaoEstoque->produtoId;?>" href="<?php echo base_url('assets/uploads/'.$programacaoEstoque->image);?>" style="margin-right:4px;">
                                        <img class="img-responsive" src="<?php echo base_url('assets/uploads/thumbs/'.$programacaoEstoque->image);?>" alt="<?php echo base_url('assets/uploads/thumbs/'.$programacaoEstoque->image);?>" style="width:60px; height:60px;">
                                    </a>
                                    <?php foreach ($images as $image){?>
                                        <a class="img-thumbnail-product change_img" produto="<?php echo $programacaoEstoque->produtoId;?>" href="<?php echo base_url('assets/uploads/'.$image->photo);?>" style="margin-right:4px;">
                                            <img class="img-responsive" src="<?php echo base_url('assets/uploads/thumbs/'.$image->photo);?>" alt="<?php echo base_url('assets/uploads/thumbs/'.$image->photo);?>" style="width:60px; height:60px;">
                                        </a>
                                    <?php }?>
                                </div>

                            <?php } else { ?>
                                <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$programacaoEstoque->image);?>"
                                   data-lightbox="<?php echo $programacaoEstoque->produtoId;?>" data-title="<?php echo $programacaoEstoque->name;?>">
                                    <img src="<?php echo base_url().'/assets/uploads/'.$programacaoEstoque->image; ?>"
                                         alt="<?php echo $programacaoEstoque->name;?>" class="img-fluid img_class" style="margin-bottom: 15px;">
                                    <div class="elementor-ribbon elementor-ribbon-left-esgotado"><div class="elementor-ribbon-inner" style="background: #dc3545;">ESGOTADO!</div></div>
                                </a>
                            <?php } ?>
                            <?php foreach ($images as $image){?>
                                <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$image->photo);?>"
                                   data-lightbox="<?php echo $programacaoEstoque->produtoId;?>" data-title="Contratar">
                                </a>
                            <?php }?>
                        </div>

                        <!-- Modal terms -->
                        <div class="modal fade" id="more-information<?php echo $programacaoEstoque->produtoId?>" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title" id="termsLabel">Mais Informações do Pacote</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="col-sm-12">
                                            <?= $programacaoEstoque->image ? '<div class="panel" style="text-align: center;"><div class="panel-body"><img src="'.base_url().'assets/uploads/'.$programacaoEstoque->image.'" alt="'.$programacaoEstoque->name.'" class="img-responsive img-thumbnail"/></div></div>' : ''; ?>
                                            <?= $programacaoEstoque->product_details ? '<div class="panel panel-primary"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('product_details') . '</div><div class="panel-body">' . $programacaoEstoque->product_details . '</div></div>' : ''; ?>
                                            <?= $programacaoEstoque->itinerario ? '<div class="panel panel-info"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Roteiro') . '</div><div class="panel-body">' . $programacaoEstoque->itinerario . '</div></div>' : ''; ?>
                                            <?= $programacaoEstoque->oqueInclui ? '<div class="panel panel-warning"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('O que inclui') . '</div><div class="panel-body">' .  $programacaoEstoque->oqueInclui . '</div></div>' : ''; ?>
                                            <?= $programacaoEstoque->valores_condicoes ? '<div class="panel panel-danger"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Valores e condições') . '</div><div class="panel-body">' . $programacaoEstoque->valores_condicoes . '</div></div>' : ''; ?>
                                            <?= $programacaoEstoque->details ? '<div class="panel panel-success"><div class="panel-heading" style="margin-top: 10px;margin-bottom: 10px;font-weight: bold;font-size: 18px;">' . lang('Detalhes da Viagem') . '</div><div class="panel-body">' .  $programacaoEstoque->details . '</div></div>' : ''; ?>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6" style="margin-top: 50px;">
                            <h2 class="nomargin_top" style="text-align: center;"><?php echo $programacaoEstoque->name?></h2>
                            <p style="margin-top: 20px;"> </p>
                            <div class="accordion">
                                <h3><span class="fa fa fa-map-marker"></span> SOBRE A VIAGEM</h3>
                                <div>
                                    <?php echo $programacaoEstoque->product_details;?>
                                </div>
                                <?php if ($programacaoEstoque->itinerario){?>
                                    <h3><span class="fa fa fa-check"></span> ROTEIRO</h3>
                                    <div id="roteiro<?php echo $programacaoEstoque->id;?>" style="padding: 10px;">
                                        <?php echo $programacaoEstoque->itinerario;?>
                                    </div>
                                <?php } ?>
                                <?php if ($programacaoEstoque->oqueInclui){?>
                                    <h3><span class="fa fa-list"></span> O QUE INCLUI</h3>
                                    <div id="oqueInclui<?php echo $programacaoEstoque->id;?>" style="padding: 10px;">
                                        <?php echo $programacaoEstoque->oqueInclui;?>
                                    </div>
                                <?php } ?>
                                <?php if ($programacaoEstoque->valores_condicoes){?>
                                    <h3><span class="fa fa-money"></span> VALORES E CONDIÇÕES</h3>
                                    <div id="valores-condicoes<?php echo $programacaoEstoque->id;?>" style="padding: 10px;">
                                        <?php echo $programacaoEstoque->valores_condicoes;?>
                                    </div>
                                <?php } ?>
                                <?php if ($programacaoEstoque->details){?>
                                    <h3><span class="fa fa-info-circle"></span> INFORMAÇÕES IMPORTANTES</h3>
                                    <div id="detalhes<?php echo $programacaoEstoque->id;?>" style="padding: 10px;">
                                        <?php echo $programacaoEstoque->details;?>
                                    </div>
                                <?php } ?>

                                <?php if (!empty($servicos_incluso)){ ?>
                                    <div class=" border-top">
                                        <h4>Serviços Incluso</h4>
                                        <?php foreach ($servicos_incluso as $servico_incluso) {?>
                                            <div class="pacote-date mt-1 pt-1" style="text-align: left;height: 25px;font-size: .8rem;">
                                                <?php if ($servico_incluso->icon) {?>
                                                    <span style="display: inline;overflow: hidden;margin-right: .5rem;"><?=$servico_incluso->icon;?> </span>
                                                    <span style="vertical-align: sub;"><?=$servico_incluso->name;?></span>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                <?php } ?>

                            </div>
                            <a href="#0" data-toggle="modal" class="btn_1 white" style="width: 100%;margin-top: 20px; background: #333333;" data-target="#more-information<?php echo $programacaoEstoque->produtoId?>"> <span class="fa fa-plus-circle"></span> MAIS DETALHES</a>
                            <?php if($programacaoEstoque->getTotalDisponvel() > 0) {?>
                                <?php if ($Settings->own_domain){?>
                                    <a href="https://api.whatsapp.com/send?text=<?php echo base_url().'carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="background: #333333;margin-top: 10px;width: 100%;" target="_blank"><img src="<?php echo base_url();?>/assets/images/whatsapp-icon.png" style="width: 5%;" /> COMPARTILHAR</a>
                                <?php  } else {?>
                                    <a href="https://api.whatsapp.com/send?text=<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="background: #333333;margin-top: 10px;width: 100%;" target="_blank"><img src="<?php echo base_url();?>/assets/images/whatsapp-icon.png" style="width: 5%;" /> COMPARTILHAR</a>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($programacaoEstoque->category_id != 14) {?>
                                <div class="caixa_datas">
                                    <p style="font-size: 16px;"><b><span class="fa fa-calendar"></span> Saída: <?php echo $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida()); ?> <?php if ($programacaoEstoque->horaSaida != '00:00:00') echo '<br/><span class="fa fa-clock-o"></span> Hora: '.$this->sma->hf($programacaoEstoque->horaSaida);?></b></p>
                                    <p style="font-size: 16px;"><b><span class="fa fa-calendar"></span> Retorno: <?php echo  $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataRetorno()); ?>  <?php if ($programacaoEstoque->horaRetorno != '00:00:00') echo '<br/><span class="fa fa-clock-o"></span> Hora: '.$this->sma->hf($programacaoEstoque->horaRetorno);?></b></p>
                                </div>
                            <?php } ?>
                            <p style="text-align: right; margin-top: 25px;">
                                <?php if ($programacaoEstoque->precoExibicaoSite > 0){?>
                                    <span style="float: right;">
                                        <small style="font-size: 16px;"><?php echo $programacaoEstoque->valor_pacote;?></small><br/>
                                        <span class="box_compra"><?php echo  $this->sma->formatMoney($programacaoEstoque->precoExibicaoSite, $programacaoEstoque->simboloMoeda);?> </span><small> / por pessoa</small>
                                    </span>
                                <?php } ?>
                                <?php if($programacaoEstoque->getTotalDisponvel() > 0) {?>
                                    <?php if ($this->Settings->usarOrcamentoLoja) {?>
                                        <?php if ($Settings->own_domain){?>
                                            <a href="<?php echo base_url().'carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> RESERVE AGORA</a>
                                        <?php } else { ?>
                                            <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> RESERVE AGORA</a>
                                        <?php } ?>
                                    <?php } else { ?>
                                        <?php if ($programacaoEstoque->apenas_cotacao) {?>
                                            <?php if ($Settings->own_domain){?>
                                                <a href="<?php echo base_url().'carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> FAZER COTAÇÃO</a>
                                            <?php } else { ?>
                                                <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> FAZER COTAÇÃO</a>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php if ($Settings->own_domain){?>
                                                <a href="<?php echo base_url().'carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> RESERVE AGORA</a>
                                            <?php } else {?>
                                                <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-ticket"></span> RESERVE AGORA</a>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } else if ($programacaoEstoque->permitirListaEmpera){ //aqui sera futuramente para lista de espera.?>
                                    <?php if ($Settings->own_domain){?>
                                        <a href="<?php echo base_url().'carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-list"></span> LISTA DE ESPERA</a>
                                    <?php } else {?>
                                        <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$programacaoEstoque->id.'/'.$vendedor->id; ?>" class="btn_1 white" style="width: 100%;background: #e58801;"> <span class="fa fa-list"></span> LISTA DE ESPERA</a>
                                    <?php } ?>
                                <?php } else {?>
                                    <a href="https://api.whatsapp.com/send?phone=55<?php echo trim($phone)?>&text=QUERO ENTRAR PARA LISTA DE ESPERA <?php echo $programacaoEstoque->name?>  - <?php echo $this->sma->dataDeHojePorExtensoRetorno($programacaoEstoque->getDataSaida()); ?>" target="_blank" class="btn_1 white" style="width: 100%;background: #e58801;"><span class="fa fa-list"></span> LISTA DE ESPERA</a>
                                <?php } ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <a href="https://api.whatsapp.com/send?phone=55<?php echo trim($phone)?>&text=Venho do link da Loja do Sistema e queria uma informação sobre um produto" target="_blank"><img  class="whatsapp" src="<?php echo base_url();?>/assets/images/whatsapp-icon.png" /></a>
            </div>
        <?php }?>
    <?php } else {?>
        <div class="pacotes-home">
            <div style="margin-top: 50px;">
                <p>NENHUM DESTINO ENCONTRADO! EXPERIMENTE OUTRO MÊS</p>
            </div>
        </div>
    <?php } ?>
</main>

<footer class="clearfix">
    <div class="container">
        <p>&copy; <?= date('Y')?> <a href="https://www.sagtur.com.br" target="_blank">Desenvolvido por SAGTUR Sistemas para Agências de Turismo</a><?php echo " | Licenciado para " . $this->Settings->site_name; ?></p>
        <ul>
            <li><a href="#0"data-toggle="modal" data-target="#terms-txt"  class="animated_link">Termos e Condições</a></li>
        </ul>
    </div>
</footer>
<!-- end footer-->

<div class="cd-overlay-nav">
    <span></span>
</div>
<!-- /cd-overlay-nav -->

<div class="cd-overlay-content">
    <span></span>
</div>
<!-- /cd-overlay-content -->

<!-- Modal terms -->
<div class="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="termsLabel">Termos e Condições</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <?php
                $aceistes_contrato_label = explode('@label@', $this->Settings->termos_aceite); ?>
                <?php
                $contadorAceite = 0;
                foreach ($aceistes_contrato_label as $labelAceite){
                    if ($labelAceite != '') {
                        $labelAceite = explode('@quebra@', $labelAceite); ?>
                        <div class="panel-heading">
                            <strong> <?php echo $labelAceite[0];?></strong>
                        </div>
                        <p><?php echo $labelAceite[1];?></p>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn_1" data-dismiss="modal">Fechar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/common_scripts.min.js"></script>

<!-- SPECIFIC SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/parallax.min.js"></script>
<script src="<?php echo base_url() ?>assets/appcompra/js/owl-carousel.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/plugins/lightbox2/js/lightbox.min.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/jquery-ui.js"></script>

<script>


    $(document).ready(function () {
        $('.change_img').click(function (event) {
            event.preventDefault();
            var img_src = $(this).attr('href');
            let produto = $(this).attr('produto');

            $('#pr-image'+produto).attr('src', img_src);
            return false;
        });

        $( ".accordion" ).accordion({
            heightStyle: "content"
        });
    });

    "use strict";
    $(".team-carousel").owlCarousel({
        items: 1,
        loop: false,
        margin: 10,
        autoplay: false,
        smartSpeed: 300,
        responsiveClass: false,
        responsive: {
            320: {
                items: 1,
            },
            768: {
                items: 2,
            },
            1000: {
                items: 3,
            }
        }
    });

    function buscarMes() {
        let mes = $("#mes option:selected" ).val();

        <?php if ($Settings->own_domain){?>
            window.location = '<?php echo base_url().$vendedor->id.'?mes=';?>'+mes;
        <?php } else { ?>
            window.location = '<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id.'?mes=';?>'+mes;
        <?php } ?>
    }

    function buscarDestino() {
        let mes = $("#mes option:selected" ).val();
        let destino = $("#destino option:selected" ).val();

        <?php if ($Settings->own_domain){?>
            window.location = '<?php echo base_url().$vendedor->id.'?mes=';?>'+mes+'&destino='+destino;
        <?php } else { ?>
            window.location = '<?php echo base_url().$this->session->userdata('cnpjempresa').'/'.$vendedor->id.'?mes=';?>'+mes+'&destino='+destino;
        <?php } ?>
    }


</script>

<!-- Sections Code -->
<?php if ($this->Settings->sections_code){?>
    <?=$this->Settings->sections_code;?>
<?php } ?>


</body>
</html>