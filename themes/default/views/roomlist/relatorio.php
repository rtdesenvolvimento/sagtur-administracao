<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 3px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }
    </style>
<body>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: left;width: 10%;border-bottom: 1px solid #0b0b0b;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;width: 70px;" />';?>
                <?php
                $data = $this->sma->dataDeHojePorExtensoRetorno($programacao->dataSaida);
                $nomeViagem = strtoupper($product->name).' '.$data;
                ?>
            </td>
            <td style="text-align: left;width: 90%;border-bottom: 1px solid #0b0b0b;" >
                <h4>RELATÓRIO DE ROOMING LIST - <?=$roomList->name;?></h4>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 5px;border-bottom: 1px solid #0b0b0b;">
                <h5>Destino: <?=$nomeViagem?></h5>
            </td>
        </tr>
        </thead>
    </table>
    <table border="0" style="width: 100%;">
        <thead>
        <tr>
            <th style="text-align: left;width: 10%;">Quarto</th>
            <th style="width: 30%">Hospede(s)</th>
            <th style="width: 15%">CPF</th>
            <th style="width: 10%;text-align: center;">Nascimento</th>
            <th style="width: 10%;text-align: center;">Idade</th>
            <th style="width: 25%">Tipo de Quarto</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $contador = 1;
        $tiposHospedagem = [];
        $hospedesJuntos = null;
        $contadorQuarto = 1;

        foreach ($hospedes as $hospede){
            $dataNacimento = $hospede->nascimento;
            if ($dataNacimento) $dataNacimento = $this->sma->hrsd($hospede->nascimento);?>
            <?php if ($hospede->room_list_hospedagem_id != $hospedesJuntos) {

                $tiposHospedagem[$hospede->tipoHospedagemId] = array(
                    'nome' => $hospede->tipoHospedagem,
                    'qtd' => $tiposHospedagem[$hospede->tipoHospedagemId]['qtd'] + 1
                );

                if ($hospede->descontarVaga) {
                    /*
                    $tiposHospedagem[$hospede->tipoHospedagemId] = array(
                        'nome' => $hospede->tipoHospedagem,
                        'qtd' => $tiposHospedagem[$hospede->tipoHospedagemId]['qtd'] + 1
                    );*/
                } else {

                    /*
                    $tiposHospedagem[$hospede->faixaNome] = array(
                        'nome' => $hospede->faixaNome,
                        'qtd' => $tiposHospedagem[$hospede->faixaNome]['qtd'] + 1
                    );
                    */
                }
                ?>
                <tr><td colspan="2">Quarto <?=$contadorQuarto;?></td></tr>
                <?=$contadorQuarto++;?>
            <?php } ?>
            <?php $hospedesJuntos = $hospede->room_list_hospedagem_id;?>
            <tr style="background: #fffa90;border-bottom: 1px solid #000;">
                <?php if ($hospede->descontarVaga == 1 || $hospede->descontarVaga == null) {
                    $sContador = $contador < 10 ? '0'.$contador : $contador;?>
                    <td style="text-align: center;"><?=$sContador;?></td>
                    <?php $contador++?>
                <?php } else if ($hospede->descontarVaga == 0) {?>
                    <td style="text-align: center;">(C) </td>
                <?php } ?>
                <td>
                    <?=$hospede->name?>
                    <?php if ($hospede->social_name){?>
                        <br/><small>Nome Social: <?=$hospede->social_name?></small>
                    <?php } ?>
                </td>
                <td><?=$hospede->documento?></td>
                <td style="text-align: center;"><?=$dataNacimento;?></td>
                <td style="text-align: center;"><?=$this->sma->getIdade($hospede->nascimento);?></td>
                <td><?=$hospede->tipoHospedagem?></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <br/><br/>
    <table class="table table-bordered" border="1" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th style="background: #ccc;">Tipo de Quarto</th>
            <th style="background: #ccc;text-align: center;">Qtd de Quartos</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($tiposHospedagem as $tipoHospedagem){?>
            <tr>
                <td><?php echo $tipoHospedagem['nome'];?></td>
                <td style="text-align: center;"><?php echo $tipoHospedagem['qtd'];?></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <?= $roomList->note ? '<div class="well well-sm"><div class="panel-heading">' . lang('Notas importantes') . '</div><div class="panel-body">' . $roomList->note. '</div></div>' : ''; ?>
</body>
</html>
