<link href="<?= $assets ?>styles/jquery-ui.css" rel="stylesheet"/>

<style>
    .sortable1 {
        list-style-type: none;
        float: left;
        margin: 0 10px 0 0;
        background: #eee; padding: 5px; width: 100%;
    }

    .sortable1 li { margin: 5px; padding: 5px; font-size: 1.2em;  }

    .hospedagem {
        list-style-type: none;
        float: left;
        margin: 0 10px 0 0;
        background: #eee; padding: 5px; width: 100%;
    }

    .hospedagem li {
        margin: 5px; padding: 5px; font-size: 1.2em;
    }

    ul li {
        cursor: move;

    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bed"></i><?= lang('roomlist') ?> <?=$roomList->name;?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-edit tip" data-placement="left"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('roomlist/editarRoomList/'.$roomList->id.'/'.$programacaoId); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-edit"></i> <?= lang('editar_room_list') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= site_url('roomlist/relatorio/'.$roomList->id) ?>" target="_blank">
                                <i class="fa fa-print"></i> <?= lang('print_list_room_list') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('roomlist/excluir/'.$roomList->id); ?>">
                                <i class="fa fa-trash-o"></i> <?= lang('excluir_room_list') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row" >
            <div class="col-md-6">
                <h4>LISTA DE PASSAGEIROS</h4>
                <div id="hospedes" >
                    <?php foreach ($tiposHospedagem as $tipoHospedagem) {
                        $itens = $this->sales_model->getItensVendasByTipoHospedagem($productId, $programacaoId, $tipoHospedagem->id);
                        ?>
                        <h3><?=$tipoHospedagem->name;?></h3>
                        <div>
                            <ul id="tipoHospedagem_<?=$tipoHospedagem->id;?>" class="droptrue sortable1">
                                <?php foreach ($itens as $item) {
                                    $customer 	 = $this->site->getCompanyByID($item->customerClient);
                                    $clienteJaInserido = $this->Roomlisthospede_model->hospedeNaoInseridoNoQuarto($roomList->id, $customer->id);
                                    $idade = '';

                                    if ($customer->data_aniversario) {
                                        list($ano, $mes, $dia) = explode('-', $customer->data_aniversario);

                                        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                        $nascimento = mktime(0, 0, 0, $mes, $dia, $ano);
                                        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25).' anos';
                                    }

                                    $note_sale  = strip_tags(html_entity_decode($item->note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                    $note_item  = strip_tags(html_entity_decode($item->note_item, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                    $note_sale_admin  = strip_tags(html_entity_decode($item->staff_note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));

                                    $note = '';

                                    if ($note_item) {
                                        $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota do Cliente:. </b>'.$note_item.'</small>';
                                    }

                                    if ($note_sale) {
                                        $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota da Venda:. </b>'.$note_sale.'</small>';
                                    }

                                    if ($note_sale_admin) {
                                        $note .= '<br/><small><i class="fa fa-comment"></i> <b>Nota do Admin:.</b> '.$note_sale_admin.'</small>';
                                    }

                                    if ($note) {
                                        $note = '<br/>'.$note;
                                    } ?>
                                    <?php if ($clienteJaInserido){ ?>
                                        <li class="ui-state-highlight" sale_id="<?php echo $item->sale_id;?>"
                                            itemId="<?php echo $item->itemId;?>"
                                            id="<?=$customer->id?>"><?=$item->sale_id;?> - <?=$customer->name.' ['.lang($item->faixaNome).']';?> <?=$note;?>
                                            <small> <?=$idade;?></small>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </div>
                    <?php }?>

                    <!--
                        Cliente sem tipo de quarto selecionado.
                    !-->
                    <h3>Clientes Sem Tipo de Hospedagem</h3>
                    <div>
                        <ul id="tipoHospedagem_<?=$tipoHospedagem->id;?>" class="droptrue sortable1">
                            <?php
                            $itens = $this->sales_model->getItensVendasSemTipoHospedagem($productId, $programacaoId);
                            foreach ($itens as $item) {

                                $customer 	 = $this->site->getCompanyByID($item->customerClient);
                                $clienteJaInserido = $this->Roomlisthospede_model->hospedeNaoInseridoNoQuarto($roomList->id, $customer->id);

                                list($ano, $mes, $dia) = explode('-', $customer->data_aniversario);

                                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                                $note_sale  = strip_tags(html_entity_decode($item->note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                $note_item  = strip_tags(html_entity_decode($item->note_item, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                $note_sale_admin  = strip_tags(html_entity_decode($item->staff_note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));

                                $note = '';

                                if ($note_item) {
                                    $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota do Cliente:. </b>'.$note_item.'</small>';
                                }

                                if ($note_sale) {
                                    $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota da Venda:. </b>'.$note_sale.'</small>';
                                }

                                if ($note_sale_admin) {
                                    $note .= '<br/><small><i class="fa fa-comment"></i> <b>Nota do Admin:.</b> '.$note_sale_admin.'</small>';
                                }

                                ?>
                                <?php if ($clienteJaInserido){ ?>
                                    <li class="ui-state-highlight"
                                        sale_id="<?php echo $item->sale_id;?>"
                                        itemId="<?php echo $item->itemId;?>"
                                        id="<?=$customer->id?>"><?=$item->sale_id;?> - <?=$customer->name.' ['.lang($item->faixaNome).']';?>
                                        <small><?=$note;?></small>
                                        <br/><small> <i class="fa fa-birthday-cake"></i> <?=$idade;?> anos </small>
                                    </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h4>LISTA DE HOSPEDES</h4>
                <div id="hospedagens">
                    <?php foreach ($tiposHospedagemHotel as $tipoHospedagem) {

                        if ($roomList->fornecedor) {
                            if ($tipoHospedagem->fornecedor != $roomList->fornecedor) {
                                continue;
                            }
                        }
                        $hospedagens = $this->Roomlisttipohospedagem_model->getAllTiposHospedagemByRoomList($roomList->id, $tipoHospedagem->id); ?>
                        <h4 style="cursor: all-scroll;">
                            <?=$tipoHospedagem->name;?>
                            <button style="font-size: 10px;padding: 1px 15px;" type="button" tipo_hospedagem_id="<?=$tipoHospedagem->id;?>"
                                    class="btn btn-warning add-tipo-hospedagem"><i class="fa fa-plus-circle"></i> <?= lang('create_new_tipo_hospedagem') ?></button>
                        </h4>
                        <?php if (!empty($hospedagens)){?>
                            <div id="div_<?=$tipoHospedagem->id;?>" >
                                <?php foreach ($hospedagens as $hospedagem){
                                    $hospedes = $this->Roomlisthospede_model->getAllHospedesByRoomList($hospedagem->id, $tipoHospedagem->id);
                                    ?>
                                    <ul class="hospedagem droptrue" id="<?=$hospedagem->id;?>" style="margin-bottom: 10px;padding-bottom: 45px;">
                                        <button data-dismiss="alert" class="close" id="<?=$hospedagem->id;?>" onclick="excluirTipoHospedagem(<?=$hospedagem->id;?>);" type="button">×</button>
                                        <span>Arraste o hospede para este quarto</span>
                                        <?php if (!empty($hospedes)){?>
                                            <?php foreach ($hospedes as $hospede){
                                                $customer 	 = $this->site->getCompanyByID($hospede->customer_id);

                                                list($ano, $mes, $dia) = explode('-', $customer->data_aniversario);

                                                $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                                $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                                $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                                                ?>
                                                <li class="ui-state-highlight" sale_id="<?php echo $hospede->sale_id;?>" itemId="<?=$hospede->itemId;?>"" id="<?=$customer->id?>" hospede="<?=$hospede->id;?>" tipo_hospedagem="<?=$tipoHospedagem->id;?>">
                                                    <span><?=$customer->name.' ['.lang($hospede->faixaNome).']';?><small> <?=$idade;?> anos </small></span>
                                                    <button data-dismiss="alert" class="close" onclick="excluirHospede(<?php echo $hospede->id;?>, <?=$tipoHospedagem->id;?>, this);"
                                                            id="<?php echo $hospede->id;?>" type="button">×</button>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        <?php } else {?>
                            <div id="div_<?=$tipoHospedagem->id;?>"></div>
                        <?php }?>
                     <?php } ?>
                </div>
            </div>
            <div class="col-md-12" style="margin-top: 20px;">
                <a href="<?= site_url('roomlist/relatorio/'.$roomList->id) ?>"
                   class="tip btn btn-primary" title="" target="_blank">
                    <i class="fa fa-bed"></i> <span class="hidden-sm hidden-xs"><?= lang('print_list_room_list') ?></span>
                </a>

                <a href="<?php echo site_url('roomlist/editarRoomList/'.$roomList->id.'/'.$programacaoId); ?>"
                   class="tip btn btn-primary" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-edit"></i> <?= lang('editar_room_list') ?>
                </a>

                <a href="javascript:history.back();" class="tip btn btn-info" title="">
                    <i class="fa fa-backward"></i> <span class="hidden-sm hidden-xs">Voltar</span>
                </a>
            </div>

        </div>
    </div>
</div>
<script>

    $(document).ready(function () {
        $(".add-tipo-hospedagem").click(function(event){
            let tipo_hospedagem_id = $(this).attr('tipo_hospedagem_id');
            adicionarTipoHospedagem(tipo_hospedagem_id);
        });
    });

    $( function(){

        $( "#hospedes" ).accordion();
        $( "#hospedagens" ).accordion();

        $( "#hospedagens" ).draggable();

        $(".ui-accordion-content").css("height", "auto");
        $(".ui-accordion-content").css("overflow-x", "hidden");

        $( "ul.droptrue" ).sortable({
            connectWith: "ul",
            cursor: "move",
            stop: function(event, ui) {
                const customer = ui.item.context.id;
                const roomListTipoHospedagem_id = ui.item.parent()[0].id;
                const itemId = ui.item.context.getAttribute('itemId');
                const hospedeId = ui.item.context.getAttribute('hospede');
                const sale_id = ui.item.context.getAttribute('sale_id');

                if ( hospedeId !== null) excluirHospede(hospedeId);

                jQuery.ajax({
                    url		: site.base_url+"roomlist/salvarHospede",
                    type	: 'GET',
                    data	: {
                        'roomListTipoHospedagem_id': roomListTipoHospedagem_id,
                        'customer': customer,
                        'itemId' : itemId,
                        'sale_id' : sale_id,
                    },
                    cache	: false,
                    async 	: false
                }).done(function(hospede_incluido){

                    if (hospedeId !== null)  $('#' + hospedeId).remove();

                    let buttom_exclud = '<button data-dismiss="alert" class="close" ' +
                        'onclick="excluirHospede('+hospede_incluido.id+','+ hospede_incluido.tipo_hospedagem_id+', this);" id="'+hospede_incluido.id+'" type="button">×</button>';

                    $('#'+customer).append(buttom_exclud);

                    ui.item.context.setAttribute('hospede', hospede_incluido.id);
                    ui.item.context.setAttribute('tipo_hospedagem', hospede_incluido.tipo_hospedagem_id)
                });
            }
        });
    });

    function adicionarTipoHospedagem(tipo_hospedagem_id) {
        $.ajax({
            type: "GET",
            url: site.base_url + "/roomlist/salvarTipoHospedagem",
            data: {
                roomlist_id : '<?=$roomList->id;?>',
                tipoHospedagemId : tipo_hospedagem_id
            },
            dataType: 'json',
            success: function (tipohospedagem) {

                const html = '<ul class="hospedagem droptrue" id="'+tipohospedagem.id+'" style="margin-bottom: 10px;padding-bottom: 45px;">' +
                    ' <button data-dismiss="alert" class="close" id="'+tipohospedagem.id+'" onclick="excluirTipoHospedagem('+tipohospedagem.id+');" type="button">×</button> ' +
                    '<span>Arraste o hospede para este quarto</span>';

                $("#div_"+tipo_hospedagem_id).prepend(html);

                $( "ul.droptrue" ).sortable({
                    connectWith: "ul",
                    cursor: "move"
                });
            }
        });
    }

    function excluirTipoHospedagem(id) {
        $.ajax({
            type: "GET",
            url: site.base_url + "/roomlist/excluirTipoHospedagem/"+id,
            dataType: 'json',
            success: function (retorno) {}
        });
    }

    function excluirHospede(id, tipoHospedagemId, tag) {
        $.ajax({
            type: "GET",
            url: site.base_url + "/roomlist/excluirHospede/"+id,
            dataType: 'json',
            success: function (retorno) {
                if (tag !== undefined) {
                    let tipoHospedagem = tag.parentElement.getAttribute('tipo_hospedagem');
                    $('#tipoHospedagem_' + tipoHospedagem).append(tag.parentElement);
                }
            }
        });
    }

</script>



