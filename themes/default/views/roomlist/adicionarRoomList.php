<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('adicionar_room_list'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("roomlist/adicionarRoomList/".$programacaoId, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <input type="hidden" name="programacao_id" value="<?=$programacaoId;?>"/>
            <div class="form-group">
                <?= lang('room_list_hotel', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang('hotel', 'fornecedor'); ?>
                <?php
                echo form_input('fornecedor','', 'data-placeholder="' . lang("select") . ' ' . lang("fornecedor") . '"id="fornecedor" class="form-control input-tip" style="width:100%;"');
                ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('adicionarRoomList', lang('adicionar_room_list'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $('#fornecedor').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });
</script>
