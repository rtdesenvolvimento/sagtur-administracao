<style>
    .code {
        font-family: Menlo,Monaco,monospace,sans-serif;
        color: #fff!important;
        background-color: #212529!important;
        font-size: 14px;
        min-height: 300px!important;
    }
</style>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i><?= lang('shop_settings'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <?php echo form_open("shop_settings"); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-shopping-cart"></i> <?= lang('inf_head_code'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("head_code", "head_code"); ?>
                                        <?php echo form_textarea('head_code', (isset($_POST['head_code']) ? $_POST['head_code'] : $Settings->head_code), 'class="form-control skip code" id="head_code" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-shopping-cart"></i> <?= lang('inf_body_code'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("body_code", "body_code"); ?>
                                        <?php echo form_textarea('body_code', (isset($_POST['body_code']) ? $_POST['body_code'] : $Settings->body_code), 'class="form-control skip code" id="body_code" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading"><i class="fa-fw fa fa-shopping-cart"></i> <?= lang('inf_sections_code'); ?></div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("sections_code", "sections_code"); ?>
                                        <?php echo form_textarea('sections_code', (isset($_POST['sections_code']) ? $_POST['sections_code'] : $Settings->sections_code), 'class="form-control skip code" id="sections_code" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-inline well well-sm">
                            <div class="form-group col-sm-12">
                                <div class="row">
                                    <label for="sitemap" class="col-sm-2 control-label" style="margin-top:8px;"><?= lang('sitemap'); ?></label>
                                    <div class="col-sm-10">
                                        <div class="input-group col-sm-12">
                                            <input type="text" class="form-control" value="<?= $this->Settings->url_site_domain .'/sitemap_' . $this->session->userdata('cnpjempresa') . '.xml'; ?>" readonly>
                                            <a href="<?= $this->Settings->url_site_domain .'/sitemap_' . $this->session->userdata('cnpjempresa') . '.xml'; ?>" target="_blank" class="input-group-addon btn btn-primary" id="basic-addon2"><?=lang('visit'); ?></a>
                                            <a href="<?= base_url('shop_settings/sitemap'); ?>" target="_blank" class="input-group-addon btn btn-warning" id="basic-addon2"><?=lang('update'); ?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("pagination_type", "pagination_type"); ?>
                            <?php
                            $lang = array(
                                'by_date' => 'Paginação por Data',
                                'by_category' => 'Paginação por Pategoria',
                                'continuous_pagination' => 'Paginacao Continua',
                            );
                            echo form_dropdown('pagination_type', $lang, $Settings->pagination_type, 'class="form-control tip" id="pagination_type" required="required" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("number_packages_per_line", "number_packages_per_line"); ?>
                            <?php
                            $lang = array(
                                '3'      => '3 Pacotes Por Linha',
                                '4'      => '4 Pacotes Por Linha',
                            );
                            echo form_dropdown('number_packages_per_line', $lang, $Settings->number_packages_per_line, 'class="form-control tip" id="pagination_type" required="required" style="width:100%;"');
                            ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group all">
                            <?= lang("max_packages_line", "max_packages_line") ?>
                            <?= form_input('max_packages_line', (isset($_POST['max_packages_line']) ? $_POST['max_packages_line'] : ($Settings ? $Settings->max_packages_line : '8')), 'class="form-control tip mask_integer" id="max_packages_line" '); ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('is_rotate', '1', $Settings->is_rotate, 'id="is_rotate"'); ?>
                                <label for="attributes" class="padding05"><?= lang('is_rotate'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('arredondar_bordas_img', '1', $Settings->arredondar_bordas_img, 'id="arredondar_bordas_img"'); ?>
                                <label for="attributes" class="padding05"><?= lang('arredondar_bordas_img'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('nao_distorcer_img', '1', $Settings->nao_distorcer_img, 'id="nao_distorcer_img"'); ?>
                                <label for="attributes" class="padding05"><?= lang('nao_distorcer_img'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('atividades_recomendadas', '1', $Settings->atividades_recomendadas, 'id="atividades_recomendadas"'); ?>
                                <label for="attributes" class="padding05"><?= lang('atividades_recomendadas'); ?></label>
                            </div>
                        </div>
                    </div>




                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('filtar_local_embarque', '1', $Settings->filtar_local_embarque, 'id="filtar_local_embarque"'); ?>
                                <label for="attributes" class="padding05"><?= lang('filtar_local_embarque'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('ocultar_horario_saida', '1', $Settings->ocultar_horario_saida, 'id="ocultar_horario_saida"'); ?>
                                <label for="attributes" class="padding05"><?= lang('ocultar_horario_saida'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('ocultar_embarques_produto', '1', $Settings->ocultar_embarques_produto, 'id="ocultar_embarques_produto"'); ?>
                                <label for="attributes" class="padding05"><?= lang('ocultar_embarques_produto'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('captar_meio_divulgacao', '1', $Settings->captar_meio_divulgacao, 'id="captar_meio_divulgacao"'); ?>
                                <label for="attributes" class="padding05"><?= lang('captar_meio_divulgacao'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('mostrar_taxas', '1', $Settings->mostrar_taxas, 'id="mostrar_taxas"'); ?>
                                <label for="attributes" class="padding05"><?= lang('mostrar_taxas'); ?></label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group all">
                                <?php echo form_checkbox('web_page_caching', '1', $Settings->web_page_caching, 'id="web_page_caching"'); ?>
                                <label for="attributes" class="padding05"><?= lang('web_page_caching'); ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-4">
                            <div class="form-group all">
                                <?= lang("cache_minutes", "cache_minutes") ?>
                                <?= form_input('cache_minutes', (isset($_POST['cache_minutes']) ? $_POST['cache_minutes'] : ($Settings ? $Settings->cache_minutes : '0')), 'class="form-control tip mask_integer" id="cache_minutes" '); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="clear: both; height: 10px;"></div>
            <div class="col-md-12">
                <div class="form-group">
                    <div class="controls">
                        <?php echo form_submit('update_settings', lang("update_settings"), 'class="btn btn-primary"'); ?>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
