
<style type="text/css" media="screen">
    #MenuTable td:nth-child(1) {display: none;}
    #MenuTable td:nth-child(2) {width: 20%;text-align: left;}
    #MenuTable td:nth-child(3) {width: 50%;text-align: left;}
    #MenuTable td:nth-child(4) {text-align: center;}
    #MenuTable td:nth-child(5) {text-align: center;}
    #MenuTable td:nth-child(6) {text-align: center;}
</style>

<script>
    $(document).ready(function () {

        function row_active(x) {
            if(x === '1') {
                return '<div class="text-center"><span class="label label-success">Ativo</span></div>';
            }
            return '<div class="text-center"><span class="label label-danger">Inativo</span></div>';
        }

        $('#MenuTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('shop_settings/getPages') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                {"mRender": fsd},
                {"mRender": fsd},
                {"mRender": row_active},
                {"bSortable": false}
            ]
        });
    });
</script>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-list"></i><?= lang('pages'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('shop_settings/addPage'); ?>">
                                <i class="fa fa-plus"></i> <?= lang('adicionar_page') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="MenuTable" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                            <th style="text-align: left;"><?= $this->lang->line("menu"); ?></th>
                            <th style="text-align: left;"><?= $this->lang->line("titulo"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("data_publicacao"); ?></th>
                            <th style="text-align: center;"><?= $this->lang->line("data_despublicacao"); ?></th>
                            <th><?= $this->lang->line("active"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script language="javascript">
    $(document).ready(function () {});
</script>