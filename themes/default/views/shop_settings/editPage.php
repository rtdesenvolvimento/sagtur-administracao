<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('adicionar_page'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php echo form_open_multipart("shop_settings/editPage/".$page->id, $attrib = array('data-toggle' => 'validator', 'role' => 'form')) ?>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("active", "active") ?>
                        <?php
                        $opts = array(
                            '1' => lang('ativo'),
                            '0' => lang('inativo')
                        );
                        echo form_dropdown('active', $opts, (isset($_POST['active']) ? $_POST['active'] : ($page ? $page->active : '1')), 'class="form-control" id="active" required="required"');
                        ?>
                    </div>
                    <div class="form-group">
                        <?= lang('menu', 'menu_id'); ?>
                        <?php
                        $cbMenuPai[''] = lang('select').' '.lang('menu');
                        foreach ($menusPai as $pai) {
                            $cbMenuPai[$pai->id] = $pai->name;
                        } ?>
                        <?= form_dropdown('menu_id', $cbMenuPai, (isset($_POST['menu_id']) ? $_POST['menu_id'] :  ($page ? $page->menu_id : '')), 'class="form-control" id="menu_id" required="required"'); ?>
                    </div>
                    <div class="form-group all">
                        <?= lang("titulo", "titulo") ?>
                        <?= form_input('titulo', (isset($_POST['name']) ? $_POST['name'] : ($page ? $page->titulo : '')), 'class="form-control" id="titulo" required="required"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang("data_publicacao", "data_publicacao"); ?>
                        <div class="input-group">
                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                            </div>
                            <?= form_input('data_publicacao', (isset($_POST['data_publicacao']) ? $_POST['data_publicacao'] : ($page ? $page->data_publicacao : '')), 'class="form-control" id="data_publicacao" required="required"', 'date'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?= lang("data_despublicacao", "data_despublicacao"); ?>
                        <div class="input-group">
                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                            </div>
                            <?= form_input('data_despublicacao', (isset($_POST['data_despublicacao']) ? $_POST['data_despublicacao'] : ($page ? $page->data_despublicacao : '')), 'class="form-control" id="data_despublicacao"', 'date'); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group all">
                        <div class="form-group all">
                            <?= lang("photo_blog", "photo") ?>
                            <input id="photo" type="file" data-browse-label="<?= lang('browse'); ?>" name="photo" data-show-upload="false"
                                   data-show-preview="false" accept="image/*" class="form-control file">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $page->photo ?>" alt="<?= $page->photo ?>" class="img-responsive img-thumbnail"/>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group all">
                        <?= lang("conteudo", "conteudo") ?>
                        <?= form_textarea('conteudo', (isset($_POST['conteudo']) ? $_POST['conteudo'] : ($page ? $page->conteudo : '')), 'class="form-control skip" id="conteudo"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php echo form_submit('adicionar_page', $this->lang->line("adicionar_page"), 'class="btn btn-primary"'); ?>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        setTimeout(function (){
            create_summernote();
        }, 1000);
    });

    function create_summernote() {
        $('#conteudo').summernote(
            {
                height: 650,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai'
                }
            }
        );
    }
</script>



