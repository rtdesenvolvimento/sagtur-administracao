<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $Settings->site_name;?>">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>
    <title><?php echo $Settings->site_name;?> || PAGAMENTO CONFIRMADO</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $Settings->site_name;?> || PAGAMENTO CONFIRMADO" >
    <meta name="keywords" content="<?php echo $Settings->site_name;?>">
    <meta name="application-name" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $Settings->site_name;?> || PAGAMENTO CONFIRMADO">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_br" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $Settings->site_name;?>"  />
    <meta property="og:description" content="PAGAMENTO CONFIRMADO || <?php echo $fatura->product_name;?>" />
    <meta property="og:site_name" content="<?php echo $Settings->site_name;?> || PAGAMENTO CONFIRMADO" />
    <meta property="og:image:alt" content="<?php echo $Settings->site_name;?> || PAGAMENTO CONFIRMADO" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <style>

        .budget_slider_info {
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            font-weight: bold;
            width: 100%;
            background: #77b43f;
            text-align: center;
            color: #ffffff;
            border-radius: 10px;
            border: 1px solid #2e2e2e;
        }

        .budget_dados {
            margin-bottom: 20px;
            padding: 10px 10px 10px 15px;
            font-weight: bold;
            width: 100%;
            background: #f8f9fa;
            text-align: left;
            border-radius: 10px;
            border: 1px solid #2e2e2e;
        }

        .header {
            background: #77b43f;
            padding: 15px;
            width: 100%;
            text-align: center;
            color: #ffffff;
            font-size: 20px;
        }

        .row-height {
            height: 0vh;
        }

        a.botoes, .botoes {
            color: #fff;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            width: 100%;
            margin-top: 10px;
            background: #30a113;
            border: 1px solid #2e2e2e;
            font-size: 15px;
        }
    </style>

    <?php if ($this->Settings->pixelFacebook) {?>
        <!-- Meta Pixel Code | Purchase -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'Purchase', {
                content_name: '<?=str_replace("'", "", $fatura->product_name);?>',
                content_type: 'product',
                value: <?=$fatura->valorpagar;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=Purchase&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div class="container-fluid full-height">
    <div class="row row-height">
        <div class="header">
            COMPROVANTE DE PAGAMENTO
        </div>
        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <div id="middle-wizard">
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34">
                            <img src="<?= $assets ?>images/confirm.gif"  style="width: auto;" width="260px" height="260px" />
                        </h2>
                        <h3>Pagamento Aprovado!</h3>
                    </div>
                </div>
                <div class="budget_slider_info">
                    VOCÊ PAGOU </br> <h1 style="color: #ffffff;font-size: 50px;"><?php echo  $this->sma->formatMoney($fatura->valorfatura);?></h1>
                    <p>Obrigado pelo pagamento!</p>
                </div>
                <div class="budget_dados">
                    <h3>ORIGEM</h3>
                    <?php echo 'NOME: '.$cliente->name;?><br/>
                    <?php
                    $cpfVisivel = substr($cliente->vat_no, 3, 9);
                    ?>
                    <?php echo 'CPF: ***'.$cpfVisivel.'**';?><br/>
                    <?php echo 'TELEFONE: '.$cliente->cf5;?><br/>
                    <?php echo 'E-MAIL: '.$cliente->email;?><br/>
                </div>
                <div class="budget_dados">
                    <h3>DESTINO</h3>
                    <?php echo 'EMPRESA: '.$Settings->site_name;?><br/>
                    <?php echo 'CNPJ: '.$vendedor->vat_no;?><br/>
                    <?php echo 'E-MAIL: '.$vendedor->email;?><br/>
                    <?php echo 'TELEFONE: '.$vendedor->phone;?><br/>
                    <?php if ($cobrancaFatura->integracao == 'mercadopago') { ?>
                        Instituição Financeira: MercadoPago.com
                    <?php } else if ($cobrancaFatura->integracao == 'asaas') { ?>
                        Instituição Financeira: Asaas.com
                    <?php } ?>
                </div>
                <?php if (!empty($venda)) {?>
                    <?php if ($venda->contract_id) {?>
                        <a href="<?php echo base_url().'apputil/downloadDocument/'.$venda->id.'?token='.$this->session->userdata('cnpjempresa'); ?>"
                           class="btn_1" style="width: 100%;margin-top: 10px;background: #1b0088" target="_blank">
                            BAIXE SEU CONTRATO
                        </a>
                    <?php } ?>
                <?php } ?>
                <p><br/><br/></p>
            </div>
        </div>
    </div>
    <hr style="border-top: 2px dotted black;"/>
</div>
<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>

</body>
</html>