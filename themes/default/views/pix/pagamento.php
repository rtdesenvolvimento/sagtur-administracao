<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $Settings->site_name;?>">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>
    <title><?php echo $Settings->site_name;?> || PAGUE COM PIX</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $Settings->site_name;?> || PAGUE COM PIX" >
    <meta name="keywords" content="<?php echo $Settings->site_name;?>">
    <meta name="application-name" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $Settings->site_name;?> || PAGUE COM PIX">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_br" />
    <meta property="og:image" content="<?= $assets ?>images/pix.jpg" />
    <meta property="og:title" content="<?php echo $Settings->site_name;?>"  />
    <meta property="og:description" content="PAGUE COM PIX || <?php echo $fatura->product_name;?>" />
    <meta property="og:site_name" content="<?php echo $Settings->site_name;?> || PAGUE COM PIX" />
    <meta property="og:image:alt" content="<?php echo $Settings->site_name;?> || PAGUE COM PIX" />
    <meta property="og:url" content="<?=current_url();?>" />

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <style>

        .header_valor {
            background-color: #e58800;
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            font-weight: bold;
            border: 1px solid #2e2e2e;
            border-radius: 10px;
            color: #ffff;
            text-align: center;
        }

        .header_qr_code {
            background-color: #ededed;
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            border: 1px solid #2e2e2e;
            border-radius: 10px;
            font-weight: bold;
        }

        .budget_slider_info {
            background-color: #ffffff;
            margin-bottom: 20px;
            padding: 20px 0px 15px 10px;
            border-radius: 5px;
            font-weight: bold;
            text-align: left;
            border: 1px solid #2e2e2e;
            color: #000000;
        }

        .budget_slider_importante {
            background-color: #ffffff;
            margin-bottom: 20px;
            padding: 20px 0px 15px 10px;
            border-radius: 5px;
            font-weight: bold;
            text-align: left;
            border: 1px solid #e58800;
            color: #000000;
        }

        .codigo_qr_code {
            border: 1px solid #2e2e2e;
            border-radius: 10px;
            padding: 20px 0px 15px 10px;
            text-align: left;
            font-size: 9px;
            line-height: 11px;
            background: #ffffff;
            color: #000000;
            font-weight: initial;
        }

        .botao_copiar {
            width: 100%;
            border-radius: 10px;
        }
    </style>

    <?php if ($this->Settings->pixelFacebook) {?>
        <!-- Meta Pixel Code | Purchase -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'Purchase', {
                content_name: '<?=str_replace("'", "", $fatura->product_name);?>',
                content_type: 'product',
                value: <?=$fatura->valorpagar;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=Purchase&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>
<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div class="container-fluid full-height">
    <div class="row row-height">
        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="top-wizard">
                    <div id="progressbar"></div>
                </div>
                <div id="middle-wizard">
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34"><img src="<?= $assets ?>images/pix.jpg"  style="width: auto;" width="160px" height="160px" /></h2>
                        <div class="header_valor">
                            <span style="font-weight: bold;font-size: 30px;">
                                Total <?php echo  $this->sma->formatMoney($fatura->valorfatura);?><br/>
                                <span style="font-weight: bold;font-size: 16px;">Você está pagando a parcela <?=$fatura->numero_parcela;?></span>
                            </span>
                        </div>
                        <div class="header_qr_code">
                            <h3>CÓDIGO QR CODE</h3>
                            <p>Você pode escanear o QR CODE ou copiar e colar o código abaixo no seu banco.</p>
                            <div style="margin-bottom: 15px;">
                                <img src="data:image/jpeg;base64,<?php echo $cobrancaFatura->qr_code_base64; ?>" alt="QRCode" style="width: 320px;border: 1px solid #2e2e2e;border-radius: 10px;">
                            </div>
                            <p class="codigo_qr_code">
                                <span style="font-weight: bold;font-size: 12px;">CÓDIGO DO QR CODE:</span><br/><br/>
                                <?php echo substr($cobrancaFatura->qr_code,0,50).'...';?>
                                <input type="hidden" id="chave" value="<?php echo $cobrancaFatura->qr_code;?>"/>
                            </p>
                            <a class="btn_1 botao_copiar mobile_btn" data-toggle="modal" data-target="#more-information" id="copy" style="background: #333333;margin-top: 10px;color: #ffffff;"> COPIAR CÓDIGO </a>
                            <div style="margin-top: 15px;">
                                <img src="<?= $assets ?>images/spinner.webp" width="80px" height="80px"/><br/>
                                Aguardando seu pagamento!
                            </div>
                            <hr style="border-top: 2px dotted black;"/>
                            <div class="budget_slider_info">
                                <p>1º Copie o código QR CODE acima</p>
                                <p>2º Abra o app do seu banco </p>
                                <p>3º Procure a função (PIX COPIA E COLA)</p>
                                <p>4º Confirme as informações  </p>
                                <p>5º Volte para o site de compras</p>
                            </div>

                            <div class="budget_slider_importante">
                                <p>
                                    Importante! <br/>
                                    Existe um limite para o valor de Pix que poderá ser movimentado no decorrer do período noturno (entre 20h e 6h) de R$1 mil.<br/>
                                    Para o período diurno (entre 6h e 20h), contudo, não há limite de movimentação.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- COMMON SCRIPTS -->
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>

<script type="text/javascript" charset="UTF-8">

    var pagou = false;

    $(window).load(function () {

        verificarPgamento();

        setInterval(function (){
            verificarPgamento();
        }, 5000);

        $('#copy').click(function (){
            copyTextToClipboard($('#chave').val(), '')
        });
    });

    function verificarPgamento() {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url().'pix/consulta/'.$cobrancaFatura->integracao.'/'.$cobrancaFatura->code;?>",
            success: function(retorno) {
                if (!pagou) {
                    <?php if (stristr($cobrancaFatura->code, 'pay_') ) { ?>
                        if (retorno.status === 'QUITADA') {
                            pagou = true;
                            window.location = '<?php echo base_url().'pix/confirmacao/'.$cobrancaFatura->code.'?token='.$this->session->userdata('cnpjempresa');?>';
                        } else if (retorno.status === 'CANCELADA'){
                            pagou = true;
                            window.location = '<?php echo base_url().'pix/error/'.$cobrancaFatura->code.'?token='.$this->session->userdata('cnpjempresa');?>';
                        }
                    <?php } else { ?>
                        if (retorno.status === 'QUITADA') {
                            pagou = true;
                            window.location = '<?php echo base_url().'pix/confirmacao/'.$cobrancaFatura->qr_code.'?token='.$this->session->userdata('cnpjempresa');?>';
                        } else if (retorno.status === 'CANCELADA'){
                            pagou = true;
                            window.location = '<?php echo base_url().'pix/error/'.$cobrancaFatura->qr_code.'?token='.$this->session->userdata('cnpjempresa');?>';
                        }
                    <?php } ?>
                }
            }
        });
    }

    function copyTextToClipboard(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }

        document.body.removeChild(textArea);
        $('.mobile_btn').css('background', '#28a745');
        $('.mobile_btn').html('COPIADO!')
    }
</script>

</body>
</html>