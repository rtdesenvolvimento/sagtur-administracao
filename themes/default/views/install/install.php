<?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
echo form_open_multipart("install/install_action/".$customer->id, $attrib); ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-database"></i> <?=lang('install');?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-4">
                <div class="form-group ">
                    <?= lang("company", "name") ?>
                    <?= form_input('name', $customer->name, 'class="form-control" readonly id="name"'); ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group ">
                    <?= lang("nome_responsavel", "nome_responsavel") ?>
                    <?= form_input('nome_responsavel', $customer->nome_responsavel, 'class="form-control" readonly id="nome_responsavel"'); ?>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="form-group ">
                    <?= lang("cnpj", "cnpj") ?>
                    <?= form_input('cnpj', $customer->vat_no, 'class="form-control" readonly id="cnpj"'); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <?= lang("theme_color", "theme_color"); ?>
                    <?php echo form_input('theme_color', '#e98b00', 'class="form-control tip" id="theme_color"', 'color'); ?>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="form-group ">
                    <?= lang("database", "database") ?>
                    <?= form_input('database', $customer->name_db, 'class="form-control" id="database" required="required"'); ?>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <?php echo form_checkbox('criar_subdominio', '1', TRUE, 'id="criar_subdominio"'); ?>
                    <label for="attributes" class="padding05"><?= lang('criar_subdominio'); ?></label>
                </div>
            </div>
            <div class="col-md-12" style="display: none;">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 0%;" id="progressBar"></div>
                </div>
            </div>
            <div class="col-md-12">
                <?= form_submit('add_install', lang('add_install'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
</div>
<?= form_close(); ?>

<script>
    $(document).ready(function () {
        $('formabc').submit(function (e) {

            e.preventDefault();

            $('#progressBar').css('width', '0%').text('0%');

            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: new FormData(this),
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (response) {

                    $('#progressBar').css('width', response.progress + '%').text(response.progress + '%');

                    if (response.progress === 100) {
                        alert(response.message); // Exibe uma mensagem de sucesso
                    } else {
                        console.log(response.message); // Loga a etapa atual
                    }
                },
                error: function (xhr, status, error) {
                    alert('Erro: ' + error);
                }
            });
        });
    });
</script>
