<?php

$sales_id = $this->uri->segment(3);
$vendedor = $this->uri->segment(4);
$product = $this->uri->segment(5);

?>

<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en" xmlns="http://www.w3.org/1999/html">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $Settings->site_name;?>|| RESERVA</title>

    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta name="description" content="<?php echo $Settings->site_name;?> || RESERVA" >
    <meta name="keywords" content="">
    <meta name="application-name" content="Resultatec Sistamas Digitais">
    <meta name="title" content="<?php echo $Settings->site_name;?> || RESERVA">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />
    <meta property="og:image" content="<?php echo base_url('assets/uploads/logos/logo.png');?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $Settings->site_name;?> || RESERVA" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta name="author" content="Resultec Sistemas Digitais"/>

    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/iCheck/skins/all.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/toastr/toastr.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/themes/theme-default.css" type="text/css" id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/estilo.css" type="text/css"/>

    <!-- end: CORE CSS
    <link rel="icon" href="https://www.viladebremen.com.br/imagens/favicon.ico"/>
     -->
    <style>

        body{
            margin: 0;
            color: #2c2f3b;
        }

        h2, .h2 {
            font-size: 32px;
            border-radius: 10px;
        }

        .fundo
        {
            background: rgb(255, 216, 166);!important;
            background-size: 100% 100%;
            bottom: 0;
            left: 0;
            overflow: auto;
            padding: 3em;
            position: absolute;
            right: 0;
            top: 0;
            font-size: 14px;
            font-weight: 800;
        }

        .cabecalho {
            background-image: url('http://viajeemfamilia.com.br/wp-content/uploads/2018/10/Turismo-de-Experi%C3%AAncia-2.jpg');
            color: #010101;
            font-family: 'Patrick Hand', fantasy;
            background-repeat: no-repeat;
        }

        @media screen and (min-width: 700px) {.painel_principal {}}
        @media screen and (max-width: 500px) {.painel_principal {margin-left: -25%}}
        @media screen and (min-width: 700px) {.logotipo {padding-left: 0%;}}
        @media screen and (max-width: 500px) {.logotipo {width: 50%;margin-left: 20%;margin-top: -20%}}
    </style>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<!-- end: SLIDING BAR -->
<div class="container">
    <div class="fundo">
        <!-- Menu -->
        <div class="container-fluid">
            <div class="row center">
                <div class="col-md-12">
                    <a href="#">
                        <img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" class="img-responsive" alt="Logotipo">
                    </a>
                </div>
            </div>
        </div>
        <div class="panel-body">
                <div class="panel panel-success" style="margin-top: 20px;">
                    <div class="panel-heading">Recebemos sua reserva!</div>
                    <div class="panel-body">
                        <div class="alert alert-block alert-warning fade in">
                            Recebemos sua reserva com sucesso! Entraremos em contato para confirmar a compra.<br/><br/>
                            <p><a href="<?php echo base_url().'/Linkcompra/reserva/'.$product.'/'.$vendedor;?>" class="btn btn-light-orange">Nova reserva</a></p><br/><br/>
                            <p><i class="fa fa-download"></i><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $sales_id;?>"> Clique aqui e baixe a sua reserva em PDF</a></p>
                            <p><br/>Atenciosamente,<br/><?php echo $Settings->site_name;?></p>
                        </div>
                    </div>
                </div>
                <!-- end: FORM WIZARD PANEL -->
            </div>
        </div>
    </div>
</div>

<script src="<?php echo base_url() ?>/assets/rapido/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url() ?>/assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/DataTables/media/js/DT_bootstrap.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>

<script src="<?php echo base_url() ?>assets/rapido/js/subview.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/subview-examples.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/form-wizard-cadastro-pessoa.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url() ?>assets/rapido/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->
</body>
<!-- end: BODY -->
</html>