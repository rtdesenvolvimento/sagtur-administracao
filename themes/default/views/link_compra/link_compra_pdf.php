<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $Settings->site_name;?>|| RESERVA</title>
    <link href="<?= $assets ?>styles/helpers/bootstrap.min.css" rel="stylesheet"/>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body onload="window.print();">

<!-- end: SLIDING BAR -->
<div class="container">
    <div class="fundo">

        <!-- start: FORM WIZARD PANEL -->
        <div class="panel-body col-md-7 col-md-offset-2">

            <!-- Menu -->
            <div class="container-fluid">
                <div class="row cabecalho col-md-12" style="text-align: center;">
                    <a href="#">
                        <img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="Logotipo da empresa">
                    </a>
                </div>
                <div class="row cabecalho col-md-12">
                    <div class="col-md-12" style="text-align: center;">
                        <H2>FORMULÁRIO DE RESERVA<br/><?php echo strtoupper($Settings->site_name);?></H2>
                    </div>
                </div>
            </div>

            <form action="<?php echo base_url().'sales/criarUmaNovaVendaViaLinkDePagamento';?>" role="form" class="smart-wizard form-horizontal" method="get" id="form">
                <div id="wizard" class="swMain">
                    <div class="progress progress-xs transparent-black no-radius active">
                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar partition-green step-bar">
                            <span class="sr-only"> 0% Complete (success)</span>
                        </div>
                    </div>
                    <div id="step-1">

                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"> <h4><div class="fa fa-map-marker"></div> Qual seu destino?</h4></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <input type="radio" checked="checked" style="margin-left: 4px;" value="<?php echo $product_detalhes->id;?>" name="viagem" class="grey radio-callback-local-embarque">
                                        <?php echo $product_detalhes->name?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Vendedor</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Vendedor <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <div class="input-group">
                                            <span class="input-group-addon"><div class="fa fa-user"></div></span>
                                            <select class="form-control" id="vendedor" name="vendedor">
                                                <?php foreach ($vendedores as $vendedor) {
                                                    if ($vendedor->id == $inv->biller_id) { ?>
                                                        <option value="<?php echo $vendedor->id;?>"><?php echo $vendedor->name;?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Informe seus dados</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        CPF <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" value="<?php echo $customer->vat_no?>" class="form-control" id="cpf" name="cpf" placeholder="CPF">
                                            <span id="spanCpf"></span>
                                            <i class="fa fa-hand-o-right"></i>
                                         </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome completo (idêntico ao RG e CPF, sem abreviações)<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" value="<?php echo $customer->name?>" class="form-control" id="nome" name="nome" placeholder="Nome">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Sexo <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <i class="fa fa-hand-o-right"></i>
                                            <select class="form-control" id="sexo" style="padding: 0 20px;" name="sexo">
                                                <option value="MASCULINO" <?php if($customer->sexo = 'MASCULINO') echo 'selected="selected"';?>>Masculino</option>
                                                <option value="FEMININO"  <?php if($customer->sexo = 'FEMININO') echo 'selected="selected"';?>>Feminino</option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        RG <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" value="<?php echo $customer->cf1?>" id="rg" name="rg" placeholder="RG">
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone celular com DDD <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" value="<?php echo $customer->cf5?>" id="celular" name="celular" placeholder="Telefone celular">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone residencial com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" value="<?php echo $customer->phone?>" id="telefone" name="telefone" placeholder="Telefone residencial">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome e Telefone de contato com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" value="<?php echo $customer->telefone_emergencia?>" id="telefone_emergencia" name="telefone_emergencia" placeholder="">
                                            <i class="fa fa-comment"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Alguma alergia ou doença grave
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" value="<?php echo $customer->alergia_medicamento?>" id="alergia_medicamento" name="alergia_medicamento" placeholder="Alergia ou doença grave">
                                            <i class="fa fa-ambulance"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group" id="alertaCadastroEncontado" style="display: none;font-size: 12px;">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <div class="alert alert-success">
                                            <button data-dismiss="alert" class="close">&times;</button>
                                            <strong>Já encontramos seu cadastro!</strong></br>Verifique e atualize seu telefone e e-mail de contato.
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        E-mail <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="email" class="form-control" value="<?php echo $customer->email?>" id="email" name="email" placeholder="E-mail">
                                            <i class="fa fa-send"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Data de nascimento<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" id="dataNascimento" value="<?php echo date('d/m/Y', strtotime($customer->data_aniversario))?>"  readonly name="dataNascimento" data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"> <h4><div class="fa fa-car"></div> Local de embarque</h4>
                                <span class="help-block">
								<i class="fa fa-info-circle"></i> verificar os embarques disponíveis do seu roteiro <br/>
								<i class="fa fa-info-circle"></i> Atenção 10 mim tolerância nos embarques e nos passeios
								</span>
                            </div>
                            <div class="panel-body" id="div_local_embarque">
                                <?php
                                $locais = $product_detalhes->origem;
                                $locais = strip_tags($locais);
                                $locais = explode(';',$locais);

                                foreach ($locais as $local){
                                    if ($local != ''){?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7" style="text-align: left;">
                                                <?php if ($inv->local_saida == $local){?>
                                                    <input type="radio" checked="checked" style="margin-left: 4px;" value="<?php echo $local;?>" name="local_embarque" class="grey">
                                                <?php } else { ?>
                                                    <input type="radio" style="margin-left: 4px;" value="<?php echo $local;?>" name="local_embarque" class="grey">
                                                <?php }?>
                                                <?php echo $local?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <?php
                        $arquivo = fopen('files/hospedagem.txt', 'r');
                        $hospedagens = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $hospedagens    .= $linha;
                        endwhile;
                        fclose($arquivo);

                        $hospedagens = explode(';',$hospedagens);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"> <h4><div class="fa fa-bed"></div> Hospedagem /  Day Use</h4></div>
                            <div class="panel-body">
                                <?php foreach ($hospedagens as $hospeagem){
                                    if ($hospeagem != ''){?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7" style="text-align: left;">
                                                <?php if ($inv->tipo_quarto_str == $hospeagem){?>
                                                    <input type="radio" style="margin-left: 4px;" checked="checked" value="<?php echo $hospeagem;?>" name="tipos_hospedagem" class="grey">
                                                <?php }else { ?>
                                                    <input type="radio" style="margin-left: 4px;" value="<?php echo $hospeagem;?>" name="tipos_hospedagem" class="grey">
                                                <?php }?>
                                                <?php echo $hospeagem?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;display: none;">
                            <div class="panel-heading">Assento do ônibus (no caso de ônibus lotado, faremos a marcação do assento, favor escolha uma das opções que faremos o possível para deixá-lo onde prefere e sempre com as pessoas que vão junto com você. O número do seu assento irá no voucher, que será enviado até 2 dias antes da viagem) *</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7" style="text-align: left;">
                                        <input type="radio" style="margin-left: 4px;" checked="checked" value="Do meio para frente" name="posicao_assento" class="grey">Do meio para frente
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7" style="text-align: left;">
                                        <input type="radio" style="margin-left: 4px;" value="Do meio para o fundo" name="posicao_assento" class="grey">Do meio para o fundo
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="step-2" style="display: none;">
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading"><h3>Mais alguém vai viajar com voce? Clique em SIM e adicione.</h3></div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <input type="radio" checked="checked" style="margin-left: 4px;" value="" name="maispassageiros" class="grey radio-callback-ifUnchecked">NÃO
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-7">
                                        <input type="radio" style="margin-left: 4px;" value="" name="maispassageiros" class="grey radio-callback-ifChecked">SIM
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;display: none;" id="divAdicionarMaisPassageiros">
                            <div class="panel-heading">Passageiro adicional</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome completo (idêntico ao RG e CPF, sem abreviações)
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" required="required"  class="form-control" id="nomeDependente" name="nomeDependente[]" placeholder="Nome">
                                            <i class="fa fa-user"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Sexo
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <select class="form-control" name="sexoDependente[]" style="padding: 0 20px;">
                                                <option value="MASCULINO">Masculino</option>
                                                <option value="FEMININO">Feminino</option>
                                            </select>
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Criança de colo <span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <select class="form-control" id="crianca_colo" name="crianca_colo[]" style="padding: 0 20px;">
                                                <option value="NAO">NÃO</option>
                                                <option value="SIM">SIM</option>
                                            </select>
                                            <small>Crianças até cinco anos (5 anos) podem viajar no colo</small>
                                           <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">CPF</label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control cpfDependente" name="cpfDependente[]" placeholder="CPF">
                                            <span id="spanCpf"></span>
                                            <small>Se criança e não possuir CPF, deixe em branco.</small>
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">RG</label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control"  name="rgDependente[]" placeholder="RG">
                                            <i class="fa fa-hand-o-right"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Data de nascimento</label>
                                    <div class="col-sm-7">
                                         <span class="input-icon">
                                            <input type="text" required="required" readonly data-date-format="dd/mm/yyyy" data-date-viewmode="years" class="form-control date-picker" name="dataNascimentoDependente[]" placeholder="Data de nascimento">
                                             <i class="fa fa-calendar"></i>
                                         </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Telefone celular com DDD
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="tel" class="form-control" id="celularDependente" name="celularDependente" placeholder="Telefone celular">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Nome de contato e telefone de emergência com DDD
                                    </label>
                                    <div class="col-sm-7">
                                       <span class="input-icon">
                                           <input type="text" class="form-control" id="telefoneEmergenciaDependente" name="telefoneEmergenciaDependente[]" placeholder="Telefone de Emergência">
                                            <i class="fa fa-phone"></i>
                                       </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Alguma alergia ou doença grave
                                    </label>
                                    <div class="col-sm-7">
                                        <span class="input-icon">
                                            <input type="text" class="form-control" id="alergiaMedicamentoDependente" name="alergiaMedicamentoDependente[]" placeholder="Alergia ou doença grave">
                                            <i class="fa fa-ambulance"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="inserirDependente"></div>
                        <div class="form-group">
                            <div class="col-sm-6">
                                <button class="btn btn-azure btn-block adicionar-dependente" type="button" style="display: none;">
                                    <i class="fa fa-plus"></i> ADICIONE MAIS UM PASSAGEIRO
                                </button>
                            </div>
                        </div>
                    </div>
                    <div id="step-3">

                        <?php
                        $arquivo = fopen('files/formas_pagamento.txt', 'r');
                        $formas_pagamento = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $formas_pagamento    .= $linha;
                        endwhile;
                        fclose($arquivo);

                        $pagamentos = explode('@label@',$formas_pagamento);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">
                                FORMA DE PAGAMENTO, DEPOSITO OU CARTÃO DE CREDITO PARA RESERVAS
                                SINAL DE 10%, RESTANTE PODE SER PARCELADODIRETO COM AGENCIA OU
                                CARTÃO DE CREDITO PELO PAGSEGURO.*com acréscimo de juros 2,99% a.m
                            </div>
                            <div class="panel-body">
                                <?php
                                $contador = 0;
                                foreach ($pagamentos as $pagamento){
                                    if ($pagamento != ''){
                                        $pag = explode('@quebra@',$pagamento); ?>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-7" style="text-align: left;">
                                                <?php if ($inv->condicao_pagamento == $pag[0]){?>
                                                    <input type="radio" style="margin-left: 4px;" checked="checked" value="<?php echo $pag[0];?>" name="formas_pagamento" class="grey">
                                                <?php }else { ?>
                                                    <input type="radio" style="margin-left: 4px;" value="<?php echo $pag[0];?>" name="formas_pagamento" class="grey">
                                                <?php }?>
                                                <?php echo $pag[0];?>
                                                <a onclick="abrirInformacoesPagamento(<?php echo $contador;?>);" style="cursor: pointer;"><i class="fa fa-eye"></i></a>
                                                <div class="form-group" id="verDadosFormaPagamento<?php echo $contador;?>" style="font-size: 10px;margin-top: 10px;display: none;">
                                                    <div class="col-sm-12">
                                                        <div class="alert alert-success">
                                                            <?php echo $pag[1];?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php $contador = $contador +1;} ?>
                            </div>
                        </div>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Informações adicionais</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">
                                        Se você estiver viajando com outra pessoa que preencheu contrato separado informe aqui o nome completo dela para que possamos deixá-los no mesmo ônibus, caso alguém que vai com você ainda não preencheu a ficha, diga para ela escrever o seu nome aqui.
                                    </label>
                                    <div class="col-sm-7">
                                        <textarea id="observacoes" class="form-control" rows="10" cols="10" placeholder="Informações adicionais." name="observacoes"><?php echo strip_tags($inv->note);?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        $arquivo = fopen('files/aceite_contrato.txt', 'r');
                        $aceistes_contrato = '';

                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $aceistes_contrato    .= $linha;
                        endwhile;
                        fclose($arquivo);
                        $aceistes_contrato_label = explode('@label@',$aceistes_contrato); ?>

                        <?php
                        $contadorAceite = 0;
                        foreach ($aceistes_contrato_label as $labelAceite){
                            if ($labelAceite != '') {
                                $labelAceite = explode('@quebra@', $labelAceite); ?>
                                <div class="panel panel-info" style="margin-top: 20px;">
                                    <div class="panel-heading"><?php echo $labelAceite[0];?></div>
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="col-sm-12" style="text-align: left;">
                                                <?php echo $labelAceite[1];?>
                                                <br/><br/>
                                                <input type="checkbox" class="grey" value="" required="required" name="temos_de_aceite<?php echo $contadorAceite;?>[]" id="" checked="checked"> ACEITO / CONCORDO
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php $contadorAceite = $contadorAceite + 1;} ?>
                        <?php
                        $arquivo = fopen('files/dados_empresa.txt', 'r');
                        $dadosEmpresa = '';
                        while (!feof($arquivo)) :
                            $linha          = fgets($arquivo, 1024);
                            $dadosEmpresa    .= $linha;
                        endwhile;
                        fclose($arquivo);
                        ?>
                        <div class="panel panel-info" style="margin-top: 20px;">
                            <div class="panel-heading">Dados da Empresa</div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <div class="col-sm-12" style="text-align: left;">
                                        <?php echo $dadosEmpresa?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-6 col-sm-offset-6">
                                <button type="submit" class="btn btn-blue next-step btn-block finish-step" style="text-align: left;">
                                    Aceitar e concluir minha reserva
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <!-- end: FORM WIZARD PANEL -->
    </div>
</div>

<!-- end: BODY -->
</html>