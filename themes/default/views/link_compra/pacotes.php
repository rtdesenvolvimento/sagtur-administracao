<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.0 Author: ClipTheme -->
<!--[if IE 8]>
<html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- start: HEAD -->
<head>
    <title><?php echo $Settings->site_name; ?>|| PACOTES DISPONÍVEL</title>

    <!-- start: META -->
    <meta charset="utf-8"/>
    <!--[if IE]>
    <meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1"/><![endif]-->
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <meta name="description" content="<?php echo $Settings->site_name; ?> || PACOTES DISPONÍVEL">
    <meta name="keywords" content="">
    <meta name="application-name" content="Resultatec Sistamas Digitais">
    <meta name="title" content="<?php echo $Settings->site_name; ?> || PACOTES DISPONÍVEL">
    <meta name="robots" content="all"/>
    <meta name="language" content="br"/>
    <meta name="robots" content="follow"/>
    <meta property="og:image" content="<?php echo base_url('assets/uploads/logos/logo.png'); ?>"/>
    <meta property="og:type" content="article"/>
    <meta property="og:description" content="<?php echo $Settings->site_name; ?> || PACOTES DISPONÍVEL"/>
    <meta property="og:url" content="<?=current_url();?>" />
    <meta name="author" content="Resultec Sistemas Digitais"/>

    <!-- end: META -->
    <!-- start: MAIN CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/iCheck/skins/all.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/animate.css/animate.min.css">
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.theme.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/owl-carousel/owl-carousel/owl.transitions.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/summernote/dist/summernote.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/toastr/toastr.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/DataTables/media/css/DT_bootstrap.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
    <link rel="stylesheet"
          href="<?php echo base_url() ?>assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
    <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
    <!-- start: CORE CSS -->

    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/lightbox2/css/lightbox.css">
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/styles-responsive.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/plugins.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/themes/theme-default.css" type="text/css"
          id="skin_color">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/print.css" type="text/css" media="print"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/css/estilo.css" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/datepicker/css/datepicker.css">

    <link rel="icon" href="<?php echo base_url() ?>/assets/images/favicon.ico"/>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>

<div class="main-content" style="min-height: 292px;">
    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
    <div class="container">
        <!-- start: PAGE HEADER -->

        <div class="row">
            <div class="col-sm-12">
                <!-- start: FORM WIZARD PANEL -->
                <div class="panel panel-white">
                    <div class="panel-body">
                        <div class="container-fluid">
                            <div class="row cabecalho col-md-12" style="text-align: center;">
                                <a href="#">
                                    <img src="<?php echo base_url('assets/uploads/logos/logo.png');?>" alt="Logotipo da empresa">
                                </a>
                            </div>
                            <div class="row cabecalho col-md-12">
                                <div class="col-md-12" style="text-align: center;">
                                    <H2>PACOTES DISPONÍVEL PARA VENDA <br/><?php echo strtoupper($Settings->site_name);?></H2>
                                </div>
                            </div>
                        </div>
                        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form' ,'id' => 'form' , 'class' => 'smart-wizard form-horizontal');
                        echo form_open_multipart("agendamento/confirmarAgendamento", $attrib); ?>
                        <div id="wizard" class="swMain">
                                <ul class="anchor">
                                    <li>
                                        <a href="#step-1" class="selected" isdone="1" rel="1">
                                            <div class="stepNumber">1</div>
                                            <span class="stepDesc">1º Passo<br><small>Pacote</small> </span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="stepContainer" style="height: 353px;">
                                    <div class="progress progress-xs transparent-black no-radius active content">
                                        <div aria-valuemax="100" aria-valuemin="0" role="progressbar"
                                             class="progress-bar partition-green step-bar" style="width: 25%;">
                                            <span class="sr-only"> 0% Complete (success)</span>
                                        </div>
                                    </div>
                                    <div id="step-1" class="content" style="display: block;">

                                        <div id="timeline" class="demo1">
                                            <div class="timeline">
                                                <div class="spine"></div>
                                                <div class="date_separator" id="november" data-appear-top-offset="-400">
                                                    <span style="font-size: 15px;background: #1fbba6;">ESCOLHA SEU PACOTE</span>
                                                </div>
                                                <ul class="columns">
                                                    <style>
                                                        .preco_pacote {
                                                            color: #1fbba6;
                                                            font-weight: bold;
                                                            font-size: 27px;
                                                        }
                                                    </style>
                                                    <?php foreach ($servicos as $servico){
                                                        $images = $this->products_model->getProductPhotos($servico->id);

                                                        $vendas = $this->sales_model->getTotalVendasRealizadasParaAhViagem($servico->id);
                                                        $totalVendas = 0;
                                                        foreach ($vendas as $venda) $totalVendas = $venda->quantity;

                                                        if ($totalVendas == '') $totalVendas = 0;

                                                        $disponivel = (int) ( $servico->quantidadePessoasViagem - $totalVendas);

                                                        ?>
                                                        <li>
                                                            <div class="timeline_element partition-white" id="timeline_element<?php echo $servico->id;?>">
                                                                <div class="timeline_date" style="text-align: center;">
                                                                    <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$servico->image);?>"
                                                                       data-lightbox="<?php echo $servico->id;?>" data-title="Contratar">
                                                                        <img src="<?php echo base_url('assets/uploads/'.$servico->image);?>">
                                                                    </a>
                                                                    <?php foreach ($images as $image){?>
                                                                        <a class="thumb-info" href="<?php echo base_url('assets/uploads/'.$image->photo);?>"
                                                                           data-lightbox="<?php echo $servico->id;?>" data-title="Contratar">
                                                                         </a>
                                                                    <?php }?>
                                                                </div>
                                                                <div class="readmore" style="text-align: center;margin-top: -30px;">
                                                                    <a  href="<?php echo base_url()?>/linkcompra/reserva/<?php echo $servico->id;?>/<?php echo $vendedor;?>" id="<?php echo $servico->id;?>" class="btn btn-green addServico"
                                                                       style="border-radius: 23px;" >
                                                                        <span class="thumb-info-title">Reservar</span>
                                                                        <i class="fa fa-shopping-cart" style="font-size: 35px;"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="inline-block" style="width: 100%;text-align: center;">
                                                                    <span class="block week-day text-extra-large"><?php echo $servico->category;?></span>
                                                                    <span class="block week-day text-extra-large"><?php echo $servico->name;?></span>
                                                                    <span class="block month preco_pacote">A partir de <?php echo $this->sma->formatMoney($servico->price);?></span>
                                                                    <span class="block month preco_pacote"><?php echo $disponivel. ' vagas disponível';?></span>
                                                                </div>
                                                                <div class="timeline_content" style="text-align: center;">
                                                                    <?php echo $servico->product_details;?>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="actionBar">
                                    <div class="msgBox">
                                        <div class="content"></div>
                                        <a href="#" class="close">X</a></div>
                                    <div class="loader">Loading</div>
                                    <a href="#" class="buttonFinish buttonDisabled">Finish</a>
                                    <a href="#" class="buttonNext">Next</a>
                                    <a href="#" class="buttonPrevious buttonDisabled">Previous</a></div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
                <!-- end: FORM WIZARD PANEL -->
            </div>
        </div>
        <!-- end: PAGE CONTENT-->
    </div>
    <footer class="inner">
        <div class="footer-inner">
            <div class="pull-left">
                <?php echo date('Y');?> © <?php echo $Settings->site_name?>.
            </div>
            <div class="pull-right">
                <span class="go-top"><i class="fa fa-chevron-up"></i></span>
            </div>
        </div>
    </footer
</div>

<script src="<?php echo base_url() ?>/assets/rapido/plugins/jQuery/jquery-2.1.1.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/iCheck/jquery.icheck.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/moment/min/moment.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-cookie/jquery.cookie.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/velocity/jquery.velocity.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script src="<?php echo base_url() ?>/assets/rapido/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/toastr/toastr.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/truncate/jquery.truncate.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/summernote/dist/summernote.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script src="<?php echo base_url() ?>/assets/rapido/plugins/lightbox2/js/lightbox.min.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/form-wizard-cadastro-agendamento.js"></script>

<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE JAVASCRIPTS  -->
<script src="<?php echo base_url() ?>assets/rapido/js/main.js"></script>
<!-- end: CORE JAVASCRIPTS  -->

<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="<?php echo base_url() ?>assets/rapido/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script src="<?php echo base_url() ?>assets/rapido/js/pages-timeline.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->


<script>
    jQuery(document).ready(function() {
    });
</script>

</body>
<!-- end: BODY -->
</html>