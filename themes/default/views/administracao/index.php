<?php

function getStatus($status, $dias) {
    if ($status == 'ABERTA') {
        if ($dias < 0)  return '<div class="text-center"><span class="label label-danger">Vencida</span></div>';
        else return '<div class="text-center"><Vencerspan class="label label-warning">À Vencer</Vencerspan></div>';
    } else if ($status == 'NAO ENCONTRADO') {
        return  '<div class="text-center"> - </div>';
    }

    return  '<div class="text-center"><span class="label label-success">Pago</span></div>';
}
?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('Empresas'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="CusData" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th></th>
                            <th style="text-align: left;width: 50%"><?= lang("company"); ?></th>
                            <th class="center" style="width: 10%"><?= lang("Última Venda"); ?></th>
                            <th class="center" style="width: 3%;"><?= lang("Dias"); ?></th>
                            <th class="right" style="width: 10%;text-align: right;"><?= lang("Total Vendas"); ?></th>
                            <th class="center" style="width: 3%;">Qtd</th>
                            <th class="center" style="width: 10%"><?= lang("Plano"); ?></th>
                            <th class="center" style="width: 3%"><?= lang("Usuários"); ?></th>
                            <th class="center" style="width: 10%"><?= lang("Vencimento"); ?></th>
                            <th class="center"><?= lang("Dias"); ?></th>
                            <th class="center"><?= lang("status"); ?></th>
                            <th class="center" style="width: 3%;">Versão</th>
                            <th class="center" style="width: 3%;"></th>
                            <th class="center" style="width: 3%;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalVolumeVendas = 0;
                        $qtdVendasNoAno = 0;
                        $totalClientes  = 0;
                        $totalClientesPoucoUso = 0;
                        $totalClientesAtrasados = 0;

                        foreach ($customers as $customer) {

                            $dtHoje = date('Y-m-d');

                            $diffUltimaVenda = $this->sma->diferencaEntreDuasDatasEmDias($dtHoje, $customer['dataUltimaVenda'])*-1;
                            $diffVencimento =  $this->sma->diferencaEntreDuasDatasEmDias($dtHoje, $customer['dataUltimoVencimento']);

                            $cnpj = $customer['cnpj'];

                            $totalVolumeVendas += $customer['totalVendas'];
                            $qtdVendasNoAno += $customer['qtdVendas'];

                            if ($diffUltimaVenda >= 15) {
                                $totalClientesPoucoUso++;
                            }

                            if ($diffVencimento < 0) {
                                $totalClientesAtrasados++;
                            }

                            $totalClientes++;
                            ?>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox empresa_ck" type="checkbox" cnpj="<?php echo $cnpj;?>" <?php if ($customer['status'] == 1) echo 'checked="checked"'; ?> name="check"/></th>
                                <td style="text-align: left;color: #428bca;" class="bold"><?php echo $customer['name'];?></td>
                                <td style="text-align: center;"><?php echo $this->sma->hrsd($customer['dataUltimaVenda']);?></td>
                                <td style="text-align: center;<?php echo $background;?>"><?php echo $diffUltimaVenda ;?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($customer['totalVendas']);?></td>
                                <td style="text-align: center;"><?php echo $customer['qtdVendas'];?></td>
                                <td style="text-align: center;"><?php echo $customer['nome_grupo'];?></td>
                                <td style="text-align: center;"><?php echo ($customer['totalUsuarios'] - 1);?></td>
                                <td style="text-align: center;"><?php if ($customer['dataUltimoPagamentoMesCorrente'] != null) echo $this->sma->hrsd($customer['dataUltimoPagamentoMesCorrente']); else echo $this->sma->hrsd($customer['dataUltimoVencimento']);?></td>
                                <?php if ($customer['statusUltimoVencimento'] == 'ABERTA') {?>
                                    <td style="text-align: center;"><?php  if ($diffVencimento < 0) echo $diffVencimento;?></td>
                                <?php } else { ?>
                                    <td style="text-align: center;"></td>
                                <?php }?>
                                <?php if ($customer['dataUltimoPagamentoMesCorrente'] != null){?>
                                    <td style="text-align: center;"><div class="text-center"><span class="label label-success">Pago</span></div></td>
                                <?php } else { ?>
                                    <td style="text-align: center;"><?php echo getStatus($customer['statusUltimoVencimento'], $diffVencimento);?></td>
                                <?php } ?>
                                <td style="text-align: center;"><?php echo $customer['versao'];?></td>

                                <td style="text-align: center;">
                                    <a href="<?php echo base_url();?>/administracao/modal_view/<?php echo $cnpj;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <span class="fa fa-eye"></span>
                                    </a>
                                </td>
                                <td style="text-align: center;">
                                    <form action="https://sistema.sagtur.com.br/auth/login" method="post" accept-charset="utf-8" target="_blank">
                                        <input type="hidden" name="token" value="d774f9a4314a1220962db98e187aa972" />
                                        <input type="hidden" name="cnpjempresa" value="<?php echo $cnpj;?>" />
                                        <input type="hidden" name="identity" value="andre@resultatec.com.br" />
                                        <input type="hidden" name="password" value="2078404abC@!" />
                                        <button type="submit"><i class="fa fa-external-link"></i> </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <th></th>
                        <th>Total <?php echo $totalClientes;?></th>
                        <th></th>
                        <th style="text-align: center;"><?php echo $totalClientesPoucoUso;?></th>
                        <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalVolumeVendas); ?></th>
                        <th style="text-align: center;"><?php echo $qtdVendasNoAno;?></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th style="text-align: center;"><?php echo $totalClientesAtrasados;?></th>
                        <th></th>
                        <th></th>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {

      	$('#CusData').DataTable({
            iDisplayLength: -1,
            paging: true,       // Ativar paginação
            searching: true,    // Ativar busca
            ordering: true,     // Ativar ordenação
            info: true,         // Exibir informações
            pageLength: 500,    // Exibir 500 registros por página
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.13.6/i18n/Portuguese-Brasil.json"
            }
        });
      
      // Delegação para elementos dinâmicos
    $('#CusData').on('ifChecked', '.empresa_ck', function (e) {
        console.log('checked');

        if (confirm('Deseja Realmente ATIVAR essa empresa?')) {
            $.ajax({
                type: "get",
                url: site.base_url + "administracao/ativar",
                data: {
                    cnpj: $(this).attr('cnpj'),
                },
                dataType: "json",
                success: function (data) {
                    alert('Empresa ativada com sucesso!');
                },
                error: function () {
                    alert('Ocorreu um erro ao ativar a empresa.');
                }
            });
        } else {
            location.reload();
        }
    });

    $('#CusData').on('ifUnchecked', '.empresa_ck', function (e) {
        if (confirm('Deseja Realmente INATIVAR essa empresa?')) {
            $.ajax({
                type: "get",
                url: site.base_url + "administracao/inativar",
                data: {
                    cnpj: $(this).attr('cnpj'),
                },
                dataType: "json",
                success: function (data) {
                    alert('Empresa inativada com sucesso!');
                },
                error: function () {
                    alert('Ocorreu um erro ao inativar a empresa.');
                }
            });
        } else {
            location.reload();
        }
    });
      
        $('.empresa_ck').on('ifChecked', function (e) {
            console.log('checked');

            if (confirm('Deseja Realmente ATIVAR essa empresa?')) {
                $.ajax({
                    type: "get",
                    url: site.base_url + "administracao/ativar",
                    data: {
                        cnpj: $(this).attr('cnpj'),
                    },
                    dataType: "json",
                    success: function (data) {
                    }
                });
            } else {
                location.reload();
            }
        });

        $('.empresa_ck').on('ifUnchecked', function (e) {
            if (confirm('Deseja Realmente INATIVAR essa empresa?')) {
                $.ajax({
                    type: "get",
                    url: site.base_url + "administracao/inativar",
                    data: {
                        cnpj: $(this).attr('cnpj'),
                    },
                    dataType: "json",
                    success: function (data) {
                    }
                });
            } else {
                location.reload();
            }
        });
    });
</script>
