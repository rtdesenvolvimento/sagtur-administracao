<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Detalhes do Cliente</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php foreach ($users as $user){?>
                    <div class="col-xs-12">
                        <div class="well">
                            *NOVO LINK DE RESERVA*<br/><br/>
                            <?php if ($user->tipo == 1){?>
                                <span class="bold">*Nome:* <?php echo $user->name;?> </span><br/>
                                <span class="bold">*Cargo:* Administrador</span><br/>
                            <?php } else { ?>
                                <span class="bold">*Nome:* <?php echo $user->name;?> </span><br/>
                                <span class="bold">*Cargo:* Vendedor</span><br/>
                            <?php } ?>
                            <br/>
                            <span>
                          <?php if ($configuracaoGeral->own_domain) {?>
                              <a href="<?php echo $configuracaoGeral->url_site;?>/<?php echo $user->biller_id;?>" target="<?php echo $nome_empresa;?>">
                                 <?php echo $configuracaoGeral->url_site;?>/<?php echo $user->biller_id;?>
                             </a>
                          <?php }  else {?>
                              <a href="https://www.suareservaonline.com.br/<?php echo $nome_empresa;?>/<?php echo $user->biller_id;?>" target="<?php echo $nome_empresa;?>">
                              	https://www.suareservaonline.com.br/<?php echo $nome_empresa;?>/<?php echo $user->biller_id;?>
                             </a>
                          <?php } ?>
                         <br/>-------------------------------------------------------------------- <br/> <br/>
                      	</span>
                        </div>

                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>