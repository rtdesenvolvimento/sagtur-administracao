<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('company'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="TableaEmpresas" cellpadding="0" cellspacing="0" border="0"
                           class="table table-bordered table-condensed table-hover table-striped">
                        <thead>
                        <tr class="primary">
                            <th style="text-align: center;width: 3%;">#</th>
                            <th style="text-align: left;"><?= lang("company"); ?></th>
                            <th style="text-align: left;"><?= lang("Base"); ?></th>
                            <th style="text-align: right;"><?= lang("Qtd"); ?></th>
                            <th style="text-align: right;display: none;"><?= lang("Total"); ?></th>
                            <th style="text-align: center;width: 5%;"><?= lang("status"); ?></th>
                            <th class="center" style="width: 3%;"><i class="fa fa-fw fa-database"></i></th>
                            <th class="center" style="width: 3%;display: none;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $totalVolumeVendas = 0;
                        foreach ($companies as $company) {
                            $totalVolumeVendas += (double) $company['total_vendido'];
                            ?>
                            <tr>
                                <td style="text-align: center;width: 3%;"><?php echo $company['qtd'];?></td>
                                <td style="text-align: left;"><?php echo $company['name'];?></td>
                                <td style="text-align: left;"><?php echo $company['data_base'];?></td>
                                <td style="text-align: right;"><?php echo $company['total_vendas'];?></td>
                                <th style="text-align: right;display: none;"><?php echo $this->sma->formatMoney($company['total_vendido']);?></th>
                                <th style="text-align: center;width: 5%;"><?php if ($company['status']) echo '<span class="label label-success">Ativo</span>'; else echo '<span class="label label-danger">Inativo</span>';?></th>
                                <td style="text-align: center;">
                                    <a href="<?php echo base_url('administracao/db_backup/' . $company['data_base']); ?>"
                                       class="btn btn-primary btn-sm" title="Backup" target="_blank">
                                        <i class="fa fa-fw fa-database"></i>
                                    </a>
                                </td>
                                <td style="text-align: center;display: none;">
                                    <form action="https://sistema.sagtur.com.br/auth/login" method="post" accept-charset="utf-8" target="_blank">
                                        <input type="hidden" name="token" value="d774f9a4314a1220962db98e187aa972" />
                                        <input type="hidden" name="cnpjempresa" value="<?php echo $company['cnpj'];?>" />
                                        <button type="submit" class="btn btn-primary btn-sm" title="Acessar">
                                            <i class="fa fa-fw fa-sign-in"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr class="primary">
                            <th style="text-align: center;width: 3%;">#</th>
                            <th style="text-align: left;"><?= lang("company"); ?></th>
                            <th style="text-align: left;"><?= lang("Base"); ?></th>
                            <th style="text-align: right;"><?= lang("Qtd"); ?></th>
                            <th style="text-align: right;display: none;"><?php echo $this->sma->formatMoney($totalVolumeVendas);?></th>
                            <th style="text-align: center;width: 5%;"><?= lang("status"); ?></th>
                            <th class="center" style="width: 3%;"><i class="fa fa-fw fa-database"></i></th>
                            <th class="center" style="width: 3%;display: none;"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

    });
</script>
