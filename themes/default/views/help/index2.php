<style>
    .lista {
        padding: 5px;
        font-size: 14px;
        line-height: 1.6;
    }
    hr {
        margin-top: 20px;
        margin-bottom: 20px;
        border: 0;
        border-top: 1px solid #0088cc;
    }
</style>
<div class="row" style="margin-bottom: 15px;">
    <div class="col-lg-12">
        <div class="box">
            <div class="box-header">
                <h2 class="blue"><i class="fa fa-question"></i><span class="break"></span>Base de conhecimento</h2>
            </div>
            <div class="box-content">
                <div class="col-lg-12">
                    <div class="col-lg-6">
                        <h2>Cadastros de serviços</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/FXWquj6Q2Kg" target="_blank">Inserindo um novo serviço.</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/_ci_nbM-xjs" target="_blank">Editar as informações de um serviço.</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/Ug5iyHjcZ4U" target="_blank">Inserir informações complementares nos serviços.</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/kNDuhbLmguM" target="_blank">inserir imagem de destaque e galeria de imagens</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/8fPVZ30ClBE" target="_blank">Configurar transporte rodoviário e locais de embarque</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/E4P6cCy6ExQ" target="_blank">Configurar os valores do bate e volta (DayUse).</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/5Fy4HUYEsMs" target="_blank">Configurar os valores por tipo de hospedagem</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/5Fy4HUYEsMs" target="_blank">Configurar os valores por tipo de hospedagem</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/5Fy4HUYEsMs" target="_blank">Configurar a Comissão dos vendedores</a></div>

                        <h2>Cadastro de Serviços Adicionais</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/a05Z6d_U2XU" target="_blank">Cadastrar Serviços Adicionais</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/GfW2Cv5rBkY" target="_blank">Vincular um serviço adicional ao cadastro de um serviço</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/6RWXs6PGtZY" target="_blank">Configurar o valor pelo tipo de passageiro (Bebe/Criança/Adulto)</a></div>



                        <h2>Vendas</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/jJFwCSR5MAA" target="_blank">Como lançar uma cotação</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/oECAuXcPR0k" target="_blank">Como lançar uma venda (faturada)</a></div>



                        <h2>Financeiro</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/ob2J2_S1xR8" target="_blank">Como adicionar pagamento e anexar comprovantes</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/9_280_ew8Ug" target="_blank">Como gerar boletos (apenas integração com JUNO/PAGSEGURO)</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=OOmqhS2FBO0" target="_blank">Como gerar crédito de clientes (cancelamentos)</a></div>


                        <h2>Configurações</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=jgKXt8JtY-A" target="_blank">Configuras as Taxas de Cartão de Crédito e Boletos</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=jgKXt8JtY-A" target="_blank">Habilitar as condiçoes de pagamento</a></div>

                        <h2>Tutorial do Vendedor</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=CgUyBGzMUfc" target="_blank">Aprender a usar o sistema como vendedor</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=3pqim6wh6mg" target="_blank">Como o vendedor gera o link de compras e emite boletos (JUNO/PAGSEGURO)</a></div>

                    </div>





                    <div class="col-lg-6">

                        <h2>Cadastro de Passageiros</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/lUxj7CvRuXo" target="_blank">Cadastrar um novo passageiro</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/Ur_29mR__e4" target="_blank">Ediar as informações de passageiros</a></div>


                        <h2>Programação de Viagens (Agenda)</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/BWWwEh8HZNs" target="_blank">Como abrir uma data de saída para uma viagem</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/FGrDiNOAqWk" target="_blank">Altera a data de saída ou número de vagas de uma viagem</a></div>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/embed/lUxj7CvRuXo" target="_blank">Como excluir uma da de saída de viagem</a></div>


                        <h2>Relatórios</h2>
                        <hr/>
                        <div class="lista"><i class="fa fa-book"></i> <a href="https://www.youtube.com/watch?v=O4HhRm4mc1I" target="_blank">Meus clientes não estão aparecendo no relatório de embarque, o que fazer?</a></div>


                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>