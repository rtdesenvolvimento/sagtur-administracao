
<style>
    h3 {
        line-height: 1.3;
    }
</style>
<div class="box">
    <div class="box-header" style="cursor: pointer;" onclick="ocultarExibir('modulo-cadastro');">
        <h2 class="blue">
            <i class="fa fa-map-signs"></i>
            Dúvidas do Módulo de  Serviços
        </h2>
    </div>
    <div class="box-content" style="display:none;" id="modulo-cadastro">
        <div class="row">


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/FXWquj6Q2Kg" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como inserir um novo serviço.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/FXWquj6Q2Kg"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                <br/><br/>
                Neste vídeo mostramos como realizar o cadastro de serviços turísticos, como pacotes
                passeios, city tour, ingressos e outros.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/_ci_nbM-xjs" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como editar as informações de um serviço já cadastrado
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/_ci_nbM-xjs"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                <br/><br/>
                Se informe aqui neste tutorial como realizar a edição das informações de um serviço já cadastrado.
            </div>

            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/Ug5iyHjcZ4U" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como inserir informações complementares nos serviços.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/Ug5iyHjcZ4U"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                <br/><br/>
                 Neste vídeo mostramos como inseriri informações complementares nos serviços, completando
                detalhes importantes como descrição, roteiros e informações sobre valores e pagamentos.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/kNDuhbLmguM" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Vejamos agora como inserir uma imagem de destaque ao serviço e galeria de imagens
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/kNDuhbLmguM" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                <br/><br/>
                Vejamos como inserir imagem de destaque ao pacote/serviço ou passeio e também como inserir uma galeria de fotos. Essas imagens
                por exemplo, serão exibidas no link de compras, dando mais profissionalista na hora da venda.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/8fPVZ30ClBE" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Confira aqui como configurar transporte rodoviário e locais de embarque
                        dos seus pacotes.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/8fPVZ30ClBE"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                <br/><br/>
                 Mostramos aqui como realizar a configuração dos transporte rodoviários e os locais de embarque
                dos seus pacotes, com isso é possível emitir relatórios de embarque por exemplo.
            </div>



            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/E4P6cCy6ExQ" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Veja, neste vídeo como configurar os valores dos seus serviços pela
                        idade dos passageiros.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300" src="https://www.youtube.com/embed/E4P6cCy6ExQ"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <br/><br/>
                Neste vídeo você poderá ver como configurar os valores dos serviços por faixa etária, ou seja pela iade dos
                passeiros.
            </div>



            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/5Fy4HUYEsMs" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Aprenda aqui, como configurar os Tipos de Quartos dos pacotes e os valores pela idade dos passageiros
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/5Fy4HUYEsMs"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Neste vídeo mostro como configurar os tipos de quarto e seus respectivos valores por faixa etária.
                Ao realizar uma venda o sistema exibira os tipos de quartos configurados e criará um subtotal de acordo com a idade dos passageiros.
            </div>





            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/a05Z6d_U2XU" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Confira como cadastrar Serviços Adicionais
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/a05Z6d_U2XU"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <br/><br/>
                Neste vídeo eu mostro como é possível deixar cadastrados todos os serviços adicionais que serão vendidos
                com os pacotes da agência, esses serviços podem ser vendidos opcionalmente e já serão somados no voucher.
            </div>



            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/GfW2Cv5rBkY" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como realizar o vinculo de um serviço adicional ao cadastro de um pacote ou serviço turístico
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/GfW2Cv5rBkY"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Vamos aprender neste vídeo como realizar o vinculo de um serviço adicional previamente cadastrado com um
                pacote ou serviço da agência, essa opção inclui na hora da venda as opções de serviços adicionais para o
                cliente adicionar ao carrinho de compra.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/6RWXs6PGtZY" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Veja aqui, como configurar o valor dos serviços adicionais pela idade dos passageiros
                        serviço.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/6RWXs6PGtZY"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Veja como vincular um serviço adicional e configurar os valores do serviço pela idade dos passageiros.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/E43tq5LshzI" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como configurar o fornecedor e a comissão que a agência receberá na venda de serviços adicionais do receptivo
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/E43tq5LshzI"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Veja neste vídeo como configurar a comissão da agência para venda de serviços adicionais e receptivos
                e como definir o fornecedor.
                Ao realizar essa configuração,
                na venda o sistema já gera uma conta a pagar em nome do fornecedor e desconta a comissão da agência.
            </div>

        </div>
    </div>
</div>



<div class="box">
    <div class="box-header"   style="cursor: pointer;"onclick="ocultarExibir('modulo-agenda');">
        <h2 class="blue">
            <i class="fa fa-calendar"></i>
            Dúvidas do Módulo de Agenda / Programação de Embarque
        </h2>
    </div>
    <div class="box-content" id="modulo-agenda" style="display: none;">
        <div class="row">


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/BWWwEh8HZNs" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como Programar (Abrir Agenda) de Embarque dos Pacotes, Serviços e Passeios da Agência para Comercialização.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/BWWwEh8HZNs"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Neste vídeo eu mostro como podemos agendar as datas programadas dos passeios,
                pacotes ou serviços da agência que serão disponibilizadas na hora da venda.
                Com isso tendo total flexibilidade em gerenciar as datas dos embarques, ou abrir novas agendas quando houver necessidade.
            </div>



            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/FGrDiNOAqWk" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como Alterar a Data ou o número de Vagas de uma Programação de Embarque dos Pacotes, Serviços e Passeios.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/FGrDiNOAqWk"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Mostramos aqui como alterar o número de vagas ou a data de uma programação.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/lUxj7CvRuXo" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como Excluir Programação de Embarque dos Pacotes, Serviços e Passeios da Agência retirando da Venda.
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/lUxj7CvRuXo"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Veja neste vídeo como excluir a programação de um pacote, passeio ou serviço tirando da venda a opção.
            </div>


        </div>
    </div>
</div>





<div class="box">
    <div class="box-header"  style="cursor: pointer;" onclick="ocultarExibir('modulo-vendas');">
        <h2 class="blue">
            <i class="fa fa-ticket"></i>
            Dúvidas do Módulo de Vendas
        </h2>
    </div>
    <div class="box-content" id="modulo-vendas" style="display: none;">
        <div class="row">

            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/jJFwCSR5MAA" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como lançar uma COTAÇÃO
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/jJFwCSR5MAA"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Veja aqui como realizar o lançamento de uma cotação de venda.
            </div>




            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/oECAuXcPR0k" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como lançar uma Venda e já FATURAR
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/oECAuXcPR0k"
                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>

                </iframe>
                <br/><br/>
                Aqui nós mostramos como lançar uma venda e já faturar o recebimento, lançando uma conta a receber.
            </div>

            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/ob2J2_S1xR8" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Como realizar/adicionar pagamentos de parcelas diretamente pelo módulo de Vendas
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/ob2J2_S1xR8"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen>
                </iframe>
                <br/><br/>
                Aprenda aqui como realizar o pagamento das parcelas dos clientes diretamente pelo módulo de vendas, sem precisar acessar o
                conta a receber.
            </div>


            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/9_280_ew8Ug" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Aprenda como gerar boletos bancários JUNO/PAGSEGURO<br/>
                        <span>Esta opção é util apenas para clientes que usam JUNO/PAGSEGURO como emissor de boletos.</span>
                    </h3>
                </a>
                <br/>
                <iframe
                        width="400"
                        height="300"
                        src="https://www.youtube.com/embed/9_280_ew8Ug"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                <br/><br/>
                Aprenda como gerar boletos bancários pelo módulo de vendas e enviar o boleto para o cliente.<br/>
                <span style="color:#F45750;">Se você tem interesse em utilizar JUNO como emissor de boletos bancários / cartão de crédito entre em contato conosco
                    para lhe orientar a criar/configurar a conta.</span>
            </div>

        </div>
    </div>
</div>




<div class="box">
    <div class="box-header"  style="cursor: pointer;" onclick="ocultarExibir('modulo-financeiro');">
        <h2 class="blue">
            <i class="fa fa-users"></i>
            Dúvidas do Módulo financeiro
        </h2>
    </div>
    <div class="box-content" id="modulo-financeiro" style="display: none;">
        <div class="row">
            Estamos criando 😅
        </div>
    </div>
</div>


<div class="box">
    <div class="box-header"  style="cursor: pointer;" onclick="ocultarExibir('modulo-clientes');">
        <h2 class="blue">
            <i class="fa fa-users"></i>
            Dúvidas do Módulo de Cadastro de Clientes
        </h2>
    </div>
    <div class="box-content" id="modulo-clientes" style="display: none;">
        <div class="row">

            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/lUxj7CvRuXo" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                       Como cadastrar um novo Passageiro
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/_HEoQthvOKY"
                        frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Cadastro de novos clientes no sistema, clientes cadastrados aqui poderão aparecer na venda.
            </div>



            <div class="col-lg-6" style="text-align: center;">
                <a href="https://www.youtube.com/embed/lUxj7CvRuXo" target="_blank">
                    <h3 style="font-size: 18px;color: #e58800;">
                        Aprenda como consultar e editar as informações de passageiros já cadastrados
                    </h3>
                </a>
                <br/>
                <iframe width="400"
                        height="300"
                        src="https://www.youtube.com/embed/Ur_29mR__e4"
                        frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
                </iframe>
                <br/><br/>
                Editar o cadastros de passageiros.
            </div>

        </div>
    </div>
</div>



<script>
    function ocultarExibir(tipo) {
        if ($('#'+tipo).is(':visible')) {
            $('#'+tipo).hide(800);
        } else {
            $('#'+tipo).show(800)
        }
    }
</script>


