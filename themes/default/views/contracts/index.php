<style type="text/css" media="screen">
    #ContractTable td:nth-child(1) {display: none;}
    #ContractTable td:nth-child(2) {width: 90%}
    #ContractTable td:nth-child(3) {width: 5%;text-align: center;}
</style>

<script>

    function row_active(x) {
        if(x === '1') {
            return '<div class="text-center"><span class="label label-success">Ativo</span></div>';
        }
        return '<div class="text-center"><span class="label label-danger">Inativo</span></div>';
    }

    $(document).ready(function () {
        $('#ContractTable').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('contracts/getContracts') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [null, null, {"bSortable": false, "mRender": row_active}, {"bSortable": false}]
        });
    });
</script>
<?= form_open('produtcs/expense_contracts_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file-text-o"></i><?= lang('contracts'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('contracts/addContract'); ?>">
                                <i class="fa fa-plus"></i> <?= lang('add_contract') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo site_url('contracts/tags_contrato'); ?>" target="_blank">
                                <i class="fa fa-tags"></i> <?= lang('tags_contrato') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="ContractTable" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkth" type="checkbox" name="check"/></th>
                                <th style="text-align: left;"><?= $this->lang->line("name"); ?></th>
                                <th style="text-align: center;"><?= $this->lang->line("active"); ?></th>
                                <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script language="javascript">
    $(document).ready(function () {});
</script>

