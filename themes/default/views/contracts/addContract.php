<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file-text-o"></i><?= lang('add_contract'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="<?= site_url('contracts/tags_contrato') ?>" class="toggle_up" target="_blank">
                        <i class="icon fa fa-tags"></i><span class="padding-right-10"><?= lang('tags_contrato'); ?></span>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
            <?php echo form_open_multipart("contracts/addContract", array('data-toggle' => 'validator', 'role' => 'form')); ?>
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group all">
                        <?= lang("name", "name") ?>
                        <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ''), 'class="form-control" id="name" required="required"'); ?>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group all">
                        <?= lang("type", "type") ?>
                        <?php
                        $opts = array(
                            'text' => lang('text'),
                            'document' => lang('document')
                        );
                        echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : 'text'), 'class="form-control" id="type" required="required"');
                        ?>
                    </div>
                </div>

                <div class="col-md-12" id="div_contract">
                    <div class="form-group all">
                        <?= lang("contract", "contract"); ?>
                        <?=form_textarea('contract', (isset($_POST['contract']) ? $_POST['contract'] : ''), 'id="contract" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('contract') . '"');?>
                    </div>
                </div>

                <div class="col-md-12" id="div_file" style="display: none;">
                    <div class="form-group all">
                        <?= lang("document", "file") ?>
                        <input id="file" type="file" data-browse-label="<?= lang('browse'); ?>" name="file" data-show-upload="false"
                               data-show-preview="false" accept="application/pdf, application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, text/rtf" class="form-control file">
                    </div>
                </div>

                <div class="col-md-12" id="div_usar_clausulas_site" style="display: none;">
                    <div class="form-group all">
                        <?php echo form_checkbox('usar_clausulas_site', '1', (isset($_POST['usar_clausulas_site']) ? $_POST['usar_clausulas_site'] : ''), 'id="usar_clausulas_site"'); ?>
                        <label for="attributes" class="padding05"><?= lang('usar_clausulas_site'); ?></label>
                    </div>
                </div>

                <div class="col-md-12" id="div_clauses" style="display:none;">
                    <div class="form-group all">
                        <?= lang("clauses", "clauses"); ?>
                        <?=form_textarea('clauses', (isset($_POST['clauses']) ? $_POST['clauses'] : ''), 'id="clauses" class="form-control kb-text skip" style="height: 100px;" placeholder="' . lang('clauses') . '"');?>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('add_contract', lang("add_contract"), 'class="btn btn-primary"'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        setTimeout(function (){
            create_summernote_contract();
            create_summernote_clauses();
        }, 1000);

        $('#type').change(function (event) {

            if ($(this).val() === 'document') {
                $('#div_contract').hide();

                $('#div_file').show();
                $('#div_usar_clausulas_site').show();
            } else {
                $('#div_contract').show();

                $('#div_file').hide();
                $('#div_usar_clausulas_site').hide();
            }
        });

        $('#usar_clausulas_site').on('ifChecked', function (e) {
            $('#div_clauses').show();
        });

        $('#usar_clausulas_site').on('ifUnchecked', function (e) {
            $('#div_clauses').hide();
        });
    });

    function create_summernote_contract() {
        $('#contract').summernote(
            {
                height: 800,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai',
                    lineNumbers: true,
                    mode: 'htmlmixed',       // Suporte para HTML e CSS juntos
                },
                toolbar: [
                    ['style', ['style']],              // Estilos de texto (negrito, itálico, sublinhado, etc.)
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                    ['fontname', ['fontname']],        // Escolha de fonte
                    ['fontsize', ['fontsize']],        // Escolha de tamanho da fonte
                    ['color', ['color']],              // Escolha de cores para texto e fundo
                    ['para', ['ul', 'ol', 'paragraph']], // Listas e alinhamentos
                    ['height', ['height']],            // Altura da linha
                    ['table', ['table']],              // Tabelas
                    ['insert', ['link', 'picture', 'video', 'hr']], // Inserir links, imagens, vídeos e linha horizontal
                    ['view', ['fullscreen', 'codeview', 'help']], // Opções de visualização e ajuda
                ],

                air: [
                    ['color', ['color']],
                    ['font', ['bold', 'underline', 'clear']]
                ],

                hint: {                      // Sugestões de tags e emojis
                    mentions: ['@cliente', '@contrato', '@data'],  // Tags customizadas
                    match: /@(\w*)$/,         // Regex para reconhecer tags
                    search: function (keyword, callback) {
                        callback($.grep(this.mentions, function (item) {
                            return item.indexOf(keyword) === 0;
                        }));
                    },
                    content: function (item) {
                        return $('<span>').text(item).html();
                    }
                },

                // Plugins externos
                plugins: {
                    'emoji': true,             // Plugin de emoji
                    'table': true,             // Plugin de tabela avançado
                    'math': true               // Plugin de equações matemáticas (se necessário)
                }

            }
        );
    }

    function create_summernote_clauses() {
        $('#clauses').summernote(
            {
                height: 800,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'monokai',
                    lineNumbers: true,
                    mode: 'htmlmixed',       // Suporte para HTML e CSS juntos
                },
                toolbar: [
                    ['style', ['style']],              // Estilos de texto (negrito, itálico, sublinhado, etc.)
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                    ['fontname', ['fontname']],        // Escolha de fonte
                    ['fontsize', ['fontsize']],        // Escolha de tamanho da fonte
                    ['color', ['color']],              // Escolha de cores para texto e fundo
                    ['para', ['ul', 'ol', 'paragraph']], // Listas e alinhamentos
                    ['height', ['height']],            // Altura da linha
                    ['table', ['table']],              // Tabelas
                    ['insert', ['link', 'picture', 'video', 'hr']], // Inserir links, imagens, vídeos e linha horizontal
                    ['view', ['fullscreen', 'codeview', 'help']], // Opções de visualização e ajuda
                ],

                air: [
                    ['color', ['color']],
                    ['font', ['bold', 'underline', 'clear']]
                ],

                hint: {                      // Sugestões de tags e emojis
                    mentions: ['@cliente', '@contrato', '@data'],  // Tags customizadas
                    match: /@(\w*)$/,         // Regex para reconhecer tags
                    search: function (keyword, callback) {
                        callback($.grep(this.mentions, function (item) {
                            return item.indexOf(keyword) === 0;
                        }));
                    },
                    content: function (item) {
                        return $('<span>').text(item).html();
                    }
                },

                // Plugins externos
                plugins: {
                    'emoji': true,             // Plugin de emoji
                    'table': true,             // Plugin de tabela avançado
                    'math': true               // Plugin de equações matemáticas (se necessário)
                }

            }
        );
    }

</script>