

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-file-text-o"></i><?= lang('tags_contrato'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <h2 style="text-align:center;">Tabela de TAG's do Contrato</h2>
                <table class="table table-bordered table-hover table-striped table-condensed dataTable">
                    <thead>
                    <tr>
                        <th>TAG</th>
                        <th>Descrição</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($constantes as $constante): ?>
                        <tr>
                            <td><?= htmlspecialchars($constante['valor']); ?></td>
                            <td><?= htmlspecialchars($constante['descricao'] ?? ''); ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>



