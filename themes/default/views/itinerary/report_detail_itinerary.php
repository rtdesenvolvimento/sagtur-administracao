<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>

        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        p {
            line-height: 25px;
        }

        b{
            font-weight: bold;
        }
    </style>
<body>

<div>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: center;width: 30%;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
            </td>
            <td style="text-align: left;width: 71%;">
                <h1><?=$Settings->site_name;?></h1>
                <h4 style="font-size: 12px;">
                    <?php
                    echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state;
                    echo '<p>';
                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != '-' && $biller->cf1 != '') {
                        echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                    }
                    if ($biller->cf2 != '-' && $biller->cf2 != '') {
                        echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                    }
                    if ($biller->cf3 != '-' && $biller->cf3 != '') {
                        echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                    }
                    if ($biller->cf4 != '-' && $biller->cf4 != '') {
                        echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                    }
                    if ($biller->cf5 != '-' && $biller->cf5 != '') {
                        echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                    }
                    if ($biller->cf6 != '-' && $biller->cf6 != '') {
                        echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                    }
                    echo '</p>';
                    echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                    ?>
                </h4>
            </td>
        </tr>
        </thead>
    </table>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                OS Nº: <b><?=$itinerary->reference_no;?></b>
            </td>
            <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                DATA OS: <b><?=$this->sma->hrsd($itinerary->dataEmbarque);?>
                    <?php if ($itinerary->horaEmbarque != null && $itinerary->horaEmbarque != '00:00:00.0' && $itinerary->horaEmbarque != '00:00:00') { ?>
                        <?=$itinerary->horaEmbarque;?>
                    <?php } ?>
                </b>
            </td>
            <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                TIPO: <b><?=$itinerary->tipo_trajeto;?></b>
            </td>
            <?php if ($itinerary->retornoPrevisto && $itinerary->retornoPrevisto != '0000-00-00 00:00:00'){?>
                <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                    Retorno Previsto: <b><?=$this->sma->hrld($itinerary->retornoPrevisto);?></b>
                </td>
            <?php } ?>
        </tr>
        </thead>
    </table>
    <br/>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <tbody>
        <?php if ($itinerary->product_id) {?>
            <tr>
                <td style="text-align: right;padding: 2px;">
                    SERVIÇO:
                </td>
                <td>
                    <b><?=$itinerary->servico;?></b>
                </td>
            </tr>
        <?php } ?>

        <?php if ($itinerary->motorista) {?>
            <tr>
                <td style="text-align: right;">
                    MOTORISTA:
                </td>
                <td>
                    <b><?=$itinerary->motorista;?></b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->motorista) {?>
            <tr>
                <td style="text-align: right;">
                    GUIA:
                </td>
                <td>
                    <b><?=$itinerary->guia;?></b>
                </td>
            </tr>
        <?php } ?>

        <?php if ($itinerary->tipo_transporte) {?>
            <tr>
                <td style="text-align: right;">
                    VEÍCULO:
                </td>
                <td>
                    <b><?=$itinerary->tipo_transporte;?></b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->executivo) {?>
            <tr>
                <td style="text-align: right;">
                    Executivo:
                </td>
                <td>
                    <b>SIM</b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->note){?>
            <tr>
                <td style="text-align: right;">
                    OBS:
                </td>
                <td>
                    <b><?=$itinerary->note;?></b>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <br/>
    <?php
    $total_passageiros_os = 0;
    foreach ($itens as $item){

        $localEmbarque  = $item->localEmbarque;
        $sale_itens     = $this->itinerary_model->getAllInvoiceItems($item->sale_id, $item->product_id, $item->localEmbarqueId);

        $endereco = '';

        if ($item->endereco) {
            $endereco .= $item->endereco;
        }

        if ($item->numero) {
            $endereco .= ' '.$item->numero;
        }

        if ($item->complemento) {
            $endereco .= ' '.$item->complemento;
        }

        if ($item->bairro) {
            $endereco .= ' '.$item->bairro;
        }

        if ($item->cidade) {
            $endereco .= ' '.$item->cidade;
        }

        if ($item->cidade) {
            $endereco .= '/'.$item->estado;
        }

        if ($item->cep) {
            $endereco .= ' CEP '.$item->cep;
        }

        ?>
        <table border="" style="width: 100%;border-collapse:collapse;">
            <thead>
            <tr>
                <td style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: left;background: #f7f7f8;">
                    <b>DATA SERVIÇO: <?=$this->sma->hrsd($itinerary->dataEmbarque);?> - <?=$item->product_name;?></b>
                </td>
            </tr>
            </thead>
        </table>
        <table border="" style="width: 100%;border-collapse:collapse;">
            <?php if ($localEmbarque) {?>
                <tr>
                    <td style="text-align: right;">
                        Embarque:
                    </td>
                    <td style="color: blue;background: #d3d3d3;padding: 5px;">
                        <b><?=$localEmbarque;?></b>
                    </td>
                </tr>
            <?php } ?>
            <?php if ($endereco) {?>
                <tr>
                    <td  colspan="1"></td>
                    <td colspan="1" style="border-bottom: 1px solid #0b0b0b;font-size: 10px;">
                        <b><?=$endereco;?></b>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <br/>
        <table border="" style="width: 100%;border-collapse:collapse;">
            <tr>
                <td style="text-align: right;">
                    <div style="padding: 10px;">
                        Venda: <b><?=$item->reference_no;?></b>
                    </div>
                </td>
                <td style="text-align: right;">Titular:</td>
                <td style="text-align: left;text-decoration: underline;"><b><?=$item->customer;?></b></td>
                <td style="text-align: right;">Pick-up:</td>
                <td style="background: #999999;text-align: center;font-size: 18px;"><b><?=date('H:i', strtotime($item->hora_embarque));?></b></td>
                <!--
                <td style="text-align: right;">Hr Ret:</td>
                <td style="background: #999999;text-align: center;font-size: 18px;"><b>xx:xx</b></td>
                !-->
            </tr>
        </table>

        <table border="" style="width: 100%;border-collapse:collapse;font-size: 10px;margin-left: 23%;">
            <thead>
            <tr style="background: #f7f7f8;font-size: 12px;">
                <th>Nome</th>
                <th>Idioma.</th>
                <th>Documentos.</th>
                <th>Dt. Nasc</th>
                <th style="text-align: right;">Qtd</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $total_item = 0;
            foreach ($sale_itens as $sl_item){
                $customer = $this->site->getCompanyByID($sl_item->customerClient);

                ?>
                <tr>
                    <td style="border-bottom: 1px dashed #000;font-size: 10px;padding: 5px;"><?=$customer->name.' ('.$sl_item->faixaNome.')';?></td>
                    <td style="border-bottom: 1px dashed #000;font-size: 10px;padding: 5px;">
                        <?=lang($customer->idioma);?>
                    </td>
                    <td style="border-bottom: 1px dashed #000;font-size: 10px;padding: 5px;"><?=strtoupper(lang($customer->tipo_documento));?> <?=$customer->cf1;?></td>
                    <td style="border-bottom: 1px dashed #000;font-size: 10px;padding: 5px;">
                        <?php if ($customer->data_aniversario) {?>
                            <?=$this->sma->hrsd($customer->data_aniversario);?>
                        <?php }?>
                    </td>
                    <td style="border-bottom: 1px dashed #000;font-size: 10px;padding: 5px;text-align: right;">
                        <?=$sl_item->quantity;?>
                    </td>
                </tr>
            <?php $total_item = $total_item + $sl_item->quantity;} ?>
            </tbody>
            <tbody>
            <tr>
                <td colspan="4"><br/></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td colspan="2" style="background: #0b0b0b;color: #FFFFFF;text-align: center">
                    PAX: <?= str_pad($total_item, 2, '0', STR_PAD_LEFT); ?>
                </td>
            </tr>
            </tbody>
        </table>
        <br/>
    <?php $total_passageiros_os = $total_passageiros_os + $total_item;}?>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="padding: 10px;text-align: center;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;">
                <b>SOLICITAR O RG FISICO NA HORA DO EMBARQUE</b>
            </td>
            <td style="padding: 10px;text-align: right;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;">
                <b>Total de PAX da OS: <?= str_pad($total_passageiros_os, 2, '0', STR_PAD_LEFT); ?></b>
            </td>
        </tr>
        </thead>
    </table>
    <br/><br/><br/>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <tr>
            <td style="width: 50%;"></td>
            <td style="width: 25%;"></td>
            <td style="width: 25%;"></td>
        </tr>
        <tr>
            <td style="text-align: center;">
                ________________________________
                <br/>Assinatura do Motorista
            </td>
            <td>
                Hora Saída: ______________________
            </td>
            <td>
                Chegada: ______________________
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br/>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                ________________________________
                <br/>Conferido
            </td>
            <td>
                Km Início: ______________________
            </td>
            <td>
                Km Final: ______________________
            </td>
        </tr>
    </table>

    <br/>
</div>

</body>
</html>