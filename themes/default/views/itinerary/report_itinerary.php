<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        body {
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
            font-size: 11px;
        }

        td {
            padding: 1px;
        }

        th {
            padding: 5px;
            border-bottom: 1px solid #0b0b0b;
        }

        p {
            line-height: 25px;
        }

        b{
            font-weight: bold;
        }
    </style>
<body>

<div>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="text-align: center;width: 30%;">
                <?php  echo '<img src="' . base_url('assets/uploads/logos/' . $Settings->logo2) . '" alt="' . $Settings->site_name . '"  style="margin-bottom:10px;" />';?>
            </td>
            <td style="text-align: left;width: 71%;">
                <h1><?=$Settings->site_name;?></h1>
                <h4 style="font-size: 12px;">
                    <?php
                    echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state;
                    echo '<p>';
                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != '-' && $biller->cf1 != '') {
                        echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                    }
                    if ($biller->cf2 != '-' && $biller->cf2 != '') {
                        echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                    }
                    if ($biller->cf3 != '-' && $biller->cf3 != '') {
                        echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                    }
                    if ($biller->cf4 != '-' && $biller->cf4 != '') {
                        echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                    }
                    if ($biller->cf5 != '-' && $biller->cf5 != '') {
                        echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                    }
                    if ($biller->cf6 != '-' && $biller->cf6 != '') {
                        echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                    }
                    echo '</p>';
                    echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                    ?>
                </h4>
            </td>
        </tr>
        </thead>
    </table>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <td style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                OS Nº: <b><?=$itinerary->reference_no;?></b>
            </td>
            <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                DATA OS: <b><?=$this->sma->hrsd($itinerary->dataEmbarque);?>
                    <?php if ($itinerary->horaEmbarque != null && $itinerary->horaEmbarque != '00:00:00.0' && $itinerary->horaEmbarque != '00:00:00') { ?>
                        <?=$itinerary->horaEmbarque;?>
                    <?php } ?>
                </b>
            </td>
            <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                TIPO: <b><?=$itinerary->tipo_trajeto;?></b>
            </td>
            <?php if ($itinerary->retornoPrevisto && $itinerary->retornoPrevisto != '0000-00-00 00:00:00'){?>
                <td  style="padding: 5px;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;text-align: center;background: #f7f7f8;">
                    Retorno Previsto: <b><?=$this->sma->hrld($itinerary->retornoPrevisto);?></b>
                </td>
            <?php } ?>
        </tr>
        </thead>
    </table>
    <br/>
    <table border="" style="width: 100%;border-collapse:collapse;">
        <tbody>

        <?php if ($itinerary->product_id) {?>
            <tr>
                <td style="text-align: right;padding: 2px;">
                    SERVIÇO:
                </td>
                <td>
                    <b><?=$itinerary->servico;?></b>
                </td>
            </tr>
        <?php } ?>

        <?php if ($itinerary->motorista) {?>
            <tr>
                <td style="text-align: right;">
                    MOTORISTA:
                </td>
                <td>
                    <b><?=$itinerary->motorista;?></b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->motorista) {?>
            <tr>
                <td style="text-align: right;">
                    GUIA:
                </td>
                <td>
                    <b><?=$itinerary->guia;?></b>
                </td>
            </tr>
        <?php } ?>

        <?php if ($itinerary->tipo_transporte) {?>
            <tr>
                <td style="text-align: right;">
                    VEÍCULO:
                </td>
                <td>
                    <b><?=$itinerary->tipo_transporte;?></b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->executivo) {?>
            <tr>
                <td style="text-align: right;">
                    Executivo:
                </td>
                <td>
                    <b>SIM</b>
                </td>
            </tr>
        <?php } ?>
        <?php if ($itinerary->note){?>
            <tr>
                <td style="text-align: right;">
                    OBS:
                </td>
                <td>
                    <b><?=$itinerary->note;?></b>
                </td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    <br/>

    <table border="0" style="width: 100%;border-collapse:collapse;">
        <thead>
        <tr>
            <th style="width: 10%;text-align: center;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Horário</th>
            <th style="width: 25%;text-align: left;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Pick-up</th>
            <th style="width: 30%;text-align: left;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Atividade</th>
            <th style="width: 30%;text-align: left;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Cliente</th>
            <td style="width: 5%;text-align: right;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Qtd</td>

            <?php if ($Settings->show_payment_report_shipment) {?>
                <th style="width: 10%;text-align:right;border-top: 1px solid #0b0b0b;border-bottom: 1px solid #0b0b0b;background: #f7f7f8;">Cobrar</th>
            <?php } ?>
        </tr>
        </thead>
        <tbody>
        <?php
        $faixas             = [];
        $strCobrar          = 0;
        $valorItem          = 0;
        $valorTotalItem     = 0;
        $valorTotalCobrar   = 0;
        $totalAcrescimo     = 0;
        $totalDesconto      = 0;
        $total_passageiros  = 0;

        foreach ($itens as $item) {

            $totalPagarItem = $item->subtotal;
            $totalPago      = $item->paid;
            $acrescimo      = $item->shipping;
            $desconto       = $item->order_discount;
            $totalVenda     = $item->grand_total - $acrescimo + $desconto;
            $total_passageiros = $total_passageiros + $item->quantity;

            $totalAcrescimo += $item->shipping;
            $totalDesconto += $item->order_discount;

            //calculo
            $totalRealPagoPorDependente = $totalPagarItem * (($item->paid*100/$totalVenda)/100);
            $totalAberto                = $totalPagarItem - $totalRealPagoPorDependente;

            $valorTotalItem += $totalPagarItem;
            $valorTotalCobrar += $totalAberto;

            if ($totalAberto > 0) {
                $strCobrar      = $this->sma->formatMoney($totalAberto);
            } else {
                $strCobrar      = 'PAGO';
                $totalAberto    = 0;
            }

            $customer   = $this->site->getCompanyByID($item->customerClient);
            $phone      = $customer->phone;
            $whatsApp   = $customer->cf5;
            $email      = $customer->email;
            $telefoneEmergencia  = $customer->telefone_emergencia;
            $contato = '';

            if ($phone)  {
                $contato = $phone;
            }

            if ($whatsApp) {
                $contato  .= ' '.$whatsApp;
            }

            if ($telefoneEmergencia) {
                $contato .= '<br/>Emergência '.$telefoneEmergencia;
            }

            if ($email) {
                $contato .= '<br/>'.$email;
            }

            $localEmbarque = $item->localEmbarque;
            $endereco = '';

            if ($item->endereco) {
                $endereco .= $item->endereco;
            }

            if ($item->numero) {
                $endereco .= ' '.$item->numero;
            }

            if ($item->complemento) {
                $endereco .= ' '.$item->complemento;
            }

            if ($item->bairro) {
                $endereco .= ' '.$item->bairro;
            }

            if ($item->cidade) {
                $endereco .= '<br/>'.$item->cidade;
            }

            if ($item->cidade) {
                $endereco .= '/'.$item->estado;
            }

            if ($item->cep) {
                $endereco .= '<br/>CEP '.$item->cep;
            }

            if ($isColorBackground) {
                $background = "#f5f5f5";
                $isColorBackground = false;
            } else {
                $background = "#ffffff";
                $isColorBackground = true;
            }

            if ($faixas[$item->faixaNome]->qty == null) {
                $faixas[$item->faixaNome]->qty = 1;
                $faixas[$item->faixaNome]->name = $item->faixaNome;
            } else {
                $faixas[$item->faixaNome]->qty = $faixas[$item->faixaNome]->qty + 1;
            }
            ?>
            <tr>
                <td style="background: <?=$background?>;text-align: center;"><?=date('H:i', strtotime($item->hora_embarque));?></td>
                <td style="background: <?=$background?>">
                    <b><?=$localEmbarque;?></b>
                    <?php if ($endereco) {?>
                        <br/><small><?=$endereco;?></small>
                    <?php } ?>
                </td>
                <td style="background: <?=$background?>;vertical-align: top;"><?=$item->product_name;?></td>
                <td style="background: <?=$background?>;vertical-align: top;"><span style="font-weight: bold;"><?=$customer->name.' ('.$item->faixaNome.')</span><br/>'.$contato;?></td>
                <td style="background: <?=$background?>;vertical-align: top;text-align: right;"><?= str_pad( (int) $item->quantity, 2, '0', STR_PAD_LEFT); ?></td>

                <?php if ($Settings->show_payment_report_shipment) {?>
                    <td style="background: <?=$background?>;text-align:right;vertical-align: top;"><?=$strCobrar;?></td>
                <?php } ?>

            </tr>
        <?php }?>

        </tbody>
        <tfoot>
        <?php if ($Settings->show_payment_report_shipment) {?>
            <tr>
                <td colspan="3" style="text-align: right;border-top: 1px solid #0b0b0b;">
                    <span style="font-weight: bold;">Valor Total:</span>
                </td>
                <td style="text-align: right;border-top: 1px solid #0b0b0b;" colspan="2">
                    <?=$this->sma->formatMoney($valorTotalItem);?>
                </td>
                <td style="text-align: right;border-top: 1px solid #0b0b0b;" colspan="1">
                    <?=$this->sma->formatMoney($valorTotalCobrar);?>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right;">
                    <span style="font-weight: bold;">Total Acrescimo:</span>
                </td>
                <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalAcrescimo);?></small></td>
                <td style="text-align: right;" colspan="1"><small><?=$this->sma->formatMoney($totalAcrescimo);?></small></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right;">
                    <span style="font-weight: bold;">Total Desconto:</span>
                </td>
                <td style="text-align: right;" colspan="2"><small><?=$this->sma->formatMoney($totalDesconto);?></small></td>
                <td style="text-align: right;" colspan="1"><small><?=$this->sma->formatMoney($totalDesconto);?></small></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: right;">
                    <span style="font-weight: bold;">Subtotal:</span>
                </td>
                <td style="text-align: right;" colspan="2"><span style="font-weight: bold;"><?=$this->sma->formatMoney($valorTotalItem+$totalAcrescimo-$totalDesconto);?></span></td>
                <td style="text-align: right;" colspan="1"><span style="font-weight: bold;"><?=$this->sma->formatMoney($valorTotalCobrar+$totalAcrescimo-$totalDesconto);?></span></td>
            </tr>
            <tr style="border-top: 1px solid #0b0b0b;">
                <td colspan="6" style="text-align: right;border-top: 1px solid #0b0b0b;"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td colspan="3" style="background: #0b0b0b;color: #FFFFFF;text-align: right;padding: 5px;">
                    <span style="font-weight: bold;">TOTAL PAX: <?= str_pad($total_passageiros, 2, '0', STR_PAD_LEFT); ?></span>
                </td>
            </tr>
        <?php } else { ?>
            <tr style="border-top: 1px solid #0b0b0b;">
                <td colspan="5" style="text-align: right;border-top: 1px solid #0b0b0b;"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <td colspan="2" style="background: #0b0b0b;color: #FFFFFF;text-align: right;padding: 5px;">
                    <span style="font-weight: bold;">TOTAL PAX: <?= str_pad($total_passageiros, 2, '0', STR_PAD_LEFT); ?></span>
                </td>
            </tr>
        <?php } ?>
        </tfoot>
    </table>

</div>
</body>
</html>