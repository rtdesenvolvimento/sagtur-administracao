<style media="screen">

    #TUData td:nth-child(1) {display: none;}
    #TUData td:nth-child(2) {text-align: center;}
    #TUData td:nth-child(6) {text-align: right;}
    #TUData td:nth-child(7) {text-align: right;}
    #TUData td:nth-child(8) {text-align: right;}
    #TUData td:nth-child(10) {display: none;}
    #TUData td:nth-child(11) {text-align: center;width: 5%;}


    #TIData td:nth-child(1) {display: none;}
    #TIData td:nth-child(2) {text-align: center;}
    #TIData td:nth-child(6) {display: none;}
    #TIData td:nth-child(7) {text-align: center;width: 2%;}


</style>

<script>
    function checkboxTours(x) {
        return '<div class="text-center"><input class="checkbox" type="checkbox" name="val[]" value="' + x + '" /></div>';
    }

    $(document).ready(function () {

        var oTable2 = $('#TUData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('itinerary/getTours')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[9];
                nRow.className = "invoice_link_itinerary";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterProduct", "value":  $('#tour').val() });
                aoData.push({ "name": "filterDateTour", "value":  $('#dataTour').val() });
                aoData.push({ "name": "filterHourTour", "value":  $('#hourTour').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                null,
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                    paid += parseFloat(aaData[aiDisplay[i]][6]);
                    balance += parseFloat(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[5].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[6].innerHTML = currencyFormat(parseFloat(paid));
                nCells[7].innerHTML = currencyFormat(parseFloat(balance));
            },
        }).fnSetFilteringDelay().dtFilter([], "footer");

        var oTable3 = $('#TIData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('itinerary/getItens/'.$itinerary->id)?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[5];
                nRow.className = "invoice_link_itinerary";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterProduct", "value":  $('#tour').val() });
                aoData.push({ "name": "filterDateTour", "value":  $('#dataTour').val() });
                aoData.push({ "name": "filterHourTour", "value":  $('#hourTour').val() });
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                null,
                null,
                null,
                null,
                null,
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {},
        }).fnSetFilteringDelay().dtFilter([], "footer");

    });
</script>

<?php echo form_open("itinerary/edit/".$itinerary->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>
    <ul id="myTab" class="nav nav-tabs" style="text-align: center">
        <li class=""><a href="#abageral" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('itinerary_details') ?></a></li>
        <li class="" id="tbFinanceiro"><a href="#tours_itinerary" class="tab-grey"><i class="fa fa-ticket" style="font-size: 20px;"></i><br/><?= lang('tours_itinerary') ?></a></li>
    </ul>
    <div class="tab-content">
        <div id="abageral" class="tab-pane fade in">
            <div class="box">
                <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-cogs"></i><?= lang('set_up_itinerary'); ?></h2></div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("reference_no", "reference_no"); ?>
                                <?php echo form_input('reference_no', $itinerary->reference_no, 'class="form-control tip" id="dataEmbarque" required="required" disabled'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("data_embarque", "dataEmbarque"); ?>
                                <?php echo form_input('dataEmbarque', $itinerary->dataEmbarque, 'class="form-control tip" id="dataEmbarque" required="required" ', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("hora_embarque", "horaEmbarque"); ?>
                                <?php echo form_input('horaEmbarque', $itinerary->horaEmbarque, 'class="form-control tip" id="horaEmbarque" required="required" ', 'time'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("fornecedor", "fornecedor"); ?>
                                <?php echo form_input('fornecedor', $itinerary->fornecedor, 'id="fornecedor" data-placeholder="' . lang("select") . ' ' . lang("fornecedor") . '" class="form-control input-tip" required="required"  style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("motorista", "motorista"); ?>
                                <?php echo form_input('motorista', $itinerary->motorista, 'id="motorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("guia", "guia"); ?>
                                <?php echo form_input('guia', $itinerary->guia, 'id="guia" data-placeholder="' . lang("select") . ' ' . lang("guia") . '"  class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("tipo_transporte", "tipo_transporte_id"); ?>
                                <?php echo form_input('tipo_transporte_id', $itinerary->tipo_transporte_id, 'id="tipo_transporte_id" data-placeholder="' . lang("select") . ' ' . lang("tipo_transporte") . '" class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <?= lang("product", "product") ?>
                                <?php
                                $cbProducts[''] =  lang("select") . " " . lang("product") ;
                                foreach ($products as $produts) {
                                    $cbProducts[$produts->id] = $produts->name;
                                }
                                echo form_dropdown('product_id', $cbProducts, $itinerary->product_id, 'class="form-control select" id="product_id" placeholder="' . lang("select") . " " . lang("product") . '"style="width:100%"')
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <?= lang("executivo", "executivo"); ?>
                                <?php
                                $opts = array(1 => lang('yes'), 0 => lang('no'));
                                echo form_dropdown('executivo', $opts, $itinerary->executivo, 'id="executivo" class="form-control select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("tipo_trajeto", "tipo_trajeto"); ?>
                                <?php
                                $cbTipoTrajeto[""] = lang("select") . ' ' . lang("tipo_trajeto") ;
                                foreach ($tiposTrajeto as $tipoTrajeto) {
                                    $cbTipoTrajeto[$tipoTrajeto->id] = $tipoTrajeto->name;
                                }
                                echo form_dropdown('tipo_trajeto_id', $cbTipoTrajeto, $itinerary->tipo_trajeto_id, 'id="tipo_trajeto_id" data-placeholder="' . lang("select") . ' ' . lang("tipo_trajeto") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("retorno_previsto", "retornoPrevisto"); ?>
                                <?php echo form_input('retornoPrevisto', $itinerary->retornoPrevisto, 'class="form-control tip" id="retornoPrevisto"', 'datetime-local'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <?= lang("note", "note") ?>
                                <?= form_textarea('note', $itinerary->note, 'class="form-control" id="note" '); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?php echo form_submit('edit_itinerary', $this->lang->line("edit_itinerary"), 'id="edit_itinerary" class="btn btn-primary"'); ?>
                                <a href="<?=base_url('itinerary/report_detail_itinerary/'.$itinerary->id);?>" target="_blank">
                                    <?php echo form_button('report_detail_itinerary', $this->lang->line("report_detail_itinerary"), 'id="report_detail_itinerary" class="btn btn-success"'); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="tours_itinerary" class="tab-pane fade">
            <div class="box">
                <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-map-signs"></i><?= lang('itineraries'); ?></h2></div>
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <?= lang("dataTour", "dataTour") ?>
                                <?= form_input('dataTour', $itinerary->dataEmbarque, 'class="form-control" id="dataTour" readonly','date'); ?>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group">
                                <?= lang("hourTour", "hourTour") ?>
                                <?= form_input('hourTour', '', 'class="form-control" id="hourTour"','time'); ?>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="form-group">
                                <?= lang("tour", "tour") ?>
                                <?php
                                $prod[''] =  lang("select") . " " . lang("product") ;
                                foreach ($products as $produts) {
                                    $prod[$produts->id] = $produts->name;
                                }
                                echo form_dropdown('tour', $prod, $itinerary->product_id, 'class="form-control select" id="tour" placeholder="' . lang("select") . " " . lang("tour") . '" required="required" style="width:100%"')
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="box">
                                <div class="box-header" style="border-top: 1px solid #dbdee0;">
                                    <h2 class="blue">
                                        <i class="fa-fw fa fa-heart"></i><?=lang('add_tours')?>
                                    </h2>
                                </div>
                                <div class="box-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="TUData" class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                        <th><?php echo $this->lang->line("hour"); ?></th>
                                                        <th><?php echo $this->lang->line("pick_up"); ?></th>
                                                        <th><?php echo $this->lang->line("destiny"); ?></th>
                                                        <th><?php echo $this->lang->line("customer"); ?></th>
                                                        <th><?php echo $this->lang->line("grand_total"); ?></th>
                                                        <th><?php echo $this->lang->line("paid"); ?></th>
                                                        <th><?php echo $this->lang->line("balance"); ?></th>
                                                        <th><?php echo $this->lang->line("payment_status"); ?></th>
                                                        <th style="display:none;"></th>
                                                        <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr><td colspan="9" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                                    </tbody>
                                                    <tfoot class="dtFilter">
                                                    <tr class="active">
                                                        <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                        <th style="text-align: center;"><?php echo $this->lang->line("hour"); ?></th>
                                                        <th><?php echo $this->lang->line("pick_up"); ?></th>
                                                        <th><?php echo $this->lang->line("destiny"); ?></th>
                                                        <th><?php echo $this->lang->line("customer"); ?></th>
                                                        <th><?php echo $this->lang->line("grand_total"); ?></th>
                                                        <th><?php echo $this->lang->line("paid"); ?></th>
                                                        <th><?php echo $this->lang->line("balance"); ?></th>
                                                        <th><?php echo $this->lang->line("payment_status"); ?></th>
                                                        <th style="display:none;"></th>
                                                        <th style="width:10px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="box">
                                <div class="box-header" style="border-top: 1px solid #dbdee0;">
                                    <h2 class="blue">
                                        <i class="fa-fw fa fa-heart"></i><?=lang('inserted_activities')?>
                                    </h2>
                                </div>
                                <div class="box-content">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="table-responsive">
                                                <table id="TIData" class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                        <th><?php echo $this->lang->line("hour"); ?></th>
                                                        <th><?php echo $this->lang->line("pick_up"); ?></th>
                                                        <th><?php echo $this->lang->line("destiny"); ?></th>
                                                        <th><?php echo $this->lang->line("customer"); ?></th>
                                                        <th style="display: none;"></th>
                                                        <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr><td colspan="5" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                                                    </tbody>
                                                    <tfoot class="dtFilter">
                                                    <tr class="active">
                                                        <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                                        <th style="text-align: center;"><?php echo $this->lang->line("hour"); ?></th>
                                                        <th><?php echo $this->lang->line("pick_up"); ?></th>
                                                        <th><?php echo $this->lang->line("destiny"); ?></th>
                                                        <th><?php echo $this->lang->line("customer"); ?></th>
                                                        <th style="display: none;"></th>
                                                        <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?=form_close()?>

<script type="text/javascript">
    $(document).ready(function () {
        $('#tour').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#dataTour').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $('#hourTour').change(function(event){
            $('#TUData').DataTable().fnClearTable();
        });

        $(document).on('ifChecked', '.checkAllTour', function(event) {
            $('.checkAllTour').iCheck('check');
        });

        $(document).on('ifUnchecked', '.checkAllTour', function(event) {
            $('.checkAllTour').iCheck('uncheck');
        });

        $('body').on('click', '.invoice_link_itinerary td:not(:first-child :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'salesutil/modal_view/' + $(this).parent('.invoice_link_itinerary').attr('id')});
            $('#myModal').modal('show');
        });

        $('#motorista').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#guia').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#fornecedor').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#tipo_transporte_id').select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "veiculo/getTipoTransporte/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "veiculo/suggestions_tipo_tranporte",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });

    function addItem(sale_item) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>itinerary/add_item",
            data: {
                sale_item: sale_item,
                itinerary_id : '<?=$itinerary->id;?>',
            },
            dataType: 'json',
            success: function (retorno) {
                $('#TIData').DataTable().fnClearTable();
                $('#TUData').DataTable().fnClearTable();
            }
        });
    }

    function removeItem(itinerary_item) {
        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>itinerary/remove_item",
            data: {
                itinerary_item: itinerary_item,
            },
            dataType: 'json',
            success: function (retorno) {
                $('#TIData').DataTable().fnClearTable();
                $('#TUData').DataTable().fnClearTable();
            }
        });
    }
</script>
