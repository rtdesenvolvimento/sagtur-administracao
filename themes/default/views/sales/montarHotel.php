<?php
        $fornecedor         = $hotel->fornecedor;
		$supplier_details 	= $this->site->getCompanyByID($fornecedor);
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i
                class="fa-fw fa fa-bed"></i><?= 'Configuração dos quartos no '. $supplier_details->company;?>
        </h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table width="100%">
                        <tr>
                            <td width="49%">
                                <div class="box">
                                    <div class="box-header">
                                        <h2 class="blue">
                                            <i class="fa-fw fa fa-bed"></i>Hospedes
                                        </h2>
                                    </div>

                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="SLData" class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 2%;">#</th>
                                                        <th>Passageiros</th>
                                                        <th style="width: 1%;">#</th>
                                                        <th style="width: 1%;">Tipo Quarto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php

                                                        $contador = 0;
                                                        $idsFilter      = '';
                                                        $idsFilterNotIn = '';

                                                        //print_r($itens_pedidos);

                                                        foreach ($itens_pedidos as $row) {

                                                            $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);
                                                            $totalDeItens    = $this->sales_model->buscarTotalDeItensDeUmaVendaPorVenda($row->sale_id);
                                                            $tipo = '';

                                                            if ($totalDeItens > 1) $tipo = 'Juntos';
                                                            else $tipo = 'Sozinho';

                                                            $encontrou      = false;

                                                            foreach ($itens_pedidos as $rowconsult) {

                                                                if($row->customerClient == $rowconsult->customer_1_id ||
                                                                    $row->customerClient == $rowconsult->customer_2_id ||
                                                                    $row->customerClient == $rowconsult->customer_3_id ||
                                                                    $row->customerClient == $rowconsult->customer_4_id ||
                                                                    $row->customerClient == $rowconsult->customer_5_id  ) {
                                                                    $encontrou = true;

                                                                    $idsFilterNotIn = $idsFilterNotIn.$row->customerClient.',';
                                                                }
                                                            }

                                                            if (!$encontrou) $idsFilter = $idsFilter . $row->customerClient . ',';


                                                            ?>
                                                            <?php if (!$encontrou) {?>
                                                                <tr>
                                                                    <td style="width: 2%;"><?echo $contador;?></td>
                                                                    <td><?php echo $objCustomer->name;?></td>
                                                                    <td><?php echo $tipo;?></td>
                                                                    <td style="width: 10%;">
                                                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                                            <a href="#" id="addHotelManually" sale_id="<?php echo $row->sale_id;?>"  slcustomer="<?php echo $objCustomer->id;?>" class="tip" title="<?= lang('add_hotel_manually') ?>">
                                                                                <i class="fa fa-bed addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            <?php }?>
                                                        <?php $contador = $contador + 1;}?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td width="2%"></td>
                            <td width="49%">
                                <div class="box">
                                    <div class="box-header">
                                        <h2 class="blue">
                                            <i class="fa-fw fa fa-bed"></i>Quartos
                                            <small>Para excluir clique em cima do nome do Primeiro passageiro e confirme!</small>
                                        </h2>
                                    </div>

                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <table id="SLData" class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>Hospede(s)</th>
                                                        <th>Tipo Quarto</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php

                                                    $total_duplo_casal          = 0;
                                                    $total_duplo_casal_1_adulto = 0;
                                                    $total_duplo_casal_2_adulto = 0;
                                                    $total_duplo_casal_3_adulto = 0;
                                                    $total_solteiro             = 0;
                                                    $total_duplo_solteiro       = 0;
                                                    $total_triplo_solteiro      = 0;
                                                    $total_single               = 0;

                                                    foreach ($itens_pedidos_group as $row) {

                                                        $hotel		= $row->hotel;
                                                        $hotel2		= $row->hotel2;
                                                        $hotel3		= $row->hotel3;
                                                        $hotel4		= $row->hotel4;
                                                        $hotel5		= $row->hotel5;
                                                        $hotel6		= $row->hotel6;
                                                        $hotel7		= $row->hotel7;
                                                        $sale_id    = $row->sale_id;

                                                        if ($fornecedor == $hotel ||
                                                            $fornecedor == $hotel2 ||
                                                            $fornecedor == $hotel3 ||
                                                            $fornecedor == $hotel4 ||
                                                            $fornecedor == $hotel5 ||
                                                            $fornecedor == $hotel6 ||
                                                            $fornecedor == $hotel7  ) {

                                                            $tipo_quarto    = $row->tipo_quarto;

                                                            $str_tipo_quarto	= '';
                                                            $total_pessoas 		= 0;

                                                            if ($tipo_quarto > 0 ) {

                                                                if ($tipo_quarto == 1) {
                                                                    $str_tipo_quarto	=  lang('duplo_casal');
                                                                    $total_pessoas = 2;
                                                                    $total_duplo_casal = $total_duplo_casal + 1;

                                                                } else if ($tipo_quarto == 2) {
                                                                    $str_tipo_quarto	= lang('duplo_casal_um_adulto');
                                                                    $total_duplo_casal_1_adulto =  $total_duplo_casal_1_adulto  + 1;
                                                                    $total_pessoas = 3;
                                                                } else if ($tipo_quarto == 3) {
                                                                    $str_tipo_quarto	= lang('duplo_casal_dois_adulto');
                                                                    $total_duplo_casal_2_adulto = $total_duplo_casal_2_adulto + 1;
                                                                    $total_pessoas = 4;
                                                                } else if ($tipo_quarto == 4) {
                                                                    $str_tipo_quarto	=  lang('duplo_casal_tres_adulto');
                                                                    $total_duplo_casal_3_adulto = $total_duplo_casal_3_adulto + 1;
                                                                    $total_pessoas = 5;
                                                                } else if ($tipo_quarto == 5) {
                                                                    $str_tipo_quarto	= lang('single');
                                                                    $total_pessoas = 1;
                                                                    $total_solteiro  = $total_solteiro  + 1;
                                                                } else if ($tipo_quarto == 6) {
                                                                    $str_tipo_quarto	= lang('duplo_solteiro');
                                                                    $total_pessoas = 2;
                                                                    $total_duplo_solteiro = $total_duplo_solteiro + 1;
                                                                } else if ($tipo_quarto == 7) {
                                                                    $str_tipo_quarto	= lang('tripo_solteiro');
                                                                    $total_triplo_solteiro = $total_triplo_solteiro + 1;
                                                                    $total_pessoas = 3;
                                                                } else if ($tipo_quarto == 8) {
                                                                    $str_tipo_quarto	= lang('single');
                                                                    $total_pessoas 		= 1;
                                                                    $total_single 		= $total_single + 1;
                                                                }

                                                                ?>
                                                                <tr style="cursor: pointer;">
                                                                    <?php if ($total_pessoas == 1) {
                                                                        $objCustomer 	 = $this->site->getCompanyByID($row->customer_1_id); ?>
                                                                        <td ordem="1"  sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);">
                                                                            <?php echo $row->customer_1; ?>
                                                                        </td>
                                                                    <?php } else { ?>
                                                                        <td>
                                                                            <table class="table table-bordered" border="1" id="table_export" style="width: 100%;border-collapse:collapse;">
                                                                                <?php if ($total_pessoas == 2) {

                                                                                    $objCustomer 	= $this->site->getCompanyByID($row->customer_1_id);
                                                                                    $objCustomer2 	= $this->site->getCompanyByID($row->customer_2_id);
                                                                                    ?>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="1" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_1.'/'.$objCustomer->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="2" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_2.'/'.$objCustomer2->vat_no; ?></td>
                                                                                    </tr>
                                                                                <?php }?>

                                                                                <?php if ($total_pessoas == 3) {
                                                                                    $objCustomer 	= $this->site->getCompanyByID($row->customer_1_id);
                                                                                    $objCustomer2 	= $this->site->getCompanyByID($row->customer_2_id);
                                                                                    $objCustomer3 	= $this->site->getCompanyByID($row->customer_3_id);

                                                                                    ?>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="1" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_1.'/'.$objCustomer->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="2" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_2.'/'.$objCustomer2->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="3" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_3.'/'.$objCustomer3->vat_no; ?></td>
                                                                                    </tr>
                                                                                <?php }?>

                                                                                <?php if ($total_pessoas == 4) {

                                                                                    $objCustomer 	= $this->site->getCompanyByID($row->customer_1_id);
                                                                                    $objCustomer2 	= $this->site->getCompanyByID($row->customer_2_id);
                                                                                    $objCustomer3 	= $this->site->getCompanyByID($row->customer_3_id);
                                                                                    $objCustomer4 	= $this->site->getCompanyByID($row->customer_4_id);
                                                                                    ?>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="1" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_1.'/'.$objCustomer->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="2" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_2.'/'.$objCustomer2->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="3" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_3.'/'.$objCustomer3->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="4" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_4.'/'.$objCustomer4->vat_no; ?></td>
                                                                                    </tr>
                                                                                <?php }?>

                                                                                <?php if ($total_pessoas == 5) {

                                                                                    $objCustomer 	= $this->site->getCompanyByID($row->customer_1_id);
                                                                                    $objCustomer2 	= $this->site->getCompanyByID($row->customer_2_id);
                                                                                    $objCustomer3 	= $this->site->getCompanyByID($row->customer_3_id);
                                                                                    $objCustomer4 	= $this->site->getCompanyByID($row->customer_4_id);
                                                                                    $objCustomer5	= $this->site->getCompanyByID($row->customer_5_id);
                                                                                    ?>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="1" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_1.'/'.$objCustomer->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="2" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_2.'/'.$objCustomer2->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="3" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_3.'/'.$objCustomer3->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="4" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_4.'/'.$objCustomer4->vat_no; ?></td>
                                                                                    </tr>
                                                                                    <tr style="cursor: pointer;">
                                                                                        <td ordem="5" sale_id="<?php echo $row->sale_id;?>" onclick="excluir_configuracao(this);"><?php echo $row->customer_5.'/'.$objCustomer5->vat_no; ?></td>
                                                                                    </tr>
                                                                                <?php }?>

                                                                            </table>
                                                                        </td>
                                                                    <?php } ?>
                                                                    <td align="CENTER"><b><?php echo $str_tipo_quarto; ?></b></td>
                                                                </tr>
                                                                <tr><td colspan=4><br/></td></tr>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <table class="table table-bordered" border="1" style="width: 100%;border-collapse:collapse;">
                        <thead>
                        <tr>
                            <th align="left">Tipo de Quarto</th>
                            <th>Qtd.</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if ($total_duplo_casal > 0) {?>
                            <tr>
                                <td><?php echo lang('duplo_casal');?></td>
                                <td  align="center"><?php echo $total_duplo_casal; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_duplo_casal_1_adulto > 0) {?>
                            <tr>
                                <td><?php echo lang('duplo_casal_um_adulto');?></td>
                                <td  align="center"><?php echo $total_duplo_casal_1_adulto; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_duplo_casal_2_adulto > 0) {?>
                            <tr>
                                <td><?php echo lang('duplo_casal_dois_adulto');?></td>
                                <td  align="center"><?php echo $total_duplo_casal_2_adulto; ?></td>
                            </tr>
                        <?php } ?>


                        <?php if ($total_duplo_casal_3_adulto > 0) {?>
                            <tr>
                                <td><?php echo lang('duplo_casal_tres_adulto');?></td>
                                <td  align="center"><?php echo $total_duplo_casal_3_adulto; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_solteiro > 0) {?>
                            <tr>
                                <td><?php echo lang('single');?></td>
                                <td  align="center"><?php echo $total_solteiro; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_duplo_solteiro > 0) {?>
                            <tr>
                                <td><?php echo lang('duplo_solteiro');?></td>
                                <td  align="center"><?php echo $total_duplo_solteiro; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_triplo_solteiro > 0) {?>
                            <tr>
                                <td><?php echo lang('tripo_solteiro');?></td>
                                <td  align="center"><?php echo $total_triplo_solteiro; ?></td>
                            </tr>
                        <?php } ?>

                        <?php if ($total_single > 0) {?>
                            <tr>
                                <td><?php echo lang('single');?></td>
                                <td  align="center"><?php echo $total_single; ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                    <div class="buttons">
                        <div class="btn-group btn-group-justified">

                                <div class="btn-group">
                                    <a href="<?= site_url('sales/pdf_hotel/' . $product_id.'/'.$idVariante) ?>"
                                       class="tip btn btn-primary" title="">
                                        <i class="fa fa-bed"></i> <span
                                                class="hidden-sm hidden-xs">Imprimir</span>
                                    </a>
                                </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ################################### MODAL HOTEL ##########################################################-->
<div class="modal" id="mModal3" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_hotel_manually') ?></h4>
            </div>

            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" action="<?php echo base_url('/sales/salvar_hotel');?>" id="formHotel" role="form">

                    <!--########################!-->
                    <!--####   FORNECEDORES   ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier" value="" id="posupplier" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--#### TIPO DE QUARTO ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('tipo_quarto') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="sale_id" id="sale_id" value="">
                            <input type="hidden" name="idVariante" value="<?php echo $idVariante;?>">
                            <input type="hidden" name="product_id" value="<?php echo $product_id;?>">
                            <input type="hidden" name="fornecedor_id" value="<?php echo $supplier_details->id;?>">
                            <input type="hidden" name="contadorHotel" value="<?php echo $contadorHotel;?>">

                            <select class="form-control" name="tipo_quarto" id="tipo_quarto" required="required">
                                <option value="8"><?php echo lang('single');?></option>
                                <option value="1"><?php echo lang('duplo_casal');?></option>
                                <option value="2"><?php echo lang('duplo_casal_um_adulto');?></option>
                                <option value="3"><?php echo lang('duplo_casal_dois_adulto');?></option>
                                <option value="4"><?php echo lang('duplo_casal_tres_adulto');?></option>
                                <option value="6"><?php echo lang('duplo_solteiro');?></option>
                                <option value="7"><?php echo lang('tripo_solteiro');?></option>
                            </select>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 1 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_1">
                        <label for="mtax" class="col-sm-4 control-label"><?= '1º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_1', (isset($_POST['customer_1']) ? $_POST['customer_1'] : ""), 'id="slcustomer_1" data-placeholder="' . lang("select") . ' ' . lang("customer_1") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 2 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_2" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '2º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_2', (isset($_POST['customer_2']) ? $_POST['customer_2'] : ""), 'id="slcustomer_2" data-placeholder="' . lang("select") . ' ' . lang("customer_2") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 3 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_3" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '3º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_3', (isset($_POST['customer_3']) ? $_POST['customer_3'] : ""), 'id="slcustomer_3" data-placeholder="' . lang("select") . ' ' . lang("customer_3") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 4 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_4" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '4º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_4', (isset($_POST['customer_4']) ? $_POST['customer_4'] : ""), 'id="slcustomer_4" data-placeholder="' . lang("select") . ' ' . lang("customer_4") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 5 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_5" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '5º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_5', (isset($_POST['customer_5']) ? $_POST['customer_5'] : ""), 'id="slcustomer_5" data-placeholder="' . lang("select") . ' ' . lang("customer_5") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="addItemHotelManually"><?= lang('submit') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--####################################################### FINAL DO MODAL HOTEL #####################################--->


<script>
    /* ------------------------------
 * Show manual hotel modal
 ------------------------------- */
    $(document).on('click', '#addHotelManually', function (e) {

        var slcustomer = $(this).attr('slcustomer');
        var sale_id     = $(this).attr('sale_id');

        $('#sale_id').val(sale_id);

        $('#posupplier').val('<?php  echo $fornecedor;?>').select2({
            minimumInputLength: 0,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + "suppliers/suggestionsByID/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        if (data.results != null) {
                            callback(data.results[0]);
                        }
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestionsINIDS",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: <?php echo $fornecedor;?>,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#slcustomer_1').val(slcustomer).select2({
            minimumInputLength: 1,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url + "customers/suggestionsByID/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        if (data.results != null) {
                            callback(data.results[0]);
                        }
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestionsByID",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        term: slcustomer,
                        limit: 1
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        nsCustomer_2();
        nsCustomer_3();
        nsCustomer_4();
        nsCustomer_5();

        $('#mModal3').appendTo("body").modal('show');
        return false;
    });

    $('#tipo_quarto').change(function (e) {
        var tipo_quarto = $(this).val();

        $('#div_slcustomer_1').hide();
        $('#div_slcustomer_2').hide();
        $('#div_slcustomer_3').hide();
        $('#div_slcustomer_4').hide();
        $('#div_slcustomer_5').hide();

        if (tipo_quarto == 1) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
        } else if (tipo_quarto == 2) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
            $('#div_slcustomer_3').show();
        } else if (tipo_quarto == 3) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
            $('#div_slcustomer_3').show();
            $('#div_slcustomer_4').show();
        } else if (tipo_quarto == 4) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
            $('#div_slcustomer_3').show();
            $('#div_slcustomer_4').show();
            $('#div_slcustomer_5').show();
        } else if (tipo_quarto == 5) {
            $('#div_slcustomer_1').show();
        } else if (tipo_quarto == 6) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
        } else if (tipo_quarto == 7) {
            $('#div_slcustomer_1').show();
            $('#div_slcustomer_2').show();
            $('#div_slcustomer_3').show();
        } else if (tipo_quarto == 8) {
            $('#div_slcustomer_1').show();
        }
    });

    $(document).on('click', '#addItemHotelManually', function (e) {
        $('#mModal3').modal('hide');
        $('#formHotel').submit();
        return false;
    });

    function excluir_configuracao(tag) {

        if (confirm('Deseja realmente excluir este configuração de quarto do passageiro?')) {
            var ordem = tag.getAttribute('ordem');
            var sale_id = tag.getAttribute('sale_id');

            $.ajax({
                type: "get",
                async: false,
                url: site.base_url + "sales/excluir_hotel/" + sale_id + '/' + ordem,
                dataType: "html",
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

    function nsCustomer_2() {
        $('#slcustomer_2').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestionsByIDS",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        idsNotIn: '<?php echo $idsFilterNotIn.'0';?>',
                        ids: '<?php echo $idsFilter.'0';?>'
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    }

    function nsCustomer_3() {
        $('#slcustomer_3').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        idsNotIn: '<?php echo $idsFilterNotIn.'0';?>',
                        ids: '<?php echo $idsFilter.'0';?>'
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    }

    function nsCustomer_4() {
        $('#slcustomer_4').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        idsNotIn: '<?php echo $idsFilterNotIn.'0';?>',
                        ids: '<?php echo $idsFilter.'0';?>'
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    }

    function nsCustomer_5() {
        $('#slcustomer_5').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        idsNotIn: '<?php echo $idsFilterNotIn.'0';?>',
                        ids: '<?php echo $idsFilter.'0';?>'
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    }
</script>