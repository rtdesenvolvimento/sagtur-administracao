<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('motivo_cancelamento_venda'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("sales/cancelar_action/" . $inv->id, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <?= lang('motivo_cancelamento', 'motivo_cancelamento'); ?>
                        <select name="motivo_cancelamento_id" id="motivo_cancelamento_id" class="form-control input-tip select" required="required" data-placeholder="Selecionar Motivo pelo Cancelamento" style="width:100%;">
                            <option value=""><?=lang("select") . ' ' . lang("motivo_cancelamento_venda");?></option>
                            <?php foreach ($motivos_cancelamento as $motivo){?>
                                <option value="<?=$motivo->id;?>" gerar_credito="<?=$motivo->gerar_credito;?>" gerar_reembolso="<?=$motivo->gerar_reembolso;?>" obriga_observacao="<?=$motivo->obriga_observacao;?>"><?=$motivo->name;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("paid", "paid"); ?>
                        <?php echo form_input('paid', $this->sma->formatDecimal($inv->paid), 'class="form-control mask_money" readonly style="padding-right: 5px;" id="paid"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', '', 'class="form-control" style="height: 100px;" id="note"'); ?>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group" id="div_checkbox_gerar_credito" style="display: none;">
                        <?php echo form_checkbox('gerar_credito', '1', FALSE, 'id="gerar_credito"'); ?>
                        <label for="attributes" class="padding05"><?= lang('gerar_credito'); ?></label>
                    </div>
                    <div id="div_credito_cliente" style="display: none;">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><h4><?= lang('credito_cliente') ?></h4></div>
                            <div class="panel-body" style="padding: 5px;">
                                <!--credito de viagem !-->
                                <div class="form-group">
                                    <?= lang("card_no", "card_no"); ?>
                                    <div class="input-group">
                                        <?php echo form_input('card_no', '', 'class="form-control" id="card_no"'); ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a href="#" id="genNo">
                                                <i class="fa fa-cogs"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("value_credit", "value"); ?>
                                            <?php echo form_input('value', 0.00, 'class="form-control mask_money" style="padding-right: 5px;" id="value"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("expiry_date", "expiry"); ?>
                                            <?php echo form_input('expiry', date("Y-m-d", strtotime("+1 year")), 'class="form-control" id="expiry"', 'date'); ?>
                                            <small>+1 ano</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= lang("note", "note"); ?>
                                    <?php echo form_textarea('note_credito', '', 'class="form-control" id="note_credito" style="height: 100px;"'); ?>
                                    <br/><br/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group" id="div_checkbox_gerar_reembolso" style="display: none;">
                        <?php echo form_checkbox('gerar_reembolso', '1', FALSE, 'id="gerar_reembolso"'); ?>
                        <label for="attributes" class="padding05"><?= lang('gerar_reembolso'); ?></label>
                    </div>
                    <div id="div_reembolso" style="display: none;">
                        <div class="panel panel-warning">
                            <div class="panel-heading"><h4><?= lang('reembolso') ?></h4></div>
                            <div class="panel-body" style="padding: 5px;">
                                <!--credito de viagem !-->
                                <div class="form-group">
                                    <?= lang("card_no_refund", "card_no_refund"); ?>
                                    <div class="input-group">
                                        <?php echo form_input('card_no_refund', '', 'class="form-control" id="card_no_refund"'); ?>
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <a href="#" id="genNoReembolso">
                                                <i class="fa fa-cogs"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("value_refund", "value"); ?>
                                            <?php echo form_input('value_refund', 0.00, 'class="form-control mask_money" style="padding-right: 5px;" id="value_refund"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("expiry_date", "expiry"); ?>
                                            <?php echo form_input('expiry_refund', date("Y-m-d", strtotime("+1 month")), 'class="form-control" id="expiry_refund"', 'date'); ?>
                                            <small>+1 mês</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <?= lang("note", "note"); ?>
                                    <?php echo form_textarea('note_refund', '', 'class="form-control" id="note_refund" style="height: 100px;"'); ?>
                                    <small>O reembolso não faz o estorno dos valores em Cartão de Crédito ou Pix automático para o cliente.</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('cancel_sale', lang('cancel_sale'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" charset="UTF-8">

    $(document).ready(function () {

        $('#gerar_credito').click(function (event){
            if ($('#gerar_credito').is(':checked')) {

                $('#card_no').attr('required', 'required');
                $('#value').attr('required', 'required');
                $('#expiry').attr('required', 'required');

                $('#div_credito_cliente').show();
            } else {

                $('#card_no').removeAttr('required');
                $('#value').removeAttr('required', 'required');
                $('#expiry').removeAttr('required', 'required');

                $('#div_credito_cliente').hide();
            }
        });

        $('#gerar_reembolso').click(function (event){
            if ($('#gerar_reembolso').is(':checked')) {

                $('#card_no_refund').attr('required', 'required');
                $('#value_refund').attr('required', 'required');
                $('#expiry_refund').attr('required', 'required');

                $('#div_reembolso').show();
            } else {

                $('#card_no_refund').removeAttr('required');
                $('#value_refund').removeAttr('required', 'required');
                $('#expiry_refund').removeAttr('required', 'required');

                $('#div_reembolso').hide();
            }
        });

        $('#motivo_cancelamento_id').change(function (event){

            var gerar_credito = $(this).find('option:selected').attr('gerar_credito');
            var gerar_reembolso = $(this).find('option:selected').attr('gerar_reembolso');
            let paid = $('#paid').val();

            if (paid > 0){
                if (gerar_credito === '1') {
                    $('#div_checkbox_gerar_credito').show();
                    $('#div_credito_cliente').hide();
                    $('#gerar_credito').attr('checked', false);
                } else {
                    $('#gerar_credito').attr('checked', false);
                    $('#div_checkbox_gerar_credito').hide();
                    $('#div_credito_cliente').hide();
                }

                if (gerar_reembolso === '1') {
                    $('#div_checkbox_gerar_reembolso').show();
                    $('#div_reembolso').hide();
                    $('#gerar_reembolso').attr('checked', false);
                } else {
                    $('#div_checkbox_gerar_reembolso').hide();
                    $('#div_reembolso').hide();
                    $('#gerar_reembolso').attr('checked', false);
                }
            }
        });

        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });

        $('#genNoReembolso').click(function (){
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });

        $('#motivo_cancelamento_id').change(function (event){
            var obrigatorio = $(this).find('option:selected').attr('obriga_observacao');
            if (obrigatorio === '1') {
                $('#note').attr('required', 'required');
            } else {
                $('#note').removeAttr('required');
            }
        });

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        /*
        $('#value').keyup(function (){
            let paid = $('#paid').val();
            let credit = $(this).val();

            $('#value_refund').val(paid-credit);
        });

        $('#value_refund').keyup(function (){
            let paid = $('#paid').val();
            let refund = $(this).val();

            $('#value').val(paid-refund);
        });
         */

        $('#genNo').click();
        $('#genNoReembolso').click();
    });

    function calcular_diferenca_pagar() {
        let paid = $('#paid').val();
        let credit = $('#value').val();
        let refund =  $('#value_refund').val();


    }
</script>