<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <?= lang("supplierTransfer", "supplierTransfer"); ?>
            <?php echo form_input('supplierTransfer', (isset($_POST['supplierTransfer']) ? $_POST['supplierTransfer'] : ""), 'class="form-control" id="supplierTransfer"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("emissaoTransfer", "emissaoTransfer"); ?>
            <?php echo form_input('emissaoTransfer', (isset($_POST['emissaoTransfer']) ? $_POST['emissaoTransfer'] : ""), 'class="form-control input-tip" id="emissaoTransfer"', 'date'); ?>
        </div>
    </div>
</div>
<h2 class="title" style="margin-top: 0px;"><?= lang('header.label.transfer.agendar.in') ?></h2>
<div class="row">
    <div class="col-md-2">
        <div class="form-group company">
            <select id="agendarIn" name="agendarIn" class="form-control" required="required">
                <option value="NAO">NAO</option>
                <option value="SIM">SIM</option>
            </select>
        </div>
    </div>
</div>
<div id="divAgendarIn" style="display: none;">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group company">
                <?= lang("tipo", "tipo"); ?>
                <select id="tipoTransferOrigemIn" name="tipoTransferOrigemIn" class="form-control">
                    <option value="Outros">Outros</option>
                    <option value="hotel">Hotel</option>
                    <option value="Aeroporto">Aeroporto</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= lang("origemTransferIn", "slOrigemTransferIn"); ?>
                <?php
                echo form_input('origemTransferIn', (isset($_POST['origemTransferIn']) ? $_POST['origemTransferIn'] : ''), 'id="slOrigemTransferIn" class="form-control input-tip autocomplete_origem_destino" style="width:100%;"');
                ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <?= lang("dataChegadaTransferIn", "slDataChegadaTransferIn"); ?>
                <?php echo form_input('dataChegadaTransferIn', (isset($_POST['dataChegadaTransferIn']) ? $_POST['dataChegadaTransferIn'] : ""), 'class="form-control input-tip" id="slDataChegadaTransferIn"', 'date'); ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <?= lang("horaChegadaTransferIn", "slHoraChegadaTransferIn"); ?>
                <?php echo form_input('horaChegadaTransferIn', (isset($_POST['horaChegadaTransferIn']) ? $_POST['horaChegadaTransferIn'] : ""), 'class="form-control input-tip" id="slHoraChegadaTransferIn"', 'time'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group company">
                <?= lang("tipo", "tipo"); ?>
                <select id="tipoTransferDestinoIn" name="tipoTransferDestinoIn" class="form-control">
                    <option value="Outros">Outros</option>
                    <option value="hotel">Hotel</option>
                    <option value="Aeroporto">Aeroporto</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= lang("destinoTransferIn", "slDestinoTransferIn"); ?>
                <?php
                echo form_input('destinoTransferIn', (isset($_POST['destinoTransferIn']) ? $_POST['destinoTransferIn'] : ''), 'id="slDestinoTransferIn" class="form-control input-tip autocomplete_origem_destino" style="width:100%;"');
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= lang("reservaTransferIn", "slReservaTransferIn"); ?>
                <?php echo form_input('reservaTransferIn', (isset($_POST['reservaTransferIn']) ? $_POST['reservaTransferIn'] : ""), 'class="form-control" id="slReservaTransferIn"'); ?>
            </div>
        </div>
    </div>
</div>
<h2 class="title" style="margin-top: 0px;"><?= lang('header.label.transfer.agendar.out') ?></h2>
<div class="row">
    <div class="col-md-2">
        <div class="form-group">
            <select id="agendarOut" name="agendarOut" class="form-control">
                <option value="NAO">NAO</option>
                <option value="SIM">SIM</option>
            </select>
        </div>
    </div>
</div>
<div id="divAgendarOut" style="display: none;">
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <?= lang("tipo", "tipo"); ?>
                <select id="tipoTransferDestinoOut" name="tipoTransferDestinoOut" class="form-control">
                    <option value="Outros">Outros</option>
                    <option value="hotel">Hotel</option>
                    <option value="Aeroporto">Aeroporto</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= lang("destinoTransferOut", "slDestinoTransferOut"); ?>
                <?php echo form_input('destinoTransferOut', (isset($_POST['destinoTransferOut']) ? $_POST['destinoTransferOut'] : ''), 'id="slDestinoTransferOut" class="form-control input-tip autocomplete_origem_destino" style="width:100%;"'); ?>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <?= lang("dataPartidaTransferOut", "slDataPartidaTransferOut"); ?>
                <?php echo form_input('dataPartidaTransferOut', (isset($_POST['dataPartidaTransferOut']) ? $_POST['dataPartidaTransferOut'] : ""), 'class="form-control input-tip" id="slDataPartidaTransferOut"', 'date'); ?>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <?= lang("horaPartidaTransferOut", "slHoraPartidaTransferOut"); ?>
                <?php echo form_input('horaPartidaTransferOut', (isset($_POST['horaPartidaTransferOut']) ? $_POST['horaPartidaTransferOut'] : ""), 'class="form-control input-tip" id="slHoraPartidaTransferOut"', 'time'); ?>
            </div>
        </div>
        <div class="col-md-1">
            <div class="form-group">
                <?= lang("referenciaVooBusTransferOut", "referenciaVooBusTransferOut"); ?>
                <?php echo form_input('referenciaVooBusTransferOut', (isset($_POST['referenciaVooBusTransferOut']) ? $_POST['referenciaVooBusTransferOut'] : ""), 'class="form-control" id="referenciaVooBusTransferOut"'); ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <?= lang("tipo", "tipo"); ?>
                <select id="tipoTransferOrigemOut" name="tipoTransferOrigemOut" class="form-control">
                    <option value="Outros">Outros</option>
                    <option value="hotel">Hotel</option>
                    <option value="Aeroporto">Aeroporto</option>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <?= lang("origemTransferOut", "slOrigemTransferOut"); ?>
                <?php
                echo form_input('origemTransferOut', (isset($_POST['origemTransferOut']) ? $_POST['origemTransferOut'] : ''), 'id="slOrigemTransferOut" class="form-control input-tip autocomplete_origem_destino" style="width:100%;"');
                ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= lang("reservaTransferOut", "slReservaTransferOut"); ?>
                <?php echo form_input('reservaTransferOut', (isset($_POST['reservaTransferOut']) ? $_POST['reservaTransferOut'] : ""), 'class="form-control" id="slReservaTransferOut"'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= lang("dataSaidaTransferOut", "slDataSaidaTransferOut"); ?>
                <?php echo form_input('dataSaidaTransferOut', (isset($_POST['dataSaidaTransferOut']) ? $_POST['dataSaidaTransferOut'] : ""), 'class="form-control input-tip" id="slDataSaidaTransferOut"', 'date'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <?= lang("horaSaidaTransferOut", "slHoraSaidaTransferOut"); ?>
                <?php echo form_input('horaSaidaTransferOut', (isset($_POST['horaSaidaTransferOut']) ? $_POST['horaSaidaTransferOut'] : ""), 'class="form-control input-tip" id="slHoraSaidaTransferOut"', 'time'); ?>
            </div>
        </div>
    </div>
</div>
<br/>
<div id="load-new-transfer"></div>
<div class="clearfix"></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
        <div class="fprom-group">
            <button type="button" class="btn btn-primary" name="adicionar-transfer" id="adicionar-transfer"><?= lang('button.label.adiconar.transfer');?></button>
        </div>
    </div>
</div>
