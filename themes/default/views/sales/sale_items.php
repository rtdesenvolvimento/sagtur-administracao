<style type="text/css" media="screen">
    #SLDataItem td:nth-child(2) {text-align: center;}
    #SLDataItem td:nth-child(3) {text-align: center;}

    #SLDataItem td:nth-child(4) {text-align: left;width: 15%;}


    #SLDataItem td:nth-child(5) {
        width: 30%;
        font-weight: 500;
        font-size: 12px;
        line-height: 1.71;
        color: #428bca;
    }

    #SLDataItem td:nth-child(6) {width: 60%;}

    #SLDataItem td:nth-child(8) {text-align: right;}
    #SLDataItem td:nth-child(9) {text-align: right;}
    #SLDataItem td:nth-child(10) {text-align: right;}

    .link_relatorio_venda {
        margin-right: 10px;
        font-weight: 600;
    }
</style>

<script>
    $(document).ready(function () {

        $('#situacao').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        $('#filterStatus').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        $('#billerFilter').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        $('#filterOpcional').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        $('#flDataVendaDe').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        $('#fdivulgacao').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });


        $('#flDataVendaAte').change(function(event){
            $('#SLDataItem').DataTable().fnClearTable()
        });

        function origem_venda(x) {
            if (x === '1') {
                return '<div class="text-center" title="Venda Vindo do Site"><span class="label label-warning"><i class="fa fa-shopping-cart"></i> Site</span></div>';
            } else {
                return '<div class="text-center" title="Venda Lançada Manualmente"><span class="label label-danger" style="background-color: #428bca;"><i class="fa fa-desktop"></i> Manual</span></div>';
            }
        }

        var oTable = $('#SLDataItem').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=  ($this->Owner || $this->Admin) && ($productId != null && $productId != 'all') ? -1 : $Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('salesutil/getSaleItems'.($programacaoId != null ? '/'.$programacaoId : ''))?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "invoice_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "situacao", "value":  $('#situacao').val() });
                aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                aoData.push({ "name": "filterOpcional", "value":  $('#filterOpcional').val() });
                aoData.push({ "name": "flDataVendaDe", "value":  $('#flDataVendaDe').val() });
                aoData.push({ "name": "flDataVendaAte", "value":  $('#flDataVendaAte').val() });
                aoData.push({ "name": "product", "value":  $('#product').val() });
                aoData.push({ "name": "fdivulgacao", "value":  $('#fdivulgacao').val() });
            },
            "aoColumns":
                [
                    {"bSortable": false, "mRender": checkbox},
                    {"mRender": fld},
                    null,
                    null,
                    null,
                    null,
                    {"mRender": row_status},
                    {"mRender": currencyFormat},
                    {"mRender": currencyFormat},
                    {"mRender": currencyFormat},
                    {"mRender": row_status},
                    {"mRender": origem_venda},
                    {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {

                    if (!isNaN(aaData[aiDisplay[i]][7]) && aaData[aiDisplay[i]][7] !== null) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][7]);
                    }

                    if (!isNaN(aaData[aiDisplay[i]][8]) && aaData[aiDisplay[i]][8] !== null) {
                        paid += parseFloat(aaData[aiDisplay[i]][8]);
                    }

                    if (!isNaN(aaData[aiDisplay[i]][9]) && aaData[aiDisplay[i]][9] !== null) {
                        balance += parseFloat(aaData[aiDisplay[i]][9]);
                    }
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[7].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[8].innerHTML = currencyFormat(parseFloat(paid));
                nCells[9].innerHTML = currencyFormat(parseFloat(balance));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('product');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('sale_status');?>]", filter_type: "text", data: []},
            {column_number: 10, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer");

        if (localStorage.getItem('remove_slls')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('sdivulgacao')) {
                localStorage.removeItem('sdivulgacao');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            localStorage.removeItem('remove_slls');
        }

        <?php if ($this->session->userdata('remove_slls')) {?>
        if (localStorage.getItem('slitems')) {
            localStorage.removeItem('slitems');
        }
        if (localStorage.getItem('sldiscount')) {
            localStorage.removeItem('sldiscount');
        }
        if (localStorage.getItem('sltax2')) {
            localStorage.removeItem('sltax2');
        }
        if (localStorage.getItem('slref')) {
            localStorage.removeItem('slref');
        }
        if (localStorage.getItem('slshipping')) {
            localStorage.removeItem('slshipping');
        }
        if (localStorage.getItem('slwarehouse')) {
            localStorage.removeItem('slwarehouse');
        }
        if (localStorage.getItem('slnote')) {
            localStorage.removeItem('slnote');
        }
        if (localStorage.getItem('slinnote')) {
            localStorage.removeItem('slinnote');
        }
        if (localStorage.getItem('slcustomer')) {
            localStorage.removeItem('slcustomer');
        }
        if (localStorage.getItem('slbiller')) {
            localStorage.removeItem('slbiller');
        }
        if (localStorage.getItem('sdivulgacao')) {
            localStorage.removeItem('sdivulgacao');
        }
        if (localStorage.getItem('slcurrency')) {
            localStorage.removeItem('slcurrency');
        }
        if (localStorage.getItem('sldate')) {
            localStorage.removeItem('sldate');
        }
        if (localStorage.getItem('slsale_status')) {
            localStorage.removeItem('slsale_status');
        }
        if (localStorage.getItem('slpayment_status')) {
            localStorage.removeItem('slpayment_status');
        }
        if (localStorage.getItem('paid_by')) {
            localStorage.removeItem('paid_by');
        }
        if (localStorage.getItem('amount_1')) {
            localStorage.removeItem('amount_1');
        }
        if (localStorage.getItem('paid_by_1')) {
            localStorage.removeItem('paid_by_1');
        }
        if (localStorage.getItem('pcc_holder_1')) {
            localStorage.removeItem('pcc_holder_1');
        }
        if (localStorage.getItem('pcc_type_1')) {
            localStorage.removeItem('pcc_type_1');
        }
        if (localStorage.getItem('pcc_month_1')) {
            localStorage.removeItem('pcc_month_1');
        }
        if (localStorage.getItem('pcc_year_1')) {
            localStorage.removeItem('pcc_year_1');
        }
        if (localStorage.getItem('pcc_no_1')) {
            localStorage.removeItem('pcc_no_1');
        }
        if (localStorage.getItem('cheque_no_1')) {
            localStorage.removeItem('cheque_no_1');
        }
        if (localStorage.getItem('slpayment_term')) {
            localStorage.removeItem('slpayment_term');
        }

        localStorage.removeItem('previsao_pagamento');
        localStorage.removeItem('valor');
        localStorage.removeItem('tipoCobrancaId');
        localStorage.removeItem('condicaopagamentoId');

        <?php $this->sma->unset_data('remove_slls');}?>

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
	    echo form_open('sales/sale_actions', 'id="action-form"');
	}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-heart"></i><?=strtoupper(lang('sale_items'))?>
            <?php if (!empty($product)) {
               echo '#'.$product->name. ' Saída '.$this->sma->hrsd($programacao->dataSaida);
            }?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('sales/add')?>">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_sale')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="pdf" data-action="export_pdf">
                                <i class="fa fa-file-pdf-o"></i> <?=lang('export_to_pdf')?>
                            </a>
                        </li>

                        <?php if ( ($this->Owner || $this->Admin) && ($productId != null && $productId != 'all') ) { ?>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="bpo"
                                   title="<b><?=$this->lang->line("generate_sales_commissions")?></b>"
                                   data-content="<p><?=lang('r_u_sure')?></p><button type='button' class='btn btn-danger' id='generate_sales_commissions' data-action='generate_sales_commissions'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>"
                                   data-html="true" data-placement="left">
                                    <i class="fa fa-paypal"></i> <?=lang('generate_sales_commissions')?>
                                </a>
                            </li>
                        <?php }?>
                        <li style="display: none;">
                            <a href="#" class="bpo"
                            title="<b><?=$this->lang->line("delete_sales")?></b>"
                            data-content="<p><?=lang('r_u_sure')?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?=lang('i_m_sure')?></a> <button class='btn bpo-close'><?=lang('no')?></button>"
                            data-html="true" data-placement="left">
                            <i class="fa fa-trash-o"></i> <?=lang('delete_sales')?>
                        </a>
                    </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <?php if ($this->Owner || $this->Admin) { ?>
            <fieldset class="scheduler-border">
                <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-c.gif"></legend>
                <div style="margin-bottom: 20px;display: none;"  class="divfilters">
                    <div class="col-sm-2">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('status', $opts,  $unit, 'class="form-control" id="status"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('ano', $opts,  $ano, 'class="form-control" id="ano"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('mes', $opts,  $mes, 'class="form-control" id="mes"'); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= lang("product", "products") ?>
                        <?php
                        $pgs[""] = lang('select').' '.lang('product');

                        echo form_dropdown('product', $pgs,  $productId, 'class="form-control" id="product" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                    </div>
                    <div  class="col-sm-4">
                        <?= lang("Status da Venda", "filterStatus") ?>
                        <?php
                        $cbStatus = array(
                            '' => lang('select'),
                            'orcamento' => lang('orcamento'),
                            'faturada' => lang('faturada'),
                            'lista_espera' => lang('lista_espera'),
                            'cancel' => lang('cancel')
                        );
                        echo form_dropdown('filterStatus', $cbStatus,  $ano, 'class="form-control" id="filterStatus"'); ?>
                    </div>
                    <div  class="col-sm-4">
                        <?= lang("Situação de Pagamento", "situacao") ?>
                        <?php
                        $cbSituacao = array(
                            '' => lang('select'),
                            'due' => lang('due'),
                            'partial' => lang('partial'),
                            'paid' => lang('paid'),
                            'cancel' => lang('cancel'),
                        );
                        echo form_dropdown('situacao', $cbSituacao,  $ano, 'class="form-control" id="situacao"'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= lang("biller", "billerFilter"); ?>
                        <?php
                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->name;
                        }
                        echo form_dropdown('billerFilter', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                    <?php if(!empty($adicionais)) {?>
                        <?php if ($productId != null && $productId != 'all') {?>
                            <div class="col-sm-12">
                                <?= lang("opcional", "filterOpcional") ?>
                                <?php
                                $cbopcionais[""] = lang('select').' '.lang('adicional');
                                foreach ($adicionais as $adicional) {
                                    $cbopcionais[$adicional->id] = $adicional->name;
                                }
                                echo form_dropdown('filterOpcional', $cbopcionais,  $ano, 'class="form-control" id="filterOpcional"');
                                ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= lang("meio_divulgacao", "fdivulgacao"); ?>
                            <?php
                            $md[""] = lang("select") . ' ' . lang("meio_divulgacao") ;
                            foreach ($meiosDivulgacao as $divulgacao) {
                                $md[$divulgacao->id] = $divulgacao->name;
                            }
                            echo form_dropdown('fdivulgacao', $md, $inv->meio_divulgacao, 'id="fdivulgacao" name="fdivulgacao" data-placeholder="' . lang("select") . ' ' . lang("meio_divulgacao") . '"class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4" style="margin-bottom: 10px;">
                        <?= lang("data_venda_de", "data_venda_de"); ?>
                        <?php echo form_input('flDataVendaDe', (isset($_POST['flDataVendaDe']) ? $_POST['flDataVendaDe'] : $flDataVendaDe), 'type="date" class="form-control" id="flDataVendaDe"', 'date'); ?>
                    </div>
                    <div class="col-sm-4">
                        <?= lang("data_venda_ate", "data_venda_ate"); ?>
                        <?php echo form_input('flDataVendaAte', (isset($_POST['flDataVendaAte']) ? $_POST['flDataVendaAte'] : $flDataVendaAte), 'type="date" class="form-control" id="flDataVendaAte"', 'date'); ?>
                    </div>
                </div>
            </fieldset>
        <?php } else { ?>
                <div class="row" style="margin-bottom: 25px;">
                    <div class="col-sm-2">
                        <?= lang("Status do Serviço", "status_viagem") ?>
                        <?php
                        $opts = array(
                            'Confirmado' => lang('confirmado'),
                            'Inativo' => lang('produto_inativo'),
                            'Arquivado' => lang('arquivado') ,
                        );
                        echo form_dropdown('status', $opts,  $unit, 'class="form-control" id="status"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("Ano do Serviço", "ano") ?>
                        <?php
                        $opts = array(
                            '2020' => lang('2020'),
                            '2021' => lang('2021'),
                            '2022' => lang('2022'),
                            '2023' => lang('2023'),
                            '2024' => lang('2024'),
                            '2025' => lang('2025'),
                            '2026' => lang('2026'),
                            '2027' => lang('2027'),
                            '2028' => lang('2028'),
                            '2029' => lang('2029'),
                            '2030' => lang('2030'),
                        );
                        echo form_dropdown('ano', $opts,  $ano, 'class="form-control" id="ano"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("Mês do Serviço", "mes") ?>
                        <?php
                        $opts = array(
                            'Todos' => lang('Todos'),
                            '01' => lang('Janeiro'),
                            '02' => lang('Fevereiro'),
                            '03' => lang('Março'),
                            '04' => lang('Abril'),
                            '05' => lang('Maio'),
                            '06' => lang('Junho'),
                            '07' => lang('Julho'),
                            '08' => lang('Agosto'),
                            '09' => lang('Setembro'),
                            '10' => lang('Outubro'),
                            '11' => lang('Novembro'),
                            '12' => lang('Dezembro'),
                        );
                        echo form_dropdown('mes', $opts,  $mes, 'class="form-control" id="mes"'); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= lang("product", "products") ?>
                        <?php
                        $pgs[""] = lang('select').' '.lang('product');

                        echo form_dropdown('product', $pgs,  $productId, 'class="form-control" id="product" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"'); ?>
                    </div>
                </div>
        <?php } ?>

        <?php if ( ($this->Owner || $this->Admin) && ($productId != null && $productId != 'all') ) { ?>
            <div class="row" style="text-transform: uppercase;line-height: 2.3rem;">
                <div class="col-lg-6">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border relatorioViagem"><i class="fa fa fa-plus-circle"></i>  <?= lang('Relatórios da Viagem') ?> <img class="imgrelatorioViagem" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;" class="divrelatorioViagem">
                            <div class="row">
                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <a href="<?= site_url('agenda/modal_view/'.$programacaoId.'/'.$programacao->produto)?>"
                                       data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"
                                       class="link_relatorio_venda" target="_blank"><i class="fa fa-eye"></i> Visão Geral de Vagas</a>
                                </div>
                                <div class="col-lg-12">
                                    <a href="<?= site_url('reports/relatorioPassageirosProgramacao/'.$programacaoId)?>"
                                       class="link_relatorio_venda" target="_blank"><i class="fa fa-users"></i> Relatório Geral de Passageiros</a>
                                </div>
                                <div class="col-lg-12">
                                    <?php
                                    $total_passageiros = $this->site->getItensVendasFaturadas($programacao->produto, $programacaoId);

                                    if (!$total_passageiros) {
                                        $total_passageiros = 0;
                                    } else {
                                        $total_passageiros = count($total_passageiros);
                                    }

                                    ?>
                                    <a href="<?= site_url('sales/relatorio_geral_passageiros_todos/'.$programacao->produto.'/'.$programacaoId)?>"
                                       class="link_relatorio_venda" target="_blank"><i class="fa fa-print"></i> Lista de Passageiros (<?=$total_passageiros;?>)</a>
                                    </a>
                                </div>
                                <div class="col-lg-12">
                                    <a href="<?= site_url('sales/relatorio_geral_ferroviario/'.$programacao->produto.'/'.$programacaoId)?>"
                                       class="link_relatorio_venda" target="_blank"><i class="fa fa-print"></i> Lista de Passageiros por Atividade (<?=$total_passageiros;?>)
                                    </a>
                                    <a href="<?= site_url('sales/relatorio_geral_ferroviario_excel/' . $programacao->produto.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                        <small><i class="fa fa-file-excel-o"></i> Excel</small>
                                    </a>
                                </div>
                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <?php foreach ($transportes as $transporte) {?>
                                        <?php if ($transporte->status == 'ATIVO'){?>
                                            <h5 style="font-weight: 600;"><?=$transporte->text;?></h5>
                                            <?php
                                            $itens = $this->site->getItensVendasPorLocalEmbarqueApenasAdultosEhCriancas($programacao->produto, $transporte->id, $programacaoId);
                                            $totalVendasItem = 0;
                                            if ($itens) $totalVendasItem = count($itens);
                                            ?>
                                            <div class="col-lg-12" >
                                                <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Lista de Embarque <?='('.$totalVendasItem.')';?>
                                                </a>

                                                <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque_com_cpf/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <small><i class="fa fa-file-pdf-o"></i> com CPF</small>
                                                </a>
                                            </div>
                                            <div class="col-lg-12" >
                                                <a href="<?= site_url('sales/relatorio_enviado_empresa_onibus/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-excel-o"></i> Lista da ANTT <?='('.$totalVendasItem.')';?>
                                                </a>
                                                <a href="<?= site_url('sales/relatorio_enviado_empresa_onibus_excel/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <small><i class="fa fa-file-excel-o"></i> Excel</small>
                                                </a>
                                            </div>
                                            <div class="col-lg-12" >
                                                <a href="<?= site_url('sales/artesp_pdf/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Lista da ARTESP <?='('.$totalVendasItem.')';?>
                                                </a>
                                                <a href="<?= site_url('sales/artesp_excel_cpf/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <small><i class="fa fa-file-excel-o"></i> Excel (com CPF) </small>
                                                </a>
                                            </div>
                                            <div class="col-lg-12" >
                                                <a href="<?= site_url('sales/artesp_pdf_rg/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Lista da ARTESP <?='('.$totalVendasItem.')';?>
                                                </a>
                                                <a href="<?= site_url('sales/artesp_excel_rg/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <small><i class="fa fa-file-excel-o"></i> Excel (com R.G)</small>
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <a href="<?= site_url('sales/relatorio_embarque_assentos/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Relatório de Assentos <?='('.$totalVendasItem.')';?>
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <a href="<?= site_url('sales/relatorio_embarque_assentos_ordem_assentos/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> Ordenado Por Assentos <?='('.$totalVendasItem.')';?>
                                                </a>
                                            </div>
                                            <div class="col-lg-12">
                                                <a href="<?= site_url('sales/relatorio_assinatura_transporte/' . $programacao->produto.'/'.$programacaoId.'/'.$transporte->id) ?>" class="link_relatorio_venda" target="_blank">
                                                    <i class="fa fa-file-pdf-o"></i> <?= lang('captar_assinatura_passageiros_transporte') ?> <?='('.$totalVendasItem.')';?>
                                                </a>
                                            </div>
                                            <div class="col-lg-12" style="margin-bottom: 10px;">
                                                <a href="<?= site_url('sales/pdf_aduana_transporte/'.$programacao->produto.'/'.$programacaoId.'/'.$transporte->id)?>"
                                                   class="link_relatorio_venda" target="_blank"><i class="fa fa-file-pdf-o"></i> Cartão de Entrada/Saída do Mercosul <?='('.$totalVendasItem.')';?>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                </div>

                                <?php $roomlists = $this->Roomlist_model->getAll($programacaoId); ?>

                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <h5>Relatórios de Hospedagem</h5>
                                    <?php foreach ($roomlists as $roomlist) {?>
                                        <div class="col-lg-12">
                                            <a href="<?php echo site_url('roomlist/relatorio/'.$roomlist->id); ?>" class="link_relatorio_venda" target="_blank">
                                                <i class="fa fa-file-pdf-o"></i> <?= lang('print').' Rooming list '.$roomlist->name;?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                    <div class="col-lg-12">
                                        <a href="<?= site_url('sales/pdf_hotel/'.$programacao->produto.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i> <?= lang('relatorio_por_tipo_hospedagem') ?>
                                        </a>
                                    </div>
                                </div>

                                <div class="col-lg-12" style="margin-top: 10px;">
                                    <h5>Outros Relatórios</h5>
                                    <div class="col-lg-12">
                                        <a href="<?= site_url('sales/relatorio_seguradora_de_passageiros/'.$programacao->produto.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i> <?= lang('relatorio_seguradora_de_passageiros') ?>
                                        </a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="<?= site_url('sales/relatorio_assinatura/'.$programacao->produto.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i> <?= lang('captar_assinatura_passageiros') ?>
                                        </a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="<?= site_url('sales/relatorio_autorizacao_imagem/'.$programacao->produto.'/'.$programacaoId) ?>" class="link_relatorio_venda" target="_blank">
                                            <i class="fa fa-file-pdf-o"></i> <?= lang('captar_assinatura_passageiros_autorizacao_imagem') ?>
                                        </a>
                                    </div>
                                    <div class="col-lg-12">
                                        <a href="<?= site_url('sales/pdf_aduana/'.$programacao->produto.'/'.$programacaoId)?>"
                                           class="link_relatorio_venda" target="_blank"><i class="fa fa-file-pdf-o"></i> Cartão de Entrada/Saída do Mercosul </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="col-lg-6">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border configuracaoViagem"><i class="fa fa fa-plus-circle"></i>  <?= lang('Configurações da Viagem') ?> <img class="imgconfiguracaoViagem" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;" class="divconfiguracaoViagem">
                            <div class="row">
                                <?php foreach ($transportes as $transporte) {?>
                                    <?php if ($transporte->status == 'ATIVO'){?>
                                        <div class="col-lg-12">
                                            <a href="<?= site_url('sales/montarPoltronas/' . $programacao->produto.'/'.$transporte->id.'/'.$programacaoId.'?idVariante='.$transporte->id) ?>" class="link_relatorio_venda">
                                                <i class="fa fa fa-cogs"></i> Marcação de Assentos Para <?php echo $transporte->text;?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="col-lg-12">
                                    <a href="<?php echo site_url('roomlist/adicionarRoomList/'.$programacaoId); ?>" data-toggle="modal" class="link_relatorio_venda" data-target="#myModal">
                                        <i class="fa fa-cogs"></i> <?= lang('adicionar_novo_room_list') ?>
                                    </a>
                                </div>
                                <?php if (!empty($roomlist)) {?>
                                    <?php foreach ($roomlists as $roomlist) {?>
                                        <div class="col-lg-12">
                                            <a href="<?php echo site_url('roomlist/montar/'.$roomlist->id); ?>" class="link_relatorio_venda">
                                                <i class="fa fa fa-cogs"></i> <?= lang('Configurar').' Rooming list '.$roomlist->name;?>
                                            </a>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                                <div class="col-lg-12">
                                    <a href="<?php echo site_url('sales/reportAllByProgramacaoId/'.$programacaoId); ?>"   class="link_relatorio_venda" >
                                        <i class="fa fa fa-cogs"></i> Exportar Dados da Venda Para Excel
                                    </a>
                                </div>
                                <div class="col-lg-12">
                                    <a href="<?php echo site_url('sales/export_customer_all_actions/'.$programacaoId); ?>"   class="link_relatorio_venda" >
                                        <i class="fa fa fa-cogs"></i> Exportar Dados dos Clientes Para Excel
                                    </a>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        <?php } else {

            if (!$this->Customer &&
                !$this->Supplier &&
                !$this->Owner &&
                !$this->Admin &&
                !$this->session->userdata('view_right')) {?>
            <?php } ?>
                <?php if ($productId != null && $productId != 'all') {?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="controls">
                                    <a href="<?php echo site_url('sales/reportAllByProgramacaoId/'.$programacaoId); ?>" >
                                        <button type="button" class="btn btn-primary" style="width: 100%;"><i class="fa fa-file-excel-o"></i> Exportar para um arquivo Excel</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
        <?php }?>
        <?php if ( ($this->Owner || $this->Admin) && ($productId != null && $productId != 'all') ) { ?>
            <?php if(!empty($adicionais)) {?>
                <div class="row">
                    <div class="col-lg-12">
                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border adicionais"><i class="fa fa fa-plus-circle"></i>  <?= lang('Relatórios dos Adicionais') ?> <img class="imgadicionais" src="<?= $assets ?>images/abrirSubTitulo-c.gif"></legend>
                            <div style="margin-bottom: 20px;display: none;" class="divadicionais">
                                <div class="row">
                                    <?php foreach ($adicionais as $adicional) {
                                        $totalClientesAdicional = $this->site->getItensVendasTodosPassageiros($adicional->id, $programacaoId);
                                        $totalVendasItem = 0;

                                        if ($totalClientesAdicional) {
                                            $totalVendasItem = count($totalClientesAdicional);
                                        }
                                        ?>
                                        <div class="col-lg-12">
                                            <a href="<?php echo site_url('sales/relatorio_adicional/'.$adicional->id.'/'.$programacaoId); ?>" class="link_relatorio_venda"
                                               target="_blank">
                                                <i class="fa fa-file-excel-o"></i> <?= lang('print').' '.$adicional->name;?> (<?php echo $totalVendasItem;?>)
                                            </a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLDataItem" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th><?php echo $this->lang->line("reference_no"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("biller"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("customer"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("product"); ?></th>
                            <th><?php echo $this->lang->line("sale_status"); ?></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th><?php echo $this->lang->line("payment_status"); ?></th>
                            <th><?php echo $this->lang->line("origem"); ?></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="13" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td></tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?php echo $this->lang->line("grand_total"); ?></th>
                            <th><?php echo $this->lang->line("paid"); ?></th>
                            <th><?php echo $this->lang->line("balance"); ?></th>
                            <th></th>
                            <th></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">

    function createFilter(nameClasse) {
        $('.'+nameClasse).click(function() {
            if ($('.div'+nameClasse).is(':visible')) {
                $('.div'+nameClasse).hide(300);
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

            } else {
                $('.div'+nameClasse).show(300).fadeIn();
                $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
            }
        });
    }

    $(document).ready(function () {

        createFilter('filters');
        createFilter('relatorioViagem');
        createFilter('configuracaoViagem');
        createFilter('adicionais');

        buscarProdutos();

        $('#product').change(function (event) {

            var warehouse = $(this).val() != '' ? $(this).val() : 'all';
            var status = $('#status').val();
            var mes = $('#mes').val();
            var ano = $('#ano').val();

            window.location = '<?= site_url('salesutil/sale_items');?>/'+warehouse+'/'+status+'/'+ano+'/'+mes;
        });

        $("#ano").change(function (event){
            buscarProdutos();
        });

        $("#mes").change(function (event){
            buscarProdutos();
        });

        $("#status").change(function (event){
            buscarProdutos();
        });
    });

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#status').val(),
                ano: $('#ano').val(),
                mes: $('#mes').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {

                $('#product').empty();
                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#product').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#product').append(option);
                });

                $('#product').select2({minimumResultsForSearch: 7});

                <?php if ($productId != null && $productId != 'all') {?>
                $('#product').select2("val", <?php echo $productId;?>);
                <?php } ?>
            }
        });
    }

</script>