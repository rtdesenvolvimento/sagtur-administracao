<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_pacote'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-pacote-form');
        echo form_open_multipart("customers/add", $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-12">

                    <!--Status do pacote!-->
                    <div class="col-md-3">
                        <div class="form-group" id="div_status">
                            <?= lang("situacao_pacote", "status") ?>
                            <?php
                            $opts = array(
                                'sim' => lang('yes'),
                                'nao' => lang('no')
                            );
                            echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                            ?>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-success">
                            <div class="panel-heading"><?= lang('please_select_client_before_adding_product') ?></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("local_embarque", "localEmbarqueId"); ?>
                                        <?php
                                        $aLocaisEmbarque[""] = "";
                                        foreach ($condicoesPagamento as $le) {
                                            $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                        }
                                        echo form_dropdown('localEmbarqueId', $aLocaisEmbarque, (isset($_POST['localEmbarqueId']) ? $_POST['localEmbarqueId'] : $Settings->default_biller), 'id="localEmbarqueId" name="localEmbarqueId" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("local_embarque", "localEmbarqueId"); ?>
                                        <?php
                                        $aLocaisEmbarque[""] = "";
                                        foreach ($condicoesPagamento as $le) {
                                            $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                        }
                                        echo form_dropdown('localEmbarqueId', $aLocaisEmbarque, (isset($_POST['localEmbarqueId']) ? $_POST['localEmbarqueId'] : $Settings->default_biller), 'id="localEmbarqueId" name="localEmbarqueId" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('horaPrevisaoEmbarque', 'horaPrevisaoEmbarque'); ?>
                                        <input type="time" required="required" name="horaPrevisaoEmbarque" value="" class="form-control tip" id="horaPrevisaoEmbarque" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('horaPrevisaoEmbarque', 'horaPrevisaoEmbarque'); ?>
                                        <input type="time" required="required" name="horaPrevisaoEmbarque" value="" class="form-control tip" id="horaPrevisaoEmbarque" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("ADT", "pQtdAdultos"); ?>
                                        <?php echo form_input('pQtdAdultos', (isset($_POST['pQtdAdultos']) ? $_POST['pQtdAdultos'] : '0'), 'class="form-control input-tip" readonly  id="pQtdAdultos"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("CHD", "pQtdCriancas"); ?>
                                        <?php echo form_input('pQtdCriancas', (isset($_POST['pQtdCriancas']) ? $_POST['pQtdCriancas'] : '0'), 'class="form-control input-tip" readonly  id="pQtdCriancas"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("FREE", "pQtdFree"); ?>
                                        <?php echo form_input('pQtdFree', (isset($_POST['pQtdFree']) ? $_POST['pQtdFree'] : '0'), 'class="form-control input-tip" readonly  id="pQtdFree"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("order_discount", "sldiscount"); ?>
                                        <?php echo form_input('order_discount', '0.00', 'class="form-control input-tip mask_money" id="sldiscount"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" id="div_status">
                                        <?= lang("situacao_pacote", "status") ?>
                                        <?php
                                        $opts = array(
                                            'Montando' => lang('status_montando_pacote'),
                                            'Confirmado' => lang('status_confirmado_para_venda'),
                                            'Executado' => lang('status_viagem_executada') ,
                                            'Cancelado' => lang('status_viagem_cancelada')
                                        );
                                        echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="div_status">
                                        <?= lang("situacao_pacote", "status") ?>
                                        <textarea name="product_details" id="product_details" style="width: 100%;"><?php echo (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : ''));?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="div_status">
                                        <?php echo form_submit('add_pacote', lang('copiar_dados'), 'class="btn btn-primary"'); ?>                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!--Status do pacote!-->
                    <div class="col-md-3">
                        <div class="form-group" id="div_status">
                            <?= lang("situacao_pacote", "status") ?>
                            <?php
                            $opts = array(
                                'sim' => lang('yes'),
                                'nao' => lang('no')
                            );
                            echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading"><?= lang('please_select_client_before_adding_product') ?></div>
                            <div class="panel-body" style="padding: 5px;">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("local_embarque", "localEmbarqueId"); ?>
                                        <?php
                                        $aLocaisEmbarque[""] = "";
                                        foreach ($condicoesPagamento as $le) {
                                            $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                        }
                                        echo form_dropdown('localEmbarqueId', $aLocaisEmbarque, (isset($_POST['localEmbarqueId']) ? $_POST['localEmbarqueId'] : $Settings->default_biller), 'id="localEmbarqueId" name="localEmbarqueId" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("local_embarque", "localEmbarqueId"); ?>
                                        <?php
                                        $aLocaisEmbarque[""] = "";
                                        foreach ($condicoesPagamento as $le) {
                                            $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                        }
                                        echo form_dropdown('localEmbarqueId', $aLocaisEmbarque, (isset($_POST['localEmbarqueId']) ? $_POST['localEmbarqueId'] : $Settings->default_biller), 'id="localEmbarqueId" name="localEmbarqueId" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('horaPrevisaoEmbarque', 'horaPrevisaoEmbarque'); ?>
                                        <input type="time" required="required" name="horaPrevisaoEmbarque" value="" class="form-control tip" id="horaPrevisaoEmbarque" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang('horaPrevisaoEmbarque', 'horaPrevisaoEmbarque'); ?>
                                        <input type="time" required="required" name="horaPrevisaoEmbarque" value="" class="form-control tip" id="horaPrevisaoEmbarque" data-original-title="" title="">
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("poltrona", "poltrona"); ?>
                                        <?php echo form_input('poltrona', '0', 'class="form-control input-tip" readonly id="poltrona"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("ADT", "pQtdAdultos"); ?>
                                        <?php echo form_input('pQtdAdultos', (isset($_POST['pQtdAdultos']) ? $_POST['pQtdAdultos'] : '0'), 'class="form-control input-tip" readonly  id="pQtdAdultos"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("CHD", "pQtdCriancas"); ?>
                                        <?php echo form_input('pQtdCriancas', (isset($_POST['pQtdCriancas']) ? $_POST['pQtdCriancas'] : '0'), 'class="form-control input-tip" readonly  id="pQtdCriancas"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("FREE", "pQtdFree"); ?>
                                        <?php echo form_input('pQtdFree', (isset($_POST['pQtdFree']) ? $_POST['pQtdFree'] : '0'), 'class="form-control input-tip" readonly  id="pQtdFree"' ); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("order_discount", "sldiscount"); ?>
                                        <?php echo form_input('order_discount', '0.00', 'class="form-control input-tip mask_money" id="sldiscount"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group" id="div_status">
                                        <?= lang("situacao_pacote", "status") ?>
                                        <?php
                                        $opts = array(
                                            'Montando' => lang('status_montando_pacote'),
                                            'Confirmado' => lang('status_confirmado_para_venda'),
                                            'Executado' => lang('status_viagem_executada') ,
                                            'Cancelado' => lang('status_viagem_cancelada')
                                        );
                                        echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ($product ? $product->unit : '')), 'class="form-control" id="unit" required="required"');
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="div_status">
                                        <?= lang("situacao_pacote", "status") ?>
                                        <textarea name="product_details" id="product_details" style="width: 100%;"><?php echo (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : ''));?></textarea>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group" id="div_status">
                                        <?php echo form_submit('add_pacote', lang('copiar_dados'), 'class="btn btn-primary"'); ?>                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_pacote', lang('add_pacote'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {});
</script>
