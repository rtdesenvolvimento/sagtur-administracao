<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $this->lang->line('purchase') . ' ' . $inv->reference_no; ?></title>
    <meta name="robots" content="noindex, nofollow" />

    <link href="<?php echo $assets ?>styles/style.css" rel="stylesheet">
    <style type="text/css">
        html, body {
            height: 100%;
            background: #FFF;
            font-family: "Segoe UI", "Selawik Light", Tahoma, Verdana, Arial, sans-serif;
        }

        body:before, body:after {
            display: none !important;
        }
        .table th {
            text-align: center;
            padding: 5px;
        }
        .table td {
            padding: 4px;
        }

        strong{
            font-weight: bold;
        }
        h3 {
            font-size: 15px;
            line-height: 15px;
        }
    </style>
</head>

<body>
<div id="wrap">
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <table border="" style="width: 100%;border-collapse:collapse;">
                    <thead>
                    <tr>
                        <td style="text-align: left;width: 20%;border-bottom: 1px solid #0b0b0b;padding: 10px;">
                            <?php
                            $path = base_url() . 'assets/uploads/logos/'.$biller->logo;
                            $logo = file_get_contents($path);
                            $type = pathinfo($path, PATHINFO_EXTENSION);
                            ?>
                            <img  src="data:image/<?php echo $type;?>;base64,<?php echo base64_encode($logo);?>"
                                  alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                        </td>
                        <td style="text-align: left;width: 40%;border-bottom: 1px solid #0b0b0b;margin-left: 20px;padding: 10px;">
                            <?php if ($inv->sale_status == 'orcamento') {?>
                                <h1><?= lang('orcamento'); ?> </h1>
                            <?php } else if ($inv->sale_status == 'lista_espera') { ?>
                                <h1><?= lang('lista_espera'); ?></h1>
                            <?php } else if ($inv->sale_status == 'cancel')  { ?>
                                <h1><?= lang('cancel'); ?></h1>
                            <?php } else { ?>
                                <h1>VOUCHER</h1>
                                <span style="font-size: 16px;color: #000000;font-weight: bold;">Documentação de Viagem</span>
                            <?php } ?>
                        </td>
                        <td style="text-align: center;width: 40%;border-bottom: 1px solid #0b0b0b;margin-left: 20px;padding: 10px;">
                            <div class="col-xs-5">
                                <div class="bold">
                                    <?php $qrimage = $this->sma->qrcode_pdf_sales('link', urlencode(site_url('sales/view/' . $inv->id)), 2, FALSE, $inv->id.'_'.time().'_'.$this->session->userdata('cnpjempresa')); ?>
                                    <img src="data:image/png;base64,<?php echo $qrimage;?>"
                                         alt="<?= $inv->reference_no ?>" class="pull-right"/><br/>

                                    <?= lang('ref'); ?>: <?= $inv->reference_no; ?><br/>
                                    <small>Este número garante a validade deste documento.</small>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </thead>
                </table>
            </div>
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="row well">
                    <div class="col-xs-6">
                        <h2 class="">
                            <?= $biller->company != '-' ? $biller->company .'<br/>'.$biller->name : $biller->name; ?>
                        </h2>
                        <?= $biller->company ? '' : 'Attn: ' . $biller->name; ?>
                        <?php
                        echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state . '<br />' . $biller->country;
                        echo '<p>';
                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("CNPJ") . ": " . $biller->vat_no;
                        }
                        if ($biller->cf1 != '-' && $biller->cf1 != '') {
                            echo '<br>' . lang('CADASTUR') . ': ' . $biller->cf1;
                        }
                        if ($biller->cf2 != '-' && $biller->cf2 != '') {
                            echo '<br>' . lang('bcf2') . ': ' . $biller->cf2;
                        }
                        if ($biller->cf3 != '-' && $biller->cf3 != '') {
                            echo '<br>' . lang('bcf3') . ': ' . $biller->cf3;
                        }
                        if ($biller->cf4 != '-' && $biller->cf4 != '') {
                            echo '<br>' . lang('bcf4') . ': ' . $biller->cf4;
                        }
                        if ($biller->cf5 != '-' && $biller->cf5 != '') {
                            echo '<br>' . lang('bcf5') . ': ' . $biller->cf5;
                        }
                        if ($biller->cf6 != '-' && $biller->cf6 != '') {
                            echo '<br>' . lang('bcf6') . ': ' . $biller->cf6;
                        }
                        echo '</p>';
                        echo lang('tel') . ': ' . $biller->phone . '<br />' . lang('email') . ': ' . $biller->email;
                        ?>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-xs-4">
                        <?php
                        $sexo           =  $customer->sexo;
                        $comprimento    =  '';

                        if ($sexo == 'MASCULINO') {
                            $comprimento = "CONTRATANTE SRº ";
                        } else if ($sexo == 'FEMININO') {
                            $comprimento = "CONTRATANTE SRª ";
                        }
                        ?>
                        <h2 class=""><?= $comprimento.'<br/>'.$customer->name; ?></h2>
                        <?php

                        if ($customer->address) echo $customer->address.' '.$customer->numero.' '.$customer->complemento;
                        if ($customer->bairro) echo " - " . $customer->bairro;
                        if ($customer->city) echo " - " . $customer->city.'/'.$customer->state ;
                        if ($customer->postal_code) echo " - CEP " . $customer->postal_code.' ';

                        echo "<br>" . $customer->country;

                        echo '<p>';
                        if ($customer->vat_no != "-" && $customer->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $customer->vat_no;
                        }
                        if ($customer->cf1 != '-' && $customer->cf1 != '') {
                            echo '<br>' . lang('ccf1') . ': ' . $customer->cf1;
                        }
                        if ($customer->cf2 != '-' && $customer->cf2 != '') {
                            echo '<br>' . lang('ccf2') . ': ' . $customer->cf2;
                        }
                        if ($customer->cf3 != '-' && $customer->cf3 != '') {
                            echo '<br>' . lang('ccf3') . ': ' . $customer->cf3;
                        }
                        if ($customer->cf4 != '-' && $customer->cf4 != '') {
                            echo '<br>' . lang('ccf4') . ': ' . $customer->cf4;
                        }
                        if ($customer->cf5 != '-' && $customer->cf5 != '') {
                            echo '<br>' . lang('ccf5') . ': ' . $customer->cf5;
                        }
                        if ($customer->cf6 != '-' && $customer->cf6 != '') {
                            echo '<br>' . lang('ccf6') . ': ' . $customer->cf6;
                        }
                        echo '</p>';
                        echo lang('tel') . ': ' . $customer->phone . '<br />' . lang('email') . ': ' . $customer->email;
                        ?>
                    </div>
                    <div class="col-xs-6">
                        <span class="bold">Status Venda:</span> <?= lang($inv->sale_status); ?> <br>
                        <span class="bold">Status Pagamento:</span> <?= lang($inv->payment_status); ?> <br>
                        <span class="bold"><?= lang('ref'); ?>:</span>  <?= $inv->reference_no; ?><br/>
                        <span class="bold"><?= lang('meio_divulgacao'); ?>:</span>  <?= $inv->meio_divulgacao_name; ?><br/>

                    </div>
                    <div class="col-xs-4">
                        <span class="bold"><?= lang('data_emissao'); ?>: </span> <?= date('d/m/Y H:i', strtotime($inv->date)); ?> <br>
                        <span class="bold"><?= lang('forma_pagamento'); ?>:</span>  <?=$inv->tipoCobranca ; ?><br>
                        <span class="bold"><?= lang('condicao_pagamento'); ?>:</span> <?=$inv->condicaoPagamento ; ?><br>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped" style="width: 100%;margin-top: -20px;">
                    <tbody>
                    <?php
                    $r = 1;
                    $details = '';
                    $product = '';

                    foreach ($rows as $row):
                        $isAdicional = $row->adicional;
                        $itinerarios = null;

                        $passageiro = $this->site->getCompanyByID($row->customerClient);
                        $product = $this->products_model->getProductByID($row->product_id);

                        $programacao = $this->AgendaViagemRespository_model->getProgramacaoById($row->programacaoId);

                        $details = $row->details;
                        $categoria = $row->nmCategoria;
                        $faixaEtaria = trim($row->faixaNome);
                        $receptivo = 'Receptivo';

                        if ($categoria == 'Hospedagem') $receptivo = 'Hotel';

                        if ($faixaEtaria) $faixaEtaria = ' ['.lang($faixaEtaria).']';

                        $dadosPassageiro = '<span style="font-size: 13px;font-weight: bold;">'.$passageiro->name.$faixaEtaria.'</span>';

                        if ($passageiro->vat_no) {
                            $dadosPassageiro .= '<br/>CPF/CNPJ: '.$passageiro->vat_no;
                        }

                        if ($passageiro->cf1) {
                            if ($passageiro->tipo_documento) {
                                $dadosPassageiro .= '<br/>'.strtoupper(lang($passageiro->tipo_documento)).' '.$passageiro->cf1.' '.$passageiro->cf3;
                            } else  {
                                $dadosPassageiro .= '<br/>R.G: '.$passageiro->cf1.' '.$passageiro->cf3;
                            }
                        }

                        if ($passageiro->cf5) {
                            $dadosPassageiro .= '<br/>TELEFONE: '.$passageiro->cf5;
                        }

                        if ($passageiro->email) {
                            $dadosPassageiro .= '<br/>E-MAIL: '.$passageiro->email;
                        }

                        if ($passageiro->data_aniversario) {

                            list($ano, $mes, $dia) = explode('-', $passageiro->data_aniversario);

                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                            $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                            $dadosPassageiro .= '<br/>NASCIMENTO: '.$this->sma->hrsd($passageiro->data_aniversario).' '.$idade.' anos';
                        }

                        if ($categoria == 'Hospedagem') $dadosClientePoltrona = '<small>Hospede</small><br/><span style="font-size: 13px;font-weight: bold;">'.$dadosPassageiro.'<br/>'.'</span>';
                        else $dadosClientePoltrona = '<small>Passageiro</small><br/>'.$dadosPassageiro.'<br/>';

                        if ($row->product_name) {
                            if ($categoria != 'Transfer' && $categoria != 'Passagem Aérea' && $categoria != 'Hospedagem') {
                                $dadosClientePoltrona .= '<br/><span style="font-size: 13px;font-weight: bold;color: #0a6aa1"> ' . strtoupper($row->product_name) . '</span>';
                            }
                        }

                        if ($row->tipoDestino) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Destino:</span> '.strtolower($row->tipoDestino);
                        if ($row->apresentacaoPassagem) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Apresentação:</span>  <small>'.$row->apresentacaoPassagem.'</small>';
                        if ($row->dataApresentacao && $row->dataApresentacao != '0000-00-00') $dadosClientePoltrona .= '<small> - '.$this->sma->hrsd($row->dataApresentacao).'</small>';
                        if ($row->horaApresentacao && $row->horaApresentacao != '00:00:00') $dadosClientePoltrona .= '<small> ÀS'.$row->horaApresentacao.'</small>';
                        if ($row->numeroBilete) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Bilhete:</span> '.strtoupper($row->numeroBilete);

                        //if ($row->nmFornecedor) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Empresa</span> '.$row->nmFornecedor;
                        if ($row->nmEmpresaManual) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">'.$receptivo.':</span> '.$row->nmEmpresaManual;
                        if ($row->enderecoEmpresa) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Endereço:</span> '.$row->enderecoEmpresa.'<br/>';
                        if ($row->telefoneEmpresa) $dadosClientePoltrona .= '<span style="font-weight: bold;">Tel:</span> '.$row->telefoneEmpresa.'<br/>';
                        if ($row->reserva) $dadosClientePoltrona .= '<br/><span style="font-weight: bold;">Reserva:</span> '.$row->reserva;

                        if ($row->tipoTransporte) {
                            $dadosClientePoltrona .= '<br/><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAABc1QAAXNUB1Qr+dQAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAGQSURBVDhPpZTLK4VBGIc/l1w2khSRHcnafyAb2RALG4nIPUq5lJXLXhZWVjY6K1sbC0UiC0nJ/eSyQGSnlMvz0xxN8833lXOmnub2zm/emfedyQr8pZzhYsgx2FYfdD7hCr4i1v8OV0ACXs2CF+pbuDPcU7+DBM+gN0osn4kt+LaYpF0GVVAJ1UbEtmn1CTY4QlqQhG3YhR3Y89jIgVAZYuQRBg3qT8AMTFv1CG3N9cEaXEBeSq2DRo0xOPbtEjPWztwllECz7HScAeiCk3+KdWJ/CrrP65SYopKJmLJAHgZv0JOhmDw7lFgtlMJwmseUR0WgtPkrY7TSCcAN6wrcu+5n4BnmYQ6mYpg1NhsmACExBUCR1U6KjJ3lvvYBNnqjyoCQmBLxAXIhG85jBEfNsZTM2rzQPaYM9MCVwLrMZIzYInNK1CXQ4w+J6W3K7SdD3DH19SiKstk0J3GdC7odb5bpN4KeShO0OB7v09ev4i11jlibx+rIslmPEtK4/rRx0G+wALoXt8jLVViBenvyBxSDjaTJXB8XAAAAAElFTkSuQmCC">  <span style="font-weight: bold;">Tipo de Transporte:</span> '.$row->tipoTransporte;
                        }

                        if ($row->localEmbarque) {
                            $local = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($row->product_id, $row->localEmbarqueId);

                            $dadosClientePoltrona .= '<br/><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAAAlwSFlzAAAOwwAADsMBx2+oZAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAH/SURBVDhPlZRNSxZRFID9aBFoEkYIgoIUZZLkxkwSEdIwkHhFKlchFhiYaaXmV2DmZ6GiKCpK6UqINoUrF636IW2DfoTPM9yxt0kXc+Dh3jlzzzn3fMzk56STSo6/hgb4DX/Smf89/YDtCrTBBGyCutRSgcU6nA2WXaxzsAGX0nq7j8GzhFEjz28hk9bZHQyGsoyK2D8Nt2tJOnuEYhEs7rkTIhWiW4Xa8O48q3VbA/fHYgof4TL0wMtTrl2O/hDewxSYorp/ZIGnMaiHM2BRb8B1uAYW+Co0wbsQ7AtrNVwJq+cjx0YxRducBx4ch2H4AN7mG+xAH7yCZbAkowFTttuRM43aoQAmwRGw4Do17QtwFwZDEO2SYvDImTXrCM6MYgq7cBNM1ZR1bNDZ8D4/4S3qtgWVe2DXpsF0M2AtPfQG9uETGMxbjIRzsc/ImdGegEUuhplExPjxMxvrUgOdMJ84F03BEjge1ikXvoIdngw8Zi2Fn+Cs2QxtMlAXuBU7t5OtoMK6OYhlUAV+f9ZUo1/gN2ojusHO2hCxDJYginIA38H276pEauAHdME2+Mt5CP41SsKZ7MWRiYyNZs7OmrdR7NbtYGyguBzW199Pc8KhN8x5DtalF7bAsUiKXc6Wi8Gh8xeL3f1vQF+gs83Z9PNsGrFugP0e2FF1fgkLR69FX5LiQfssAAAAAElFTkSuQmCC">  <span style="font-weight: bold;">Embarque:</span> '.$row->localEmbarque;

                            if ($local->note) {
                                $dadosClientePoltrona .= '<br/>'.$local->note;
                            }

                            if ($local->dataEmbarque != null) {
                                if ($local->dataEmbarque != '0000-00-00'){
                                    $dadosClientePoltrona .= ' - <span style="font-weight: bold;">'.$this->sma->hrsd($local->dataEmbarque).'</span>';
                                }
                            }

                            if ($local->horaEmbarque != null &&
                                $local->horaEmbarque != '00:00:00.0'&&
                                $local->horaEmbarque != '00:00:00') {
                                $dadosClientePoltrona .= '  <span style="font-weight: bold;">'.$this->sma->hf($local->horaEmbarque).'</span>';
                            }
                        }

                        if ($row->tipoHospedagem) {
                            $dadosClientePoltrona .= '<br/><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAQABMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7/wDhX/wVc/aa+B/x9+Kfw68SQeB/jl4F+JH/AAW3/bp/Z3u9a+N/7QmgeAvGHwW+BHwut/hO/hLwX8CvCfiG/s73xZY6R/wkmrf2DoOk22tWNrrJ07wqumWt54ut9VtPMvAP/B5F+xV418e+DvBN1+yT+1T4eh8W+LdA8LNrk+ofDHUo9G/t7V7TSF1O60u116K7u4rFrsXNxZ2kpupo4nhtvMmZFbxpP+CDX/BRD9pz9pv/AIKEfG7w1+3l8YP+Cf3gnxf/AMFEP2mvE/wy+GS+EPHN7p/j/wAKa3r+lazov7QHhW68OfFDwlZQ6Z4+tLy302x1KDSkvrxPCitPfSrBbwwdhD/wbJf8FGEmikj/AOC6fxOhkSWORJo/hz8TI5IpEcMkscn/AAvOPZJG4DpJ5iFHUOHQjcAD98vh3/wVo/Zm+Jeg6p4h0fw/8VrC00vx/wDFb4dzQ6p4d0VbmXVfhF8UPGHwo12/jW1165Qadqmt+C9Q1LSBJIt2NJu7L7fb2l99otYSvwr/AGDv+CfP7aHwd/Zm8M/DT4jfCn4na74z8KfEP9oOz1nxR4j0uSx1jxr9q/aI+Kupad4+uYNV1O71BovH2lXtj4ysbq5vL17+w1y1vlvbxLlLqUoA/9k=">  <span style="font-weight: bold;">' . $row->tipoHospedagem . ' </span> ';

                            if ($row->categoriaAcomodacao) $dadosClientePoltrona .=  ' <small>('.$row->categoriaAcomodacao.' - '. $row->regimeAcomodacao .')</small>';

                            if ($row->valorHospedagem > 0) $dadosClientePoltrona .=  ' <small>('.$this->sma->formatMoney($row->valorHospedagem).')</small>';
                        }

                        if ($row->poltronaClient != '') {
                            $dadosClientePoltrona .= '<br/> <img src="'. $assets .'/images/assento.svg" style="height: 24px;width: 24px;" ><span style="padding 3px;background: #999;font-size: 13px;font-weight: bold;color: #0a6aa1">Poltrona: '.$row->poltronaClient.' </span> ';
                        }

                        $labelOriem = 'Origem';
                        $labelDestino = 'Destino';

                        if ($categoria == 'Passagem Aérea') {
                            $labelOriem = 'De';
                            $labelDestino = 'Para';
                        }

                        if ($row->origem) $dadosClientePoltrona .= '<br/><img src="'.base_url().'assets/images/origem.png" style="width: 2%;">  <span style="font-weight: bold;">'.$labelOriem.':</span> '.$row->origem;
                        if ($row->destino) $dadosClientePoltrona .= '<br/><img src="'.base_url().'assets/images/destino.png" style="width: 2%;">  <span style="font-weight: bold;">'.$labelDestino.':</span> '.$row->destino;

                        ?>
                        <?php if (!$isAdicional){?>
                        <tr>
                            <td colspan="6" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border: 1px solid #ffffff;">&nbsp;</td>
                        </tr>
                        <tr>
                            <?php if ($categoria == 'Hospedagem'){?>
                                <td colspan="1" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">
                                    <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/2wBDAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQH/wAARCAAQABMDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD7/wDhX/wVc/aa+B/x9+Kfw68SQeB/jl4F+JH/AAW3/bp/Z3u9a+N/7QmgeAvGHwW+BHwut/hO/hLwX8CvCfiG/s73xZY6R/wkmrf2DoOk22tWNrrJ07wqumWt54ut9VtPMvAP/B5F+xV418e+DvBN1+yT+1T4eh8W+LdA8LNrk+ofDHUo9G/t7V7TSF1O60u116K7u4rFrsXNxZ2kpupo4nhtvMmZFbxpP+CDX/BRD9pz9pv/AIKEfG7w1+3l8YP+Cf3gnxf/AMFEP2mvE/wy+GS+EPHN7p/j/wAKa3r+lazov7QHhW68OfFDwlZQ6Z4+tLy302x1KDSkvrxPCitPfSrBbwwdhD/wbJf8FGEmikj/AOC6fxOhkSWORJo/hz8TI5IpEcMkscn/AAvOPZJG4DpJ5iFHUOHQjcAD98vh3/wVo/Zm+Jeg6p4h0fw/8VrC00vx/wDFb4dzQ6p4d0VbmXVfhF8UPGHwo12/jW1165Qadqmt+C9Q1LSBJIt2NJu7L7fb2l99otYSvwr/AGDv+CfP7aHwd/Zm8M/DT4jfCn4na74z8KfEP9oOz1nxR4j0uSx1jxr9q/aI+Kupad4+uYNV1O71BovH2lXtj4ysbq5vL17+w1y1vlvbxLlLqUoA/9k=">
                                </td>
                                <td colspan="5" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;"><span><?php echo mb_strtoupper($categoria,  mb_internal_encoding());?></span></td>
                            <?php } else if ($categoria == 'Passagem Aérea') { ?>
                                <td colspan="1" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">
                                    <img src="<?php echo base_url();?>assets/images/plane.png" style="width: 25px;">
                                </td>
                                <td colspan="5" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;"><span><?php echo 'DETALHES DO TRECHO AÉREO';?></span></td>
                            <?php } else if ($categoria === 'Transfer') { ?>
                                <td colspan="1" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">
                                    <img src="<?php echo base_url();?>assets/images/transfer.png" style="width: 25px;">
                                </td>
                                <td colspan="5" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;"><span><?php echo 'DETALHES DO TRANSFER';?></span></td>
                            <?php } else { ?>
                                <td colspan="1" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;">
                                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAgY0hSTQAAeiYAAICEAAD6AAAAgOgAAHUwAADqYAAAOpgAABdwnLpRPAAAARhJREFUOE9jYCAMcoBK/gPxHSBmJaycgUEcqCgRiFOBOAUJJwDZh6GG/QLSjVB1MDVpQH48EAsiW5IM1QByATn4IFCfBMxAkA3kGIKsZwHMsEwKDXsD1O8MM8wAyFgFxOuAeA0aXgLkzwdikM0gNcjyIPUrgNgKOczwsUOAknOBuB+I+YnVhEvdHKQgUCbWsHygwnlAPBMJTweyr0EN+w2kVwMxSAxZDUhPLhAzgSzihHqBkti8DzSDC2SYPpI3yDUQ5HpuWBAUUmggKKtxIIdnAJBTBcTlaBgkVgvENUBcgUM+iNiIAQXuASDeAcSixGrCpW4BUhDoUWoYKBmAIuYbEOsMKsNgOeAH0FUUe7ML6s3XQBpvdgIABsedH7i2NpQAAAAASUVORK5CYII=">
                                </td>
                                <td colspan="5" style="text-align: left;color: #000000;font-weight: bold;font-size: 16px;border-top: 1px solid #ffffff;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;"><span><?php echo mb_strtoupper($categoria, mb_internal_encoding());?></span></td>
                            <?php }?>
                        </tr>
                    <?php } else {?>
                        <?php $dadosClientePoltrona = '&nbsp;&nbsp;<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAAQCAYAAAD0xERiAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAABMSURBVDhP1Y5bCgAgCAQ9ujc3hPyotEWKHgOLgjIsCYCZ64aBsgwfNdMmKBGubMaSjKh9uS9TSR/F7t4808zYKutJy1AiBtkKr8pECiubaYd5pl/YAAAAAElFTkSuQmCC"><span> <small style="color: #0a6aa1">'.strtoupper($row->product_name).'</small></span>'; ?>
                    <?php }?>
                        <tr>
                            <?php

                            $labelIda = lang('Saída');
                            $labelVolta = lang('Volta');
                            $horaCheckin = '';
                            $horaCheckOut = '';

                            if ($categoria == 'Hospedagem') {
                                $labelIda = lang('Check-IN');
                                $horaCheckin = '<br/><span style="font-weight: bold;">Entrada às: </span><br/>'.$row->horaCheckin;

                                $labelVolta = '<span style="font-weight: bold;">'.lang('Check-OUT').'</span>';
                                $horaCheckOut = '<br/><span style="font-weight: bold;">Saída às:</span><br/>'.$row->horaCheckOut;
                            }

                            ?>
                            <td style="text-align:center; width:40px; vertical-align:middle;background: #ddd"><?php if (!$isAdicional) echo $r; ?></td>

                            <td style="vertical-align:middle;"> <?= $dadosClientePoltrona; ?></td>

                            <?php if ($categoria === 'Transfer' || $categoria == 'Passagem Aérea') {?>
                                <td style="vertical-align:middle;text-align: right;" colspan="3"><?php if (!$isAdicional) echo '<span style="font-weight: bold;color: #0a6aa1"">Serviço<br/></span>'.strtoupper($row->product_name); ?></td>
                            <?php } else {?>
                                <?php if ($programacao) {?>
                                    <td style="vertical-align:middle;text-align: right;" colspan="3">
                                        <?php if (!$isAdicional) echo '<span style="font-weight: bold;" >'.$labelIda.'</span><br/>'. date('d/m/Y', strtotime($programacao->dataSaida)).'<br/>'.$programacao->horaSaida; ?><br/><br/>
                                        <?php if (!$isAdicional) echo '<span style="font-weight: bold;">'.$labelVolta.'</span><br/>'.date('d/m/Y', strtotime($programacao->dataRetorno)).'<br/>'.$programacao->horaRetorno; ?>
                                    </td>
                                <?php } else { ?>
                                    <td style="vertical-align:middle;text-align: right;" colspan="3">
                                        <?php if (!$isAdicional) echo '<span style="font-weight: bold;" >'.$labelIda.'</span><br/>'.date('d/m/Y', strtotime($row->dateCheckin))?><br/><br/>
                                        <?php if (!$isAdicional) echo '<span style="font-weight: bold;">'.$labelVolta.'</span><br/>'.date('d/m/Y', strtotime($row->horaCheckOut)); ?>
                                    </td>
                                <?php } ?>
                            <?php } ?>
                            <td style="vertical-align:middle; text-align:right; width:110px;">
                                <?php if (!$isAdicional) echo '<span style="font-weight: bold;">'.lang('Desc.').'</span><br/>'.$this->sma->formatMoney($row->discount).'<br/><br/> '.($row->quantity > 1 ? '<span style="font-weight: bold;">Qtd</span><br/>'.(int) $row->quantity.' Passagens <br/> '.$this->sma->formatMoney($row->unit_price).'<br/><br/>': '').' <span style="font-weight: bold;">'.lang('subtotal').'</span><br/>'; ?><?= '<b>'. $this->sma->formatMoney($row->subtotal).'</b>'; ?>
                            </td>                        </tr>
                        <?php if ($row->descricaoServicos) {?>
                        <tr>
                            <td colspan="1" style="font-weight: bold;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;">
                                <img src="<?php echo base_url();?>assets/images/info-icon.png" style="width: 25px;">
                            </td>
                            <td colspan="5" style="font-weight: bold;border-left: 1px solid #ffffff;border-right: 1px solid #ffffff;border-bottom: 1px solid #ffffff;">
                                <?= $row->descricaoServicos;?>
                            </td>
                        </tr>
                    <?php } ?>

                        <?php $itinerarios = $this->sales_model->getAllItinerarioByItem($row->id); ?>

                        <?php if ($itinerarios) { ?>
                        <tr style="">
                            <td></td>
                            <td colspan="5">
                                <table style="width: 100%;">
                                    <?php foreach ($itinerarios as $itinerario) {?>
                                        <tr>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;"><span style="font-weight: bold;">De</span><br><?php echo $itinerario->origem; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;"><span style="font-weight: bold;">Para </span><br><?php echo $itinerario->destino; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Cia </span><br><?php echo $itinerario->companhiaAerea; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Vôo </span><br><?php echo $itinerario->numeroVoo; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Assento </span><br><?php echo $itinerario->assentoVoo; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Partida </span><br><?php echo $this->sma->hrsd($itinerario->dataPartida); ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Embarque </span><br><?php echo $itinerario->horaPartida; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Chegada </span><br><?php echo $this->sma->hrsd($itinerario->dataChegada); ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Desembarque </span><br><?php echo $itinerario->horaChegada; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                    <?php } ?>

                        <?php $transfers = $this->sales_model->getTransferBySaleItem($row->id); ?>

                        <?php if ($transfers) { ?>
                        <tr>
                            <td></td>
                            <td colspan="5">
                                <table style="width: 100%;">
                                    <?php foreach ($transfers as $transfer) {
                                        $refOut = '';

                                        if ($transfer->dataSaidaVooBus) {
                                            $refOut = '<small><br/>Vôo/bus: '.$this->sma->hrsd($transfer->dataSaidaVooBus);

                                            if ($transfer->horaSaidaBooBus) $refOut .= ' - '.$transfer->horaSaidaBooBus;
                                            if ($transfer->referenciaVooBus) $refOut .= ' Ref: '.$transfer->referenciaVooBus;

                                            $refOut .= '</small>';
                                        }
                                        ?>
                                        <tr>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Tipo</span><br><?php echo $transfer->tipoTransfer; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;"><span style="font-weight: bold;">Origem </span><br><?php echo $transfer->origem; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;"><span style="font-weight: bold;">Destino </span><br><?php echo $transfer->destino.$refOut; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Data </span><br><?php echo $this->sma->hrsd($transfer->data); ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;text-align: center;"><span style="font-weight: bold;">Hora </span><br><?php echo $transfer->hora; ?></td>
                                            <td style="border: 1px solid #ffffff;vertical-align: top;"><span style="font-weight: bold;">Ref:. </span><br><?php echo $transfer->referencia; ?></td>
                                        </tr>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                    <?php } ?>
                        <?php
                        if (!$isAdicional) $r++;
                    endforeach;
                    ?>
                    </tbody>
                </table>
            </div>

            <?php
            $arrayProduct = [];
            foreach ($rows as $row){
                $isAdicional = $row->adicional;

                if (!$isAdicional) {
                    $arrayProduct[$row->programacaoId] = $this->products_model->getProductByID($row->product_id);
                }
            }
            ?>
            <?php if ($faturas && $inv->sale_status == 'faturada' && $this->Settings->exibirDadosFaturaVoucher){?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="clearfix"></div>
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                                <thead>
                                <tr><th colspan="6" style="text-align: center;background: #73a9d0;color: #ffffff;font-weight: bold;">DADOS DA FATURA</th></tr>
                                </thead>
                                <thead>
                                <tr>
                                    <th style="text-align: center;width: 5%;">#</th>
                                    <th style="text-align: center;">Vencimento</th>
                                    <th style="text-align: center;">Cobrança</th>
                                    <th style="text-align: right;">Pagar</th>
                                    <th style="text-align: right;">Recebido</th>
                                    <th style="text-align: center;">Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($faturas as $fatura){
                                    $cobranca = $this->financeiro_model->getCobrancaIntegracaoByFatura($fatura->id);
                                    ?>
                                    <tr>
                                        <?php if (  $cobranca->tipo == 'carne_cartao_transparent' ||
                                                    $cobranca->tipo == 'carne_cartao_transparent_mercado_pago' ||
                                                    $cobranca->tipo == 'cartao_credito_transparent_valepay' ||
                                                    $cobranca->tipo == 'link_pagamento') { ?>
                                            <td style="text-align: center;">CC</td>
                                        <?php } else { ?>
                                            <td style="text-align: center;"><?php echo $fatura->numeroparcela.'X';?></td>
                                        <?php } ?>
                                        <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                                        <td style="text-align: center;">
                                            <?php echo $fatura->tipoCobranca;?>
                                            <?php if ($fatura->note_tipo_cobranca){?>
                                                <br/><?php echo $fatura->note_tipo_cobranca;?>
                                            <?php } ?>
                                        </td>
                                        <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                                        <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                                        <td style="text-align: center;">
                                            <?php echo lang($fatura->status);?>

                                            <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                                <?php if ($cobranca->link) {?>
                                                    <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Pagar Agora (Clique Aqui)</a>
                                                <?php } else if ($cobranca->checkoutUrl) {?>
                                                    <br/><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-money"></i> Pagar Agora (Clique Aqui)</a>
                                                <?php } ?>
                                            <?php } ?>


                                        </td>
                                    </tr>
                                <?php }?>
                                </tbody>

                                <tfoot>
                                <?php
                                $col = 5;
                                if ($Settings->product_serial) {
                                    //$col++;
                                }
                                if ( $Settings->product_discount && $inv->product_discount != 0) {
                                    //$col++;
                                }
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    //$col++;
                                }
                                if ( $Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                    //$tcol = $col - 2;
                                } elseif ( $Settings->product_discount && $inv->product_discount != 0) {
                                   /// $tcol = $col - 1;
                                } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                    $tcol = $col - 1;
                                } else {
                                    //$tcol = $col;
                                }
                                ?>
                                <?php if ($inv->grand_total != $inv->total) {
                                    ?>
                                    <tr>
                                        <td colspan="<?= $tcol; ?>" style="text-align:right;"><?= lang('total'); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <?php
                                        if ($Settings->tax1 && $inv->product_tax > 0) {
                                            echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                                        }
                                        if ( $Settings->product_discount && $inv->product_discount != 0) {
                                            echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                                        }
                                        ?>
                                        <td style="text-align:right;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                                    </tr>
                                <?php }
                                ?>
                                <?php if ($return_sale && $return_sale->surcharge != 0) {
                                    echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                                }
                                ?>
                                <?php if ($inv->order_discount != 0) {

                                    if ($inv->cupom_id != null) {

                                        $codigoCupom = $this->CupomDescontoRepository_model->getById($inv->cupom_id )->codigo;

                                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('Cupom Desconto')  . ' (' . $codigoCupom . ')</td><td style="text-align:right;">-' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                                    } else {
                                        echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">-' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                                    }
                                }
                                ?>
                                <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                    echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_tax') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                                }
                                ?>
                                <?php if ($inv->shipping != 0) {
                                    echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                                }
                                ?>
                                <tr>
                                    <td colspan="<?= $col; ?>"
                                        style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                                        (<?= $default_currency->code; ?>)
                                    </td>
                                    <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                                </tr>

                                <tr>
                                    <td colspan="<?= $col; ?>" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                        (<?= $default_currency->code; ?>)
                                    </td>
                                    <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->paid); ?></td>
                                </tr>
                                <tr>
                                    <td colspan="<?= $col; ?>" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                        (<?= $default_currency->code; ?>)
                                    </td>
                                    <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            <?php }?>
            <?php if ($payments && $this->Settings->exibirDadosFaturaVoucher) { ?>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed print-table" style="width: 100%">
                                <thead>
                                <tr>
                                    <th colspan="5" style="background: #73a9d0;color: #ffffff;text-align: center;">DADOS DO PAGAMENTO</th>
                                </tr>
                                <tr>
                                    <th style="background: #73a9d0;color: #ffffff;"><?= lang('date_pagamento') ?></th>
                                    <th style="background: #73a9d0;color: #ffffff;"><?= lang('payment_reference') ?></th>
                                    <th style="background: #73a9d0;color: #ffffff;"><?= lang('paid_by') ?></th>
                                    <th style="background: #73a9d0;color: #ffffff;text-align: right;"><?= lang('amount') ?></th>
                                    <th style="background: #73a9d0;color: #ffffff;"><?= lang('type') ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($payments as $payment) {
                                    if ($payment->status == 'ESTORNO') continue;
                                    ?>
                                    <tr <?= $payment->type == 'returned' ? 'class="warning"' : ''; ?>>
                                        <td style="text-align: center;"><?= date('d/m/Y H:i', strtotime($payment->date)) ?></td>
                                        <td style="text-align: center;"><?= $payment->reference_no; ?></td>
                                        <td style="text-align: center;"><?= lang($payment->paid_by);
                                            if ($payment->paid_by == 'gift_card' || $payment->paid_by == 'CC') {
                                                echo ' (' . $payment->cc_no . ')';
                                            } elseif ($payment->paid_by == 'Cheque') {
                                                echo ' (' . $payment->cheque_no . ')';
                                            }
                                            ?></td>
                                        <td style="text-align: right;"><?= $this->sma->formatMoney($payment->amount + $payment->taxa) . ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_self"> comprovante </a>' : ''); ?></td>
                                        <td style="text-align: center;"><?= lang($payment->type); ?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($inv->local_saida || $inv->local_saida != '') { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang('local_saida'); ?>:</p>

                            <div><?= $this->sma->decode_html( $inv->local_saida); ?></div>
                        </div>
                    <?php }
                    ?>
                </div>

                <!--
                <div class="col-xs-12" style="margin-bottom: 20px;">
                    <div class="well">
                        <div class="btn-group btn-group-justified">
                            <?php if ($inv->attachment) { ?>
                                <div class="btn-group">
                                    <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" style="padding: 5px;" title="<?= lang('attachment') ?>">
                                        <i class="fa fa-chain"></i>
                                        <span class="hidden-sm hidden-xs"><?= 'Esta Venda Possui Anexo - Clique aqui para baixar'?></span>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                !-->
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <?= $Settings->voucherCustomText ? '<div class="well well-sm" style="background: #fffa90;"><div class="panel-heading">' . lang('product_voucher_custom_text') . '</div><div class="panel-body">' . $Settings->voucherCustomText . '</div></div>' : ''; ?>
                    <div class="clearfix"></div>
                    <?php if ($inv->note || $inv->note != '') { ?>
                        <div class="well well-sm"><div class="panel-heading"><?php echo lang('note');?></div><div class="panel-body" style="text-align: justify;"><?php echo $this->sma->decode_html($inv->note);?></div></div>
                    <?php } ?>


                    <?php foreach ($arrayProduct as $product) {
                        $product_details = $this->ProductDetailsRepository_model->getAll($product->id);

                        ?>
                        <?= $product->product_details ? '<div class="well well-sm"><div class="panel-heading">' . lang('product_details').' | '.$product->name . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                        <?= $product->itinerario ? '<div class="well well-sm"><div class="panel-heading">' . lang('Roteiro') . ' | '. $product->name  . '</div><div class="panel-body">' . $product->itinerario . '</div></div>' : ''; ?>
                        <?= $product->oqueInclui ? '<div class="well well-sm"><div class="panel-heading">' . lang('O que inclui') .' | '. $product->name . '</div><div class="panel-body">' . $product->oqueInclui . '</div></div>' : ''; ?>

                        <?php
                        foreach ($adicionais as $adicional) {?>
                            <?php if ($adicional->details){?>
                                <div class="well well-sm">
                                    <div class="panel-heading"><?php echo $adicional->produtc_name;?> </div>
                                    <div class="panel-body">
                                        <?php echo $adicional->details;?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                        <?= $product->valores_condicoes ? '<div class="well well-sm"><div class="panel-heading">' . lang('Valores e condições'). ' | ' . $product->name . '</div><div class="panel-body" style="text-align: justify;">' . $product->valores_condicoes . '</div></div>' : ''; ?>
                        <?= $product->details ? '<div class="well well-sm" style="background: #fffa90;"><div class="panel-heading">' . lang('Notas importantes') . ' | ' . $product->name  . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                        <?php foreach ($product_details as $pDetail) {?>
                            <?= $product->oqueInclui ? '<div class="well well-sm"><div class="panel-heading">' . $pDetail->titulo .' | '. $product->name . '</div><div class="panel-body">' . $pDetail->note . '</div></div>' : ''; ?>
                        <?php } ?>
                    <?php } ?>

                    <?= $Settings->informacoesImportantesVoucher ? '<div class="well well-sm" style="background: #fffa90;"><div class="panel-heading">' . lang('Notas importantes') . '</div><div class="panel-body">' . $Settings->informacoesImportantesVoucher . '</div></div>' : ''; ?>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12">
                    <?= $Settings->contrato ? '<div class="well well-sm"><div class="panel-heading">' . lang('Contrato') . '</div><div class="panel-body" style="text-align: justify;">' . $Settings->contrato . '</div></div>' : ''; ?>
                </div>
                <div class="clearfix"></div>
                <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                    <div class="col-xs-4 pull-right" style="display: none;">
                        <div class="well well-sm">
                            <?=
                            '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                            .'<br>'.
                            lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>