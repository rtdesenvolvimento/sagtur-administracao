<style>
    .estrelai {
        font-size: 24px;
        cursor: pointer;
        color: gray;
    }
    .estrelai:not(.estrela-vazia) {
        color: orange;
    }
</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-2">
                        <?php if ($logo) { ?>
                            <div class="text-center">
                                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                     alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                            </div>
                        <?php } ?>
                    </div>
                    <div class="col-xs-3">
                        <p class="bold" style="padding: 20px;">
                                <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                            <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                            <?php if (!empty($inv->return_sale_ref)) {
                                echo lang("return_ref").': '.$inv->return_sale_ref;
                                if ($inv->return_id) {
                                    echo ' <a data-target="#myModal2" data-toggle="modal" href="'.site_url('salesutil/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                                } else {
                                    echo '<br>';
                                }
                            } ?>
                            <?= lang("sale_status"); ?>: <?= lang($inv->sale_status); ?><br>
                            <?= lang("payment_status"); ?>: <?= lang($inv->payment_status); ?>
                            <?= lang("meio_divulgacao"); ?>: <?= lang($inv->meio_divulgacao_name); ?>
                        </p>
                    </div>
                    <div class="col-xs-7 text-right">
                        <?php $br = $this->sma->save_barcode($inv->reference_no, 'code128', 70, false); ?>
                        <img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png"
                             alt="<?= $inv->reference_no ?>"/>
                        <?php $qrimage = $this->sma->qrcode_pdf_sales('link', urlencode(site_url('sales/view/' . $inv->id)), 3,
                            FALSE, $inv->id.'_'.time().'_'.$this->session->userdata('cnpjempresa')); ?>
                        <img src="data:image/png;base64,<?php echo $qrimage;?>"
                             alt="<?= $inv->reference_no ?>"/>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom:15px;margin-top: -20px;">
                <div class="col-xs-6">
                    <h2 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>

                    <?php

                    if ($biller->address != '') {
                        echo $biller->address . "<br>" . $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                    } else {
                        echo $biller->city . " " . $biller->state. " ". $biller->postal_code  . " " . $biller->country;
                    }

                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }

                    echo "<br>" .lang("tel") . ": " . $biller->phone . " " . lang("email") . ": " . $biller->email;
                    ?>
                </div>
                <div class="col-xs-6">
                    <h2 style="margin-top:10px;"><?= $customer->name; ?></h2>
                    <?php

                    $address_cliente = '';
                    if ($customer->address) $address_cliente .=  $customer->address.' '.$customer->numero.' '.$customer->complemento;
                    if ($customer->bairro) $address_cliente .= " - " . $customer->bairro;
                    if ($customer->city) $address_cliente .= " - " . $customer->city.'/'.$customer->state ;
                    if ($customer->postal_code) $address_cliente .= " CEP " . $customer->postal_code.' ';
                    if ($customer->country) $address_cliente .= $customer->country;

                    if ($address_cliente) {
                        $address_cliente = '<br/>'.$address_cliente;
                    }

                    echo $address_cliente;

                    if ($customer->vat_no != "-" && $customer->vat_no != "") {
                        echo lang("vat_no") . ": " . $customer->vat_no.' <br/>';
                    }
                    if ($customer->cf1 != "-" && $customer->cf1 != "") {
                        echo lang("ccf1") . ": " . $customer->cf1.' <br/>';
                    }
                    if ($customer->cf2 != "-" && $customer->cf2 != "") {
                        echo lang("ccf2") . ": " . $customer->cf2.' <br/>';
                    }
                    if ($customer->cf3 != "-" && $customer->cf3 != "") {
                        echo lang("ccf3") . ": " . $customer->cf3.' <br/>';
                    }
                    if ($customer->cf4 != "-" && $customer->cf4 != "") {
                        echo lang("ccf4") . ": " . $customer->cf4.' <br/>';
                    }
                    if ($customer->cf5 != "-" && $customer->cf5 != "") {
                        echo lang("ccf5") . ": " . $customer->cf5.' <br/>';
                    }
                    if ($customer->cf6 != "-" && $customer->cf6 != "") {
                        echo lang("ccf6") . ": " . $customer->cf6.' <br/>';
                    }

                    echo lang("tel") . ": " . $customer->phone . " " . lang("email") . ": " . $customer->email;
                    ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr>
                        <th><?= lang("nº"); ?></th>
                        <th><?= lang("description"); ?></th>
                        <?php
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            echo '<th>' . lang("tax") . '</th>';
                        }
                        if ($Settings->product_discount && $inv->product_discount != 0) {
                            echo '<th>' . lang("discount") . '</th>';
                        }
                        ?>
                        <th><?= lang("subtotal"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $r = 1;
                    $tax_summary = array();
                    foreach ($rows as $row):

                        $customerClientName     = $row->customerClientName;
                        $poltronaClient         = $row->poltronaClient;
                        $dadosClientePoltrona   = '';
                        $passageiro             = $this->site->getCompanyByID($row->customerClient);

                        if ($customerClientName != '' && $customerClientName != 'undefined' && $customerClientName != '0') {

                            if ($poltronaClient != '') {
                                $dadosClientePoltrona = '<span class="bold">'.$customerClientName.' ['.lang($row->faixaNome).']'. ( $row->quantity > 1 ? '<br/>'.(int)$row->quantity.' Vendas X '.$this->sma->formatMoney($row->unit_price) : '').'</span><br/>Poltrona '.$poltronaClient.'<br/>';
                            } else {
                                $dadosClientePoltrona = '<span class="bold">'.$customerClientName.' ['.lang($row->faixaNome).']'.( $row->quantity > 1 ? '<br/>'.(int)$row->quantity.' Vendas X '.$this->sma->formatMoney($row->unit_price) : '').'</span><br/>';
                            }

                        } else {

                            if ($poltronaClient != '') {
                                $dadosClientePoltrona = '<span class="bold">'.$customer->name.' ['.lang($row->faixaNome).']'.( $row->quantity > 1 ? '<br/>'.(int)$row->quantity.' Vendas X '.$this->sma->formatMoney($row->unit_price) : '').'</span><br/>Poltrona '.$poltronaClient.'<br/>';
                            } else {
                                $dadosClientePoltrona = '<span class="bold">'.$customer->name.' ['.lang($row->faixaNome).']'.( $row->quantity > 1 ? '<br/>'.(int)$row->quantity.' Vendas X '.$this->sma->formatMoney($row->unit_price) : '').'</span><br/>';
                            }
                        }

                        if ($row->note_item) {
                            $dadosClientePoltrona .= $row->note_item.'<br/>';
                        }

                        $programacao        = $this->AgendaViagemRespository_model->getProgramacaoById($row->programacaoId);
                        $dadosPassageiro    = '';
                    ?>
                        <tr>
                            <?php if (!$row->adicional) {?>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">

                                    <?= $dadosClientePoltrona.' <span class="bold" style="color: #428bca;">'.$row->product_name.'</span> saída '.date('d/m/Y', strtotime($programacao->dataSaida)). ' retorno '. date('d/m/Y', strtotime($programacao->dataRetorno)); ?>

                                    <?php
                                        if ($passageiro->vat_no) {
                                            $dadosPassageiro .= '<br/><span class="bold">CPF/CNPJ:</span> '.$passageiro->vat_no;
                                        }

                                        if ($passageiro->cf1) {
                                            if ($passageiro->tipo_documento) {
                                                $dadosPassageiro .= ' <span class="bold">'.strtoupper(lang($passageiro->tipo_documento)).'</span> '.$passageiro->cf1.' '.$passageiro->cf3;
                                            } else  {
                                                $dadosPassageiro .= ' <span class="bold">R.G:</span> '.$passageiro->cf1.' '.$passageiro->cf3;
                                            }
                                        }

                                        if ($passageiro->cf5) {
                                            $dadosPassageiro .= ' <span class="bold"><br/>Telefone:</span> '.$passageiro->cf5;
                                        }

                                        if ($passageiro->email) {
                                            $dadosPassageiro .= ' <span class="bold">E-mail:</span> '.$passageiro->email;
                                        }

                                        if ($passageiro->data_aniversario) {

                                            list($ano, $mes, $dia) = explode('-', $passageiro->data_aniversario);

                                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                            $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                                            $dadosPassageiro .= '<br/><span class="bold">Nascimento:</span> '.$this->sma->hrsd($passageiro->data_aniversario).' '.$idade.' anos';
                                        }

                                        if ($row->tipoHospedagem) {
                                            $dadosPassageiro .= '<br/><i class="fa fa-bed"></i>  <span style="font-weight: bold;"></span> '.$row->tipoHospedagem;

                                            if ($row->categoriaAcomodacao) $dadosPassageiro .=  ' <small>('.$row->categoriaAcomodacao.' - '. $row->regimeAcomodacao .')</small>';

                                            if ($row->valorHospedagem > 0) $dadosPassageiro .=  ' <small>('.$this->sma->formatMoney($row->valorHospedagem).')</small>';
                                        }

                                        if ($row->tipoTransporte) {
                                            $dadosPassageiro .= '<br/><i class="fa fa-bus"></i> <span style="font-weight: bold;">'. $row->tipoTransporte .'</span> ';
                                        }

                                        if ($row->localEmbarque) {
                                            $local = $this->ProdutoRepository_model->getLocalEmbarqueRodoviarioById($row->product_id, $row->localEmbarqueId);

                                            $dadosPassageiro .= '<br/><i class="fa fa-map-marker"></i> <span style="font-weight: bold;">Embarque:</span> '.$row->localEmbarque;

                                            if ($local->note) {
                                                $dadosPassageiro .= '<br/>'.$local->note;
                                            }

                                            if ($local->dataEmbarque != null && $local->dataEmbarque != '0000-00-00'){
                                                $dadosPassageiro .= ' - <span style="font-weight: bold;">'.$this->sma->hrsd($local->dataEmbarque).'</span>';
                                            }

                                            if ($local->horaEmbarque != null &&
                                                $local->horaEmbarque != '00:00:00.0'&&
                                                $local->horaEmbarque != '00:00:00') {
                                                $dadosPassageiro .= '  <span style="font-weight: bold;">'.$this->sma->hf($local->horaEmbarque).'</span>';
                                            }
                                        }
                                        $rating = $this->RatingRepository_model->encontrouAvaliacao($row->sale_id, $row->customerClient); ?>

                                    <?php echo $dadosPassageiro;?>

                                    <?php if (!empty($rating)) {?>
                                        <?php if (!$rating->rating_private){?>
                                            <?php if ($rating->answered) {//respondido?>
                                                <span class="star" rating="<?=$rating->average;?>"></span>
                                            <?php } else { ?>
                                                <br/><span style="margin-top: 2px;">Sem avaliação, <a href="<?=$this->Settings->url_site_domain.'/rating/'.$rating->uuid?>" target="_blank">abrir formulário</a></span>
                                            <?php }?>
                                        <?php } ?>
                                    <?php } ?>

                                </td>
                            <?php } else {
                                $dadosClientePoltrona = ' <img src="'.base_url().'assets/images/plus.png"> <span class="bold">' . $row->product_name . '</span> saída ' . date('d/m/Y', strtotime($programacao->dataSaida)) . ' retorno ' . date('d/m/Y', strtotime($programacao->dataRetorno));
                                ?>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;"><?php echo $dadosClientePoltrona;?></td>
                            <?php }?>

                            <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                        </tr>
                        <?php
                        $r++;
                    endforeach;
                    if ($return_rows) {
                        foreach ($return_rows as $row):
                        ?>
                            <tr class="warning">
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_name . " (" . $row->product_code . ")" . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                </td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->unit; ?></td>
                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                }
                                ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                    }
                    ?>

                    </tbody>
                    <tfoot>
                    <?php
                    $col = 2;
                    if ($Settings->product_discount && $inv->product_discount != 0) {
                        $col++;
                    }
                    if ($Settings->tax1 && $inv->product_tax > 0) {
                        $col++;
                    }
                    if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 2;
                    } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                        $tcol = $col - 1;
                    } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 1;
                    } else {
                        $tcol = $col;
                    }
                    ?>
                    <?php if ($inv->grand_total != $inv->total) { ?>
                        <tr>
                            <td colspan="<?= $tcol; ?>"
                                style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($return_sale) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                        if ($return_sale->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                        }
                    }
                    ?>
                    <?php if ($inv->order_discount != 0) {
                        if ($inv->cupom_id != null) {
                            $codigoCupom = $this->CupomDescontoRepository_model->getById($inv->cupom_id)->codigo;
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("Cupom Desconto") . ' (' . $codigoCupom . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';

                        } else {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                        }
                    }
                    ?>
                    <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                    }
                    ?>
                    <?php if ($inv->shipping != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                    </tr>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid); ?></td>
                    </tr>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid)); ?></td>
                    </tr>
                    </tfoot>
                </table>
            </div>

            <?php if ($faturas){?>
                <div class="clearfix"></div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                        <thead>
                        <tr><th colspan="8" style="text-align: center;color: #ffffff;font-weight: bold;">DADOS DA FATURA</th></tr>
                        </thead>
                        <thead>
                        <tr>
                            <th style="text-align: center;width: 5%;">#</th>
                            <th style="text-align: center;">Vencimento</th>
                            <th style="text-align: center;">Cobrança</th>
                            <th style="text-align: right;">Pagar</th>
                            <th style="text-align: right;">Recebido</th>
                            <th style="text-align: center;">Status</th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($faturas as $fatura) {
                            $parcela = $this->site->getParcelaOneByFatura($fatura->id);
                            $cobranca = $this->site->getCobrancaIntegracaoByFatura($fatura->id);
                            $tipoCobranca = $this->site->getTipoCobrancaById($fatura->tipoCobrancaId);
                            ?>
                            <tr>
                                <td style="text-align: center;"><?php echo $fatura->numeroparcela.'X';?></td>
                                <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                                <td style="text-align: center;">
                                    <?php echo $fatura->tipoCobranca;?>
                                    <br/><?php echo $fatura->reference;?>
                                    <?php if (count($cobranca) > 0 && $tipoCobranca->tipo == 'boleto') {?>
                                        <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                    <?php } ?>
                                </td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                                <td style="text-align: center;">
                                    <?php echo lang($fatura->status);?>
                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL' || $fatura->status == 'QUITADA') {?>
                                        <?php if ($cobranca->link) {?>
                                            <?php if ($fatura->status == 'QUITADA') {?>
                                                <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-check"></i> Ver comprovante de pagamento</a>
                                            <?php } else { ?>
                                                <?php if ($tipoCobranca->tipo == 'boleto' || $tipoCobranca->tipo == 'boleto_pix') {?>
                                                    <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Boleto</a>
                                                <?php } else if ($tipoCobranca->tipo == 'pix') { ?>
                                                    <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Pagar com Pix</a>
                                                <?php } else if ($tipoCobranca->tipo == 'carne' && $tipoCobranca->integracao == 'asaas') {?>
                                                    <br/><a href="<?=base_url().'salesutil/impressao_carne_asaas/'.$cobranca->cod_installment?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Impressão do Carnê</a>
                                                    <br/><br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Fatura p/ Pagamento</a>
                                                <?php } else { ?>
                                                    <br/><a href="<?php echo $cobranca->link?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Fatura p/ Pagamento</a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php  } else if ($cobranca->checkoutUrl) {?>
                                            <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                                <?php if (  ($cobranca->integracao == 'pagseguro' && $tipoCobranca->tipo == 'carne_cartao') ||
                                                    ($cobranca->integracao == 'mercadopago' && $tipoCobranca->tipo == 'link_pagamento') ||
                                                    ($cobranca->integracao == 'valepay' && $tipoCobranca->tipo == 'link_pagamento')) {?>
                                                    <?php if ($cobranca->integracao == 'valepay' && $tipoCobranca->tipo == 'link_pagamento') {?>
                                                        <br/><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-barcode"></i> Ver Link de Pagamento</a>
                                                    <?php } else {?>
                                                        <br/>
                                                        <span>Copie o Link Abaixo:<br/></span>
                                                        <span><?php echo $cobranca->checkoutUrl?></span>
                                                    <?php } ?>
                                                <?php } else {?>
                                                    <br/><a href="<?php echo $cobranca->checkoutUrl?>" target="<?php echo $fatura->id;?>"><i class="fa fa-money"></i> Pagar Agora</a>
                                                <?php } ?>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </td>
                                <td style="text-align: center;width: 3%;">
                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                        <a href="<?php echo base_url();?>faturas/adicionarPagamento/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false" class="tip" title="<?= lang('add_payment_fatura') ?>"><span class="fa fa-usd"></span> Pagar</a>
                                    <?php } else { ?>
                                        -
                                    <?php  } ?>
                                </td>
                                <td style="text-align: center;width: 3%;">
                                    <?php if ($fatura->status == 'ABERTA' || $fatura->status == 'PARCIAL') {?>
                                        <a href="<?php echo base_url();?>faturas/editarFatura/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false" class="tip" title="<?= lang('edit_fatura') ?>"><span class="fa fa-edit"></span> Editar</a>
                                    <?php } else { ?>
                                        -
                                    <?php  } ?>
                                </td>
                            </tr>
                        <?php }?>
                        </tbody>

                        <tfoot>
                        <?php
                        $col = 7;
                        if ($Settings->product_serial) {
                            $col++;
                        }
                        if ( $Settings->product_discount && $inv->product_discount != 0) {
                            $col++;
                        }
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            $col++;
                        }
                        if ( $Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 2;
                        } elseif ( $Settings->product_discount && $inv->product_discount != 0) {
                            $tcol = $col - 1;
                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                            $tcol = $col - 1;
                        } else {
                            $tcol = $col;
                        }
                        ?>
                        <?php if ($inv->grand_total != $inv->total) {
                            ?>
                            <tr>
                                <td colspan="<?= $tcol; ?>" style="text-align:right;"><?= lang('total'); ?>
                                    (<?= $default_currency->code; ?>)
                                </td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                                }
                                if ( $Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                                }
                                ?>
                                <td style="text-align:right;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                            </tr>
                        <?php }
                        ?>
                        <?php if ($return_sale && $return_sale->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                        }
                        ?>
                        <?php if ($inv->order_discount != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                        }
                        ?>
                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_tax') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                        }
                        ?>
                        <?php if ($inv->shipping != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                        }
                        ?>
                        </tfoot>
                    </table>

                    <table class="table table-bordered table-hover table-striped" style="width: 100%;margin-top: -20px;">
                        <tfoot>
                        <tr>
                            <td colspan="5"
                                style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                        </tr>

                        <tr>
                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->paid); ?></td>
                        </tr>
                        <tr>
                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            <?php }?>

            <?php if ($payments) { ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-condensed print-table">
                        <thead>
                        <tr><th colspan="9" style="text-align: center;color: #ffffff;font-weight: bold;">PAGAMENTOS</th></tr>
                        </thead>
                        <thead>
                        <tr>
                            <th><?= lang('date') ?></th>
                            <th><?= lang('payment_reference') ?></th>
                            <th><?= lang('paid_by') ?></th>
                            <th><?= lang('amount') ?></th>
                            <th><?= lang('created_by') ?></th>
                            <th><?= lang('type') ?></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                            <th style="text-align: center;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($payments as $payment) {

                            $parcela = $this->site->getParcelaOneByFatura($payment->fatura);
                            $cobranca = $this->site->getCobrancaIntegracaoByFatura($payment->fatura);
                            ?>
                            <tr <?= $payment->status == 'ESTORNO' ? 'class="warning" style="text-decoration: line-through;"' : ''; ?>>
                                <td><?= $this->sma->hrld($payment->date) ?><?= $payment->motivo_estorno; ?></td>
                                <td><?= $payment->reference_no.'<br/>Parcela paga '.$parcela->numeroparcela.'/'.$parcela->totalParcelas; ?></td>
                                <td><?= lang($payment->paid_by);
                                    if ($payment->paid_by == 'gift_card' || $payment->paid_by == 'CC') {
                                        echo ' (' . $payment->cc_no . ')';
                                    } elseif ($payment->paid_by == 'Cheque') {
                                        echo ' (' . $payment->cheque_no . ')';
                                    }
                                    ?>

                                    <?php if (count($cobranca) > 0) {?>
                                        <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                    <?php } ?>

                                    <?php if ($payment->note){?>
                                        <br/><small><?=$payment->note;?></small>
                                    <?php } ?>
                                </td>
                                <td><?= $this->sma->formatMoney($payment->amount + $payment->taxa) . ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_blank"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                                <td><?= lang($payment->type); ?></td>
                                <td style="text-align: center;"><a href="<?php echo base_url();?>sales/payment_note/<?php echo $payment->payment_id;?>" target="<?php echo $payment->payment_id;?>" class="tip" title="<?= lang('payment_print_note') ?>"><span class="fa fa-eye no-print"></span></a></td>
                                <td style="text-align: center;"><a href="<?php echo base_url();?>sales/payment_pdf/<?php echo $payment->payment_id;?>" class="tip" title="<?= lang('payment_download_payment') ?>"><span class="fa fa-download no-print"></span></a></td>
                                <?php if(!$payment->status == 'ESTORNO') {?>
                                    <td style="text-align: center;"><a href="<?= site_url('financeiro/motivoEstorno/' . $payment->payment_id) ?>" class="tip" title="<?= lang('estonar_payment') ?>" data-toggle="modal" data-target="#myModal4"><span class="fa fa-trash-o no-print"></span></a></td>
                                <?php } else { ?>
                                    <td style="text-align: center;">-</td>
                                <?php }?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php } ?>

            <?php if(!empty($itensComissao)) {?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped print-table order-table">
                        <thead>
                        <tr><th colspan="8" style="text-align: center;color: #ffffff;font-weight: bold;">COMISSÕES PENDENTES</th></tr>
                        <thead>
                        <tr>
                            <th><?= lang("nº"); ?></th>
                            <th style="text-align: left;"><?= lang("biller"); ?></th>
                            <th style="text-align: left;"><?= lang("customer"); ?></th>
                            <th style="text-align: left;"><?= lang("product"); ?></th>
                            <th style="text-align: right;"><?= lang("base_value"); ?></th>
                            <th style="text-align: right;"><?= lang("%"); ?></th>
                            <th style="text-align: right;"><?= lang("commission"); ?></th>
                            <th style="text-align: center;"><?= lang("situacao"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $contador = 1;
                        $totalComissao = 0;
                        foreach ($itensComissao as $item) {
                            $totalComissao = $totalComissao + $item->commission;
                            ?>
                            <tr>
                                <td><?= $contador; ?></td>
                                <td><?= $item->biller; ?></td>
                                <td><?= $item->customer; ?></td>
                                <td><?= $item->product_name; ?></td>
                                <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($item->base_value); ?></td>
                                <td style="text-align: right;"><?= $item->commission_percentage.'%'; ?></td>
                                <td style="text-align: right;"><?= $this->sma->formatMoney($item->commission); ?></td>
                                <td style="width: 10%;text-align: center;"><?= lang($item->status); ?></td>
                            </tr>
                            <?php $contador++; } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="4" style="text-align: right;font-weight:bold;"><?= lang("total_commission"); ?>(<?= $default_currency->code; ?>)</td>
                            <td colspan="4" style="text-align: right;right;font-weight:bold;"><?= $this->sma->formatMoney($totalComissao); ?></td>
                        </tfoot>
                    </table>
                </div>
            <?php } ?>

            <?php if(!empty($itensFechamento)) {?>
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">
                    <thead>
                    <tr><th colspan="13" style="text-align: center;color: #ffffff;font-weight: bold;">COMISSÕES INSERIDAS NO FECHAMENTO</th></tr>
                    <thead>
                    <tr>
                        <th><?= lang("nº"); ?></th>
                        <th style="text-align: left;"><?= lang("biller"); ?></th>
                        <th style="text-align: left;"><?= lang("customer"); ?></th>
                        <th style="text-align: left;"><?= lang("product"); ?></th>
                        <th style="text-align: right;"><?= lang("base_value"); ?></th>
                        <th style="text-align: right;"><?= lang("%"); ?></th>
                        <th style="text-align: right;"><?= lang("commission"); ?></th>
                        <th style="text-align: right;"><?= lang("paid"); ?></th>
                        <th style="text-align: right;"><?= lang("balance"); ?></th>
                        <th style="text-align: center;"><?= lang("situacao"); ?></th>

                        <?php
                        if (!$this->Customer &&
                        !$this->Supplier &&
                        !$this->Owner &&
                        !$this->Admin &&
                        !$this->session->userdata('view_right')) {?>
                            <th style="min-width:30px; width: 30px; text-align: center;" colspan="3"><i class="fa fa-print"></i></th>
                        <?php } else { ?>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-print"></i></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-user"></i></th>
                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-cogs"></i></th>
                        <?php } ?>
                    </tr>
                    </thead>
                    <tbody>
                        <?php
                        $contador = 1;
                        $totalComissao = 0;
                        foreach ($itensFechamento as $item) {
                            $totalComissao = $totalComissao + $item->commission;
                            ?>
                        <tr>
                            <td><?= $contador; ?></td>
                            <td><?= $item->biller; ?></td>
                            <td><?= $item->customer; ?></td>
                            <td><?= $item->product_name; ?></td>
                            <td style="width: 10%;text-align: right;"><?= $this->sma->formatMoney($item->base_value); ?></td>
                            <td style="text-align: right;"><?= $item->commission_percentage.'%'; ?></td>
                            <td style="text-align: right;"><?= $this->sma->formatMoney($item->commission); ?></td>
                            <td style="text-align: right;"><?= $this->sma->formatMoney($item->paid); ?></td>
                            <td style="text-align: right;"><?= $this->sma->formatMoney($item->commission-$item->paid); ?></td>
                            <td style="width: 10%;text-align: center;"><?= lang($item->status); ?></td>
                            <?php
                            if (!$this->Customer &&
                                !$this->Supplier &&
                                !$this->Owner &&
                                !$this->Admin &&
                                !$this->session->userdata('view_right')) {?>
                                <td style="text-align: center;" colspan="3">
                                    <a href="<?= site_url('commissions/report_biller/'.$fechamento->id.'/'.$inv->biller_id) ?>" target="_blank" class="tip" title="<?= lang('report_commissions_biller') ?>">
                                        <i class="fa fa-print"></i>
                                    </a>
                                </td>
                            <?php } else {?>
                                <td style="text-align: center;">
                                    <a href="<?= site_url('commissions/report/'.$fechamento->id.'/0/0/portrait') ?>" target="_blank" class="tip" title="<?= lang('report_commissions') ?>">
                                        <i class="fa fa-print"></i>
                                    </a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="<?= site_url('commissions/report_biller/'.$fechamento->id.'/'.$inv->biller_id) ?>" target="_blank" class="tip" title="<?= lang('report_commissions_biller') ?>">
                                        <i class="fa fa-user"></i>
                                    </a>
                                </td>
                                <td style="text-align: center;">
                                    <a href="<?= site_url('commissions/montar_fechamento/'.$fechamento->id) ?>" target="_blank" class="tip" title="<?= lang('montar_fechamento') ?>">
                                        <i class="fa fa-cogs"></i>
                                    </a>
                                </td>
                            <?php } ?>
                        </tr>
                        <?php $contador++; } ?>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="6" style="text-align: right;font-weight:bold;"><?= lang("total_commission"); ?>(<?= $default_currency->code; ?>)</td>
                        <td colspan="7" style="text-align: right;right;font-weight:bold;"><?= $this->sma->formatMoney($totalComissao); ?></td>
                    </tfoot>
                </table>
            </div>
            <?php } ?>

            <?php
            if ($inv->note || $inv->note != "") { ?>
                <div class="well well-sm">
                    <p class="bold"><?= lang("note"); ?>:</p>
                    <div><?= $this->sma->decode_html($inv->note); ?></div>
                </div>
                <?php
            }
            if ($inv->staff_note || $inv->staff_note != "") { ?>
                <div class="well well-sm staff_note">
                    <p class="bold"><?= lang("staff_note"); ?>:</p>
                    <div><?= $this->sma->decode_html($inv->staff_note); ?></div>
                </div>
            <?php } ?>

            <div class="row">
                <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                <div class="col-xs-5 pull-left">
                    <div class="well well-sm">
                        <?=
                        '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                        .'<br>'.
                        lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                    </div>
                </div>
                <?php } ?>
                <?php if ($Settings->informacoesImportantesVoucher){?>
                    <div class="col-xs-6 pull-left">
                        <div class="well well-sm">
                            <p><?php echo $Settings->informacoesImportantesVoucher;?></p>
                        </div>
                    </div>
                <?php } ?>
                <div class="col-xs-6 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                        <?php if ($inv->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="buttons">
                    <div class="btn-group btn-group-justified">
                        <?php if ($inv->attachment) { ?>
                            <div class="btn-group">
                                <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                                    <i class="fa fa-chain"></i>
                                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="btn-group">
                            <a href="<?= site_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-primary" title="<?= lang('email') ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= site_url('salesutil/pdf/' . $inv->id) ?>" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                            </a>
                        </div>
                        <?php if ( ! $inv->sale_id) { ?>
                        <div class="btn-group">
                            <a href="<?= site_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-warning sledit" title="<?= lang('edit') ?>">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();

        $('.star').each(function() {
            let rating = $(this).attr('rating'); // Remove 'star' do início do ID
            let ratingHTML = criarRatingStar(rating);
            $(this).html(ratingHTML);
        });
    });

    function criarRatingStar(i) {
        let star = '';

        i = Math.max(0, Math.min(5, i));

        for (let j = 0; j < i; j++) {
            star += '<span class="estrelai">★</span>';
        }

        for (let j = i; j < 5; j++) {
            star += '<span class="estrelai estrela-vazia">★</span>';
        }

        return '<div>' + star + '</div>';
    }

</script>
