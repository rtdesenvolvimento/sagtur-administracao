<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="urn:layr:template">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Recibo</title>
    <link href="<?= $assets ?>styles/recibo.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
</head>
<body style="background-color: white;">
<div id="non-printable" class="actions-div">
    <button class="btn" style="cursor: pointer;" onclick="window.close();">Cancelar</button>
    <a href="<?php echo base_url().'sales/payment_pdf/'.$payment->id;?>"><button class="btn" style="cursor: pointer;">BAIXAR EM PDF</button></a>
    <button class="btn btn-primary print-btn" style="cursor: pointer;" onclick="window.print();">Imprimir Recibo</button>
</div>
<div id="printable" class="receipt-container">
    <hr class="solid-line">
    <div class="receipt-info-container">
        <div class="container-company-logo">
            <table class="company-logo" cellpadding="0" cellspacing="0" border="0">
                <tbody>
                <tr>
                    <td>
                        <?php if ($Settings->logo) { ?>
                            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                 alt="<?= $this->Settings->site_name ?>"
                                 class="company-logo" style="height: auto;" id="act-logo-topo-novo" align="middle">
                        <?php } else { ?>
                            <img src="<?= $assets ?>/images/image-logo-padrao.png" class="company-logo" id="act-logo-topo-novo" align="middle">
                        <?php }?>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="receipt-info-column-1">
            <div><span style="font-size: 15px;"><strong><?php echo $this->Settings->site_name;?></strong></span></div>
            <div class="company-info-container">
                <strong>CNPJ: <?php echo $biller->vat_no;?> </strong>
            </div>
            <div class="company-address-container">
                <?php echo $biller->phone?> - <?php echo $biller->email?>
                <br/>
                <?php  echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state?>
            </div>
        </div>
        <div class="receipt-info-column-2">
            <span style="font-size: 16px;"><strong></strong></span>
            <br>
            <br>
        </div>
    </div>
    <hr class="solid-line">
    <table style="width: 100%; padding: 30px 0px 5px;">
        <tbody>
        <tr>
            <td style="flex: 1; font-size: 30px;"><strong><span class="condensed">RECIBO N° <?=$payment->reference_no	?></span></strong></td>
            <td style="flex: 1; text-align: right;"><span style="font-size: 38px;" class="condensed semi-bold"><span
                            style="font-size: 28px;" class="gray condensed semi-bold">R$</span> <?php echo $this->sma->formatNumber($payment->amount + $payment->taxa);?></span></td>
        </tr>
        </tbody>
    </table>
    <hr class="dotted-line">
    <div class="receipt-detail-container">

        <?php

        $tipoOperacao = '';
        if ($payment->tipoOperacao == Financeiro_model::TIPO_OPERACAO_CREDITO) {
            $tipoOperacao = 'Recebi(emos) de ';
        } else {
            $tipoOperacao = 'Pagamos para ';
        }
        $itens =  $this->sales_model->getSaleItemByContaReceber($fatura->contas_receber);


        ?>
        <div class="receipt-details"><?php echo $tipoOperacao?> <strong><?php echo $customer->name;?>  - <?php echo $customer->vat_no;?></strong>, a
            importância de <strong><?php echo $this->sma->formatMoney($payment->amount + $payment->taxa);?> (<?php echo  trim($this->sma->extenso($payment->amount + $payment->taxa));?>)</strong>
            pago em <strong><?php echo lang($payment->paid_by);?></strong>, referente a <strong>Fatura <?php echo $fatura->reference;?>
                pagamento da parcela <?php echo $parcela->numeroparcela.'/'.$parcela->totalParcelas;?></strong> com pagamento na data <strong><?php echo $this->sma->hrld($payment->date);?></strong>.
            <?php foreach ($itens as $item){?>
                Este recibo se refere a contratação do serviço:  <strong><?php echo ' '.$item->product_name;?></strong>.
            <?php }?>
            <br>Para maior clareza
            firmo(amos) o presente.
        </div>
        <div class="receipt-date"><?php echo $biller->city.', '.$this->sma->dataDeHojePorExtensoRetorno();?>.</div>
        <?php if ($Settings->assinatura){?>
        <div style="padding: 5px;"> <img src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->assinatura ?>"/></div>
        __________________________________________________
        <?php } else { ?>
            __________________________________________________
        <?php }?>
        <div class="receipt-company">
            <?= $this->session->userdata('nome_completo'); ?><br>
        </div>
    </div>
</div>


</body>
</html>