<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <?= lang("hotel_consulta", "slhote"); ?>
            <?php echo form_input('hotel', (isset($_POST['hotel']) ? $_POST['hotel'] : ""), 'class="form-control" data-placeholder="' . lang("select") . '" id="slhotel"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("emissaoHospedagem", "slEmissaoHospedagem"); ?>
            <?php echo form_input('emissaoHospedagem', (isset($_POST['emissaoHospedagem']) ? $_POST['emissaoHospedagem'] : ""), 'class="form-control input-tip" id="slEmissaoHospedagem"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <?= lang("hotelManual", "hotelManual"); ?>
            <?php echo form_input('hotelManual', (isset($_POST['hotelManual']) ? $_POST['hotelManual'] : ""), 'class="form-control buscar_hoteis"  placeholder="Insira uma cidade ou o nome do hotel" id="hotelManual"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("telefoneHotel", "telefoneHotel"); ?>
            <?php echo form_input('telefoneHotel', (isset($_POST['telefoneHotel']) ? $_POST['telefoneHotel'] : ""), 'class="form-control" id="telefoneHotel"'); ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group">
            <?= lang("enderecoHotel", "enderecoHotel"); ?>
            <?php echo form_input('enderecoHotel', (isset($_POST['enderecoHotel']) ? $_POST['enderecoHotel'] : ""), 'class="form-control"  placeholder="Endereço do hotel" id="enderecoHotel"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("dateCheckinHospedagem", "slDateCheckinHospedagem"); ?>
            <?php echo form_input('dateCheckinHospedagem', (isset($_POST['dateCheckinHospedagem']) ? $_POST['dateCheckinHospedagem'] : ""), 'class="form-control input-tip" id="slDateCheckinHospedagem"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("horaCheckinHospedagem", "slHoraCheckinHospedagem"); ?>
            <?php echo form_input('horaCheckinHospedagem', (isset($_POST['horaCheckinHospedagem']) ? $_POST['horaCheckinHospedagem'] : ""), 'class="form-control input-tip" id="slHoraCheckinHospedagem"', 'time'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("dateCheckoutHospedagem", "slDateCheckoutHospedagem"); ?>
            <?php echo form_input('dateCheckoutHospedagem', (isset($_POST['dateCheckoutHospedagem']) ? $_POST['dateCheckoutHospedagem'] : ""), 'class="form-control input-tip" id="slDateCheckoutHospedagem"', 'date'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("horaCheckoutHospedagem", "slHoraCheckoutHospedagem"); ?>
            <?php echo form_input('horaCheckoutHospedagem', (isset($_POST['horaCheckoutHospedagem']) ? $_POST['horaCheckoutHospedagem'] : ""), 'class="form-control input-tip" id="slHoraCheckoutHospedagem"', 'time'); ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <?= lang("acomodacaoHospedagem", "slAcomodacaoHospedagem"); ?>
            <?php
            $cbTipoHospedagem[''] = '';
            foreach ($tiposHospedagem as $tipoHospedagem) {
                $cbTipoHospedagem[$tipoHospedagem->id] = $tipoHospedagem->name;
            }
            echo form_dropdown('tipoHospedagem', $cbTipoHospedagem, (isset($_POST['tipoHospedagem']) ? $_POST['tipoHospedagem'] : ''), 'id="tipoHospedagem" data-placeholder="' . lang("select") . ' ' . lang("acomodacao") . '" class="form-control input-tip select" style="width:100%;"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= lang("categoriaAcomodacaoHospedagem", "slCategoriaAcomodacaoHospedagem"); ?>
            <?php
            $cbCategoria = array(
                '' => lang('select'),
                'Albergue' => lang('Albergue'),
                'Bangalô' => lang('Bangalô') ,
                'Bed & Breakfest' => lang('Bed & Breakfest'),
                'Chalé' => lang('Chalé'),
                'Luxo' => lang('Luxo'),
                'Standard' => lang('Standard'),
            );
            echo form_dropdown('categoriaAcomodacaoHospedagem', $cbCategoria, (isset($_POST['categoriaAcomodacaoHospedagem']) ? $_POST['categoriaAcomodacaoHospedagem'] : ''), 'id="slCategoriaAcomodacaoHospedagem" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
            ?>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <?= lang("acomodacaoRegimeCategoria", "slAcomodacaoRegimeCategoria"); ?>
            <?php
            $cbCategoria = array(
                '' => lang('select'),
                'All Inclusive' => lang('All Inclusive'),
                'Café da Manhã' => lang('Café da Manhã') ,
                'Meia Pensão' => lang('Meia Pensão'),
                'Pensão Completa' => lang('Pensão Completa'),
            );
            echo form_dropdown('acomodacaoRegimeCategoria', $cbCategoria, (isset($_POST['acomodacaoRegimeCategoria']) ? $_POST['acomodacaoRegimeCategoria'] : ''), 'id="slAcomodacaoRegimeCategoria" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip select" style="width:100%;"');
            ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <?= lang("operadora", "slSupplierHospedagem"); ?>
            <?php echo form_input('supplierHospedagem', (isset($_POST['supplierHospedagem']) ? $_POST['supplierHospedagem'] : ""), 'class="form-control" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" id="slSupplierHospedagem"'); ?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <?= lang("reservaHospedagem", "slReservaHospedagem"); ?>
            <?php echo form_input('reservaHospedagem', (isset($_POST['reservaHospedagem']) ? $_POST['reservaHospedagem'] : ""), 'class="form-control" id="slReservaHospedagem"'); ?>
        </div>
    </div>
</div>
<div id="load-new-hospedagem"></div>
<div class="clearfix"></div>
<div class="row" style="margin-top: 10px;">
    <div class="col-md-12">
        <div class="fprom-group">
            <button type="button" class="btn btn-primary" id="adicionar-hospedagem"><?= lang('adicionar_hospedagem');?></button>
        </div>
    </div>
</div>

