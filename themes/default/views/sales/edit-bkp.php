<script type="text/javascript">
    var count = 1, an = 1, product_variant = 0, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    //var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    //var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    $(document).ready(function () {

        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }

        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        <?php if ($inv) { ?>
            localStorage.setItem('sldate', '<?= $this->sma->hrld($inv->date) ?>');
            localStorage.setItem('slcustomer', '<?= $inv->customer_id ?>');
            localStorage.setItem('slbiller', '<?= $inv->biller_id ?>');
            localStorage.setItem('slref', '<?= $inv->reference_no ?>');
            localStorage.setItem('slwarehouse', '<?= $inv->warehouse_id ?>');
            localStorage.setItem('slsale_status', '<?= $inv->sale_status ?>');
            localStorage.setItem('slpayment_status', '<?= $inv->payment_status ?>');
            localStorage.setItem('slpayment_term', '<?= $inv->payment_term ?>');
            localStorage.setItem('slnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->note)); ?>');
            localStorage.setItem('slinnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($inv->staff_note)); ?>');
            localStorage.setItem('sldiscount', '<?= $inv->order_discount_id ?>');
            localStorage.setItem('sltax2', '<?= $inv->order_tax_id ?>');
            localStorage.setItem('slshipping', '<?= $inv->shipping ?>');
            localStorage.setItem('slitems', JSON.stringify(<?= $inv_items; ?>));
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
            $(document).on('change', '#sldate', function (e) {
                localStorage.setItem('sldate', $(this).val());
            });
            if (sldate = localStorage.getItem('sldate')) {
                $('#sldate').val(sldate);
            }
            $(document).on('change', '#slbiller', function (e) {
                localStorage.setItem('slbiller', $(this).val());
            });
            if (slbiller = localStorage.getItem('slbiller')) {
                $('#slbiller').val(slbiller);
            }
        <?php } ?>

        ItemnTotals();

        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#slcustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= site_url('sales/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#slwarehouse").val(),
                        customer_id: $("#slcustomer").val()
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 200,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });

        $(window).bind('beforeunload', function (e) {
            localStorage.setItem('remove_slls', true);
            if (count > 1) {
                var message = "You will loss data!";
                return message;
            }
        });

        $('#reset').click(function (e) {
            $(window).unbind('beforeunload');
        });

        $('#edit_sale').click(function () {
            $(window).unbind('beforeunload');

            $('form.edit-so-form').submit();
        });


    });
</script>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-plus"></i><?= lang('edit_sale'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-so-form');
                echo form_open_multipart("sales/edit/" . $inv->id, $attrib)
                ?>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("date", "sldate"); ?>
                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : $this->sma->hrld($inv->date)), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("reference_no", "slref"); ?>
                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ''), 'class="form-control input-tip" id="slref" required="required"'); ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("biller", "slbiller"); ?>
                                <?php
                                $bl[""] = "";
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $inv->biller_id), 'id="slbiller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="panel panel-warning">
                                <div
                                        class="panel-heading"><?= lang('please_select_these_before_adding_product') ?></div>
                                <div class="panel-body" style="padding: 5px;">

                                    <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("warehouse", "slwarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $inv->warehouse_id), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } else {
                                        /*
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'slwarehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                        );

                                        echo form_input($warehouse_input);
                                        */
                                        ?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <?= lang("warehouse", "slwarehouse"); ?>
                                                <?php
                                                $wh[''] = '';
                                                foreach ($warehouses as $warehouse) {
                                                    $wh[$warehouse->id] = $warehouse->name;
                                                }
                                                echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $inv->warehouse_id), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                                ?>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <?= lang("customer", "slcustomer"); ?>
                                            <div class="input-group">
                                                <?php
                                                echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                                ?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="#" id="removeReadonly">
                                                        <i class="fa fa-unlock" id="unLock"></i>
                                                    </a>
                                                </div>

                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="#" id="addNovoItem" class="tip" title="<?= lang('Adiconar novo item') ?>">
                                                        <i class="fa fa-plus addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                                    </a>
                                                </div>

                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;display: none;">
                                                    <a href="#" id="addHotelManually" class="tip" title="<?= lang('add_hotel_manually') ?>">
                                                        <i class="fa fa-bed" id="addIcon"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="sticker" style="display: none;">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i></a></div>
                                        <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <div class="controls table-controls">
                                    <table id="slTable"
                                           class="table items table-striped table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th class="col-md-4"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                            <?php
                                            if ($Settings->product_serial) {
                                                echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                            }
                                            ?>
                                            <th class="col-md-1"><?= lang("net_unit_price"); ?></th>
                                            <th class="col-md-1"><?= lang("quantity_itens"); ?></th>
                                            <?php
                                            if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount') || $inv->product_discount)) {
                                                echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                            }
                                            ?>
                                            <?php
                                            if ($Settings->tax1) {
                                                echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                            }
                                            ?>
                                            <th><?= lang("subtotal"); ?> (<span
                                                        class="currency"><?= $default_currency->code ?></span>)
                                            </th>
                                            <th style="width: 30px !important; text-align: center;"><i

                                                        style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <?php if ($Settings->tax2) { ?>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <?= lang("order_tax", "sltax2"); ?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="sltax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (($Owner || $Admin || $this->session->userdata('allow_discount')) || $inv->order_discount_id) { ?>
                            <div class="col-md-6" style="display: none;">
                                <div class="form-group">
                                    <?= lang("order_discount", "sldiscount"); ?>
                                    <?php echo form_input('order_discount', '', 'class="form-control input-tip mask_money" id="sldiscount" '.(($Owner || $Admin || $this->session->userdata('allow_discount')) ? '' : 'readonly="true"')); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-6" style="display: none;">
                            <div class="form-group">
                                <?= lang("acrescimo", "slshipping"); ?>
                                <?php echo form_input('shipping', '', 'class="form-control input-tip mask_money" id="slshipping"'); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                        <div class="col-sm-6" <?php if (!$Owner && !$Admin) echo 'style="display: none;"';?>>
                            <div class="form-group">
                                <?= lang("sale_status", "slsale_status"); ?>
                                <?php $sst = array('pending' => lang('pending'), 'completed' => lang('completed'), 'lista_espera' => lang('lista_espera'));
                                echo form_dropdown('sale_status', $sst, '', 'class="form-control input-tip" required="required" id="slsale_status"');
                                ?>

                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="form-group">
                                <?= lang("Vendedor", "Vendedor"); ?>*
                                <?php echo form_input('vendedor', (isset($_POST['vendedor']) ? $_POST['vendedor'] : $inv->vendedor), 'class="form-control input-tip" id="vendedor"' ); ?>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("local_saida", "local_saida"); ?>
                                <?php echo form_input('local_saida', (isset($_POST['local_saida']) ? $_POST['local_saida'] : $inv->local_saida), 'class="form-control input-tip" id="local_saida" required="required" ' ); ?>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("tipo_quarto_str", "tipo_quarto_str"); ?>
                                <?php echo form_input('tipo_quarto_str', (isset($_POST['tipo_quarto_str']) ? $_POST['tipo_quarto_str'] : $inv->tipo_quarto_str), 'class="form-control input-tip" id="tipo_quarto_str"' ); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("posicao_assento", "posicao_assento"); ?>
                                <?php echo form_input('posicao_assento', (isset($_POST['posicao_assento']) ? $_POST['posicao_assento'] : $inv->posicao_assento), 'class="form-control input-tip" id="posicao_assento"' ); ?>
                            </div>
                        </div>
                        <div class="col-md-10">
                            <div class="form-group">
                                <?= lang("condicao_pagamento", "condicao_pagamento"); ?>
                                <?php echo form_input('condicao_pagamento', (isset($_POST['condicao_pagamento']) ? $_POST['condicao_pagamento'] : $inv->condicao_pagamento), 'class="form-control input-tip" id="condicao_pagamento"' ); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang('previsao_pagamento', 'previsao_pagamento'); ?>
                                <input type="date" name="vencimento" value="<?php echo  (isset($_POST['vencimento']) ? $_POST['vencimento'] : $inv->vencimento);?>" class="form-control tip" id="vencimento" data-original-title="" title="">
                            </div>
                        </div>
                        <?= form_hidden('payment_status', $inv->payment_status); ?>
                        <div class="clearfix"></div>
                        <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                        <div class="row" id="bt">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("sale_note", "slnote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="slnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("staff_note", "slinnote"); ?>
                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="slinnote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="fprom-group">
                                <?php echo form_submit('edit_sale', lang("submit"), 'id="edit_sale" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                        <tr class="warning">
                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                            <?php if (($Owner || $Admin || $this->session->userdata('allow_discount')) || $inv->total_discount) { ?>
                                <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                            <?php } ?>
                            <?php if ($Settings->tax2) { ?>
                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                            <?php } ?>
                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                        </tr>
                    </table>
                </div>
                <input type="hidden" name="supplier_id" value="<?php echo $inv->hotel; ?>" id="supplier_id">
                <input type="hidden" name="supplier_id2" value=<?php echo $inv->hotel2; ?>"" id="supplier_id2">
                <input type="hidden" name="supplier_id3" value="<?php echo $inv->hotel3; ?>" id="supplier_id3">
                <input type="hidden" name="supplier_id4" value="<?php echo $inv->hotel4; ?>" id="supplier_id4">
                <input type="hidden" name="supplier_id5" value="<?php echo $inv->hotel5; ?>" id="supplier_id5">
                <input type="hidden" name="supplier_id6" value="<?php echo $inv->hotel6; ?>" id="supplier_id6">
                <input type="hidden" name="supplier_id7" value="<?php echo $inv->hotel7; ?>" id="supplier_id7">
                <input type="hidden" name="totalHoteis" value="<?php echo $inv->totalHoteis; ?>" id="totalHoteis">
                <input type="hidden" name="form_slcustomer_1" value="<?php echo $inv->customer_1_id; ?>" id="form_slcustomer_1"/>
                <input type="hidden" name="form_slcustomer_2" value="<?php echo $inv->customer_2_id; ?>" id="form_slcustomer_2"/>
                <input type="hidden" name="form_slcustomer_3" value="<?php echo $inv->customer_3_id; ?>" id="form_slcustomer_3"/>
                <input type="hidden" name="form_slcustomer_4" value="<?php echo $inv->customer_4_id; ?>" id="form_slcustomer_4"/>
                <input type="hidden" name="form_slcustomer_5" value="<?php echo $inv->customer_5_id; ?>" id="form_slcustomer_5"/>
                <input type="hidden" name="form_slnote_hotel" value="<?php echo $inv->note_hotel; ?>" id="form_slnote_hotel"/>
                <input type="hidden" name="form_sltipo_quarto" value="<?php echo $inv->tipo_quarto; ?>" id="form_sltipo_quarto"/>
                <input type="hidden" name="reference_no_variacao" value="<?php echo $inv->reference_no_variacao; ?>" id="reference_no_variacao"/>
                <?php echo form_close(); ?>

            </div>

        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>

            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity_item') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>

                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('customer') ?></label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customerClient', (isset($_POST['customerClient']) ? $_POST['customerClient'] : ""), 'id="customerClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                            ?>

                        </div>
                    </div>

                    <!--########################!-->
                    <!--####     ONIBUS     ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('Onibus') ?></label>
                        <div class="col-sm-8">
                            <input type="hidden" name="reference_no_variacao_onibus" value="" id="reference_no_variacao_onibus" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("onibus") ?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('poltrona') ?></label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <?php
                                echo form_input('poltronaClient', (isset($_POST['poltronaClient']) ? $_POST['poltronaClient'] : ""), 'id="poltronaClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" required="required" style="width:100%;"');
                                ?>
                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                    <a href="#" id="addBusManuallyClient" class="tip" title="<?= lang('add_poltrona_manually') ?>">
                                        <i class="fa fa-bus  addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                    </a>
                                </div>

                                <!--
                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                    <i class="fa fa-times tip pointer sldel  addIcon"  onclick="$('#poltronaClient').val('');" style="font-size: 1.2em;"></i>
                                </div>
                                !-->
                            </div>
                        </div>
                    </div>

                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount" <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? '' : 'readonly="true"'; ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('valor') ?></label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="net_price"></span></th>
                        </tr>
                    </table>



                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<!--#################### INICIO MODAL ONIBUS ######################### -->
<div class="modal" id="mModal2" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
        <div class="modal-content">
            <center>
                <div class="modal-header">
                    <button type="button" class="close" id="close_bus" data-dismiss="modal"><span aria-hidden="true"><i
                                    class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                    <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_poltrona') ?></h4>
                </div>

                <iframe src="#" width="100%"  id="iframe_onibus" scrolling="auto" height="900px"></iframe>

            </center>
        </div>
    </div>
</div>
<!--######################## FINAL MODAL ONIBUS ################################### -->

<!-- ################################### MODAL HOTEL ##########################################################-->
<div class="modal" id="mModal3" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_hotel_manually') ?></h4>
            </div>

            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">

                    <!--########################!-->
                    <!--#### QT HOTEIS ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('qts_hoteis') ?> *</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="qts_hoteis" id="qts_hoteis" required="required">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                            </select>
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDOR   ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= '1º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier" value="" id="posupplier" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 2  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier2">
                        <label for="mtax" class="col-sm-4 control-label"><?= '2º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier2" value="" id="posupplier2" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 3  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier3">
                        <label for="mtax" class="col-sm-4 control-label"><?= '3º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier3" value="" id="posupplier3" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 4  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier4">
                        <label for="mtax" class="col-sm-4 control-label"><?= '4º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier4" value="" id="posupplier4" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 5  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier5">
                        <label for="mtax" class="col-sm-4 control-label"><?= '5º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier5" value="" id="posupplier5" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 6  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier6">
                        <label for="mtax" class="col-sm-4 control-label"><?= '6º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier6" value="" id="posupplier6" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--####   FORNECEDORES 7  ####!-->
                    <!--#########################-->
                    <div class="form-group" style="display: none;" id="div_supplier7">
                        <label for="mtax" class="col-sm-4 control-label"><?= '7º '.lang('hotel') ?> *</label>
                        <div class="col-sm-8">
                            <input type="hidden" name="supplier7" value="" id="posupplier7" class="form-control"  placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                        </div>
                    </div>

                    <!--########################!-->
                    <!--#### TIPO DE QUARTO ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('tipo_quarto') ?> *</label>
                        <div class="col-sm-8">
                            <select class="form-control" name="tipo_quarto" id="tipo_quarto" required="required">
                                <option value="8"><?php echo lang('single');?></option>
                                <option value="1"><?php echo lang('duplo_casal');?></option>
                                <option value="2"><?php echo lang('duplo_casal_um_adulto');?></option>
                                <option value="3"><?php echo lang('duplo_casal_dois_adulto');?></option>
                                <option value="4"><?php echo lang('duplo_casal_tres_adulto');?></option>
                                <option value="6"><?php echo lang('duplo_solteiro');?></option>
                                <option value="7"><?php echo lang('tripo_solteiro');?></option>
                            </select>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 1 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_1">
                        <label for="mtax" class="col-sm-4 control-label"><?= '1º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_1', (isset($_POST['customer_1']) ? $_POST['customer_1'] : ""), 'id="slcustomer_1" data-placeholder="' . lang("select") . ' ' . lang("customer_1") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 2 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_2" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '2º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_2', (isset($_POST['customer_2']) ? $_POST['customer_2'] : ""), 'id="slcustomer_2" data-placeholder="' . lang("select") . ' ' . lang("customer_2") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>


                    <!--#########################!-->
                    <!--#### 3 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_3" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '3º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_3', (isset($_POST['customer_3']) ? $_POST['customer_3'] : ""), 'id="slcustomer_3" data-placeholder="' . lang("select") . ' ' . lang("customer_3") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--#########################!-->
                    <!--#### 4 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_4" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '4º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_4', (isset($_POST['customer_4']) ? $_POST['customer_4'] : ""), 'id="slcustomer_4" data-placeholder="' . lang("select") . ' ' . lang("customer_4") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>


                    <!--#########################!-->
                    <!--#### 5 pessoa do quarto #!-->
                    <!--##########################-->
                    <div class="form-group" id="div_slcustomer_5" style="display:none">
                        <label for="mtax" class="col-sm-4 control-label"><?= '5º '.lang('Hospede') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_input('customer_5', (isset($_POST['customer_5']) ? $_POST['customer_5'] : ""), 'id="slcustomer_5" data-placeholder="' . lang("select") . ' ' . lang("customer_5") . '" required="required" class="form-control input-tip"');
                            ?>
                        </div>
                    </div>

                    <!--########################!-->
                    <!--#### NOTA DO HOTEL  ####!-->
                    <!--#########################-->
                    <div class="form-group">
                        <label for="note_hotel" class="col-sm-4 control-label"><?= lang('sale_note_hotel') ?></label>
                        <div class="col-sm-8">
                            <?php echo form_textarea('note_hotel', (isset($_POST['note_hotel']) ? $_POST['note_hotel'] : ""), 'class="form-control" id="note_hotel" style="margin-top: 10px; height: 100px;"'); ?>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="addItemHotelManually"><?= lang('submit') ?></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!--####################################################### FINAL DO MODAL HOTEL #####################################--->

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_poltrona') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('descricao') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity_item') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="mserial" class="col-sm-4 control-label"><?= lang('product_serial') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mserial">
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_discount) { ?>
                        <div class="form-group">
                            <label for="mdiscount" class="col-sm-4 control-label">
                                <?= lang('product_discount') ?>
                            </label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount" <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? '' : 'readonly="true"'; ?>>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('valor') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:25%;"><span id="mnet_price"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>