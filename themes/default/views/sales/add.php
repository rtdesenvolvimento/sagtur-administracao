

<style>
    #s2id_mes {
        height: 46px;
    }

    #s2id_mes a {
        height: 46px;
        padding: 5px 0px 0px 8px;
    }

    #s2id_ano {
        height: 46px;
    }

    #s2id_ano a {
        height: 46px;
        padding: 5px 0px 0px 8px;
    }

    /* Estilizando o ícone do calendário do input de data */
    input[type="date"]::-webkit-calendar-picker-indicator {
        float: left;
        margin-right: 5px; /* Ajuste conforme necessário */
    }


    .btn2 {
        display: inline-block;
        *display: inline;
        *zoom: 1;
        padding: 4px 12px;
        margin-bottom: 0;
        font-size: 12px;
        line-height: 20px;
        text-align: center;
        vertical-align: middle;
        cursor: pointer;
        color: #333333;
        text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
        background-color: #f5f5f5;
        background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
        background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
        background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
        background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
        border-color: #e6e6e6 #e6e6e6 #bfbfbf;
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        *background-color: #e6e6e6;
        filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
        border: 1px solid #cccccc;
        *border: 0;
        border-bottom-color: #b3b3b3;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        *margin-left: .3em;
        -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
        -moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
        box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
    }

    .btn2-primary {
        color: #ffffff;
        text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
        background-color: #006dcc;
        background-image: -moz-linear-gradient(top, #0088cc, #0044cc);
        background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#0088cc), to(#0044cc));
        background-image: -webkit-linear-gradient(top, #0088cc, #0044cc);
        background-image: -o-linear-gradient(top, #0088cc, #0044cc);
        background-image: linear-gradient(to bottom, #0088cc, #0044cc);
        background-repeat: repeat-x;
        filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
        border-color: #0044cc #0044cc #002a80;
        border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
        *background-color: #0044cc;
        filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
    }

</style>
<script type="text/javascript">
    var count = 1,
        an = 1,
        ITEM_SELECTED = null;
        ADD_NEW_PASS = false;
        product_variant = 0,
        DT = <?= $Settings->default_tax_rate ?>,
        OPERADORA = <?= $Settings->default_supplier ?>,
        product_tax = 0,
        invoice_tax = 0,
        product_discount = 0,
        order_discount = 0,
        total_discount = 0,
        total = 0,
        allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        staff = <?php echo json_encode($staff); ?>,
        tax_rates = <?php echo json_encode($tax_rates); ?>;

    var audio_success = new Audio('<?=$assets?>sounds/sound2.mp3');
    var audio_error = new Audio('<?=$assets?>sounds/sound3.mp3');
    
	$(document).ready(function () {

        if (localStorage.getItem('remove_slls')) {

            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }

            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }

            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }

            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }

            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }

            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }

            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }

            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }

            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }

            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }

            if (localStorage.getItem('sdivulgacao')) {
                localStorage.removeItem('sdivulgacao');
            }

            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }

            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }

            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }

            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }

            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }

            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }

            localStorage.removeItem('previsao_pagamento');
            localStorage.removeItem('valor');
            localStorage.removeItem('tipoCobrancaId');
            localStorage.removeItem('condicaopagamentoId');

            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }

            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }

            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }

            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }

            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }

            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }

            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }

            if (localStorage.getItem('payment_note_1')) {
                localStorage.removeItem('payment_note_1');
            }

            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }

            localStorage.removeItem('remove_slls');
        }

        <?php if($quote_id) { ?>
			localStorage.setItem('sldate', '<?= $this->sma->hrld($quote->date) ?>');
			localStorage.setItem('slcustomer', '<?= $quote->customer_id ?>');
			localStorage.setItem('slbiller', '<?= $quote->biller_id ?>');
			localStorage.setItem('slwarehouse', '<?= $quote->warehouse_id ?>');
			localStorage.setItem('slnote', '<?= str_replace(array("\r", "\n"), "", $this->sma->decode_html($quote->note)); ?>');
			localStorage.setItem('sldiscount', '<?= $quote->order_discount_id ?>');
			localStorage.setItem('sltax2', '<?= $quote->order_tax_id ?>');
			localStorage.setItem('slshipping', '<?= $quote->shipping ?>');
			localStorage.setItem('slitems', JSON.stringify(<?= $quote_items; ?>));
        <?php } ?>

        <?php if($this->input->get('customer')) { ?>
			if (!localStorage.getItem('slitems')) {
			    localStorage.setItem('slcustomer', <?=$this->input->get('customer');?>);
			}
        <?php } ?>
		
        <?php if ($Owner || $Admin) { ?>

			if (!localStorage.getItem('sldate')) {
				$("#sldate").datetimepicker({
					format: site.dateFormats.js_ldate,
					fontAwesome: true,
					language: 'sma',
					weekStart: 1,
					todayBtn: 1,
					autoclose: 1,
					todayHighlight: 1,
					startView: 2,
					forceParse: 0
				}).datetimepicker('update', new Date());
			}

			$(document).on('change', '#sldate', function (e) {
				localStorage.setItem('sldate', $(this).val());
			});

            $(document).on('change', '#slbiller', function (e) {
                localStorage.setItem('slbiller', $(this).val());
            });

			if (sldate = localStorage.getItem('sldate')) {
				$('#sldate').val(sldate);
			}

        <?php } ?>

        $(document).on('change', '#sdivulgacao', function (e) {
            localStorage.setItem('sdivulgacao', $(this).val());
        });

        if (localStorage.getItem('sdivulgacao') && (<?=$this->Settings->meio_divulgacao_default;?> !== localStorage.getItem('sdivulgacao')) ) {
            $('#sdivulgacao').val(localStorage.getItem('sdivulgacao'));
        } else {
            $('#sdivulgacao').val(<?=$this->Settings->meio_divulgacao_default;?>);
        }

        if (!localStorage.getItem('slref')) {
            localStorage.setItem('slref', '<?=$slnumber?>');
        }

        if (!localStorage.getItem('sltax2')) {
            localStorage.setItem('sltax2', <?=$Settings->default_tax_rate2;?>);
        }

        ItemnTotals();

        $('.bootbox').on('hidden.bs.modal', function (e) {
            $('#add_item').focus();
        });

        $('#mModal2').on('hidden.bs.modal', function (e) {
            setTimeout(function(){
                $('#prModal').appendTo("body").modal({
                    backdrop: 'static',
                    keyboard: false
                }).modal('show');
            },600);
        });

        $("#add_item").autocomplete({
            source: function (request, response) {

                if (!$('#slcustomer').val()) {
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    bootbox.alert('<?=lang('select_above');?>');
                    $('#add_item').focus();
                    return false;
                }

                $.ajax({
                    type: 'get',
                    url: '<?= site_url('saleitem/consultar_servicos'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#slwarehouse").val(),
                        customer_id: $("#slcustomer").val(),
                        ano: $('#ano').val(),
                        mes: $('#mes').val(),
                        data_passeio: $('#data_passeio').val(),
                    },
                    success: function (data) {
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 300,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    var row = adicionarNovoItemVenda(ui.item);

                    if (row !== null) {

                        console.log(row.row.row_no);

                        $('#'+row.row.row_no).click();

                        $(this).val(ui.item.row.name);

                        ITEM_SELECTED = ui.item;//TODO
                    }

                } else {
                    audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });

        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "sales/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id !== $('#slcustomer').val()) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            $('#gc_details').html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });

        $('#add_item').bind('keypress', function (e) {
            if (e.keyCode === 13) {
                e.preventDefault();
                $(this).autocomplete("search");
            }
        });
    });
</script>

<div class="box">
    <div class="box-header"><h2 class="blue"><i class="fa-fw fa fa-folder-open"></i><?= lang('header.label.venda.pacotes.servicos.turisticos'); ?></h2></div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('enter_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id=' => 'formSales');
                echo form_open_multipart("sales/add", $attrib);
                if ($quote_id) echo form_hidden('quote_id', $quote_id); ?>
                <input type="hidden" name="senderHash">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-3" style="display: none;">
                                <div class="form-group">
                                    <?= lang("warehouse", "slwarehouse"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="slwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $warehouse_input = array(
                                'type' => 'hidden',
                                'name' => 'warehouse',
                                'id' => 'slwarehouse',
                                'value' => $this->session->userdata('warehouse_id'),
                            );

                            echo form_input($warehouse_input);
                            ?>
                        <?php } ?>

                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("date", "sldate"); ?>
                                    <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="sldate" required="required"'); ?>
                                </div>
                            </div>
                        <?php } ?>
						
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang("biller", "slbiller"); ?>
                                    <?php
                                    $bl[""] = "";
                                    foreach ($billers as $biller) {
                                        //$bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bl[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $this->session->userdata('biller_id')), 'id="slbiller" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } else {
                            $biller_input = array(
                                'type' => 'hidden',
                                'name' => 'biller',
                                'id' => 'slbiller',
                                'value' => $this->session->userdata('biller_id'),
                            );
                            echo form_input($biller_input);
                        } ?>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-sm-3" style="display: none;">
                                    <div class="form-group">
                                        <?= lang("sale_status", "slsale_status"); ?>
                                        <?php
                                        $sst = array(
                                            //'completed' => lang('completed'),//TODO POR ENQUANTO PRIVADO O USO
                                            //'pending' => lang('pending'),
                                            'orcamento' => lang('orcamento'),
                                            'faturada' => lang('faturada'),
                                            'lista_espera' => lang('lista_espera')
                                        );
                                        echo form_dropdown('sale_status', $sst, '', 'class="form-control input-tip" required="required" id="slsale_status"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("pagamente", "slcustomer"); ?>
                                        <div class="input-group">
                                            <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") .' '. lang("search_terms_customer") . '" required="required" class="form-control input-tip" style="width:100%;"'); ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 7px;" title="Pesquisar Passageiro">
                                                <a href="#" id="search-customer-modal" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-search" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;color: #428bca;cursor: pointer;display: none;">
                                                <i class="fa fa-lock" id="toogle-customer-read-attr" style="font-size: 1.2em;"></i>
                                            </div>
                                            <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;" title="Ver Dados do Passageiro">
                                                <a href="#" id="view-customer" class="external" data-toggle="modal" data-target="#myModal">
                                                    <i class="fa fa-eye" id="addIcon" style="font-size: 1.2em;"></i>
                                                </a>
                                            </div>
                                            <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                                <div class="input-group-addon no-print" style="padding: 2px 8px;" title="Adicionar Passageiro">
                                                    <a href="<?= site_url('customers/add'); ?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                                        <i class="fa fa-user-plus" id="addIcon"  style="font-size: 1.2em;"></i>
                                                    </a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" id="sticker" >
                            <div class="well well-sm">
                                <!--
                                <div class="col-lg-1" style="padding: 0px 0px 0px 0px;">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">Ano</div>
                                            <select class="form-control" id="ano"">
                                                <option value="2020" <?php if($anoFilter == '2020') echo ' selected="selected"';?>>2020</option>
                                                <option value="2021" <?php if($anoFilter == '2021') echo ' selected="selected"';?>>2021</option>
                                                <option value="2022" <?php if($anoFilter == '2022') echo ' selected="selected"';?>>2022</option>
                                                <option value="2023" <?php if($anoFilter == '2023') echo ' selected="selected"';?>>2023</option>
                                                <option value="2024" <?php if($anoFilter == '2024') echo ' selected="selected"';?>>2024</option>
                                                <option value="2024" <?php if($anoFilter == '2025') echo ' selected="selected"';?>>2025</option>
                                                <option value="2024" <?php if($anoFilter == '2026') echo ' selected="selected"';?>>2026</option>
                                                <option value="2024" <?php if($anoFilter == '2027') echo ' selected="selected"';?>>2027</option>
                                                <option value="2024" <?php if($anoFilter == '2028') echo ' selected="selected"';?>>2028</option>
                                                <option value="2024" <?php if($anoFilter == '2029') echo ' selected="selected"';?>>2029</option>
                                                <option value="2024" <?php if($anoFilter == '2030') echo ' selected="selected"';?>>2030</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2" style="padding: 0px 0px 0px 0px;">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">Mês</div>
                                            <select class="form-control" id="mes">
                                                <option value="">ANO TODO</option>
                                                <option value="01" <?php if($mesFilter == '01') echo ' selected="selected"';?>>JANEIRO</option>
                                                <option value="02" <?php if($mesFilter == '02') echo ' selected="selected"';?>>FEVEREIRO</option>
                                                <option value="03" <?php if($mesFilter == '03') echo ' selected="selected"';?>>MARÇO</option>
                                                <option value="04" <?php if($mesFilter == '04') echo ' selected="selected"';?>>ABRIL</option>
                                                <option value="05" <?php if($mesFilter == '05') echo ' selected="selected"';?>>MAIO</option>
                                                <option value="06" <?php if($mesFilter == '06') echo ' selected="selected"';?>>JUNHO</option>
                                                <option value="07" <?php if($mesFilter == '07') echo ' selected="selected"';?>>JULHO</option>
                                                <option value="08" <?php if($mesFilter == '08') echo ' selected="selected"';?>>AGOSTO</option>
                                                <option value="09" <?php if($mesFilter == '09') echo ' selected="selected"';?>>SETEMBRO</option>
                                                <option value="10" <?php if($mesFilter == '10') echo ' selected="selected"';?>>OUTUBRO</option>
                                                <option value="11" <?php if($mesFilter == '11') echo ' selected="selected"';?>>NOVEMBRO</option>
                                                <option value="12" <?php if($mesFilter == '12') echo ' selected="selected"';?>>DEZEMBRO</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-2" style="padding: 0px 0px 0px 0px;border-left: 1px solid #ccc;">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;height: 46px;">
                                                <input class="flatpickr form-control" type="date" id="data_passeio" name="data_passeio">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                !-->

                                <div class="col-lg-12" style="padding: 0px 0px 0px 0px;">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <?php if ($this->Settings->receptive) {?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <input class="flatpickr form-control" style="font-size: 18px;font-weight: 500;" type="date" id="data_passeio" name="data_passeio">
                                                </div>
                                            <?php } else {?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <i class="fa fa-2x fa-map-signs addIcon"></i>
                                                </div>
                                            <?php } ?>
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;display: none;">
                                                <a href="#" id="addManually" class="tip" title="<?= lang('add_product_manually') ?>">
                                                    <i class="fa fa-2x fa-plus-circle addIcon"></i>
                                                </a>
                                            </div>
                                            <?php if ($this->Settings->servicos_adicionais_venda){?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                    <i class="fa fa-2x fa-plane addIcon"  style="color: #428bca;"  title="Adicionar Transporte Aéreo" id="adicionar-formulario-aereo"></i>
                                                </div>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                    <i class="fa fa-2x fa-bed addIcon" style="color: #428bca;"  title="Adicionar Hospedagem" id="adicionar-formulario-hospedagem"></i>
                                                </div>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                    <i class="fa fa-2x fa-plus addIcon" style="color: #428bca;" title="Adicionar Serviço Turístico" id="adicionar-formulario-pacote-operadora"></i>
                                                </div>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                    <i class="fa fa-2x fa-exchange addIcon" style="color: #428bca;"  title="Adicionar Transfer" id="adicionar-formulario-transfer"></i>
                                                </div>
                                            <?php } ?>
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                <button type="button" class="btn btn-primary addPass" id="adicionar-passageiro-buttom" style="text-align: right;"><i class="fa fa-plus-circle"></i> Add Passageiros</button>
                                            </div>
                                            <?php if ($this->Settings->receptive){?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;cursor: pointer;">
                                                    <i class="fa fa-2x fa-trash-o addIcon" style="color: #428bca;"  title="Limpar Campo" id="clear-add-product"></i>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <ul id="myTab" class="nav nav-tabs">
                                <style>
                                    .total_itens_contador {
                                        background: #fff;
                                        color: #3fb29d;
                                        padding: 0 7px;
                                        min-width: 9px;
                                        border-radius: 12px;
                                        font-weight: 700;
                                        border-bottom: 1px solid #dbdee0;
                                    }
                                </style>
                                <li class=""><a href="#meus-pacotes" class="tab-grey" style="text-align: center;"><i class="fa fa-shopping-cart" style="font-size: 20px;"></i><span class="number blightOrange black total_itens_contador"><span id="total_meus_pacotes">0</span></span><br/> <?=lang('label_itens');?></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="meus-pacotes" class="tab-pane fade in" style="border: none; padding: 1px;">
                                    <div class="box">
                                        <div class="box-header" id="barra-remover" style="display: none;">
                                            <h2 class="blue"><i class="fa-fw fa fa-plus" id="iconCadastroManual"></i><span id="descricaoCadastroManual"></span></h2>
                                            <button type="button" class="btn btn-primary add-payment-item-remove" id="remover-pacote" style="text-align: right;"><i class="fa fa-trash" style="font-size: 23px;"></i></button>
                                        </div>
                                        <div class="box-content">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="table-responsive">
                                                        <div class="controls table-controls">
                                                            <table id="slTable" class="table items table-bordered table-condensed table-hover" style="cursor: pointer;">
                                                                <thead>
                                                                <tr>
                                                                    <th style="text-align: left;width: 12%;display: none;"><?= lang("type"); ?></th>
                                                                    <th class="col-md-4" style="text-align: left;"><?= lang("service"); ?></th>
                                                                    <th class="col-md-1" style="text-align: center;"><?= lang("emissao"); ?></th>
                                                                    <th class="col-md-1" style="text-align: right;"><?= lang("net_unit_price"); ?></th>
                                                                    <th class="col-md-1" style="text-align: center;"><?= lang("quantity_itens"); ?></th>
                                                                    <?php
                                                                    if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                                        echo '<th class="col-md-1" style="text-align: right;">' . lang("discount") . '</th>';
                                                                    }
                                                                    ?>
                                                                    <th style="text-align: right;"><?= lang("subtotal"); ?>(<span class="currency"><?= $default_currency->code ?></span>)</th>
                                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-edit" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                                <tfoot></tfoot>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- ################# !-->
                                            <!-- ## MEUS PACOTES # !-->
                                            <!-- ################# !-->
                                            <div class="row">
                                                <div class="col-md-12" id="form-pacotes-operadoras" style="display: none;">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group all" id="div_category">
                                                                <?= lang("category", "category") ?>
                                                                <?php
                                                                $cat[''] = "";
                                                                foreach ($categories as $category)$cat[$category->id] = $category->name;
                                                                echo form_dropdown('category', $cat, '', 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" style="width:100%"')
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang("nomePacote", "lsNomePacote"); ?>
                                                                <?php echo form_input('nomePacote', (isset($_POST['nomePacote']) ? $_POST['nomePacote'] : ""), 'class="form-control" id="lsNomePacote"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group company">
                                                                <?= lang("tipoDestino", "slTipoDestino"); ?>
                                                                <select id="slTipoDestino" name="tipoDestino" class="form-control">
                                                                    <option value="NACIONAL">NACIONAL</option>
                                                                    <option value="INTERNACIONAL">INTERNACIONAL</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("buscarReceptivo", "receptivo"); ?>
                                                                <?php echo form_input('receptivo', (isset($_POST['receptivo']) ? $_POST['receptivo'] : ""), 'class="form-control" id="receptivo"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-9">
                                                            <div class="form-group">
                                                                <?= lang("fornecedorReceptivo", "fornecedorReceptivo"); ?>
                                                                <?php echo form_input('fornecedorReceptivo', (isset($_POST['fornecedorReceptivo']) ? $_POST['fornecedorReceptivo'] : ""), 'class="form-control"  placeholder="Insira o nome do fornecedor" id="fornecedorReceptivo"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <?= lang("telefoneReceptivo", "telefoneReceptivo"); ?>
                                                                <?php echo form_input('telefoneReceptivo', (isset($_POST['telefoneReceptivo']) ? $_POST['telefoneReceptivo'] : ""), 'class="form-control" placeholder="Telefone com (DDD)"  id="telefoneReceptivo"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <?= lang("enderecoReceptivo", "enderecoReceptivo"); ?>
                                                                <?php echo form_input('enderecoReceptivo', (isset($_POST['enderecoReceptivo']) ? $_POST['enderecoReceptivo'] : ""), 'class="form-control"  placeholder="Endereço" id="enderecoReceptivo"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang("origemPacote", "origemPacote"); ?>
                                                                <?php echo form_input('origemPacote', (isset($_POST['origemPacote']) ? $_POST['origemPacote'] : ""), 'class="form-control input-tip" placeholder="Digite uma origem/aeroporto" id="origemPacote"'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <?= lang("destinoPacote", "destinoPacote"); ?>
                                                                <?php echo form_input('destinoPacote', (isset($_POST['destinoPacote']) ? $_POST['destinoPacote'] : ""), 'class="form-control input-tip" placeholder="Digite um destino/aeroporto" id="destinoPacote"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("emissaoPacote", "slEmissaoPacote"); ?>
                                                                <?php echo form_input('emissaoPacote', (isset($_POST['emissaoPacote']) ? $_POST['emissaoPacote'] : ""), 'class="form-control input-tip" id="slEmissaoPacote"', 'date'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckinPacote", "slDateCheckinPacote"); ?>
                                                                <?php echo form_input('dateCheckinPacote', (isset($_POST['dateCheckinPacote']) ? $_POST['dateCheckinPacote'] : ""), 'class="form-control input-tip" id="slDateCheckinPacote"', 'date'); ?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <?= lang("dateCheckoutPacote", "slDateCheckoutPacote"); ?>
                                                                <?php echo form_input('dateCheckoutPacote', (isset($_POST['dateCheckoutPacote']) ? $_POST['dateCheckoutPacote'] : ""), 'class="form-control input-tip" id="slDateCheckoutPacote"', 'date'); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <div class="form-group">
                                                                <?= lang("operadora", "slSupplierPacote"); ?>
                                                                <div class="input-group">
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <i class="fa fa-building-o" style="font-size: 1.2em;"></i>
                                                                    </div>
                                                                    <?php echo form_input('supplierPacote', (isset($_POST['supplierPacote']) ? $_POST['supplierPacote'] : ""), 'class="form-control" id="slSupplierPacote"'); ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <?= lang("reservaPacote", "slReservaPacote"); ?>
                                                                <?php echo form_input('reservaPacote', (isset($_POST['reservaPacote']) ? $_POST['reservaPacote'] : ""), 'class="form-control" id="slReservaPacote"'); ?>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div id="load-new-pacote-operadora"></div>

                                                    <div class="clearfix"></div>
                                                    <div class="row" style="margin-top: 10px;">
                                                        <div class="col-md-12">
                                                            <div class="fprom-group">
                                                                <button type="button" class="btn btn-primary" name="adicionar-pacote-operadora" id="adicionar-pacote-operadora"><?= lang('button.label.adiconar.pacote.operadora');?></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="pacotes-com-aereo"></div>
                                            <div id="pacotes-com-hospedagem"></div>
                                            <div id="pacotes-com-transfer"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php if ($Settings->tax2) { ?>
                            <div class="col-md-4" style="display: none;">
                                <div class="form-group">
                                    <?= lang("order_tax", "sltax2"); ?>
                                    <?php
                                    $tr[""] = "";
                                    foreach ($tax_rates as $tax) {
                                        $tr[$tax->id] = $tax->name;
                                    }
                                    echo form_dropdown('order_tax', $tr, (isset($_POST['order_tax']) ? $_POST['order_tax'] : $Settings->default_tax_rate2), 'id="sltax2" data-placeholder="' . lang("select") . ' ' . lang("order_tax") . '" class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                            <div class="col-md-6" >
                                <div class="form-group">
                                    <?= lang("order_discount", "sldiscount"); ?>
                                    <?php echo form_input('order_discount', '0.00', 'class="form-control input-tip mask_money" id="sldiscount"'); ?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("acrescimo", "slshipping"); ?>
                                <?php echo form_input('shipping', '0.00', 'class="form-control input-tip mask_money" id="slshipping"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="form-group">
                                <?= lang("Vendedor", "Vendedor"); ?>
                                <?php echo form_input('vendedor', (isset($_POST['vendedor']) ? $_POST['vendedor'] : ''), 'class="form-control input-tip" id="vendedor"' ); ?>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin-top: 10px;">
                            <ul id="myTab3" class="nav nav-tabs">
                                <li class=""><a href="#pagamento-cliente" class="tab-grey" style="text-align: center;"><i class="fa fa-money" style="font-size: 25px;"></i><br/>Faturar Pagamento Cliente</span></a></li>
                            </ul>
                            <div class="tab-content">
                                <div id="pagamento-cliente" class="tab-pane fade in" style="padding: 1px;">
                                    <div class="box-content">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row" id="form-fatura-cliente">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <?= lang('previsao_pagamento', 'previsao_pagamento'); ?>
                                                            <?php echo form_input('previsao_pagamento', (isset($_POST['previsao_pagamento']) ? $_POST['previsao_pagamento'] : date('d/m/Y')), 'class="form-control input-tip date" id="previsao_pagamento"'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <?= lang("valor_vencimento", "valor"); ?>
                                                            <input type="text" name="valor" id="valor" value="0.00" readonly style="padding: 5px;" class="pa form-control kb-pad mask_money" required="required"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <?= lang("tipo_cobranca", "tipoCobrancaId"); ?>
                                                            <?php
                                                            $cbTipoCobranca[""] = "";
                                                            foreach ($tiposCobranca as $tc) {
                                                                $cbTipoCobranca[$tc->id] = $tc->name;
                                                            }
                                                            echo form_dropdown('tipoCobrancaId', $cbTipoCobranca, (isset($_POST['tipoCobrancaId']) ? $_POST['tipoCobrancaId'] : $this->Settings->tipoCobrancaBoleto), 'id="tipoCobrancaId" name="tipoCobrancaId" required="required" class="form-control input-tip select" style="width:100%;"');
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <?= lang("condicao_pagamento", "condicaopagamentoId"); ?>
                                                            <?php
                                                            $cbCondicaoPagamento[""] = "";
                                                            foreach ($condicoesPagamento as $cp) {
                                                                $cbCondicaoPagamento[$cp->id] = $cp->name;
                                                            }
                                                            echo form_dropdown('condicaopagamentoId', $cbCondicaoPagamento, (isset($_POST['condicaopagamentoId']) ? $_POST['condicaopagamentoId'] : $this->Settings->condicaoPagamentoAVista), 'id="condicaopagamentoId" name="condicaopagamentoId" required="required" class="form-control input-tip select" style="width:100%;"');
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php if ($Owner || $Admin || $GP['sales-payments']) { ?>
                                                        <div class="col-sm-2" style="display:none;">
                                                            <div class="form-group">
                                                                <?= lang("payment_status", "slpayment_status"); ?>
                                                                <?php $pst = array('pending' => lang('pending'), 'due' => lang('due'), 'partial' => lang('partial'), 'paid' => lang('paid'));
                                                                echo form_dropdown('payment_status', $pst, '', 'class="form-control input-tip" required="required" id="slpayment_status"'); ?>
                                                            </div>
                                                        </div>
                                                    <?php } else {
                                                        echo form_hidden('payment_status', 'pending');
                                                    } ?>
                                                    <div class="col-md-6" style="display: none;">
                                                        <div class="form-group">
                                                            <?= lang("reference_no", "slref"); ?>
                                                            <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $slnumber), 'class="form-control input-tip" id="slref"'); ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="controls table-controls slTableParcelas table-responsive">
                                                            <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                                                <thead>
                                                                <tr>
                                                                    <th style="text-align: left;">#</th>
                                                                    <th class="col-md-3" style="text-align: left;">Valor</th>
                                                                    <th class="col-md-1" style="text-align: left;">Desc.</th>
                                                                    <th class="col-md-2" style="text-align: left;">Vencimento</th>
                                                                    <th class="col-md-3" style="text-align: left;">Cobrança</th>
                                                                    <th class="col-md-3" style="text-align: left;">Conta</th>
                                                                </tr>
                                                                </thead>
                                                                <tbody></tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div id="payments" style="display: none;">
                                                        <div class="col-md-12">
                                                            <div class="well well-sm well_1">
                                                                <div class="col-md-12">
                                                                    <div class="row">
                                                                        <div class="col-md-4">
                                                                            <div class="form-group">
                                                                                <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                                                <?= form_input('payment_reference_no', (isset($_POST['payment_reference_no']) ? $_POST['payment_reference_no'] : $payment_ref), 'class="form-control tip" id="payment_reference_no"'); ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="payment">
                                                                                <div class="form-group ngc">
                                                                                    <?= lang("amount", "amount_1"); ?>
                                                                                    <input name="amount-paid" type="text" id="amount_1" required="required" value="0" class="pa form-control kb-pad amount"/>
                                                                                </div>
                                                                                <div class="form-group gc" style="display: none;">
                                                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                                    <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>
                                                                                    <div id="gc_details"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-sm-4">
                                                                            <div class="form-group">
                                                                                <?= lang("paying_by", "paid_by_1"); ?>
                                                                                <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                                                    <?= $this->sma->paid_opts(); ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <div class="pcc_1" style="display:none;">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <input name="pcc_no" type="text" id="pcc_no_1"
                                                                                           class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control"
                                                                                           placeholder="<?= lang('cc_holder') ?>"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <select name="pcc_type" id="pcc_type_1"
                                                                                            class="form-control pcc_type"
                                                                                            placeholder="<?= lang('card_type') ?>">
                                                                                        <option value="Visa"><?= lang("Visa"); ?></option>
                                                                                        <option
                                                                                                value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                                        <option value="Amex"><?= lang("Amex"); ?></option>
                                                                                        <option value="Discover"><?= lang("Discover"); ?></option>
                                                                                    </select>
                                                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">
                                                                                    <input name="pcc_month" type="text" id="pcc_month_1"
                                                                                           class="form-control" placeholder="<?= lang('month') ?>"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">

                                                                                    <input name="pcc_year" type="text" id="pcc_year_1"
                                                                                           class="form-control" placeholder="<?= lang('year') ?>"/>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="form-group">

                                                                                    <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                                                           class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pcheque_1" style="display:none;">
                                                                        <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                                            <input name="cheque_no" type="text" id="cheque_no_1"
                                                                                   class="form-control cheque_no"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <?= lang('payment_note', 'payment_note_1'); ?>
                                                                        <textarea name="payment_note" id="payment_note_1"
                                                                                  class="pa form-control kb-text payment_note"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("meio_divulgacao", "sdivulgacao"); ?>
                                <?php
                                $md[""] = lang("select") . ' ' . lang("meio_divulgacao");
                                foreach ($meiosDivulgacao as $divulgacao) {
                                    $md[$divulgacao->id] = $divulgacao->name;
                                }
                                echo form_dropdown('meio_divulgacao', $md, (isset($_POST['meio_divulgacao']) ? $_POST['meio_divulgacao'] : $this->session->userdata('meio_divulgacao')), 'id="sdivulgacao" name="meio_divulgacao" data-placeholder="' . lang("select") . ' ' . lang("meio_divulgacao") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                       data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                        <div class="row" id="bt">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("sale_note", "slnote"); ?>
                                        <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="slnote" style="margin-top: 10px; height: 100px;"'); ?>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("staff_note", "slinnote"); ?>
                                        <?php echo form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="slinnote" style="margin-top: 10px; height: 100px;"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="from-group">
                                <?php echo form_submit('add_sale', lang("submit"), 'id="add_sale" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;display: none;"'); ?>
                                <button type="button" class="btn btn-primary" id="enviar_orcamento" style="padding: 6px 15px; margin:15px 5px;"><?= lang('enviar_orcamento') ?>
                                <button type="button" class="btn btn-success" id="enviar_faturar" style="padding: 6px 15px; margin:15px 5px;"><?= lang('enviar_faturar') ?>
                                <button type="button" class="btn btn-warning" id="enviar_lista_espera" style="padding: 6px 15px; margin:15px 5px;"><?= lang('enviar_lista_espera') ?>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                    <style>
                        .info-view {
                            background-color: #428bca;
                            color: #ffffff;
                            font-size: 14px;
                        }
                    </style>
                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                        <tr class="info-view">
                            <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                            <td><?= lang('total') ?> <span class="totals_val pull-right" id="total">0.00</span></td>
                            <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                            <td><?= lang('order_discount') ?> <span class="totals_val pull-right" id="tds">0.00</span></td>
                            <?php }?>
                            <?php if ($Settings->tax2) { ?>
                                <td><?= lang('order_tax') ?> <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                            <?php } ?>
                            <td><?= lang('shipping') ?> <span class="totals_val pull-right" id="tship">0.00</span></td>
                            <td><?= lang('grand_total') ?> <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                        </tr>
                    </table>
                </div>

                <input type="hidden" name="totalHoteis" value="" id="totalHoteis">
                <input type="hidden" name="supplier_id" value="" id="supplier_id">
                <input type="hidden" name="supplier_id2" value="" id="supplier_id2">
                <input type="hidden" name="supplier_id3" value="" id="supplier_id3">
                <input type="hidden" name="supplier_id4" value="" id="supplier_id4">
                <input type="hidden" name="supplier_id5" value="" id="supplier_id5">
                <input type="hidden" name="supplier_id6" value="" id="supplier_id6">
                <input type="hidden" name="supplier_id7" value="" id="supplier_id7">
                <input type="hidden" name="form_slcustomer_1" value="" id="form_slcustomer_1"/>
				<input type="hidden" name="form_slcustomer_2" value="" id="form_slcustomer_2"/>
				<input type="hidden" name="form_slcustomer_3" value="" id="form_slcustomer_3"/>
				<input type="hidden" name="form_slcustomer_4" value="" id="form_slcustomer_4"/>
				<input type="hidden" name="form_slcustomer_5" value="" id="form_slcustomer_5"/>
				<input type="hidden" name="form_slnote_hotel" value="" id="form_slnote_hotel"/>
				<input type="hidden" name="form_sltipo_quarto" value="" id="form_sltipo_quarto"/>
				<input type="hidden" name="reference_no_variacao" value="" id="reference_no_variacao"/>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>


<!--#################### INICIO MODAL INSERIR PASSAGEIROS ######################### -->
<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span>
                </button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"><?= lang('product_tax') ?></label>
                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($Settings->product_serial) { ?>
                        <div class="form-group">
                            <label for="pserial" class="col-sm-4 control-label"><?= lang('serial_no') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pserial">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group" style="display: none;">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity_item') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div>

                    <style>
                        .responsavel_pagador {
                            text-align: right;
                            font-size: 1.5rem;
                            color: #428bca;
                            font-weight: 600;
                            text-transform: uppercase;
                        }
                    </style>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <label for="poption" class="control-label"><small><?= lang('pagador_responsavel') ?>: </small></label> <span id="nmpagador" class="responsavel_pagador"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('customer') ?> *</label>
                        <div class="col-sm-8">
                            <div class="input-groupx">
                                <?php echo form_input('customerClient', (isset($_POST['customerClient']) ? $_POST['customerClient'] : ""), 'id="customerClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"'); ?>
                                <div class="input-group-addon no-print" style="padding: 2px 7px;display: none;" title="Pesquisar Passageiro">
                                    <a href="#" id="search-customer-add-modal" class="external" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-search" style="font-size: 1.2em;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="div_fv">
                        <label class="col-sm-4 control-label"><?= lang('faixa_etaria') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_dropdown('faixa_etaria', [], "", 'id="faixa_etaria" class="form-control pos-input-tip" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group" id="div_tipo_hospedagem">
                        <label class="col-sm-4 control-label"><?= lang('tipo_hospedagem') ?> *</label>
                        <div class="col-sm-8">
                            <?php
                            echo form_dropdown('tipo_hospedagem', [], "", 'id="tipo_hospedagem" class="form-control pos-input-tip" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"><?= lang('descontarVaga', 'descontarVaga'); ?> ?</label>
                        <div class="col-sm-8" style="margin-top: 18px;">
                            <span id="sdescontarVaga"></span>
                            <span style="display: none;">
                                <?php echo form_checkbox('descontarVaga', '1', TRUE, 'id="descontarVaga" disabled="disabled"'); ?>
                            </span>
                        </div>
                    </div>
                    <!--########################!-->
                    <!--####     ONIBUS     ####!-->
                    <!--#########################-->
                    <div class="form-group" id="div_tipo_transporte">
                        <label for="mtax" class="col-sm-4 control-label"><?= lang('onibus') ?></label>
                        <div class="col-sm-6">
                            <?php
                            echo form_dropdown('tipo_transporte', [], "", 'id="tipo_transporte" class="form-control pos-input-tip" style="width:100%;"');
                            ?>
                        </div>
                        <div class="col-sm-2">
                            <div class="input-group">
                                <?php
                                echo form_input('poltronaClient', (isset($_POST['poltronaClient']) ? $_POST['poltronaClient'] : ""), 'id="poltronaClient" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" required="required" style="width:100%;"');
                                ?>
                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                    <a href="#" id="addBusManuallyClient" class="tip" title="<?= lang('add_poltrona_manually') ?>">
                                        <i class="fa fa-bus  addIcon" id="addIcon" style="font-size: 1.2em;"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="div_local_embarque">
                        <label class="col-sm-4 control-label"><?= lang('local_embarque') ?></label>
                        <div class="col-sm-5">
                            <?php
                            echo form_dropdown('local_embarque', [], "", 'id="local_embarque" class="form-control pos-input-tip" style="width:100%;"');
                            ?>
                        </div>
                        <label class="col-sm-1 control-label"><?= lang('hora_embarque') ?></label>
                        <div class="col-sm-2">
                            <input type="time" class="form-control" id="hora_embarque">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="referencia_item" class="col-sm-4 control-label"><?= lang('referencia_item') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="referencia_item" placeholder="Ex: Número do Quarto, Vôo">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="valorPorFaixaEtaria" class="col-sm-4 control-label"><?= lang('valorPorFaixaEtaria') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" readonly id="valorPorFaixaEtaria">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="memoriapreco" class="col-sm-4 control-label"><?= lang('valor') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="memoriapreco">
                        </div>
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="valorHospedagem" class="col-sm-4 control-label"><?= lang('valor') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="valorHospedagem">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="pdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="pprice" class="col-sm-4 control-label"><?= lang('valorPorFaixaEtaria') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pprice">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="observacao_item" class="col-sm-4 control-label"><?= lang('note') ?></label>
                        <div class="col-sm-8">
                            <textarea type="text" class="form-control" id="observacao_item"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label"></label>
                        <div class="col-sm-8">
                            <table class="table table-bordered table-striped">
                                <tr>
                                    <th style="width:25%;text-align: right;"><?= 'Subtotal(R$)' ?></th>
                                    <th style="width:25%;text-align: right;"><span id="net_price"></span></th>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- SERVIÇOS ADICIONAIS !-->
                    <div id="div_servicos_adicionais">
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <h3 class="bold"><?= lang('servicos_adicionais') ?></h3>
                                <table class="table table-bordered table-striped table-condensed" id="tbServicosAdicionais">
                                    <thead>
                                    <tr>
                                        <th style="width:5%;">#</th>
                                        <th style="width:20%;text-align: left;">Categoria</th>
                                        <th style="width:50%;text-align: left;">Serviço</th>
                                        <th style="width:15%;text-align: right;">Preço</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div style="text-align: right;margin-bottom: 25px;"></div>
                    <div class="modal-footer" style="margin-right: -15px;">
                        <button type="button" class="btn btn-primary" id="editItem"><i class="fa fa-save"></i> <?= lang('save_close') ?></button>
                    </div>
                    <div  id="div_tbslTableDependents">
                        <h3 class="bold"><?= lang('passengers') ?></h3>
                        <div class="controls table-controls">
                            <table id="slTableDependents" class="table items table-striped table-bordered table-condensed table-hover">
                                <thead>
                                <tr>
                                    <th class="col-md-5" style="text-align: left;">Nome</th>
                                    <th class="col-md-5" style="text-align: left;">Pacote</th>
                                    <th class="col-md-2" style="text-align: right;">Valor</th>
                                    <th style="text-align: center;"><i class="fa fa-edit"></i></th>
                                    <th style="text-align: center;"><i class="fa fa-times"></i></th>
                                </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-sm-8">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th style="width:25%;text-align: right;"><?= 'Subtotal(R$)' ?></th>
                                        <th style="width:25%;text-align: right;"><span id="net_price_passengers"></span></th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer" style="margin-right: -15px;">
                        <button type="button" style="display: none;" class="btn btn-success" id="add_passageiro"><i class="fa fa-plus-circle"></i> <?= lang('save_add_new_passageiro') ?></button>
                        <button type="button" class="btn btn-primary" id="closeItem"><?= lang('finish') ?></button>
                    </div>
                    <!--inputs de controle !-->
                    <input type="hidden" id="punit_price" value=""/>
                    <input type="hidden" id="old_tax" value=""/>
                    <input type="hidden" id="old_qty" value=""/>
                    <input type="hidden" id="old_price" value=""/>
                    <input type="hidden" id="row_id" value=""/>
                    <input type="hidden" id="productId" value=""/>
                    <input type="hidden" id="programacaoId" value=""/>
                    <input type="hidden" id="tipoHospedagemId" value=""/>

                </form>
            </div>

        </div>
    </div>
</div>
<!--#################### FINAL MODAL INSERIR PASSAGEIROS ######################### -->

<!--#################### INICIO MODAL ONIBUS ######################### -->
<div class="modal" id="mModal2" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 900px">
        <div class="modal-content">
		<center>
            <div class="modal-header">
                <button type="button" class="close" id="close_bus" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('adicionar_poltrona') ?></h4>
            </div>
			<iframe width="100%"  id="iframe_onibus" scrolling="auto" height="900px"></iframe>
		</center>
        </div>
    </div>
</div>
<!--######################## FINAL MODAL ONIBUS ################################### -->


<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {?>
                        <div class="form-group">
                            <label for="mdiscount"
                                   class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount">
                            </div>
                        </div>
                        <?php
                    } ?>
                    <div class="form-group">
                        <label for="mprice" class="col-sm-4 control-label"><?= lang('unit_price') ?> *</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mprice">
                        </div>
                    </div>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th style="width:33%;"><?= lang('net_unit_price'); ?></th>
                            <th style="width:33%;"><span id="mnet_price"></span></th>
                            <th style="width:33%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<!-- API PagSeguro is active-->
<?php if ($configPagSeguro->active) { ?>
    <?php if ($configPagSeguro->sandbox == 1) {?>
        <!-- API PagSeguro -->
        <script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <?php } else {?>
        <!-- API PagSeguro -->
        <script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>
    <?php }?>
<?php } ?>


<script type="text/javascript">



    $(document).ready(function () {

        $('#prModal').on('hidden.bs.modal', function (e) {

            $('#add_passageiro').show();

            atualizar_item();
        });

        slitems = JSON.parse(localStorage.getItem('slitems'));

        //atualizar_item();

        if (slitems !== null) {
            $.each(slitems, function () {
                var item = this;

                if (item.row.customerClient === localStorage.getItem('slcustomer') && !item.row.adicional) {
                    ITEM_SELECTED = item;
                    $('#add_item').val(item.row.name);
                }
            });
            if (ITEM_SELECTED === null) {
                $.each(slitems, function () {
                    var item = this;

                    if (!item.row.adicional) {
                        ITEM_SELECTED = item;
                        $('#add_item').val(item.row.name);
                    }
                });
            }
        }

        $('#add_passageiro').click(function(){
            //adicionar op novo passageiro
            adicionarNovoPassageiro();
            //atualiza as tabelas de passageiros
            loadItems();

        });

        function adicionarNovoPassageiro() {

            $('input[name="servicoAdicional[]"]').iCheck('uncheck');

            ITEM_SELECTED.supdate = 'I';

            ITEM_SELECTED.id = new Date().getTime();

            var item_id = site.settings.item_addition === 1 ? ITEM_SELECTED.item_id : ITEM_SELECTED.id;

            if (!controllerSales.isPreencheuFilial() && ITEM_SELECTED == null) {
                return false;
            }

            if (slitems[item_id]) {
                slitems[item_id].row.qty = parseFloat(slitems[item_id].row.qty) + 1;
            } else {
                slitems[item_id] = ITEM_SELECTED;
            }

            slitems[item_id].order = new Date().getTime();

            localStorage.setItem('slitems', JSON.stringify(slitems));

            loadItems();

            $('#row_id').val('row_' +  item_id);

            $('#referencia_item').val('');
            $('#poltronaClient').val('');
            $('#observacao_item').redactor('set', '');

            //limpar dados do formulario para inserir o novo passageiro
            $('#customerClient').select2('val', null).select2('open');
            $('#faixa_etaria').val('').change();

            $('#div_tipo_hospedagem').hide(100);
            $('#div_servicos_adicionais').hide(100);

            $('#tipo_hospedagem').empty();
            $('#tbServicosAdicionais tbody').empty();
            $('#add_passageiro').hide();

            calcularValorProduto();

            var modal = $('#prModal');
            modal.animate({
                scrollTop: 0
            }, 300);
        }

        $('#adicionar-passageiro-buttom').click(function() {

            if (!controllerSales.isPreencheuFilial() && ITEM_SELECTED == null) {
                return false;
            }

            if (ITEM_SELECTED === null) {
                bootbox.alert('Você não selecionou nenhum pacote/serviço para adicionar passageiro(s)');
                $('#add_item').focus();
                return false;
            }

            if (Object.keys(slitems).length === 0) {

                var row = adicionarNovoItemVenda(ITEM_SELECTED);

                if (row !== null) {

                    ADD_NEW_PASS = false;

                    $('#'+row.row.row_no).click();

                    $(this).val(row.label);
                }
            } else {

                ADD_NEW_PASS = true;

                $("#add_item").autocomplete('search',  $('#add_item').val());
            }

        });


        $('#data_passeio').change(function (event){

            var data = new Date($(this).val());

            if (!isNaN(data.getTime())) {
                var ano = data.getFullYear();
                var mes = data.getMonth() + 1;

                $('#ano').select2("val", ano);
                $('#mes').select2("val", mes);
            }

            $('#add_item').focus();
        });

        $('#mes').change(function (evet){
            $('#add_item').focus();
        });

        $('#ano').change(function (evet){
            $('#add_item').focus();
        });

        $('#gccustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#clear-add-product').click(function(){
            $('#add_item').val('');
            $('#add_item').focus();
            ITEM_SELECTED = null;
        });

        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $('#search-customer-modal').click(function(){
            $('#myModal').modal({remote: site.base_url + 'customers/search/' + $("input[name=customer]").val()});
            $('#myModal').modal('show');
        });

        $('#search-customer-add-modal').click(function(){
            $('#myModal2').modal({remote: site.base_url + 'customers/search/' + $("input[name=customerClient]").val() + '/_add'});
            $('#myModal2').modal({
                backdrop: 'static',
                keyboard: false
            }).modal('show').css('z-index', 1060);
        });

        $("#enviar_faturar").click(function(){
            <?php if ($configPagSeguro->active) { ?>

            // cria uma sessão
            PagSeguroDirectPayment.setSessionId('<?php echo $sessionCode;?>');

            // obter meios de pagamento
            PagSeguroDirectPayment.getPaymentMethods({
                success: function(json){
                    console.log(json);
                },
                error: function(json){
                    console.log(json);
                    var erro = "";
                    for(i in json.errors){
                        erro = erro + json.errors[i];
                    }
                    alert(erro);
                },
                complete: function(json){
                }
            });

            // Obter identificação do comprador
            var senderHash = PagSeguroDirectPayment.getSenderHash();
            $("input[name='senderHash']").val(senderHash);

            <?php } else { ?>
            $("input[name='senderHash']").val('');
            <?php } ?>
        });

        $(document).on('click', '#addBusManuallyClient', function (e) {

            if (count == 1) {
                slitems = {};

                if ($('#slwarehouse').val() && $('#slcustomer').val()) {
                    //$('#slcustomer').select2("readonly", true);
                    $('#slwarehouse').select2("readonly", true);
                } else {
                    bootbox.alert(lang.select_above);
                    item = null;
                    return false;
                }
            }

            slitems = JSON.parse(localStorage.getItem('slitems'));

            if (slitems == null){
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }

            var produto = $('#productId').val();
            var programacaoId = $('#programacaoId').val();
            var tipoTransporte = $('#tipo_transporte').val();

            $('#iframe_onibus').attr('src', site.base_url + "products/bus/"+produto+'/'+tipoTransporte+'/'+programacaoId);

            $('#mnet_price').text('0.00');
            $('#mpro_tax').text('0.00');
            $('#mModal2').appendTo("body").modal('show');
            return false;
        });
    });

    function buscarTipoHospedagem(productId, tipoHospedagem, faixaId) {

        if (faixaId === null) return;

        $.ajax({
            url: site.base_url + "saleitem/getTipoHospedagem",
            dataType: 'json',
            type: 'get',
            //async: false,
            data: {
                productId : productId,
                faixaId: faixaId,
                programacao_id: $('#programacaoId').val(),
            }
        }).done(function (data) {

            if (data) {

                console.log('montando tipo de hospedagem');

                $('#tipo_hospedagem').empty();

                $('#div_tipo_hospedagem').hide();

                criarOpcaoSelecioneUmaOpcao('tipo_hospedagem');

                $(data).each(function (index, option) {

                    if (data.length === 1) tipoHospedagem = option.id;

                    let newOption = new Option(option.text, option.id, false, false);
                    newOption.setAttribute("preco", option.preco);

                    $('#tipo_hospedagem').append(newOption);
                    $('#div_tipo_hospedagem').show();
                });

                if (tipoHospedagem !== '') {
                    $('#tipo_hospedagem').val(tipoHospedagem).trigger('change');
                }
            }
        });
    }

    function selecionar_assento (tag) {
        let assento = tag.getAttribute('data-position-order_name');
        let status = tag.getAttribute('data-status');

        if (status === 'enabled') {
            $('#poltronaClient').val(assento);
            $('#mModal2').appendTo("body").modal('hide');
        }
    }

    function preencher_customer(customer_id) {

        var $customer = $('#slcustomer');

        $customer.val(customer_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "saleitem/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                        localStorage.setItem('slcustomer', customer_id);
                    }
                });
            },
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#myModal').modal('hide');
    }

    function preencher_customer_add(customer_id) {

        var $customerClient = $('#customerClient');

        $customerClient.val(customer_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "saleitem/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                        localStorage.setItem('slcustomer', customer_id);
                    }
                });
            },
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
        $('#myModal').modal('hide');
        $('#myModal2').modal('hide');
    }

    function atualizar_item() {
        if (slitems !== null) {
            $.each(slitems, function () {
                var item = this;
                if (item.supdate === 'I') {
                    delete slitems[item.id];
                }
            });

            localStorage.setItem('slitems', JSON.stringify(slitems));
            loadItems();
        }
    }


    function adicionarNovoItemVenda(item) {

        if (!controllerSales.isPreencheuFilial() ) {
            return null;
        }

        if (item == null) return null;

        var item_id = site.settings.item_addition === 1 ? item.item_id : item.id;

        if (slitems[item_id]) {
            slitems[item_id].row.qty = parseFloat(slitems[item_id].row.qty) + 1;
        } else {
            slitems[item_id] = item;
        }

        slitems[item_id].order = new Date().getTime();

        slitems[item_id].supdate = 'I';

        localStorage.setItem('slitems', JSON.stringify(slitems));

        loadItems();

        //limparDadosFormularioPacotes();

        //audio_success.play();

        return slitems[item_id];
    }
</script>
