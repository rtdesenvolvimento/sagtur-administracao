<div class="row" style="margin-top: 12px;">
    <div class="col-md-12" style="margin-top: -20px;">
        <div class="form-group" style="float: left;width: 10%;">
            <?= lang("companhiaPassagemAerea", "companhiaPassagemAerea"); ?>*
            <?php echo form_input('companhiaPassagemAerea[]', (isset($_POST['companhiaPassagemAerea']) ? $_POST['companhiaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio" placeholder="CIA"'); ?>
        </div>
        <div class="form-group" style="float: left;width: 16%;">
            <?= lang("origemPassagemAerea", "slOrigemPassagemAerea"); ?>*
            <?php echo form_input('slOrigemPassagemAerea[]', (isset($_POST['slOrigemPassagemAerea']) ? $_POST['slOrigemPassagemAerea'] : ""), 'class="form-control_aereo input-tip autocomplete_origem_destino obrigatorio" placeholder="Digite uma origem/aeroporto"'); ?>
        </div>
        <div class="form-group" style="float: left;width: 16%;">
            <?= lang("destinoPassagemAerea", "slDestinoPassagemAerea"); ?>*
            <?php echo form_input('destinoPassagemAerea[]', (isset($_POST['destinoPassagemAerea']) ? $_POST['destinoPassagemAerea'] : ""), 'class="form-control_aereo input-tip autocomplete_origem_destino obrigatorio" placeholder="Digite um destino/aeroporto"'); ?>
        </div>
        <div class="form-group" style="float: left;">
            <?= lang("dataSaidaPassagemAerea", "dataSaidaPassagemAerea"); ?>*
            <?php echo form_input('dataSaidaPassagemAerea[]', (isset($_POST['dataSaidaPassagemAerea']) ? $_POST['dataSaidaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'date'); ?>
        </div>
        <div class="form-group" style="float: left;">
            <?= lang("horaSaidaPassagemAerea", "horaSaidaPassagemAerea"); ?>*
            <?php echo form_input('horaSaidaPassagemAerea[]', (isset($_POST['horaSaidaPassagemAerea']) ? $_POST['horaSaidaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'time'); ?>
        </div>
        <div class="form-group" style="float: left;">
            <?= lang("dataChegadaPassagemAerea", "dataChegadaPassagemAerea"); ?>*
            <?php echo form_input('dataChegadaPassagemAerea[]', (isset($_POST['dataChegadaPassagemAerea']) ? $_POST['dataChegadaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'date'); ?>
        </div>
        <div class="form-group" style="float: left;">
            <?= lang("horaChegadaPassagemAerea", "horaChegadaPassagemAerea"); ?>*
            <?php echo form_input('horaChegadaPassagemAerea[]', (isset($_POST['horaChegadaPassagemAerea']) ? $_POST['horaChegadaPassagemAerea'] : ""), 'class="form-control_aereo input-tip obrigatorio"', 'time'); ?>
        </div>
        <div class="form-group" style="float: left;width: 6%;">
            <?= lang("vooPassagemAerea", "slVooPassagemAerea"); ?>*
            <?php echo form_input('vooPassagemAerea[]', (isset($_POST['vooPassagemAerea']) ? $_POST['vooPassagemAerea'] : ""), 'class="form-control_aereo obrigatorio"'); ?>
        </div>
        <div class="form-group" style="float: left;width: 6%;">
            <?= lang("assento", "assento"); ?>*
            <?php echo form_input('assento[]', (isset($_POST['assento']) ? $_POST['assento'] : ""), 'class="form-control_aereo obrigatorio"'); ?>
        </div>
        <div class="form-group" style="float: left;width: 6%;display: none;">
            <?= lang("bagagem", "bagagem"); ?>
            <?php echo form_input('bagagem[]', (isset($_POST['bagagem']) ? $_POST['bagagem'] : ""), 'class="form-control_aereo"'); ?>
        </div>
        <div class="form-group" style="float: left;margin-top: 25px;margin-left: 15px;font-size: 20px;">
            <i class="fa fa-trash removeItinerario" style="cursor: pointer"></i>
        </div>
    </div>
</div>