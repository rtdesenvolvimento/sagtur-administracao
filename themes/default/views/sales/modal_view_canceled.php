<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <?php if ($logo) { ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                         alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            <?php } ?>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                            <?= lang("condicao_pagamento"); ?>: <?= $inv->condicao_pagamento; ?><br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?php if (!empty($inv->return_sale_ref)) {
                            echo lang("return_ref").': '.$inv->return_sale_ref;
                            if ($inv->return_id) {
                                echo ' <a data-target="#myModal2" data-toggle="modal" href="'.site_url('salesutil/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                            } else {
                                echo '<br>';
                            }
                        } ?>
                        <?= lang("sale_status"); ?>: <?= lang($inv->sale_status); ?><br>
                        <?= lang("payment_status"); ?>: <?= lang($inv->payment_status); ?>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right">
                        <?php $br = $this->sma->save_barcode($inv->reference_no, 'code128', 70, false); ?>
                        <img src="<?= base_url() ?>assets/uploads/barcode<?= $this->session->userdata('user_id') ?>.png"
                             alt="<?= $inv->reference_no ?>"/>
                        <?php $this->sma->qrcode('link', urlencode(site_url('sales/view/' . $inv->id)), 2); ?>
                        <img src="<?= base_url() ?>assets/uploads/qrcode<?= $this->session->userdata('user_id') ?>.png"
                             alt="<?= $inv->reference_no ?>"/>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row" style="margin-bottom:15px;">
                <div class="col-xs-6">
                    <?php echo $this->lang->line("from"); ?>:
                    <h2 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>

                    <?php
                    echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;

                    echo "<p>";

                    if ($biller->vat_no != "-" && $biller->vat_no != "") {
                        echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                    }
                    if ($biller->cf1 != "-" && $biller->cf1 != "") {
                        echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                    }
                    if ($biller->cf2 != "-" && $biller->cf2 != "") {
                        echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                    }
                    if ($biller->cf3 != "-" && $biller->cf3 != "") {
                        echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                    }
                    if ($biller->cf4 != "-" && $biller->cf4 != "") {
                        echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                    }
                    if ($biller->cf5 != "-" && $biller->cf5 != "") {
                        echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                    }
                    if ($biller->cf6 != "-" && $biller->cf6 != "") {
                        echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                    }

                    echo "</p>";
                    echo lang("tel") . ": " . $biller->phone . "<br>" . lang("email") . ": " . $biller->email;
                    ?>
                </div>
                <div class="col-xs-6">
                    <?php echo $this->lang->line("to"); ?>:<br/>
                    <h2 style="margin-top:10px;"><?= $customer->company ? $customer->company : $customer->name; ?></h2>
                    <?= $customer->company ? "" : "Attn: " . $customer->name ?>

                    <?php
                    echo $customer->address . "<br>" . $customer->city . " " . $customer->postal_code . " " . $customer->state . "<br>" . $customer->country;

                    echo "<p>";

                    if ($customer->vat_no != "-" && $customer->vat_no != "") {
                        echo "<br>" . lang("vat_no") . ": " . $customer->vat_no;
                    }
                    if ($customer->cf1 != "-" && $customer->cf1 != "") {
                        echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                    }
                    if ($customer->cf2 != "-" && $customer->cf2 != "") {
                        echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                    }
                    if ($customer->cf3 != "-" && $customer->cf3 != "") {
                        echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                    }
                    if ($customer->cf4 != "-" && $customer->cf4 != "") {
                        echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                    }
                    if ($customer->cf5 != "-" && $customer->cf5 != "") {
                        echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                    }
                    if ($customer->cf6 != "-" && $customer->cf6 != "") {
                        echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                    }

                    echo "</p>";
                    echo lang("tel") . ": " . $customer->phone . "<br>" . lang("email") . ": " . $customer->email;
                    ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped print-table order-table">

                    <thead>

                    <tr>
                        <th><?= lang("no"); ?></th>
                        <th><?= lang("description"); ?></th>
                        <th><?= lang("quantity"); ?></th>
                        <th><?= lang("unit_price"); ?></th>
                        <?php
                        if ($Settings->tax1 && $inv->product_tax > 0) {
                            echo '<th>' . lang("tax") . '</th>';
                        }
                        if ($Settings->product_discount && $inv->product_discount != 0) {
                            echo '<th>' . lang("discount") . '</th>';
                        }
                        ?>
                        <th><?= lang("subtotal"); ?></th>
                    </tr>

                    </thead>

                    <tbody>

                    <?php $r = 1;
                    $tax_summary = array();
                    foreach ($rows as $row):

                        $customerClientName = $row->customerClientName;
                        $poltronaClient     = $row->poltronaClient;
                        $dadosClientePoltrona = '';

                        if ($customerClientName != '' && $customerClientName != 'undefined' && $customerClientName != '0') {

                            if ($poltronaClient != '') {
                                $dadosClientePoltrona = $customerClientName.'<br/>Poltrona '.$poltronaClient.'<br/>';
                            } else {
                                $dadosClientePoltrona = $customerClientName.'<br/>';
                            }

                        } else {

                            if ($poltronaClient != '') {
                                $dadosClientePoltrona = $customer->name.'<br/>Poltrona '.$poltronaClient.'<br/>';
                            } else {
                                $dadosClientePoltrona = $customer->name.'<br/>';
                            }
                        }

                    ?>
                        <tr>
                            <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                            <td style="vertical-align:middle;">
                                <!--
                                <a href="<?= base_url().'/sales/meuticket/'.$row->sale_id.'/'.$row->customerClient.'/' ?>" target="_blank">
                                <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                </a>
                                !-->
                                <?= $dadosClientePoltrona.$row->product_name; ?><?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                            </td>
                            <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity) ?></td>
                            <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                            <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                        </tr>
                        <?php
                        $r++;
                    endforeach;
                    if ($return_rows) {
                        foreach ($return_rows as $row):
                        ?>
                            <tr class="warning">
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_name . " (" . $row->product_code . ")" . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                </td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->unit; ?></td>
                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($row->real_unit_price); ?></td>
                                <?php
                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 && $row->tax_code ? '<small>('.$row->tax_code.')</small>' : '') . ' ' . $this->sma->formatMoney($row->item_tax) . '</td>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($row->item_discount) . '</td>';
                                }
                                ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($row->subtotal); ?></td>
                            </tr>
                            <?php
                            $r++;
                        endforeach;
                    }
                    ?>
                    </tbody>
                    <tfoot>
                    <?php
                    $col = 4;
                    if ($Settings->product_discount && $inv->product_discount != 0) {
                        $col++;
                    }
                    if ($Settings->tax1 && $inv->product_tax > 0) {
                        $col++;
                    }
                    if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 2;
                    } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                        $tcol = $col - 1;
                    } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                        $tcol = $col - 1;
                    } else {
                        $tcol = $col;
                    }
                    ?>
                    <?php if ($inv->grand_total != $inv->total) { ?>
                        <tr>
                            <td colspan="<?= $tcol; ?>"
                                style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                (<?= $default_currency->code; ?>)
                            </td>
                            <?php
                            if ($Settings->tax1 && $inv->product_tax > 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_tax+$return_sale->product_tax) : $inv->product_tax) . '</td>';
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                echo '<td style="text-align:right;">' . $this->sma->formatMoney($return_sale ? ($inv->product_discount+$return_sale->product_discount) : $inv->product_discount) . '</td>';
                            }
                            ?>
                            <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax)); ?></td>
                        </tr>
                    <?php } ?>
                    <?php if ($return_sale) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_total") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->grand_total) . '</td></tr>';
                        if ($return_sale->surcharge != 0) {
                            echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("return_surcharge") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                        }
                    }
                    ?>
                    <?php if ($inv->order_discount != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_discount+$return_sale->order_discount) : $inv->order_discount) . '</td></tr>';
                    }
                    ?>
                    <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</td></tr>';
                    }
                    ?>
                    <?php if ($inv->shipping != 0) {
                        echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $default_currency->code . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                    }
                    ?>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?></td>
                    </tr>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("paid"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid); ?></td>
                    </tr>
                    <tr>
                        <td colspan="<?= $col; ?>"
                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                            (<?= $default_currency->code; ?>)
                        </td>
                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney(($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid)); ?></td>
                    </tr>

                    </tfoot>
                </table>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?php if ($parcelasOrcamento  && $inv->sale_status == 'orcamento'){?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                                        <thead>
                                        <tr><th colspan="4" style="text-align: center;background: #73a9d0;color: #ffffff;font-weight: bold;">DADOS DA FATURA</th></tr>
                                        </thead>
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;">#</th>
                                            <th style="text-align: center;">Vencimento</th>
                                            <th style="text-align: center;">Cobrança</th>
                                            <th style="text-align: right;">Pagar</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($parcelasOrcamento as $parcela){?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $parcela->numero_parcela.'X';?></td>
                                                <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($parcela->dtVencimento));?></td>
                                                <td style="text-align: center;"><?php echo $parcela->tipoCobranca;?></td>
                                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($parcela->valorVencimento);?></td>
                                            </tr>
                                        <?php }?>
                                        </tbody>

                                        <tfoot>
                                        <?php
                                        $col = 3;
                                        if ($Settings->product_serial) {
                                            $col++;
                                        }
                                        if ( $Settings->product_discount && $inv->product_discount != 0) {
                                            $col++;
                                        }
                                        if ($Settings->tax1 && $inv->product_tax > 0) {
                                            $col++;
                                        }
                                        if ( $Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                            $tcol = $col - 2;
                                        } elseif ( $Settings->product_discount && $inv->product_discount != 0) {
                                            $tcol = $col - 1;
                                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                            $tcol = $col - 1;
                                        } else {
                                            $tcol = $col;
                                        }
                                        ?>
                                        <?php if ($inv->grand_total != $inv->total) {
                                            ?>
                                            <tr>
                                                <td colspan="<?= $tcol; ?>" style="text-align:right;"><?= lang('total'); ?>
                                                    (<?= $default_currency->code; ?>)
                                                </td>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                                                }
                                                if ( $Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                        <?php if ($return_sale && $return_sale->surcharge != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($inv->order_discount != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_tax') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($inv->shipping != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                                        }
                                        ?>
                                        <tr>
                                            <td colspan="5"
                                                style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                                        </tr>

                                        <tr>
                                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->paid); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php }?>

                    <?php if ($faturas){?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="clearfix"></div>
                                <div class="table-responsive">
                                    <table class="table table-bordered table-hover table-striped" style="width: 100%;">
                                        <thead>
                                        <tr><th colspan="6" style="text-align: center;color: #ffffff;font-weight: bold;">DADOS DA FATURA</th></tr>
                                        </thead>
                                        <thead>
                                        <tr>
                                            <th style="text-align: center;width: 5%;">#</th>
                                            <th style="text-align: center;">Vencimento</th>
                                            <th style="text-align: center;">Cobrança</th>
                                            <th style="text-align: right;">Pagar</th>
                                            <th style="text-align: right;">Recebido</th>
                                            <th style="text-align: center;">Status</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($faturas as $fatura) {

                                            $parcela = $this->site->getParcelaOneByFatura($fatura->id);
                                            $cobranca = $this->site->getCobrancaIntegracaoByFatura($fatura->id);
                                            $tipoCobranca = $this->site->getTipoCobrancaById($fatura->tipoCobrancaId);

                                            ?>
                                            <tr>
                                                <td style="text-align: center;"><?php echo $fatura->numeroparcela.'X';?></td>
                                                <td style="text-align: center;"><?php echo date('d/m/Y', strtotime($fatura->dtvencimento));?></td>
                                                <td style="text-align: center;">
                                                    <?php echo $fatura->tipoCobranca;?>
                                                    <br/><?php echo $fatura->reference;?>
                                                    <?php if (count($cobranca) > 0 && $tipoCobranca->tipo == 'boleto') {?>
                                                        <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                                    <?php } ?>
                                                </td>
                                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></td>
                                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($fatura->valorpago);?></td>
                                                <td style="text-align: center;"><?php echo lang($fatura->status);?></td>
                                            </tr>
                                        <?php }?>
                                        </tbody>

                                        <tfoot>
                                        <?php
                                        $col = 5;
                                        if ($Settings->product_serial) {
                                            $col++;
                                        }
                                        if ( $Settings->product_discount && $inv->product_discount != 0) {
                                            $col++;
                                        }
                                        if ($Settings->tax1 && $inv->product_tax > 0) {
                                            $col++;
                                        }
                                        if ( $Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1 && $inv->product_tax > 0) {
                                            $tcol = $col - 2;
                                        } elseif ( $Settings->product_discount && $inv->product_discount != 0) {
                                            $tcol = $col - 1;
                                        } elseif ($Settings->tax1 && $inv->product_tax > 0) {
                                            $tcol = $col - 1;
                                        } else {
                                            $tcol = $col;
                                        }
                                        ?>
                                        <?php if ($inv->grand_total != $inv->total) {
                                            ?>
                                            <tr>
                                                <td colspan="<?= $tcol; ?>" style="text-align:right;"><?= lang('total'); ?>
                                                    (<?= $default_currency->code; ?>)
                                                </td>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_tax) . '</td>';
                                                }
                                                if ( $Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($inv->product_discount) . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right;"><?= $this->sma->formatMoney($inv->total + $inv->product_tax); ?></td>
                                            </tr>
                                        <?php }
                                        ?>
                                        <?php if ($return_sale && $return_sale->surcharge != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('surcharge') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($return_sale->surcharge) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($inv->order_discount != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_discount') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_discount) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('order_tax') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->order_tax) . '</td></tr>';
                                        }
                                        ?>
                                        <?php if ($inv->shipping != 0) {
                                            echo '<tr><td colspan="' . $col . '" style="text-align:right;">' . lang('shipping') . ' (' . $default_currency->code . ')</td><td style="text-align:right;">' . $this->sma->formatMoney($inv->shipping) . '</td></tr>';
                                        }
                                        ?>

                                        <tr>
                                            <td colspan="5"
                                                style="text-align:right; font-weight:bold;"><?= lang('total_amount'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total); ?></td>
                                        </tr>

                                        <tr>
                                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('paid'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->paid); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align:right; font-weight:bold;"><?= lang('balance'); ?>
                                                (<?= $default_currency->code; ?>)
                                            </td>
                                            <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatMoney($inv->grand_total - $inv->paid); ?></td>
                                        </tr>

                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php }?>

                    <?php if ($payments) { ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-condensed print-table">
                                        <thead>
                                        <tr>
                                            <th><?= lang('date') ?></th>
                                            <th><?= lang('payment_reference') ?></th>
                                            <th><?= lang('paid_by') ?></th>
                                            <th><?= lang('amount') ?></th>
                                            <th><?= lang('created_by') ?></th>
                                            <th><?= lang('type') ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($payments as $payment) {

                                            $parcela = $this->site->getParcelaOneByFatura($payment->fatura);
                                            $cobranca = $this->site->getCobrancaIntegracaoByFatura($payment->fatura);
                                            ?>
                                            <tr <?= $payment->status == 'ESTORNO' ? 'class="warning" style="text-decoration: line-through;"' : ''; ?>>
                                                <td><?= $this->sma->hrld($payment->date) ?><?= $payment->motivo_estorno; ?></td>
                                                <td><?= $payment->reference_no; ?></td>
                                                <td><?= lang($payment->paid_by);
                                                    if ($payment->paid_by == 'gift_card' || $payment->paid_by == 'CC') {
                                                        echo ' (' . $payment->cc_no . ')';
                                                    } elseif ($payment->paid_by == 'Cheque') {
                                                        echo ' (' . $payment->cheque_no . ')';
                                                    }
                                                    ?>

                                                    <?php if (count($cobranca) > 0) {?>
                                                        <br/><?php echo ' Código de Integração: '.$cobranca->code;?>
                                                    <?php } ?>

                                                    <?php if ($payment->note){?>
                                                        <br/><small><?=$payment->note;?></small>
                                                    <?php } ?>
                                                </td>
                                                <td><?= $this->sma->formatMoney($payment->amount) . ' ' . (($payment->attachment) ? '<a href="' . base_url('assets/uploads/' . $payment->attachment) . '" target="_blank"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                                <td><?= $payment->first_name . ' ' . $payment->last_name; ?></td>
                                                <td><?= lang($payment->type); ?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <?php
                        if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php
                        }
                        if ($inv->staff_note || $inv->staff_note != "") { ?>
                            <div class="well well-sm staff_note">
                                <p class="bold"><?= lang("staff_note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->staff_note); ?></div>
                            </div>
                        <?php } ?>
                </div>

                <?php if ($customer->award_points != 0 && $Settings->each_spent > 0) { ?>
                <div class="col-xs-5 pull-left">
                    <div class="well well-sm">
                        <?=
                        '<p>'.lang('this_sale').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                        .'<br>'.
                        lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>';?>
                    </div>
                </div>
                <?php } ?>

                <div class="col-xs-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                        <?php if ($inv->updated_by) { ?>
                        <p>
                            <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                            <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                        </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php if (!$Supplier || !$Customer) { ?>
                <div class="buttons">
                    <div class="btn-group btn-group-justified">
                        <?php if ($inv->attachment) { ?>
                            <div class="btn-group">
                                <a href="<?= site_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                                    <i class="fa fa-chain"></i>
                                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                                </a>
                            </div>
                        <?php } ?>
                        <div class="btn-group">
                            <a href="<?= site_url('sales/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-primary" title="<?= lang('email') ?>">
                                <i class="fa fa-envelope-o"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href="<?= site_url('salesutil/pdf/' . $inv->id) ?>" class="tip btn btn-primary" title="<?= lang('download_pdf') ?>">
                                <i class="fa fa-download"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                            </a>
                        </div>
                        <?php if ( ! $inv->sale_id) { ?>
                        <div class="btn-group">
                            <a href="<?= site_url('sales/edit/' . $inv->id) ?>" class="tip btn btn-warning sledit" title="<?= lang('edit') ?>">
                                <i class="fa fa-edit"></i>
                                <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                            </a>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
