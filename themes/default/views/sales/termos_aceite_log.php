<!-- saved from url=(0014)about:internet -->
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="urn:layr:template">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Recibo</title>
    <link href="<?= $assets ?>styles/recibo.css" rel="stylesheet"/>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
</head>
<body style="background-color: white;">
<div id="non-printable" class="actions-div">
    <button class="btn" style="cursor: pointer;" onclick="window.close();">Cancelar</button>
    <button class="btn btn-primary print-btn" style="cursor: pointer;" onclick="window.print();">Imprimir Recibo</button>
</div>
<div id="printable" class="receipt-container">
    <hr class="solid-line">
    <div class="receipt-info-container">
        <div class="container-company-logo">
            <table class="company-logo" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td>
                        <?php if ($Settings->logo) { ?>
                            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                                 alt="<?= $this->Settings->site_name ?>"
                                 class="company-logo" style="height: auto;" id="act-logo-topo-novo" align="middle">
                        <?php } else { ?>
                            <img src="<?= $assets ?>/images/image-logo-padrao.png" class="company-logo" id="act-logo-topo-novo" align="middle">
                        <?php }?>
                    </td>
                    <td>
                        <div class="receipt-info-column-1">
                            <div><span style="font-size: 15px;"><strong><?php echo $this->Settings->site_name;?></strong></span></div>
                            <div class="company-info-container">
                                <strong>CNPJ: <?php echo $biller->vat_no;?> </strong>
                            </div>
                            <div class="company-address-container">
                                <?php echo $biller->phone?> - <?php echo $biller->email?>
                                <br/>
                                <?php  echo $biller->address . '<br />' . $biller->city . ' ' . $biller->postal_code . ' ' . $biller->state?>
                            </div>
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>


    </div>
    <hr class="solid-line">
    <table style="width: 100%; padding: 30px 0px 5px;">
        <tbody>
        <tr>
            <td style="flex: 1; font-size: 30px;text-align: center;"><strong><span class="condensed">COMPROVANTE DE ACEITE DE COMPRA</span></strong></td>
        </tr>
        </tbody>
    </table>
    <hr class="dotted-line">
    <div class="receipt-detail-container" style="padding: 0px;">

        <div class="receipt-details">

            <?php $itens =  $this->sales_model->getAllInvoiceItems($inv->id); ?>

            <h3>Dados do Cliente Responsável pela Compra</h3>
            Cliente: <?=$customer->name;?><br/>
            CPF: <?=$customer->vat_no;?><br/>
            <?=strtoupper($customer->tipo_documento);?>: <?=$customer->cf1;?><br/>
            Telefone: <?=$customer->cf5;?><br/>
            E-mail: <?=$customer->email;?><br/>
            <hr class="dotted-line">
            <h3>Dados do Vendedor</h3>
            Vendedor: <?=$biller->name;?><br/>
            E-mail: <?=$biller->email;?><br/>

            <hr class="dotted-line">
            <h3>Dados da Compra</h3>
            Código da compra:<?=$inv->reference_no;?> <br/>
            Data da compra: <?=$this->sma->hrld($inv->date);?><br/>
            Serviço(s):<br/>
            <?php foreach ($itens as $item){?>
                <strong><?php echo ' '.$item->product_name;?></strong>.
            <?php }?>
            <hr class="dotted-line">
            <h3>Dados Técnicos Captados Para Rastrear Compra Online</h3>
            <?php if ($event->date){?> <?=lang('date')?>: <?=$this->sma->hrld($event->date);?><br/> <?php }?>
            <?php if ($event->ip_address){?> <?=lang('ip_address')?>: <?=$event->ip_address;?><br/> <?php }?>
            <?php if ($event->sessions_id){?> <?=lang('sessions_id')?>: <?=$event->sessions_id;?><br/> <?php }?>
            <?php if ($event->page_web){?> <?=lang('page_web')?>: <?=$event->page_web;?><br/> <?php }?>
            <?php if ($event->server_name){?> <?=lang('server_name')?>: <?=$event->server_name;?><br/> <?php }?>
            <?php if ($event->server_port){?> <?=lang('server_port')?>: <?=$event->server_port;?><br/> <?php }?>
            <?php if ($event->server_signature){?> <?=lang('server_signature')?>: <?=$event->server_signature;?><br/> <?php }?>
            <?php if ($event->server_software){?> <?=lang('server_software')?>: <?=$event->server_software;?><br/> <?php }?>
            <?php if ($event->http_referer){?> <?=lang('http_referer')?>: <?=$event->http_referer;?><br/> <?php }?>
            <?php if ($event->http_method){?> <?=lang('http_method')?>: <?=$event->http_method;?><br/> <?php }?>
            <?php if ($event->http_response_code){?> <?=lang('http_response_code')?>: <?=$event->http_response_code;?><br/> <?php }?>
            <?php if ($event->browser_information_name){?> <?=lang('browser_information_name')?>: <?=$event->browser_information_name;?><br/> <?php }?>
            <?php if ($event->browser_information_version){?> <?=lang('browser_information_version')?>: <?=$event->browser_information_version;?><br/> <?php }?>
            <?php if ($event->user_agent_info){?> <?=lang('user_agent_info')?>: <?=$event->user_agent_info;?><br/> <?php }?>

            <hr class="dotted-line">
            <h3>Termos de Aceite</h3>
            Aceitou os termos abaixo: [ X ] SIM<br/>
            Termos de aceite: <br/>
            <?=$event->termos_aceite;?>

            <br/><br/> <br/><br/>
        </div>

        <?php if ($Settings->assinatura){?>
            <div style="padding: 5px;"> <img src="<?= base_url() ?>assets/uploads/logos/shop/<?= $Settings->assinatura ?>"/></div>
            __________________________________________________
        <?php } else { ?>
            __________________________________________________
        <?php } ?>
        <div class="receipt-company">
            <?= $this->Settings->site_name ?><br>
        </div>
    </div>
</div>


</body>
</html>