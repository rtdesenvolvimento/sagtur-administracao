<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('whatsapp_send'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <?= lang("name", 'name'); ?>
                <?php echo form_input('name', $customer->name, 'class="form-control" id="name" required="required"  disabled '); ?>
            </div>
            <div class="form-group">
                <?= lang("phone", 'phone'); ?>
                <?php
                $phone = str_replace('(', '', str_replace(')', '', $customer->cf5));
                $phone = str_replace('-', '', $phone);
                $phone = str_replace(' ', '', $phone);
                ?>
                <?php echo form_input('phone', '55'+$phone, 'class="form-control" id="phone" required="required" '); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_button('send_message', lang('send_message'), 'class="btn btn-primary" id="abrir"'); ?>
        </div>
    </div>

</div>
<?= $modal_js ?>

<script type="application/javascript">
    $(document).ready(function () {
        $('#abrir').click(function () {
           console.log('click');

            let phone =  $('#phone').val();
            window.open('https://api.whatsapp.com/send?phone='+phone, '_blank');
        });
    });

</script>
