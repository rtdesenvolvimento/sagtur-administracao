<style>
    small {
        font-size: 12px; !important;
    }
    .stick {
        position: fixed;
        top: 0px;
        margin-left: 10%;
    }
</style>

<div class="row">
    <div class="col-lg-5">
        <div class="box">
            <div class="box-header">
                <h2 class="blue">
                    <i class="fa-fw fa fa-bus"></i>Informações sobre o tipo de transporte.
                </h2>
            </div>
            <div class="box-content">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="<?php echo base_url().'sales/montarPoltronas'?>" method="get" class="bv-form">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-12" style="display: none;">
                                        <label>Filtrar PAX por Ônibus</label>
                                        <select class="form-control" name="variacaoFilter" id="variacaoFilter">
                                            <option value="">TODOS</option>
                                            <?php
                                            foreach ($variacoes as $variacao){
                                                $suppler = $this->site->getCompanyByID($onibus->fornecedor);
                                                ?>
                                                <?php if ($variacao->id==$variacaoFilter){?>
                                                    <option value="<?php echo $variacao->id;?>" selected="selected"><?php echo '#'.$variacao->id.' ('.$variacao->name.' - '.$suppler->name.') '.$variacao->localizacao;?></option>
                                                <?php } else {?>
                                                    <option value="<?php echo $variacao->id;?>"><?php echo '#'.$variacao->id.' ('.$variacao->name.' - '.$suppler->name.') '.$variacao->localizacao;?></option>
                                                <?php }?>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-lg-12">
                                        <label>Ônibus selecionado para embarque</label>
                                        <select class="form-control" name="idVariante" id="idVariante">
                                            <?php
                                            foreach ($tiposTransporte as $tipoTransporte){?>
                                                <?php if ($tipoTransporte->id== $idVariante){?>
                                                    <option value="<?php echo $tipoTransporte->id;?>" selected="selected"><?php echo $tipoTransporte->text;?></option>
                                                <?php } else {?>
                                                    <option value="<?php echo $tipoTransporte->id;?>"><?php echo $tipoTransporte->text;?></option>
                                                <?php }?>
                                            <?php }?>
                                        </select>
                                    </div>
                                    <div class="col-lg-9">
                                        <label>Filtrar PAX por Local de embarque</label>
                                        <select class="form-control" name="localEmbarque" id="localEmbarque">
                                            <option value="">TODOS</option>
                                            <?php
                                            foreach ($embarques as $tipoEmbarque){?>
                                                <?php if ($tipoEmbarque->id == $embarqueFilter){?>
                                                    <option value="<?php echo $tipoEmbarque->id;?>" selected="selected"><?php echo $tipoEmbarque->name.' '.$this->sma->hf($tipoEmbarque->horaEmbarque);?></option>
                                                <?php } else {?>
                                                    <option value="<?php echo $tipoEmbarque->id;?>"><?php echo $tipoEmbarque->name.' '.$this->sma->hf($tipoEmbarque->horaEmbarque);?></option>
                                                <?php }?>
                                            <?php }?>
                                        </select>
                                        <input type="hidden" value="<?php echo $product_id;?>" name="id"/>
                                    </div>
                                    <div class="col-lg-3">
                                        <label>Exibir</label>
                                        <select class="form-control" name="sem_poltronas" id="sem_poltronas">
                                            <option value="">TODOS</option>
                                            <option value="S" <?php if($sem_poltronas == 'S') echo 'selected="selected"';?>>Sem poltronas</option>
                                            <option value="N" <?php if($sem_poltronas == 'N') echo 'selected="selected"';?> >Com poltronas</option>
                                        </select>

                                        <input type="hidden" value="<?php echo $product_id;?>" name="id"/>
                                        <input type="hidden" value="<?php echo $tipoTransporteId;?>" name="tipoTransporteId"/>
                                        <input type="hidden" value="<?php echo $programacaoId;?>" name="programacaoId"/>
                                    </div>
                                    <div class="col-lg-12" style="margin-top: 10px;">
                                        <button type="submit" value="Enviar" class="btn btn-primary">Buscar</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row" style="margin-top: 15px;">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-12">
                                <small><b style="color: #F45750">Deixe um 'C' depois do número para crianças de colo. Ex: 45C</b></small> <br/>
                                <table id="" style="cursor: pointer;" class="table table-bordered table-hover table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 1%;text-align: center">#</th>
                                        <th style="width: 40%;text-align: left;">Passageiros</th>
                                        <th style="display: none;">#</th>
                                        <th>Poltrona</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $idsFilter      = '';
                                    $idsFilterNotIn = '';
                                    $contador = 1;
                                    $sales_id = null;
                                    $tipoTransporteRef = '';

                                    foreach ($itens_pedidos as $row) {

                                        $objCustomer 	 = $this->site->getCompanyByID($row->customerClient);
                                        $totalDeItens    = $this->sales_model->buscarTotalDeItensDeUmaVendaPorVenda($row->sale_id);

                                        $embarque = '';
                                        $tipoTransporteRodoviario = '';
                                        $tipoFaixaEtaria = $row->faixaNome;
                                        $poltronaClient  = $row->poltronaClient;
                                        $localEmbarqueEntigo = $row->local_saida;
                                        $posicao_assento = '';

                                        if ($sem_poltronas == 'S' && $poltronaClient != '') {
                                            continue;
                                        }

                                        if ($sem_poltronas == 'N' && $poltronaClient == '') {
                                            continue;
                                        }


                                        if ($row->tipoTransporte)  {
                                            $tipoTransporteRodoviario = '<i class="fa fa-bus"></i> '.$row->tipoTransporteRodoviario;
                                        }

                                        if ($row->posicao_assento) {
                                            $posicao_assento = '<br/><i class="fa fa-info-circle"></i> '.$row->posicao_assento;
                                        }

                                        $note_sale  = strip_tags(html_entity_decode($row->note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                        $note_item  = strip_tags(html_entity_decode($row->note_item, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));
                                        $note_sale_admin  = strip_tags(html_entity_decode($row->staff_note, ENT_QUOTES | ENT_XHTML | ENT_HTML5, 'UTF-8'));

                                        list($ano, $mes, $dia) = explode('-', $objCustomer->data_aniversario);

                                        $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                                        $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                                        $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                                        $note = '';

                                        if ($note_item) {
                                            $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota do Cliente:. </b>'.$note_item.'</small>';
                                        }

                                        if ($note_sale) {
                                            $note .= '<br/><small><i class="fa fa-comments"></i> <b>Nota da Venda:. </b>'.$note_sale.'</small>';
                                        }

                                        if ($note_sale_admin) {
                                            $note .= '<br/><small><i class="fa fa-comment"></i> <b>Nota do Admin:.</b> '.$note_sale_admin.'</small>';
                                        }

                                        if ($localEmbarqueEntigo) {
                                            $localEmbarqueEntigo = '<br/><small><i class="fa fa-info"></i> '.$localEmbarqueEntigo.'</small>';
                                        }

                                        ?>

                                        <?php if ($row->tipoTransporte != $tipoTransporteRef) {
                                            $contador = 1;
                                            ?>
                                            <tr>
                                                <td colspan="3" style="background: #3c763d;font-size: 25px;color: #ffff;vertical-align: top;">
                                                    <?php echo $tipoTransporteRodoviario;?>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                        <?php $tipoTransporteRef = $row->tipoTransporte;?>

                                        <tr>
                                            <td style="text-align: center;">
                                                <a href="<?php echo base_url().'sales/edit/'.$row->sale_id?>" target="<?php echo $row->sale_id;?>"><?php echo $contador;?></a>
                                            </td>
                                            <td>
                                                <?php echo '<b style="font-size: 18px;">'.
                                                    $objCustomer->name.'</b><small>'.' ('.lang($tipoFaixaEtaria).')'.'</small>
                                                    <br/><small> <i class="fa fa-ticket"></i> '.$row->reference_no.'</small>'.$localEmbarqueEntigo.$posicao_assento.$note.'
                                                    <br/><small> <i class="fa fa-birthday-cake"></i>  '. $this->sma->hrsd($objCustomer->data_aniversario) .' - '.$idade.' anos</small>';?>
                                                <br/>
                                                <label style="margin-top: 10px;">Transporte e Embarque Atual:</label><br/>
                                                <select class="opt-tipo-transporte" id="opt-tipo-transporte<?=$row->id_item;?>" style="margin-bottom: 5px;">
                                                    <option value="">--Selecione--</option>
                                                    <?php
                                                    foreach ($tiposTransporte as $tipoTransporteM){?>
                                                        <?php if ($tipoTransporteM->id== $row->tipoTransporte){?>
                                                            <option value="<?php echo $tipoTransporteM->id;?>" itemId="<?=$row->id_item;?>" selected="selected"><?php echo $tipoTransporteM->text;?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $tipoTransporteM->id;?>" itemId="<?=$row->id_item;?>"><?php echo $tipoTransporteM->text;?></option>
                                                        <?php }?>
                                                    <?php }?>
                                                </select>


                                                <?php
                                                if (!empty($embarques)) {

                                                    $option = '';
                                                    $bol    = false;

                                                    foreach ($embarques as $tipoEmbarque){
                                                        if ($tipoEmbarque->id == $row->localEmbarque) {
                                                            $option .= '<option itemId="'.$row->id_item.'" value="'.$tipoEmbarque->id.'" selected="selected">'.$tipoEmbarque->name.'</option>';
                                                            $bol = true;
                                                        } else {
                                                            $option .= '<option itemId="'.$row->id_item.'" value="'.$tipoEmbarque->id.'">'.$tipoEmbarque->name.'</option>';
                                                        }
                                                    }

                                                    //if (!$bol) $option .= '<option  itemId="'.$row->id_item.'" selected="selected" value="'.$tipoEmbarque->id.'">'.$tipoEmbarque->name.'</option>';

                                                    if (!$bol) {
                                                        $sl = '
                                                    <select style="background: #a94442;color: #FFFFFF;" class="opt-embarques" style="width: 50%;margin-bottom: 5px;">
                                                        <option value="">--selecione--</option>'
                                                            .$option.'
                                                    </select>';
                                                    } else {
                                                        $sl = '<select class="opt-embarques" style="width: 50%;margin-bottom: 10px;""><option value="">--selecione--</option>'.$option.'</select>';
                                                    }

                                                    $embarque = $sl;
                                                }
                                                ?>
                                                <?=$embarque;?>
                                            </td>
                                            <td style="width: 10%;"><input type="text" style="text-align: right;" sale_id="<?php echo $row->sale_id;?>" id_item="<?php echo $row->id_item;?>" tipoTransporte="<?php echo $row->tipoTransporte;?>" min="0" name="poltrona" poltrona="<?php echo $poltronaClient;?>" value="<?php echo $poltronaClient;?>" class="form-control input-tip poltronas" id="slref" data-original-title="" title=""></td>
                                        </tr>
                                        <?php $sale_id = $row->sale_id; $contador = $contador + 1; }?>
                                    </tbody>
                                </table>
                                <b style="color: #F45750">
                                    ATENÇÃO: O local de embarque é obrigatório para que o cliente seja exibido no relatório de embarque.<br/>
                                    Caso os passageiros não apareçam no relatório de embarque clique no botão abaixo "Atribuir todos a este veículo".
                                    <br/> <br/><i class="fa fa-youtube"></i> <a href="https://youtu.be/O4HhRm4mc1I" target="_blank">
                                        Clique aqui para ver um tutorial de como resolver
                                        se os seus passageiros não estão sendo exibidos na lista de embarque.</a>
                                </b>
                            </div>
                        </div>
                        <div class="buttons" style="margin-top: 10px;">
                            <div class="btn-group btn-group-justified">
                                <div class="btn-group">
                                    <a href="<?= site_url('sales/relatorioEmbarqueAgrupadoPorLocalDeEmbarque/'.$product_id.'/'.$tipoTransporteId.'/'.$programacaoId) ?>"
                                       class="tip btn btn-primary" title="" target="_blank">
                                        <i class="fa fa-bus"></i> <span class="hidden-sm hidden-xs">Imprimir Lista de Embarque</span>
                                    </a>
                                    <button type="button" class="btn btn-danger" onclick="atribuirTodosAoVeiculo();">Clique aqui para atribuir todos ao ônibus atual <br/>Cuidado!!! Todos os passageiros serão alterados para o ônibus selecionado.</button>
                                    <a href="javascript:history.back();" class="tip btn btn-info" title="">
                                        <i class="fa fa-backward"></i> <span class="hidden-sm hidden-xs">Voltar</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <style>
                        <?php if ($tipoTransporte->automovel_id) {?>
                        .iframe_onibus {
                            position: fixed;
                            top: 95px;
                            z-index: 9999;
                            height: 810px;
                            width: 46%;
                            border: 1px solid #dbdee0;
                            cursor: crosshair;
                            transition: top 0.5s ease-out; /* Adiciona uma transição suave */
                        }
                        <?php } else { ?>
                        .iframe_onibus {
                            position: fixed;
                            top: 10px;
                            z-index: 9999;
                            height: 810px;
                            width: 46%;
                            cursor: crosshair;
                            transition: top 0.5s ease-out; /* Adiciona uma transição suave */
                        }
                        <?php } ?>

                    </style>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="row">
            <div class="col-lg-12">
                <iframe frameborder="0" scrolling="no"  id="iframe_onibus" class="iframe_onibus"></iframe>
            </div>
        </div>
    </div>
</div>

<script>

    window.onload = function() {
        $('.opt-embarques').select2('destroy');
        $('.opt-embarques').css('width','50%');

        $('.opt-tipo-transporte').select2('destroy');
        $('.opt-tipo-transporte').css('width','50%');
    };

    $(document).ready(function(){

        let tipoTransporte = $('#idVariante').val();

        $('#iframe_onibus').attr('src', site.base_url + "products/bus/" + <?php echo $product_id;?> + '/' + tipoTransporte + '/' +<?php echo $programacaoId;?>);

        $('.poltronas').blur(function (ex) {

            var item = $(this).attr('id_item');
            var tipoTransporte = $('#opt-tipo-transporte'+item).val();

            atribuirPoltronas(item, tipoTransporte,  $(this).val(), this);
        });

        $('.opt-embarques').change(function (){

            var localEmbarque = $(this).val();
            var itemId = $('option:selected', this).attr('itemid');

            $.ajax({
                type: "get",
                async: false,
                data: {
                    localEmbarque : localEmbarque,
                    itemId: itemId
                },
                url: site.base_url + "sales/atribuir_local_embarque",
                dataType: "html",
                success: function (data) {}
            });

            const iframe = document.getElementById("iframe_onibus");
            let lastScrollTop = 0;

            window.addEventListener("scroll", function () {
                let st = window.pageYOffset || document.documentElement.scrollTop;

                if (st > lastScrollTop) {
                    iframe.style.top = "0";
                } else {
                    iframe.style.top = "95px";
                }
                lastScrollTop = st <= 0 ? 0 : st; // Para dispositivos móveis
            });
        });

        $('.opt-tipo-transporte').change(function (){

            var tipoTransporte = $(this).val();
            var itemId = $('option:selected', this).attr('itemid');

            $.ajax({
                type: "get",
                async: false,
                data: {
                    tipoTransporte : tipoTransporte,
                    itemId: itemId
                },
                url: site.base_url + "sales/atribuir_tipo_transporte",
                dataType: "html",
                success: function (data) {
                    let andar = getAndarIframeSelecionado();

                    $('#idVariante').val(tipoTransporte).change();
                    $('#iframe_onibus').attr('src', site.base_url + "products/bus/<?php echo $product_id;?>/"+tipoTransporte+"/<?php echo $programacaoId;?>?andar=" + andar);
                }
            });
        });
    });

    function getAndarIframeSelecionado() {

        const iframe = document.getElementById('iframe_onibus');

        if (iframe && iframe.contentDocument && iframe.contentDocument.getElementById) {
            const inputNoIframe = iframe.contentDocument.getElementById('one-floor-radio');
            if (inputNoIframe) {
                let primeiroAndar = document.getElementById('iframe_onibus').contentDocument.getElementById('one-floor-radio').checked;
                if (primeiroAndar) {
                    console.log('andar 1');
                    return 1;
                } else {
                    console.log('andar 2');
                    return 2;
                }
            }
        }

        return 1;
    }

    function atribuirTodosAoVeiculo() {

        if (confirm('Desja realmente atribuir todos esses passageiros a esse veículo?')) {

            let tipoTransporte = $('#idVariante').val();
            let embarque =  $('#localEmbarque').val();

            $.ajax({
                type: "get",
                async: false,
                url: site.base_url + "bus/atribuir_todos_ao_automovel/",
                data: {
                    'product_id' : <?=$product_id;?>,
                    'programacao_id' : <?=$programacaoId;?>,
                    'tipo_transporte_id' : tipoTransporte,
                    'embarque_id' : embarque,
                },
                dataType: "html",
                success: function (data) {
                    location.reload();
                }
            });
        }
    }

    function atribuirPoltronas(id_item, tipoTransporte, poltrona, tag) {

        $('#idVariante').val(tipoTransporte).change();

        $.ajax({
            type: "get",
            async: false,
            url: site.base_url + "bus/atribuir_assento/",
            data: {
                'product_id' : <?=$product_id;?>,
                'programacao_id' : <?=$programacaoId;?>,
                'tipoTransporte' : tipoTransporte,
                'itemId' : id_item,
                'poltrona' : poltrona,
            },
            dataType: "json",
            success: function (result) {
                if (result.ocupado) {
                    alert('Assento ocupado! Passageiro '+result.passageiro);
                    tag.value = tag.getAttribute('poltrona');
                } else  {
                    if (result.bloqueado) {
                        alert('Assento bloqueado! '+result.note);
                        tag.value = tag.getAttribute('poltrona');
                    } else {
                        let andar = getAndarIframeSelecionado();

                        tag.setAttribute('poltrona', tag.value);
                        $('#iframe_onibus').attr('src', site.base_url + "products/bus/<?php echo $product_id;?>/"+tipoTransporte+"/<?php echo $programacaoId;?>?andar=" + andar);
                    }
                }
            }
        });
    }
</script>