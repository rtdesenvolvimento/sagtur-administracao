<ul id="myTabMeusPacotes" class="nav nav-tabs">
    <li class=""><a href="#passageiro1" class="tab-grey" style="text-align: center;"><i class="fa fa-user" style="font-size: 20px;"></i><br/><span id="meu-pacote-new-contato"> <?= lang("label.novo.passageiro")?></span></a></li>
</ul>
<div class="tab-content">
    <div id="passageiro1" style="padding: 0px;" class="tab-pane fade in">
        <div class="box-header">
            <h2 class="blue"><?= lang("header.label.adicionar.passageiro")?></h2>
        </div>
        <div class="box-content">
            <div class="row">
                <div class="col-md-12 well">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("tipoContato", "tipoContato"); ?>
                                <select id="tipoContato" name="tipoContato" class="form-control" required="required">
                                    <option value="buscar">Contato existente</option>
                                    <option value="criar">Criar Novo Contato</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-10" id="view-cliente-ja-cadastrado">
                            <div class="form-group">
                                <?= lang("cliente", "search-customer"); ?>
                                <?php echo form_input('search-customer', (isset($_POST['search-customer']) ? $_POST['search-customer'] : ""), 'id="search-customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"'); ?>
                            </div>
                        </div>
                    </div>
                    <div id="view-novo-contato" style="display: none;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group company">
                                    <?= lang("sexo", "sexo"); ?>
                                    <select id="sexo" name="sexo" class="form-control" required="required">
                                        <option value="FEMININO">FEMININO</option>
                                        <option value="MASCULINO">MASCULINO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-10">
                                <div class="form-group person">
                                    <?= lang("name", "slName"); ?>
                                    <?php echo form_input('name', '', 'class="form-control tip" id="slName" autocomplete="off" data-bv-notempty="true"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("vat_no", "vat_no"); ?>
                                    <?php echo form_input('vat_no', '', 'class="form-control cpf" id="vat_no"'); ?>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <?= lang("email_address", "email_address"); ?>
                                    <input type="email" name="email"  id="email" class="form-control"/>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= lang('data_aniversario', 'data_aniversario'); ?>
                                    <input type="date" name="data_aniversario" value="" class="form-control tip" id="data_aniversario">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <div><b><?php echo lang("tipo_documento", "tipo_documento");?></b></div>
                                    <select id="tipo_documento" name="tipo_documento" class="form-control" required="required">
                                        <option value="rg">RG</option>
                                        <option value="passaporte">Passaporte</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="rg"><div id="div_rgPassaporte"><b>R.G</b></div></label>
                                    <?php echo form_input('cf1', '', 'class="form-control rg" id="cf1"'); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="emissaorg"><div id="div_vlPassaporteRG"><b>Emissão RG</b></div></label>
                                    <input type="date" name="validade_rg_passaporte" value="" class="form-control tip" id="validade_rg_passaporte">
                                </div>
                            </div>
                            <div class="col-md-2" id="div_orgaoemissor">
                                <div class="form-group">
                                    <?= lang("ccf3", "cf3"); ?>
                                    <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("naturalidade", "naturalidade"); ?>
                                    <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("phone", "phone"); ?>
                                    <input type="tel" name="phone" class="form-control" id="phone"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("ccf5", "cf5"); ?>
                                    <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("telefone_emergencia", "telefone_emergencia"); ?>
                                    <?php echo form_input('telefone_emergencia', '', 'class="form-control" id="telefone_emergencia"'); ?>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang("nQuarto", "nQuarto"); ?>
                                    <?php echo form_input('nQuarto', '', 'class="form-control input-tip" id="nQuarto"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <fieldset>
                                    <legend style="cursor: pointer;" id="field-view-endereco-new-contact">Clique aqui para ver endereço</legend>
                                    <div id="div_endereco" style="display: none;">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("postal_code", "postal_code"); ?>
                                                    <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group">
                                                    <?= lang("address", "address"); ?>
                                                    <?php echo form_input('address', '', 'class="form-control" id="address" '); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("country", "country"); ?>
                                                    <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("city", "city"); ?>
                                                    <?php echo form_input('city', '', 'class="form-control" id="city"'); ?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("state", "state"); ?>
                                                    <?php echo form_input('state', '', 'class="form-control" id="state"'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
                <ul id="myTabMeusPacotesParametros" class="nav nav-tabs" style="display: none;">
                    <li class=""><a href="#transporte" class="tab-grey" style="text-align: center;"><i class="fa fa-bus" style="font-size: 20px;"></i><br/>Transporte</a></li>
                    <li class=""><a href="#hospedagem" class="tab-grey" style="text-align: center;"><i class="fa fa-bed" style="font-size: 20px;"></i><br/> Hospedagem</a></li>
                    <li class=""><a href="#passeios-adicionais" class="tab-grey" style="text-align: center;"><i class="fa fa-map-signs" style="font-size: 20px;"></i><br/>Passeios adicionais</a></li>
                    <li class=""><a href="#outros-servicos" class="tab-grey" style="text-align: center;"><i class="fa fa-heart" style="font-size: 20px;"></i><br/>Outros Servços</a></li>
                </ul>
                <div  style="display: none;" class="tab-content">
                    <div id="transporte" style="padding: 0px;" class="tab-pane fade in">
                        <div class="box sales-table">
                            <div class="box-header">
                                <h2 class="blue">Verifique as informações do viajante</h2>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-md-12 well">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("transporte", "onibusId"); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                        <i class="fa fa-bus" style="font-size: 1.2em;"></i>
                                                    </div>
                                                    <?php
                                                    $aHotel[""] = "Executivo 56/leitos - JJê - Embarque Florianpolis";
                                                    $aHotel[""] = "Executivo 56 leitos - JJê - Embarque São José ";

                                                    foreach ($hoteis as $h) {
                                                        $aHotel[$h->id] = $h->company != '-' ? $h->company : $h->name;
                                                    }
                                                    echo form_dropdown('hotelId', $aHotel, (isset($_POST['hotelId']) ? $_POST['hotelId'] : ''), 'id="hotelId" name="hotelId" data-placeholder="' . lang("select") . ' ' . lang("hotel") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?= lang("poltrona", "poltrona"); ?>
                                                <div class="input-group">
                                                    <?php echo form_input('poltrona', '0', 'class="form-control input-tip" id="poltrona"'); ?>
                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                        <i class="fa fa-eye" style="font-size: 1.2em;"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("local_embarque", "localEmbarqueId"); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                        <i class="fa fa-thumbs-o-up" style="font-size: 1.2em;"></i>
                                                    </div>
                                                    <?php
                                                    $aLocaisEmbarque[""] = "Vila Olimbia";
                                                    foreach ($condicoesPagamento as $le) {
                                                        $aLocaisEmbarque[$le->id] = $le->company != '-' ? $le->company : $le->name;
                                                    }
                                                    echo form_dropdown('localEmbarqueId', $aLocaisEmbarque, (isset($_POST['localEmbarqueId']) ? $_POST['localEmbarqueId'] : $Settings->default_biller), 'id="localEmbarqueId" name="localEmbarqueId" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <?= lang('horaPrevisaoEmbarque', 'horaPrevisaoEmbarque'); ?>
                                                <input type="time" required="required" name="horaPrevisaoEmbarque" value="18:00" class="form-control tip" id="horaPrevisaoEmbarque" data-original-title="" title="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="hospedagem" style="padding: 0px;" class="tab-pane">
                        <div class="box sales-table">
                            <div class="box-header">
                                <h2 class="blue">Verifique as informações do viajante</h2>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"> Diária da hospedagem Check-in 29/04/2020 e Check-out 30/05/2020  <i class="fa fa-map-marker"></i> Concórdia</div>
                                        <div class="panel-body" style="padding: 5px;">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("hotel", "hotelId"); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            <i class="fa fa-bed" style="font-size: 1.2em;"></i>
                                                        </div>
                                                        <?php
                                                        $aHotel[""] = "HOTEL CONCÓRDIA";
                                                        foreach ($hoteis as $h) {
                                                            $aHotel[$h->id] = $h->company != '-' ? $h->company : $h->name;
                                                        }
                                                        echo form_dropdown('hotelId', $aHotel, (isset($_POST['hotelId']) ? $_POST['hotelId'] : ''), 'id="hotelId" name="hotelId" data-placeholder="' . lang("select") . ' ' . lang("hotel") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("acomodacao", "acomodacao"); ?>
                                                    <?php
                                                    $aTipoQuarto[1] = "Double";
                                                    $aTipoQuarto[2] = "Tripe";
                                                    $aTipoQuarto[3] = "Twin";

                                                    echo form_dropdown('tipoQuartoId', $aTipoQuarto, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("categoria", "categoria"); ?>
                                                    <?php
                                                    $aaCatTipoQuarto[1] = "Chale";
                                                    $aaCatTipoQuarto[2] = "Luxo";
                                                    $aaCatTipoQuarto[3] = "Bangalo";

                                                    echo form_dropdown('tipoQuartoId', $aaCatTipoQuarto, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("regime", "regime"); ?>
                                                    <?php
                                                    $cbRegime[1] = "All Incluse";
                                                    $cbRegime[2] = "Café da manha";
                                                    $cbRegime[3] = "Bangalo";

                                                    echo form_dropdown('tipoQuartoId', $cbRegime, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="panel panel-warning">
                                        <div class="panel-heading"> Diária da hospedagem Check-in 29/04/2020 e Check-out 30/05/2020  <i class="fa fa-map-marker"></i> Concórdia</div>
                                        <div class="panel-body" style="padding: 5px;">

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("hotel", "hotelId"); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                            <i class="fa fa-bed" style="font-size: 1.2em;"></i>
                                                        </div>
                                                        <?php
                                                        $aHotel[""] = "HOTEL CONCÓRDIA";
                                                        foreach ($hoteis as $h) {
                                                            $aHotel[$h->id] = $h->company != '-' ? $h->company : $h->name;
                                                        }
                                                        echo form_dropdown('hotelId', $aHotel, (isset($_POST['hotelId']) ? $_POST['hotelId'] : ''), 'id="hotelId" name="hotelId" data-placeholder="' . lang("select") . ' ' . lang("hotel") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("acomodacao", "acomodacao"); ?>
                                                    <?php
                                                    $aTipoQuarto[1] = "Double";
                                                    $aTipoQuarto[2] = "Tripe";
                                                    $aTipoQuarto[3] = "Twin";

                                                    echo form_dropdown('tipoQuartoId', $aTipoQuarto, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <?= lang("categoria", "categoria"); ?>
                                                    <?php
                                                    $aaCatTipoQuarto[1] = "Chale";
                                                    $aaCatTipoQuarto[2] = "Luxo";
                                                    $aaCatTipoQuarto[3] = "Bangalo";

                                                    echo form_dropdown('tipoQuartoId', $aaCatTipoQuarto, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group">
                                                    <?= lang("regime", "regime"); ?>
                                                    <?php
                                                    $cbRegime[1] = "All Incluse";
                                                    $cbRegime[2] = "Café da manha";
                                                    $cbRegime[3] = "Bangalo";

                                                    echo form_dropdown('tipoQuartoId', $cbRegime, (isset($_POST['tipoQuartoId']) ? $_POST['tipoQuartoId'] : $Settings->default_biller), 'id="tipoQuartoId" name="tipoQuartoId" data-placeholder="' . lang("select") . ' ' . lang("tipo_quarto") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="passeios-adicionais" style="padding: 0px;" class="tab-pane">
                        <div class="box sales-table">
                            <div class="box-header">
                                <h2 class="blue">Verifique as informações do viajante</h2>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">Passeios adicionais</div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="controls table-controls">
                                                    <table id="slTableDependentes" class="table items table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">#</th>
                                                            <th class="col-md-4" style="text-align: left;">Passeio</th>
                                                            <th class="col-md-2" style="text-align: left;">Data</th>
                                                            <th class="col-md-2" style="text-align: left;">Origem</th>
                                                            <th class="col-md-2" style="text-align: left;">Hora</th>
                                                            <th class="col-md-4" style="text-align: left;">R$</th>
                                                            <th class="col-md-2" style="text-align: left;">Ref</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                            <td>City Tour</td>
                                                            <td><input class="form-control" type="date" value="2020-02-40" /></td>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option>SAÍDA DO HOTEL</option>
                                                                </select>
                                                            </td>
                                                            <td><input class="form-control" type="time" value="12:00" /></td>
                                                            <td><input class="form-control" type="text" value="120.50" /></td>
                                                            <td><input class="form-control" type="text" value="" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                            <td>Trilha Ilha do Macaco - Concordia</td>
                                                            <td><input class="form-control" type="date" value="2020-02-40" /></td>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option>SAÍDA DO HOTEL</option>
                                                                </select>
                                                            </td>
                                                            <td><input class="form-control" type="time" value="12:00" /></td>
                                                            <td><input class="form-control" type="text" value="120.50" /></td>
                                                            <td><input class="form-control" type="text" value="" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                            <td>Escuna - Concordia</td>
                                                            <td><input class="form-control" type="date" value="2020-02-40" /></td>
                                                            <td>
                                                                <select class="form-control">
                                                                    <option>SAÍDA DO HOTEL</option>
                                                                </select>
                                                            </td>
                                                            <td><input class="form-control" type="time" value="12:00" /></td>
                                                            <td><input class="form-control" type="text" value="120.50" /></td>
                                                            <td><input class="form-control" type="text" value="" /></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="outros-servicos" style="padding: 0px;" class="tab-pane">
                        <div class="box sales-table">
                            <div class="box-header">
                                <h2 class="blue">Verifique as informações do viajante</h2>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading">Outros Serviços</div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="controls table-controls">
                                                    <table id="slTableDependentes" class="table items table-striped table-bordered table-condensed table-hover">
                                                        <thead>
                                                        <tr>
                                                            <th style="text-align: left;">#</th>
                                                            <th class="col-md-6" style="text-align: left;">Serviço</th>
                                                            <th class="col-md-4" style="text-align: left;">R$</th>
                                                            <th class="col-md-2" style="text-align: left;">Ref</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                            <td>Seguro</td>
                                                            <td>R$ 120,30</td>
                                                            <td><input class="form-control" type="text" value="" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td><input type="checkbox"/></td>
                                                            <td>Show Noturno</td>
                                                            <td>R$ 50,00</td>
                                                            <td><input class="form-control" type="text" value="" /></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("sale_descricao_servico", "sldescricaoservico"); ?>
                            <?php echo form_textarea('noteitem', (isset($_POST['noteitem']) ? $_POST['noteitem'] : ""), 'class="form-control" id="sldescricaoservico" style="margin-top: 10px; height: 100px;"'); ?>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("sale_obsrevacao_item", "slobservacaoitem"); ?>
                            <?php echo form_textarea('observacaoitem', (isset($_POST['observacaoitem']) ? $_POST['observacaoitem'] : ""), 'class="form-control" id="slobservacaoitem" style="margin-top: 10px; height: 100px;"'); ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>