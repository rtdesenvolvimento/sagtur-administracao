<?= form_open('financeiro/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('vehicle_fleet_panel'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li class="divider"></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;" class="divfilters">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("start_date", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("end_date", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("start_time", "start_time"); ?>
                                    <?php echo form_input('start_time', (isset($_POST['start_time']) ? $_POST['start_time'] : $start_time), 'class="form-control" id="start_time"', 'time'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("end_time", "end_time"); ?>
                                    <?php echo form_input('end_time', (isset($_POST['end_time']) ? $_POST['end_time'] : $end_time), 'type="date" class="form-control" id="end_time"', 'time'); ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

        <div class="row">
            <?php foreach ($carros as $carro) {?>
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><i class="fa-fw fa fa-car fa-2x"></i>
                        <span style="font-size: 2em;font-weight: 700;cursor: pointer;"><?= $carro->veiculo ?> </span>
                        <br/>
                        <div style="font-size: 12px;cursor: pointer;color: #777"><div class="text-center" style="text-transform: uppercase;"></div> <span class="label" style="background-color:#0044cc"><?= $carro->placa ?></span> <?= $carro->tipo_veiculo ?>/<?= $carro->categoria_veiculo;?></div>
                    </div>
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $carro->image ?>" alt="<?= $carro->veiculo ?>" class="img-responsive img-thumbnail"/>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="button" class="btn btn-primary" href="<?=site_url('location/add')?>" data-toggle="modal" data-target="#myModal2" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket"></i> <?= lang('book_now') ?></button>
                        <button type="button" class="btn btn-primary" href="<?=site_url('location/availability_searh_modal')?>" data-toggle="modal" data-target="#myModal3" data-backdrop="static" data-keyboard="false"><i class="fa fa-calendar"></i> <?= lang('availability_searh') ?></button>
                    </div>
                </div>
            </div>
            <?php }?>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#end_date').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#start_time').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#end_time').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });

    });
</script>

