<link href="<?= $assets ?>js/flatpickr/css/flatpickr.min.css" rel="stylesheet"/>
<link href="<?= $assets ?>js/flatpickr/css/theme/material_blue.css" rel="stylesheet"/>

<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('lcdate')) {
            $("#lcdate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        <?php } ?>
    });
</script>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_location'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-location-form');
        echo form_open_multipart("location/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="row">
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("date", "lcdate"); ?>
                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="lcdate" required="required"'); ?>
                        </div>
                    </div>
                <?php } ?>
                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <?= lang("biller", "lcbiller"); ?>
                            <?php
                            $bl[""] = "";
                            foreach ($billers as $biller) {
                                $bl[$biller->id] = $biller->name;
                            }
                            echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $this->session->userdata('biller_id')), 'id="lcbiller" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                            ?>
                        </div>
                    </div>
                <?php } else {
                    $biller_input = array(
                        'type' => 'hidden',
                        'name' => 'biller',
                        'id' => 'slbiller',
                        'value' => $this->session->userdata('biller_id'),
                    );
                    echo form_input($biller_input);
                } ?>
            </div>
            <div class="clearfix"></div>
            <div class="row" style="margin-bottom: 15px;">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("customer", "lccustomer"); ?>
                        <div class="input-group">
                            <?php
                            echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="lccustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"');
                            ?>
                            <div class="input-group-addon no-print" style="padding: 2px 8px;color: #428bca;cursor: pointer;">
                                <i class="fa fa-pencil" id="toogle-customer-read-attr" style="font-size: 1.2em;"></i>
                            </div>
                            <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                <a href="#" id="view-customer" class="external" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-user" id="addIcon" style="font-size: 1.2em;"></i>
                                </a>
                            </div>
                            <?php if ($Owner || $Admin || $GP['customers-add']) { ?>
                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                    <a href="<?= site_url('customers/add'); ?>" id="add-customer" class="external" data-toggle="modal" data-target="#myModal">
                                        <i class="fa fa-plus-circle" id="addIcon"  style="font-size: 1.2em;"></i>
                                    </a>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-car"></i> <?= lang("data_location") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <?= lang("veiculo", "lcveiculo"); ?>
                                        <div class="input-group">
                                            <?php
                                            echo form_input('veiculo', (isset($_POST['veiculo']) ? $_POST['veiculo'] : ""), 'id="lcveiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                            ?>
                                            <div class="input-group-addon no-print" style="padding: 2px 7px; border-left: 0;">
                                                <a href="<?= site_url('location/availability_searh_modal'); ?>" id="view-availabitity" class="external" data-toggle="modal" data-target="#myModal2">
                                                    <i class="fa fa-calendar-o" id="addIcon" style="font-size: 1.2em;"></i> Consultar Disponibilidade
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("cor", "cor"); ?>
                                        <?php echo form_input('cor', '', 'readonly class="form-control" id="cor"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("placa", "placa"); ?>
                                        <?php echo form_input('placa', '', 'readonly class="form-control" id="placa"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <?= lang("ano", "ano"); ?>
                                        <?php echo form_input('ano', '', 'readonly class="form-control" id="ano"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <?= lang("tipo_contrato", "lctipocontrato") ?>
                                    <?php
                                    $cbTipoContratoLocacao[''] = lang("select") . " " . lang("tipo_contrato") ;
                                    foreach ($tiposContrato as $tipoContrato) {
                                        $cbTipoContratoLocacao[$tipoContrato->id] = $tipoContrato->name;
                                    }
                                    echo form_dropdown('tipo_contrato', $cbTipoContratoLocacao, '', 'class="form-control select" id="lctipocontrato" placeholder="' . lang("select") . " " . lang("tipo_contrato") . '"required="required" style="width:100%"')
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-car"></i> <?= lang("data_location") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("dataRetiradaPrevista", "lcdataretiradaprevista"); ?>
                                        <?php echo form_input('dataRetiradaPrevista', (isset($_POST['dataRetiradaPrevista']) ? $_POST['dataRetiradaPrevista'] : ""), 'required="required" class="form-control input-tip" id="lcdataretiradaprevista"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("dataDevolucaoPrevista", "lcdatadevolucaoprevista"); ?>
                                        <?php echo form_input('dataDevolucaoPrevista', (isset($_POST['dataDevolucaoPrevista']) ? $_POST['dataDevolucaoPrevista'] : ""), 'required="required" class="form-control input-tip" id="lcdatadevolucaoprevista"', 'date'); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("horaRetiradaPrevista", "lchoraretiradaprevista"); ?>
                                        <?php
                                        echo form_dropdown('horaRetiradaPrevista', [], (isset($_POST['horaRetiradaPrevista']) ? $_POST['horaRetiradaPrevista'] : ""), 'class="form-control select" id="lchoraretiradaprevista" placeholder="' . lang("select") . " " . lang("horaRetiradaPrevista") . '" required="required" style="width:100%"')
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <?= lang("horaDevolucaoPrevista", "lchoradevolucaoprevista"); ?>
                                        <?php echo form_dropdown('horaDevolucaoPrevista', [], (isset($_POST['horaDevolucaoPrevista']) ? $_POST['horaDevolucaoPrevista'] : ""), 'class="form-control select" id="lchoradevolucaoprevista" placeholder="' . lang("select") . " " . lang("horaDevolucaoPrevista") . '" required="required" style="width:100%"') ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("valor_franquia", "valor_franquia"); ?>
                                        <?php echo form_input('valor_franquia', '0.00', 'readonly class="form-control input-tip mask_money" id="valor_franquia"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("valor_excedente", "valor_excedente"); ?>
                                        <?php echo form_input('valor_excedente', '0.00', 'readonly class="form-control input-tip mask_money" id="valor_excedente"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("qtdDias", "qtdDias"); ?>
                                        <?php echo form_input('qtdDias', '0', 'readonly class="form-control input-tip mask_money" id="qtdDias"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("qtdHoras", "qtdHoras"); ?>
                                        <?php echo form_input('qtdHoras', '0', 'readonly class="form-control input-tip mask_money" id="qtdHoras"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("acrescimo", "acrescimo"); ?>
                                        <?php echo form_input('acrescimo', '0.00', 'class="form-control input-tip mask_money" id="acrescimo"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("desconto", "desconto"); ?>
                                        <?php echo form_input('desconto', '0.00', 'class="form-control input-tip mask_money" id="desconto"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("valor_aluguel", "valor_aluguel"); ?>
                                        <?php echo form_input('valor_aluguel', '0.00', 'readonly class="form-control input-tip mask_money" id="valor_aluguel"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-map-signs"></i> <?= lang("info_local_retirada") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("local_retirada", "lclocalretirada"); ?>
                                        <?php
                                        $cbLocalRetiradaVeiculo[''] = "";
                                        foreach ($locais_embarque as $local_embarque) {
                                            $cbLocalRetiradaVeiculo[$local_embarque->id] = $local_embarque->name;
                                        }
                                        echo form_dropdown('local_retirada_id', $cbLocalRetiradaVeiculo, '', 'class="form-control select" id="lclocalretirada" placeholder="' . lang("select") . " " . lang("local_retirada") . '" required="required"  style="width:100%"')
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("local_retirada_digitada", "local_retirada_info"); ?>
                                        <?php echo form_input('local_retirada', '', 'class="form-control" id="local_retirada_info"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("postal_code", "cep_retirada"); ?>
                                        <?php echo form_input('cep_retirada', '', 'class="form-control" id="cep_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang("address", "endereco_retirada"); ?>
                                        <?php echo form_input('endereco_retirada', '', 'class="form-control" id="endereco_retirada" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("numero", "numero_retirada"); ?>
                                        <?php echo form_input('numero_retirada', '', 'class="form-control" id="numero_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang("complemento", "complemento_retirada"); ?>
                                        <?php echo form_input('complemento_retirada', '', 'class="form-control" id="complemento_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("bairro", "bairro_retirada"); ?>
                                        <?php echo form_input('bairro_retirada', '', 'class="form-control" id="bairro_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("country", "country_retirada"); ?>
                                        <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("city", "cidade_retirada"); ?>
                                        <?php echo form_input('cidade_retirada', '', 'class="form-control" id="cidade_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("state", "estado_retirada"); ?>
                                        <?php echo form_input('estado_retirada', '', 'class="form-control" id="estado_retirada"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-primary" id="ver_mapa_retirada" style="width: 100%"> <i class="fa fa-map-marker"></i> <?=lang('ver_map');?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-map-signs"></i> <?= lang("info_local_devolucao") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("local_devolucao", "lclocaldevolucao"); ?>
                                        <?php
                                        $cbLocalDevolucaoVeiculo[''] = "";
                                        foreach ($locais_embarque as $local_embarque) {
                                            $cbLocalDevolucaoVeiculo[$local_embarque->id] = $local_embarque->name;
                                        }
                                        echo form_dropdown('local_devolucao_id', $cbLocalDevolucaoVeiculo, '', 'class="form-control select" id="lclocaldevolucao" placeholder="' . lang("select") . " " . lang("local_devolucao") . '" required="required"  style="width:100%"')
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("local_devolucao_digitada", "local_devolucao_info"); ?>
                                        <?php echo form_input('local_devolucao', '', 'class="form-control" id="local_devolucao_info"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= lang("postal_code", "cep_devolucao"); ?>
                                        <?php echo form_input('cep_devolucao', '', 'class="form-control" id="cep_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang("address", "endereco_devolucao"); ?>
                                        <?php echo form_input('endereco_devolucao', '', 'class="form-control" id="endereco_devolucao" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("numero", "numero_devolucao"); ?>
                                        <?php echo form_input('numero_devolucao', '', 'class="form-control" id="numero_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <?= lang("complemento", "complemento_devolucao"); ?>
                                        <?php echo form_input('complemento_devolucao', '', 'class="form-control" id="complemento_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("bairro", "bairro_devolucao"); ?>
                                        <?php echo form_input('bairro_devolucao', '', 'class="form-control" id="bairro_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("country", "country_devolucao"); ?>
                                        <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("city", "cidade_devolucao"); ?>
                                        <?php echo form_input('cidade_devolucao', '', 'class="form-control" id="cidade_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("state", "estado_devolucao"); ?>
                                        <?php echo form_input('estado_devolucao', '', 'class="form-control" id="estado_devolucao"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <input id="address" type="textbox" value="">
                                    <input id="submit" type="button" value="buscar">
                                    <div id="mapa" style="height: 450px;"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-eject"></i> <?= lang("info_faturamento") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row" id="form-fatura-cliente">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang('previsao_pagamento', 'previsao_pagamento'); ?>
                                                <?php echo form_input('previsao_pagamento', (isset($_POST['previsao_pagamento']) ? $_POST['previsao_pagamento'] : date('Y-m-d')), 'class="form-control input-tip" id="previsao_pagamento"','date'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("tipo_cobranca", "tipoCobrancaId"); ?>
                                                <?php
                                                $cbTipoCobranca[""] = "";
                                                foreach ($tiposCobranca as $tc) {
                                                    $cbTipoCobranca[$tc->id] = $tc->name;
                                                }
                                                echo form_dropdown('tipoCobrancaId', $cbTipoCobranca, (isset($_POST['tipoCobrancaId']) ? $_POST['tipoCobrancaId'] : $this->Settings->tipoCobrancaBoleto), 'required="required" id="tipoCobrancaId" name="tipoCobrancaId" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("condicao_pagamento", "condicaopagamentoId"); ?>
                                                <?php
                                                $cbCondicaoPagamento[""] = "";
                                                foreach ($condicoesPagamento as $cp) {
                                                    $cbCondicaoPagamento[$cp->id] = $cp->name;
                                                }
                                                echo form_dropdown('condicaopagamentoId', $cbCondicaoPagamento, (isset($_POST['condicaopagamentoId']) ? $_POST['condicaopagamentoId'] : $this->Settings->condicaoPagamentoAVista), 'required="required" id="condicaopagamentoId" name="condicaopagamentoId" class="form-control input-tip select" style="width:100%;"');
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="controls table-controls slTableParcelas table-responsive">
                                                <table id="slTableParcelas" class="table items table-striped table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th style="text-align: left;">#</th>
                                                        <th class="col-md-3" style="text-align: left;">Valor</th>
                                                        <th class="col-md-1" style="text-align: left;">Desc.</th>
                                                        <th class="col-md-2" style="text-align: left;">Vencimento</th>
                                                        <th class="col-md-3" style="text-align: left;">Cobrança</th>
                                                        <th class="col-md-3" style="text-align: left;">Conta</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("note", "note"); ?>
                        <?php echo form_textarea('note', '', 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php echo form_submit('add_location', lang('add_location'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<!--flatpickr to calender-->
<script type="text/javascript" src="<?= $assets ?>js/flatpickr/flatpickr.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/flatpickr/l10n/pt.js"></script>

<!--
<script src="https://maps.googleapis.com/maps/api/js?key=XXXX&callback=initMap"async defer></script>
!-->

<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>


<script type="text/javascript">

    var fp = null;
    var fp1 = null;
    var optional_config = null;
    var optional_config1 = null;

    $(document).ready(function (e) {

        optional_config1 = {
            enableTime: false,
            dateFormat: "Y-m-d",
            locale : "pt",
            theme: 'material_blue',
            inline: true,
            enable: [],
            onChange: function(selectedDates, dateStr, instance) {

                $('#lchoradevolucaoprevista').empty();

                $.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>location/searh_vehicle_availability_hour',
                    data: {
                        date: dateStr
                    },
                    dataType: 'json',
                    success: function (horarios_disponveis) {

                        var horas = [];

                        $(horarios_disponveis).each(function(index, horario) {
                            horas.push({value: horario.hora, label: horario.hora})
                        });

                        const manhaInicio = '08:00';
                        const manhaFim = '12:00';
                        const tardeInicio = '13:00';
                        const tardeFim = '18:00';
                        const noiteInicio = '19:00';
                        const noiteFim = '21:00';

                        const manha = horas.filter(hora => hora.value >= manhaInicio && hora.value < manhaFim);
                        const tarde = horas.filter(hora => hora.value >= tardeInicio && hora.value < tardeFim);
                        const noite = horas.filter(hora => hora.value >= noiteInicio && hora.value <= noiteFim);

                        const manhaGroup = $('<optgroup label="Manhã"></optgroup>');
                        manha.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            manhaGroup.append(option);
                        });

                        const tardeGroup = $('<optgroup label="Tarde"></optgroup>');
                        tarde.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            tardeGroup.append(option);
                        });

                        const noiteGroup = $('<optgroup label="Noite"></optgroup>');
                        noite.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            noiteGroup.append(option);
                        });

                        $('#lchoradevolucaoprevista').append(manhaGroup).append(tardeGroup).append(noiteGroup);

                        //$("#lchoradevolucaoprevista").val("").change(); // seleciona a opção "Selecione uma hora"
                    }
                });
            },
        };

        optional_config = {
            enableTime: false,
            dateFormat: "Y-m-d",
            locale : "pt",
            theme: 'material_blue',
            inline: true,
            enable: [],
            onChange: function(selectedDates, dateStr, instance) {

                $('#lchoraretiradaprevista').empty();
                $('#lchoradevolucaoprevista').empty();

                $.ajax({
                    type: "GET",
                    url: '<?php echo base_url();?>location/searh_vehicle_availability_hour',
                    data: {
                        date: dateStr
                    },
                    dataType: 'json',
                    success: function (horarios_disponveis) {

                        var horas = [];

                        $(horarios_disponveis).each(function(index, horario) {
                            horas.push({value: horario.hora, label: horario.hora})
                        });

                        const manhaInicio = '08:00';
                        const manhaFim = '12:00';
                        const tardeInicio = '13:00';
                        const tardeFim = '18:00';
                        const noiteInicio = '19:00';
                        const noiteFim = '21:00';

                        const manha = horas.filter(hora => hora.value >= manhaInicio && hora.value < manhaFim);
                        const tarde = horas.filter(hora => hora.value >= tardeInicio && hora.value < tardeFim);
                        const noite = horas.filter(hora => hora.value >= noiteInicio && hora.value <= noiteFim);

                        const manhaGroup = $('<optgroup label="Manhã"></optgroup>');
                        manha.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            manhaGroup.append(option);
                        });

                        const tardeGroup = $('<optgroup label="Tarde"></optgroup>');
                        tarde.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            tardeGroup.append(option);
                        });

                        const noiteGroup = $('<optgroup label="Noite"></optgroup>');
                        noite.forEach(hora => {
                            const option = $('<option></option>').attr('value', hora.value).text(hora.label);
                            noiteGroup.append(option);
                        });

                        $('#lchoraretiradaprevista').append(manhaGroup).append(tardeGroup).append(noiteGroup);

                        //$("#lchoraretiradaprevista").val("").change(); // seleciona a opção "Selecione uma hora"

                        optional_config1.enable = [];
                        fp1.destroy();
                        fp1 = flatpickr("#lcdatadevolucaoprevista",  optional_config1);

                    }
                });
            },
        };

        fp  = flatpickr("#lcdataretiradaprevista",  optional_config);
        fp1 = flatpickr("#lcdatadevolucaoprevista",  optional_config1);

        $('#lccustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#lcveiculo').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "vehicle/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#lcveiculo').change(function (event){
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>location/searh_vehicle_availability',
                data: {
                },
                dataType: 'json',
                success: function (horarios_disponveis) {
                    $(horarios_disponveis).each(function(index, horario) {
                        optional_config.enable.push({from: horario.data, to: horario.data})
                    });
                    fp.destroy();
                    fp = flatpickr("#lcdataretiradaprevista",  optional_config);
                }
            });
        });

        $('#lchoraretiradaprevista').change(function (event){
            $.ajax({
                type: "GET",
                url: '<?php echo base_url();?>location/searh_vehicle_availability',
                data: {
                },
                dataType: 'json',
                success: function (horarios_disponveis) {
                    $(horarios_disponveis).each(function(index, horario) {
                        optional_config1.enable.push({from: horario.data, to: horario.data})
                    });
                    fp1.destroy();
                    fp1 = flatpickr("#lcdatadevolucaoprevista",  optional_config1);
                }
            });
        });

        $('#lchoradevolucaoprevista').change(function(event){

            let dtRetirada = $('#lcdataretiradaprevista').val();
            let dtDevolucao = $('#lcdatadevolucaoprevista').val();

            let horaRetirada = $('#lchoraretiradaprevista').val();
            let horaDevolucao = $('#lchoradevolucaoprevista').val();

            let dtHoraReitarada = new Date(dtRetirada+'T'+horaRetirada);
            let dtHoraDevolucao = new Date(dtDevolucao+'T'+horaDevolucao);

            const diferencaEmDias = calcularDiferencaDias(dtHoraReitarada, dtHoraDevolucao);
            const diferencaEmHora = calcularDiferencaHoras(dtHoraReitarada, dtHoraDevolucao);

            $('#qtdDias').val(diferencaEmDias);
            $('#qtdHoras').val(diferencaEmHora);

            buscar_valor();
            getParcelamento();

        });

        $( "#cep_retirada" ).blur(function() {
            getConsultaCEPRetirada();
        });

        $( "#cep_devolucao" ).blur(function() {
            getConsultaCEPDevolucao();
        });

        $("#condicaopagamentoId").select2().on("change", function(e) {
            getParcelamento();
        });

        $("#tipoCobrancaId").select2().on("change", function(e) {
            getParcelamento();
        });

        $('#previsao_pagamento').blur(function (event) {
            getParcelamento();
        });

    });

    function getConsultaCEPDevolucao() {

        if($.trim($("#cep_devolucao").val()) === "") return false;

        var cep = $.trim($("#cep_devolucao").val());
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';


        $.get(url,
            function (data) {
                if(data !== null){
                    $("#endereco_devolucao").val(data.logradouro);
                    $("#bairro_devolucao").val(data.bairro);
                    $("#cidade_devolucao").val(data.localidade);
                    $("#estado_devolucao").val(data.uf);
                }
            });
    }

    function getConsultaCEPRetirada() {

        if($.trim($("#cep_retirada").val()) === "") return false;

        var cep = $.trim($("#cep_retirada").val());
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url,
            function (data) {
                if(data !== null){
                    $("#endereco_retirada").val(data.logradouro);
                    $("#bairro_retirada").val(data.bairro);
                    $("#cidade_retirada").val(data.localidade);
                    $("#estado_retirada").val(data.uf);
                }
            });
    }

    function getParcelamento() {

        var condicaoPagamento = $("#condicaopagamentoId  option:selected").val();
        var tipoCobranca = $("#tipoCobrancaId  option:selected").val();

        if (condicaoPagamento !== '' && tipoCobranca !== '') {
            $.ajax({
                type: "GET",
                url: site.base_url + "saleitem/getParcelas",
                data: {
                    condicaoPagamentoId: condicaoPagamento,
                    tipoCobrancaId: tipoCobranca,
                    dtvencimento: $('#previsao_pagamento').val(),
                    valorVencimento: $('#valor_aluguel').val(),
                },
                dataType: 'html',
                success: function (html) {
                    $('#slTableParcelas tbody').html(html);
                }
            });
        } else {
            $('#slTableParcelas tbody').html('');
        }
    }

    function calcularDiferencaHoras(data1, data2) {
        const diferenca = Math.abs(data2 - data1);

        return Math.floor(diferenca / (1000 * 60 * 60));
    }

    function calcularDiferencaDias(data1, data2) {
        const data1Ms = data1.getTime();
        const data2Ms = data2.getTime();

        const diferencaMs = Math.abs(data2Ms - data1Ms);

        return Math.floor(diferencaMs / (1000 * 60 * 60 * 24)) + 1;
    }

    function buscar_valor() {
        $.ajax({
            type: "GET",
            url: site.base_url + "location/search_value_vehicle_rental_rule",
            data: {
                tipo_contrato: $('#lctipocontrato').val(),
                qtd_dias: $('#qtdDias').val(),
                qtd_horas: $('#qtdHoras').val(),
            },
            dataType: 'json',
            success: function (regra) {
                if (regra != null) {
                    $('#valor_excedente').val(regra.excedente);
                    $('#valor_franquia').val(regra.valor);
                    $('#valor_aluguel').val(regra.valor_aluguel);
                }
            }
        });
    }

    var map = null;
    var geocoder = null;

    function initMap() {
        var map = new google.maps.Map(document.getElementById('mapa'), {
            zoom: 10,
        });

        var geocoder = new google.maps.Geocoder();
        document.getElementById('submit').addEventListener('click', function() {
            geocodeAddress(geocoder, map);
        });

        geocodeAddressAberto(geocoder, map);
    }

    function geocodeAddressAberto(geocoder, map) {

        var infowindow = new google.maps.InfoWindow({
            content: 'OLHA SO'
        });

        geocoder.geocode({'address': 'Rua Giovanni Pisano 221, Aririu - Palhoça Santa Catariana 88135432' }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    title: 'TESTE',
                    map: map,
                    animation: google.maps.Animation.DROP,
                    position: results[0].geometry.location,
                });

                marker.addListener('click', function() {
                    infowindow.open(map, marker);
                });
            }
        });
    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                resultsMap.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: resultsMap,
                    position: results[0].geometry.location,
                });
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

</script>
