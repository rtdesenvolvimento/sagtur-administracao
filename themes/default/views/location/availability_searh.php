
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
<link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/material_orange.css">

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('availability_searh_by_vehicle'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li class="divider"></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <?= lang("veiculo", "fl_veiculo"); ?>
                    <?php echo form_input('veiculo', '', 'id="fl_veiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '" required="required" class="form-control input-tip" style="width:100%;"'); ?>
                </div>
            </div>
        </div>
        <?php if ($vehicle->id) {?>
            <div class="row">
                <div class="col-md-3">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-calendar"></i> <?= lang("data_availability_searh_day") ?></div>
                        <div class="panel-body">
                            <div class="col-md-12">
                                <br/>
                                <img id="pr-image" src="<?= base_url() ?>assets/uploads/<?= $vehicle->photo ?>" alt="<?= $vehicle->name ?>" class="img-responsive img-thumbnail"/>
                            </div>
                            <div class="col-md-12">
                                <br/>
                                <h3> <?=lang('select_day');?></h3>
                                <input class="flatpickr" type="text" style="display: none;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="panel panel-default">
                        <div class="panel-heading"><i class="fa-fw fa fa-calendar"></i> <?= lang("data_availability_searh") ?></div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id='calendar'></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<script src='https://cdn.jsdelivr.net/npm/fullcalendar@6.1.5/index.global.min.js'></script>
<script src="https://cdn.jsdelivr.net/npm/@fullcalendar/core@6.1.5/locales-all.global.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr@4.6.13/dist/l10n/pt.js"></script>

<script type="text/javascript">

    var calendar = null;
    var calendarEl = document.getElementById('calendar');

    var optional_config = {
        enableTime: false,
        dateFormat: "Y-m-d",
        locale : "pt",
        theme: 'material_orange',
        inline: true,

        enable: [
            <?php foreach ($datetimes as $datetime) {
            $date = explode(' ', $datetime)[0];
            $time = explode(' ', $datetime)[1];
            ?>
            {
                from: "<?php echo $date;?>",
                to: "<?php echo $date;?>"
            },
            <?php } ?>

            <?php foreach ($datas as $data) {?>
            {
                from: "<?php echo $data->data;?>",
                to: "<?php echo $data->data;?>"
            },
            <?php } ?>
        ],
        onChange: function(selectedDates, dateStr, instance) {
            console.log(selectedDates);
            console.log(dateStr);
            console.log(instance);

            calendar.destroy();

            calendar = new FullCalendar.Calendar(calendarEl, {
                initialView: 'listDay',
                initialDate: dateStr,
                editable: true,
                selectable: true,
                //businessHours: true,
                locale: 'pt-br',
                views: {
                    listDay: { buttonText: '<?=lang('list_by_day');?>' },
                    listWeek: { buttonText: 'Lista da Semana' },
                    listMonth: { buttonText: 'Lista do Mês' },
                    listYear: { buttonText: 'list year' }
                },
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    //right: 'dayGridMonth,timeGridWeek,timeGridDay,listDay,listWeek,listMonth,listYear'
                    right: 'listDay,listWeek,listMonth,timeGridWeek,timeGridDay,dayGridMonth'

                },
                events: site.base_url+'location/get_events',
            });
            calendar.render();
        },
    };
    var fp = flatpickr(".flatpickr",  optional_config);

    $(document).ready(function () {

        calendar = new FullCalendar.Calendar(calendarEl, {
            initialView: 'listDay',
            //editable: true,
            //selectable: true,
            //businessHours: true,
            locale: 'pt-br',
            views: {
                listDay: { buttonText: '<?=lang('list_by_day');?>' },
                listWeek: { buttonText: 'Lista da Semana' },
                listMonth: { buttonText: 'Lista do Mês' },
                listYear: { buttonText: 'list year' }
            },
            headerToolbar: {
                left: 'prev,next today',
                center: 'title',
                //right: 'dayGridMonth,timeGridWeek,timeGridDay,listDay,listWeek,listMonth,listYear'
                right: 'listDay,listWeek,listMonth,timeGridWeek,timeGridDay,dayGridMonth'

            },
            events: site.base_url+'location/get_events',
        });
        calendar.render();

        <?php if ($vehicle->id) {?>
            $('#fl_veiculo').val('<?=$vehicle->id;?>').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "vehicle/getVehicle/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "vehicle/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } else { ?>
            $('#fl_veiculo').select2({
                minimumInputLength: 1,
                ajax: {
                    url: site.base_url + "vehicle/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } ?>
    });
</script>

