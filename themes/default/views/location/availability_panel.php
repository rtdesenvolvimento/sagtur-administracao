<style type="text/css" media="screen">
    #DTAvailabitity td:nth-child(1) {display: none;}
 </style>

<script>

    function show_status(status) {
        if (status === 'Locado') {
            return '<div class="text-center"><span class="label label-danger">LOCADO</span></div>';
        } else {
            return '<div class="text-center"><span class="label label-success">DISPONÍVEL</span></div>';
        }
    }

    function placa(placa) {
        return '<div class="text-center" style="text-transform: uppercase;"><span class="label" style="background-color:#0044cc">' + placa + '</span></div>';
    }

    $(document).ready(function () {
        $('#DTAvailabitity').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('location/getAvailability') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "start_time", "value":  $('#start_time').val() });
                aoData.push({ "name": "end_time", "value":  $('#end_time').val() });

                aoData.push({ "name": "fl_veiculo", "value":  $('#fl_veiculo').val() });
                aoData.push({ "name": "fl_categoria_veiculo", "value":  $('#fl_categoria_veiculo').val() });
                aoData.push({ "name": "fl_tipo_veiculo", "value":  $('#fl_tipo_veiculo').val() });
                aoData.push({ "name": "fl_status_disponibilidade", "value":  $('#fl_status_disponibilidade').val() });


            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"bSortable": false, "mRender": img_hl},
                null,
                null,
                null,
                null,
                {"mRender": placa},
                null,
                null,
                {"mRender": show_status},
                {"mRender": fsd},
                null,
                null,
                {"mRender": fld},
                {"bSortable": false}
            ]
        });
    });
</script>
<?= form_open('financeiro/expense_category_actions', 'id="action-form"') ?>
<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-calendar"></i><?= lang('availability_panel'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li class="divider"></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;" class="divfilters">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("start_date", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("end_date", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("start_time", "start_time"); ?>
                                    <?php echo form_input('start_time', (isset($_POST['start_time']) ? $_POST['start_time'] : $start_time), 'class="form-control" id="start_time"', 'time'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("end_time", "end_time"); ?>
                                    <?php echo form_input('end_time', (isset($_POST['end_time']) ? $_POST['end_time'] : $end_time), 'type="date" class="form-control" id="end_time"', 'time'); ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <?= lang("categoria_veiculo", "fl_categoria_veiculo") ?>
                                <?php
                                $cbCategoriasVeiculo[''] = lang("select") . " " . lang("categoria_veiculo") ;
                                foreach ($categoriasVeiculo as $categoriaVeiculo) {
                                    $cbCategoriasVeiculo[$categoriaVeiculo->id] = $categoriaVeiculo->name;
                                }
                                echo form_dropdown('fl_categoria_veiculo', $cbCategoriasVeiculo, '', 'class="form-control select" id="fl_categoria_veiculo" placeholder="' . lang("select") . " " . lang("categoria_veiculo") . ' style="width:100%"')
                                ?>
                            </div>

                            <div class="col-sm-3">
                                <?= lang("tipo_veiculo", "tipo_veiculo") ?>
                                <?php
                                $cbTiposVeiculo[''] = lang("select") . " " . lang("tipo_veiculo") ;
                                foreach ($tiposVeiculo as $tipoVeiculo) {
                                    $cbTiposVeiculo[$tipoVeiculo->id] = $tipoVeiculo->name;
                                }
                                echo form_dropdown('fl_tipo_veiculo', $cbTiposVeiculo, '', 'class="form-control select" id="fl_tipo_veiculo" placeholder="' . lang("select") . " " . lang("tipo_contrato") . ' style="width:100%"')
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("veiculo", "fl_veiculo"); ?>
                                    <?php echo form_input('fl_veiculo', (isset($_POST['fl_veiculo']) ? $_POST['fl_veiculo'] : ""), 'id="fl_veiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '"class="form-control input-tip" style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("status_disponibilidade", "fl_status_disponibilidade"); ?>
                                    <?php
                                    $cbStatusDisponibilidade[''] = lang("select") . " " . lang("status_disponibilidade") ;
                                    foreach ($statusDisponibilidade as $stDisponibilidade) {
                                        $cbStatusDisponibilidade[$stDisponibilidade->id] = $stDisponibilidade->name;
                                    }
                                    echo form_dropdown('fl_status_disponibilidade', $cbStatusDisponibilidade, '', 'class="form-control select" id="fl_status_disponibilidade" placeholder="' . lang("select") . " " . lang("status_disponibilidade") . '" style="width:100%"')
                                    ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="DTAvailabitity" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:40px; width: 40px; text-align: center;display: none;"><?php echo $this->lang->line("ck"); ?></th>
                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                            <th><?= $this->lang->line("reference_no"); ?></th>
                            <th><?= $this->lang->line("categoria"); ?></th>
                            <th><?= $this->lang->line("tipo"); ?></th>
                            <th><?= $this->lang->line("veiculo"); ?></th>
                            <th><?= $this->lang->line("placa"); ?></th>
                            <th><?= $this->lang->line("ano"); ?></th>
                            <th><?= $this->lang->line("cor"); ?></th>
                            <th><?= $this->lang->line("status"); ?></th>
                            <th><?= $this->lang->line("date"); ?></th>
                            <th><?= $this->lang->line("hora"); ?></th>
                            <th><?= $this->lang->line("customer"); ?></th>
                            <th><?= $this->lang->line("devolucao"); ?></th>
                            <th style="width:100px;"><?= $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="14" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#end_date').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#start_time').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });
        $('#end_time').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });

        $('#fl_veiculo').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });

        $('#fl_categoria_veiculo').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });

        $('#fl_tipo_veiculo').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });

        $('#fl_status_disponibilidade').change(function(event){
            $('#DTAvailabitity').DataTable().fnClearTable()
        });


        $('#fl_veiculo').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "vehicle/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });
</script>

