<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Chat Assistant</title>
    <style>
        body {
            font-family: Arial, sans-serif;
        }
        .chat-container {
            max-width: 600px;
            margin: 0 auto;
            padding: 20px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }
        .chat-box {
            height: 400px;
            overflow-y: auto;
            margin-bottom: 20px;
            border: 1px solid #ddd;
            padding: 10px;
            background: #f9f9f9;
        }
        .message {
            margin: 10px 0;
        }
        .message.user {
            text-align: right;
            color: blue;
        }
        .message.assistant {
            text-align: left;
            color: green;
        }
    </style>
</head>
<body>
<div class="chat-container">
    <div style="text-align: center;font-weight: 600;font-size: 1.2rem;margin-bottom: 10px;">Assistente de Ajuda</div>

    <div class="chat-box" id="chat-box">
        <!-- Messages will appear here -->
    </div>
    <form id="chat-form">
        <input type="text" style="width: 80%;" id="message-input" placeholder="Envie sua dúvida..." required>
        <button type="submit" style="width: 10%;">Enviar</button>
    </form>
</div>

<script>
    const chatBox = document.getElementById('chat-box');
    const chatForm = document.getElementById('chat-form');
    const messageInput = document.getElementById('message-input');

    chatForm.addEventListener('submit', async (event) => {

        event.preventDefault();

        const userMessage = messageInput.value;

        appendMessage('user', userMessage);

        $.ajax({
            type: "GET",
            url: '<?=base_url("/assistants/sendMessage")?>',
            data: {
                message: userMessage,
            },
            dataType: 'json',
            success: function (result) {

                let messagem = JSON.parse(result).message.content;
                messagem = messagem.replace(/\n/g, '<br/>');

                if (messagem) {
                    appendMessage('assistant', messagem);
                } else {
                    appendMessage('assistant', 'Error: Could not fetch a response.');
                }
            },
            error: function(error) {
                console.error(error);
                appendMessage('assistant', 'Error: ' + error);
            }
        });

        messageInput.value = '';
    });

    function appendMessage(role, content) {
        const messageDiv = document.createElement('div');
        messageDiv.className = `message ${role}`;
        messageDiv.innerHTML  = content;
        chatBox.appendChild(messageDiv);
        chatBox.scrollTop = chatBox.scrollHeight;
    }
</script>
</body>
</html>
