<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_customer'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("customers/edit/" . $customer->id, $attrib); ?>
        <div class="modal-body">
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group company">
                        <div style="max-width:200px; margin: 0 auto;">
                            <?php if (strpos($customer->image, 'pdf') !== false ||
                                strpos($customer->image, 'doc') !== false ||
                                strpos($customer->image, 'docx') !== false) { ?>
                                <?= '<a alt="" href="' . base_url() . 'assets/uploads/' . $customer->image . '" target="_blank" class="avatar">Ver o arquivo</a>'; ?>
                            <?php } else { ?>
                                <?= '<img alt="" src="' . base_url() . 'assets/uploads/' . $customer->image . '" class="avatar">'; ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-md-2">
                    <?= lang("status", "status") ?>
                    <?php
                    $opts = array(
                        1 => lang('Ativo'),
                        0 => lang('Cancelar')
                    );
                    echo form_dropdown('active', $opts,  $customer->active , 'class="form-control" id="status" required="required"');
                    ?>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label"
                               for="customer_group"><?php echo $this->lang->line("default_customer_group"); ?></label>
                        <div class="controls"> <?php foreach ($customer_groups as $customer_group) {
                                $cgs[$customer_group->id] = $customer_group->name;
                            }
                            echo form_dropdown('customer_group', $cgs, $customer->customer_group_id, 'class="form-control tip select" id="customer_group" style="width:100%;" required="required"'); ?>                        </div>
                    </div>
                </div>
                <div class="col-md-2" style='display: none;'>
                    <div class="form-group company">
                        <?= lang("sexo", "sexo"); ?>
                        <select id="sexo" name="sexo" class="form-control" required="required">
                            <option value="OUTROS" <?php if ($customer->sexo == 'OUTROS') echo 'selected="selected"' ?> >OUTROS</option>
                            <option value="FEMININO" <?php if ($customer->sexo == 'FEMININO') echo 'selected="selected"' ?> >FEMININO</option>
                            <option value="MASCULINO" <?php if ($customer->sexo == 'MASCULINO') echo 'selected="selected"' ?> >MASCULINO</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group company">
                        <?= lang("tipoPessoa", "tipoPessoa"); ?>
                        <select id="tipoPessoa" name="tipoPessoa" class="form-control" required="required">
                            <option value="PF" <?php if($customer->tipoPessoa == 'PF') echo 'selected="selected"';?> >Física</option>
                            <option value="PJ" <?php if ($customer->tipoPessoa == 'PJ') echo 'selected="selected"'; ?>>Jurídica</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?><?php echo form_input('vat_no', $customer->vat_no, 'class="form-control cpf" id="vat_no" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group person">
                        <?= lang("empresa", "name"); ?>
                        <?php echo form_input('name', $customer->name, 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group company">
                        <?= lang("nome_fantasia", "company"); ?>
                        <?php echo form_input('company', $customer->company, 'class="form-control tip" id="company" data-bv-notempty="true"'); ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group company">
                        <?= lang("nome_responsavel", "nome_responsavel"); ?>
                        <?php echo form_input('nome_responsavel', $customer->nome_responsavel, 'class="form-control tip" id="nome_responsavel" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("email_address", "email"); ?>
                        <input type="email" name="email" id="email" value="<?php echo $customer->email; ?>" class="form-control" required="required"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel"
                               name="phone"
                               value="<?php echo $customer->phone; ?>"
                               class="form-control"
                               id="phone"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("cf5", "cf5"); ?>
                        <input type="tel"
                               name="cf5"
                               value="<?php echo $customer->cf5; ?>"
                               class="form-control"
                               id="cf5"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('data_contratacao', 'data_contratacao'); ?>
                        <input type="date" name="data_contratacao" value="<?php echo $customer->data_contratacao; ?>" class="form-control tip" id="data_contratacao" required="required">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang('data_cancelamento', 'data_cancelamento'); ?>
                        <input type="date" name="data_cancelamento" value="<?php echo $customer->data_cancelamento; ?>"
                               class="form-control tip" id="data_cancelamento">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("Nome do Banco de Dados", "name_db"); ?>
                        <input type="tel"
                               name="name_db"
                               value="<?php echo $customer->name_db; ?>"
                               class="form-control"
                               id="name_db"/>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-2">
                    <div class="form-group">
                        <div><b><?php echo lang("tipo_documento", "tipo_documento"); ?></b></div>
                        <select id="tipo_documento" name="tipo_documento" class="form-control" required="required">
                            <option value="rg" <?php if ($customer->tipo_documento == 'rg') echo 'selected="selected"' ?>>
                                RG
                            </option>
                            <option value="passaporte" <?php if ($customer->tipo_documento == 'passaporte') echo 'selected="selected"' ?>>
                                Passaporte
                            </option>
                            <option value="CN" <?php if ($customer->tipo_documento == 'CN') echo 'selected="selected"' ?>>
                                Certidão de Nascimento
                            </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="rg">
                            <div id="div_rgPassaporte"><b>R.G</b></div>
                        </label>
                        <?php echo form_input('cf1', $customer->cf1, 'class="form-control rg" id="cf1"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="emissaorg">
                            <div id="div_vlPassaporteRG"><b>Emissão RG</b></div>
                        </label>
                        <input type="date" name="validade_rg_passaporte"
                               value="<?php echo $customer->validade_rg_passaporte; ?>" class="form-control tip"
                               id="validade_rg_passaporte">
                    </div>
                </div>
                <div class="col-md-2" id="div_orgaoemissor">
                    <div class="form-group">
                        <?= lang("ccf3", "cf3"); ?>
                        <?php echo form_input('cf3', $customer->cf3, 'class="form-control" id="cf3"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("naturalidade", "naturalidade"); ?>
                        <?php echo form_input('cf4', $customer->cf4, 'class="form-control" id="cf4"'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("telefone_emergencia", "telefone_emergencia"); ?>
                        <?php echo form_input('telefone_emergencia', $customer->telefone_emergencia, 'class="form-control" id="telefone_emergencia"'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("plano_saude", "plano_saude"); ?>
                        <input type="text" name="plano_saude" value="<?php echo $customer->plano_saude; ?>"
                               class="form-control" id="plano_saude"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("alergia_medicamento", "alergia_medicamento"); ?>
                        <input type="text" name="alergia_medicamento"
                               value="<?php echo $customer->alergia_medicamento; ?>" class="form-control"
                               id="alergia_medicamento"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("doenca_informar", "doenca_informar"); ?>
                        <input type="text" name="doenca_informar" value="<?php echo $customer->doenca_informar; ?>"
                               class="form-control" id="doenca_informar"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?php echo form_checkbox('bloqueado', '1', $customer->bloqueado, 'id="bloqueado"'); ?>
                <label for="attributes" class="padding05"><?= lang('bloqueio_cliente'); ?></label>
            </div>
            <div class="form-group all">
                <?= lang("observacao", "observacao") ?>
                <?= form_textarea('observacao', (isset($_POST['observacao']) ? $_POST['observacao'] : $customer->observacao), 'class="form-control" id="observacao" '); ?>
            </div>
            <fieldset>
                <legend style="cursor: pointer;" onclick="exibirEndereco();">Adicionar Endereço</legend>
                <div id="div_endereco">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("postal_code", "postal_code"); ?>
                                <?php echo form_input('postal_code', $customer->postal_code, 'class="form-control" id="postal_code"'); ?>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= lang("address", "address"); ?>
                                <?php echo form_input('address', $customer->address, 'class="form-control" id="address" '); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("numero", "numero"); ?>
                                <?php echo form_input('numero', $customer->numero, 'class="form-control" id="numero"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= lang("complemento", "complemento"); ?>
                                <?php echo form_input('complemento', $customer->complemento, 'class="form-control" id="complemento"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("bairro", "bairro"); ?>
                                <?php echo form_input('bairro', $customer->bairro, 'class="form-control" id="bairro"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("country", "country"); ?>
                                <?php echo form_input('country', $customer->country, 'class="form-control" id="country"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("city", "city"); ?>
                                <?php echo form_input('city', $customer->city, 'class="form-control" id="city"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("state", "state"); ?>
                                <?php echo form_input('state', $customer->state, 'class="form-control" id="state"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("ccf2", "cf2"); ?>
                        <?php echo form_input('cf2', $customer->cf2, 'class="form-control" id="cf2"'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("ccf6", "cf6"); ?>
                        <?php echo form_input('cf6', $customer->cf6, 'class="form-control" id="cf6"'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang('award_points', 'award_points'); ?>
                        <?= form_input('award_points', set_value('award_points', $customer->award_points), 'class="form-control tip" id="award_points"  required="required"'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_customer', lang('edit_customer'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript" src="<?= $assets ?>js/valida_cpf_cnpj.js"></script>


<script type="text/javascript">

    $(document).ready(function (e) {

        $("#cf5").mask("(99)99999-9999");

    <?php if ($customer->tipo_documento == 'passaporte') {?>
        $('#div_vlPassaporteRG').html('<b>Validade Passaporte</b>');
        $('#div_rgPassaporte').html('<b>Passaporte</b>');
        <?php } else if($customer->tipo_documento == 'CN') { ?>
        $('#div_vlPassaporteRG').html('<b>Emissão</b>');
        $('#div_rgPassaporte').html('<b>Certidão de Nascimento</b>');
        <?php }?>

        <?php if ($customer->tipoPessoa == 'PJ') {?>
        $("#vat_no").mask("99.999.999/9999-99");
        <?php } else{ ?>
        $("#vat_no").mask("999.999.999-99");
        <?php } ?>

        $('#vat_no').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $('#tipoPessoa').change(function (e) {
            $('#vat_no').val('');
            if ($(this).val() === 'PJ') {
                $("#vat_no").mask("99.999.999/9999-99");
            } else {
                $("#vat_no").mask("999.999.999-99");
            }
        });

        $('#tipo_documento').change(function (e) {
            if ($(this).val() === 'rg') {
                $('#div_vlPassaporteRG').html('<b>Emissão RG</b>');
                $('#div_rgPassaporte').html('<b>R.G</b>');
            } else if ($(this).val() == 'CN') {
                $('#div_vlPassaporteRG').html('<b>Emissão</b>');
                $('#div_rgPassaporte').html('<b>Certidão de Nascimento</b>');
            } else {
                $('#div_vlPassaporteRG').html('<b>Validade Passaporte</b>');
                $('#div_rgPassaporte').html('<b>Passaporte</b>');
            }
        });

        $('textarea').not('.skip').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var editor = this.$editor.next('textarea');
                if ($(editor).attr('required')) {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
                }
            }
        });

        $("#postal_code").blur(function () {
            getConsultaCEP();
        });
    });

    function exibirEndereco() {
        if ($('#div_endereco').is(':visible')) {
            $('#div_endereco').hide(600);
        } else {
            $('#div_endereco').show(600);
        }
    }

    function exibirArquivos() {
        if ($('#div_arquivos').is(':visible')) {
            $('#div_arquivos').hide(600);
        } else {
            $('#div_arquivos').show(600);
        }
    }

    function consultaPessoa(tagCpfCnpj, tipoPessoa, base_url) {

        var cpf_cnpj = tagCpfCnpj.val();

        if (cpf_cnpj != '' && cpf_cnpj != '___.___.___-__' && cpf_cnpj != '__.___.___/____-__') {
            if (tipoPessoa === 'PJ') {

                if (!valida_cnpj(cpf_cnpj)) {
                    alert('CNPJ inválido!');
                    tagCpfCnpj.val('');
                    tagCpjCnpj.focus();
                } else {
                    var cpf_cnpj = cpf_cnpj.replace(/[^0-9]/g, '');

                    $.ajax({
                        type: "POST",
                        url: base_url + 'customers/wscliente',
                        data: {
                            cnpj: cpf_cnpj
                        },
                        dataType: 'json',
                        success: function (empresa) {

                            empresa = JSON.parse(empresa);

                            if (empresa.status === undefined) {
                                alert("Não foi possível encontrar o cnpj");
                                return;
                            }

                            if (empresa.status === 'ERROR') {
                                alert(empresa.message);
                                return;
                            }

                            if (empresa.situacao !== 'ATIVA') {
                                if (confirm('Esta empres encontra-se na situação ' + empresa.situacao + ', motivo ' + empresa.motivo_situacao + '. Deseja realmente importar seus dados?')) {
                                    preencherDadosDaPessoaJuridica(empresa);
                                }
                            } else {
                                preencherDadosDaPessoaJuridica(empresa);
                            }
                        }
                    });
                }
            } else {
                if (!valida_cpf(cpf_cnpj)) {
                    alert('CPF inválido!');
                    tagCpfCnpj.val('');
                    tagCpfCnpj.focus();
                }
            }
        }
    }

    function preencherDadosDaPessoaJuridica(empresa) {

        var date = empresa.data_situacao.split('/').reverse().join('-');

        $('#company').val(empresa.nome);
        $('#name').val(empresa.fantasia);

        $('#phone').val(empresa.telefone);
        $('#email').val(empresa.email);
        $('#data_aniversario').val(date);

        $('#postal_code').val(empresa.cep);
        $('#address').val(empresa.logradouro);

        $('#numero').val(empresa.numero);
        $('#complemento').val(empresa.complemento);
        $('#bairro').val(empresa.bairro);

        $('#city').val(empresa.municipio);
        $('#state').val(empresa.uf);

        let atividades = '';

        for (let i = 0; i < empresa.atividade_principal.length; i++) {
            atividades = atividades + empresa.atividade_principal[i].code + ' - ' + empresa.atividade_principal[i].text + '<br/>';
        }

        for (let j = 0; j < empresa.atividades_secundarias.length; j++) {
            atividades = atividades + empresa.atividades_secundarias[j].code + ' - ' + empresa.atividades_secundarias[j].text + '<br/>';
        }

        for (let k = 0; k < empresa.qsa.length; k++) {
            atividades = atividades + empresa.qsa[k].qual + ' - ' + empresa.qsa[k].nome + '<br/>';
            ;
        }

        $('#observacao').redactor('set', atividades);
        $('#observacao').val(atividades);
    }

    function getConsultaCEP() {

         if($.trim($("#postal_code").val()) === "") return false;

        var cep = $.trim($("#postal_code").val());
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url,
            function (data) {
                if(data !== null){
                    $("#address").val(data.logradouro);
                    $("#bairro").val(data.bairro);
                    $("#city").val(data.localidade);
                    $("#state").val(data.uf);
                }
            });
    }
</script>

