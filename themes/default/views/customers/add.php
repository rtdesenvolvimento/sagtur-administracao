<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_customer'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-customer-form');
        echo form_open_multipart("customers/add", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group">
                <label class="control-label"
                       for="customer_group"><?php echo $this->lang->line("default_customer_group"); ?></label>
                <div class="controls"> <?php
                    foreach ($customer_groups as $customer_group) {
                        $cgs[$customer_group->id] = $customer_group->name;
                    }
                    echo form_dropdown('customer_group', $cgs, $this->Settings->customer_group, 'class="form-control tip select" id="customer_group" style="width:100%;" required="required"');
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group company">
                        <?= lang("sexo", "sexo"); ?>
                        <select id="sexo" name="sexo" class="form-control" required="required">
                            <option value="FEMININO">FEMININO</option>
                            <option value="MASCULINO">MASCULINO</option>
                        </select>
                     </div>
                </div>
                <div class="col-md-10">
                    <div class="form-group person">
                        <?= lang("name", "name"); ?>
                        <?php echo form_input('name', '', 'class="form-control tip" id="name" data-bv-notempty="true"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <div class="form-group person">
                        <?= lang("social_name", "social_name"); ?>
                        <?php echo form_input('social_name', '', 'class="form-control tip" id="social_name"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group company">
                        <?= lang("idioma", "idioma"); ?>
                        <select id="idioma" name="idioma" class="form-control" required="required">
                            <option value="Português">Português</option>
                            <option value="Inglês">Inglês</option>
                            <option value="Espanhol">Espanhol</option>
                            <option value="Japonês">Japonês</option>
                            <option value="Alemão">Alemão</option>
                            <option value="Francês">Francês</option>
                            <option value="Italiano">Italiano</option>
                            <option value="Coreano">Coreano</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group company">
                        <?= lang("empresa", "empresa"); ?>
                        <?php echo form_input('company', '', 'class="form-control tip" id="company" data-bv-notempty="false"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group company">
                        <?= lang("tipoPessoa", "tipoPessoa"); ?>
                        <select id="tipoPessoa" name="tipoPessoa" class="form-control" required="required">
                            <option value="PF">Física</option>
                            <option value="PJ">Jurídica</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang("vat_no", "vat_no"); ?>
                        <?php echo form_input('vat_no', '', 'class="form-control cpf" id="vat_no"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("email_address", "email_address"); ?>
                        <input type="email" name="email" id="email" class="form-control"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <?= lang('data_aniversario', 'data_aniversario'); ?>
                        <input type="date" name="data_aniversario" value="" class="form-control tip" id="data_aniversario">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <div><b><?php echo lang("tipo_documento", "tipo_documento");?></b></div>
                        <select id="tipo_documento" name="tipo_documento" class="form-control" required="required">
                            <option value="rg">RG</option>
                            <option value="novo rg">NOVO RG</option>
                            <option value="passaporte">Passaporte</option>
                            <option value="CN">Certidão de Nascimento</option>
                            <option value="CNH">CNH</option>
                            <option value="RNE">RNE</option>
                            <option value="PIS">CARTEIRA DE TRABALHO / PIS</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="rg"><div id="div_rgPassaporte"><b>R.G</b></div></label>
                        <?php echo form_input('cf1', '', 'class="form-control rg" id="cf1"'); ?>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="emissaorg"><div id="div_vlPassaporteRG"><b>Emissão RG</b></div></label>
                        <input type="date" name="validade_rg_passaporte" value="" class="form-control tip" id="validade_rg_passaporte">
                    </div>
                </div>
                <div class="col-md-2" id="div_orgaoemissor">
                    <div class="form-group">
                        <?= lang("ccf3", "cf3"); ?>
                        <?php echo form_input('cf3', '', 'class="form-control" id="cf3"'); ?>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <?= lang("naturalidade", "naturalidade"); ?>
                        <?php echo form_input('cf4', '', 'class="form-control" id="cf4"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("phone", "phone"); ?>
                        <input type="tel" name="phone" class="form-control" id="phone"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("ccf5", "cf5"); ?>
                        <?php echo form_input('cf5', '', 'class="form-control" id="cf5"'); ?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("telefone_emergencia", "telefone_emergencia"); ?>
                        <?php echo form_input('telefone_emergencia', '', 'class="form-control" id="telefone_emergencia"'); ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("plano_saude", "plano_saude"); ?>
                        <input type="text" name="plano_saude" class="form-control" id="plano_saude"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("alergia_medicamento", "alergia_medicamento"); ?>
                        <input type="text" name="alergia_medicamento" class="form-control" id="alergia_medicamento"/>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?= lang("doenca_informar", "doenca_informar"); ?>
                        <input type="text" name="doenca_informar" class="form-control" id="doenca_informar"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("profession", "profession"); ?>
                        <input type="text" name="profession" class="form-control" id="profession"/>
                    </div>
                </div>
            </div>
            <div class="form-group all">
                <?= lang("observacao", "observacao") ?>
                <?= form_textarea('observacao', (isset($_POST['observacao']) ? $_POST['observacao'] :  ''), 'class="form-control" id="observacao" '); ?>
            </div>
            <fieldset>
                <legend style="cursor: pointer;" onclick="exibirEndereco();">Clique aqui para ver endereço</legend>
                <div id="div_endereco" style="display: none;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("postal_code", "postal_code"); ?>
                                <?php echo form_input('postal_code', '', 'class="form-control" id="postal_code"'); ?>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= lang("address", "address"); ?>
                                <?php echo form_input('address', '', 'class="form-control" id="address" '); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("numero", "numero"); ?>
                                <?php echo form_input('numero', '', 'class="form-control" id="numero"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= lang("complemento", "complemento"); ?>
                                <?php echo form_input('complemento', '', 'class="form-control" id="complemento"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("bairro", "bairro"); ?>
                                <?php echo form_input('bairro', '', 'class="form-control" id="bairro"'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("country", "country"); ?>
                                <?php echo form_input('country', 'BRASIL', 'class="form-control" id="country"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("city", "city"); ?>
                                <?php echo form_input('city', '', 'class="form-control" id="city"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("state", "state"); ?>
                                <?php echo form_input('state', '', 'class="form-control" id="state"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
            <div class="row" style="display: none;">
                <div class="col-md-6">
                    <div class="form-group">
                        <?= lang("ccf2", "cf2"); ?>
                        <?php echo form_input('cf2', '', 'class="form-control" id="cf2"'); ?>
                    </div>
                </div>
            </div>
            <div class="row" style="display: none;">
                <div class="col-md-12">
                    <div class="form-group">
                        <?= lang("ccf6", "cf6"); ?>
                        <?php echo form_input('cf6', '', 'class="form-control" id="cf6"'); ?>
                    </div>
                </div>
            </div>
            <fieldset>
                <legend style="cursor: pointer;" onclick="exibirArquivos();">Clique aqui para adicionar arquivos</legend>
                <div class="row" id="div_arquivos" style="display: none;">
                    <div class="col-md-12">
                        <div class="form-group all">
                                <?= lang("customers_image", "customers_image") ?>
                                <input id="customers_image" type="file" data-browse-label="<?= lang('browse'); ?>" name="customers_image" data-show-upload="true"
                                       data-show-preview="true"  accept=".pdf, .doc, .docx, image/*" class="form-control file">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group all">
                            <?= lang("customers_gallery_images", "images") ?>
                            <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="true"
                                   data-show-preview="true" accept=".pdf, .doc, .docx, image/*" class="form-control file" >
                        </div>
                        <div id="img-details" ></div>
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_customer', lang('add_customer'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript" src="<?= $assets ?>js/valida_cpf_cnpj.js"></script>

<script type="text/javascript">
		
    $(document).ready(function (e) {

        $("#vat_no").mask("999.999.999-99");
        $("#cf5").mask("(99)99999-9999");

        $('#vat_no').blur(function () {
            var cpf_cnpj = $(this);
            var tipoPessoa = $('#tipoPessoa').val();
            consultaPessoa(cpf_cnpj, tipoPessoa, '<?php echo base_url();?>');
        });

        $('#tipoPessoa').change(function (e) {
            $('#vat_no').val('');
            if ($(this).val() === 'PJ' ) {
                $("#vat_no").mask("99.999.999/9999-99");
            } else {
                $("#vat_no").mask("999.999.999-99");
            }
        });

        $('#tipo_documento').change(function (e) {
            if ($(this).val() === 'rg') {
                $('#div_vlPassaporteRG').html('<b>Emissão RG</b>');
                $('#div_rgPassaporte').html('<b>R.G</b>');
            } else if ($(this).val() == 'CN') {
                $('#div_vlPassaporteRG').html('<b>Emissão</b>');
                $('#div_rgPassaporte').html('<b>Certidão de Nascimento</b>');
            } else {
                $('#div_vlPassaporteRG').html('<b>Validade Passaporte</b>');
                $('#div_rgPassaporte').html('<b>Passaporte</b>');
            }
        });

        $('#cf1').change(function (e) {
            $.ajax({
                type: "get",
                url: '<?= site_url('customers/validarRG/') ?>/'+ $(this).val(),
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        alert("O R.G. já esta sendo utilizado pelo passageiro "+data[0].name);
                        $('#cf1').val('');
                    }
                }
            });
        });

        $('#vat_no').change(function (e) {
            $.ajax({
                type: "get",
                url: '<?= site_url('customers/validarCPF/') ?>/'+ $(this).val(),
                dataType: 'json',
                success: function (data) {
                    if (data != null) {
                        alert("O CPF/CNPJ já esta sendo utilizado pelo passageiro "+data[0].name);
                        $('#vat_no').val('');
                    }
                }
            });
        });

        $('textarea').not('.skip').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function(e) {
                var editor = this.$editor.next('textarea');
                if($(editor).attr('required')){
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
                }
            }
        });

    	$("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);
                    img.src = 'images/' + result[i];
                }
            }
        });
        
        $('#add-customer-form').bootstrapValidator({
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh'
            }, excluded: [':disabled']
        });
		
        $('select.select').select2({minimumResultsForSearch: 7});
        fields = $('.modal-content').find('.form-control');
        $.each(fields, function () {
            var id = $(this).attr('id');
            var iname = $(this).attr('name');
            var iid = '#' + id;
            if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
                $("label[for='" + id + "']").append(' *');
                $(document).on('change', iid, function () {
                    $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
                });
            }
        });
		
		$( "#postal_code" ).blur(function() {
            getConsultaCEP();
		});
    });

    function exibirEndereco() {
        if ($('#div_endereco').is(':visible')) {
            $('#div_endereco').hide(600);
        } else {
            $('#div_endereco').show(600);
        }
    }

    function exibirArquivos() {
        if ($('#div_arquivos').is(':visible')) {
            $('#div_arquivos').hide(600);
        } else {
            $('#div_arquivos').show(600);
        }
    }

    function consultaPessoa(tagCpfCnpj, tipoPessoa, base_url) {

        var cpf_cnpj = tagCpfCnpj.val();

        if (cpf_cnpj != '' && cpf_cnpj != '___.___.___-__' && cpf_cnpj != '__.___.___/____-__') {
            if (tipoPessoa === 'PJ') {

                if (!valida_cnpj(cpf_cnpj)) {
                    alert('CNPJ inválido!');
                    tagCpfCnpj.val('');
                    tagCpjCnpj.focus();
                } else {
                    var cpf_cnpj = cpf_cnpj.replace(/[^0-9]/g, '');

                    $.ajax({
                        type: "POST",
                        url: base_url+'customers/wscliente',
                        data : {
                            cnpj : cpf_cnpj
                        },
                        dataType: 'json',
                        success: function (empresa) {

                            empresa = JSON.parse(empresa);

                            if (empresa.status === undefined) {
                                alert("Não foi possível encontrar o cnpj");
                                return;
                            }

                            if (empresa.status === 'ERROR') {
                                alert(empresa.message);
                                return;
                            }

                            if (empresa.situacao !== 'ATIVA') {
                                if (confirm('Esta empres encontra-se na situação '+empresa.situacao+', motivo ' + empresa.motivo_situacao+'. Deseja realmente importar seus dados?')) {
                                    preencherDadosDaPessoaJuridica(empresa);
                                }
                            } else {
                                preencherDadosDaPessoaJuridica(empresa);
                            }
                        }
                    });
                }
            } else {
                if (!valida_cpf(cpf_cnpj)) {
                    alert('CPF inválido!');
                    tagCpfCnpj.val('');
                    tagCpfCnpj.focus();
                }
            }
        }
    }

    function preencherDadosDaPessoaJuridica(empresa) {

        var d = new Date(empresa.abertura);
        var date = [
            d.getFullYear(),
            ('0' + (d.getMonth() + 1)).slice(-2),
            ('0' + d.getDate()).slice(-2)
        ].join('-');

        $('#company').val(empresa.fantasia);
        $('#name').val(empresa.nome);
        $('#phone').val(empresa.telefone);
        $('#email').val(empresa.email);
        $('#data_aniversario').val(date);

        $('#postal_code').val(empresa.cep);
        $('#address').val(empresa.logradouro);

        $('#numero').val(empresa.numero);
        $('#complemento').val(empresa.complemento);
        $('#bairro').val(empresa.bairro);

        $('#city').val(empresa.municipio);
        $('#state').val(empresa.uf);

        let atividades = '';

        for(let i=0; i < empresa.atividade_principal.length; i++) {
            atividades = atividades + empresa.atividade_principal[i].code + ' - ' + empresa.atividade_principal[i].text +'<br/>';
        }

        for (let j=0; j < empresa.atividades_secundarias.length; j++) {
            atividades = atividades + empresa.atividades_secundarias[j].code + ' - ' + empresa.atividades_secundarias[j].text +'<br/>';
        }

        for (let k=0; k < empresa.qsa.length; k++) {
            atividades = atividades + empresa.qsa[k].qual + ' - '+ empresa.qsa[k].nome  +'<br/>';;
        }

        $('#observacao').redactor('set', atividades);
        $('#observacao').val(atividades);
    }

    function getConsultaCEP() {

        if($.trim($("#postal_code").val()) === "") return false;

        var cep = $.trim($("#postal_code").val());
        cep = cep.replace('-','');
        cep = cep.replace('.','');
        cep = cep.replace(' ','');

        var url = 'https://viacep.com.br/ws/' + cep + '/json/';

        $.get(url,
            function (data) {
                if(data !== null){
                    $("#address").val(data.logradouro);
                    $("#bairro").val(data.bairro);
                    $("#city").val(data.localidade);
                    $("#state").val(data.uf);
                }
            });
    }

</script>
