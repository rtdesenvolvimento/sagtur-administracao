<style>
    .document_name {
        color: #14112d;
        font-size: .75em;
        font-weight: 700;
        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
        word-break: break-word;
        margin: 0;
        padding: 4px 4px 4px;
        line-height: normal;
        border-bottom: 1px solid #dbdee0;
    }
</style>
<?php if (!empty($documents)) { ?>
    <?php foreach ($documents as $document) {
        $folder = new Folder_model($document->folder_id);
        $total_signatories = 0;
        $total_signed = 0;
        $aguardando = 0;
        ?>
        <div class="col-lg-2">
            <div class="panel panel-info documents tip" status="<?=$document->document_status;?>" title="<?=$document->name;?>" document_id="<?=$document->id;?>" style="cursor: pointer;">

                <div class="panel-body" style="height: 130px;overflow-y: auto;    padding: 5px;">

                    <?php if (!empty($document->getSignatories())) {?>

                        <?php foreach ($document->getSignatories() as $signatory) {

                            if ($signatory->status == 'signed') {
                                $total_signed++;
                            }

                            if ($signatory->status != 'removed') {
                                $total_signatories++;
                            }
                            ?>
                        <?php $aguardando = $total_signatories -  $total_signed; } ?>

                        <?php foreach ($document->getSignatories() as $signatory) { ?>
                            <div class="sigantarios">
                                <?php if ($signatory->status == 'unsigned') { ?>
                                    <i class="fa fa-clock-o"></i>
                                    <?php if ($signatory->is_biller) {?>
                                        <strong><?= $signatory->name ?> <small>(admin)</small></strong>
                                    <?php } else { ?>
                                        <strong><?= $signatory->name ?> <small>(cliente)</small></strong>
                                    <?php } ?>
                                <?php } else if ($signatory->status == 'canceled' || $signatory->status == 'signed') {?>
                                    <i class="fa fa-check"></i>
                                    <?php if ($signatory->is_biller) {?>
                                        <strong><?= $signatory->name ?> <small>(admin)</small></strong>
                                    <?php } else { ?>
                                        <strong><?= $signatory->name ?> <small>(cliente)</small></strong>
                                    <?php } ?>
                                <?php } else if ($signatory->status == 'removed') {?>
                                    <span style="text-decoration: line-through;"><i class="fa fa-trash-o"></i> <?= $signatory->name ?> <small>(removido)</small></span>
                                <?php } else if ($signatory->status == 'rejected') {?>
                                    <span style="text-decoration: line-through;"><i class="fa fa-trash-o"></i> <?= $signatory->name ?> <small>(rejeitado)</small></span>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } else {?>
                        <div class="alert">
                            <?= lang('no_signatories_found') ?>
                        </div>
                    <?php } ?>
                </div>

                <div class="panel-heading">
                    <div class="box-icon">
                        <ul class="btn-tasks">
                            <li class="dropdown">
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" style="font-size: 20px;" data-placement="left" title="" data-original-title="Ações"></i></a>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel" style="display: none;">
                                    <li>
                                        <a href="<?=base_url().'contracts/events/'.$document->id;?>"
                                           data-toggle="modal"
                                           data-target="#myModal">
                                            <i class="fa fa-archive"></i> <?=lang('events')?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <?php if ($document->sale_id){?>
                                        <li>
                                            <a href="<?=base_url().'salesutil/modal_view/'.$document->sale_id;?>"
                                               data-toggle="modal"
                                               data-target="#myModal">
                                                <i class="fa fa-eye"></i> <?=lang('view_sale')?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                    <?php } ?>

                                    <?php if ($document->document_status == 'draft') {?>
                                        <li>
                                            <a href="<?=base_url('contracts/addSignatories/'.$document->id);?>" target="_blank">
                                                <i class="fa fa-send"></i> <?=lang('open_new_page')?>
                                            </a>
                                        </li>
                                    <?php } else { ?>
                                       <li>
                                            <a href="<?=base_url('contracts/monitorContractSignature/'.$document->id);?>" target="_blank">
                                                <i class="fa fa-send"></i> <?=lang('open_new_page')?>
                                            </a>
                                        </li>
                                    <?php } ?>

                                    <li>
                                        <a href="<?=site_url('contracts/to_share/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-share-alt"></i> <?=lang('to_share')?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=site_url('contracts/to_share_validate/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-list"></i> <?=lang('to_share_validate')?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=site_url('contracts/sendDocumentTo/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-envelope-o"></i> <?=lang('send_document')?>
                                        </a>
                                    </li>

                                     <?php if ($document->document_status == 'unsigned' || $document->document_status == 'signed' || $document->document_status == 'pending') {?>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#" class="resend_to_signatories" id="<?=$document->id;?>">
                                                <i class="fa fa-history"></i> <?=lang('resend_to_signatories')?>
                                            </a>
                                        </li>
                                     <?php } elseif ($document->document_status == 'completed') { ?>
                                         <li class="divider"></li>
                                         <li>
                                             <a href="#" class="send_documento_to_whatsapp" id="<?=$document->id;?>">
                                                 <i class="fa fa-whatsapp"></i> <?=lang('send_documento_to_whatsapp')?>
                                             </a>
                                         </li>
                                     <?php } ?>

                                    <li>
                                        <a href="<?=site_url('contracts/editDocument/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-edit"></i> <?=lang('rename_document')?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="<?=site_url('contracts/downloadDocument/'.$document->id)?>" target="_blank">
                                            <i class="fa fa-download"></i> <?=lang('download_signed_document')?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a href="#" class="cancel_document" id="<?=$document->id;?>">
                                            <i class="fa fa-trash-o"></i> <?=lang('cancel_document')?>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                   <small><?= $this->sma->hrld($document->created_at);?></small><br/>

                    <?php if ($aguardando > 0) {?>
                        <div class="aguardando">
                            <small><i class="fa fa-clock-o"></i> Aguardando (<?= $aguardando;?> de <?= $total_signatories;?>)</small>
                        </div>
                    <?php } else { ?>
                        <div><br/></div>
                    <?php } ?>

                    <div class="status_documento">
                        <?php if($document->document_status == 'unsigned') {?>
                            <div style="color: #f44336;">
                                <i class="fa fa-clock-o"></i> <?=lang($document->document_status)?>
                            </div>
                        <?php } else if ($document->document_status == 'draft') { ?>
                            <div style="color: #ff9800;">
                                <i class="fa fa-edit"></i> <?=lang($document->document_status)?>
                            </div>
                        <?php } else if ($document->document_status == 'canceled') { ?>
                            <div style="color: #333333;">
                                <i class="fa fa-trash-o"></i> <?=lang($document->document_status)?>
                            </div>
                        <?php } else if ($document->document_status == 'pending'){ ?>
                            <div style="color: #0064cd;">
                                <i class="fa fa-clock-o"></i> <?=lang($document->document_status)?>
                            </div>
                        <?php } else  if ($document->document_status == 'signed' || $document->document_status == 'completed') {?>
                            <div style="color: #4caf50;">
                                <i class="fa fa-check"></i> <?=lang($document->document_status)?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } else { ?>
    <div class="alert alert-info">
        <?= lang('no_documents_found') ?>
    </div>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('.cancel_document').click(function (e) {

            e.preventDefault();

           if (confirm('Deseja Realmente Cancelar o Documento?')) {
               var document_id = $(this).attr('id');
               var url = '<?=base_url('contracts/cancelDocument/');?>' + document_id;
               $.ajax({
                   url: url,
                   type: 'GET',
                   success: function (data) {
                       location.reload();
                   }
               });
           }
        });

        $('.resend_to_signatories').click(function (e) {

            e.preventDefault();

            if (confirm('Deseja Realmente Reenviar o Documento para os Signatários?')) {
                var document_id = $(this).attr('id');
                var url = '<?=base_url('contracts/resendToSignatories/');?>' + document_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.send_documento_to_whatsapp').click(function (e) {

            e.preventDefault();

            if (confirm('Deseja Realmente Enviar o Documento Assinado para os Signatários via WhatsApp?')) {
                var document_id = $(this).attr('id');
                var url = '<?=base_url('contracts/sendDocumentToWhatsApp/');?>' + document_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });
    });
</script>
