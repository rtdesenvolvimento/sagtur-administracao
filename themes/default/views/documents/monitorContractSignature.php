<style>
    .signatory_name {
        color: #14112d;
        font-size: 1.5em;
        font-weight: 700;
        line-height: 1.12em;
        width: 100%;
        word-break: keep-all;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 0 5px 10px 0;
    }

    .signatory_history {
        color: #14112d;
        font-size: 12px;
        width: 100%;
        word-break: keep-all;
        overflow: hidden;
        text-overflow: ellipsis;
        padding: 0 5px 10px 0;
    }


    .signatory_note {
        padding: 5px;
        border: 1px solid #ddd;
        background-color: #f6f6f6;
        box-shadow: none;
        margin-bottom: 10px;
    }

    .signatory_status {
        padding: 10px 0px 10px;
    }

    .signatory_status .signatory_status_nao_assinado {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
        background-color: #f44336;

    }

    .signatory_status .signatory_status_assinado {
        display: inline;
        padding: .2em .6em .3em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25em;
        background-color: #8BC34A;
    }
</style>


<?php echo form_open("contracts/create_signatories/".$document->id, array('data-toggle' => 'validator', 'role' => 'form')) ?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa fa-file-text-o"></i>#<?= $document->reference_no.' | '.$document->name; ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=base_url().'contracts/events/'.$document->id;?>"
                               data-toggle="modal"
                               data-target="#myModal">
                                <i class="fa fa-archive"></i> <?=lang('events')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <?php if ($document->sale_id){?>
                            <li>
                                <a href="<?=base_url().'salesutil/modal_view/'.$document->sale_id;?>"
                                   data-toggle="modal"
                                   data-target="#myModal">
                                    <i class="fa fa-eye"></i> <?=lang('view_sale')?>
                                </a>
                            </li>
                            <li class="divider"></li>
                        <?php } ?>
                        <li>
                            <a href="<?=site_url('contracts/to_share/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-share-alt"></i> <?=lang('to_share')?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=site_url('contracts/to_share_validate/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-list"></i> <?=lang('to_share_validate')?>
                            </a>
                        </li>

                        <li>
                            <a href="<?=site_url('contracts/sendDocumentTo/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-envelope-o"></i> <?=lang('send_document')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=site_url('contracts/editDocument/'.$document->id)?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-edit"></i> <?=lang('rename_document')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=site_url('contracts/downloadDocument/'.$document->id)?>" target="_blank">
                                <i class="fa fa-download"></i> <?=lang('download_signed_document')?>
                            </a>
                        </li>

                        <?php if ($document->document_status == 'unsigned' || $document->document_status == 'signed' || $document->document_status == 'pending') {?>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="resend_to_signatories" id="<?=$document->id;?>">
                                    <i class="fa fa-history"></i> <?=lang('resend_to_signatories')?>
                                </a>
                            </li>
                        <?php } elseif ($document->document_status == 'completed')  { ?>
                            <li class="divider"></li>
                             <li>
                                 <a href="<?=site_url('contracts/sendDocumentToWhatsApp/'.$document->id)?>" target="_blank">
                                     <i class="fa fa-whatsapp"></i> <?=lang('send_documento_to_whatsapp')?>
                                 </a>
                             </li>
                        <?php } ?>

                        <li class="divider"></li>
                        <li>
                            <a href="#" class="cancel_document" id="<?=$document->id;?>">
                                <i class="fa fa-trash-o"></i> <?=lang('cancel_document')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-md-4">

                <ul id="myTab" class="nav nav-tabs" style="text-align: center">
                    <li><a href="#abageral" class="tab-grey"><i class="fa fa-edit"></i> <?= lang('signatorys') ?></a></li>
                    <li style="display: none;"><a href="#history" class="tab-grey"><i class="fa fa-history"></i> <?= lang('signatorys_history') ?></a></li>
                </ul>

                <div class="tab-content">
                    <div id="abageral" class="tab-pane fade in">

                        <div class="row">
                            <div class="col-md-12">
                                <?php foreach ($document->getSignatories() as $signatory) {
                                    if ($signatory->status == 'removed') {
                                        continue;
                                    }
                                    ?>
                                    <div class="col-md-12 signatory_note">
                                        <div class="col-md-12">
                                            <table style="width: 100%;">

                                                <tr>
                                                    <td colspan="2">
                                                        <div class="signatory_status">
                                                            <?php if ($signatory->is_biller) { ?>
                                                                Responsável
                                                            <?php } else { ?>
                                                                Signatário
                                                            <?php } ?>
                                                            <?php if (($signatory->status == 'signed')) { ?>
                                                                <span class="signatory_status_assinado"><?=lang($signatory->status);?></span>
                                                            <?php } elseif ($signatory->status == 'rejected') { ?>
                                                                <span class="signatory_status_nao_assinado"><?=lang($signatory->status);?></span>
                                                            <?php } else { ?>
                                                                <span class="signatory_status_nao_assinado"><?=lang($signatory->status);?></span>
                                                            <?php } ?>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <?php if ($signatory->status == 'unsigned') { ?>
                                                    <tr>
                                                        <td colspan="2">
                                                            <div class="text-right">
                                                                <div class="btn-group text-right">
                                                                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                                                    <ul class="dropdown-menu pull-right" role="menu">
                                                                        <li>
                                                                            <a href="#" class="resend_to_signatory" signatory_id="<?=$signatory->id;?>">
                                                                                <i class="fa fa-recycle"></i> <?=lang('resend_to_signatory')?>
                                                                            </a>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li>
                                                                            <a href="#" class="cancel_signatory" signatory_id="<?=$signatory->id;?>">
                                                                                <i class="fa fa-trash-o"></i> <?=lang('cancel_signatory')?>
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <tr>
                                                    <td><i class="fa fa-user  fa-2x"></i></td>
                                                    <td><div class="signatory_name"><?=$signatory->name;?></div></td>
                                                </tr>
                                                <?php if ($signatory->email) {?>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <div class="input-group" style="margin-top: 5px;">
                                                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                    <i class="fa fa-envelope-o"  style="font-size: 1.2em;cursor: pointer;"></i>
                                                                </div>
                                                                <?= form_input('email', $signatory->email, 'disabled class="form-control"'); ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($signatory->phone) {?>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <div class="input-group" style="margin-top: 5px;">
                                                                <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                    <i class="fa fa-whatsapp"  style="font-size: 1.2em;cursor: pointer;"></i>
                                                                </div>
                                                                <?= form_input('phone', $signatory->phone, 'disabled class="form-control"'); ?>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <?php if ($signatory->url_link) {?>
                                                    <tr>
                                                        <td></td>
                                                        <td>
                                                            <div class="input-group" style="margin-top: 5px;">
                                                                <?= form_input('url_link', $signatory->url_link, 'disabled class="form-control"'); ?>
                                                                <div class="input-group-addon no-print copy" url="<?=$signatory->url_link;?>" style="padding: 2px 8px;">
                                                                    <i class="fa fa-copy"  style="font-size: 1.2em;cursor: pointer;"></i>
                                                                </div>

                                                                <?php if ($signatory->is_biller && $signatory->status == 'unsigned') { ?>
                                                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                                        <a href="<?=$signatory->url_link;?>" target="_blank" >
                                                                            <i class="fa fa-send"  style="font-size: 1.2em;cursor: pointer;"></i>
                                                                        </a>
                                                                    </div>
                                                                <?php } ?>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                <?php } ?>

                                                <?php if ($signatory->status == 'unsigned') { ?>
                                                    <tr>
                                                        <td></td>
                                                        <td style="border-bottom: 1px dotted #9d8686;padding: 10px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fa fa-clock-o fa-2x" style="margin-top: 15px;"></i></td>
                                                        <td><div class="signatory_history" style="margin-top: 15px;">Ainda não assinou <br/><small>Ainda não assinou este documento</small></div></td>
                                                    </tr>
                                                <?php } else if ($signatory->status == 'signed') { ?>
                                                    <tr>
                                                        <td></td>
                                                        <td style="border-bottom: 1px dotted #9d8686;padding: 10px;"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><i class="fa fa-check fa-2x" style="color: green;margin-top: 15px;"></i></td>
                                                        <td><div class="signatory_history" style="margin-top: 15px;">Assinou o documento<br/><small><?=$this->sma->hrld($signatory->date_signature);?></small></div></td>
                                                    </tr>
                                                <?php } ?>


                                                <tr style="display:none;">
                                                    <td colspan="2">
                                                        <?php echo form_button('assinar_documento',
                                                            $this->lang->line("assinar_documento"), 'id="assinar_documento" document_id="'.$document->id.'" signatory_id="'.$signatory->id.'"   style="width: 100%;margin-top: 15px;margin-bottom: 15px;" class="btn btn-success me_buttom"'); ?>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>

                            <div class="col-md-12">
                                <div class="well well-sm">
                                    <p>
                                        Criado por: <?= $document->biller;?> <br>
                                        Data: <?= $this->sma->hrld($document->created_at); ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a href="<?=site_url('contracts/addSignatory/'.$document->id)?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <?php echo form_button('add_signatory', '<i class="fa fa-plus"></i> '.$this->lang->line("add_signatory"), 'id="add_signatory"  style="width: 49%;" class="btn btn-primary"'); ?>
                                    </a>
                                    <a href="<?=site_url('contracts/addCustomer/'.$document->id)?>" class="external" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                        <?php echo form_button('add_customer', '<i class="fa fa-search"></i> '. $this->lang->line("search_customers"), 'id="add_customer" style="width: 50%;" class="btn btn-primary"'); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <iframe src="<?=site_url('contracts/downloadDocument/'.$document->id)?>" width="100%" height="900px"></iframe>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">
    $(document).ready(function () {

        $('.cancel_document').click(function (e) {
            e.preventDefault();
            if (confirm('Deseja Realmente Cancelar o Documento?')) {
                var document_id = $(this).attr('id');
                var url = '<?=base_url('contracts/cancelDocument/');?>' + document_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.resend_to_signatories').click(function (e) {
            e.preventDefault();
            if (confirm('Deseja Realmente Reenviar o Documento para os Signatários?')) {
                var document_id = $(this).attr('id');
                var url = '<?=base_url('contracts/resendToSignatories/');?>' + document_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.resend_to_signatory').click(function (e) {
            e.preventDefault();
            if (confirm('Deseja Realmente Reenviar o Documento para o Signatário?')) {
                var signatory_id = $(this).attr('signatory_id');
                var url = '<?=base_url('contracts/resendToSignatory/'.$document->id.'/');?>' + signatory_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.cancel_signatory').click(function (e) {
            e.preventDefault();
            if (confirm('Deseja Realmente Cancelar o Signatário?')) {
                var signatory_id = $(this).attr('signatory_id');
                var url = '<?=base_url('contracts/cancelSignatory/');?>' + signatory_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.me_buttom').click(function () {

            var document_id = $(this).attr('document_id');
            var signatory_id = $(this).attr('signatory_id');

            if (confirm('Deseja Realmente Assinar o Documento?')) {
                $.ajax({
                    url: '<?=site_url('contracts/sign/');?>'+document_id+'/'+signatory_id,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

        $('.copy').click(function (){

            var url = $(this).attr('url');

            copyURLToClipboard(url);
        });
    });

    function copyURLToClipboard(text) {
        var textArea = document.createElement("textarea");

        textArea.style.position = 'fixed';
        textArea.style.top = 0;
        textArea.style.left = 0;
        textArea.style.width = '2em';
        textArea.style.height = '2em';
        textArea.style.padding = 0;
        textArea.style.border = 'none';
        textArea.style.outline = 'none';
        textArea.style.boxShadow = 'none';
        textArea.style.background = 'transparent';
        textArea.value = text;

        document.body.appendChild(textArea);
        textArea.select();

        try {
            var successful = document.execCommand('copy');
            var msg = successful ? 'successful' : 'unsuccessful';
            console.log('Copying text command was ' + msg);
        } catch (err) {
            console.log('Oops, unable to copy');
            window.prompt("Copie para área de transferência: Ctrl+C e tecle Enter", text);
        }

        document.body.removeChild(textArea);
        operacaoSucesso('Copiado com sucesso!', 2000);
    }
</script>
