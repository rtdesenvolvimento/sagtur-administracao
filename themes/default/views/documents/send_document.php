<?php
$contact = '';
$type = 'telefone';
foreach ($document->getSignatories() as $signatory) { ?>
    <?php if (!$signatory->is_biller || !$signatory->status == 'removed') { ?>
        <?php if ($signatory->phone) { ?>
            <?php $contact = $signatory->phone; ?>
            <?php $type = 'telefone'; ?>
            <?php break; ?>
        <?php } else {?>
            <?php $contact = $signatory->email; ?>
            <?php $type = 'email'; ?>
            <?php break; ?>
        <?php }?>
    <?php }?>
<?php }?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('send_document'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("contracts/sendDocumentTo/".$document->id, $attrib); ?>
        <div class="modal-body">
            <div class="form-group">
                <?= lang("shipping_type", "shipping_type_send") ?>
                <?php
                $opts = array(
                    'telefone' => lang('signer_telefone'),
                    'email' => lang('signer_email'),
                );
                echo form_dropdown('shipping_type', $opts, $type , 'id="shipping_type_send" class="form-control" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?php echo form_input('signer_field', $contact, 'id="signer_field_send" class="form-control tip" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("signatories_note_send", "signatories_note_send"); ?>
                <?php echo form_textarea('signatories_note', $this->Settings->note_contract, 'class="form-control skip" placeholder="' . lang("signatory_note_placeholder") . '" id="signatories_note_send" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('send_document', lang('send_document'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $('#shipping_type_send').change(function () {
            var type = $(this).val();

            $('#signer_field_send').val('');

            if (type === 'telefone') {
                $('#signer_field_send').attr('type', 'tel');
                $('#signer_field_send').attr('placeholder', 'Telefone do Signatário');

                $('#signer_field_send').keyup(function (event) {
                    mascaraTelefone( this, mtel );
                });

            } else {
                $('#signer_field_send').attr('type', 'email');
                $('#signer_field_send').attr('placeholder', 'E-mail do Signatário');
                $('#signer_field_send').off('keyup');
            }
        });
    });

    function mascaraTelefone(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaratelefone()",1)
    }

    function execmascaratelefone(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mtel(v){
        v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
        v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
        v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
        return v;
    }
</script>
