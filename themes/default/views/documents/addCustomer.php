<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_signatory'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open("contracts/addCustomer/".$document->id, $attrib); ?>
        <div class="modal-body">
            <div class="form-group">
                <?= lang("customer", "slcustomer") ?>
                <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="slcustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" required="required" class="form-control input-tip" style="width:100%;"'); ?>
            </div>
            <div class="form-group">
                <?= lang("signatories_note_send", "signatories_note_send"); ?>
                <?php echo form_textarea('signatories_note', $this->Settings->note_contract, 'class="form-control skip" placeholder="' . lang("signatory_note_placeholder") . '" id="signatories_note_send" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_signatory', lang('add_signatory'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        nsCustomer();
    });

    function nsCustomer() {

        $('#slcustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "saleitem/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {

            if (e.added === undefined) return false;

            add_signatory(e.added);

        });
    }

</script>
