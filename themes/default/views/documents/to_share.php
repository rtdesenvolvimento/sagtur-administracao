<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('to_share'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div  class="col-xs-12">
                    <h1 style="font-size: 25px;"><?=$document->name;?></h1>
                    <hr>
                    <b style="font-size: 12px;"> URL: <a href="<?=$document->url_link;?>" target="_blank"><?=$document->url_link;?></a></b><br/>
                    <a class="btn btn-success mobile_btn" id="copy"
                       style="background: #77b43f;margin-top: 10px;color: #ffffff;font-size: 1.5rem;width: 100%;"> COPIAR URL </a>
                </div>
            </div>
        </div>
        <div class="modal-footer" style="text-align: left;">
            <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>

<script type="application/javascript">

    $(document).ready(function () {
        $('#copy').click(function (){
            copyTextToClipboardQRCode('<?=$document->url_link;?>', '');
        });
    });

    function copyTextToClipboardQRCode(text) {

        if (!navigator.clipboard) {
            fallbackCopyTextToClipboard(text);
            return;
        }

        document.body.focus();

        navigator.clipboard.writeText(text)
            .then(() => {
                $('.mobile_btn').css('background', '#28a745');
                $('.mobile_btn').html('COPIADO!')
                console.log('Texto copiado com sucesso para a área de transferência:', text);
            })
            .catch(err => {
                console.error('Ocorreu um erro ao copiar o texto para a área de transferência:', err);
            });
    }

    function fallbackCopyTextToClipboard(text) {

        document.body.focus();

        var textArea = document.createElement("textarea");
        textArea.value = text;

        document.body.appendChild(textArea);

        textArea.select();
        document.execCommand('copy');

        document.body.removeChild(textArea);

        $('.mobile_btn').css('background', '#28a745');
        $('.mobile_btn').html('COPIADO!')
    }
</script>
