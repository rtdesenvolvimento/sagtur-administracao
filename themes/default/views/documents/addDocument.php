<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_document'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("contracts/addDocument", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <div class="form-group all">
                <?= lang("folder", "folder_id") ?>
                <?php
                $cbFolders[''] = lang("select") . " " . lang("folder");
                foreach ($folders as $folder) {
                    $cbFolders[$folder->id] = $folder->name;
                }
                echo form_dropdown('folder_id', $cbFolders,  $folderID, 'class="form-control select" id="folder_id" placeholder="' . lang("select") . " " . lang("folder") . '"style="width:100%" required="required"');
                ?>
            </div>
            <div class="form-group">
                <?= lang('doc_name', 'name'); ?>
                <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group all">
                <?= lang("document", "file") ?>
                <input id="file" type="file" data-browse-label="<?= lang('browse'); ?>" required="required" name="file" data-show-upload="false"
                       data-show-preview="false" accept="application/pdf,text/rtf" class="form-control file">
            </div>
            <div class="form-group" style="display:none;">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_document', lang('add_document'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>