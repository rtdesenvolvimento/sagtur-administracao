<style type="text/css" media="screen">

    #SLData td:nth-child(7) {text-align: right;}
    #SLData td:nth-child(8) {text-align: right;}
    #SLData td:nth-child(9) {text-align: right;}

</style>

<script>
    $(document).ready(function () {

        $('#situacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#billerFilter').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#filterOpcional').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=site_url('itinerario/getSalesReceptivo')?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.className = "invoice_link";
                return nRow;
            },
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "situacao", "value":  $('#situacao').val() });
                aoData.push({ "name": "billerFilter", "value":  $('#billerFilter').val() });
                aoData.push({ "name": "filterOpcional", "value":  $('#filterOpcional').val() });
            },
            "aoColumns": [
                {
                    "bSortable": false,
                    "mRender": checkbox
                },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {"bSortable": false}
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

            }
        });

        if (localStorage.getItem('remove_slls')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            localStorage.removeItem('remove_slls');
        }

        <?php if ($this->session->userdata('remove_slls')) {?>
        if (localStorage.getItem('slitems')) {
            localStorage.removeItem('slitems');
        }
        if (localStorage.getItem('sldiscount')) {
            localStorage.removeItem('sldiscount');
        }
        if (localStorage.getItem('sltax2')) {
            localStorage.removeItem('sltax2');
        }
        if (localStorage.getItem('slref')) {
            localStorage.removeItem('slref');
        }
        if (localStorage.getItem('slshipping')) {
            localStorage.removeItem('slshipping');
        }
        if (localStorage.getItem('slwarehouse')) {
            localStorage.removeItem('slwarehouse');
        }
        if (localStorage.getItem('slnote')) {
            localStorage.removeItem('slnote');
        }
        if (localStorage.getItem('slinnote')) {
            localStorage.removeItem('slinnote');
        }
        if (localStorage.getItem('slcustomer')) {
            localStorage.removeItem('slcustomer');
        }
        if (localStorage.getItem('slbiller')) {
            localStorage.removeItem('slbiller');
        }
        if (localStorage.getItem('slcurrency')) {
            localStorage.removeItem('slcurrency');
        }
        if (localStorage.getItem('sldate')) {
            localStorage.removeItem('sldate');
        }
        if (localStorage.getItem('slsale_status')) {
            localStorage.removeItem('slsale_status');
        }
        if (localStorage.getItem('slpayment_status')) {
            localStorage.removeItem('slpayment_status');
        }
        if (localStorage.getItem('paid_by')) {
            localStorage.removeItem('paid_by');
        }
        if (localStorage.getItem('amount_1')) {
            localStorage.removeItem('amount_1');
        }
        if (localStorage.getItem('paid_by_1')) {
            localStorage.removeItem('paid_by_1');
        }
        if (localStorage.getItem('pcc_holder_1')) {
            localStorage.removeItem('pcc_holder_1');
        }
        if (localStorage.getItem('pcc_type_1')) {
            localStorage.removeItem('pcc_type_1');
        }
        if (localStorage.getItem('pcc_month_1')) {
            localStorage.removeItem('pcc_month_1');
        }
        if (localStorage.getItem('pcc_year_1')) {
            localStorage.removeItem('pcc_year_1');
        }
        if (localStorage.getItem('pcc_no_1')) {
            localStorage.removeItem('pcc_no_1');
        }
        if (localStorage.getItem('cheque_no_1')) {
            localStorage.removeItem('cheque_no_1');
        }
        if (localStorage.getItem('slpayment_term')) {
            localStorage.removeItem('slpayment_term');
        }

        localStorage.removeItem('previsao_pagamento');
        localStorage.removeItem('valor');
        localStorage.removeItem('tipoCobrancaId');
        localStorage.removeItem('condicaopagamentoId');

        <?php $this->sma->unset_data('remove_slls');}
        ?>

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });

    });

</script>

<?php if ($Owner || $GP['bulk_actions']) {
	    echo form_open('itinerario/itinerario_actions', 'id="action-form"');
	}
?>
<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa-fw fa fa-heart"></i><?=lang('sales');?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i>
                    </a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('sales/add')?>">
                                <i class="fa fa-plus-circle"></i> <?=lang('add_sale')?>
                            </a>
                        </li>
                    </ul>
                </li>
        </div>
    </div>
    <div class="box-content">

        <div class="panel panel-info">
            <div class="panel-heading"><i class="fa-fw fa fa-filter"></i> Filtros</div>
            <div class="panel-body">
                <div class="row">
                    <div style="margin-bottom: 20px;">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("data_ate", "data_ate"); ?>
                                <?php echo form_input('data_de', (isset($_POST['data_de']) ? $_POST['data_de'] : ""), 'class="form-control" id="data_de"','date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("data_ate", "data_ate"); ?>
                                <?php echo form_input('data_ate', (isset($_POST['data_ate']) ? $_POST['data_ate'] : ""), 'class="form-control" id="data_ate"', 'date'); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("hora_de", "hora_de"); ?>
                                <?php echo form_input('hora_de', (isset($_POST['hora_de']) ? $_POST['hora_de'] : ""), 'class="form-control" id="hora_de"','date'); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("hora_ate", "hora_ate"); ?>
                                <?php echo form_input('hora_ate', (isset($_POST['hora_ate']) ? $_POST['hora_ate'] : ""), 'class="form-control" id="hora_ate"','date'); ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("local_embarque", "f_local_embarque"); ?>
                            <?php echo form_input('f_local_embarque', (isset($_POST['f_local_embarque']) ? $_POST['f_local_embarque'] : ""), 'id="f_local_embarque" data-placeholder="' . lang("select") . ' ' . lang("local_embarque") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("product", "f_servico"); ?>
                            <?php echo form_input('f_servico', (isset($_POST['f_servico']) ? $_POST['f_servico'] : ""), 'id="f_servico" data-placeholder="' . lang("select") . ' ' . lang("product") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("category", "f_categoria"); ?>
                            <?php echo form_input('f_categoria', (isset($_POST['f_categoria']) ? $_POST['f_categoria'] : ""), 'id="f_categoria" data-placeholder="' . lang("select") . ' ' . lang("category") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("motorista", "f_motorista"); ?>
                            <?php echo form_input('f_motorista', (isset($_POST['f_motorista']) ? $_POST['f_motorista'] : ""), 'id="f_motorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("guia", "f_guia"); ?>
                            <?php echo form_input('f_guia', (isset($_POST['f_guia']) ? $_POST['f_guia'] : ""), 'id="f_guia" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                        <div class="col-sm-4">
                            <?= lang("veiculo", "f_veiculo"); ?>
                            <?php echo form_input('f_veiculo', (isset($_POST['f_veiculo']) ? $_POST['f_veiculo'] : ""), 'id="f_veiculo" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="controls"> <?php echo form_button('gerar_itineario', $this->lang->line("gerar_itineario"), 'class="btn btn-primary" id="gerar_itineario"'); ?> </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                            </th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th><?php echo $this->lang->line("time"); ?></th>
                            <th><?php echo $this->lang->line("local_embarque"); ?></th>
                            <th><?php echo $this->lang->line("product"); ?></th>
                            <th><?php echo $this->lang->line("total_pax"); ?></th>
                            <th><?php echo $this->lang->line("veiculo"); ?></th>
                            <th><?php echo $this->lang->line("meio_divulgacao"); ?></th>
                            <th><?php echo $this->lang->line("category"); ?></th>
                            <th><?php echo $this->lang->line("idioma"); ?></th>
                            <th><?php echo $this->lang->line("customer"); ?></th>
                            <th><?php echo $this->lang->line("guia"); ?></th>
                            <th><?php echo $this->lang->line("motorista"); ?></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="14" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th colspan="14"></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <input type="hidden" name="motorista" value="" id="motorista"/>
        <input type="hidden" name="guia" value="" id="guia"/>
        <input type="hidden" name="veiculo" value="" id="veiculo"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php } ?>

<!--#################### INICIO MODAL GERAR ITINERARIO ######################### -->
<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                <h4 class="modal-title"><?=lang('gerar_itinerario');?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group" style="margin-bottom: 15px;">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('motorista') ?></label>
                        <div class="col-sm-8">
                            <?php echo form_input('m_motorista', (isset($_POST['m_motorista']) ? $_POST['m_motorista'] : ""), 'id="m_motorista" data-placeholder="' . lang("select") . ' ' . lang("motorista") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('guia') ?></label>
                        <div class="col-sm-8">
                            <?php echo form_input('m_guia', (isset($_POST['m_guia']) ? $_POST['m_guia'] : ""), 'id="m_guia" data-placeholder="' . lang("select") . ' ' . lang("guia") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>
                    </div>
                    <div class="form-group" style="margin-bottom: 15px;">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('veiculo') ?></label>
                        <div class="col-sm-8">
                            <?php echo form_input('m_veiculo', (isset($_POST['m_veiculo']) ? $_POST['m_veiculo'] : ""), 'id="m_veiculo" data-placeholder="' . lang("select") . ' ' . lang("veiculo") . '" class="form-control input-tip" style="width:100%;"'); ?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary"  data-action="export_montar" id="montar"><?= lang('save') ?></button>
            </div>
        </div>
    </div>
</div>
<!--#################### FINAL MODAL PACOTES PROPRIOS ######################### -->

<script type="text/javascript">

    $(document).ready(function(){

        $('#gerar_itineario').click(function() {
            $('#prModal').appendTo("body").modal('show');
        });

        $('#m_motorista').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#motorista').val($('#m_motorista').val());
        });

        $('#m_guia').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#guia').val($('#m_guia').val());
        });

        $('#m_veiculo').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "veiculo/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#veiculo').val($('#m_veiculo').val());
        });


        $('#f_motorista').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "veiculo/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

    });
</script>
