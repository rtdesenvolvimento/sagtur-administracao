<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $Settings->site_name;?>">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>
    <title><?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO</title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $Settings->site_name;?> || PAGUE COM CARTÃO DE CRÉDITO" >
    <meta name="keywords" content="<?php echo $Settings->site_name;?>">
    <meta name="application-name" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="pt_br" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $Settings->site_name;?>"  />
    <meta property="og:description" content="CARTÃO DE CRÉDITO || <?php echo $fatura->product_name;?>" />
    <meta property="og:site_name" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO" />
    <meta property="og:image:alt" content="<?php echo $Settings->site_name;?> || CARTÃO DE CRÉDITO" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= $assets ?>styles/mercadopago/index.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/rapido/plugins/font-awesome/css/font-awesome.min.css">

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
    <script src="<?php echo base_url() ?>assets/rapido/plugins/blockUI/jquery.blockUI.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

    <script src="https://sdk.mercadopago.com/js/v2"></script>

    <style>
        body {
            background: #ff4957;
        }
        .error {
            border: 1px solid red;
        }

        label.error {
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            font-size: 12px;
            position: absolute;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            top: -5px;
            right: -5px;
            z-index: 2;
            height: 25px;
            line-height: 1;
            background-color: #e34f4f;
            color: #fff !important;;
            font-weight: normal;
            display: inline-block;
            padding: 6px 8px;
        }

        label.error:after {
            content: '';
            position: absolute;
            border-style: solid;
            border-width: 0 6px 6px 0;
            border-color: transparent #e34f4f;
            display: block;
            width: 0;
            z-index: 1;
            bottom: -6px;
            left: 20%;
        }

    </style>

    <?php if ($this->Settings->pixelFacebook) {?>
        <!-- Meta Pixel Code | Purchase -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document,'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '<?php echo $this->Settings->pixelFacebook;?>');
            fbq('track', 'Purchase', {
                content_name: '<?=str_replace("'", "", $fatura->product_name);?>',
                content_type: 'product',
                value: <?=$fatura->valorpagar;?>,
                currency: 'BRL'
            });
        </script>
        <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=<?php echo $this->Settings->pixelFacebook;?>&ev=Purchase&noscript=1"/></noscript><!-- End Facebook Pixel Code -->
        <!-- End Meta Pixel Code -->
    <?php }?>

</head>
<body>
<main>
    <!-- Payment -->
    <section class="payment-form dark">
        <div class="container__payment">
            <div class="block-heading">
                <h2>Cartão de Crédito</h2>
            </div>
            <div class="form-payment">
                <div class="products">
                    <div class="item">
                        <span class="price" id="summary-price"></span>
                        <p class="item-name"> <?php echo $fatura->product_name;?> <span id="summary-quantity"></span></p>
                    </div>
                    <div class="total">Valor Pagar<span class="price" id="summary-total"><?php echo $this->sma->formatMoney($fatura->valorpagar);?></span></div>
                </div>
                <div class="payment-details">
                    <?php $attrib = array('id' => 'form-checkout');echo form_open_multipart("cartaocredito/processar_pagamento/".$cobrancaFatura->code, $attrib);  ?>
                        <h3 class="title">Informe os dados do titular</h3>
                        <div class="row">
                            <div class="form-group col">
                                <label for="cardholderEmail">E-mail</label>
                                <input id="form-checkout__cardholderEmail" name="cardholderEmail" type="email" required="required"
                                       value="<?php echo $cliente->email;?>" class="form-control"/>
                                <span style="font-size: 11px;">Enviaremos os detalhes do pagamento para você assim que terminar</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-sm-5">
                                <label for="identificationType">Identificação</label>
                                <select id="form-checkout__identificationType" name="identificationType" required="required" class="form-control"></select>
                            </div>
                            <div class="form-group col-sm-7">
                                <label for="docNumber">Documento do Titular do Cartão</label>
                                <input id="form-checkout__identificationNumber" name="docNumber" type="tel"
                                       required="required"
                                       value="<?php echo str_replace ( '-', '', str_replace ( '.', '', $cliente->vat_no));?>" class="form-control"/>
                            </div>
                        </div>
                        <br>
                        <h3 class="title">Preencha os dados do seu cartão</h3>
                        <div class="row">
                            <div class="form-group col-sm-8">
                                <label for="cardholderName">Nome do titular</label>
                                <input id="form-checkout__cardholderName" name="cardholderName" type="text" required="required" class="form-control"/>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="cardExpirationMonth">Vencimento</label>
                                <div class="input-group expiration-date">
                                    <input id="form-checkout__cardExpirationMonth" name="cardExpirationMonth" maxlength="2" required="required" type="tel" class="form-control"/>
                                    <span class="date-separator">/</span>
                                    <input id="form-checkout__cardExpirationYear" name="cardExpirationYear" maxlength="2" required="required" type="tel" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="cardNumber">Número do cartão</label>
                                <input id="form-checkout__cardNumber" name="cardNumber" maxlength="19"
                                       placeholder="•••• •••• •••• ••••" required="required" type="tel" class="form-control"/>
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="cardholderName">Código de segurnaça</label>
                                <input id="form-checkout__securityCode" name="securityCode"required="required"  type="tel" class="form-control"/>
                            </div>
                            <div id="issuerInput" class="form-group col-sm-12">
                                <select id="form-checkout__issuer" name="issuer" required="required" class="form-control"></select>
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="installments">Selecione o número de parcelas</label>
                                <select id="form-checkout__installments" name="installments" required="required" type="text" class="form-control"></select>
                            </div>
                            <div class="form-group col-sm-12">
                                <input type="hidden" name="issuerId" id="issuerId" />
                                <button id="form-checkout__submit" type="button" class="btn btn-primary btn-block"><i class="fa fa-credit-card"></i>  PAGAR</button>
                                <span style="font-size: 11px;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="17" height="16" viewBox="0 0 17 16"><g fill="none" fill-opacity=".45" fill-rule="evenodd"><g fill="#000" fill-rule="nonzero"><g><g><g><path d="M8.062.599l.383.317c1.92 1.59 3.81 2.374 5.68 2.374h.6v.6c0 5.633-2.165 9.242-6.473 10.679l-.19.063-.19-.063C3.564 13.132 1.4 9.523 1.4 3.89v-.6h.6c1.87 0 3.76-.783 5.68-2.374l.383-.317zm0 1.548c-1.8 1.4-3.62 2.179-5.455 2.32.135 4.725 1.947 7.648 5.455 8.898 3.508-1.25 5.32-4.173 5.455-8.898-1.835-.141-3.656-.92-5.455-2.32zm2.286 2.895l.896.798-4.02 4.513-2.472-2.377.831-.865 1.574 1.513 3.191-3.582z" transform="translate(-71 -504) translate(56 503) translate(15 1) translate(.5)"></path></g></g></g></g></g></svg>
                                    Você está pagando com segurança pelo Mercado Pago
                                </span>
                                <br/>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Result -->
    <section class="shopping-cart dark" style="background: #ff4957;">
        <div class="container container__result">
            <div class="content">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="items product info product-details" style="background: #ff4957;">
                            <div class="row justify-content-md-center">
                                <div class="col-md-4 product-detail">
                                    <div class="product-info">
                                        <div id="fail-response" style="background: #ff4957">
                                            <br/>
                                            <img src="<?= $assets ?>images/cancelamento.gif"  style="width: 100%;" />
                                            <p class="text-center" style="color: #FFFFFF;">Ocorreu um erro...</p>
                                            <p id="error-message" class="text-center" style="color: #FFFFFF;font-size: 20px;font-weight: bold;padding: 15px;"></p>
                                            <br/>
                                            <button id="nova-tentativa" type="button" class="btn btn-primary btn-block" style="padding: 30px;background: #495057;border-color: #495057;">FAZER UMA NOVA TENTATIVA</button>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<script type="text/javascript">

    const mercadopago = new MercadoPago('<?php echo $configMercadoPago->account_token_public;?>');
    var cardForm = null;

    jQuery(document).ready(function() {

        jQuery.extend(jQuery.validator.messages, {
            required: "Campo é obrigatório.",
            email: "Por favor insira um endereço de e-mail válido.",
            date: "Please enter a valid date.",
            number: "Por favor insira um número válido.",
            digits: "Por favor insira um número válido.",
            maxlength: jQuery.validator.format("Não insira mais de {0} caracteres."),
            minlength: jQuery.validator.format("Insira pelo menos {0} caracteres."),
        });

        loadCardFormMercadoPago();

        $('#form-checkout__submit').click(function (){
            processar();
        });

        <?php if ($cobrancaFatura->errorProcessamento){?>
            $('.container__payment').fadeOut(0);
            setTimeout(() => { $('.container__result').show(0).fadeIn(); }, 100);

            document.getElementById("error-message").innerText = '<?php echo $cobrancaFatura->errorProcessamento;?>'
            document.getElementById("fail-response").style.display = "block";
        <?php } ?>

        $('#nova-tentativa').click(function(){
            setTimeout(() => {
                $('.container__result').fadeOut(0);
                $('.container__payment').show(0).fadeIn();
            }, 100);
        });

        $('#form-checkout__cardNumber').keyup(function (event) {
            mascaraCartaoCredito( this, mcc );
        });

        $('#form-checkout__cardholderName').on('keyup', (ev) => {
            $('#form-checkout__cardholderName').val($('#form-checkout__cardholderName').val().toUpperCase());
        });

        $('#form-checkout__cardNumber').keypress(function (event) {
            recriarFormularioMercadoPago();
        });
    });

    function recriarFormularioMercadoPago() {
        $('#form-checkout__issuer').empty();
        $('#form-checkout__installments').empty();
        loadCardFormMercadoPago();
    }

    function processar() {

        if ($('#form-checkout').valid()) {

            $.blockUI({
                overlayCSS: { backgroundColor: '#007bff' } ,
                message: '<h3><img src="<?php echo base_url() ?>assets/images/busy.gif" /></h3>',
                css: {width: '70%', left: '15%'}
            });

            cardForm.createCardToken().then(function () {
                $('#form-checkout__submit').prop('disabled', true);
                $("input[name='issuerId']").val(cardForm.getCardFormData().issuerId);
                $('#form-checkout').submit();
            }, function (errors) {
                $.unblockUI();

                let erros = '';
                $(errors).each(function (indexd, error) {
                    erros += error.code + ' - ' + error.message + '<br/>';
                });

                $.blockUI({
                    message: erros,
                    fadeIn: 700,
                    fadeOut: 700,
                    timeout: 10000,
                    showOverlay: false,
                    centerY: false,
                    css: {
                        width: '350px',
                        top: '10px',
                        left: '',
                        right: '10px',
                        border: 'none',
                        padding: '5px',
                        backgroundColor: '#dc3545',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .6,
                        color: '#fff'
                    }
                });

            });
        }
    }
    function loadCardFormMercadoPago() {

        if (cardForm != null ) cardForm.unmount();

        cardForm = mercadopago.cardForm({
            amount: "<?php echo $fatura->valorpagar;?>",
            autoMount: true,
            form: {
                id: "form-checkout",
                cardholderName: {
                    id: "form-checkout__cardholderName",
                    placeholder: "COMO GRAVADO NO CARTÃO",
                },
                cardholderEmail: {
                    id: "form-checkout__cardholderEmail",
                    placeholder: "E-MAIL",
                },
                cardNumber: {
                    id: "form-checkout__cardNumber",
                    placeholder: "•••• •••• •••• ••••",
                },
                cardExpirationMonth: {
                    id: "form-checkout__cardExpirationMonth",
                    placeholder: "MM",
                },
                cardExpirationYear: {
                    id: "form-checkout__cardExpirationYear",
                    placeholder: "YY",
                },
                securityCode: {
                    id: "form-checkout__securityCode",
                    placeholder: "CVV",
                },
                installments: {
                    id: "form-checkout__installments",
                    placeholder: "PARCELAS",
                },
                identificationType: {
                    id: "form-checkout__identificationType",
                },
                identificationNumber: {
                    id: "form-checkout__identificationNumber",
                    placeholder: "Número da Identificação",
                },
                issuer: {
                    id: "form-checkout__issuer",
                    placeholder: "Bandeira",
                },
            },
            callbacks: {
                onFormMounted: error => {
                    if (error)
                        return console.warn("Form Mounted handling error: ", error);
                    console.log("Form mounted");
                },
                onSubmit: event => {},
                onFetching: (resource) => {
                    console.log("Fetching resource: ", resource);
                    const payButton = document.getElementById("form-checkout__submit");
                    payButton.setAttribute('disabled', true);
                    return () => {
                        payButton.removeAttribute("disabled");
                    };
                },
            },
        });
    }

    function mascaraCartaoCredito(o,f){
        v_obj=o
        v_fun=f
        setTimeout("execmascaracartaocredito()",1)
    }

    function execmascaracartaocredito(){
        v_obj.value=v_fun(v_obj.value)
    }

    function mcc(v){
        v=v.replace(/\D/g,"");
        v=v.replace(/^(\d{4})(\d)/g,"$1 $2");
        v=v.replace(/^(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3");
        v=v.replace(/^(\d{4})\s(\d{4})\s(\d{4})(\d)/g,"$1 $2 $3 $4");
        return v;
    }

</script>
</body>
</html>