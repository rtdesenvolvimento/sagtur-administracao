<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php echo $Settings->site_name;?>">
    <meta name="author" content="Resultec Sistemas Digitais || Desenvolvedor || SAGtur Sistema para Agência de Turismo"/>
    <title><?php echo $Settings->site_name;?> || PAGAMENTO CARTÃO DE CRÉDITO  - ERROR </title>

    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!--Cabecalho-->
    <meta name="description" content="<?php echo $Settings->site_name;?> || PAGAMENTO CARTÃO DE CRÉDITO - ERROR" >
    <meta name="keywords" content="">
    <meta name="application-name" content="SAGTur Sistema para Agência de Turismo">
    <meta name="title" content="<?php echo $Settings->site_name;?> || PAGAMENTO CARTÃO DE CRÉDITO  - ERROR">
    <meta name="robots" content="all" />
    <meta name="language" content="br" />
    <meta name="robots" content="follow" />

    <meta property="og:type" content="website" />
    <meta property="og:locale" content="Pt_BR" />
    <meta property="og:image" content="<?= base_url(); ?>assets/uploads/logos/<?php echo $Settings->logo2;?>" />
    <meta property="og:title" content="<?php echo $Settings->site_name;?>"  />
    <meta property="og:description" content="PAGAMENTO CARTÃO DE CRÉDITO  - ERROR || <?php echo $fatura->product_name;?>" />
    <meta property="og:site_name" content="<?php echo $Settings->site_name;?> || PAGAMENTO CARTÃO DE CRÉDITO  - ERROR" />
    <meta property="og:image:alt" content="<?php echo $Settings->site_name;?> || PAGAMENTO CARTÃO DE CRÉDITO  - ERROR" />
    <meta property="og:url" content="<?=current_url();?>" />
    <meta property="og:image:width" content="800">
    <meta property="og:image:height" content="600">

    <?php if ($this->Settings->head_code){?>
        <?=$this->Settings->head_code;?>
    <?php } ?>

    <!-- Favicons-->
    <link rel="shortcut icon" href="<?php echo base_url() ?>/assets/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url() ?>/assets/images/favicon.ico">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url() ?>/assets/images/favicon.ico">

    <!-- GOOGLE WEB FONT -->
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:400,500,600" rel="stylesheet">

    <!-- BASE CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/menu.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/appcompra/css/vendors.css" rel="stylesheet">

    <!-- YOUR CUSTOM CSS -->
    <link href="<?php echo base_url() ?>assets/appcompra/css/custom.css" rel="stylesheet">

    <!-- MODERNIZR MENU -->
    <script src="<?php echo base_url() ?>assets/appcompra/js/modernizr.js"></script>

    <style>

        body{
            background: #ff4957;
        }

        .budget_slider_info {
            margin-bottom: 20px;
            padding: 20px 30px 15px 30px;
            font-weight: bold;
            width: 100%;
            background: #ff4957;
            text-align: center;
            color: #ffffff;
            border-radius: 10px;
        }

        .row-height {
            height: 0vh;
        }

        a.botoes, .botoes {
            color: #fff;
            outline: none;
            cursor: pointer;
            display: inline-block;
            text-decoration: none;
            padding: 25px 50px;
            font-weight: 600;
            text-align: center;
            line-height: 1;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            -webkit-transition: all 0.3s ease-in-out;
            -ms-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-border-radius: 3px;
            -moz-border-radius: 3px;
            border-radius: 3px;
            width: 100%;
            margin-top: 10px;
            background: #30a113;
            border: 1px solid #2e2e2e;
            font-size: 15px;
        }
    </style>

</head>

<body>

<?php if ($this->Settings->body_code){?>
    <?=$this->Settings->body_code;?>
<?php } ?>

<div class="container-fluid full-height">
    <div class="row row-height">
        <div class="col-lg-12 content-right" id="start">
            <div id="wizard_container">
                <div id="middle-wizard">
                    <div class="step" style="text-align: center;">
                        <h2 class="main_question" style="color: #1e7e34">
                            <img src="<?= $assets ?>images/cancelamento.gif"  style="width: auto;" width="260px" height="260px" />
                        </h2>
                    </div>
                </div>
                <div class="budget_slider_info">
                    OCORREU UM PROBLEMA NO PROCESSAMENTO DO PAGAMENTO COM CARTÃO DE CRÉDITO. <br/>VERIFIQUE COM SUA OPERADORA DE CARTÃO.<br/> <span style="font-size: 23px">VENDA CANCELADA.<br/> PAGAMENTO NÃO CONFIRMADO!</span>
                </div>
                <?php if (!empty($venda)) {?>
                    <?php if ($Settings->own_domain){?>
                        <a href="<?php echo base_url().'/carrinho/'.$fatura->programacaoId.'/'.$venda->biller_id; ?>"
                           class="botoes" style="background: #495057;">FAZER UMA NOVA RESERVA
                        </a>
                    <?php } else { ?>
                        <a href="<?php echo base_url().$this->session->userdata('cnpjempresa').'/carrinho/'.$fatura->programacaoId.'/'.$venda->biller_id; ?>"
                           class="botoes" style="background: #495057;">FAZER UMA NOVA RESERVA
                        </a>
                    <?php } ?>
                <?php } ?>
                <a href="<?=$this->Settings->url_site_domain.'/'.$venda->biller_id?>" class="btn_1" style=";background: #0B2C5F;width: 100%;margin-top: 10px;">VOLTAR PARA O SITE</a>
                <p><br/><br/></p>
            </div>
        </div>
    </div>
</div>
<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url() ?>assets/appcompra/js/jquery-3.2.1.min.js" type="text/javascript"></script>

</body>
</html>