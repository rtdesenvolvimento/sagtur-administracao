<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('editar_fatura'); ?></h4>
        </div>
        <?php echo form_open_multipart("faturas/editarFatura/".$fatura->id, array('data-toggle' => 'validator', 'role' => 'form')); ?>
            <div class="modal-body">
                <input type="hidden" value="<?php echo $fatura->id?>" name="id">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-8">
                            <div class="form-group">
                                <?= lang("cliente_conta", "pessoa"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-user"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <?php
                                    echo form_input('pessoa', (isset($_POST['pessoa']) ? $_POST['pessoa'] :  $fatura->pessoa), 'id="pessoa" readonly data-placeholder="' . lang("select") . ' ' . lang("pessoa") . '" required="required" class="form-control input-tip" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($fatura->tipooperacao == 'CREDITO') {?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang('receita', 'receita'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <select class="form-control tip" name="receita" id="receita" required="required">
                                            <option value=""><?php echo lang('select').' '.lang('receita');?></option>
                                            <?php
                                            foreach ($receitas as $receita) {?>
                                                <optgroup label="<?php echo $receita->name;?>">
                                                    <?php
                                                    $receitasFilhas = $this->site->getReceitaByReceitaSuperiorId($receita->id);
                                                    foreach ($receitasFilhas as $item) {?>
                                                        <?php if ($fatura->receita == $item->id ) {?>
                                                            <option  selected="selected" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } ?>
                                                    <?php }?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php } else {?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang('despesa', 'despesa'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-list"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <select class="form-control tip" name="despesa" id="despesa" required="required">
                                            <option value=""><?php echo lang('select').' '.lang('despesa');?></option>
                                            <?php
                                            foreach ($despesas as $despesa) {?>
                                                <optgroup label="<?php echo $despesa->name;?>">
                                                    <?php
                                                    $despesasFilhas = $this->site->getReceitaByDespesaSuperiorId($despesa->id);
                                                    foreach ($despesasFilhas as $item) {?>
                                                        <?php if ($fatura->despesa  == $item->id ) {?>
                                                            <option  selected="selected" value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } else {?>
                                                            <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                        <?php } ?>
                                                    <?php }?>
                                                </optgroup>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("vencimento", "dtvencimento"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-calendar"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="date" name="dtvencimento" value="<?php echo $fatura->dtvencimento;?>" class="form-control tip" id="dtvencimento" required="required" data-original-title="Data do primeiro vencimento" title="Data do primeiro vencimento">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('tipo_cobranca_financeiro', 'tipo_cobranca'); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-barcode"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <?php
                                    $cbTipoDeCobranca[''] = '';
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('tipocobranca', $cbTipoDeCobranca, $fatura->tipoCobranca, 'id="tipocobranca" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca_financeiro") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang('conta_destino', 'conta_destino'); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <?php
                                    $cbContas[''] = lang('select').' '.lang('conta_destino');
                                    foreach ($movimentadores as $movimentador) {
                                        $cbContas[$movimentador->id] = $movimentador->name;
                                    } ?>
                                    <?= form_dropdown('movimentador', $cbContas,  $fatura->movimentador, 'class="form-control" required="required" readonly id="movimentador"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("valor_vencimento", "valorfatura"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="text" name="valorfatura" id="valorfatura" value="<?php echo $fatura->valorfatura;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money" readonly required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="display: none;">
                            <div class="form-group">
                                <?= lang("acrescimo", "acrescimo"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="text" name="totalAcrescimo" id="totalAcrescimo" value="<?php echo $fatura->totalAcrescimo;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3" style="display: none;">
                            <div class="form-group">
                                <?= lang("desconto", "desconto"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="text" name="totalDesconto" id="totalDesconto" value="<?php echo $fatura->totaldesconto;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("valor_pagar", "valor_pagar"); ?>
                                <div class="input-group">
                                    <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                        <i class="fa fa-usd"  style="font-size: 1.2em;"></i>
                                    </div>
                                    <input type="text" name="valorpagar" id="valorpagar" value="<?php echo $fatura->valorpagar;?>" style="padding: 5px;" class="pa form-control kb-pad mask_money" readonly required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("note", "note"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $fatura->note), 'class="form-control" id="note"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <?php echo form_submit('editarFatura', lang('editar_fatura'), 'class="btn btn-primary"'); ?>
                </div>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">

    var mask = {
        money: function() {
            var el = this
                ,exec = function(v) {
                v = v.replace(/\D/g,"");
                v = new String(Number(v));
                var len = v.length;
                if (1 == len)
                    v = v.replace(/(\d)/,"0.0$1");
                else if (2 == len)
                    v = v.replace(/(\d)/,"0.$1");
                else if (len > 2) {
                    v = v.replace(/(\d{2})$/,'.$1');
                }
                return v;
            };

            setTimeout(function(){
                el.value = exec(el.value);
            },1);
        }
    }

    $(document).ready(function () {
        $('#movimentador').attr('readonly', true);

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;

        $('#totalAcrescimo').keyup(function (event) {
            calcularValorPagar();
        });

        $('#totalDesconto').keyup(function (event) {
            calcularValorPagar();
        });

        $("#tipocobranca").select2().on("change", function(e) {
            $.ajax({
                type: "GET",
                url: site.base_url + "faturas/buscaTipoCobrancaById/"+$(this).val(),
                data: {},
                dataType: 'json',
                success: function (objeto) {
                    let conta = parseInt(objeto.conta);
                    $('#movimentador').attr('readonly', false);

                    if (conta > 0) {
                        $('#movimentador').val(conta).trigger('change');
                        $('#movimentador').attr('readonly', true);
                    }
                }
            });
        });

        $('#pessoa').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
    });

    function calcularValorPagar() {
        let valor = $('#valorfatura').val();
        let totalAcrescimo = $('#totalAcrescimo').val();
        let totalDesconto = $('#totalDesconto').val();

        if (valor !== '') valor = parseFloat(valor);
        else valor = 0;

        if (totalAcrescimo !== '') totalAcrescimo = parseFloat(totalAcrescimo);
        else totalAcrescimo = 0;

        if (totalDesconto !== '') totalDesconto = parseFloat(totalDesconto);
        else totalDesconto = 0;

        $('#valorpagar').val(valor + totalAcrescimo - totalDesconto);

    }

</script>
