<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_payment'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo form_open_multipart("faturas/adicionarPagamento/" . $fatura->id, $attrib); ?>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("reference_no", "reference_no"); ?>
                        <?= form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : $payment_ref), 'class="form-control tip" disabled id="reference_no"'); ?>
                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("date", "date"); ?>
                        <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required"'); ?>
                    </div>
                </div>

                <!--
                <?php if ($Owner || $Admin) { ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?= lang("date", "date"); ?>
                            <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required"'); ?>
                        </div>
                    </div>
                <?php } ?>
                !-->
                <input type="hidden" value="<?php echo $fatura->id; ?>" name="parcela_id"/>
            </div>
            <div class="clearfix"></div>
            <div id="payments">
                <div class="well well-sm well_1">
                    <div class="col-md-12">
                        <div class="row">

                            <div class="col-sm-3">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("valor_vencimento", "valor"); ?>
                                        <input name="valor" style="padding: 5px;" type="text" readonly
                                               value="<?= $this->sma->formatMoney($fatura->valorfatura); ?>"
                                               class="pa form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("pago_financeiro", "valorpago"); ?>
                                        <input name="valorpago" style="padding: 5px;" type="text" readonly
                                               value="<?= $this->sma->formatMoney($fatura->valorpago); ?>"
                                               class="pa form-control"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("acrescimo", "acrescimo"); ?>
                                        <input name="acrescimo" style="padding: 5px;" type="text" id="acrescimo" readonly
                                               value="<?= $this->sma->formatMoney($fatura->acrescimo); ?>"
                                               class="pa form-control kb-pad amount mask_money"/>
                                    </div>
                                </div>
                            </div>
                            <!--
                            <div class="col-sm-3">
                                <div class="payment" style="display: none;">
                                    <div class="form-group">
                                        <?= lang("desconto", "desconto"); ?>
                                        <input name="desconto" style="padding: 5px;" type="text" id="desconto"
                                               value="<?= $this->sma->formatDecimal($fatura->desconto); ?>"
                                               class="pa form-control kb-pad amount mask_money"/>
                                    </div>
                                </div>
                            </div>
                            !-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <?= lang('tipo_cobranca_financeiro', 'tipo_cobranca'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-barcode"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?php
                                        $cbTipoDeCobranca[''] = '';
                                        foreach ($tiposCobranca as $tipoCobranca) {
                                            $cbTipoDeCobranca[$tipoCobranca->id] = $tipoCobranca->name;
                                        }
                                        echo form_dropdown('tipoCobrancaPagamento', $cbTipoDeCobranca, $fatura->tipoCobranca, 'id="tipoCobrancaPagamento" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("tipo_cobranca_financeiro") . '" required="required" style="width:100%;" ');
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang('conta_destino', 'movimentador'); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                            <i class="fa fa-university"  style="font-size: 1.2em;"></i>
                                        </div>
                                        <?php
                                        $cbContas[''] = lang('select') . ' ' . lang('conta_destino');
                                        foreach ($movimentadores as $movimentador) {
                                            $cbContas[$movimentador->id] = $movimentador->name;
                                        }
                                        ?>
                                        <?= form_dropdown('movimentador', $cbContas, $fatura->movimentador, 'class="form-control" required="required" '.$readonly.' id="movimentador"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="payment">
                                    <div class="form-group">
                                        <?= lang("valor_pagamento", "amount_1"); ?>
                                        <input name="amount-paid" style="padding: 5px;" type="text" id="amount_1"
                                               value="<?= $this->sma->formatDecimal($fatura->valorpagar + $fatura->acrescimo); ?>"
                                               class="pa form-control kb-pad amount mask_money" required="required"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang("forma_pagamento", "formapagamento"); ?>
                                    <?php
                                    $cbFormaPagamento[''] = lang('select') . ' ' . lang('forma_pagamento');
                                    foreach ($formaspagamento as $formapagamento) {
                                        $cbFormaPagamento[$formapagamento->id] = $formapagamento->name;
                                    }
                                    ?>
                                    <?= form_dropdown('formapagamento', $cbFormaPagamento, $formaPagamentoId, 'class="form-control" required="required" '.$readonly.' id="formapagamento"'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php if ($gift_card->card_no){?>
                            <div class="form-group gc">
                                <?= lang("gift_card_no", "gift_card_no"); ?>
                                <input name="gift_card_no" type="text" id="gift_card_no" value="<?=$gift_card->card_no;?>"
                                       readonly="readonly" class="pa form-control kb-pad"/>
                                <div id="gc_details" card_no="<?=$gift_card->card_no;?>" style="margin-top: 10px;font-weight: 600;width: 50%;">
                                    <?php if ($gift_card->expiry && $gift_card->expiry < date('Y-m-d')) { ?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_expired') ?> <?=$gift_card->card_no;?></b>
                                        </div>
                                    <?php } elseif($gift_card->status == 'cancel') {?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_cancel') ?> <?=$gift_card->card_no;?></b>
                                        </div>
                                    <?php } else if ($gift_card->balance > 0) { ?>
                                        <div class="alert alert-success">
                                            <b><?= lang('card_not_used'); ?> <?=$gift_card->card_no;?></b><hr/>
                                            <?= lang('value').': '.$this->sma->formatMoney($gift_card->value) ?><br/>
                                            <?= lang('saldo').': '.$this->sma->formatMoney($gift_card->balance); ?><br/>
                                            <?= lang('expiry').': '.$this->sma->hrsd($gift_card->expiry); ?><br/>
                                            <?php if($gift_card->note_gift){?>
                                                <?= lang('note').': '. $gift_card->note_gift; ?><br/>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_is_used'); ?> <?=$gift_card->card_no;?></b>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } elseif ($refund_card->card_no) {?>
                            <div class="form-group gc">
                                <?= lang("refund_card_no", "refund_card_no"); ?>
                                <input name="gift_card_no" type="text" id="gift_card_no" value="<?=$refund_card->card_no;?>"
                                       readonly="readonly" class="pa form-control kb-pad"/>
                                <div id="gc_details" card_no="<?=$refund_card->card_no;?>" style="margin-top: 10px;font-weight: 600;width: 50%;">
                                    <?php if ($refund_card->expiry && $refund_card->expiry < date('Y-m-d')) { ?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_expired') ?> <?=$refund_card->card_no;?></b>
                                        </div>
                                    <?php } elseif($refund_card->status == 'cancel') {?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_cancel') ?> <?=$refund_card->card_no;?></b>
                                        </div>
                                    <?php } else if ($refund_card->balance > 0) { ?>
                                        <div class="alert alert-success">
                                            <b><?= lang('card_refund_not_used'); ?> <?=$refund_card->card_no;?></b><hr/>
                                            <?= lang('value').': '.$this->sma->formatMoney($refund_card->value) ?><br/>
                                            <?= lang('saldo').': '.$this->sma->formatMoney($refund_card->balance); ?><br/>
                                            <?= lang('expiry').': '.$this->sma->hrsd($refund_card->expiry); ?><br/>
                                            <?php if($refund_card->note_gift){?>
                                                <?= lang('note').': '. $refund_card->note_gift; ?><br/>
                                            <?php } ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="alert alert-danger">
                                            <b><?= lang('card_is_used'); ?> <?=$refund_card->card_no;?></b>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php } else if (!empty($gifts_card)) { ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <?= lang("gift_card_no", "gift_card_no"); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;">
                                                <i class="fa fa-credit-card"  style="font-size: 1.2em;"></i>
                                            </div>
                                            <input name="gift_card_no" type="text" id="gift_card_no" value="" readonly="readonly" class="pa form-control kb-pad"/>
                                            <div class="input-group-addon no-print" style="padding: 2px 8px;cursor: pointer;" id="limpar_gift_card_no">
                                                <i class="fa fa-trash-o"  style="font-size: 1.2em;"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($gifts_card as $gift_card){?>
                                    <div class="col-sm-6">
                                        <div class="gc_details" card_no="<?=$gift_card->card_no;?>" style="margin-top: 10px;font-weight: 600;cursor: pointer;">
                                            <?php if ($gift_card->expiry && $gift_card->expiry < date('Y-m-d')) { ?>
                                                <div class="alert alert-danger">
                                                    <b><?= lang('card_expired') ?> <?=$gift_card->card_no;?></b>
                                                </div>
                                            <?php } elseif($gift_card->status == 'cancel') {?>
                                                <div class="alert alert-danger">
                                                    <b><?= lang('card_cancel') ?> <?=$gift_card->card_no;?></b>
                                                </div>
                                            <?php } else if ($gift_card->balance > 0) { ?>
                                                <div class="alert alert-success card_not_used" id="card_no<?=$gift_card->card_no;?>">
                                                    <b><?= lang('card_not_used'); ?> <?=$gift_card->card_no;?></b><hr/>
                                                    <?= lang('value').': '.$this->sma->formatMoney($gift_card->value) ?><br/>
                                                    <?= lang('saldo').': '.$this->sma->formatMoney($gift_card->balance); ?><br/>
                                                    <?= lang('expiry').': '.$this->sma->hrsd($gift_card->expiry); ?><br/>
                                                    <?= lang('note').': '. $gift_card->note; ?><br/>
                                                </div>
                                            <?php } else { ?>
                                                <div class="alert alert-danger">
                                                    <b><?= lang('card_is_used'); ?> <?=$gift_card->card_no;?></b>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } else { ?>
                            <div class="form-group gc" style="display:none;">
                                <?= lang("gift_card_no", "gift_card_no"); ?>
                                <input name="gift_card_no" type="text" id="gift_card_no" class="pa form-control kb-pad"/>
                                <div id="gc_details"></div>
                            </div>
                        <?php }?>
                        <div class="pcc_1" style="display:none;">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="pcc_no" type="text" id="pcc_no_1" class="form-control"
                                               placeholder="<?= lang('cc_no') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <input name="pcc_holder" type="text" id="pcc_holder_1" class="form-control"
                                               placeholder="<?= lang('cc_holder') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type"
                                                placeholder="<?= lang('card_type') ?>">
                                            <option value="Visa"><?= lang("Visa"); ?></option>
                                            <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                            <option value="Amex"><?= lang("Amex"); ?></option>
                                            <option value="Discover"><?= lang("Discover"); ?></option>
                                        </select>
                                        <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <input name="pcc_month" type="text" id="pcc_month_1" class="form-control"
                                               placeholder="<?= lang('month') ?>"/>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">

                                        <input name="pcc_year" type="text" id="pcc_year_1" class="form-control"
                                               placeholder="<?= lang('year') ?>"/>
                                    </div>
                                </div>
                                <!--<div class="col-md-3">
                                                        <div class="form-group">
                                                            <input name="pcc_ccv" type="text" id="pcc_cvv2_1" class="form-control" placeholder="<?= lang('cvv2') ?>" />
                                                        </div>
                                                    </div>-->
                            </div>
                        </div>
                        <div class="pcheque_1" style="display:none;">
                            <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                <input name="cheque_no" type="text" id="cheque_no_1" class="form-control cheque_no"/>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_payment', lang('add_payment'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        var mask = {
            money: function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"0.0$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"0.$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'.$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        var mask_integer = {
            money : function() {
                var el = this
                    ,exec = function(v) {
                    v = v.replace(/\D/g,"");
                    v = new String(Number(v));
                    var len = v.length;
                    if (1 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (2 == len)
                        v = v.replace(/(\d)/,"$1");
                    else if (len > 2) {
                        v = v.replace(/(\d{2})$/,'$1');
                    }
                    return v;
                };

                setTimeout(function(){
                    el.value = exec(el.value);
                },1);
            }
        }

        $(function(){
            $('.mask_money').bind('keypress',mask.money);
            $('.mask_money').click(function(){$(this).select();});
        });

        $(function(){
            $('.mask_integer').bind('keypress',mask_integer.money);
            $('.mask_integer').click(function(){$(this).select();});
        });

        $(document).on('change', '#gift_card_no', function () {
            var cn = $(this).val() ? $(this).val() : '';
            if (cn != '') {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "gifts/validate_gift_card/" + cn,
                    dataType: "json",
                    success: function (data) {
                        if (data === false) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('incorrect_gift_card')?>');
                        } else if (data.customer_id !== null && data.customer_id != <?=$fatura->pessoa;?>) {
                            $('#gift_card_no').parent('.form-group').addClass('has-error');
                            bootbox.alert('<?=lang('gift_card_not_for_customer')?>');

                        } else {
                            var due = <?=$fatura->valorpagar?>;
                            if (due > data.balance) {
                                $('#amount_1').val(formatDecimal(data.balance));
                            }
                            $('#gc_details').html('Nº da Carta de Crédito: <span style="max-width:60%;float:right;">' + data.card_no + '</span><br>Valor: <span style="max-width:60%;float:right;">' + currencyFormat(data.value) + '</span><br>Saldo: <span style="max-width:60%;float:right;">' + currencyFormat(data.balance) + '</span>');
                            $('#gift_card_no').parent('.form-group').removeClass('has-error');
                        }
                    }
                });
            }
        });
        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').show();
                $('#amount_1').focus();
            } else if (p_val == 'CC') {
                $('.pcheque_1').hide();
                $('.pcash_1').hide();
                $('.pcc_1').show();
                $('#pcc_no_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcc_1').hide();
                $('.pcash_1').hide();
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').hide();
            }
            if (p_val == 'gift_card') {
                $('.gc').show();
                $('#gift_card_no').focus();
            } else {
                $('.gc').hide();
            }
        });

        $('#pcc_no_1').change(function (e) {
            var pcc_no = $(this).val();
            localStorage.setItem('pcc_no_1', pcc_no);
            var CardType = null;
            var ccn1 = pcc_no.charAt(0);
            if (ccn1 == 4)
                CardType = 'Visa';
            else if (ccn1 == 5)
                CardType = 'MasterCard';
            else if (ccn1 == 3)
                CardType = 'Amex';
            else if (ccn1 == 6)
                CardType = 'Discover';
            else
                CardType = 'Visa';

            $('#pcc_type_1').select2("val", CardType);
        });

        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());

        $('.gc_details').click(function (event){
            let card_no = $(this).attr('card_no');

            $('.card_not_used').css('border-color', '#d6e9c6');
            $('#card_no'+card_no).css('border-color', '#3c763d');

            $('#gift_card_no').val(card_no);
            $('#gift_card_no').change();
        });

        $('#limpar_gift_card_no').click(function (event){
            $('.card_not_used').css('border-color', '#d6e9c6');
            $('#gift_card_no').val('');
        })
    });
</script>
