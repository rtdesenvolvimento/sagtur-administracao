
<style type="text/css" media="screen">
    #CobData td:nth-child(1) {
        display: none;
    }


    #CobData td:nth-child(2) {
        text-align: center;
    }

    #CobData td:nth-child(3) {
        text-align: center;
    }

    #CobData td:nth-child(4) {
        text-align: center;
    }

    #CobData td:nth-child(5) {
        text-align: center;
    }


    #CobData td:nth-child(6) {
        display: none;
    }

    #HysData td:nth-child(1) {
        display: none;
    }

    #HysData td:nth-child(2) {
        text-align: center;
    }

    #HysData td:nth-child(3) {
        text-align: center;
    }

    #HysData td:nth-child(4) {
        text-align: center;
    }

    #HysData td:nth-child(5) {
        text-align: center;
    }

</style>

<script>
    $(document).ready(function () {

        function row_status_financeiro(x,  alignment, aaData) {

            let diasAtraso = aaData[5];

            if(x === 'ABERTA') {
                if (diasAtraso < 0) {
                    return '<div class="text-center"><span class="label label-danger">VENCIDA</span></div>';
                } else {
                    return '<div class="text-center"><span class="label label-warning">PENDENTE</span></div>';
                }
            } else if (x === 'QUITADA') {
                return '<div class="text-center"><span class="label label-success">PAGO</span></div>';
            }
        }

        var cTable = $('#CobData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('account/getNextCharges') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                {"mRender": fsd},
                {"mRender": currencyFormat},
                null,
                {"mRender": row_status_financeiro},
                null,
                {"bSortable": false}
            ]
        }).dtFilter([], "footer");


        var cTableh = $('#HysData').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= site_url('account/getBillingHistory') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                null,
                {"mRender": fsd},
                {"mRender": currencyFormat},
                null,
                {"mRender": row_status_financeiro},
                {"bSortable": false}
            ]
        }).dtFilter([], "footer");

    });
</script>

<div class="row">
    <div class="col-lg-2">
        <div class="col-sm-12 text-center">
            <div class="max-width:200px; margin: 0 auto;">
                <img src="<?= base_url(); ?>assets/uploads/logos/sagtur.png" alt="<?php echo $Settings->site_name ;?>">
                <h4>Plano Contratado</h4>
                <p><?= $customer->customer_group_name;?></p>
                <p><?= $this->sma->formatMoney($group->price);?>/mês</p>
            </div>
        </div>
    </div>
    <div class="col-lg-10">
        <ul id="myTab" class="nav nav-tabs" style="text-align: center">
            <li class=""><a href="#next_changes" class="tab-grey"><i class="fa fa-usd" style="font-size: 20px;"></i><br/><?= lang('next_changes') ?></a></li>
            <li class="" id="tbFinanceiro"><a href="#billing_history" class="tab-grey"><i class="fa fa-money" style="font-size: 20px;"></i><br/><?= lang('billing_history') ?></a></li>
            <li class="" id="tbTermosCancelamento"><a href="#termos_cancelamento" class="tab-grey"><i class="fa fa-tasks" style="font-size: 20px;"></i><br/><?= lang('termos_cancelamento') ?></a></li>
            <li class="" id="tbDadosCadastradis"><a href="#seus_dados" class="tab-grey"><i class="fa fa-edit" style="font-size: 20px;"></i><br/><?= lang('my_data') ?></a></li>
        </ul>
        <div class="tab-content">
            <div id="next_changes" class="tab-pane fade in">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('next_changes_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="CobData" cellpadding="0" cellspacing="0" border="0"
                                           class="table table-bordered table-condensed table-hover table-striped">
                                        <thead>
                                        <tr class="primary">
                                            <th style="text-align: center;width: 1%;display: none;" ></th>
                                            <th style="text-align: center;"><?= lang("due_date"); ?></th>
                                            <th style="text-align: center;"><?= lang("payment_amount"); ?></th>
                                            <th style="text-align: center;"><?= lang("plan"); ?></th>
                                            <th style="text-align: center;"><?= lang("situacao"); ?></th>
                                            <th style="display: none;"></th>
                                            <th style="text-align: center;"><?= lang("pay"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="min-width:20px !important;" class="text-center"><?= lang("actions"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="billing_history" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('billing_history_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="HysData" cellpadding="0" cellspacing="0" border="0"
                                           class="table table-bordered table-condensed table-hover table-striped">
                                        <thead>
                                        <tr class="primary">
                                            <th style="text-align: center;width: 1%;display: none;" ></th>
                                            <th style="text-align: center;"><?= lang("pay_day"); ?></th>
                                            <th style="text-align: center;"><?= lang("amount_paid"); ?></th>
                                            <th style="text-align: center;"><?= lang("plan"); ?></th>
                                            <th style="text-align: center;"><?= lang("situacao"); ?></th>
                                            <th style="text-align: center;width: 5%;"><?= lang("pay"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="min-width:20px !important;" class="text-center"><?= lang("actions"); ?></th>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="termos_cancelamento" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('termos_cancelamento_info'); ?></h2>
                        <div class="box-icon">
                        </div>
                    </div>

                    <div class="box-content">
                        <p style="margin-bottom: 25px;">
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 1ª – PAGAMENTO DA MENSALIDADE</strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO PRIMEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong> pagará mensalmente ao <strong>LICENCIANTE</strong> o valor (<strong>MENSAL</strong>) via <strong>PIX/BOLETO</strong> referente ao plano contratado, pela disponibilidade da licença do sistema <strong>SAGTUR </strong>por 30 dias.
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO SEGUNDO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong> terá acesso aos seus boletos do ano corrente em sua área do cliente para acompanhar seus boletos pagos, vencidos e boletos a vencer.
                        </p>
                        <p>
                            <strong>PARÁGRAFO TERCEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO </strong>poderá solicitar a mudança do plano a qualquer momento. O valor referente à mudança do plano ocorrerá a partir do mês consecutivo.
                        </p>
                        <p style="margin-bottom: 25px;margin-top: 25px;">
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 2ª – CANCELAMENTO DO SISTEMA</strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO PRIMEIRO:</strong>
                        </p>
                        <p>
                            Após a contratação, caso o <strong>LICENCIADO </strong>solicite o cancelamento do contrato em até 7 dias corridos, faremos a devolução da mensalidade paga e realizaremos o cancelamento do sistema.  A partir do 8º dia, caso seja solicitado o cancelamento, não haverá reembolso da mensalidade paga.
                        </p>
                        <p>
                            <strong>PARÁGRAFO SEGUNDO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO </strong>poderá <strong>CANCELAR</strong> seu plano a qualquer momento sem cobrança de taxa de cancelamento ou multas, desde que ocorra a comunicação (<strong>PRÉVIA DE NO MÁXIMO 5 DIAS)</strong> antes do vencimento do próximo boleto. Caso não o faça, será necessário pagar a mensalidade do mês corrente e solicitar o <strong>(CANCELAMENTO).</strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p style="margin-bottom: 25px;margin-top: 25px;">
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 3ª – BOLETO VENCIDO</strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO PRIMEIRO:</strong>
                        </p>
                        <p>
                            Caso o <strong>LICENCIADO </strong>esteja a mais de 6 dias com seu boleto vencido, a partir do 7º dia seu sistema será <strong>(BLOQUEADO AUTOMATICAMENTE), </strong>a mais de 13 dias vencido, a partir do 14º dia seu sistema é <strong>(CANCELADO AUTOMATICAMENTE).</strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO SEGUNDO:</strong>
                        </p>
                        <p>
                            Se ocorrer o <strong>(CANCELAMENTO AUTOMÁTICO)</strong> do sistema por falta de pagamento e sem solicitação do <strong>LICENCIADO</strong>, sua mensalidade ficará pendente em nosso sistema, e seu boleto poderá ser protestado. Para que possamos <strong>REATIVAR</strong> seu sistema será necessário pagar sua pendência junto com o valor da próxima mensalidade.
                        </p>
                        <p style="margin-bottom: 25px;margin-top: 25px;">
                            <strong style="margin-bottom: 15px;color: #df8505">CLÁUSULA 4ª – PROTESTO DE BOLETO VENCIDO</strong>
                        </p>
                        <p>
                            <strong></strong>
                        </p>
                        <p>
                            <strong>PARÁGRAFO PRIMEIRO:</strong>
                        </p>
                        <p>
                            O <strong>LICENCIADO</strong> concorda em manter todos os boletos em dia, de acordo com os termos e condições estabelecidos no contrato de serviço assinado digitalmente. Caso o <strong>LICENCIADO</strong> atrase o pagamento do boleto por mais de 15 dias, a <strong>SAGTUR</strong> se reserva ao direito de protestar o boleto através da plataforma <strong>PROTESTO 24HORAS</strong> e de incluir o nome do <strong>LICENCIADO</strong> em cadastros de inadimplência e/ou negativação. Todas as custas e taxas resultantes deste protesto serão de responsabilidade exclusiva do <strong>LICENCIADO</strong>
                        </p>
                    </div>
                </div>
            </div>
            <div id="seus_dados" class="tab-pane">
                <div class="box">
                    <div class="box-header">
                        <h2 class="blue"><i class="fa-fw fa fa-tasks"></i><?= lang('my_data_info'); ?></h2>
                        <div class="box-icon"></div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-12">
                                    <div class="alert alert-info">
                                        <button data-dismiss="alert" class="close" type="button">×</button>
                                        <a class="spwHtmlIcoInfo">&nbsp;</a>
                                        <span class="spwTextoRef">AVISO!</span>
                                        <p class="spwTextoRefMensagem">
                                            Se houver inconsistência com seus dados apresentados aqui, entre em contato conosco para ajustar o cadastro.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"><?= lang('info_data') ?></div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <?php echo lang('company', 'company'); ?>
                                                    <div class="controls">
                                                        <?php echo form_input('company', $customer->name, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?= lang("vat_no", "vat_no"); ?>
                                                    <?php echo form_input('vat_no', $customer->vat_no, 'class="form-control" disabled'); ?>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo lang('responsible', 'responsible'); ?>
                                                    <div class="controls">
                                                        <?php echo form_input('name', $customer->company, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <?php echo lang('phone', 'phone'); ?>
                                                    <div class="controls">
                                                        <input type="tel" class="form-control" disabled value="<?= $customer->phone ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?php echo lang('email', 'email'); ?>
                                                    <input type="email" class="form-control" disabled value="<?= $customer->email ?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="panel panel-warning">
                                        <div class="panel-heading"><?= lang('info_address') ?></div>
                                        <div class="panel-body" style="padding: 5px;">
                                            <div class="col-md-12">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <?= lang("address", "address"); ?>
                                                        <?php echo form_input('address', $customer->address, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("city", "city"); ?>
                                                        <?php echo form_input('city', $customer->city, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("state", "state"); ?>
                                                        <?php echo form_input('state', $customer->state, 'class="form-control" disabled'); ?>
                                                    </div>

                                                    <div class="form-group">
                                                        <?= lang("postal_code", "postal_code"); ?>
                                                        <?php echo form_input('postal_code',  $customer->postal_code, 'class="form-control" disabled'); ?>
                                                    </div>
                                                    <div class="form-group">
                                                        <?= lang("country", "country"); ?>
                                                        <?php echo form_input('country', $customer->country, 'class="form-control" disabled'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

	

