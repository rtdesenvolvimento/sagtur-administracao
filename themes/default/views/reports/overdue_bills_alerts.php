<style>

    @media print {
        #SLData td:nth-child(1) {display: none;}
        #SLData td:nth-child(2) {text-align: center;}
        #SLData td:nth-child(3) {text-align: left;width: 40%;}
        #SLData td:nth-child(4) {text-align: left;width: 40%;}
        #SLData td:nth-child(5) {text-align: left;display: none !important;}
        #SLData td:nth-child(6) {text-align: left;display: none !important;}
        #SLData td:nth-child(7) {text-align: right;}
        #SLData td:nth-child(8) {text-align: right;}
        #SLData td:nth-child(9) {text-align: right;}
        #SLData td:nth-child(10) {text-align: right;}
        #SLData td:nth-child(11) {text-align: right;}
        #SLData td:nth-child(12) {text-align: center;display: none !important;}
        #SLData td:nth-child(13) {text-align: center;display: none !important;}
        #SLData td:nth-child(14) {text-align: center;display: none !important;}
        #SLData td:nth-child(15) {text-align: center;display: none !important;}
        #SLData td:nth-child(16) {text-align: center;display: none !important;}
        #SLData td:nth-child(17) {text-align: center;display: none !important;}
        #SLData td:nth-child(18) {text-align: center;display: none !important;}
        #SLData td:nth-child(19) {text-align: center;display: none !important;}
        #SLData td:nth-child(20) {text-align: center;}
    }

    #SLData td:nth-child(1) {display: none;}
    #SLData td:nth-child(2) {text-align: center;}
    #SLData td:nth-child(3) {text-align: left;width: 40%;}
    #SLData td:nth-child(4) {text-align: left;width: 40%;}
    #SLData td:nth-child(5) {text-align: left;display: none !important;}
    #SLData td:nth-child(6) {text-align: left;display: none !important;}
    #SLData td:nth-child(7) {text-align: right;}
    #SLData td:nth-child(8) {text-align: right;}
    #SLData td:nth-child(9) {text-align: right;}
    #SLData td:nth-child(10) {text-align: right;}
    #SLData td:nth-child(11) {text-align: right;}
    #SLData td:nth-child(12) {text-align: center;display: none !important;}
    #SLData td:nth-child(13) {text-align: center;display: none !important;}
    #SLData td:nth-child(14) {text-align: center;display: none !important;}
    #SLData td:nth-child(15) {text-align: center;display: none !important;}
    #SLData td:nth-child(16) {text-align: center;display: none !important;}
    #SLData td:nth-child(17) {text-align: center;display: none !important;}
    #SLData td:nth-child(18) {text-align: center;display: none !important;}
    #SLData td:nth-child(19) {text-align: center;display: none !important;}
    #SLData td:nth-child(20) {text-align: center;}
</style>

<script>

    var struture = {
        'id' : 0,
        'dtVencimento' : 1,
        'name': 2,
        'product_name': 3,
        'receita' : 4,
        'tipoCobranca' : 5,
        'valorfatura' : 6,
        'taxas' : 7,
        'valorpago' : 8,
        'valorpagar' : 9,
        'status' : 10,
        'parcelas' : 11,
        'dtultimopagamento' : 12,
        'quantidade_dias' : 13,
        'sale_id' : 14,
        'link,' : 15,
        'tipo' : 16,
        'checkoutUrl' : 17,
        'code' : 18,
    };

    function attachDescriptionToName(x, alignment, aaData) {

        x = '<span style="font-weight: bold;font-size: 12px;line-height: 1.71;color: #428bca">'+x+'</span>';

        if ( aaData[struture.tipo] !== null ) {
            if ( aaData[struture.tipo] === 'boleto') x = '<a href="'+aaData[15]+'" target="_blank" title="Ver Boleto"> '+x+'</a>';
            else  x = '<a href="'+aaData[17]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[struture.tipo] === 'boleto')  {
            x += '<br/><small><i class="fa fa-barcode"></i> '+aaData[struture.tipoCobranca]+'</small>';
        } else {
            x += '<br/><small><i class="fa fa-money"></i> '+aaData[struture.tipoCobranca]+'</small>';
        }

        return x;
    }

    function attachDescriptionToName_bckp(x, alignment, aaData) {
        if ( aaData[struture.tipo] !== null ) {

            let codigo = aaData[struture.code];

            if ( aaData[struture.tipo] === 'boleto') x = '<a href="'+aaData[struture.link]+'" target="_blank" title="Ver Boleto"><i class="fa fa-barcode"></i> '+x+'</a><br/><small>Cod. '+codigo+'</small> ';
            else  x = '<a href="'+aaData[struture.checkoutUrl]+'" target="_blank" title="Ver Link do Cartão"><i class="fa fa-credit-card"></i> '+x+'</a>';
        }

        if (aaData[struture.tipoCobranca] !== null) x += '<br/><small><i class="fa fa-money"></i> '+aaData[struture.tipoCobranca]+'</small>';

        return x;
    }

    function currencyFormatValuePay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            return '<div class="'+alignment+'">'+formatMoney(x)+'<br/><small style="color: red;font-weight: bold;">'+aaData[struture.parcelas]+'</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }

    }

    function currencyFormatTaxas(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        if (x != null) {
            if (x > 0) return '<div class="'+alignment+'" style="color: red;font-weight: bold;">'+formatMoney(x)+'<br/>&nbsp;</small></div>';
            else return '<div class="'+alignment+'">'+formatMoney(x)+'<br/>&nbsp;</small></div>';
        } else {
            return '<div class="'+alignment+'">'+formatMoney(0)+'<br/>&nbsp;</small></div>';
        }
    }

    function currencyFormatValuePaid(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let dtUltimoPagamento = aaData[struture.dtultimopagamento] != null ? aaData[struture.dtultimopagamento] : '&nbsp;';

        if (x != null) {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>' + dtUltimoPagamento + '</small></div>';
        } else {
            return '<div class="'+alignment+'">formatMoney(0)</div>';
        }
    }

    function currencyFormatValueToPay(x, alignment, aaData) {

        if (alignment === undefined) alignment = 'text-right';

        let diferencaEmDias = aaData[struture.quantidade_dias] != null ? aaData[struture.quantidade_dias] : '&nbsp;';
        let status = aaData[struture.status];

        if (diferencaEmDias < 0 && status !== 'QUITADA') {
            if (diferencaEmDias === -1) return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small style="color: red;font-weight: bold;">' + diferencaEmDias + ' dia</small></div>';
            else return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small style="color: red;font-weight: bold;">' + diferencaEmDias + ' dias</small></div>';
        } else {
            return '<div class="' + alignment + '">' + formatMoney(x) + '<br/><small>&nbsp;</small></div>';
        }
    }

    function row_status_financeiro(x,  alignment, aaData) {

        let diasAtraso = aaData[struture.quantidade_dias];

        if(x == null) {
            return '';
        } else if(x == 'pending') {
            return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
        } else if(x == 'completed' || x == 'paid' || x == 'sent' || x == 'received' || x == 'QUITADA') {
            return '<div class="text-center"><span class="label label-success">'+lang[x]+'</span></div>';
        } else if(x == 'partial' || x == 'transferring' || x == 'ordered' || x == 'PARCIAL') {
            return '<div class="text-center"><span class="label label-info">'+lang[x]+'</span></div>';
        } else if(x == 'due' || x == 'returned' || x == 'ABERTA') {
            if (diasAtraso < 0) {
                return '<div class="text-center"><span class="label label-danger">Vencida</span></div>';
            } else {
                return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
            }
        } else if (x == 'cancel' || x == 'CANCELADA') {
            return '<div class="text-center"><span class="label label-default">'+lang[x]+'</span></div>';
        } else if (x== 'reembolso'){
            return '<div class="text-center"><span class="label label-default">'+lang[x]+'</span></div>';
        } else if (x == 'orcamento') {
            return '<div class="text-center"><span class="label label-warning">'+lang[x]+'</span></div>';
        } else if (x == 'lista_espera') {
            return '<div class="text-center"><span class="label label-info">'+lang[x]+'</span></div>';
        } else if (x == 'faturada'){
            return '<div class="text-center"><span class="label label-success">'+lang[x]+'</span></div>';
        }
    }

    $(document).ready(function () {

        $('#start_date').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#end_date').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterTipoCobranca').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterReceita').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filterStatus').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#date_payment_from').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#date_payment_until').change(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        $('#filter_numero_documento').keydown(function(event){
            $('#SLData').DataTable().fnClearTable();
        });

        var oTable = $('#SLData').dataTable({
            "aaSorting": [[0, "asc"], [1, "desc"]],
            "iDisplayLength": <?=$Settings->rows_per_page?>,
            "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "<?=lang('all')?>"]],
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=site_url('reports/getReceitas')?>',
            "fnServerParams": function (aoData) {
                aoData.push({ "name": "start_date", "value":  $('#start_date').val() });
                aoData.push({ "name": "end_date", "value":  $('#end_date').val() });
                aoData.push({ "name": "filter_numero_documento", "value":  $('#filter_numero_documento').val() });
                aoData.push({ "name": "filterTipoCobranca", "value":  $('#filterTipoCobranca').val() });
                aoData.push({ "name": "filterReceita", "value":  $('#filterReceita').val() });
                aoData.push({ "name": "filterProgramacao", "value":  $('#programacao').val() });
                aoData.push({ "name": "filterStatus", "value":  $('#filterStatus').val() });
                aoData.push({ "name": "filterBiller", "value":  $('#biller_filter').val() });
                aoData.push({ "name": "filterDataPagamentoDe", "value":  $('#date_payment_from').val() });
                aoData.push({ "name": "filterDataPagamentoAte", "value":  $('#date_payment_until').val() });
                aoData.push({ "name": "filterCustomer", "value":  $('#customer_filter').val() });
            },
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>",
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                let vendaId = aData[struture.sale_id];

                if (vendaId !== null) {
                    nRow.id = aData[struture.sale_id];
                    nRow.className = "invoice_link";
                }

                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fsd},
                {"mRender": attachDescriptionToName},
                null,
                null,
                null,
                {"mRender": currencyFormatValuePay},
                {"mRender": currencyFormatTaxas},
                {"mRender": currencyFormatValuePaid},
                {"mRender": currencyFormatValueToPay},
                {"mRender": row_status_financeiro},
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {

                const totalizadorCredito = aaData
                    .reduce( (totalizadores, faturaAtual) => {
                        return createTotalizador(totalizadores, faturaAtual);
                    }, {total:0, pago: 0, taxas: 0, pagar: 0} );

                var nCells = nRow.getElementsByTagName('th');
                nCells[6].innerHTML = currencyFormat(parseFloat(totalizadorCredito.total));
                nCells[7].innerHTML = currencyFormat(parseFloat(totalizadorCredito.taxas));
                nCells[8].innerHTML = currencyFormat(parseFloat(totalizadorCredito.pago));
                nCells[9].innerHTML = currencyFormat(parseFloat(totalizadorCredito.pagar));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[(ano-mes-dia)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('pessoa');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('servico');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('receita');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('cobranca');?>]", filter_type: "text", data: []},
            {
                column_number: 9, select_type: 'select2',
                select_type_options: {
                    placeholder: '<?=lang('payment_status');?>',
                    width: '100%',
                    minimumResultsForSearch: -1,
                    allowClear: true
                },
                data: [
                    {value: 'ABERTA', label: '<?=lang('ABERTA');?>'},
                    {value: 'PARCIAL', label: '<?=lang('PARCIAL');?>'},
                    {value: 'VENCIDA', label: '<?=lang('VENCIDA');?>'},
                    {value: 'QUITADA', label: '<?=lang('QUITADA');?>'}
                ]
            }

        ], "footer");
    });
    
    function createTotalizador(totalizadores, faturaAtual) {

        let taxa = 0;

        if (!isNaN(parseFloat(faturaAtual[struture.taxas]))) {
            taxa = parseFloat(faturaAtual[struture.taxas]);
        }

        if (isNaN(totalizadores.taxas)) {
            totalizadores.taxas = 0;
        }

        totalizadores.total = totalizadores.total + parseFloat(faturaAtual[struture.valorfatura]);
        totalizadores.pago = totalizadores.pago + parseFloat(faturaAtual[struture.valorpago]);
        totalizadores.pagar = totalizadores.pagar + parseFloat(faturaAtual[struture.valorpagar]);
        totalizadores.taxas = totalizadores.taxas + taxa;
        
        return totalizadores;
    }

</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue">
            <i class="fa fa-sign-in"></i><?=lang('overdue_bills_alerts');?>
        </h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-tasks tip" data-placement="left" title="<?=lang("actions")?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?=site_url('financeiro/adicionarContaReceber')?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">
                                <i class="fa fa-arrow-circle-o-up"></i> <?=lang('adicionar_conta_receber')?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div id="form">
                <div class="col-lg-12">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border filters"><i class="fa  fa-search"></i> <?= lang('filters') ?> <img class="imgfilters" src="<?= $assets ?>images/abrirSubTitulo-o.gif"></legend>
                        <div style="margin-bottom: 20px;" class="divfilters">

                            <div >
                                <div class="col-sm-2">
                                    <?= lang("status_viagem", "status_viagem") ?>
                                    <?php
                                    $opts = array(
                                        'Confirmado' => lang('confirmado'),
                                        'Inativo' => lang('produto_inativo'),
                                        'Arquivado' => lang('arquivado') ,
                                    );
                                    echo form_dropdown('status', $opts,  $unit, 'class="form-control" id="status"');
                                    ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= lang("ano", "ano") ?>
                                    <?php
                                    $opts = array(
                                        '2020' => lang('2020'),
                                        '2021' => lang('2021'),
                                        '2022' => lang('2022'),
                                        '2023' => lang('2023'),
                                        '2024' => lang('2024'),
                                        '2025' => lang('2025'),
                                        '2026' => lang('2026'),
                                        '2027' => lang('2027'),
                                        '2028' => lang('2028'),
                                        '2029' => lang('2029'),
                                        '2030' => lang('2030'),
                                    );
                                    echo form_dropdown('ano', $opts,  $ano, 'class="form-control" id="ano"');
                                    ?>
                                </div>
                                <div class="col-sm-2">
                                    <?= lang("mes", "mes") ?>
                                    <?php
                                    $opts = array(
                                        'Todos' => lang('Todos'),
                                        '01' => lang('Janeiro'),
                                        '02' => lang('Fevereiro'),
                                        '03' => lang('Março'),
                                        '04' => lang('Abril'),
                                        '05' => lang('Maio'),
                                        '06' => lang('Junho'),
                                        '07' => lang('Julho'),
                                        '08' => lang('Agosto'),
                                        '09' => lang('Setembro'),
                                        '10' => lang('Outubro'),
                                        '11' => lang('Novembro'),
                                        '12' => lang('Dezembro'),
                                    );
                                    echo form_dropdown('mes', $opts,  $mes, 'class="form-control" id="mes"');
                                    ?>
                                </div>
                                <div class="col-sm-6">
                                    <label class="control-label" for="product"><?= lang("products"); ?></label>
                                    <?php
                                    $pgs[""] = lang('select').' '.lang('product');
                                    echo form_dropdown('programacao', $pgs,  '', 'class="form-control" id="programacao" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_de", "start_date"); ?>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : $start_date), 'class="form-control" id="start_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_vencimento_ate", "end_date"); ?>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : $end_date), 'type="date" class="form-control" id="end_date"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="filterStatus"><?=lang('payment_status');?></label>
                                    <?php
                                    $stc[""]        = $this->lang->line("select") . " " . $this->lang->line("payment_status");
                                    $stc["ABERTA"]  =  lang('ABERTA');
                                    $stc["PARCIAL"] =  lang('PARCIAL');
                                    $stc["QUITADA"] =  lang('QUITADA');
                                    $stc["VENCIDA"] =  lang('VENCIDA');
                                    echo form_dropdown('filterStatus', $stc, (isset($_POST['filterStatus']) ? $_POST['filterStatus'] : $filterStatus), 'class="form-control" id="filterStatus" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("payment_status") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_cobranca"); ?></label>
                                    <?php
                                    $tcs[""] = lang('select').' '.lang('tipo_cobranca');
                                    foreach ($tiposCobranca as $tipoCobranca) {
                                        $tcs[$tipoCobranca->id] = $tipoCobranca->name;
                                    }
                                    echo form_dropdown('filterTipoCobranca', $tcs, (isset($_POST['filterTipoCobranca']) ? $_POST['filterTipoCobranca'] : $filterTipoCobranca), 'class="form-control" id="filterTipoCobranca" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("tipo_cobranca") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label class="control-label" for="user"><?= lang("tipo_receita"); ?></label>
                                    <select class="form-control tip" name="filterReceita" id="filterReceita">
                                        <option value=""><?php echo lang('select').' '.lang('receita');?></option>
                                        <?php
                                        foreach ($planoReceitas as $objReceita) {?>
                                            <optgroup label="<?php echo $objReceita->name;?>">
                                                <?php
                                                $receitasFilhas = $this->financeiro_model->getReceitaByReceitaSuperiorId($objReceita->id);
                                                foreach ($receitasFilhas as $item) {?>
                                                    <option value="<?php echo $item->id;?>"><?php echo $item->name;?></option>
                                                <?php }?>
                                            </optgroup>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_pagamento_de", "date_payment_from"); ?>
                                    <?php echo form_input('date_payment_from', (isset($_POST['date_payment_from']) ? $_POST['date_payment_from'] : $date_payment_from), 'class="form-control" id="date_payment_from"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?= lang("data_pagamento_ate", "date_payment_until"); ?>
                                    <?php echo form_input('date_payment_until', (isset($_POST['date_payment_until']) ? $_POST['date_payment_until'] : $date_payment_until), 'type="date" class="form-control" id="date_payment_until"', 'date'); ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= lang("customer", "customer_filter"); ?>
                                    <?php
                                    echo form_input('customer_filter', (isset($_POST['customer_filter']) ? $_POST['customer_filter'] : ""), 'id="customer_filter" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <?= lang("biller", "biller_filter"); ?>
                                    <?php
                                    $bl[""] = lang("select") . ' ' . lang("biller") ;
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller_filter', $bl, (isset($_POST['biller_filter']) ? $_POST['biller_filter'] : $biller_filter), 'id="biller_filter" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '"class="form-control input-tip select" style="width:100%;"');
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-2" style="display: none;">
                                <div class="form-group">
                                    <?= lang("numero_documento", "filter_numero_documento") ?>
                                    <?= form_input('filter_numero_documento', (isset($_POST['filter_numero_documento']) ? $_POST['filter_numero_documento'] : ''), 'class="form-control" id="filter_numero_documento"'); ?>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="controls">
                                    <input type="button" onclick="limparFormulario();" name="submit_fin" value="<?=lang('clear')?>" class="btn btn-primary input-xs">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table id="SLData" class="table table-bordered table-hover table-striped" style="cursor: pointer;">
                        <thead>
                        <tr>
                            <th style="min-width:30px; width: 30px; text-align: center;display: none;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                            <th><?php echo $this->lang->line("date"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("pessoa"); ?></th>
                            <th style="text-align: left;"><?php echo $this->lang->line("servico"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("receita"); ?></th>
                            <th style="text-align: left;display: none;"><?php echo $this->lang->line("cobranca"); ?></th>
                            <th><?php echo $this->lang->line("total"); ?></th>
                            <th><?php echo $this->lang->line("taxas"); ?></th>
                            <th><?php echo $this->lang->line("pago"); ?></th>
                            <th><?php echo $this->lang->line("pagar"); ?></th>
                            <th style="text-align: center;"><?php echo $this->lang->line("payment_status"); ?></th>
                            <th style="display: none;" class="no-print">Parc.</th>
                            <th style="display: none;" class="no-print">Ult.</th>
                            <th style="display: none;" class="no-print">Atras.</th>
                            <th style="display: none;" class="no-print">Venda.</th>
                            <th style="display: none;" class="no-print">Link.</th>
                            <th style="display: none;" class="no-print">Tipo.</th>
                            <th style="display: none;" class="no-print">Cartao.</th>
                            <th style="display: none;" class="no-print">Cod.</th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td colspan="12" class="dataTables_empty"><?php echo $this->lang->line("loading_data"); ?></td>
                        </tr>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="display: none;"></th>
                            <th style="width:80px; text-align:center;"><?php echo $this->lang->line("actions"); ?></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        createFilter('filters');
        buscarProdutos();

        $('#programacao').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $('#biller_filter').change(function(event){
            $('#SLData').DataTable().fnClearTable()
        });

        $("#ano").change(function (event){
            buscarProdutos();
        });

        $("#mes").change(function (event){
            buscarProdutos();
        });

        $("#status").change(function (event){
            buscarProdutos();
        });

        $('#customer_filter').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        }).on('change', function (e) {
            $('#SLData').DataTable().fnClearTable();
        });

        function createFilter(nameClasse) {
            $('.'+nameClasse).click(function() {
                if ($('.div'+nameClasse).is(':visible')) {
                    $('.div'+nameClasse).hide(300);
                    $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-c.gif');

                } else {
                    $('.div'+nameClasse).show(300).fadeIn();
                    $('.img'+nameClasse).attr('src', '<?= $assets ?>images/abrirSubTitulo-o.gif');
                }
            });
        }

        <?php if ($customer_filter) {?>
            $('#customer_filter').val(<?=$customer_filter?>).trigger('change');
        <?php } ?>
    });

    function limparFormulario() {
        $('#start_date').val('<?=$start_date?>');
        $('#end_date').val('<?=$end_date?>');
        $('#date_payment_from').val('');
        $('#date_payment_until').val('');

        if ($('#programacao').val() !== '') $('#programacao').val('').change();
        if ($('#filterTipoCobranca').val() !== '') $('#filterTipoCobranca').val('').change();
        if ($('#filterStatus').val() !== '') $('#filterStatus').val('').change();
        if ($('#customer_filter').val() !== '') $('#customer_filter').val('').trigger('change');

        $('#SLData').DataTable().fnClearTable();
    }

    function buscarProdutos() {

        $.ajax({
            type: "GET",
            url: "<?php echo base_url() ?>apputil/buscarProgramacao",
            data: {
                status: $('#status').val(),
                ano: $('#ano').val(),
                mes: $('#mes').val()
            },
            dataType: 'json',
            async: true,
            success: function (agendamentos) {
                $('#programacao').empty();

                var option = $('<option/>');

                option.attr({ 'value': '' }).text('Selecione uma opção');
                $('#programacao').append(option);

                $(agendamentos).each(function( index, agendamento ) {
                    var option = $('<option/>');

                    option.attr({ 'value': agendamento.id }).text(agendamento.label);
                    $('#programacao').append(option);
                });

                $('#programacao').select2({minimumResultsForSearch: 7});

                <?php if ($filterProgramacao) {?>
                $('#programacao').val(<?=$filterProgramacao?>).trigger('change');
                <?php } ?>
            }
        });
    }

</script>
