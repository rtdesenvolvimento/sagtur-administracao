<?php

$v = "";

if ($this->input->post('unit')) {
    $v .= "&unit=" . $this->input->post('unit');
}
if ($this->input->post('data_saida')) {
    $v .= "&data_saida=" . $this->input->post('data_saida');
}
if ($this->input->post('data_retorno')) {
    $v .= "&data_retorno=" . $this->input->post('data_retorno');
}

?>


<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('relatorio_financeiro_despesas_viagem_geral'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            }
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="form">
                    <?php echo form_open("reports/relFinanceiroDespesasViagemNivel1"); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("status", "status") ?>
                                <?php
                                $opts = array(
                                    '' => lang('selecione_opcao'),
                                    'Aguardando' => lang('Aguardando'),
                                    'Confirmado' => lang('confirmado'),
                                    'Em Viagem' => lang('em_viagem') ,
                                    'Executado' => lang('executado') ,
                                    'Cancelado' => lang('cancelado')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ''), 'class="form-control" id="unit"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('data_saida', 'data_saida'); ?>
                                <input type="date" name="data_saida" value="<?php echo (isset($_POST['data_saida']) ? $_POST['data_saida'] : '');?>" class="form-control tip" id="data_saida" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('data_retorno', 'data_retorno'); ?>
                                <input type="date" name="data_retorno" value="<?php echo (isset($_POST['data_retorno']) ? $_POST['data_retorno'] : '');?>" class="form-control tip" id="data_retorno" data-original-title="" title="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?> </div>
                    </div>

                    <?php echo form_close(); ?>
                </div>
                <div class="clearfix"></div>

                <div class="table-responsive">
                    <table id=""
                           style="cursor: pointer;"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th style="text-align: left;width: 40%;"><?= lang("viagem"); ?></th>
                            <th style="text-align: center;"><?= lang("saida"); ?></th>
                            <th style="text-align: center;"><?= lang("retorno"); ?></th>
                            <th style="text-align: right;"><?= lang("valor"); ?></th>
                            <th style="text-align: right;"><?= lang("pago"); ?></th>
                            <th style="text-align: right;"><?= lang("falta_pagar"); ?></th>
                            <th style="text-align: center;width: 1%;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $total = 0;
                        $totalPago = 0;
                        $totalFalta = 0;
                        foreach ($products as $product) {

                            $data_saida     = $product->data_saida;
                            $data_retorno   = $product->data_retorno;
                            $price          = $product->price;
                            $grand_total    = $product->grand_total;
                            $paid           = $product->paid;
                            $balance        = $product->balance;
                            $nomeProduto    = $product->name;

                            $total = $total + $grand_total;
                            $totalPago = $totalPago + $paid;
                            $totalFalta = $totalFalta + $balance;

                            if ($data_saida) {
                                $data_saida = $this->sma->hrsd($data_saida);
                            }

                            if ($data_retorno) {
                                $data_retorno = $this->sma->hrsd($data_retorno);
                            }

                            if ($product->name) {
                                $nomeProduto = $product->name;
                            }  else {
                                $nomeProduto = 'OUTRAS RECEITAS';
                            }
                            ?>
                            <tr class="product_link" id="<?php echo $product->id;?>">
                                <td style="text-align: left;"><?php echo $nomeProduto;?></td>
                                <td style="text-align: center;"><?php echo $data_saida;?></td>
                                <td style="text-align: center;"><?php echo $data_retorno;?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($grand_total);?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($paid);?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($balance);?></td>
                                <td style="text-align: center">
                                    <?php if ($product->name) {?>
                                        <a href="<?php echo  site_url();?>reports/relFinanceiroGeralDespesasNivel2/<?php echo $product->code;?>"><i class="fa fa-fast-forward"></i>  </a>
                                    <?php } else { ?>
                                        <a href="<?php echo  site_url();?>reports/relFinanceiroGeralDespesasNivel2/outras_receitas"><i class="fa fa-fast-forward"></i>  </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($total);?></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalPago);?></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalFalta);?></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#form').show();



        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>