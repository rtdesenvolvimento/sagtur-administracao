<?php

$v = "";

if ($this->input->post('status_pagamento')) {
    $v .= "&status_pagamento=" . $this->input->post('status_pagamento');
}

if ($this->input->post('cliente')) {
    $v .= "&cliente=" . $this->input->post('cliente');
}
if ($this->input->post('vencimento_de')) {
    $v .= "&vencimento_de=" . $this->input->post('vencimento_de');
}

if ($this->input->post('vencimento_ate')) {
    $v .= "&vencimento_ate=" . $this->input->post('vencimento_ate');
}

if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}

?>

<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($this->input->post('cliente') || $clinteObj) {
        $cliente = $this->input->post('cliente');
        if (!$cliente) {
            $cliente = $clinteObj;
        }
        ?>
        $('#cliente').val(<?=$cliente ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/suggestionsByID/<?=$cliente ?>",
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });

        $('#cliente').val(<?= $this->input->post('cliente') ?>);
        <?php } else { ?>
        $('#cliente').select2({
            minimumInputLength: 1,
            data: [],
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                    }
                }
            }
        });
        <?php } ?>

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-usd"></i><?= lang('relatorio_comissao'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            }
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">

                <li class="dropdown">
                    <a href="#" id="imprimir" class="tip" title="<?= lang('print') ?>">
                        <i class="icon fa fa-print"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <div id="form">
                    <?php echo form_open("reports/relatoriocomissao"); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("status_viagem", "status_viagem") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'Aguardando' => lang('Aguardando'),
                                    'Confirmado' => lang('confirmado'),
                                    'Em Viagem' => lang('em_viagem') ,
                                    'Executado' => lang('executado') ,
                                    'Cancelado' => lang('cancelado')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ''), 'class="form-control" id="unit"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                <?php
                                $wh[""]                 = lang('select').' '.lang('warehouse');
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }

                                if ($warehouseObjId) {
                                    echo form_dropdown('warehouse', $wh,   $warehouseObjId, 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                } else {
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("biller", "slbiller"); ?>
                                <?php
                                $bl["todos"] =  lang('todos');
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->name : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : ''), 'id="slbiller" name="biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group">
                                <?= lang("status_pagamento", "status_pagamento") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'pending' => lang('pending'),
                                    'partial' => lang('partial'),
                                    'paid' => lang('paid')
                                );
                                echo form_dropdown('status_pagamento', $opts, (isset($_POST['status_pagamento']) ? $_POST['status_pagamento'] : ''), 'class="form-control" id="status_pagamento"');
                                ?>
                            </div>
                        </div>

                        <!--
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('vencimento_de', 'vencimento_de'); ?>
                                <input type="date" name="vencimento_de" value="<?php echo (isset($_POST['vencimento_de']) ? $_POST['vencimento_de'] : '');?>" class="form-control tip" id="vencimento_de" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang('vencimento_ate', 'vencimento_ate'); ?>
                                <input type="date" name="vencimento_ate" value="<?php echo (isset($_POST['vencimento_ate']) ? $_POST['vencimento_ate'] : '');?>" class="form-control tip" id="vencimento_ate" data-original-title="" title="">
                            </div>
                        </div>
                        !-->
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                            <input type="button" value="Limpar"  class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>reports/relatoriocomissao'">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="clearfix"></div>

                <div class="table-responsive" id="divImprimir">
                    <table id="tbFinanceiroGeralNivel2"
                           style="cursor: pointer;font-size: 11px;"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th style="text-align: left;width: 35%;"><?= lang("product"); ?></th>
                            <th style="text-align: left;width: 35%;"><?= lang("biller"); ?></th>
                            <!--<th style="text-align: center;"><?= lang("vencimento"); ?></th>!-->
                            <th style="text-align: right;"><?= lang("valor_pagar"); ?></th>
                            <th style="text-align: center;" colspan="2"><?= lang("forma_pagamento"); ?></th>
                            <th style="text-align: right;"><?= lang("falta_pagar"); ?></th>
                            <th style="text-align: center;"><?= lang("status"); ?></th>
                            <th style="text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>

                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <!--<th></th>!-->
                            <th style="text-align: center;"><?= lang("cartao_credito"); ?></th>
                            <th style="text-align: center;"><?= lang("dinheiro"); ?></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $totalValor = 0;
                        $totalPago = 0;
                        $TotalFaltaPagar = 0;
                        $TotalCartao = 0;
                        $TotalDinheiro = 0;

                        foreach ($purchases as $purchase) {

                            $filterStatusPagamento = $this->input->post('status_pagamento');
                            $warehouse = $this->site->getWarehouseByID($purchase->warehouse_id);
                            $biller_details = $this->site->getCompanyByID($purchase->supplier_id);

                            $subtotal           = $purchase->grand_total;
                            $vencimento         = $purchase->vencimento;
                            $dataHoje           = date('Y-m-d');
                            $paidItemDinheiro   = 0;
                            $paidDinheiro       = 0;
                            $vencimentoExibir   = '';

                            $atrasada           = false;

                            if (!$vencimento) {
                                $vencimento = date('Y-m-d');
                            }

                            if(strtotime($vencimento) < strtotime($dataHoje)) {
                                $atrasada = true;
                            }

                            $paidObjectDinheiro = $this->reports_model->getPaymentsDinheiroDespesa($purchase->id);
                            $paidObjectCartao   = $this->reports_model->getPaymentsCartaoDespesa($purchase->id);

                            if (count($paidObjectDinheiro) >0) {
                                $paidDinheiro = $paidObjectDinheiro->amount;
                            }

                            if (count($paidObjectCartao) >0) {
                                $paidCartao = $paidObjectCartao->amount;
                            }


                            $falta              = $subtotal - $paidDinheiro - $paidCartao;

                            $totalValor         = $totalValor + $subtotal;
                            $totalPago          = $totalPago + $paidItemDinheiro;
                            $TotalFaltaPagar    = $TotalFaltaPagar + $falta;

                            $TotalCartao        = $TotalCartao + $paidCartao;
                            $TotalDinheiro      = $TotalDinheiro + $paidDinheiro;

                            $supplier_id        = $purchase->supplier_id;
                            $fornecedor 	    = $this->site->getCompanyByID($supplier_id);

                            if ($purchase->vencimento) {
                                $vencimentoExibir = $this->sma->hrsd($purchase->vencimento);
                            }

                            $statusPagamento = '';
                            $color           = '';

                            if ($purchase->payment_status == 'partial') {
                                if ($atrasada) {
                                    $statusPagamento = '<span class="label label-danger">'.lang('atrasada').'</span>';
                                    $color = '';
                                } else {
                                    $statusPagamento = '<span class="label label-info">'.lang($purchase->payment_status).'</span>';
                                    $color = '';
                                }
                            } else if ($purchase->payment_status == 'pending') {
                                if ($atrasada) {
                                    $statusPagamento = '<span class="label label-danger">'.lang('atrasada').'</span>';
                                    $color = '';
                                } else {
                                    $statusPagamento = '<span class="label label-info" style="background: blue;">Em Aberto</span>';
                                    $color = '';
                                }
                            } else {
                                $statusPagamento = '<span class="label label-success">'.lang($purchase->payment_status).'</span>';
                                $color = '';
                            }

                            ?>
                            <tr class="purchase_link" id="<?php echo $purchase->id;?>">
                                <td style="text-align: left;text-transform: uppercase;width: 35%"><?php echo $warehouse->name;?></td>
                                <td style="text-align: left;text-transform: uppercase;"><?php echo $biller_details->name;?></td>
                                <!--<td style="text-align: center;"><?php echo $vencimentoExibir;?></td>!-->
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($subtotal);?></td>
                                <td style="text-align: center;"><?php echo $this->sma->formatMoney($paidCartao);?></td>
                                <td style="text-align: center;"><?php echo $this->sma->formatMoney($paidDinheiro);?></td>
                                <td style="text-align: right;color: <?php echo $color;?>;"><?php echo $this->sma->formatMoney($falta);?></td>
                                <td style="text-align: center;" ><?php echo $statusPagamento;?></td>
                                <td class="">
                                    <div class="text-center"><div class="btn-group text-left"><button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">Ações <span class="caret"></span></button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo base_url();?>purchases/view/<?php echo $purchase->id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Despesa</a></li>
                                                <li><a href="<?php echo base_url();?>purchases/payments/<?php echo $purchase->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Ver Pagamentos</a></li>
                                                <li><a href="<?php echo base_url();?>purchases/add_payment/<?php echo $purchase->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Adicionar Pagamento</a></li>
                                                <li><a href="<?php echo base_url();?>purchases/edit/<?php echo $purchase->id;?>"><i class="fa fa-edit"></i> Editar Despesa</a></li>
                                                <li><a href="<?php echo base_url();?>purchases/pdf/<?php echo $purchase->id;?>"><i class="fa fa-file-pdf-o"></i> Baixar como PDF</a></li>
                                                <li><a href="<?php echo base_url();?>purchases/email/<?php echo $purchase->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> E-mail de Despesa</a></li>
                                             </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>


                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <!--<th></th>!-->
                            <th></th>
                            <th></th>
                            <th style="text-align: center;">Total</th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalDinheiro + $TotalCartao);?></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <!--<th></th>!-->
                            <th></th>
                            <th></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalValor);?></th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalCartao);?></th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalDinheiro);?></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($TotalFaltaPagar);?></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $('#form').show();

        $('#imprimir').click(function (event) {
            event.preventDefault();

            //pega o Html da DIV
            var divElements = document.getElementById('divImprimir').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";

            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;

            location.reload();
        });


        $('#irCartao').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoDespesaNivel3/CC_other/"+warehouse+"/"+cliente;
        });

        $('#irDinheiro').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoDespesaNivel3/cash_deposit/"+warehouse+"/"+cliente;
        });

        $('#idFormaPagamento').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoDespesaNivel3/all/"+warehouse+"/"+cliente;
        });

        $('#unit').change(function (event) {
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: site.base_url + "products/getProdutoFilterStatus/"+$('#unit').val(),
                dataType: 'json',
                success: function(produtos)
                {
                    $('#warehouse').empty();
                    $("#warehouse").append('<option value=><?php echo lang('select').' '.lang('warehouse');?></option>');
                    $.each(produtos, function( index, produto ) {
                        $("#warehouse").append('<option value='+produto.id+'>'+produto.name+'</option>');
                    });
                    $('#warehouse').select2();
                }
            });
        });
    });
</script>