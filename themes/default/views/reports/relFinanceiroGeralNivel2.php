<?php

$v = "";

if ($this->input->post('status_pagamento')) {
    $v .= "&status_pagamento=" . $this->input->post('status_pagamento');
}

if ($this->input->post('cliente')) {
    $v .= "&cliente=" . $this->input->post('cliente');
}
if ($this->input->post('vencimento_de')) {
    $v .= "&vencimento_de=" . $this->input->post('vencimento_de');
}

if ($this->input->post('vencimento_ate')) {
    $v .= "&vencimento_ate=" . $this->input->post('vencimento_ate');
}

if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}

?>


<script type="text/javascript">
    $(document).ready(function () {
        <?php if ($this->input->post('cliente') || $clinteObj) {
                $cliente = $this->input->post('cliente');
                if (!$cliente) {
                    $cliente = $clinteObj;
                }
            ?>
            $('#cliente').val(<?=$cliente ?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/suggestions/<?=$cliente ?>",
                        dataType: "json",
                        success: function (data) {
                            callback(data.results[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('#cliente').val(<?= $this->input->post('cliente') ?>);
        <?php } else { ?>
            $('#cliente').select2({
                minimumInputLength: 1,
                data: [],
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } ?>

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-bus"></i><?= lang('relatorio_financeiro_passageiros'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            }
            ?>
        </h2>

        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">

                <li class="dropdown">
                    <a href="#" id="imprimir" class="tip" title="<?= lang('print') ?>">
                        <i class="icon fa fa-print"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <div id="form">
                    <?php echo form_open("reports/relFinanceiroGeralNivel2"); ?>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <?= lang("status_viagem", "status_viagem") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'Aguardando' => lang('Aguardando'),
                                    'Confirmado' => lang('confirmado'),
                                    'Em Viagem' => lang('em_viagem') ,
                                    'Executado' => lang('executado') ,
                                    'Cancelado' => lang('cancelado')
                                );
                                echo form_dropdown('unit', $opts, (isset($_POST['unit']) ? $_POST['unit'] : ''), 'class="form-control" id="unit"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-8">
                            <div class="form-group">
                                <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                <?php
                                $wh[""]                 = lang('select').' '.lang('warehouse');
                                $wh["outras_receitas"]  = lang('outras_receitas');
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }

                                if ($warehouseObjId) {
                                    echo form_dropdown('warehouse', $wh,   $warehouseObjId, 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                } else {
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                }
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                <?php if ($clinteObj) {?>
                                    <?php echo form_input('cliente',$clinteObj, 'class="form-control" id="cliente" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("cliente") . '"'); ?>
                                <?php } else { ?>
                                    <?php echo form_input('cliente', (isset($_POST['cliente']) ? $_POST['cliente'] : ""), 'class="form-control" id="cliente" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("cliente") . '"'); ?>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("status_pagamento", "status_pagamento") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'pending' => lang('pending'),
                                    'partial' => lang('partial'),
                                    'paid' => lang('paid') ,
                                    'atrasada' => lang('atrasada')
                                );
                                echo form_dropdown('status_pagamento', $opts, (isset($_POST['status_pagamento']) ? $_POST['status_pagamento'] : ''), 'class="form-control" id="status_pagamento"');
                                ?>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang('vencimento_de', 'vencimento_de'); ?>
                                <input type="date" name="vencimento_de" value="<?php echo (isset($_POST['vencimento_de']) ? $_POST['vencimento_de'] : '');?>" class="form-control tip" id="vencimento_de" data-original-title="" title="">
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <?= lang('vencimento_ate', 'vencimento_ate'); ?>
                                <input type="date" name="vencimento_ate" value="<?php echo (isset($_POST['vencimento_ate']) ? $_POST['vencimento_ate'] : '');?>" class="form-control tip" id="vencimento_ate" data-original-title="" title="">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary"'); ?>
                            <input type="button" value="Limpar"  class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>reports/relFinanceiroGeralNivel2'">
                            <input type="button" value="Voltar" class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>reports/relFinanceiroViagemNivel1'">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="clearfix"></div>

                <div class="table-responsive" id="divImprimir">
                    <table id="tbFinanceiroGeralNivel2"
                           style="cursor: pointer;"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">

                        <thead>
                        <tr>
                            <th style="text-align: center;width: 1%;"><?= lang("n"); ?></th>
                            <th style="text-align: center;width: 1%;"><?= lang("ref"); ?></th>
                            <th style="text-align: left;width: 35%;"><?= lang("passageiro"); ?></th>
                            <th style="text-align: center;width: 2%;"><?= lang("poltrona"); ?></th>
                            <th style="text-align: center;"><?= lang("vencimento"); ?></th>
                            <th style="text-align: right;"><?= lang("valor_pagar"); ?></th>
                            <th style="text-align: center;" colspan="2"><u><a style="color: white;" id="idFormaPagamento"><?= lang("forma_pagamento"); ?></a></u></th>
                            <th style="text-align: right;"><?= lang("falta"); ?></th>
                            <th style="text-align: center;"><?= lang("status"); ?></th>
                            <th style="text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>

                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: center;"><u> <a id="irCartao"><?= lang("cartao_credito"); ?></a></u></th>
                            <th style="text-align: center;"><u> <a id="irDinheiro"><?= lang("dinheiro"); ?></a></u></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $contadoColunas = 0;
                        $totalValor = 0;
                        $totalPago = 0;
                        $TotalFaltaPagar = 0;
                        $TotalCartao = 0;
                        $TotalDinheiro = 0;

                        foreach ($sales as $sale) {

                            $filterStatusPagamento = $this->input->post('status_pagamento');
                            $contadoColunas = $contadoColunas + (int) $sale->quantity;

                            $grand_total        = $sale->grand_total;
                            $subtotal           = $sale->subtotal;
                            $vencimento         = $sale->vencimento;
                            $dataHoje           = date('Y-m-d');
                            $paidItemDinheiro   = 0;
                            $paidDinheiro       = 0;
                            $vencimentoExibir   = '';

                            $paidItemCartao     = 0;
                            $paidCartao         = 0;
                            $atrasada           = false;

                            if (!$vencimento) {
                                $vencimento = date('Y-m-d');
                            }

                            if(strtotime($vencimento) < strtotime($dataHoje)) {
                                $atrasada = true;
                            }

                            $paidObjectDinheiro = $this->reports_model->getPaymentsDinheiro($sale->id);
                            $paidObjectCartao   = $this->reports_model->getPaymentsCartao($sale->id);

                            if (count($paidObjectDinheiro) >0) {
                                $paidDinheiro = $paidObjectDinheiro->amount;
                            }

                            if (count($paidObjectCartao) >0) {
                                $paidCartao = $paidObjectCartao->amount;
                            }

                            if ($grand_total > 0) {
                                $percentSubTotalGrad    =  ((($subtotal*100)/$grand_total)/100);
                                $paidItemDinheiro       =  $percentSubTotalGrad*$paidDinheiro;
                            }

                            if ($grand_total > 0) {
                                $percentSubTotalGrad    =  ((($subtotal*100)/$grand_total)/100);
                                $paidItemCartao         =  $percentSubTotalGrad*$paidCartao;
                            }

                            $falta              = $subtotal - $paidItemDinheiro - $paidItemCartao;

                            $totalValor         = $totalValor + $subtotal;
                            $totalPago          = $totalPago + $paidItemDinheiro;
                            $TotalFaltaPagar    = $TotalFaltaPagar + $falta;

                            $TotalCartao        = $TotalCartao + $paidItemCartao;
                            $TotalDinheiro      = $TotalDinheiro + $paidItemDinheiro;

                            $customerClient     = $sale->customerClient;
                            $cliente 			= $this->site->getCompanyByID($customerClient);

                            if ($sale->vencimento) {
                                $vencimentoExibir = $this->sma->hrsd($sale->vencimento);
                            }

                            $statusPagamento = '';
                            $color           = '';

                            if ($sale->payment_status == 'partial') {
                                if ($atrasada) {
                                    $statusPagamento = '<span class="label label-danger">'.lang('atrasada').'</span>';
                                    $color = 'red';
                                } else {
                                    $statusPagamento = '<span class="label label-info">'.lang($sale->payment_status).'</span>';
                                    $color = '#5bc0de';
                                }
                            } else if ($sale->payment_status == 'pending') {
                                if ($atrasada) {
                                    $statusPagamento = '<span class="label label-danger">'.lang('atrasada').'</span>';
                                    $color = '#red';
                                } else {
                                    $statusPagamento = '<span class="label label-warning">'.lang($sale->payment_status).'</span>';
                                    $color = '#f0ad4e';
                                }
                            } else if ($sale->payment_status == 'due') {
                                $statusPagamento = '<span class="label label-warning">'.lang($sale->payment_status).'</span>';
                                $color = '#f0ad4e';
                            } else {
                                $statusPagamento = '<span class="label label-success">'.lang($sale->payment_status).'</span>';
                                $color = '#5cb85c';
                            }

                            ?>
                            <tr class="invoice_link" id="<?php echo $sale->id;?>">
                                <td style="text-align: center;"><?php echo $contadoColunas;?></td>
                                <td style="text-align: center;"><?php echo $sale->id;?></td>
                                <td style="text-align: left;"><?php echo $cliente->name.' '. (int) $sale->quantity.'un';?></td>
                                <td style="text-align: center;"><?php echo $sale->poltronaClient;?></td>
                                <td style="text-align: center;"><?php echo $vencimentoExibir;?></td>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($sale->subtotal);?></td>
                                <td style="text-align: center;"><?php echo $this->sma->formatMoney($paidItemCartao);?></td>
                                <td style="text-align: center;"><?php echo $this->sma->formatMoney($paidItemDinheiro);?></td>
                                <td style="text-align: right;color: <?php echo $color;?>;font-weight: bold;font-size: 14px;"><?php echo $this->sma->formatMoney($falta);?></td>
                                <td style="text-align: center;" ><?php echo $statusPagamento;?></td>
                                <td class="acoes">
                                    <div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo base_url();?>sales/view/<?php echo $sale->id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>sales/emitir_contrato/<?php echo $sale->id;?>"><i class="fa fa-book"></i> Baixar Contrato</a></li>
                                                <li><a href="<?php echo base_url();?>sales/payments/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Ver Pagamentos</a></li>
                                                <li><a href="<?php echo base_url();?>sales/add_payment/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-money"></i> Adicionar Pagamento</a></li>
                                                <li><a href="<?php echo base_url();?>sales/edit/<?php echo $sale->id;?>" class="sledit"><i class="fa fa-edit"></i> Editar Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $sale->id;?>"><i class="fa fa-file-pdf-o"></i> Baixar Voucher</a></li>
                                                <li><a href="<?php echo base_url();?>sales/email/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> E-mail de Passagem</a></li>
                                                <li class="divider"></li>
                                                <li><a href="<?php echo base_url();?>customers/edit/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Visualizar Dados Passageiro</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>


                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: center;">Cartão + Dinheiro</th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalDinheiro + $TotalCartao);?></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>

                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalValor);?></th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalCartao);?></th>
                            <th style="text-align: center;"><?php echo $this->sma->formatMoney($TotalDinheiro);?></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($TotalFaltaPagar);?></th>
                            <th></th>
                            <th></th>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#form').show();

        $('#imprimir').click(function (event) {
            event.preventDefault();

            //pega o Html da DIV
            var divElements = document.getElementById('divImprimir').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";


            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;

            location.reload();

        });


        $('#irCartao').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/CC_other/"+warehouse+"/"+cliente;
        });

        $('#irDinheiro').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/cash_deposit/"+warehouse+"/"+cliente;
        });

        $('#idFormaPagamento').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/all/"+warehouse+"/"+cliente;
        });

        $('#unit').change(function (event) {
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: site.base_url + "products/getProdutoFilterStatus/"+$('#unit').val(),
                dataType: 'json',
                success: function(produtos)
                {
                    $('#warehouse').empty();
                    $("#warehouse").append('<option value=><?php echo lang('select').' '.lang('warehouse');?></option>');
                    $("#warehouse").append('<option value="outras_receitas"><?php echo lang('outras_receitas');?></option>');
                    $.each(produtos, function( index, produto ) {
                        $("#warehouse").append('<option value='+produto.id+'>'+produto.name+'</option>');
                    });
                    $('#warehouse').select2();
                }
            });
        });
    });
</script>