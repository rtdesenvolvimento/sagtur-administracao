<?php
function row_status($x)
{
    if($x == null) {
        return '';
    } else if($x == 'pending') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if($x == 'completed' || $x == 'paid' || $x == 'sent' || $x == 'received') {
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    } else if($x == 'partial' || $x == 'transferring' || $x == 'ordered') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if($x == 'due' || $x == 'returned') {
        return '<div class="text-center"><span class="label label-danger">'.lang($x).'</span></div>';
    } else if ($x == 'cancel') {
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'reembolso'){
        return '<div class="text-center"><span class="label label-default">'.lang($x).'</span></div>';
    } else if ($x == 'orcamento') {
        return '<div class="text-center"><span class="label label-warning">'.lang($x).'</span></div>';
    } else if ($x == 'lista_espera') {
        return '<div class="text-center"><span class="label label-info">'.lang($x).'</span></div>';
    } else if ($x == 'faturada'){
        return '<div class="text-center"><span class="label label-success">'.lang($x).'</span></div>';
    }
}
?>
<?php

$v = "";

if ($this->input->post('status_pagamento')) {
    $v .= "&status_pagamento=" . $this->input->post('status_pagamento');
}

if ($this->input->post('cliente')) {
    $v .= "&cliente=" . $this->input->post('cliente');
}
if ($this->input->post('vencimento_de')) {
    $v .= "&vencimento_de=" . $this->input->post('vencimento_de');
}
?>


<script type="text/javascript">
    $(document).ready(function () {

        <?php if ($this->input->post('cliente') || $cliente) {
                $cliente = $this->input->post('cliente');
                if (!$cliente) $cliente = $cliente;
            ?>

            $('#cliente').val(<?=$cliente ?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url + "customers/suggestions/<?=$cliente ?>",
                        dataType: "json",
                        success: function (data) {
                            callback(data.results[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });

            $('#cliente').val(<?= $this->input->post('cliente') ?>);
        <?php } else { ?>
            $('#cliente').select2({
                minimumInputLength: 1,
                data: [],
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'Nenhum resultado encontrado'}]};
                        }
                    }
                }
            });
        <?php } ?>
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-users"></i><?= lang('relatorio_financeiro_passageiros'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" id="imprimir" class="tip" title="<?= lang('print') ?>">
                        <i class="icon fa fa-print"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <div id="form">
                    <?php echo form_open("reports/relatorioPassageirosProgramacao/".$programacaoId); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label" for="programacaoId"><?= lang("product"); ?></label>
                                <?php
                                $prgs[""] = lang('select').' '.lang('product');
                                foreach ($programacoes as $programacao) {

                                    $label =  $this->sma->hrsd($programacao->dataSaida). ' - '.$programacao->name;

                                    if ($programacao->programacaoId != null) {
                                        $agenda = $this->AgendaViagemService_model->getProgramacaoById($programacao->programacaoId);

                                        if ($agenda->getTotalDisponvel() > 1) $label .=  ' - '.$agenda->getTotalDisponvel() .' Vagas disponível';
                                        else $label .=  ' - '.$agenda->getTotalDisponvel() .' Vaga disponível';
                                    }

                                    $prgs[$programacao->programacaoId] = $label;
                                }

                                echo form_dropdown('programacaoId', $prgs, (isset($_POST['programacaoId']) ? $_POST['programacaoId'] : $programacaoId) , 'class="form-control" id="programacaoId" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("product") . '"');

                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                <?php echo form_input('cliente', (isset($_POST['cliente']) ? $_POST['cliente'] : $cliente), 'class="form-control" id="cliente" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("cliente") . '"'); ?>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("status_pagamento", "status_pagamento") ?>
                                <?php
                                $opts = array(
                                    '' => lang('todos'),
                                    'due' => lang('due'),
                                    'partial' => lang('partial'),
                                    'paid' => lang('paid')
                                );
                                echo form_dropdown('status_pagamento', $opts, (isset($_POST['status_pagamento']) ? $_POST['status_pagamento'] : $status_pagamento), 'class="form-control" id="status_pagamento"');
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="controls">
                            <?php echo form_submit('submit_report', $this->lang->line("Consultar"), 'class="btn btn-primary"'); ?>
                            <input type="button" value="Limpar"  class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>reports/relatorioPassageirosProgramacao/<?php echo $programacaoId;?>'">
                            <input type="button" value="Voltar" class="btn btn-primary" onclick="window.location ='<?php echo base_url();?>products/edit/<?php echo $programacaoObj->produto;?>'">
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
                <div class="clearfix"></div>
                <div class="table-responsive" id="divImprimir">
                    <table id="tbFinanceiroGeralNivel2"
                           style="cursor: pointer;"
                           class="table table-bordered table-hover table-striped table-condensed reports-table">
                        <thead>
                        <tr>
                            <th style="text-align: center;width: 1%;"><?= lang("Nº"); ?></th>
                            <th style="text-align: center;width: 1%;"><?= lang("sale"); ?></th>
                            <th style="text-align: left;width: 80%;"><?= lang("passageiro"); ?></th>
                            <th style="text-align: center;width: 2%;"><?= lang("poltrona"); ?></th>
                            <th style="text-align: center;width: 2%;"><?= lang("status"); ?></th>

                            <!--
                            <th style="text-align: right;"><?= lang("Valor"); ?></th>
                            <th style="text-align: center;" colspan="<?php echo count($formaspagamento);?>"><?= lang("Recebido"); ?></th>
                            <th style="text-align: right;"><?= lang("Pagar"); ?></th>
                            !-->

                            <th style="text-align: center;"><?= lang("payment_status"); ?></th>
                            <th style="text-align: center;"><?= lang("actions"); ?></th>
                        </tr>
                        </thead>
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <!--
                            <th></th>
                            <?php foreach ($formaspagamento as $formaPagamento){?>
                                <th style="text-align: center;"><small> <a id="<?php echo $formaPagamento->id;?>"><?php echo $formaPagamento->name;?></a></small></th>
                            <?php } ?>
                            <th></th>
                            !-->
                            <th></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $contadoColunas = 0;
                        $totalValor = 0;
                        $pag = [];

                        foreach ($sales as $sale) {

                            $filterStatusPagamento = $this->input->post('status_pagamento');
                            $contadoColunas = $contadoColunas + (int) $sale->quantity;

                            $cliente 			= $this->site->getCompanyByID($sale->customerClient);

                            list($ano, $mes, $dia) = explode('-', $cliente->data_aniversario);

                            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                            $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
                            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

                            $faturas = $this->financeiro_model->getParcelasFaturaByContaReceber($sale->id);
                            $fatura = null;

                            $subTotal += $sale->subtotal;
                            foreach ($faturas as $faturaObj) $fatura = $faturaObj;

                            ?>

                            <tr class="invoice_link" id="<?php echo $sale->id;?>">
                                <td style="text-align: center;"><?php echo $contadoColunas;?></td>
                                <td style="text-align: center;"><?php echo $sale->id;?></td>
                                <td style="text-align: left;"><?php echo $cliente->name.'<small> idade '.$idade.' ['.lang($sale->faixaNome).']'.' '. (int) $sale->quantity.'un</small>';?></td>
                                <td style="text-align: center;"><?php echo $sale->poltronaClient;?></td>
                                <td style="text-align: center;" ><?php echo row_status($sale->sale_status) ;?></td>

                                <!--
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($sale->subtotal);?></td>
                                <?php
                                $totalPago = 0;

                                foreach ($formaspagamento as $formaPagamento){

                                    $pago = $this->reports_model->getSumPaymentsVendaAndFormaPagamento($sale->id, $formaPagamento->id);

                                    $totalRealPagoPorDependente = $sale->subtotal * (($pago->amount*100/$sale->grand_total)/100);

                                    $totalPago += $totalRealPagoPorDependente;

                                    $pag[$formaPagamento->id] =  $pag[$formaPagamento->id]['valor'] + $pago->amount;

                                    ?>
                                    <td style="text-align: center;"><?php echo $this->sma->formatMoney($totalRealPagoPorDependente);?></td>
                                <?php } ?>

                                <?php
                                $totalPagoDependente += $totalPago;
                                ?>
                                <td style="text-align: right;"><?php echo $this->sma->formatMoney($totalPago);?></td>
                                !-->
                                <td style="text-align: center;"><?php echo row_status($sale->payment_status) ;?></td>
                                <td class="acoes">
                                    <div class="text-center">
                                        <div class="btn-group text-left">
                                            <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">
                                                Ações <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right" role="menu">
                                                <li><a href="<?php echo base_url();?>sales/view/<?php echo $sale->id;?>"><i class="fa fa-file-text-o"></i> Detalhes da Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>sales/emitir_contrato/<?php echo $sale->id;?>"><i class="fa fa-book"></i> Baixar Contrato</a></li>
                                                <?php if ($fatura != null){?>
                                                    <li><a href="<?php echo base_url();?>financeiro/historico/<?php echo $fatura->id;?>" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false"><i class="fa fa-eye"></i><?php echo lang('Ver Parcelas / Adicionar Pagamento')?></a></li>
                                                <?php }?>
                                                <li><a href="<?php echo base_url();?>sales/edit/<?php echo $sale->id;?>" class="sledit"><i class="fa fa-edit"></i> Editar Passagem</a></li>
                                                <li><a href="<?php echo base_url();?>salesutil/pdf/<?php echo $sale->id;?>"><i class="fa fa-file-pdf-o"></i> Baixar Voucher</a></li>
                                                <li><a href="<?php echo base_url();?>sales/email/<?php echo $sale->id;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-envelope"></i> E-mail de Passagem</a></li>

                                                <li class="divider"></li>
                                                <li>

                                                    <?php
                                                    $phone = str_replace('(', '', str_replace(')', '', $cliente->cf5));
                                                    $phone = str_replace('-', '', $phone);
                                                    $phone = str_replace(' ', '', $phone);
                                                    ?>
                                                    <a href="https://api.whatsapp.com/send?phone=55<?php echo $phone;?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('whatsapp_send')?></a>
                                                </li>
                                                <li><a href="<?php echo base_url().'appcompra/whatsapp/'.$sale->id.'?token='.$this->session->userdata('cnpjempresa');?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('compartilhar_dados_venda_whatsapp');?></a></li>
                                                <li><a href="<?php echo base_url().'appcompra/whatsapp_pdf_atualizado/'.$sale->id.'?token='.$this->session->userdata('cnpjempresa');?>" target="_blank"><i class="fa fa-whatsapp"></i> <?php echo lang('compartilhar_voucher_whatsapp');?></a></li>
                                                <li class="divider"></li>
                                                
                                                <li><a href="<?php echo base_url();?>customers/edit/<?php echo $sale->customerClient;?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-edit"></i> Visualizar Dados Passageiro</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                        <tfoot class="dtFilter">
                        <tr class="active">
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <!--
                            <th></th>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($subTotal);?></th>
                            <?php foreach ($formaspagamento as $formaPagamento){
                                ?>
                                <th style="text-align: center;"><?php echo $this->sma->formatMoney($pag[$formaPagamento->id]);?></th>
                            <?php }?>
                            <th style="text-align: right;"><?php echo $this->sma->formatMoney($totalPagoDependente);?></th>
                            !-->
                            <th></th>
                            <th></th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">

    $(document).ready(function () {

        $('#form').show();

        $('#imprimir').click(function (event) {
            event.preventDefault();

            //pega o Html da DIV
            var divElements = document.getElementById('divImprimir').innerHTML;
            //pega o HTML de toda tag Body
            var oldPage = document.body.innerHTML;

            //Alterna o body
            document.body.innerHTML =
                "<html><head><title></title></head><body>" +
                divElements + "</body>";


            //Imprime o body atual
            window.print();

            //Retorna o conteudo original da página.
            document.body.innerHTML = oldPage;

            location.reload();

        });


        $('#irCartao').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/CC_other/"+warehouse+"/"+cliente;
        });

        $('#irDinheiro').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/cash_deposit/"+warehouse+"/"+cliente;
        });

        $('#idFormaPagamento').click(function (event) {
            var warehouse = $('#warehouse').val();
            var cliente   = $('#cliente').val();

            if (warehouse === '') {
                warehouse = 'all';
            }

            if (cliente ==='') {
                cliente = 'all'
            }
            window.location.href = "<?php echo base_url();?>reports/relFinanceiroFormaPagamentoDetalhadoNivel3/all/"+warehouse+"/"+cliente;
        });

        $('#unit').change(function (event) {
            event.preventDefault();
            $.ajax({
                type: "GET",
                url: site.base_url + "products/getProdutoFilterStatus/"+$('#unit').val(),
                dataType: 'json',
                success: function(produtos)
                {
                    $('#warehouse').empty();
                    $("#warehouse").append('<option value=><?php echo lang('select').' '.lang('warehouse');?></option>');
                    $("#warehouse").append('<option value="outras_receitas"><?php echo lang('outras_receitas');?></option>');
                    $.each(produtos, function( index, produto ) {
                        $("#warehouse").append('<option value='+produto.id+'>'+produto.name+'</option>');
                    });
                    $('#warehouse').select2();
                }
            });
        });
    });
</script>