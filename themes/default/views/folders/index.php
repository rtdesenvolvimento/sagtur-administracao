<style>
    .panel-info {
        transition: box-shadow 0.3s ease-in-out;
    }

    .panel-info:hover {
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.3);
    }

    .folder {
        padding: 35px;
        border: 1px solid #ddd;
        box-shadow: none;
        border-radius: 10px;
        margin-bottom: 10px;
    }

    .status_documento {
        font-size: 1.0rem;
    }
    .sigantarios {
        padding: 2px;
        font-size: 11px;
        font-weight: 500;
    }

</style>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-folder"></i><?= lang('my_folders'); ?></h2>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="icon fa fa-plus tip" data-placement="left" title="<?= lang("actions") ?>"></i></a>
                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?php echo site_url('folders/addFolder'); ?>" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-plus"></i> <?= lang('add_folder') ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="box-icon">
            <ul class="btn-tasks">
                <li class="dropdown">
                    <a href="#" class="toggle_up tip" title="<?= lang('hide_form') ?>">
                        <i class="icon fa fa-toggle-up"></i>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="toggle_down tip" title="<?= lang('show_form') ?>">
                        <i class="icon fa fa-toggle-down"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="box-content">

        <div class="row">
            <div class="col-md-12" style="margin-bottom: 25px;">
                <div id="form">
                    <div  class="col-sm-2">
                        <?= lang("status_document", "flStatusDocument") ?>
                        <?php
                        $cbStatus = array(
                            '' => lang('select'),
                            'draft' => lang('draft'),
                            'unsigned' => lang('unsigned'),
                            'pending' => lang('pending'),
                            'completed' => lang('completed'),
                            'canceled' => lang('canceled'),
                        );
                        echo form_dropdown('flStatusDocument', $cbStatus,  '', 'class="form-control" id="flStatusDocument"'); ?>
                    </div>
                    <div class="col-sm-2">
                        <?= lang("biller", "billerFilter"); ?>
                        <?php
                        $bl[""] = lang("select") . ' ' . lang("biller") ;
                        foreach ($billers as $biller) {
                            $bl[$biller->id] = $biller->name;
                        }
                        echo form_dropdown('flBiller', $bl, '', 'id="billerFilter" name="billerFilter" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"'); ?>
                    </div>
                    <div class="col-sm-8">
                        <?= lang("search_by", "flFolder"); ?>
                        <?php echo form_input('flFolder', '', 'class="form-control" id="flFolder" placeholder="' . lang("search_folder") . '" '); ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div id="grid_searh"></div>
            </div>
            <div class="col-md-12" style="text-align: center;margin-top: 25px;">
                <div class="form-group">
                    <?php echo form_button('load_more',  $this->lang->line("load_more_folders"), 'id="load_more" class="btn btn-primary"'); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript">

    var limit = 0;

    $(document).ready(function () {

        load_folders_searh();

        $('#load_more').click(function () {

            limit += 25;

            load_folders_searh();
        });

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });

        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });

        $('#flStatusDocument').change(function() {
            load_folders_searh();
        });


        $('#billerFilter').change(function() {
            load_folders_searh();
        });

        $('#flFolder').on('input', debounce(load_folders_searh, 300)); // 300ms de atraso

    });

    function load_folders_searh() {

        var searchTerm = $('#search_term').val();
        var statusDocument = $('#flStatusDocument').val();
        var billerFilter = $('#billerFilter').val();
        var flFolder = $('#flFolder').val();

        var url = '<?= base_url('folders/folders_search') ?>?search_term=' + encodeURIComponent(searchTerm) +
            '&statusDocument=' + encodeURIComponent(statusDocument) +
            '&billerID=' + encodeURIComponent(billerFilter) +
            '&strFolder=' + encodeURIComponent(flFolder) +
            '&limit=' + limit;

        $('#grid_searh').load(url, function () {

            $('.folders').click(function (event) {

                if ($(event.target).closest('.box-icon').length > 0) {
                    if ($(event.target).is('a')) {
                        return true;
                    } else {
                        return;
                    }
                }

                var folder_id = $(this).attr('folder_id');

                var url_open = '<?= base_url('folders/folder_open') ?>/' + folder_id + '?search_term=' + encodeURIComponent(searchTerm) +
                    '&statusDocument=' + encodeURIComponent(statusDocument) +
                    '&billerID=' + encodeURIComponent(billerFilter) +
                    '&strFolder=' + encodeURIComponent(flFolder);

                window.location.href = url_open;

            });

            $('.dropdown').on('show.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
            });
            $('.dropdown').on('hide.bs.dropdown', function(e){
                $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
            });

            $('.tip').tooltip();

        });
    }

    function debounce(func, wait) {
        let timeout;
        return function(...args) {
            const later = () => {
                clearTimeout(timeout);
                func.apply(this, args);
            };
            clearTimeout(timeout);
            timeout = setTimeout(later, wait);
        };
    }

</script>

