<?php if (!empty($folders)) { ?>
    <?php foreach ($folders as $folder) { ?>
        <div class="col-lg-2">
            <div class="panel panel-info folders tip" title="<?=$folder->name;?>" folder_id="<?=$folder->id;?>" style="cursor: pointer;border-color: #dbdee0;">
                <div class="panel-heading" style="color: #31708f;background-color: #FFFFFF;border-color: #FFFFFF;">
                    <?php if ($folder->id > 1) {?>
                        <div class="box-icon">
                            <ul class="btn-tasks">
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#" style="color: #333333;"><i class="icon fa fa-tasks tip" style="font-size: 20px;" data-placement="left" title="" data-original-title="Ações"></i></a>
                                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel" style="display: none;">
                                        <li>
                                            <a href="<?=site_url('folders/editFolder/'.$folder->id)?>" data-toggle="modal" data-target="#myModal">
                                                <i class="fa fa-edit"></i> <?=lang('rename_folder')?>
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="#" class="delete_folder" id="<?=$folder->id;?>">
                                                <i class="fa fa-trash-o"></i> <?=lang('delete_folder')?>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <?php }?>
                </div>
                <?php
                $folder_name = $folder->name;
                if (strlen($folder_name) > 60) {
                    $folder_name = substr($folder_name, 0, 60) . '...';
                }
                ?>
                <div class="panel-body" style="height: 170px;overflow-y: auto;">
                    <div style="text-align: center">
                        <i class="fa fa-folder fa-4x"></i><br/> <?=$folder_name;?>
                    </div>
                </div>

            </div>
        </div>
    <?php } ?>
<?php } else {?>
    <div class="alert alert-info">
        <?= lang('no_documents_found') ?>
    </div>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function () {

        $('.delete_folder').click(function (e) {
            e.preventDefault();
            if (confirm('Deseja Realmente Deletar a Pasta?')) {
                var folder_id = $(this).attr('id');
                var url = '<?=base_url('folders/deleteFolder/');?>' + folder_id;
                $.ajax({
                    url: url,
                    type: 'GET',
                    success: function (data) {
                        location.reload();
                    }
                });
            }
        });

    });
</script>
